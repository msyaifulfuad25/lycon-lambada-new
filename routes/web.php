<?php

use Illuminate\Support\Facades\Route;


Route::get('/', 'HomeController@index');
Route::get('/login', 'DashboardBaruController@login');
Route::post('/login/loginPost', 'LoginUserController@loginPost');
Route::get('/login/logout', 'LoginUserController@logout');
Route::get('/pages/clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('config:clear');
    return "Cache is cleared";
});

Route::post('/savedata', 'SystemController@savedata');
Route::get('/getdata', 'SystemController@getdata');
Route::post('/ambildata_tabel', 'DashboardController@ambildata_tabel');
Route::get('/export_excel', 'DashboardController@export_excel');
Route::post('/ambildata', 'SystemController@ambildata');
Route::post('/ambildata_from', 'DashboardController@ambildata_from');
Route::post('/buttonFilt2', 'DashboardController@buttonFilt');
Route::post('/ordertable', 'SystemController@ordertable');
Route::post('/hapus', 'SystemController@hapus');
Route::post('/edit', 'SystemController@edit');
Route::post('/filtertabel', 'SystemController@filtertabel');
Route::post('/ambilchart', 'ChartController@ambilchart');
Route::post('/get_chart', 'ChartController@get_chart');
Route::post('/ambilSlide', 'ChartController@ambilSlide');
Route::post('/ambilTabel', 'ChartController@ambilTabel');
Route::get('/{noid}', 'HomeController@home');
Route::get('/{noid}/{urlprint}', 'HomeController@print');
Route::post('/read_notification', 'SystemController@read_notification');

//Master Table
Route::post('/get_master_table', 'MasterTabelController@get_master_table');
Route::post('/get_data_master_table', 'MasterTabelController@get_data_master_table');
Route::post('/get_filter_button_master_table', 'MasterTabelController@get_filter_button_master_table');
Route::post('/get_form_master_table', 'MasterTabelController@get_form_master_table');
Route::post('/get_slide_master_table', 'MasterTabelController@get_slide_master_table');
Route::post('/find_master_table', 'MasterTabelController@find_master_table');
Route::post('/save_master_table', 'MasterTabelController@save_master_table');
Route::post('/delete_master_table', 'MasterTabelController@delete_master_table');
Route::post('/delete_selected_master_table', 'MasterTabelController@delete_selected_master_table');
Route::post('/save_filter_master_table', 'MasterTabelController@save_filter_master_table');
Route::post('/get_next_noid_master_table', 'MasterTabelController@get_next_noid_master_table');
Route::post('/upload_file_master_table', 'MasterTabelController@upload_file_master_table');
Route::post('/get_file_master_table', 'MasterTabelController@get_file_master_table');
Route::post('/edit_file_master_table', 'MasterTabelController@edit_file_master_table');

//Transaksi Master Detail
Route::post('/get_tmd', 'TransaksiMasterDetailController@get_tmd');
Route::post('/get_data_tmd', 'TransaksiMasterDetailController@get_data_tmd');
Route::post('/get_tab_data_tmd', 'TransaksiMasterDetailController@get_tab_data_tmd');
Route::post('/get_filter_button_tmd', 'TransaksiMasterDetailController@get_filter_button_tmd');
Route::post('/get_form_tmd', 'TransaksiMasterDetailController@get_form_tmd');
Route::post('/get_slide_tmd', 'TransaksiMasterDetailController@get_slide_tmd');
Route::post('/find_tmd', 'TransaksiMasterDetailController@find_tmd');
Route::post('/save_tmd', 'TransaksiMasterDetailController@save_tmd');
Route::post('/delete_tmd', 'TransaksiMasterDetailController@delete_tmd');
Route::post('/delete_selected_tmd', 'TransaksiMasterDetailController@delete_selected_tmd');
Route::post('/save_filter_tmd', 'TransaksiMasterDetailController@save_filter_tmd');
Route::post('/delete_tab_tmd', 'TransaksiMasterDetailController@delete_tab_tmd');
Route::post('/get_next_noid_tmd', 'TransaksiMasterDetailController@get_next_noid_tmd');
Route::post('/get_trigger_on_tmd', 'TransaksiMasterDetailController@get_trigger_on_tmd');
Route::post('/get_select_child_tmd', 'TransaksiMasterDetailController@get_select_child_tmd');
Route::post('/save_log_tmd', 'TransaksiMasterDetailController@save_log_tmd');
Route::post('/print_tmd', 'TransaksiMasterDetailController@print_tmd');

// APPSETUP
// Application Config
// Master
// Master Applicant
Route::post('/getApplicant_99020209', 'lam_custom_master\MApplicantController@getApplicant');
Route::post('/getApplicantDetail_99020209', 'lam_custom_master\MApplicantController@getApplicantDetail');


//Custom Transaksi Master Detail
// Dashboard Purchase
Route::post('/getMostItemRequested_40020101', 'transaction\purchase_modules\dashboard\DashboardController@getMostItemRequested');
Route::post('/getMostItemRequestedDetail_40020101', 'transaction\purchase_modules\dashboard\DashboardController@getMostItemRequestedDetail');

// Cash Bank Advance
Route::post('/getData_20211001', 'fica\finance_modules\cash_bank_advance\CashBankAdvanceController@getData');
Route::post('/saveData_20211001', 'fica\finance_modules\cash_bank_advance\CashBankAdvanceController@saveData');
Route::post('/updateData_20211001', 'fica\finance_modules\cash_bank_advance\CashBankAdvanceController@updateData');
Route::post('/deleteData_20211001', 'fica\finance_modules\cash_bank_advance\CashBankAdvanceController@deleteData');


//PRQ
Route::post('/get_prq_ctmd', 'purchase\PurchaseRequestController@get');
Route::post('/get_detail_prq_ctmd', 'purchase\PurchaseRequestController@get_detail');
Route::post('/get_detailpi_prq_ctmd', 'purchase\PurchaseRequestController@get_detailpi');
Route::post('/get_log_prq_ctmd', 'purchase\PurchaseRequestController@get_log');
Route::post('/get_costcontrol_prq_ctmd', 'purchase\PurchaseRequestController@get_costcontrol');
Route::post('/save_prq_ctmd', 'purchase\PurchaseRequestController@save');
Route::post('/find_prq_ctmd', 'purchase\PurchaseRequestController@find');
Route::post('/delete_prq_ctmd', 'purchase\PurchaseRequestController@delete');
Route::post('/find_last_supplier_prq_ctmd', 'purchase\PurchaseRequestController@find_last_supplier');
Route::post('/send_to_next_prq_ctmd', 'purchase\PurchaseRequestController@send_to_next');
Route::post('/set_pending_prq_ctmd', 'purchase\PurchaseRequestController@set_pending');
Route::post('/snst_prq_ctmd', 'purchase\PurchaseRequestController@snst');
Route::post('/div_prq_ctmd', 'purchase\PurchaseRequestController@div');
Route::post('/sapcf_prq_ctmd', 'purchase\PurchaseRequestController@sapcf');
Route::post('/set_canceled_prq_ctmd', 'purchase\PurchaseRequestController@set_canceled');
Route::post('/generate_code_prq_ctmd', 'purchase\PurchaseRequestController@generate_code');
Route::post('/is_enable_request_by_prq_ctmd', 'purchase\PurchaseRequestController@is_enable_request_by');
Route::post('/get_info_prq_ctmd', 'purchase\PurchaseRequestController@get_info');
Route::post('/delete_select_prq_ctmd', 'purchase\PurchaseRequestController@delete_select_m');
Route::post('/get_notification_prq_ctmd', 'purchase\PurchaseRequestController@get_notification');
Route::post('/get_supplier_to_next_prq_ctmd', 'purchase\PurchaseRequestController@get_supplier_to_next');
Route::post('/get_department_by_cc_prq_ctmd', 'purchase\PurchaseRequestController@get_department_by_cc');
Route::post('/save_inventor_prq_ctmd', 'purchase\PurchaseRequestController@save_inventor');
Route::post('/get_inventor_prq_ctmd', 'purchase\PurchaseRequestController@get_inventor');
Route::post('/get_select2ajax_prq_ctmd', 'purchase\PurchaseRequestController@get_select2ajax');
//POF
Route::post('/get_pof_ctmd', 'purchase\PurchaseOfferController@get');
Route::post('/get_detail_pof_ctmd', 'purchase\PurchaseOfferController@get_detail');
Route::post('/get_detailpi_pof_ctmd', 'purchase\PurchaseOfferController@get_detailpi');
Route::post('/get_log_pof_ctmd', 'purchase\PurchaseOfferController@get_log');
Route::post('/get_costcontrol_pof_ctmd', 'purchase\PurchaseOfferController@get_costcontrol');
Route::post('/save_pof_ctmd', 'purchase\PurchaseOfferController@save');
Route::post('/find_pof_ctmd', 'purchase\PurchaseOfferController@find');
Route::post('/delete_pof_ctmd', 'purchase\PurchaseOfferController@delete');
Route::post('/find_last_supplier_pof_ctmd', 'purchase\PurchaseOfferController@find_last_supplier');
Route::post('/send_to_next_pof_ctmd', 'purchase\PurchaseOfferController@send_to_next');
Route::post('/set_pending_pof_ctmd', 'purchase\PurchaseOfferController@set_pending');
Route::post('/snst_pof_ctmd', 'purchase\PurchaseOfferController@snst');
Route::post('/div_pof_ctmd', 'purchase\PurchaseOfferController@div');
Route::post('/sapcf_pof_ctmd', 'purchase\PurchaseOfferController@sapcf');
Route::post('/set_canceled_pof_ctmd', 'purchase\PurchaseOfferController@set_canceled');
Route::post('/generate_code_pof_ctmd', 'purchase\PurchaseOfferController@generate_code');
Route::post('/get_info_pof_ctmd', 'purchase\PurchaseOfferController@get_info');
Route::post('/delete_select_pof_ctmd', 'purchase\PurchaseOfferController@delete_select_m');
Route::post('/get_notification_pof_ctmd', 'purchase\PurchaseOfferController@get_notification');
Route::post('/duplicate_data_pof_ctmd', 'purchase\PurchaseOfferController@duplicate_data');
Route::post('/get_supplier_in_offer_pof_ctmd', 'purchase\PurchaseOfferController@get_supplier_in_offer');
Route::post('/get_duplicate_pof_ctmd', 'purchase\PurchaseOfferController@get_duplicate');
Route::post('/get_inventor_pof_ctmd', 'purchase\PurchaseOfferController@get_inventor');
//POFI
Route::post('/get_pofi_ctmd', 'purchase\PurchaseOfferItemsController@get');
Route::post('/get_detail_pofi_ctmd', 'purchase\PurchaseOfferItemsController@get_detail');
Route::post('/get_detailpi_pofi_ctmd', 'purchase\PurchaseOfferItemsController@get_detailpi');
Route::post('/get_log_pofi_ctmd', 'purchase\PurchaseOfferItemsController@get_log');
Route::post('/get_costcontrol_pofi_ctmd', 'purchase\PurchaseOfferItemsController@get_costcontrol');
Route::post('/save_pofi_ctmd', 'purchase\PurchaseOfferItemsController@save');
Route::post('/find_pofi_ctmd', 'purchase\PurchaseOfferItemsController@find');
Route::post('/delete_pofi_ctmd', 'purchase\PurchaseOfferItemsController@delete');
Route::post('/find_last_supplier_pofi_ctmd', 'purchase\PurchaseOfferItemsController@find_last_supplier');
Route::post('/send_to_next_pofi_ctmd', 'purchase\PurchaseOfferItemsController@send_to_next');
Route::post('/send_to_nexts_pofi_ctmd', 'purchase\PurchaseOfferItemsController@send_to_nexts');
Route::post('/set_pending_pofi_ctmd', 'purchase\PurchaseOfferItemsController@set_pending');
Route::post('/snst_pofi_ctmd', 'purchase\PurchaseOfferItemsController@snst');
Route::post('/div_pofi_ctmd', 'purchase\PurchaseOfferItemsController@div');
Route::post('/sapcf_pofi_ctmd', 'purchase\PurchaseOfferItemsController@sapcf');
Route::post('/set_canceled_pofi_ctmd', 'purchase\PurchaseOfferItemsController@set_canceled');
Route::post('/generate_code_pofi_ctmd', 'purchase\PurchaseOfferItemsController@generate_code');
Route::post('/get_info_pofi_ctmd', 'purchase\PurchaseOfferItemsController@get_info');
Route::post('/delete_select_pofi_ctmd', 'purchase\PurchaseOfferItemsController@delete_select_m');
Route::post('/get_notification_pofi_ctmd', 'purchase\PurchaseOfferItemsController@get_notification');
Route::post('/duplicate_data_pofi_ctmd', 'purchase\PurchaseOfferItemsController@duplicate_data');
Route::post('/get_supplier_in_offer_pofi_ctmd', 'purchase\PurchaseOfferItemsController@get_supplier_in_offer');
//POR
Route::post('/get_por_ctmd', 'purchase\PurchaseOrderController@get');
Route::post('/get_detail_por_ctmd', 'purchase\PurchaseOrderController@get_detail');
Route::post('/get_detailpi_por_ctmd', 'purchase\PurchaseOrderController@get_detailpi');
Route::post('/get_log_por_ctmd', 'purchase\PurchaseOrderController@get_log');
Route::post('/get_costcontrol_por_ctmd', 'purchase\PurchaseOrderController@get_costcontrol');
Route::post('/save_por_ctmd', 'purchase\PurchaseOrderController@save');
Route::post('/find_por_ctmd', 'purchase\PurchaseOrderController@find');
Route::post('/delete_por_ctmd', 'purchase\PurchaseOrderController@delete');
Route::post('/find_last_supplier_por_ctmd', 'purchase\PurchaseOrderController@find_last_supplier');
Route::post('/send_to_next_por_ctmd', 'purchase\PurchaseOrderController@send_to_next');
Route::post('/set_pending_por_ctmd', 'purchase\PurchaseOrderController@set_pending');
Route::post('/snst_por_ctmd', 'purchase\PurchaseOrderController@snst');
Route::post('/div_por_ctmd', 'purchase\PurchaseOrderController@div');
Route::post('/sapcf_por_ctmd', 'purchase\PurchaseOrderController@sapcf');
Route::post('/set_canceled_por_ctmd', 'purchase\PurchaseOrderController@set_canceled');
Route::post('/generate_code_por_ctmd', 'purchase\PurchaseOrderController@generate_code');
Route::post('/get_info_por_ctmd', 'purchase\PurchaseOrderController@get_info');
Route::post('/delete_select_por_ctmd', 'purchase\PurchaseOrderController@delete_select_m');
Route::post('/get_notification_por_ctmd', 'purchase\PurchaseOrderController@get_notification');
//PORI
Route::post('/get_pori_ctmd', 'purchase\PurchaseOrderItemsController@get');
Route::post('/get_detail_pori_ctmd', 'purchase\PurchaseOrderItemsController@get_detail');
Route::post('/get_detailpi_pori_ctmd', 'purchase\PurchaseOrderItemsController@get_detailpi');
Route::post('/get_log_pori_ctmd', 'purchase\PurchaseOrderItemsController@get_log');
Route::post('/get_costcontrol_pori_ctmd', 'purchase\PurchaseOrderItemsController@get_costcontrol');
Route::post('/save_pori_ctmd', 'purchase\PurchaseOrderItemsController@save');
Route::post('/find_pori_ctmd', 'purchase\PurchaseOrderItemsController@find');
Route::post('/delete_pori_ctmd', 'purchase\PurchaseOrderItemsController@delete');
Route::post('/find_last_supplier_pori_ctmd', 'purchase\PurchaseOrderItemsController@find_last_supplier');
Route::post('/send_to_next_pori_ctmd', 'purchase\PurchaseOrderItemsController@send_to_next');
Route::post('/send_to_nexts_pori_ctmd', 'purchase\PurchaseOrderItemsController@send_to_nexts');
Route::post('/set_pending_pori_ctmd', 'purchase\PurchaseOrderItemsController@set_pending');
Route::post('/snst_pori_ctmd', 'purchase\PurchaseOrderItemsController@snst');
Route::post('/div_pori_ctmd', 'purchase\PurchaseOrderItemsController@div');
Route::post('/sapcf_pori_ctmd', 'purchase\PurchaseOrderItemsController@sapcf');
Route::post('/set_canceled_pori_ctmd', 'purchase\PurchaseOrderItemsController@set_canceled');
Route::post('/generate_code_pori_ctmd', 'purchase\PurchaseOrderItemsController@generate_code');
Route::post('/get_info_pori_ctmd', 'purchase\PurchaseOrderItemsController@get_info');
Route::post('/delete_select_pori_ctmd', 'purchase\PurchaseOrderItemsController@delete_select_m');
//PDN
Route::post('/get_pdn_ctmd', 'purchase\PurchaseDeliveryController@get');
Route::post('/get_detail_pdn_ctmd', 'purchase\PurchaseDeliveryController@get_detail');
Route::post('/get_detailpi_pdn_ctmd', 'purchase\PurchaseDeliveryController@get_detailpi');
Route::post('/get_kstock_pdn_ctmd', 'purchase\PurchaseDeliveryController@get_kstock');
Route::post('/get_log_pdn_ctmd', 'purchase\PurchaseDeliveryController@get_log');
Route::post('/get_costcontrol_pdn_ctmd', 'purchase\PurchaseDeliveryController@get_costcontrol');
Route::post('/save_pdn_ctmd', 'purchase\PurchaseDeliveryController@save');
Route::post('/find_pdn_ctmd', 'purchase\PurchaseDeliveryController@find');
Route::post('/delete_pdn_ctmd', 'purchase\PurchaseDeliveryController@delete');
Route::post('/find_last_supplier_pdn_ctmd', 'purchase\PurchaseDeliveryController@find_last_supplier');
Route::post('/send_to_next_pdn_ctmd', 'purchase\PurchaseDeliveryController@send_to_next');
Route::post('/set_pending_pdn_ctmd', 'purchase\PurchaseDeliveryController@set_pending');
Route::post('/snst_pdn_ctmd', 'purchase\PurchaseDeliveryController@snst');
Route::post('/div_pdn_ctmd', 'purchase\PurchaseDeliveryController@div');
Route::post('/sapcf_pdn_ctmd', 'purchase\PurchaseDeliveryController@sapcf');
Route::post('/set_canceled_pdn_ctmd', 'purchase\PurchaseDeliveryController@set_canceled');
Route::post('/generate_code_pdn_ctmd', 'purchase\PurchaseDeliveryController@generate_code');
Route::post('/get_info_pdn_ctmd', 'purchase\PurchaseDeliveryController@get_info');
Route::post('/delete_select_pdn_ctmd', 'purchase\PurchaseDeliveryController@delete_select_m');
Route::post('/get_itemorder_pdn_ctmd', 'purchase\PurchaseDeliveryController@get_itemorder');
Route::post('/find_por_pdn_ctmd', 'purchase\PurchaseDeliveryController@find_por');
Route::post('/get_notification_pdn_ctmd', 'purchase\PurchaseDeliveryController@get_notification');
Route::post('/get_inventor_pdn_ctmd', 'purchase\PurchaseDeliveryController@get_inventor');
//PIV
Route::post('/get_piv_ctmd', 'purchase\PurchaseInvoiceController@get');
Route::post('/get_detail_piv_ctmd', 'purchase\PurchaseInvoiceController@get_detail');
Route::post('/get_detailpi_piv_ctmd', 'purchase\PurchaseInvoiceController@get_detailpi');
Route::post('/get_log_piv_ctmd', 'purchase\PurchaseInvoiceController@get_log');
Route::post('/get_costcontrol_piv_ctmd', 'purchase\PurchaseInvoiceController@get_costcontrol');
Route::post('/save_piv_ctmd', 'purchase\PurchaseInvoiceController@save');
Route::post('/find_piv_ctmd', 'purchase\PurchaseInvoiceController@find');
Route::post('/delete_piv_ctmd', 'purchase\PurchaseInvoiceController@delete');
Route::post('/find_last_supplier_piv_ctmd', 'purchase\PurchaseInvoiceController@find_last_supplier');
Route::post('/send_to_next_piv_ctmd', 'purchase\PurchaseInvoiceController@send_to_next');
Route::post('/set_pending_piv_ctmd', 'purchase\PurchaseInvoiceController@set_pending');
Route::post('/snst_piv_ctmd', 'purchase\PurchaseInvoiceController@snst');
Route::post('/div_piv_ctmd', 'purchase\PurchaseInvoiceController@div');
Route::post('/sapcf_piv_ctmd', 'purchase\PurchaseInvoiceController@sapcf');
Route::post('/set_canceled_piv_ctmd', 'purchase\PurchaseInvoiceController@set_canceled');
Route::post('/generate_code_piv_ctmd', 'purchase\PurchaseInvoiceController@generate_code');
Route::post('/get_info_piv_ctmd', 'purchase\PurchaseInvoiceController@get_info');
Route::post('/delete_select_piv_ctmd', 'purchase\PurchaseInvoiceController@delete_select_m');
Route::post('/get_notification_piv_ctmd', 'purchase\PurchaseInvoiceController@get_notification');
Route::post('/find_por_piv_ctmd', 'purchase\PurchaseInvoiceController@find_por');
Route::post('/get_inventor_piv_ctmd', 'purchase\PurchaseInvoiceController@get_inventor');
Route::post('/pay_piv_ctmd', 'purchase\PurchaseInvoiceController@pay');
//RPTD
Route::post('/get_rptd_ctmd', 'purchase\ReportingPurchaseController@get');
Route::post('/get_costcenter_rptd_ctmd', 'purchase\ReportingPurchaseController@get_costcenter');
Route::post('/generate_urlprint_rptd_ctmd', 'purchase\ReportingPurchaseController@generate_urlprint');
Route::get('/exportexcel/40029901/{urlprint}', 'purchase\ReportingPurchaseController@exportexcel');
//RPTW
Route::post('/get_rptw_ctmd', 'purchase\ReportingWeeklyController@get');
//RPTM
Route::post('/get_rptm_ctmd', 'purchase\ReportingMonthlyController@get');
//PPY
Route::post('/get_ppy_ctmd', 'purchase\PurchasePaymentController@get');
Route::post('/get_detail_ppy_ctmd', 'purchase\PurchasePaymentController@get_detail');
Route::post('/get_detailpi_ppy_ctmd', 'purchase\PurchasePaymentController@get_detailpi');
Route::post('/get_log_ppy_ctmd', 'purchase\PurchasePaymentController@get_log');
Route::post('/get_costcontrol_ppy_ctmd', 'purchase\PurchasePaymentController@get_costcontrol');
Route::post('/save_ppy_ctmd', 'purchase\PurchasePaymentController@save');
Route::post('/find_ppy_ctmd', 'purchase\PurchasePaymentController@find');
Route::post('/delete_ppy_ctmd', 'purchase\PurchasePaymentController@delete');
Route::post('/find_last_supplier_ppy_ctmd', 'purchase\PurchasePaymentController@find_last_supplier');
Route::post('/send_to_next_ppy_ctmd', 'purchase\PurchasePaymentController@send_to_next');
Route::post('/set_pending_ppy_ctmd', 'purchase\PurchasePaymentController@set_pending');
Route::post('/snst_ppy_ctmd', 'purchase\PurchasePaymentController@snst');
Route::post('/div_ppy_ctmd', 'purchase\PurchasePaymentController@div');
Route::post('/sapcf_ppy_ctmd', 'purchase\PurchasePaymentController@sapcf');
Route::post('/set_canceled_ppy_ctmd', 'purchase\PurchasePaymentController@set_canceled');
Route::post('/generate_code_ppy_ctmd', 'purchase\PurchasePaymentController@generate_code');
Route::post('/get_info_ppy_ctmd', 'purchase\PurchasePaymentController@get_info');
Route::post('/delete_select_ppy_ctmd', 'purchase\PurchasePaymentController@delete_select_m');
Route::post('/find_por_ppy_ctmd', 'purchase\PurchasePaymentController@find_por');
//KAD
Route::post('/get_kad_ctmd', 'fica\KasAdvanceController@get');
Route::post('/get_detail_kad_ctmd', 'fica\KasAdvanceController@get_detail');
Route::post('/get_detailpi_kad_ctmd', 'fica\KasAdvanceController@get_detailpi');
Route::post('/get_log_kad_ctmd', 'fica\KasAdvanceController@get_log');
Route::post('/get_costcontrol_kad_ctmd', 'fica\KasAdvanceController@get_costcontrol');
Route::post('/save_kad_ctmd', 'fica\KasAdvanceController@save');
Route::post('/find_kad_ctmd', 'fica\KasAdvanceController@find');
Route::post('/delete_kad_ctmd', 'fica\KasAdvanceController@delete');
Route::post('/find_last_supplier_kad_ctmd', 'fica\KasAdvanceController@find_last_supplier');
Route::post('/send_to_next_kad_ctmd', 'fica\KasAdvanceController@send_to_next');
Route::post('/set_pending_kad_ctmd', 'fica\KasAdvanceController@set_pending');
Route::post('/snst_kad_ctmd', 'fica\KasAdvanceController@snst');
Route::post('/div_kad_ctmd', 'fica\KasAdvanceController@div');
Route::post('/sapcf_kad_ctmd', 'fica\KasAdvanceController@sapcf');
Route::post('/set_canceled_kad_ctmd', 'fica\KasAdvanceController@set_canceled');
Route::post('/generate_code_kad_ctmd', 'fica\KasAdvanceController@generate_code');
Route::post('/get_info_kad_ctmd', 'fica\KasAdvanceController@get_info');
Route::post('/delete_select_kad_ctmd', 'fica\KasAdvanceController@delete_select_m');
Route::post('/find_por_kad_ctmd', 'fica\KasAdvanceController@find_por');


// SALES INQUIRY
Route::post('/get_si_ctmd', 'sales\SalesInquiryController@get');
Route::post('/get_detail_si_ctmd', 'sales\SalesInquiryController@get_detail');
Route::post('/get_detailpi_si_ctmd', 'sales\SalesInquiryController@get_detailpi');
Route::post('/get_log_si_ctmd', 'sales\SalesInquiryController@get_log');
Route::post('/get_costcontrol_si_ctmd', 'sales\SalesInquiryController@get_costcontrol');
Route::post('/save_si_ctmd', 'sales\SalesInquiryController@save');
Route::post('/find_si_ctmd', 'sales\SalesInquiryController@find');
Route::post('/delete_si_ctmd', 'sales\SalesInquiryController@delete');
Route::post('/find_last_supplier_si_ctmd', 'sales\SalesInquiryController@find_last_supplier');
Route::post('/send_to_next_si_ctmd', 'sales\SalesInquiryController@send_to_next');
Route::post('/set_pending_si_ctmd', 'sales\SalesInquiryController@set_pending');
Route::post('/snst_si_ctmd', 'sales\SalesInquiryController@snst');
Route::post('/div_si_ctmd', 'sales\SalesInquiryController@div');
Route::post('/sapcf_si_ctmd', 'sales\SalesInquiryController@sapcf');
Route::post('/set_canceled_si_ctmd', 'sales\SalesInquiryController@set_canceled');
Route::post('/generate_code_si_ctmd', 'sales\SalesInquiryController@generate_code');
Route::post('/get_info_si_ctmd', 'sales\SalesInquiryController@get_info');
Route::post('/delete_select_si_ctmd', 'sales\SalesInquiryController@delete_select_m');
Route::post('/cdf_upload_si_ctmd', 'sales\SalesInquiryController@cdf_upload');
Route::post('/cdf_get_si_ctmd', 'sales\SalesInquiryController@cdf_get');
Route::post('/cdf_edit_si_ctmd', 'sales\SalesInquiryController@cdf_edit');
Route::get('/download_si_ctmd/document/{id}', 'sales\SalesInquiryController@download');
Route::get('/download_template_si_ctmd/document/rap', 'sales\SalesInquiryController@download_template');
Route::post('/treeview_rap_si_ctmd', 'sales\SalesInquiryController@treeview_rap');
Route::post('/treeview_ws_si_ctmd', 'sales\SalesInquiryController@treeview_ws');
Route::post('/get_subgroup_si_ctmd', 'sales\SalesInquiryController@get_subgroup');
Route::get('/{noid}/print_rap/{urlprint}', 'sales\SalesInquiryController@print_rap');
Route::get('/{noid}/print_ws/{urlprint}', 'sales\SalesInquiryController@print_ws');
Route::get('/{noid}/print_recap/{urlprint}', 'sales\SalesInquiryController@print_recap');
Route::get('/{noid}/print_ps/{urlprint}', 'sales\SalesInquiryController@print_ps');
Route::post('/get_select2ajax_si_ctmd', 'sales\SalesInquiryController@get_select2ajax');
Route::post('/import_rap_si_ctmd', 'sales\SalesInquiryController@import_rap');
// SALES ORDER
Route::post('/get_so_ctmd', 'sales\SalesOrderController@get');
Route::post('/get_detail_so_ctmd', 'sales\SalesOrderController@get_detail');
Route::post('/get_detailpi_so_ctmd', 'sales\SalesOrderController@get_detailpi');
Route::post('/get_log_so_ctmd', 'sales\SalesOrderController@get_log');
Route::post('/get_costcontrol_so_ctmd', 'sales\SalesOrderController@get_costcontrol');
Route::post('/save_so_ctmd', 'sales\SalesOrderController@save');
Route::post('/find_so_ctmd', 'sales\SalesOrderController@find');
Route::post('/delete_so_ctmd', 'sales\SalesOrderController@delete');
Route::post('/find_last_supplier_so_ctmd', 'sales\SalesOrderController@find_last_supplier');
Route::post('/send_to_next_so_ctmd', 'sales\SalesOrderController@send_to_next');
Route::post('/set_pending_so_ctmd', 'sales\SalesOrderController@set_pending');
Route::post('/snst_so_ctmd', 'sales\SalesOrderController@snst');
Route::post('/div_so_ctmd', 'sales\SalesOrderController@div');
Route::post('/sapcf_so_ctmd', 'sales\SalesOrderController@sapcf');
Route::post('/set_canceled_so_ctmd', 'sales\SalesOrderController@set_canceled');
Route::post('/generate_code_so_ctmd', 'sales\SalesOrderController@generate_code');
Route::post('/get_info_so_ctmd', 'sales\SalesOrderController@get_info');
Route::post('/delete_select_so_ctmd', 'sales\SalesOrderController@delete_select_m');
Route::post('/cdf_upload_so_ctmd', 'sales\SalesOrderController@cdf_upload');
Route::post('/cdf_get_so_ctmd', 'sales\SalesOrderController@cdf_get');
Route::post('/cdf_edit_so_ctmd', 'sales\SalesOrderController@cdf_edit');

// SALES DELIVERY
Route::post('/get_sd_ctmd', 'sales\SalesDeliveryController@get');
Route::post('/get_detail_sd_ctmd', 'sales\SalesDeliveryController@get_detail');
Route::post('/get_detailpi_sd_ctmd', 'sales\SalesDeliveryController@get_detailpi');
Route::post('/get_log_sd_ctmd', 'sales\SalesDeliveryController@get_log');
Route::post('/get_costcontrol_sd_ctmd', 'sales\SalesDeliveryController@get_costcontrol');
Route::post('/save_sd_ctmd', 'sales\SalesDeliveryController@save');
Route::post('/find_sd_ctmd', 'sales\SalesDeliveryController@find');
Route::post('/delete_sd_ctmd', 'sales\SalesDeliveryController@delete');
Route::post('/find_last_supplier_sd_ctmd', 'sales\SalesDeliveryController@find_last_supplier');
Route::post('/send_to_next_sd_ctmd', 'sales\SalesDeliveryController@send_to_next');
Route::post('/set_pending_sd_ctmd', 'sales\SalesDeliveryController@set_pending');
Route::post('/snst_sd_ctmd', 'sales\SalesDeliveryController@snst');
Route::post('/div_sd_ctmd', 'sales\SalesDeliveryController@div');
Route::post('/sapcf_sd_ctmd', 'sales\SalesDeliveryController@sapcf');
Route::post('/set_canceled_sd_ctmd', 'sales\SalesDeliveryController@set_canceled');
Route::post('/generate_code_sd_ctmd', 'sales\SalesDeliveryController@generate_code');
Route::post('/get_info_sd_ctmd', 'sales\SalesDeliveryController@get_info');
Route::post('/delete_select_sd_ctmd', 'sales\SalesDeliveryController@delete_select_m');
Route::post('/cdf_upload_sd_ctmd', 'sales\SalesDeliveryController@cdf_upload');
Route::post('/cdf_get_sd_ctmd', 'sales\SalesDeliveryController@cdf_get');
Route::post('/cdf_edit_sd_ctmd', 'sales\SalesDeliveryController@cdf_edit');

// SALES INVOICE
Route::post('/get_siv_ctmd', 'sales\SalesInvoiceController@get');
Route::post('/get_detail_siv_ctmd', 'sales\SalesInvoiceController@get_detail');
Route::post('/get_detailpi_siv_ctmd', 'sales\SalesInvoiceController@get_detailpi');
Route::post('/get_log_siv_ctmd', 'sales\SalesInvoiceController@get_log');
Route::post('/get_costcontrol_siv_ctmd', 'sales\SalesInvoiceController@get_costcontrol');
Route::post('/save_siv_ctmd', 'sales\SalesInvoiceController@save');
Route::post('/find_siv_ctmd', 'sales\SalesInvoiceController@find');
Route::post('/delete_siv_ctmd', 'sales\SalesInvoiceController@delete');
Route::post('/find_last_supplier_siv_ctmd', 'sales\SalesInvoiceController@find_last_supplier');
Route::post('/send_to_next_siv_ctmd', 'sales\SalesInvoiceController@send_to_next');
Route::post('/set_pending_siv_ctmd', 'sales\SalesInvoiceController@set_pending');
Route::post('/snst_siv_ctmd', 'sales\SalesInvoiceController@snst');
Route::post('/div_siv_ctmd', 'sales\SalesInvoiceController@div');
Route::post('/sapcf_siv_ctmd', 'sales\SalesInvoiceController@sapcf');
Route::post('/set_canceled_siv_ctmd', 'sales\SalesInvoiceController@set_canceled');
Route::post('/generate_code_siv_ctmd', 'sales\SalesInvoiceController@generate_code');
Route::post('/get_info_siv_ctmd', 'sales\SalesInvoiceController@get_info');
Route::post('/delete_select_siv_ctmd', 'sales\SalesInvoiceController@delete_select_m');
Route::post('/cdf_upload_siv_ctmd', 'sales\SalesInvoiceController@cdf_upload');
Route::post('/cdf_get_siv_ctmd', 'sales\SalesInvoiceController@cdf_get');
Route::post('/cdf_edit_siv_ctmd', 'sales\SalesInvoiceController@cdf_edit');
Route::post('/get_subjob_siv_ctmd', 'sales\SalesInvoiceController@get_subjob');

// SALES RETURN
Route::post('/get_sr_ctmd', 'sales\SalesReturnController@get');
Route::post('/get_detail_sr_ctmd', 'sales\SalesReturnController@get_detail');
Route::post('/get_detailpi_sr_ctmd', 'sales\SalesReturnController@get_detailpi');
Route::post('/get_log_sr_ctmd', 'sales\SalesReturnController@get_log');
Route::post('/get_costcontrol_sr_ctmd', 'sales\SalesReturnController@get_costcontrol');
Route::post('/save_sr_ctmd', 'sales\SalesReturnController@save');
Route::post('/find_sr_ctmd', 'sales\SalesReturnController@find');
Route::post('/delete_sr_ctmd', 'sales\SalesReturnController@delete');
Route::post('/find_last_supplier_sr_ctmd', 'sales\SalesReturnController@find_last_supplier');
Route::post('/send_to_next_sr_ctmd', 'sales\SalesReturnController@send_to_next');
Route::post('/set_pending_sr_ctmd', 'sales\SalesReturnController@set_pending');
Route::post('/snst_sr_ctmd', 'sales\SalesReturnController@snst');
Route::post('/div_sr_ctmd', 'sales\SalesReturnController@div');
Route::post('/sapcf_sr_ctmd', 'sales\SalesReturnController@sapcf');
Route::post('/set_canceled_sr_ctmd', 'sales\SalesReturnController@set_canceled');
Route::post('/generate_code_sr_ctmd', 'sales\SalesReturnController@generate_code');
Route::post('/get_info_sr_ctmd', 'sales\SalesReturnController@get_info');
Route::post('/delete_select_sr_ctmd', 'sales\SalesReturnController@delete_select_m');
Route::post('/cdf_upload_sr_ctmd', 'sales\SalesReturnController@cdf_upload');
Route::post('/cdf_get_sr_ctmd', 'sales\SalesReturnController@cdf_get');
Route::post('/cdf_edit_sr_ctmd', 'sales\SalesReturnController@cdf_edit');

// SALES PAYMENT
Route::post('/get_sp_ctmd', 'sales\SalesPaymentController@get');
Route::post('/get_detail_sp_ctmd', 'sales\SalesPaymentController@get_detail');
Route::post('/get_detailpi_sp_ctmd', 'sales\SalesPaymentController@get_detailpi');
Route::post('/get_log_sp_ctmd', 'sales\SalesPaymentController@get_log');
Route::post('/get_costcontrol_sp_ctmd', 'sales\SalesPaymentController@get_costcontrol');
Route::post('/save_sp_ctmd', 'sales\SalesPaymentController@save');
Route::post('/find_sp_ctmd', 'sales\SalesPaymentController@find');
Route::post('/delete_sp_ctmd', 'sales\SalesPaymentController@delete');
Route::post('/find_last_supplier_sp_ctmd', 'sales\SalesPaymentController@find_last_supplier');
Route::post('/send_to_next_sp_ctmd', 'sales\SalesPaymentController@send_to_next');
Route::post('/set_pending_sp_ctmd', 'sales\SalesPaymentController@set_pending');
Route::post('/snst_sp_ctmd', 'sales\SalesPaymentController@snst');
Route::post('/div_sp_ctmd', 'sales\SalesPaymentController@div');
Route::post('/sapcf_sp_ctmd', 'sales\SalesPaymentController@sapcf');
Route::post('/set_canceled_sp_ctmd', 'sales\SalesPaymentController@set_canceled');
Route::post('/generate_code_sp_ctmd', 'sales\SalesPaymentController@generate_code');
Route::post('/get_info_sp_ctmd', 'sales\SalesPaymentController@get_info');
Route::post('/delete_select_sp_ctmd', 'sales\SalesPaymentController@delete_select_m');
Route::post('/cdf_upload_sp_ctmd', 'sales\SalesPaymentController@cdf_upload');
Route::post('/cdf_get_sp_ctmd', 'sales\SalesPaymentController@cdf_get');
Route::post('/cdf_edit_sp_ctmd', 'sales\SalesPaymentController@cdf_edit');


//RAP
Route::post('/get_rap_ctmd', 'ra\RAPController@get');
Route::post('/get_milestone_and_header_rap_ctmd', 'ra\RAPController@get_milestone_and_header');
Route::post('/get_milestone_rap_ctmd', 'ra\RAPController@get_milestone');
Route::post('/get_header_rap_ctmd', 'ra\RAPController@get_header');
Route::post('/get_detail_rap_ctmd', 'ra\RAPController@get_detail');
Route::post('/get_detailpi_rap_ctmd', 'ra\RAPController@get_detailpi');
Route::post('/get_log_rap_ctmd', 'ra\RAPController@get_log');
Route::post('/get_generatetemplate_rap_ctmd', 'ra\RAPController@get_generatetemplate');
Route::post('/save_rap_ctmd', 'ra\RAPController@save');
Route::post('/save_milestone_rap_ctmd', 'ra\RAPController@save_milestone');
Route::post('/save_header_rap_ctmd', 'ra\RAPController@save_header');
Route::post('/find_rap_ctmd', 'ra\RAPController@find');
Route::post('/delete_rap_ctmd', 'ra\RAPController@delete');
Route::post('/find_last_supplier_rap_ctmd', 'ra\RAPController@find_last_supplier');
Route::post('/send_to_next_rap_ctmd', 'ra\RAPController@send_to_next');
Route::post('/set_pending_rap_ctmd', 'ra\RAPController@set_pending');
Route::post('/snst_rap_ctmd', 'ra\RAPController@snst');
Route::post('/div_rap_ctmd', 'ra\RAPController@div');
Route::post('/sapcf_rap_ctmd', 'ra\RAPController@sapcf');
Route::post('/set_canceled_rap_ctmd', 'ra\RAPController@set_canceled');
Route::post('/generate_code_rap_ctmd', 'ra\RAPController@generate_code');
Route::post('/get_info_rap_ctmd', 'ra\RAPController@get_info');
Route::post('/delete_select_rap_ctmd', 'ra\RAPController@delete_select_m');
Route::post('/cdf_upload_rap_ctmd', 'ra\RAPController@cdf_upload');
Route::post('/cdf_get_rap_ctmd', 'ra\RAPController@cdf_get');
Route::post('/cdf_edit_rap_ctmd', 'ra\RAPController@cdf_edit');
Route::post('/get_header_of_milestone_rap_ctmd', 'ra\RAPController@get_header_of_milestone');
Route::post('/get_select2ajax_header_rap_ctmd', 'ra\RAPController@get_select2ajax_header');
Route::get('/exportexcel/50011102/{urlprint}', 'ra\RAPController@exportexcel');
Route::get('/download_rap_ctmd/document/{id}', 'ra\RAPController@download');


// WORK SCHEDULE
Route::post('/get_ws', 'project\WorkScheduleController@get');
Route::post('/get_milestone_and_header_ws', 'project\WorkScheduleController@get_milestone_and_header');
Route::post('/get_milestone_ws', 'project\WorkScheduleController@get_milestone');
Route::post('/get_header_ws', 'project\WorkScheduleController@get_header');
Route::post('/get_detail_ws', 'project\WorkScheduleController@get_detail');
Route::post('/get_detailpi_ws', 'project\WorkScheduleController@get_detailpi');
Route::post('/get_log_ws', 'project\WorkScheduleController@get_log');
Route::post('/get_generatetemplate_ws', 'project\WorkScheduleController@get_generatetemplate');
Route::post('/save_ws', 'project\WorkScheduleController@save');
Route::post('/save_milestone_ws', 'project\WorkScheduleController@save_milestone');
Route::post('/save_header_ws', 'project\WorkScheduleController@save_header');
Route::post('/find_ws', 'project\WorkScheduleController@find');
Route::post('/delete_ws', 'project\WorkScheduleController@delete');
Route::post('/find_last_supplier_ws', 'project\WorkScheduleController@find_last_supplier');
Route::post('/send_to_next_ws', 'project\WorkScheduleController@send_to_next');
Route::post('/set_pending_ws', 'project\WorkScheduleController@set_pending');
Route::post('/snst_ws', 'project\WorkScheduleController@snst');
Route::post('/div_ws', 'project\WorkScheduleController@div');
Route::post('/sapcf_ws', 'project\WorkScheduleController@sapcf');
Route::post('/set_canceled_ws', 'project\WorkScheduleController@set_canceled');
Route::post('/generate_code_ws', 'project\WorkScheduleController@generate_code');
Route::post('/get_info_ws', 'project\WorkScheduleController@get_info');
Route::post('/delete_select_ws', 'project\WorkScheduleController@delete_select_m');
Route::post('/cdf_upload_ws', 'project\WorkScheduleController@cdf_upload');
Route::post('/cdf_get_ws', 'project\WorkScheduleController@cdf_get');
Route::post('/cdf_edit_ws', 'project\WorkScheduleController@cdf_edit');
Route::post('/get_header_of_milestone_ws', 'project\WorkScheduleController@get_header_of_milestone');
Route::post('/get_select2ajax_header_ws', 'project\WorkScheduleController@get_select2ajax_header');
Route::get('/exportexcel/50011102/{urlprint}', 'project\WorkScheduleController@exportexcel');
Route::get('/download_ws/document/{id}', 'project\WorkScheduleController@download');


//RAB
Route::post('/get_rab_ctmd', 'ra\RABController@get');
Route::post('/get_milestone_and_header_rab_ctmd', 'ra\RABController@get_milestone_and_header');
Route::post('/get_milestone_rab_ctmd', 'ra\RABController@get_milestone');
Route::post('/get_header_rab_ctmd', 'ra\RABController@get_header');
Route::post('/get_detail_rab_ctmd', 'ra\RABController@get_detail');
Route::post('/get_detailpi_rab_ctmd', 'ra\RABController@get_detailpi');
Route::post('/get_log_rab_ctmd', 'ra\RABController@get_log');
Route::post('/get_costcontrol_rab_ctmd', 'ra\RABController@get_costcontrol');
Route::post('/save_rab_ctmd', 'ra\RABController@save');
Route::post('/save_milestone_rab_ctmd', 'ra\RABController@save_milestone');
Route::post('/save_header_rab_ctmd', 'ra\RABController@save_header');
Route::post('/find_rab_ctmd', 'ra\RABController@find');
Route::post('/delete_rab_ctmd', 'ra\RABController@delete');
Route::post('/find_last_supplier_rab_ctmd', 'ra\RABController@find_last_supplier');
Route::post('/send_to_next_rab_ctmd', 'ra\RABController@send_to_next');
Route::post('/set_pending_rab_ctmd', 'ra\RABController@set_pending');
Route::post('/snst_rab_ctmd', 'ra\RABController@snst');
Route::post('/div_rab_ctmd', 'ra\RABController@div');
Route::post('/sapcf_rab_ctmd', 'ra\RABController@sapcf');
Route::post('/set_canceled_rab_ctmd', 'ra\RABController@set_canceled');
Route::post('/generate_code_rab_ctmd', 'ra\RABController@generate_code');
Route::post('/get_info_rab_ctmd', 'ra\RABController@get_info');
Route::post('/delete_select_rab_ctmd', 'ra\RABController@delete_select_m');
Route::post('/cdf_upload_rab_ctmd', 'ra\RABController@cdf_upload');
Route::post('/cdf_get_rab_ctmd', 'ra\RABController@cdf_get');
Route::post('/cdf_edit_rab_ctmd', 'ra\RABController@cdf_edit');
Route::post('/opname_rab_ctmd', 'ra\RABController@opname');
Route::post('/get_header_of_milestone_rab_ctmd', 'ra\RABController@get_header_of_milestone');
Route::post('/get_select2ajax_header_rab_ctmd', 'ra\RABController@get_select2ajax_header');
//RABOpname
Route::post('/get_rabopname_ctmd', 'ra\RABOpnameController@get');
Route::post('/get_milestone_and_header_rabopname_ctmd', 'ra\RABOpnameController@get_milestone_and_header');
Route::post('/get_milestone_rabopname_ctmd', 'ra\RABOpnameController@get_milestone');
Route::post('/get_header_rabopname_ctmd', 'ra\RABOpnameController@get_header');
Route::post('/get_detail_rabopname_ctmd', 'ra\RABOpnameController@get_detail');
Route::post('/get_detailpi_rabopname_ctmd', 'ra\RABOpnameController@get_detailpi');
Route::post('/get_log_rabopname_ctmd', 'ra\RABOpnameController@get_log');
Route::post('/get_costcontrol_rabopname_ctmd', 'ra\RABOpnameController@get_costcontrol');
Route::post('/save_rabopname_ctmd', 'ra\RABOpnameController@save');
Route::post('/save_milestone_rabopname_ctmd', 'ra\RABOpnameController@save_milestone');
Route::post('/save_header_rabopname_ctmd', 'ra\RABOpnameController@save_header');
Route::post('/find_rabopname_ctmd', 'ra\RABOpnameController@find');
Route::post('/delete_rabopname_ctmd', 'ra\RABOpnameController@delete');
Route::post('/find_last_supplier_rabopname_ctmd', 'ra\RABOpnameController@find_last_supplier');
Route::post('/send_to_next_rabopname_ctmd', 'ra\RABOpnameController@send_to_next');
Route::post('/set_pending_rabopname_ctmd', 'ra\RABOpnameController@set_pending');
Route::post('/snst_rabopname_ctmd', 'ra\RABOpnameController@snst');
Route::post('/div_rabopname_ctmd', 'ra\RABOpnameController@div');
Route::post('/sapcf_rabopname_ctmd', 'ra\RABOpnameController@sapcf');
Route::post('/set_canceled_rabopname_ctmd', 'ra\RABOpnameController@set_canceled');
Route::post('/generate_code_rabopname_ctmd', 'ra\RABOpnameController@generate_code');
Route::post('/get_info_rabopname_ctmd', 'ra\RABOpnameController@get_info');
Route::post('/delete_select_rabopname_ctmd', 'ra\RABOpnameController@delete_select_m');
Route::post('/cdf_upload_rabopname_ctmd', 'ra\RABOpnameController@cdf_upload');
Route::post('/cdf_get_rabopname_ctmd', 'ra\RABOpnameController@cdf_get');
Route::post('/cdf_edit_rabopname_ctmd', 'ra\RABOpnameController@cdf_edit');
//RAPOpname
Route::post('/get_rapopname_ctmd', 'ra\RAPOpnameController@get');
Route::post('/get_milestone_and_header_rapopname_ctmd', 'ra\RAPOpnameController@get_milestone_and_header');
Route::post('/get_milestone_rapopname_ctmd', 'ra\RAPOpnameController@get_milestone');
Route::post('/get_header_rapopname_ctmd', 'ra\RAPOpnameController@get_header');
Route::post('/get_detail_rapopname_ctmd', 'ra\RAPOpnameController@get_detail');
Route::post('/get_detailpi_rapopname_ctmd', 'ra\RAPOpnameController@get_detailpi');
Route::post('/get_log_rapopname_ctmd', 'ra\RAPOpnameController@get_log');
Route::post('/get_costcontrol_rapopname_ctmd', 'ra\RAPOpnameController@get_costcontrol');
Route::post('/save_rapopname_ctmd', 'ra\RAPOpnameController@save');
Route::post('/save_milestone_rapopname_ctmd', 'ra\RAPOpnameController@save_milestone');
Route::post('/save_header_rapopname_ctmd', 'ra\RAPOpnameController@save_header');
Route::post('/find_rapopname_ctmd', 'ra\RAPOpnameController@find');
Route::post('/delete_rapopname_ctmd', 'ra\RAPOpnameController@delete');
Route::post('/find_last_supplier_rapopname_ctmd', 'ra\RAPOpnameController@find_last_supplier');
Route::post('/send_to_next_rapopname_ctmd', 'ra\RAPOpnameController@send_to_next');
Route::post('/set_pending_rapopname_ctmd', 'ra\RAPOpnameController@set_pending');
Route::post('/snst_rapopname_ctmd', 'ra\RAPOpnameController@snst');
Route::post('/div_rapopname_ctmd', 'ra\RAPOpnameController@div');
Route::post('/sapcf_rapopname_ctmd', 'ra\RAPOpnameController@sapcf');
Route::post('/set_canceled_rapopname_ctmd', 'ra\RAPOpnameController@set_canceled');
Route::post('/generate_code_rapopname_ctmd', 'ra\RAPOpnameController@generate_code');
Route::post('/get_info_rapopname_ctmd', 'ra\RAPOpnameController@get_info');
Route::post('/delete_select_rapopname_ctmd', 'ra\RAPOpnameController@delete_select_m');
Route::post('/cdf_upload_rapopname_ctmd', 'ra\RAPOpnameController@cdf_upload');
Route::post('/cdf_get_rapopname_ctmd', 'ra\RAPOpnameController@cdf_get');
Route::post('/cdf_edit_rapopname_ctmd', 'ra\RAPOpnameController@cdf_edit');

// CUSTOM MASTER

// INVMINVENTORY
Route::post('/get_invminventory_cm', 'lam_custom_master\InventoryController@get');
Route::post('/save_invminventory_cm', 'lam_custom_master\InventoryController@save');
Route::post('/find_invminventory_cm', 'lam_custom_master\InventoryController@find');
Route::post('/delete_invminventory_cm', 'lam_custom_master\InventoryController@delete');
Route::post('/delete_select_invminventory_cm', 'lam_custom_master\InventoryController@delete_select_m');
Route::post('/generate_code_invminventory_cm', 'lam_custom_master\InventoryController@generate_code');
Route::post('/get_select2ajax_invminventory_cm', 'lam_custom_master\InventoryController@get_select2ajax');

// MCARDGUDANG
Route::post('/get_genmgudang_cm', 'lam_custom_master\GudangController@get');
Route::post('/save_genmgudang_cm', 'lam_custom_master\GudangController@save');
Route::post('/find_genmgudang_cm', 'lam_custom_master\GudangController@find');
Route::post('/delete_genmgudang_cm', 'lam_custom_master\GudangController@delete');
Route::post('/delete_select_genmgudang_cm', 'lam_custom_master\GudangController@delete_select_m');
Route::post('/generate_code_genmgudang_cm', 'lam_custom_master\GudangController@generate_code');
Route::post('/get_select2ajax_genmgudang_cm', 'lam_custom_master\GudangController@get_select2ajax');

// GENMSTRUCTUREORG
Route::post('/get_genmstructureorg_cm', 'lam_custom_master\GenmstructureorgController@get');
Route::post('/save_genmstructureorg_cm', 'lam_custom_master\GenmstructureorgController@save');
Route::post('/find_genmstructureorg_cm', 'lam_custom_master\GenmstructureorgController@find');
Route::post('/delete_genmstructureorg_cm', 'lam_custom_master\GenmstructureorgController@delete');
Route::post('/delete_select_genmstructureorg_cm', 'lam_custom_master\GenmstructureorgController@delete_select_m');
Route::post('/generate_code_genmstructureorg_cm', 'lam_custom_master\GenmstructureorgController@generate_code');


// MCARD
Route::post('/get_mcard_cm', 'lam_custom_master\MCardController@get');
Route::post('/save_mcard_cm', 'lam_custom_master\MCardController@save');
Route::post('/find_mcard_cm', 'lam_custom_master\MCardController@find');
Route::post('/delete_mcard_cm', 'lam_custom_master\MCardController@delete');
Route::post('/delete_select_mcard_cm', 'lam_custom_master\MCardController@delete_select_m');
Route::post('/upload_image_mcard_cm', 'lam_custom_master\MCardController@upload_image');
Route::post('/generate_code_mcard_cm', 'lam_custom_master\MCardController@generate_code');
Route::post('/get_info_mcard_cm', 'lam_custom_master\MCardController@get_info');
Route::post('/get_mcardcontact_mcard_cm', 'lam_custom_master\MCardController@get_mcardcontact');
Route::post('/get_mcardprivilidge_mcard_cm', 'lam_custom_master\MCardController@get_mcardprivilidge');
Route::post('/get_select2ajax_mcard_cm', 'lam_custom_master\MCardController@get_select2ajax');

// USER
Route::post('/get_mcardgroupuser_cm', 'lam_custom_master\MGroupuserController@get');
Route::post('/save_mcardgroupuser_cm', 'lam_custom_master\MGroupuserController@save');
Route::post('/find_mcardgroupuser_cm', 'lam_custom_master\MGroupuserController@find');
Route::post('/delete_mcardgroupuser_cm', 'lam_custom_master\MGroupuserController@delete');
Route::post('/delete_select_mcardgroupuser_cm', 'lam_custom_master\MGroupuserController@delete_select_m');

// USER
Route::post('/get_mcarduser_cm', 'lam_custom_master\MUserController@get');
Route::post('/get_employeechild_mcarduser_cm', 'lam_custom_master\MUserController@get_employeechild');
Route::post('/save_mcarduser_cm', 'lam_custom_master\MUserController@save');
Route::post('/find_mcarduser_cm', 'lam_custom_master\MUserController@find');
Route::post('/delete_mcarduser_cm', 'lam_custom_master\MUserController@delete');
Route::post('/delete_select_mcarduser_cm', 'lam_custom_master\MUserController@delete_select_m');

// EMPLOYEE
Route::post('/get_mcardemployee_cm', 'lam_custom_master\MEmployeeController@get');
Route::post('/save_mcardemployee_cm', 'lam_custom_master\MEmployeeController@save');
Route::post('/find_mcardemployee_cm', 'lam_custom_master\MEmployeeController@find');
Route::post('/delete_mcardemployee_cm', 'lam_custom_master\MEmployeeController@delete');
Route::post('/delete_select_mcardemployee_cm', 'lam_custom_master\MEmployeeController@delete_select_m');

// CUSTOMER
Route::post('/get_mcardcustomer_cm', 'lam_custom_master\MCardCustomerController@get');
Route::post('/get_select2ajax_mcardcustomer_cm', 'lam_custom_master\MCardCustomerController@get_select2ajax');
Route::post('/save_mcardcustomer_cm', 'lam_custom_master\MCardCustomerController@save');
Route::post('/find_mcardcustomer_cm', 'lam_custom_master\MCardCustomerController@find');
Route::post('/delete_mcardcustomer_cm', 'lam_custom_master\MCardCustomerController@delete');
Route::post('/delete_select_mcardcustomer_cm', 'lam_custom_master\MCardCustomerController@delete_select_m');

// SUPPLIER
Route::post('/get_mcardsupplier_cm', 'lam_custom_master\MCardSupplierController@get');
Route::post('/get_select2ajax_mcardsupplier_cm', 'lam_custom_master\MCardSupplierController@get_select2ajax');
Route::post('/save_mcardsupplier_cm', 'lam_custom_master\MCardSupplierController@save');
Route::post('/find_mcardsupplier_cm', 'lam_custom_master\MCardSupplierController@find');
Route::post('/delete_mcardsupplier_cm', 'lam_custom_master\MCardSupplierController@delete');
Route::post('/delete_select_mcardsupplier_cm', 'lam_custom_master\MCardSupplierController@delete_select_m');

// PRIVILEGE
Route::post('/get_mcardprivilege_cm', 'lam_custom_master\MCardPrivilegeController@get');
Route::post('/get_select2ajax_mcardprivilege_cm', 'lam_custom_master\MCardPrivilegeController@get_select2ajax');
Route::post('/save_mcardprivilege_cm', 'lam_custom_master\MCardPrivilegeController@save');
Route::post('/find_mcardprivilege_cm', 'lam_custom_master\MCardPrivilegeController@find');
Route::post('/delete_mcardprivilege_cm', 'lam_custom_master\MCardPrivilegeController@delete');
Route::post('/delete_select_mcardprivilege_cm', 'lam_custom_master\MCardPrivilegeController@delete_select_m');

// USER
Route::post('/get_muser_cm', 'lam_custom_master\MUserController@get');
Route::post('/get_select2ajax_muser_cm', 'lam_custom_master\MUserController@get_select2ajax');
Route::post('/get_employeechild_muser_cm', 'lam_custom_master\MUserController@get_employeechild');
Route::post('/save_muser_cm', 'lam_custom_master\MUserController@save');
Route::post('/find_muser_cm', 'lam_custom_master\MUserController@find');
Route::post('/delete_muser_cm', 'lam_custom_master\MUserController@delete');
Route::post('/delete_select_muser_cm', 'lam_custom_master\MUserController@delete_select_m');

// EMPLOYEE
Route::post('/get_memployee_cm', 'lam_custom_master\MEmployeeController@get');
Route::post('/get_select2ajax_memployee_cm', 'lam_custom_master\MEmployeeController@get_select2ajax');
Route::post('/get_employeechild_memployee_cm', 'lam_custom_master\MEmployeeController@get_employeechild');
Route::post('/save_memployee_cm', 'lam_custom_master\MEmployeeController@save');
Route::post('/find_memployee_cm', 'lam_custom_master\MEmployeeController@find');
Route::post('/delete_memployee_cm', 'lam_custom_master\MEmployeeController@delete');
Route::post('/delete_select_memployee_cm', 'lam_custom_master\MEmployeeController@delete_select_m');

// 3RPARTY
Route::post('/get_mcard3rparty_cm', 'lam_custom_master\M3rpartyController@get');
Route::post('/save_mcard3rparty_cm', 'lam_custom_master\M3rpartyController@save');
Route::post('/find_mcard3rparty_cm', 'lam_custom_master\M3rpartyController@find');
Route::post('/delete_mcard3rparty_cm', 'lam_custom_master\M3rpartyController@delete');
Route::post('/delete_select_mcard3rparty_cm', 'lam_custom_master\M3rpartyController@delete_select_m');



// MTYPECARD
Route::post('/get_mtypecard_cm', 'lam_custom_master\MTypeCardController@get');
Route::post('/save_mtypecard_cm', 'lam_custom_master\MTypeCardController@save');
Route::post('/find_mtypecard_cm', 'lam_custom_master\MTypeCardController@find');
Route::post('/delete_mtypecard_cm', 'lam_custom_master\MTypeCardController@delete');
Route::post('/delete_select_mtypecard_cm', 'lam_custom_master\MTypeCardController@delete_select_m');

// Route::get('/{slug}', 'DashboardController@index')->name('dashboard');
// Route::get('/{id}', 'MasterTabelController@index')->name('mastertael');


// Route::group(['prefix' => '{slug}','middleware'=>
//   ['middleware']], function () {
//     Route::get('{slug}', 'DashboardController@index')->name('dashboard');
//           Route::get('{slug}', 'CustomController@index')->name('custom');
// });
// Route::group(['middleware'=>['systemUser'] ], function(){
//       Route::get('{slug}', 'DashboardController@index')->name('dashboard');
//       Route::get('{slug}', 'CustomController@index')->name('custom');
//       //ETC
//     });

// Route::get('/', [
//     'uses' => 'HomeController@index',
//     'middleware' => 'systemUser'
// ]);
// Route::get('/{slug}', [
//     // 'uses' => 'CustomController@index',
//     'middleware' => 'systemUser'
// ]);

// Route::group(['namespace' => '/'], function($slug)
// {
//   dd($slug);
//   $menu = App\Menu::where('noid',$slug)->first();
//    $page = App\Page::where('noid',$menu->linkidpage)->first();
//     Route::get('{admin/news}', [
//         'uses' => 'NewsController@index'
//     ]);
//
//     Route::get('admin/users', [
//         'uses' => 'UserController@index'
//     ]);
//
// });


// Route::get('/{slug}', 'ThoughtRecordController@edit')->middleware('can:isOwner,thoughtRecord');
// Route::get('/{slug}', function ($slug) {
//     //
// })->middleware('systemUser');
// Route::get('/{slug}', array('as' => 'home.home', 'uses' => 'HomeController@home'));
// Route::get('/{slug?}', ['as' => 'home', 'uses' => 'HomeController@home']);
// Route::get('/{slug?}', function ($slug) {
//   $menu = App\Menu::where('noid',$slug)->first();
//   $page = App\Page::where('noid',$menu->linkidpage)->first();
//   dd('a');
//   return $slug;
// });
// Route::get('/{slug?}', function ($slug) {
//   $menu = App\Menu::where('noid',$slug)->first();
//   $page = App\Page::where('noid',$menu->linkidpage)->first();
//     dd('b');
//   return $slug;
// });

// Route::get('{slug}', function ($slug) {
//   //
//   // dd($slug);
//   $menu = App\Menu::where('noid',$slug)->first();
//      $page = App\Page::where('noid',$menu->linkidpage)->first();
//      // dd($page->idtypepage);
//      if ($page->idtypepage == 9)
//      {
//        $url = route('CustomController@index',['slug' => $slug])->name('custom');
//        return redirect()->route('custom');
//        // return redirect()->action('CustomController@index', ['slug' => $slug]);
//        // Route::get('/'.$slug.' ', 'CustomController@index')->name('custom');
//        //
//        // return redirect(route('custom'));
//
//      }
// })->where('slug', '[0-9]+');

// Route::group([], function()
// {
//   Route::get('/{slug}', function ($slug) {
//     $menu = App\Menu::where('noid',$slug)->first();
//     $page = App\Page::where('noid',$menu->linkidpage)->first();
//     // dd($page->idtypepage);
//     if ($page->idtypepage == 9)
//     {
//       return redirect()->action('CustomController@index', ['slug' => $slug]);
//       // Route::get('/'.$slug.' ', 'CustomController@index')->name('custom');
//       //
//       // return redirect(route('custom'));
//
//     }
//
//   });
//
//
// });



// $pages =
// Cache::remember('pages', 5, function() {
//     return DB::table('cmspage')
//             ->where('idtypepage', $slug)
//
// });



// Route::get('/{slug?}', function ($slug) {
//     $class = false;
//     $action = false;
//     $menu = App\Menu::where('noid',$slug)->first();
//     $page = App\Page::where('noid',$menu->linkidpage)->first();
//     $route = app(\Illuminate\Routing\Route::class);
//     $request = app(\Illuminate\Http\Request::class);
//     $router = app(\Illuminate\Routing\Router::class);
//     $container = app(\Illuminate\Container\Container::class);
//     if ($page->idtypepage == 9) {
//       //custom
//       $class = CustomController::class;
//           $action = 'index/'.$slug;
//               return (new App\Http\Controllers\CustomController($router, $container))->dispatch($route, $request, $class, $action);
//       // return redirect(route('custom'));
//     }else if ($page->idtypepage == 1) {
//       //Halaman System
//       return redirect(route('system'));
//     }else if ($page->idtypepage == 2) {
//       //Dashboard
//       return redirect(route('dashboard'));
//     }else if ($page->idtypepage == 3) {
//       // Halaman Data Master (Tabel Biasa)
//       return redirect(route('MasterTabelController'));
//     }else if ($page->idtypepage == 4) {
//       // Halaman Data Master (Tabel Biasa)
//       return redirect(route('MasterTreelistController'));
//     }else if ($page->idtypepage == 5) {
//       return redirect(route('TransaksiMasterDetailController'));
//     }else if ($page->idtypepage == 6) {
//         return redirect(route('TransaksiMasterDetail6Controller'));
//     }else if ($page->idtypepage == 7) {
//           return redirect(route('ReportingStandardController'));
//     }else if ($page->idtypepage == 8) {
//             return redirect(route('ReportingNonStandardController'));
//     }
//
//     // $user = UserModel::where('slug', $slug)->first();
//     // if ($user) {
//     //     $class = UserController::class;
//     //     $action = 'userProfile';
//     // }
//     //
//     // if (!$class) {
//     //     $article= ArticleModel::where('slug', $slug)->first();
//     //     if ($article) {
//     //         $class = ArticleController::class;
//     //         $action = 'index';
//     //     }
//     // }
//     //
//     // if ($class) {
//     //
//     //     return (new ControllerDispatcher($router, $container))->dispatch($route, $request, $class, $action);
//     // }
//
//     // Some fallback to 404
//     throw new NotFoundHttpException;
// });
// Route::get('/{slug}', 'HomeController@home');
// Route::get('{slug}', function($slug) {
//    // return 'User '.$slug;
//       if(!Session::get('login')){
//           return view('pages.login');
//       }
//     $menu = App\Menu::where('noid',$slug)->first();
//     $page = App\Page::where('noid',$menu->linkidpage)->first();
    // if ($page->idtypepage == 9) {
    //   //custom
    //   return redirect(route('custom'));
    // }else if ($page->idtypepage == 1) {
    //   //Halaman System
    //   return redirect(route('system'));
    // }else if ($page->idtypepage == 2) {
    //   //Dashboard
    //   return redirect(route('dashboard'));
    // }else if ($page->idtypepage == 3) {
    //   // Halaman Data Master (Tabel Biasa)
    //   return redirect(route('MasterTabelController'));
    // }else if ($page->idtypepage == 4) {
    //   // Halaman Data Master (Tabel Biasa)
    //   return redirect(route('MasterTreelistController'));
    // }else if ($page->idtypepage == 5) {
    //   return redirect(route('TransaksiMasterDetailController'));
    // }else if ($page->idtypepage == 6) {
    //     return redirect(route('TransaksiMasterDetail6Controller'));
    // }else if ($page->idtypepage == 7) {
    //       return redirect(route('ReportingStandardController'));
    // }else if ($page->idtypepage == 8) {
    //         return redirect(route('ReportingNonStandardController'));
    // }
//     return abort(404);
// });

// Route::get('/{slug}', 'CustomController@index')->name('custom');
// Route::get('/{slug}', 'SystemController@index')->name('system');
// Route::patch('/{slug}',[
//   'as' => 'custom.index',
//   'uses' => 'CustomController@index'
// ]);
// Route::get('/{slug}', 'HomeController@home')->name('home');

// Route::get('/{slug}', 'DashboardController@index');
// Route::get('/{slug}', 'MasterTabelController@index');
// Route::get('/{slug}', 'MasterTreelistController@index');
// Route::get('/{slug}', 'TransaksiMasterDetailController@index');
// Route::get('/{slug}', 'TransaksiMasterDetail6Controller@index');
// Route::get('/{slug}', 'ReportingStandardController@index');
// Route::get('/{slug}', 'ReportingNonStandardController@index');







// Demo routes
Route::get('/datatables', 'PagesController@datatables');
Route::get('/ktdatatables', 'PagesController@ktDatatables');
Route::get('/select2', 'PagesController@select2');
Route::get('/icons/custom-icons', 'PagesController@customIcons');
Route::get('/icons/flaticon', 'PagesController@flaticon');
Route::get('/icons/fontawesome', 'PagesController@fontawesome');
Route::get('/icons/lineawesome', 'PagesController@lineawesome');
Route::get('/icons/socicons', 'PagesController@socicons');
Route::get('/icons/svg', 'PagesController@svg');
Route::get('/pages/error', 'PagesController@error');


// Quick search dummy route to display html elements in search dropdown (header search)
Route::get('/quick-search', 'PagesController@quickSearch')->name('quick-search');

// Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');




/////////MOBILE
Route::get('/api/mobile/getcsrftoken', 'lam_mobile\LoginController@getCsrfToken');
Route::post('/api/mobile/login', 'lam_mobile\LoginController@login');
Route::get('/api/mobile/getnotification', 'lam_mobile\NotificationController@get');
Route::post('/api/mobile/savetask', 'lam_mobile\TaskController@save');
Route::get('/api/mobile/readnotification/{noid}', 'lam_mobile\NotificationController@read');


/////////GUEST
Route::get('/applicant', 'frontend\ApplicantController@index');
Route::post('/applicant/save', 'frontend\ApplicantController@save')->name('applicant.save');