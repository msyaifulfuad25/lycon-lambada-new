<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWidgetgridView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement($this->createView());
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement($this->dropView());
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    private function createView(): string
    {
        return <<<SQL
            CREATE VIEW view_widget_grid_fiedl_data AS
                SELECT
                  *
                FROM cmswidgetgridfield
            SQL;
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    private function dropView(): string
    {
      return <<<SQL
            DROP VIEW IF EXISTS 'view_widget_grid_fiedl_data';
            SQL;
    }
}
