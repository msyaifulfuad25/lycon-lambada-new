/*
 Navicat Premium Data Transfer

 Source Server         : LYCON Lambada New
 Source Server Type    : MySQL
 Source Server Version : 50733
 Source Host           : 101.50.2.69:3306
 Source Schema         : lambada_data

 Target Server Type    : MySQL
 Target Server Version : 50733
 File Encoding         : 65001

 Date: 06/04/2021 12:18:20
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for invminventory
-- ----------------------------
DROP TABLE IF EXISTS `invminventory`;
CREATE TABLE `invminventory` (
  `noid` bigint(11) NOT NULL COMMENT 'No. Index Table',
  `kode` varchar(30) DEFAULT NULL COMMENT 'Data Kode',
  `barcode` varchar(20) DEFAULT NULL COMMENT 'Data Barcode',
  `barcode2d` varchar(250) DEFAULT NULL COMMENT 'Data Barcode 2D',
  `nama` varchar(100) DEFAULT NULL COMMENT 'Data Nama',
  `namaalias` varchar(100) DEFAULT NULL COMMENT 'Data Nama Alias',
  `keterangan` text COMMENT 'Keterangan Type',
  `isactive` smallint(1) DEFAULT '2' COMMENT 'Status Active Data. Lookup Data => mtypestatus',
  `issystem` tinyint(1) DEFAULT '0' COMMENT 'Milik Sistem. 1=> TIDK BOLEH DI HAPUS',
  `idinvmgroup` int(11) DEFAULT NULL,
  `idinvmkategori` int(11) DEFAULT NULL,
  `idsatuan` int(11) DEFAULT NULL,
  `konversi` double DEFAULT NULL,
  `hpp` double DEFAULT '0',
  `salesprice` double DEFAULT '0',
  `purchaseprice` double DEFAULT '0',
  `istaxed` tinyint(1) DEFAULT '0',
  `idtax` int(11) DEFAULT '0',
  `taxprocent` double DEFAULT NULL,
  `nourut` int(11) DEFAULT '0' COMMENT 'No Urut dalam View, Default Sort',
  `classicon` varchar(20) DEFAULT 'fa fa-file-o' COMMENT 'Class Icon',
  `classcolor` enum('blue','blue-hoki','blue-steel','blue-madison','blue-chambray','blue-ebonyclay','green','green-meadow','green-seagreen','green-turquoise','green-haze','green-jungle','red','red-pink','red-sunglo','red-intense','red-thunderbird','red-flamingo','yellow','yellow-gold','yellow-casablanca','yellow-crusta','yellow-lemon','yellow-saffron','purple','purple-plum','purple-medium','purple-studio','purple-wisteria','purple-seance','grey','grey-cascade','grey-silver','grey-steel','grey-cararra','grey-gallery') DEFAULT 'blue',
  `idcreate` int(11) DEFAULT '0' COMMENT 'Actor yang membuat pertama kali',
  `docreate` datetime DEFAULT NULL COMMENT 'Waktu Create Data',
  `idupdate` int(11) DEFAULT '0' COMMENT 'Actor yang melakukan perubahan terakhir',
  `lastupdate` datetime DEFAULT NULL COMMENT 'Waktu terakhir perubahan',
  PRIMARY KEY (`noid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of invminventory
-- ----------------------------
BEGIN;
INSERT INTO `invminventory` VALUES (0, 'UNSPECIFIED', NULL, NULL, 'UNSPECIFIED', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 10000, 0, 0, NULL, 6, 'fa fa-file-o', '', 1, '2015-06-13 09:16:49', 1, '2015-06-13 09:16:49');
INSERT INTO `invminventory` VALUES (1, 'INV01', NULL, NULL, 'INVENTOR 01', NULL, NULL, 2, 1, NULL, NULL, NULL, NULL, 0, 0, 20000, 0, 0, NULL, 1, 'fa fa-file-o', '', 1, '2015-06-13 09:16:49', 1, '2015-06-13 09:16:49');
INSERT INTO `invminventory` VALUES (2, 'INV02', NULL, NULL, 'INVENTOR 02', NULL, NULL, 2, 1, NULL, NULL, NULL, NULL, 0, 0, 30000, 0, 0, NULL, 2, 'fa fa-file-o', '', 1, '2015-06-13 09:16:49', 1, '2015-06-13 09:16:49');
INSERT INTO `invminventory` VALUES (3, 'INV03', NULL, NULL, 'INVENTOR 03', NULL, NULL, 2, 1, NULL, NULL, NULL, NULL, 0, 0, 40000, 0, 0, NULL, 3, 'fa fa-file-o', '', 1, '2015-06-13 09:16:49', 1, '2015-06-13 09:16:49');
INSERT INTO `invminventory` VALUES (4, 'INV04', NULL, NULL, 'INVENTOR 04', NULL, NULL, 2, 0, NULL, NULL, NULL, NULL, 0, 0, 50000, 0, 0, NULL, 4, 'fa fa-file-o', '', 1, '2015-06-13 09:16:49', 1, '2015-06-13 09:16:49');
INSERT INTO `invminventory` VALUES (5, 'INV05', NULL, NULL, 'INVENTOR 05', NULL, NULL, 2, 0, NULL, NULL, NULL, NULL, 0, 0, 60000, 0, 0, NULL, 5, 'fa fa-file-o', '', 1, '2015-06-13 09:16:49', 1, '2015-06-13 09:16:49');
INSERT INTO `invminventory` VALUES (6, 'INV06', 'b', 'b', 'INVENTOR 06', 'na', 'k', 0, 1, 1, 1, 1, 0, 0, 0, 70000, 0, 1, 0, 5, 'fa fa-file-o', 'blue', 0, NULL, 0, NULL);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
