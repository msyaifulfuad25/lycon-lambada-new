$(document).ready(function() {
  var values3 = $("#tabel_baru").val();
  var table = '<table id="example" class="table table-bordered table-hover hover" style="width:100%;background-color:#90CAF9 !important"><thead>';
  var divform = '<div class="form-group">';
  var tablenya = '';
  var groupbtn = '';

  $.ajax({
        type: "POST",
        header:{
           'X-CSRF-TOKEN':$('#token').val()
         },
        url: "/ambildata",
        dataType: "json",
        data:{
          "_token":$('#token').val(),
          "table": values3
        },
        success: function (response) {
          $.each(response.buttonfilter, function(key, value) {
            groupbtn += '<div class="btn-group mr-1" role="group" aria-label="All">';
            groupbtn += '<a href="#"  class="btn btn-outline-secondary btn-sm"  onclick="caridata("All")"   style="background-color:#BBDEFB !important" >ALL </a>';
            groupbtn += '</div>';

            $.each(value, function(key2, value2) {
              groupbtn += '<div class="btn-group mr-1" role="group" aria-label="'+value2.fieldname+'">';
              groupbtn += value2.taga;
              groupbtn += '</div>';
            });

          });
          $('#filterlain').append(groupbtn);
          var isicol = '';
          var lebar = '';
          table += response.header_atas;
          table += '<tr>';
          $.each(response.columns, function(key, value) {
           isicol = response.colheaderalign[key];
           lebar = response.width[key];
           table += "<th width='"+lebar+"' style='text-align:"+isicol+"'  >"+value+"</th>";
         });
         table += "</tr>";
         table += "</thead>";
         table += response.footter;
        table += '<tbody>';
        table += '</tbody></table>';

        // console.log(tabel);return false;
        $('#datad').append(table);
        tablenya = $('#example').DataTable({
          "processing": false,
          "serverSide": false,
           "scrollX": true,
           "searching": true,
            // "pagingType": "input",
            // "bPaginate": false,
           // "pagingType" : "input",
            // "stateSave" : true,
           "order": [],
      "dom": '<"top dtfiler"pli<"dtfilter2_search"f><"clear">>rt',

     oLanguage:{
       "sInfo": "<span class='seperator'>|</span> Total _TOTAL_ records",
       "sLengthMenu": "<span class='seperator'>|</span> Rec/Page _MENU_",
       "sInfoFiltered": "",
       "sInfoEmpty": "| No records found to show",
       "sEmptyTable": "No data available in table",
       "sZeroRecords": "No matching records found",
       "sSearch": "<a onclick='ambilhide()'  class='fa fa-search dtfilter_search_btn' ></a>",
    //    "oPaginate": {
    //     sPage: "Page ",
    //     sPageOf: "of ",
    //     sNext: '<i class="fa fa-angle-right"></i>',
    //     sPrevious: '<i class="fa fa-angle-left"></i>',
    //     sLast: '',
    //     sFirst: '',
    // }
       "oPaginate": {
         "sPrevious": "<i class='fa fa-angle-left'></i>",
         "sNext": "<i class='fa fa-angle-right'></i> ",
         "sPage": "Page",
         "sPageOf": "of"
       }
     },
     bAutoWidth:false,
     bServerSide:true,
     //bDeferRender:false,
     bSortCellsTop:true,
     bFilter:true,
          'aaData' : response.data
        });
        $('#example_filter input').unbind();
   $('#example_filter input').bind('keyup', function (e) {
       if (e.keyCode == 13) {
           tablenya.search(this.value).draw();
       }
   });


      },
      });

      $.ajax({
            type: "POST",
            header:{
               'X-CSRF-TOKEN':$('#token').val()
             },
            url: "/ambildata2",
              dataType: "json",
            data:{
              "_token":$('#token').val(),
              "table": values3
            },
            success: function (response) {
              divform += '<input type="hidden" id="noid" name="noid" value="">';
              $.each(response.field, function(key, value) {
               divform += "<label>"+value.label+"</label>";
               divform += value.field;
             });
             divform += '</div>';
              $('#formdepartement').html(divform);

            },
          });

  $('#example').on( 'order.dt',  function () {

    let order = tablenya.order();
    console.log("Ordered column " + order[0][0] + ", in direction " + order[0][1]);
  } );

$('#formdepartement').on('keyup keypress', function(e) {
var keyCode = e.keyCode || e.which;
if (keyCode === 13) {
e.preventDefault();
return false;
}
});

$("#from-departement").hide();
$("#tool-kedua").hide();
$("#tool-ketiga").hide();
$("#divsucces").hide();
$('.datepicker').datepicker({
format: 'd-m-yyyy'
// startDate: '-3d'
});
$('.datepicker2').datepicker({
format: 'd-m-yyyy'
// startDate: '-3d'
});

$(".first.paginate_button, .last.paginate_button").hide();

});
