function awal(id) {
  $.each(id, function (key, val) {
       $.ajax({
             type: "POST",
             header:{
                'X-CSRF-TOKEN':$('#token').val()
              },
             url: "/ambilchart",
             dataType: "json",
             data:{
               "_token":$('#token').val(),
               "id_cart": val
             },
             success: function (response) {
               if (response.defaultchart == 'column') {
                 chartBatang(response,val);
               }else if (response.defaultchart == 'pie') {
                   chartPie(response,val);
               }else{
                   chartBatang(response,val);
               }

             },
           });
   });

}



$(document).ready(function() {
  var values = $("input[name='panel[]']")
                .map(function(){return $(this).val();}).get();
                awal(values);

                var values2 = $("#slider").val();
                slider(values2);
                var html = '';

                var values3 = $("#tabel1").val();
                tabel(values3);


                $('#example').on( 'order.dt',  function () {

                  let order = tablenya.order();
                  console.log("Ordered column " + order[0][0] + ", in direction " + order[0][1]);
                } );

              $('#formdepartement').on('keyup keypress', function(e) {
              var keyCode = e.keyCode || e.which;
              if (keyCode === 13) {
              e.preventDefault();
              return false;
              }
              });

              $("#from-departement").hide();
              $("#tool-kedua").hide();
              $("#tool-ketiga").hide();
              $("#divsucces").hide();
              $('.datepicker').datepicker({
              format: 'd-m-yyyy'
              // startDate: '-3d'
              });
              $('.datepicker2').datepicker({
              format: 'd-m-yyyy'
              // startDate: '-3d'
              });

              $(".first.paginate_button, .last.paginate_button").hide();
});


function ambilhide() {
  $('#example_filter label input').toggleClass('clicked');
}

function tabel(id) {
  var table = '<table id="example" class="table table-bordered table-hover hover" style="width:100%;background-color:#90CAF9 !important"><thead>';
  var divform = '<div class="form-group">';
  var tablenya = '';
  var groupbtn = '';

  $.ajax({
        type: "POST",
        header:{
           'X-CSRF-TOKEN':$('#token').val()
         },
        url: "/ambildata",
        dataType: "json",
        data:{
          "_token":$('#token').val(),
          "table": id
        },
        success: function (response) {
          $.each(response.buttonfilter, function(key, value) {
            groupbtn += '<div class="btn-group mr-1" role="group" aria-label="All">';
            groupbtn += '<a href="#"  class="btn btn-outline-secondary btn-sm"  onclick="caridata("All")"   style="background-color:#BBDEFB !important" >ALL </a>';
            groupbtn += '</div>';

            $.each(value, function(key2, value2) {
              groupbtn += '<div class="btn-group mr-1" role="group" aria-label="'+value2.fieldname+'">';
              groupbtn += value2.taga;
              groupbtn += '</div>';
            });

          });
          $('#filterlain').append(groupbtn);
          var isicol = '';
          var lebar = '';
          table += response.header_atas;
          table += '<tr>';
          $.each(response.columns, function(key, value) {
           isicol = response.colheaderalign[key];
           lebar = response.width[key];
           table += "<th width='"+lebar+"' style='text-align:"+isicol+"'  >"+value+"</th>";
         });
         table += "</tr>";
         table += "</thead>";
         table += response.footter;
        table += '<tbody>';
        table += '</tbody></table>';

        // console.log(tabel);return false;
        $('#datad').append(table);
        tablenya = $('#example').DataTable({
          "processing": false,
          "serverSide": false,
           "scrollX": true,
           "searching": true,
            // "pagingType": "input",
            // "bPaginate": false,
           // "pagingType" : "input",
            // "stateSave" : true,
           "order": [],
      "dom": '<"top dtfiler"pli<"dtfilter2_search"f><"clear">>rt',

     oLanguage:{
       "sInfo": "<span class='seperator'>|</span> Total _TOTAL_ records",
       "sLengthMenu": "<span class='seperator'>|</span> Rec/Page _MENU_",
       "sInfoFiltered": "",
       "sInfoEmpty": "| No records found to show",
       "sEmptyTable": "No data available in table",
       "sZeroRecords": "No matching records found",
       "sSearch": "<a onclick='ambilhide()'  class='fa fa-search dtfilter_search_btn' ></a>",
    //    "oPaginate": {
    //     sPage: "Page ",
    //     sPageOf: "of ",
    //     sNext: '<i class="fa fa-angle-right"></i>',
    //     sPrevious: '<i class="fa fa-angle-left"></i>',
    //     sLast: '',
    //     sFirst: '',
    // }
       "oPaginate": {
         "sPrevious": "<i class='fa fa-angle-left'></i>",
         "sNext": "<i class='fa fa-angle-right'></i> ",
         "sPage": "Page",
         "sPageOf": "of"
       }
     },
     bAutoWidth:false,
     bServerSide:true,
     //bDeferRender:false,
     bSortCellsTop:true,
     bFilter:true,
          'aaData' : response.data
        });
        $('#example_filter input').unbind();
   $('#example_filter input').bind('keyup', function (e) {
       if (e.keyCode == 13) {
           tablenya.search(this.value).draw();
       }
   });


      },
      });

      $.ajax({
            type: "POST",
            header:{
               'X-CSRF-TOKEN':$('#token').val()
             },
            url: "/ambildata2",
              dataType: "json",
            data:{
              "_token":$('#token').val(),
              "table": id
            },
            success: function (response) {
              divform += '<input type="hidden" id="noid" name="noid" value="">';
              $.each(response.field, function(key, value) {
               divform += "<label>"+value.label+"</label>";
               divform += value.field;
             });
             divform += '</div>';
              $('#formdepartement').html(divform);

            },
          });
}

function caridata(id) {
  var tabelnya = $("#tabel1").val();
  $.ajax({
        type: "POST",
        header:{
           'X-CSRF-TOKEN':$('#token').val()
         },
        url: "/filtertabel",
        dataType: "json",
        data:{
          "_token":$('#token').val(),
          "id": id,
          "tabel": tabelnya
        },
        success: function (response) {
          console.log(response);

        },
      });
}

function slider(id) {
  var owl = $('.owl-carousel').owlCarousel({
      loop:true,
      smartSpeed: 100,
      autoplay: true,
      autoplaySpeed: 100,
      mouseDrag: false,
      margin:10,
      animateIn: 'slideInUp',
      animateOut: 'fadeOut',
      nav:false,
      responsive:{
          0:{
              items:1
          },
          600:{
              items:1
          },
          1000:{
              items:1
          }
      }
  });
  $.ajax({
        type: "POST",
        header:{
           'X-CSRF-TOKEN':$('#token').val()
         },
        url: "/ambilSlide",
        dataType: "json",
        data:{
          "_token":$('#token').val(),
          "id_slider": id
        },
        success: function (response) {
          // console.log(response.hasil);
          var base_url = '{{ url('/') }}';
          if(isNaN(base_url)) {
            base_url = '';
          }
var name1 = '';
var name2 = '';

        $.each(response.hasil, function(i, item) {
          // console.log(item.mainimage);
          name1 = 'public/'+item.mainimage;

          console.log(name1);
        	var random_num =  Math.floor(Math.random()*60);
			    owl.trigger('add.owl.carousel', [jQuery('<div class="notification-message"> <img src="'+base_url+name1+'" class="user-image" alt=""> <div class="user-name">'+ item.newstitle+' <span class="lighter">from '+item.idnews+'</span></div> <div class="bought-details">Bought This <br>'+random_num+' minutes ago</div> </div>')]);
			});
			owl.trigger('refresh.owl.carousel');

        },
      });
}

function chartBatang(response,id_cart) {
  var dataatas = response.hasil;
      var dataProvideratas = [];// this variable you have to pass in dataProvider inside chart
      for(var key in dataatas) {
        dataProvideratas.push({
          category: dataatas[key]['categories'],
          visits: dataatas[key]['jumlahdata'],
        });
      }
      var chart = AmCharts.makeChart("kt_amcharts_"+id_cart, {
        "rtl": KTUtil.isRTL(),
        "type": "serial",
         "hideCredits":true,
        "theme": "light",
        "dataProvider": dataProvideratas,
        "valueAxes": [{
          "gridColor": "#FFFFFF",
          "gridAlpha": 0.2,
          "dashLength": 0
        }],
        "series": [{
            "type": "LineSeries",
            "stroke": "#ff0000",
            "strokeWidth": 3
          }],
  "valueAxes": [ {
  "position": "left",
  "title": "Jumlah Data"
  }, {
  "position": "bottom",
  "title": response.widget.widget_widgetcaption
  } ],
  "legend": {
  "useGraphSettings": true
  },
  "titles": [
  {
   "size": 15,
   "text":response.widget.widget_widgetcaption
  },
  {
      "text": response.widget.widget_widgetsubcaption,
      "bold": false
  }
  ],
        "gridAboveGraphs": true,
        "startDuration": 1,
        "graphs": [{
          "balloonText": "[[category]]: <b>[[value]]</b>",
          "fillAlphas": 0.8,
          "lineAlpha": 0.2,
          "type": "column",
          "valueField": "visits",
          "title": response.widget.widget_widgetcaption
        }],
        "chartCursor": {
          "categoryBalloonEnabled": false,
          "cursorAlpha": 0,
          "zoomable": false
        },
        "categoryField": "category",
        "categoryAxis": {
          "gridPosition": "start",
        },
        "export": {
          "enabled": true
        }
      });
}
function chartPie(response,id_cart) {
    var data = response.hasil;
  var dataProvider = [];// this variable you have to pass in dataProvider inside chart
  for(var key in data) {
    dataProvider.push({
      country: data[key]['messagestatus'],
      litres: data[key]['jumlahdata'],
    });
  }

var chart = AmCharts.makeChart("kt_amcharts_"+id_cart,{
"type"    : "pie",
"hideCredits"    : true,
"theme"    : "light",
"addClassNames": true,
"titles": [
{
"size": 15,
"text": response.widget.widget_widgetcaption
},
{
"text": response.widget.widget_widgetsubcaption,
"bold": false
}
],
// "pullOutDuration": 0,
// "pullOutRadius": 0,

"labelsEnabled": false,
"titleField"  : "country",
"valueField"  : "litres",
"dataProvider"  : dataProvider,
"numberFormatter": {
  "precision": -1,
  "decimalSeparator": ",",
  "thousandsSeparator": ""
},
"legend": {
     "align": "center",
     "position": "bottom",
     "marginRight": 0,
     "labelText": "[[title]]",
     "valueText": "",
     "valueWidth": 50,
     "textClickEnabled": true
 },
"export": {
"enabled": true
}

});
}


$("#savedata").click(function(e){
e.preventDefault();
var kode = $("#kode").val();
var nama = $("#nama").val();
var namatable =$('#namatable').val();
var formdata = $("#formdepartement").serialize();
$.ajax({
      type: "POST",
      header:{
         'X-CSRF-TOKEN':$('#token').val()
       },
      url: "/savedata",
      dataType: "json",
      data:{
         "_token":$('#token').val(),
        "data" :{
            'data' : formdata,
            'tabel': namatable
        }
      },
      success:function(result){
        if (result.status == 'add') {
                              $("#notiftext").text('Data  Berhasil di Tambah');
        }else{
                                                  $("#notiftext").text('Data  Berhasil di Edit');
        }

        document.getElementById("formdepartement").reset();
        awal();
        $('#divsucces').show(0).delay(5000).hide(0);
        var x = document.getElementById("from-departement");
        var x2 = document.getElementById("tabledepartemen");
        var y = document.getElementById("tool-kedua");
        var y2 = document.getElementById("tool-pertama");
          x.style.display = "none";
          x2.style.display = "block";
          y.style.display = "none";
          y2.style.display = "block";

}});
});

function AddData() {
  var x = document.getElementById("from-departement");
  var x2 = document.getElementById("tabledepartemen");
  var y = document.getElementById("tool-kedua");
  var y2 = document.getElementById("tool-pertama");
    if (x.style.display === "none") {
      x.style.display = "block";
      x2.style.display = "none";
      y.style.display = "block";
      y2.style.display = "none";
    } else {
      x.style.display = "none";
      x2.style.display = "block";
    }
}

function CancelData() {
  var x = document.getElementById("from-departement");
  var x2 = document.getElementById("tabledepartemen");
  var y = document.getElementById("tool-kedua");
  var y2 = document.getElementById("tool-pertama");
  var y3 = document.getElementById("tool-ketiga");
    x.style.display = "none";
    x2.style.display = "block";
    y.style.display = "none";
    y2.style.display = "block";
    y3.style.display = "none";
                      document.getElementById("formdepartement").reset();
}

function viewDepartemen(id) {
  var x = document.getElementById("from-departement");
  var x2 = document.getElementById("tabledepartemen");
  var y = document.getElementById("tool-kedua");
  var y2 = document.getElementById("tool-pertama");
  var y3 = document.getElementById("tool-ketiga");
    if (x.style.display === "none") {
      x.style.display = "block";
      x2.style.display = "none";
      y.style.display = "none";
      y2.style.display = "none";
      y3.style.display = "block";
    } else {
      x.style.display = "none";
      x2.style.display = "block";
    }
    $.ajax({
        url: '/editdepartemen',
        type: "POST",
        dataType: 'json',
        header:{
           'X-CSRF-TOKEN':$('#token').val()
         },
        data: {
          "id" : id,
          "namatable" : $("#namatable").val(),
          "_token": $('#token').val(),
        },
        success: function (data) {
          $.each(data, function(key, value) {
            $.each(value, function(key2, value2) {
              // console.log(key2);
            $("#"+key2).val(value2);
          });
         });
        },

    });
}
function editDepartemen(id) {
  var x = document.getElementById("from-departement");
  var x2 = document.getElementById("tabledepartemen");
  var y = document.getElementById("tool-kedua");
  var y2 = document.getElementById("tool-pertama");
  if (x.style.display === "none") {
    x.style.display = "block";
    x2.style.display = "none";
    y.style.display = "block";
    y2.style.display = "none";
  } else {
    x.style.display = "none";
    x2.style.display = "block";
  }
  $.ajax({
      url: '/editdepartemen',
      type: "POST",
      dataType: 'json',
      header:{
         'X-CSRF-TOKEN':$('#token').val()
       },
      data: {
        "id" : id,
        "namatable" : $("#namatable").val(),
        "_token": $('#token').val()
      },
      success: function (response) {
        $.each(response, function(key, value) {
          $.each(value, function(key2, value2) {
          $("#"+key2).val(value2);
        });
       });
      }
  });

}

function hapusDepartemen(id) {
  if (confirm('Are you sure you want to delete this?')) {
      $.ajax({
          url: '/hapusdepartemen',
          type: "POST",
          header:{
             'X-CSRF-TOKEN':$('#token').val()
           },
          data: {
            "id" : id,
            "namatable" :  $("#namatable").val(),
            "_token": $('#token').val()
          },
          success: function () {
                  $("#notiftext").text('Data  Berhasil di hapus');
                awal();
                  $('#divsucces').show(0).delay(5000).hide(0);
                  var x = document.getElementById("from-departement");
                  var x2 = document.getElementById("tabledepartemen");
                  var y = document.getElementById("tool-kedua");
                  var y2 = document.getElementById("tool-pertama");
                    x.style.display = "none";
                    x2.style.display = "block";
                    y.style.display = "none";
                    y2.style.display = "block";
          }
      });
  }
  }
