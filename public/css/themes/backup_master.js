.table-bordered {
  border: 1px solid #EBEDF3;
}
.table-bordered th,
.table-bordered td {
  border: 1px solid #EBEDF3;
}
.table-bordered thead th,
.table-bordered thead td {
  border-bottom-width: 2px;
}

.table-borderless th,
.table-borderless td,
.table-borderless thead th,
.table-borderless tbody + tbody {
  /* border: 0; */
}

.table-striped tbody tr:nth-of-type(odd) {
  background-color: #ffffff;
}

.table-hover tbody tr:hover {
  color: #3F4254;
  background-color: red;
}

.table-primary,
.table-primary > th,
.table-primary > td {
  background-color: #c7e2ff;
}
.table-primary th,
.table-primary td,
.table-primary thead th,
.table-primary tbody + tbody {
  border-color: #96caff;
}

.table-hover .table-primary:hover {
  background-color: #aed5ff;
}
.table-hover .table-primary:hover > td,
.table-hover .table-primary:hover > th {
  background-color: #aed5ff;
}

.table-secondary,
.table-secondary > th,
.table-secondary > td {
  background-color: #f7f8fb;
}
.table-secondary th,
.table-secondary td,
.table-secondary thead th,
.table-secondary tbody + tbody {
  border-color: #f1f2f7;
}

.table-hover .table-secondary:hover {
  background-color: #e6e9f3;
}
.table-hover .table-secondary:hover > td,
.table-hover .table-secondary:hover > th {
  background-color: #e6e9f3;
}

.table-success,
.table-success > th,
.table-success > td {
  background-color: #bfefed;
}
.table-success th,
.table-success td,
.table-success thead th,
.table-success tbody + tbody {
  border-color: #88e1dd;
}

.table-hover .table-success:hover {
  background-color: #abeae7;
}
.table-hover .table-success:hover > td,
.table-hover .table-success:hover > th {
  background-color: #abeae7;
}

.table-info,
.table-info > th,
.table-info > td {
  background-color: #decefe;
}
.table-info th,
.table-info td,
.table-info thead th,
.table-info tbody + tbody {
  border-color: #c2a4fd;
}

.table-hover .table-info:hover {
  background-color: #cdb5fd;
}
.table-hover .table-info:hover > td,
.table-hover .table-info:hover > th {
  background-color: #cdb5fd;
}

.table-warning,
.table-warning > th,
.table-warning > td {
  background-color: #ffe7b8;
}
.table-warning th,
.table-warning td,
.table-warning thead th,
.table-warning tbody + tbody {
  border-color: #ffd27a;
}

.table-hover .table-warning:hover {
  background-color: #ffde9f;
}
.table-hover .table-warning:hover > td,
.table-hover .table-warning:hover > th {
  background-color: #ffde9f;
}

.table-danger,
.table-danger > th,
.table-danger > td {
  background-color: #fccdd2;
}
.table-danger th,
.table-danger td,
.table-danger thead th,
.table-danger tbody + tbody {
  border-color: #faa3ac;
}

.table-hover .table-danger:hover {
  background-color: #fbb5bc;
}
.table-hover .table-danger:hover > td,
.table-hover .table-danger:hover > th {
  background-color: #fbb5bc;
}

.table-light,
.table-light > th,
.table-light > td {
  background-color: #fcfcfd;
}
.table-light th,
.table-light td,
.table-light thead th,
.table-light tbody + tbody {
  border-color: #f9fafc;
}

.table-hover .table-light:hover {
  background-color: #ededf3;
}
.table-hover .table-light:hover > td,
.table-hover .table-light:hover > th {
  background-color: #ededf3;
}

.table-dark,
.table-dark > th,
.table-dark > td {
  background-color: #bebfc6;
}
.table-dark th,
.table-dark td,
.table-dark thead th,
.table-dark tbody + tbody {
  border-color: #878994;
}

.table-hover .table-dark:hover {
  background-color: #b0b2ba;
}
.table-hover .table-dark:hover > td,
.table-hover .table-dark:hover > th {
  background-color: #b0b2ba;
}

.table-white,
.table-white > th,
.table-white > td {
  background-color: white;
}
.table-white th,
.table-white td,
.table-white thead th,
.table-white tbody + tbody {
  border-color: white;
}

.table-hover .table-white:hover {
  background-color: #f2f2f2;
}
.table-hover .table-white:hover > td,
.table-hover .table-white:hover > th {
  background-color: #f2f2f2;
}

.table-active,
.table-active > th,
.table-active > td {
  background-color: #E4E6EF;
}

.table-hover .table-active:hover {
  background-color: #d4d7e6;
}
.table-hover .table-active:hover > td,
.table-hover .table-active:hover > th {
  background-color: #d4d7e6;
}

.table .thead-dark th {
  color: #ffffff;
  background-color: #181C32;
  border-color: #242a4c;
}
.table .thead-light th {
  color: #3F4254;
  background-color: #F3F6F9;
  border-color: #EBEDF3;
}

.table-dark {
  color: #ffffff;
  background-color: #181C32;
}
.table-dark th,
.table-dark td,
.table-dark thead th {
  border-color: #242a4c;
}
.table-dark.table-bordered {
  border: 0;
}
.table-dark.table-striped tbody tr:nth-of-type(odd) {
  background-color: rgba(255, 255, 255, 0.05);
}
.table-dark.table-hover tbody tr:hover {
  color: #ffffff;
  background-color: rgba(255, 255, 255, 0.075);
}

@media (max-width: 575.98px) {
  .table-responsive-sm {
    display: block;
    width: 100%;
    overflow-x: auto;
    -webkit-overflow-scrolling: touch;
  }
  .table-responsive-sm > .table-bordered {
    border: 0;
  }
}
@media (max-width: 767.98px) {
  .table-responsive-md {
    display: block;
    width: 100%;
    overflow-x: auto;
    -webkit-overflow-scrolling: touch;
  }
  .table-responsive-md > .table-bordered {
    border: 0;
  }
}
@media (max-width: 991.98px) {
  .table-responsive-lg {
    display: block;
    width: 100%;
    overflow-x: auto;
    -webkit-overflow-scrolling: touch;
  }
  .table-responsive-lg > .table-bordered {
    border: 0;
  }
}
@media (max-width: 1199.98px) {
  .table-responsive-xl {
    display: block;
    width: 100%;
    overflow-x: auto;
    -webkit-overflow-scrolling: touch;
  }
  .table-responsive-xl > .table-bordered {
    border: 0;
  }
}
@media (max-width: 1399.98px) {
  .table-responsive-xxl {
    display: block;
    width: 100%;
    overflow-x: auto;
    -webkit-overflow-scrolling: touch;
  }
  .table-responsive-xxl > .table-bordered {
    border: 0;
  }
}
.table-responsive {
  display: block;
  width: 100%;
  overflow-x: auto;
  -webkit-overflow-scrolling: touch;
}
.table-responsive > .table-bordered {
  border: 0;
}
