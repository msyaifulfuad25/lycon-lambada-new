const express = require('express');

const app = express();
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

const server = require('http').createServer(app);


const io = require('socket.io')(server, {
    cors: { 
        origin: "*",
        cridentials: true
    },
    allowEIO3: true, 
});


server.listen(3000, () => {
    console.log('Server is running');
});

io.on('connection', (socket) => {
    console.log('connection = '+socket.id);

    socket.on('sendChatToServer', (message) => {
        console.log(message);

        io.sockets.emit('sendChatToClient', message);
        // console.log('berhasil emit broadcast')
        // socket.broadcast.emit('sendChatToClient', message);
    });

    socket.on('sendNotifToServer', (response) => {
        io.sockets.emit('sendNotifToClient', response);
    });
    
    // socket.on('message', function (message) {

    //     console.log("Received: " + message);
    
    //     socket.send('Text from server');   
    // });    
    // socket.on('sendChatToServer', (message) => {
    //     console.log(message);

    //     // io.sockets.emit('sendChatToClient', message);
    //     socket.broadcast.emit('sendChatToClient', message);
    // });


    // socket.on('disconnect', (socket) => {
    //     console.log('Disconnect');
    // });
});