{{-- Extends layout --}}

@extends('lam_custom_layouts.default')

@section('styles')
    <!-- <link href="{{ asset('css/component.css') }}" rel="stylesheet" type="text/css"/> -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet"></link>
    <link href="//www.amcharts.com/lib/3/plugins/export/export.css?v=7.0.6" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.0.2/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.0.2/css/bootstrap-datetimepicker.css">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/tabel.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/myDatatable.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <style media="screen">
        .container {
            display: none
        }

        .dataTable thead .sorting_asc,
        .dataTable thead .sorting_desc,
        .dataTable thead .sorting_disabled,
        .dataTable thead .sorting {
            padding: 3px 3px 3px 3px !important;
            /* color: black !important; */
        }

        .dataTable thead th {
            text-align: center;
        }
        
        .dataTable tbody td,
        .dataTable tbody td:hover,
        .dataTable tbody tr:hover {
            height: 26px;
            padding: 3px 3px 3px 3px !important;
        }

        .dataTable thead tr{
            height: 40px !important;
        }

        .dataTable tbody tr{
            height: 0px !important;
        }

        /* .my_row { */
            /* height: 35px; */
        /* } */

        /* .my_row:hover { */
            /* display: none; */
            /* padding: 0px 0px 0px 0px !important; */
        /* } */

        .table-bordered th,
        .table-bordered td,
        .table-bordered tfoot th {
            border: 1px solid #95c9ed !important
        }

        .dataTables_wrapper {
            margin-top: -1px !important
        }
        
        #table_master {
            /* width:2500px !important; */
            border: 0;
        }

        /* tbody tr td {
            padding: 0px 5px 0px 5px !important;f
            width: 100px !important;
        } */

        /* .dataTable thead th:nth-child(10) {
            width: 140px !important;
        } */

        /* .dataTable tbody tr td:nth-child(10) {
            width: 140px !important;
        } */


        /* tbody:nth-child(odd) tr td:nth-child(odd) {
            width: 100px !important;
        } */

        #btn-dropdown-info button::after {
            color: white;
        }

        #pagepanel:fullscreen {
            overflow: scroll !important;
        }
        #pagepanel:-ms-fullscreen {
            overflow: scroll !important;
        }
        #pagepanel:-webkit-full-screen {
            overflow: scroll !important;
        }
        #pagepanel:-moz-full-screen {
            overflow: scroll !important;
        }

        .modal-detail {
            min-width: 950px;
        }

        table.dataTable tfoot th {
            padding: 10px 3px 10px 3px !important;
        }

        .tfoot-kode {
            text-align: right;
        }

        #table-detail #thead-qty,
        #table-detail #thead-price,
        #table-detail #thead-total {
            text-align: center !important;
        }
        
        #th-action {
            width: 60px !important;
        }

        #table-log {
            width: 1104px !important;
        }

        td.details-control {
            background: url('https://cdn.rawgit.com/DataTables/DataTables/6c7ada53ebc228ea9bc28b1b216e793b1825d188/examples/resources/details_open.png') no-repeat center center;
            cursor: pointer;
        }
        tr.shown td.details-control {
            background: url('https://cdn.rawgit.com/DataTables/DataTables/6c7ada53ebc228ea9bc28b1b216e793b1825d188/examples/resources/details_close.png') no-repeat center center;
        }

        #header-sticky-1 {
            padding: 10px 16px;
            background: #F7F7F7;
        }

        .sticky-1 {
            position: fixed;
            z-index: 100;
            top: 46px;
            width: 100%;
        }

        #header-sticky-2 {
            padding: 10px 16px;
            background: #F7F7F7;
        }

        .sticky-2 {
            position: fixed;
            z-index: 100;
            top: 90px;
            width: 100%;
        }

        #header-sticky-3 {
            padding: 10px 16px;
            background: #F7F7F7;
        }

        .sticky-3 {
            position: fixed;
            z-index: 100;
            top: 142px;
            width: 100%;
        }

        .dataTables_processing {
            padding-bottom: 40px !important;
        }
    </style>
@endsection


{{-- Content --}}
@section('content')
    <div class="container">
    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
    <div id="pagepanel" class="card card-custom flat table-scrollable pagepanel">
        <div id="loading-fullscreen" style="position: fixed;
            z-index: 999;
            height: 100%;
            width: 100%;
            top: 0;
            left: 0;
            background-color: Black;
            filter: alpha(opacity=60);
            opacity: 0.6;
            -moz-opacity: 0.8;
            display: none">
            <div style="z-index: 1000;
                margin: 300px auto;
                padding: 10px;
                width: 130px;
                background-color: White;
                border-radius: 10px;
                filter: alpha(opacity=100);
                opacity: 1;
                -moz-opacity: 1;">
                <img alt="" src="assets/loading.gif" width="110" height="110"/>
            </div>
        </div>
        <div class="card-header card-header_ bg-main-1 d-flex flat" >
            <div class="card-title">
                <span class="card-icon">
                    <i id="widgeticon" class="text-white icon-briefcase"></i>
                </span>
                <h3 id="widgetcaption" class="card-label text-white">
                    Sales Order
                </h3>
            </div>
            <div class="card-toolbar" >
                <!-- <div id="tool-pertama" style="display: {{ $privilege['isadd'] ? 'block': 'none'}}">
                    <a  onclick="addData()" href="javascript:;" id="btn-add-data" style="color: #FFFFFF;margin-right: 5px;" class="btn btn-sm flat" onmouseover="mouseoverAddData('btn-add-data')" onmouseout="mouseoutAddData('btn-add-data')"><i style="color:#FFFFFF" class="icon-save"></i> Tambah Data</a>
                </div> -->
                <div id="tool-kedua" style="display: none">
                    <!-- <a class="btn btn-sm btn-success flat" style="margin-right: 5px;" id="btn-save-data" onclick="saveData()">
                        <i class="fa fa-save"></i> Save
                    </a> -->
                    <a onclick="cancelData()" class="btn btn-sm btn-default flat" style="margin-right: 5px;">
                        <i class="fa fa-reply"></i> Cancel
                    </a>
                </div>
                
                <div id="btn-saveto">
                </div>

                <div id="tool-ketiga" style="display: none">
                    <a id="back" class="btn btn-sm btn-success flat" onclick="cancelData()"  style="margin-right: 5px;">
                        <i class="fa fa-reply"></i> Back
                    </a>
                </div>
                <div class="dropdown" style="margin-right: 5px;" id="tools-dropdown">
                    <a style="background-color: transparent !important; border: 1px solid white; color: white;" class="btn btn-default btn-sm flat" href="javascript:;" data-toggle="dropdown">
                        <i style="color:#FFFFFF" class="icon-cogs"></i>
                            Tools
                        <i style="color:#FFFFFF" class="icon-angle-down"></i>
                    </a>
                    <!-- <ul style="" id="drop_export" class="dropdown-menu dropdown-menu-right">
                        <li id="drop_export2"></li>
                    </ul> -->

                    <div class="dropdown-menu dropdown-menu-right" style="min-width: 220px; padding: 5px">
                        <div id="drop_export2">
                        </div>
                    </div>

                    <!-- <div class="dropdown-menu dropdown-menu-right" style="min-width: 220px; padding: 5px">
                        <div class="row">
                            <button type="button" class="btn btn-danger col-md-12" onclick="deleteSelected()"><i class="fa fa-trash"></i> Delete Selected</button>
                        </div>
                        <div class="row mt-2">
                            <button type="button" class="btn btn-danger col-md-12" onclick="deleteAll()"><i class="fa fa-trash"></i> Delete All</button>
                        </div>
                        <div class="row mt-2">
                            <button type="button" class="btn btn-success col-md-12" onclick="exportExcel()"><i class="fa fa-file-excel"></i> Export Excel</button>
                        </div>
                    </div> -->
                </div>
                <a style="background-image: none !important; margin-right: 5px; /* height: 27px; */ width: 27px !important; text-align: center; padding: 2px 2px 0px 2px !important; border-radius: 50% !important; background-color: #2196F3; color: #FFF;" id="tool_refresh" href="javascript:;" onclick="refresh()" class="bg-main-1"><i style="color:#FFFFFF" class="icon-refresh"></i></a>
                <!-- <a style="background-image: none !important; margin-right: 5px; /* height: 27px; */ width: 27px !important; text-align: center; padding: 2px 2px 0px 2px !important; border-radius: 50% !important; background-color: #2196F3; color: #FFF;" id="tool_filter" href="javascript:;" onclick="showModalFilter()" class="bg-main-1"><i style="color:#FFFFFF" class="icon-filter"></i></a> -->
                <a style="background-image: none !important; margin-right: 5px; /* height: 27px; */ width: 27px !important; text-align: center; padding: 2px 2px 0px 2px !important; border-radius: 50% !important; background-color: #2196F3; color: #FFF;" id="tool_refresh_up" onClick="toggleButtonCollapse()" href="javascript:;" class="bg-main-1"><i style="color:#FFFFFF" class="icon-sort"></i></a>
                <a class="bg-main-1 ml-2" id="tool_fullscreen" onclick="toggleFullscreen()" style="background-image: none !important; /* height: 27px; */ width: 27px !important; text-align: center; padding: 2px 2px 0px 2px !important; border-radius: 50% !important; background-color: #2196F3; color: #FFF;">
                    <i class="icon-fullscreen" style="color:#FFFFFF;"></i>   
                </a>
            </div>
            </div>
            <div class="card-body card-body-custom flat" id="pagepanel-body" style="padding: 10px !important">
                <input type="hidden" id="judul_excel">
                <input type="hidden"  id="tabel1" value="">
                <input type="hidden"  id="namatable">
                <input type="hidden"  id="url_code">
                
                <div class="row">
                    <div class="btn-toolbar" role="toolbar" aria-label="...">
                        <div id="filterlain">
                        </div>
                    </div>
                </div>

                <div class="cursor"></div>
                <!-- <input type="text" id="datetime" readonly> -->
                <div id="from-departement">
                    <!-- <form data-toggle="validator" class="form" id="formdepartement"> -->
                        
                        <!-- <div class="row" style="margin-top: 20px">
                            <div class="col-md-12">
                                <ul class="nav nav-tabs mb-3 justify-content-end" id="tab-form" role="tablist">
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link active" id="tab-general" data-mdb-toggle="tab" role="tab" aria-controls="tab-general" aria-selected="true" onclick="setTab('tab-general')">General</a>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link" id="tab-others" data-mdb-toggle="tab" role="tab" aria-controls="tab-others" aria-selected="false" onclick="setTab('tab-others')">Others</a>
                                    </li>
                                </ul>

                                <div class="tab-content" id="tab-content">
                                    <div class="tab-pane fade show active" id="tab-general-content" role="tabpanel" aria-labelledby="tab-general-content">
                                        <div class="card">
                                            <div class="card-body">
                                                Content General
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab-others-content" role="tabpanel" aria-labelledby="tab-others-content">
                                        Content Others
                                    </div>
                                </div>
                            </div>
                        </div> -->

                        <!-- Header -->
                        <div class="row" style="margin-top: 20px">
                            <div class="col-md-12" style="padding: 0px !important">
                                <div class="card" style="margin: 0px !important">
                                    <div class="card-header" id="header-sticky-1" style="padding-top: 0px; padding-bottom: 0px;">
                                        <div class="row">
                                            <!-- <div class="col-md-2" style="padding-top: 8px; padding-left: 0px">
                                            </div> -->
                                            <div class="col-md-7">
                                                <div class="form-group pull-left" style="padding-top: 8px; padding-left: 0px; padding-right: 8px">
                                                    <b style="font-size: 16px" id="tab-title">Header</b>
                                                </div>
                                                <div class="form-group pull-left" style="margin-top: 5px">
                                                    <button type="button" class="btn btn-md btn-outline-light" id="idtypetranc-info" title="contoh" style="background-color: {{$color['blue-madison']}};  border-radius: 5px !important">
                                                        <b class="text-light">{{$mtypetranc_nama}}</b>
                                                    </button>
                                                </div>
                                                <div class="form-group pull-left" style="margin-top: 5px; margin-left: 5px;">
                                                    <!-- <div class="col-md-6"></div> -->
                                                    <div class="btn-group" id="btn-dropdown-info">
                                                        <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-md btn-outline-light dropdown-toggle" id="idstatustranc-info" style="background-color: {{$color[$mtypetranctypestatus_classcolor]}}; color: white; border-radius: 5px !important">
                                                            <b class="text-light idstatustranc-info"></b>
                                                        </button>
                                                        <div class="dropdown-menu dropdown-menu-right" style="min-width: 220px">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    Status Transaksi: <span class="idstatustranc-info"></span>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <b>By: <span id="idstatuscard-info"></span></b>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <b>On: <span id="dostatus-info"></span></b>
                                                                </div>
                                                            </div>
                                                            <!-- <div class="form-group row">
                                                                <label class="col-md-4">User</label>
                                                                <div class="col-md-8">
                                                                    <button type="button" class="btn btn-md btn-outline-light" id="idstatuscard-info" style="background-color: #606060; color: white; border-radius: 5px !important">
                                                                        <b></b>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row" style="margin-bottom: 0px !important">
                                                                <label class="col-md-4">Tanggal</label>
                                                                <div class="col-md-8">
                                                                    <button type="button" class="btn btn-md btn-outline-light" id="dostatus-info" style="background-color: #606060; color: white; border-radius: 5px !important">
                                                                        <b></b>
                                                                    </button>
                                                                </div>
                                                            </div> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <ul class="nav nav-tabs mb-3 justify-content-end" id="tab-form" role="tablist" style="margin-bottom: 0px !important">
                                                    <li></li>
                                                    <li class="nav-item" role="presentation">
                                                        <a class="nav-link active" id="tab-general" data-mdb-toggle="tab" role="tab" aria-controls="tab-general" aria-selected="true" onclick="setTab('tab-general', 'Header')">General</a>
                                                    </li>
                                                    <!-- <li class="nav-item" role="presentation">
                                                        <a class="nav-link" id="tab-internal" data-mdb-toggle="tab" role="tab" aria-controls="tab-internal" aria-selected="false" onclick="setTab('tab-internal', 'Internal')">Internal</a>
                                                    </li> -->
                                                    <li class="nav-item" role="presentation">
                                                        <a class="nav-link" id="tab-attachment" data-mdb-toggle="tab" role="tab" aria-controls="tab-attachment" aria-selected="false" onclick="setTab('tab-attachment', 'Attachment')">Attachment (<span id="qty-attachment"></span>)</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="tab-content" id="tab-content">
                                            <div class="tab-pane fade show active" id="tab-general-content" role="tabpanel" aria-labelledby="tab-general-content">
                                                <form id="form-header">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <!-- <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Type Tranc</label>
                                                                <div class="col-md-9">
                                                                    <input type="hidden" name="idtypetranc" value="{{$mtypetranc_noid}}" class="form-control">
                                                                    <input type="text" value="{{$mtypetranc_nama}}" class="form-control" disabled>
                                                                </div>
                                                            </div> -->
                                                            <div class="form-group row">
                                                                <label class="col-md-4 col-form-label">Kode / Kode SI</label>
                                                                <div class="col-md-8">
                                                                    <div class="input-group">
                                                                        <input type="hidden" id="noid">
                                                                        <input type="hidden" name="nourut" id="nourut">
                                                                        <input type="text" name="kode" id="kode" class="form-control" style="background-color: #E9ECEF !important" placeholder="Kode" readonly>

                                                                        <input type="hidden" name="idtypetranc" value="{{$mtypetranc_noid}}" class="form-control">
                                                                        <input type="hidden" name="kodetypetranc" value="{{$mtypetranc_kode}}" class="form-control">

                                                                        <input type="hidden" name="idstatustranc" id="idstatustranc">
                                                                        <input type="hidden" name="idstatuscard" id="idstatuscard">
                                                                        <input type="hidden" name="dostatus" id="dostatus">

                                                                        <div class="input-group-append">
                                                                            <button type="button" id="generatekode" class="btn btn-outline-secondary" onclick="getGenerateCode()">
                                                                                <i class="icon-refresh" style="padding: 0px"></i>
                                                                            </button>
                                                                        </div>
                                                                        <input type="text" name="kodereff" id="kodereff" class="form-control" style="background-color: #E9ECEF !important" placeholder="Kode SI" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-4 col-form-label">Tgl. <span class="text-danger">*</span> / Tgl. Due <span class="text-danger">*</span></label>
                                                                <div class="col-md-8">
                                                                    <div class="input-group">
                                                                        <input type="text" name="tanggal" value="{{$defaulttanggal}}" id="tanggal" class="form-control datepicker" placeholder="Tanggal" autocomplete="off">
                                                                        <div class="input-group-append">
                                                                            <span class="input-group-text">/</span>
                                                                        </div>
                                                                        <input type="text" name="tanggaldue" value="{{$defaulttanggaldue}}" id="tanggaldue" class="form-control datepicker" placeholder="Tanggal Due" autocomplete="off">
                                                                        <div class="input-group-append">
                                                                            <button type="button" class="btn btn-outline-secondary" onclick="showModalTanggal()">
                                                                                <i class="icon-eye-open" style="padding: 0px"></i>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-4 col-form-label">Type Cost Ctr. <span class="text-danger">*</span></label>
                                                                <div class="col-md-8">
                                                                    <select class="form-control select2" name="idtypecostcontrol" id="idtypecostcontrol" onchange="getGenerateCode()" disabled>
                                                                        <option value="">[Choose Type Cost Center]</option>
                                                                        @foreach($mtypecostcontrol as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <!-- <div class="form-group row">
                                                                <label class="col-md-4 col-form-label">Gudang <span class="text-danger">*</span></label>
                                                                <div class="col-md-8">
                                                                    <select class="form-control select2" name="idgudang" id="idgudang">
                                                                        <option value="" id="idgudang-empty">[Choose Gudang]</option>
                                                                        @foreach($genmgudang as $v)
                                                                            <option 
                                                                                value="{{$v->noid}}" id="idgudang-{{$v->noid}}"
                                                                                <?= $sessionidgudang == $v->noid ? 'selected' : '' ?>
                                                                            >{{$v->kode}} - {{$v->namalias}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div> -->
                                                            <div class="form-group row">
                                                                <label class="col-md-4 col-form-label">Nama Pekerjaan <span class="text-danger">*</span></label>
                                                                <div class="col-md-8">
                                                                    <input type="text" name="projectname" class="form-control" id="projectname" placeholder="Nama Pekerjaan" disabled>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-4 col-form-label">Lokasi Pekerjaan <span class="text-danger">*</span></label>
                                                                <div class="col-md-8">
                                                                    <textarea name="projectlocation" id="projectlocation" cols="3" rows="5" class="form-control" placeholder="Lokasi Pekerjaan" disabled></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-4 col-form-label">Nilai Pekerjaan <span class="text-danger">*</span></label>
                                                                <div class="col-md-8">
                                                                    <input type="text" name="projectvalue" class="form-control input-mask" id="projectvalue" placeholder="Nilai Pekerjaan" disabled>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row" style="display: none">
                                                                <label class="col-md-4 col-form-label">ID Currency</label>
                                                                <div class="col-md-8">
                                                                    <select class="form-control select2" name="idcurrency" id="idcurrency">
                                                                        <option value="" id="idcurrency-empty">[Choose Currency]</option>
                                                                        @foreach($accmcurrency as $v)
                                                                            <option 
                                                                                value="{{$v->noid}}" 
                                                                                id="idcurrency-{{$v->noid}}"
                                                                                data-accmcurrency-nama="{{$v->nama}}" 
                                                                                <?= $v->noid == 1 ? 'selected' : '' ?>
                                                                            >{{$v->kode}} - {{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="col-md-4 col-form-label">Jenis Pekerjaan <span class="text-danger">*</span></label>
                                                                <div class="col-md-8">
                                                                    <select class="form-control select2" name="projecttype" id="projecttype" disabled>
                                                                        <option value="">[Choose Jenis Pekerjaan]</option>
                                                                        @foreach($mtypeproject as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-4 col-form-label">Customer <span class="text-danger">*</span></label>
                                                                <div class="col-md-8">
                                                                    <select class="form-control select2" name="idcardcustomer" id="idcardcustomer" disabled>
                                                                        <option value="">[Choose Customer]</option>
                                                                        @foreach($mcardcustomer as $v)
                                                                            <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-4 col-form-label">Marketing</label>
                                                                <div class="col-md-8">
                                                                    <select class="form-control select2" name="idcardsales_" id="idcardsales" disabled>
                                                                        <option value="">[Choose Marketing]</option>
                                                                        @foreach($mcard as $v)
                                                                            @if($v->isemployee == 1)
                                                                                <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <!-- <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Company - Dept.</label>
                                                                <div class="col-md-4">
                                                                    <select class="form-control select2" name="idcompany" id="idcompany" onclick="changeCompany()">
                                                                        <option value="" id="idcompany-empty">[Choose Company]</option>
                                                                        @foreach($genmcompany as $v)
                                                                            <option 
                                                                                value="{{$v->noid}}" id="idcompany-{{$v->noid}}"
                                                                                <?= $sessionidcompany == $v->noid ? 'selected' : '' ?>
                                                                            >{{$v->kode}} - {{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <select class="form-control select2" name="iddepartment" id="iddepartment" onclick="changeDepartment()">
                                                                        <option value="" id="iddepartment-empty">[Choose Department]</option>
                                                                        @foreach($genmdepartment as $v)
                                                                            <option 
                                                                                value="{{$v->noid}}" id="iddepartment-{{$v->noid}}"
                                                                                <?= $sessioniddepartment == $v->noid ? 'selected' : '' ?>
                                                                            >{{$v->kode}} - {{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div> -->
                                                            <!-- <div class="form-group row">
                                                                <label class="col-md-4 col-form-label">Gudang</label>
                                                                <div class="col-md-8">
                                                                    <select class="form-control select2" name="idgudang" id="idgudang">
                                                                        <option value="" id="idgudang-empty">[Choose Gudang]</option>
                                                                        @foreach($genmgudang as $v)
                                                                            <option 
                                                                                value="{{$v->noid}}" id="idgudang-{{$v->noid}}"
                                                                                <?= $sessionidgudang == $v->noid ? 'selected' : '' ?>
                                                                            >{{$v->kode}} - {{$v->namalias}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div> -->
                                                            <div class="form-group row">
                                                                <label class="col-md-4 col-form-label">Project Koordinator</label>
                                                                <div class="col-md-8">
                                                                    <select class="form-control select2" name="idcardkoor" id="idcardkoor">
                                                                        <option value="" id="idcardkoor-empty">[Choose Project Koordinator]</option>
                                                                        @foreach($mcard as $v)
                                                                            @if($v->isemployee == 1)
                                                                                <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-4 col-form-label">Project Manager</label>
                                                                <div class="col-md-8">
                                                                    <select class="form-control select2" name="idcardpj" id="idcardpj">
                                                                        <option value="" id="idcardpj-empty">[Choose Project Manager]</option>
                                                                        @foreach($mcard as $v)
                                                                            @if($v->isemployee == 1)
                                                                                <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-4 col-form-label">Admin Site Project</label>
                                                                <div class="col-md-8">
                                                                    <select class="form-control select2" name="idcardsitekoor" id="idcardsitekoor">
                                                                        <option value="" id="idcardsitekoor-empty">[Choose Admin Site Project]</option>
                                                                        @foreach($mcard as $v)
                                                                            @if($v->isemployee == 1)
                                                                                <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-4 col-form-label">Drafter</label>
                                                                <div class="col-md-8">
                                                                    <select class="form-control select2" name="idcarddrafter" id="idcarddrafter">
                                                                        <option value="" id="idcarddrafter-empty">[Choose Drafter]</option>
                                                                        @foreach($mcard as $v)
                                                                            @if($v->isemployee == 1)
                                                                                <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-4 col-form-label">Sponsor</label>
                                                                <div class="col-md-8">
                                                                    <select class="form-control select2" name="idcardsponsor" id="idcardsponsor">
                                                                        <option value="" id="idcardsponsor-empty">[Choose Sponsor.]</option>
                                                                        @foreach($mcard as $v)
                                                                            @if($v->isemployee == 1)
                                                                                <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <!-- <div class="tab-pane fade" id="tab-internal-content" role="tabpanel" aria-labelledby="tab-internal-content">
                                                <form id="form-internal">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Cash Bayar</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="idcashbankbayar" id="idcashbankbayar">
                                                                        <option value="" id="idcashbankbayar-empty">[Choose Cash Bayar]</option>
                                                                        @foreach($accmcashbank as $v)
                                                                            <option value="{{$v->noid}}" id="idcashbankbayar-{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Akun Persediaan</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="idakunpersediaan" id="idakunpersediaan">
                                                                        <option value="" id="idakunpersediaan-empty">[Choose Akun Persediaan]</option>
                                                                        @foreach($accmcashbank as $v)
                                                                            <option value="{{$v->noid}}" id="idakunpersediaan-{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div> -->
                                            <div class="tab-pane fade" id="tab-attachment-content" role="tabpanel" aria-labelledby="tab-attachment-content">
                                                <form id="form-cdfdetail">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div id="flowchart"></div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <a class="btn btn-default" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="true" aria-controls="collapseExample" style="margin-left: 6px; margin-bottom: 6px">
                                                                <i class="icon-list"></i> Detail Attachment
                                                            </a>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="collapse" id="collapseExample">
                                                                <div class="card card-body">
                                                                    <div id="cdf-detail" style="border: 0px solid black;">
                                                                        -
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Detail -->
                        <div class="row" style="margin-top: 10px; display: none">
                            <div class="col-md-12" style="padding: 0px !important">
                                <div class="card" style="margin: 0px !important">
                                    <div class="card-header" id="header-sticky-2">
                                        <div class="row">                                        
                                            <b class="col-md-1" style="font-size: 16px">Detail</b>
                                            <div class="col-md-3 create-detail" style="display: none">
                                                <div style="margin-top: 5px">
                                                    <input type="radio" name="searchtypeprev" id="searchtypeprev-1" value="1"> <label for="searchtypeprev-1">Previeous</label>
                                                    <input type="radio" name="searchtypeprev" id="searchtypeprev-2" value="2" checked> <label for="searchtypeprev-2">Inventory</label>
                                                </div>
                                            </div>
                                            <div class="col-md-3 create-detail">
                                                <input type="text" name="searchprev" id="searchprev" class="form-control" placeholder="Search">
                                            </div>
                                            <div class="col-md-2 create-detail">
                                                <button type="button" class="btn btn-primary" onclick="showModalDetailPrev()"><i class="icon-search"></i> Search</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <form id="form-detail">
                                            <div class="card" style="margin: 0px 0px 10px 0px !important; display: none">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label>Kode Inventor</label>
                                                                <input type="hidden" name="kodeprev" id="kodeprev" class="form-control" style="background-color: #E9ECEF !important" placeholder="Kode Purchase Offer" readonly>
                                                                <input type="hidden" id="keydetail">
                                                                <input type="hidden" id="noiddetail">
                                                                <input type="hidden" id="idmaster">
                                                                <select class="form-control" style="background-color: #E9ECEF !important" name="idinventor" id="idinventor" readonly>
                                                                    <option value="" style="display:none">[Choose Inventor]</option>
                                                                    @foreach($invminventory as $v)
                                                                        <option 
                                                                            value="{{$v->noid}}"
                                                                            data-invminventory-kode="{{$v->kode}}"
                                                                            data-invminventory-nama="{{$v->nama}}"
                                                                            data-invminventory-idsatuan="{{$v->idsatuan}}"
                                                                            data-invminventory-namasatuan="{{$v->namasatuan}}"
                                                                            data-invminventory-konversi="{{$v->konversi}}"
                                                                            data-invminventory-idtax="{{$v->idtax}}"
                                                                            data-invminventory-taxprocent="{{$v->taxprocent}}"
                                                                            data-invminventory-purchaseprice="{{$v->purchaseprice}}"
                                                                            style="display:none"
                                                                        >{{$v->kode}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label>Nama Inventor Original</label>
                                                                <input type="text" name="namainventor" id="namainventor" class="form-control" style="background-color: #E9ECEF !important" placeholder="Nama Inventor Original" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label>Qty Updated <span class="text-danger">*</span></label>
                                                                <input type="hidden" name="namainventor2" id="namainventor2" class="form-control" style="background-color: #E9ECEF !important" placeholder="Nama Inventor Updated" readonly>
                                                                <input type="hidden" id="unitqtyoriginal" class="form-control" style="background-color: #E9ECEF !important" placeholder="Qty" readonly>
                                                                <input type="text" name="unitqty" id="unitqty" class="form-control" placeholder="Qty Updated">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label>Keterangan</label>
                                                                <textarea name="keterangan" id="keterangandetail" cols="3" rows="3" class="form-control" placeholder="Keterangan"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row pull-right">
                                                        <div class="col-md-12">
                                                            <button type="button" id="cancel-detail" class="btn btn-default" onclick="cancelDetail()">
                                                                <i class="fa fa-arrow-left"></i> Cancel
                                                            </button>
                                                            <button type="button" id="save-detail" class="btn btn-success" onclick="saveDetail()">
                                                                <i class="fa fa-save"></i> Add
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                        <div id="appendInfoDatatableDetail" class="mt-2" style="border-top: 2px solid #3598dc; border-right: 3px solid #3598dc; border-left: 2px solid #3598dc;">
                                        </div>
                                        <div id="ts-9921050102" class="table-scrollable" style="border: 1px solid #3598dc; background-color: #3598dc; margin-top: 0px !important">
                                            <table bg-color="blue" class="nowrap table table-striped table-bordered table-hover  dataTable no-footer" id="table-detail" style="background-color:#BBDEFB !important; margin: 0 !important; border: 1px solid #3598dc"  aria-describedby="table_master_info" role="grid">
                                                <thead>
                                                    <!-- <tr id="tab-thead-9921050102" class="bg-main"><th style="text-align:center" colspan="2">#</th><th style="text-align:center" colspan="26"></th><th colspan="1" style="text-align:center">---</th></tr> -->
                                                    <tr id="tab-thead-columns-d-9921050102">
                                                        <th style="width: 30px;">
                                                            <input type="checkbox" id="select_all_9921050102" onchange="selectAllCheckboxTab(9921050102)" class="checkbox-9921050102 checkbox-9921050102-0" name="checkbox-9921050102[]" value="FALSE">
                                                        </th>
                                                        <th>No</th>
                                                        <!-- <th>Kode Prev.</th> -->
                                                        <th>Kode Inv.</th>
                                                        <th>Nama</th>
                                                        <th>Inventor</th>
                                                        <th>Keterangan</th>
                                                        <!-- <th>ID Gudang</th>
                                                        <th>ID Company</th>
                                                        <th>ID Department</th>
                                                        <th>ID Satuan</th>
                                                        <th>Konv Satuan</th> -->
                                                        <!-- <th id="thead-qty">Qty</th> -->
                                                        <!-- <th id="thead-qtysisa">Qty Sisa</th> -->
                                                        <!-- <th>Unit</th> -->
                                                        <!-- <th id="thead-price">Price</th> -->
                                                        <!-- <th id="thead-total">Total</th> -->
                                                        <!-- <th>Currency</th> -->
                                                        <!-- <th>Subtotal</th>
                                                        <th>Discount Var</th>
                                                        <th>Discount</th>
                                                        <th>Nilai Disc</th>
                                                        <th>ID Type Tax</th>
                                                        <th>ID Tax</th> -->
                                                        <!-- <th>Harga Total</th> -->
                                                        <!-- <th>ID Akun HPP</th>
                                                        <th>ID Akun Sales</th>
                                                        <th>ID Akun Persediaan</th>
                                                        <th>ID Akun Sales Return</th>
                                                        <th>ID Akun Purchase Return</th>
                                                        <th>Serial Number</th>
                                                        <th>Garansi Tanggal</th>
                                                        <th>Garansi Expired</th> -->
                                                        <!-- <th>Type Tranc Prev</th>
                                                        <th>Tranc Prev</th>
                                                        <th>Tranc Prev D</th>
                                                        <th>Type Tranc Order</th>
                                                        <th>Tranc Order</th>
                                                        <th>Tranc Order D</th> -->
                                                        <th id="th-action">Action</th>
                                                    </tr>
                                                </thead>
                                                <tfoot>
                                                    <tr>
                                                        <th></th>
                                                        <th></th>
                                                        <!-- <th></th> -->
                                                        <th class="tfoot-kode"></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <!-- <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th> -->
                                                        <th></th>
                                                        <!-- <th class="text-right" colspan="2" style="border: 1px solid #95c9ed"></th> -->
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Footer -->
                        <div class="row" style="margin-top: 10px">
                            <div class="col-md-12" style="padding: 0px !important">
                                <div class="card" style="margin: 0px !important">
                                    <div class="card-header" id="header-sticky-3">
                                        <b style="font-size: 16px">Footer</b>
                                    </div>
                                    <div class="card-body">
                                        <form id="form-footer">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label">Keterangan</label>
                                                        <div class="col-md-9">
                                                            <textarea name="keterangan" id="keterangan" cols="3" rows="5" class="form-control" placeholder="Keterangan"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6" style="display: none">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label">Subtotal</label>
                                                        <div class="col-md-9">
                                                            <input type="text" name="subtotal" id="subtotal" class="form-control input-mask" style="background-color: #E9ECEF !important" placeholder="Subtotal" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label">Total</label>
                                                        <div class="col-md-9">
                                                            <input type="text" name="total" id="total" class="form-control input-mask" style="background-color: #E9ECEF !important" placeholder="Total" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <!-- <ul class="nav nav-tabs mb-3" id="tab-form_" role="tablist">
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link active" id="tab-transaksi" data-mdb-toggle="tab" role="tab" aria-controls="tab-transaksi" aria-selected="true" onclick="setTab('tab-transaksi')">Transaksi</a>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link" id="tab-supplier" data-mdb-toggle="tab" role="tab" aria-controls="tab-supplier" aria-selected="false" onclick="setTab('tab-supplier')">Supplier</a>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link" id="tab-delivery" data-mdb-toggle="tab" role="tab" aria-controls="tab-delivery" aria-selected="false" onclick="setTab('tab-delivery')">Delivery</a>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link" id="tab-akun" data-mdb-toggle="tab" role="tab" aria-controls="tab-akun" aria-selected="false" onclick="setTab('tab-akun')">Akun</a>
                                    </li>
                                </ul> -->

                                <div class="tab-content" id="tab-content_">
                                    <div class="tab-pane fade" id="tab-transaksi-content" role="tabpanel" aria-labelledby="tab-transaksi-content">
                                        <form id="form-transaksi">
                                            <div class="card">
                                                <!-- <div class="card-header" style="background-color: #8E44AD; color: white"> -->
                                                <div class="card-header">
                                                    <b style="font-size: 18px">Data Transaksi</b>
                                                </div>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Status Tranc</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Status Card</label>
                                                                <select class="form-control select2">
                                                                <option value="">[Choose Status Card]</option>
                                                                    @foreach($mcard as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Kode</label>
                                                                <input type="text" name="kode" class="form-control" placeholder="Kode">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Kode SI</label>
                                                                <input type="text" name="kodereff" class="form-control" placeholder="Kode SI">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Tanggal</label>
                                                                <input type="text" class="form-control datepicker" placeholder="Tanggal" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Tanggal Tax</label>
                                                                <input type="text" name="tanggaltax" class="form-control datepicker" placeholder="Tanggal Tax" autocomplete="off">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Tanggal Due</label>
                                                                <input type="text" class="form-control datepicker" placeholder="Tanggal Due" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <!-- <div class="form-group">
                                                                <label>Tanggal Tax</label>
                                                                <input type="text" class="form-control datepicker" placeholder="Tanggal Tax">
                                                            </div> -->
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Keterangan</label>
                                                                <textarea name="keterangan" id="" cols="3" rows="5" class="form-control" placeholder="Keterangan"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>ID Currency</label>
                                                                <select class="form-control select2">
                                                                    @foreach($accmcurrency as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Konv Curr</label>
                                                                <input type="text" name="konvcurr" class="form-control" placeholder="Konv Curr">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>ID Gudang</label>
                                                                <select class="form-control select2">
                                                                    @foreach($genmgudang as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->namalias}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>ID Company</label>
                                                                <!-- <select class="form-control select2" name="idcompany">
                                                                    @foreach($genmcompany as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                    @endforeach
                                                                </select> -->
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>ID Department</label>
                                                                <!-- <select class="form-control select2" name="iddepartment">
                                                                    @foreach($genmdepartment as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                    @endforeach
                                                                </select> -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>ID Type Tranc Prev</label>
                                                                <select class="form-control select2" name="idtypetrancprev">
                                                                    @foreach($mtypetranc as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>ID Tranc Prev</label>
                                                                <input type="text" name="idtrancprev" class="form-control" placeholder="ID Tranc Prev">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>ID Type Tranc Order</label>
                                                                <select class="form-control select2" name="idtypetrancorder">
                                                                    @foreach($mtypetranc as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>ID Tranc Order</label>
                                                                <input type="text" name="idtrancorder" class="form-control" placeholder="ID Tranc Order">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane fade" id="tab-supplier-content" role="tabpanel" aria-labelledby="tab-supplier-content">
                                        
                                    </div>
                                    <div class="tab-pane fade" id="tab-delivery-content" role="tabpanel" aria-labelledby="tab-delivery-content">
                                        <form id="form-delivery">
                                            <div class="card">
                                                <!-- <div class="card-header" style="background-color: #32C5D2; color: white"> -->
                                                <div class="card-header">
                                                    <b style="font-size: 18px">Data Delivery</b>
                                                </div>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label>Card Delivery</label>
                                                                <select class="form-control select2" name="idcarddelivery">
                                                                    @foreach($mcard as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Delivery Name</label>
                                                                <input type="text" name="deliveryname" class="form-control" placeholder="Delivery Name">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Delivery Tanggal</label>
                                                                <input type="text" name="deliverytanggal" class="form-control" placeholder="Delivery Tanggal">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Delivery Vehicle</label>
                                                                <input type="text" name="deliveryvehicle" class="form-control" placeholder="Delivery Vehicle">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Delivery Vehicle Plat</label>
                                                                <input type="text" name="deliveryvehicleplat" class="form-control" placeholder="Delivery Vehicle Plat">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane fade" id="tab-akun-content" role="tabpanel" aria-labelledby="tab-akun-content">
                                        <form id="form-akun">
                                            <div class="card">
                                                <!-- <div class="card-header" style="background-color: #C49F47; color: white"> -->
                                                <div class="card-header">
                                                    <b style="font-size: 18px">Data Akun</b>
                                                </div>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Akun Bayar</label>
                                                                <select class="form-control select2" name="idakunbayar">
                                                                    @foreach($accmcashbank as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Akun Deposit</label>
                                                                <select class="form-control select2" name="idakundeposit">
                                                                    @foreach($accmcashbank as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Akun Biaya Lain</label>
                                                                <select class="form-control select2" name="idakunbiayalain">
                                                                    @foreach($accmcashbank as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Akun Hutang</label>
                                                                <select class="form-control select2" name="idakunhutang">
                                                                    @foreach($accmcashbank as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Akun Purchase</label>
                                                                <select class="form-control select2" name="idakunpurchase">
                                                                    @foreach($accmcashbank as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Akun Persediaan</label>
                                                                <select class="form-control select2" name="idakunpersediaan">
                                                                    @foreach($accmcashbank as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Akun Sales Return</label>
                                                                <select class="form-control select2" name="idakunsalesreturn">
                                                                    @foreach($accmcashbank as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Akun Discount</label>
                                                                <select class="form-control select2" name="idakundiscount">
                                                                    @foreach($accmcashbank as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="row" style="margin-top: 20px">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body" style="display: none">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>ID Create</label>
                                                    <select class="form-control" disabled>
                                                        @foreach($mcard as $v)
                                                            <option value="{{$v->noid}}" <?= $v->noid == $sessionnoid ? 'selected' : '' ?>>{{$v->kode}} - {{$v->nama}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Do Create</label>
                                                    <input type="text" class="form-control" value="<?= date('d-M-Y H:i:s')?>" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>ID Update</label>
                                                    <select class="form-control" disabled>
                                                        @foreach($mcard as $v)
                                                            <option value="{{$v->noid}}" <?= $v->noid == $sessionnoid ? 'selected' : '' ?>>{{$v->kode}} - {{$v->nama}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Last Update</label>
                                                    <input type="text" class="form-control" value="<?= date('d-M-Y H:i:s')?>" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    <!-- </form> -->
                </div>
                <div id="tabledepartemen" class="table table-bordered " style="width: 100%; border: 0px; margin: -10px 0px -10px 0px !important">
                    <!-- <div id="divsucces" class="alert alert-custom alert-light-success fade show mb-5" role="alert">
                        <p id="notiftext"></p>
                    </div> -->
                <!-- <table id="tabletest" class="nowrap" style="width 100px; table-layout: auto">
                    <thead>
                        <tr>
                            <th style="width: 10% !important">Halaman 1</th>
                            <th style="width: 10% !important">Halaman 2</th>
                            <th style="width: 10% !important">Halaman 3</th>
                            <th style="width: 10% !important">Halaman 4</th>
                        </tr>
                    </thead>
                </table> -->
                <!-- <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12 col-xs-12 dtfiler">
                            <div class="col-md-4" id="table_master_paginate">
                                <div class="pagination-panel row">
                                    Page
                                    <a href="#" class="btn btn-sm default prev disabled" title="<i class='fa fa-angle-left'></i>">
                                        <i class="fa fa-angle-left"></i>
                                    </a>
                                    <input type="text" class="pagination-panel-input form-control input-mini input-inline input-sm" maxlenght="5" style="text-align:center; margin: 0 5px;">
                                    <a href="#" class="btn btn-sm default next disabled" title="<i class='fa fa-angle-right'> </i>">
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                     of 
                                    <span class="pagination-panel-total">1</span>
                                </div>
                            </div>
                            <div class="col-md-4" id="table_master_length">
                                <label class="">
                                    <span class="seperator">|</span>
                                    Rec/Page 
                                    <select name="table_master_length" aria-controls="table_master" class="form-control input-inline input-xsmall input-sm recofdt">
                                        <option value="10">10</option>
                                        <option value="50">50</option>
                                        <option value="150">150</option>
                                        <option value="1000">All</option>
                                    </select>
                                </label>
                            </div>
                            <div class="col-md-4" id="table_master_info" role="status" aria-live="polite">
                                <span class="seperator">|</span>Total 6 records 
                            </div>
                        </div>
                        <!-- <div class="dtfilter_search">
                            <div id="table_master_filter" class="dataTables_filter">
                                <label>
                                    <input type="search" class="form-control input-inline input-sm" placeholder="" aria-controls="table_master">
                                </label>
                            </div>
                            <div class="btn fa fa-search dtfilter_search_btn">
                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="row" style="margin-top: 10px">
                    <!-- <div class="col"> -->
                        <div id="space_filter_button" style="width: 100%">
                            <div class="row">
                                <div class="btn-group">
                                    <button type="button" id="dropdown-date-filter" class="btn btn-sm dropdown-toggle text-right" style="height: 31px; background-color: #a1b1bc; color: white; margin-right: 0px;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="icon-calendar pull-left" style="margin-top: 2px; color: white"></i>
                                        <span>{{$datefilter['a']}}</span>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" style="width: 215px">
                                        <a id="did-a" data-date="{{$datefilter['a']}}" class="dropdown-item dropdown-item-date" style="padding-left: 10px; padding-right: 10px;" onclick="filterDate('a')" onmouseover="hoverFilterDate('a')" onmouseout="hoverOutFilterDate('a')">
                                            <span style="background-color: {{ $datefilterdefault == 'a' ? '#4b8df8' : '#f5f5f5' }}; width: 100%; padding: 2px 5px 2px 5px; color: {{ $datefilterdefault == 'a' ? 'white' : '#08c' }}">All</span>
                                        </a>
                                        <a id="did-t"data-date="{{$datefilter['t']}}" class="dropdown-item dropdown-item-date" style="padding-left: 10px; padding-right: 10px;" onclick="filterDate('t')" onmouseover="hoverFilterDate('t')" onmouseout="hoverOutFilterDate('t')">
                                            <span style="background-color: {{ $datefilterdefault == 't' ? '#4b8df8' : '#f5f5f5' }}; width: 100%; padding: 2px 5px 2px 5px; color: {{ $datefilterdefault == 't' ? 'white' : '#08c' }}">Today</span>
                                        </a>
                                        <a id="did-y" data-date="{{$datefilter['y']}}" class="dropdown-item dropdown-item-date" style="padding-left: 10px; padding-right: 10px;" onclick="filterDate('y')" onmouseover="hoverFilterDate('y')" onmouseout="hoverOutFilterDate('y')">
                                            <span style="background-color: {{ $datefilterdefault == 'y' ? '#4b8df8' : '#f5f5f5' }}; width: 100%; padding: 2px 5px 2px 5px; color: {{ $datefilterdefault == 'y' ? 'white' : '#08c' }};">Yesterday</span>
                                        </a>
                                        <a id="did-l7d" data-date="{{$datefilter['l7d']}}" class="dropdown-item dropdown-item-date" style="padding-left: 10px; padding-right: 10px;" onclick="filterDate('l7d')" onmouseover="hoverFilterDate('l7d')" onmouseout="hoverOutFilterDate('l7d')">
                                            <span style="background-color: {{ $datefilterdefault == 'l7d' ? '#4b8df8' : '#f5f5f5' }}; width: 100%; padding: 2px 5px 2px 5px; color: {{ $datefilterdefault == 'l7d' ? 'white' : '#08c' }};">Last 7 Days</span>
                                        </a>
                                        <a id="did-l30d" data-date="{{$datefilter['l30d']}}" class="dropdown-item dropdown-item-date" style="padding-left: 10px; padding-right: 10px;" onclick="filterDate('l30d')" onmouseover="hoverFilterDate('l30d')" onmouseout="hoverOutFilterDate('30d')">
                                            <span style="background-color: {{ $datefilterdefault == 'l30d' ? '#4b8df8' : '#f5f5f5' }}; width: 100%; padding: 2px 5px 2px 5px; color: {{ $datefilterdefault == 'l30d' ? 'white' : '#08c' }};">Last 30 Days</span>
                                        </a>
                                        <a id="did-tm" data-date="{{$datefilter['tm']}}" class="dropdown-item dropdown-item-date" style="padding-left: 10px; padding-right: 10px;" onclick="filterDate('tm')" onmouseover="hoverFilterDate('tm')" onmouseout="hoverOutFilterDate('tm')">
                                            <span style="background-color: {{ $datefilterdefault == 'tm' ? '#4b8df8' : '#f5f5f5' }}; width: 100%; padding: 2px 5px 2px 5px; color: {{ $datefilterdefault == 'tm' ? 'white' : '#08c' }};">This Month</span>
                                        </a>
                                        <a id="did-lm" data-date="{{$datefilter['lm']}}" class="dropdown-item dropdown-item-date" style="padding-left: 10px; padding-right: 10px;" onclick="filterDate('lm')" onmouseover="hoverFilterDate('lm')" onmouseout="hoverOutFilterDate('lm')">
                                            <span style="background-color: {{ $datefilterdefault == 'lm' ? '#4b8df8' : '#f5f5f5' }}; width: 100%; padding: 2px 5px 2px 5px; color: {{ $datefilterdefault == 'lm' ? 'white' : '#08c' }};">Last Month</span>
                                        </a>
                                        <a id="did-cr" class="dropdown-item dropdown-item-date" style="padding-left: 10px; padding-right: 10px;" onclick="filterDate('cr')" onmouseover="hoverFilterDate('cr')" onmouseout="hoverOutFilterDate('cr')" disabled>
                                            <span style="background-color: {{ $datefilterdefault == 'cr' ? '#4b8df8' : '#f5f5f5' }}; width: 100%; padding: 2px 5px 2px 5px; color: {{ $datefilterdefault == 'cr' ? 'white' : '#08c' }};">Custom Range</span>
                                        </a>
                                        <div class="row" style="padding-left: 5px; padding-right: 5px;">
                                            <div class="col-md-6">
                                                <label>From</label>
                                                <input type="text" name="did-from" value="{{date('d-m-Y', mktime (0, 0, 0, date('m'), 1, date('Y')))}}" id="did-from" class="form-control datepicker2" onchange="changeCustomRange()" style="font-size: 12px" placeholder="From">
                                            </div>
                                            <div class="col-md-6">
                                                <label>To</label>
                                                <input type="text" name="did-to" value="{{date('d-m-Y', mktime (0, 0, 0, date('m'), date('t'), date('Y')))}}" id="did-to" class="form-control datepicker2" onchange="changeCustomRange()" style="font-size: 12px" placeholder="To">
                                            </div>
                                        </div>
                                        <div class="row" style="padding-left: 5px; padding-right: 5px; margin-top: 8px; margin-bottom: 2px">
                                            <div class="col-md-12">
                                                <button class="btn btn-success" onclick="filterDate('cr')">Apply</button>
                                                <div class="btn btn-default">Cancel</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="btn" style="padding: 0px; width: 5px"></div>
                                @foreach($mtypetranctypestatus as $k => $v)
                                    @if($k == 0)
                                        <button class="btn btn-outline-secondary btn-sm flat btn-filter-all btn-filter-idstatustranc" id="btn-filter-idstatustranc-all" style="background: rgb(144, 202, 249); color: black; font-weight: bold; margin-right: 0px; margin-bottom: 5px; height: 31px;" onclick="filterData('btn-filter-idstatustranc-all', 'btn-filter-idstatustranc', 1, 'ALL', 'grey', 0, true, '', '', 'idstatustranc')">ALL</button>
                                    @else
                                        <button class="btn btn-outline-secondary btn-sm flat btn-filter-idstatustranc" id="btn-filter-idstatustranc-{{$v->noid}}" style="background-color:#E3F2FD; color: grey; margin-right: 0px; margin-bottom: 5px; height: 31px;" onclick="filterData('btn-filter-idstatustranc-{{$v->noid}}', 'btn-filter-idstatustranc', 1, 'DRAFT', 'grey', -1, false, 'fincashbank', 'AND', 'idstatustranc', '=', '{{$v->noid}}')">{{$v->nama}}</button>
                                    @endif
                                @endforeach

                                <div class="btn" style="padding: 0px; width: 5px"></div>
                                @foreach($mtypecostcontrol as $k => $v)
                                    @if($k == 0)
                                        <button class="btn btn-outline-secondary btn-sm flat btn-filter-all btn-filter-idtypecostcontrol" id="btn-filter-idtypecostcontrol-all" style="background: rgb(144, 202, 249); color: black; font-weight: bold; margin-right: 0px; margin-bottom: 5px; height: 31px;" onclick="filterData('btn-filter-idtypecostcontrol-all', 'btn-filter-idtypecostcontrol', 1, 'ALL', 'grey', 0, true, '', '', 'idtypecostcontrol')">ALL</button>
                                    @else
                                        <button class="btn btn-outline-secondary btn-sm flat btn-filter-idtypecostcontrol" id="btn-filter-idtypecostcontrol-{{$v->noid}}" style="background-color:#E3F2FD; color: grey; margin-right: 0px; margin-bottom: 5px; height: 31px;" onclick="filterData('btn-filter-idtypecostcontrol-{{$v->noid}}', 'btn-filter-idtypecostcontrol', 1, 'DRAFT', 'grey', -1, false, 'fincashbank', 'AND', 'idtypecostcontrol', '=', '{{$v->noid}}')">{{$v->nama}}</button>
                                    @endif
                                @endforeach

                                <!-- <div class="btn" style="padding: 0px; width: 5px"></div>
                                @foreach($genmcompany as $k => $v)
                                    @if($k == 0)
                                        <button class="btn btn-outline-secondary btn-sm flat btn-filter-all btn-filter-idcompany" id="btn-filter-idcompany-all" style="background: rgb(144, 202, 249); color: black; font-weight: bold; margin-right: 0px; margin-bottom: 5px; height: 31px;" onclick="filterData('btn-filter-idcompany-all', 'btn-filter-idcompany', 1, 'ALL', 'grey', 0, true, '', '', 'idcompany')">ALL</button>
                                    @else
                                        <button class="btn btn-outline-secondary btn-sm flat btn-filter-idcompany" id="btn-filter-idcompany-{{$v->noid}}" style="background-color:#E3F2FD; color: grey; margin-right: 0px; margin-bottom: 5px; height: 31px;" onclick="filterData('btn-filter-idcompany-{{$v->noid}}', 'btn-filter-idcompany', 1, 'DRAFT', 'grey', -1, false, 'fincashbank', 'AND', 'idcompany', '=', '{{$v->noid}}')">{{$v->nama}}</button>
                                    @endif
                                @endforeach -->
                            </div>
                        </div>
                            <!-- <button class="btn btn-outline-secondary btn-sm flat btn-filter-1-9921050101" id="btn-filter-NEEDAPPROVED-9921050101" style="background-color:#E3F2FD; color: grey; margin-right: 0px" onclick="filterData('btn-filter-NEEDAPPROVED-9921050101', 'btn-filter-1-9921050101', 1, 'NEED APPROVED', 'grey', -1, false, 'fincashbank', 'AND', 'idstatustranc', '=', '2')">NEED APPROVED </button><button class="btn btn-outline-secondary btn-sm flat btn-filter-1-9921050101" id="btn-filter-APPROVED-9921050101" style="background-color:#E3F2FD; color: grey; margin-right: 0px" onclick="filterData('btn-filter-APPROVED-9921050101', 'btn-filter-1-9921050101', 1, 'APPROVED', 'grey', -1, false, 'fincashbank', 'AND', 'idstatustranc', '=', '3')">APPROVED </button><div class="btn" style="padding: 0px; width: 5px"></div></div> -->
                    <!-- </div> -->
                </div>
                <div id="appendInfoDatatable" style="border-top: 0px solid #3598dc; border-right: 0px solid #3598dc; border-left: 0px solid #3598dc;">
                </div>
                <!-- <div id="appendButtonDatatable"></div> -->
                <div class="table-scrollable" style="border: 0px solid #3598dc; margin-top: 0px !important">
                    <table bg-color="blue" class="nowrap table table-striped table-bordered table-hover  dataTable no-footer" style="background-color:#BBDEFB !important; margin: 0 !important; border: 0px solid #3598dc" id="table_master"  aria-describedby="table_master_info" role="grid">
                        <thead id="thead_pagepanel_9921050101">
                            <tr id="thead-columns-d" style="background: #BBDEFC">
                                <th style="width: 30px;">
                                    <input type="checkbox" id="checkbox-m-all" onchange="selectCheckboxMAll()" class="checkbox-m" name="checkbox-m-all" value="FALSE">
                                </th>
                                <th>No</th>
                                <!-- <th>Noid</th> -->
                                <!-- <th>Type Tranc.</th> -->
                                <th>Status</th>
                                <!-- <th>ID Status Card</th> -->
                                <!-- <th>Do Status</th> -->
                                <th>Kode</th>
                                <th>Kode SI</th>
                                <th>Tanggal</th>
                                <th>Tanggal Due</th>
                                <!-- <th>Sub Total</th> -->
                                <th>Nilai Pekerjaan</th>
                                <th>Nama</th>
                                <th>Customer</th>
                                <th>Marketing</th>
                                <!-- <th>Company</th>
                                <th>Department</th> -->
                                <!-- <th>Gudang</th> -->
                                <th>Action</th>
                                <!-- <th>ID Card Supplier</th>
                                <th>ID Akun Supplier</th>
                                <th>Supplier Nama</th>
                                <th>Supplier Address 1</th>
                                <th>Supplier Address 2</th>
                                <th>Supplier Address RT</th>
                                <th>Supplier Address RW</th>
                                <th>Supplier Lokasi Neg</th>
                                <th>Supplier Lokasi Prop</th>
                                <th>Supplier Lokasi Kab</th>
                                <th>Supplier Lokasi Kec</th>
                                <th>Supplier Lokasi Kel</th>
                                <th>ID Card Delivery</th>
                                <th>Delivery Name</th>
                                <th>Delivery Vehicle</th>
                                <th>Delivery Vehicle Plat</th>
                                <th>Delivery Tanggal</th>
                                <th>Total QTY</th>
                                <th>Total Item</th>
                                <th>Total Subtotal</th>
                                <th>Total Disc</th>
                                <th>Total Tax</th>
                                <th>Disc Nota</th>
                                <th>Total Disc Nota</th>
                                <th>General Price</th>
                                <th>Biaya Lain</th>
                                <th>Kode Voucher</th>
                                <th>General Total</th>
                                <th>Total Bayar</th>
                                <th>Total Deposit</th>
                                <th>Total Hutang</th>
                                <th>Total Sisa Hutang</th>
                                <th>ID Type Payment</th>
                                <th>ID Cashbank Bayar</th>
                                <th>ID Akun Bayar</th>
                                <th>ID Akun Deposit</th>
                                <th>ID Akun Biaya Lain</th>
                                <th>ID Akun Hutang</th>
                                <th>ID Akun Purchase</th>
                                <th>ID Akun Persediaan</th>
                                <th>ID Akun Sales Return</th>
                                <th>ID Akun Discount</th>
                                <th>Keterangan</th>
                                <th>ID Currency</th>
                                <th>Konv Curr</th>
                                <th>ID Gudang</th>
                                <th>ID Cost Control</th>
                                <th>No Urut</th>
                                <th>ID Company</th>
                                <th>ID Department</th>
                                <th>ID Type Tranc Prev</th>
                                <th>ID Tranc Prev</th>
                                <th>ID Type Tranc Order</th>
                                <th>ID Tranc Order</th>
                                <th>ID Create</th>
                                <th>Do Create</th>
                                <th>ID Update</th>
                                <th>Last Update</th> -->
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <!-- <th></th> -->
                                <th></th>
                                <th></th>
                                <th></th>
                                <th class="tfoot-kode"></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <!-- <th></th> -->
                                <!-- <th></th>
                                <th></th> -->
                                <!-- <th></th> -->
                                <th></th>
                                <!-- <th class="text-right" colspan="2" style="border: 1px solid #95c9ed"></th> -->
                            </tr>
                        </tfoot>
                    </table>
                </div>

                <!-- <div class="table-scrollable" style="border: 1px solid #3598dc;">
                    <table id="example_baru" class="table table-striped table-bordered table-hover nowrap dataTable no-footer" style="background-color: #BBDEFB; border-collapse: collapse;">
                        <thead>
                            <tr id="thead" class="bg-main"></tr>
                            <tr id="thead-columns"></tr>
                        </thead>
                        <tfoot>
                            <tr id="tfoot-columns">
                            </tr>
                        </tfoot>
                    </table>
                </div> -->
            </div>
        </div>
    </div>

    <br>
    <!-- <div class="card card-custom flat">
        <div class="card-body flat">
            <div class="row" id="another-form">
            </div>
        </div>
    </div> -->

    <div id="modal-delete" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-filter_ modal-dialog modal-lg">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="hideModalDelete()"></a>
                    <h4 class="modal-title" style="padding-left: 12px">Are you sure you want to delete this?</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-top: 8px">
                        <div class="col-md-12" style="padding: 0px">
                            <div class="card" style="margin: 0px 0px 5px 1px !important">
                                <div class="card-body">
                                    <form id="">
                                        <div class="row text-center">
                                            <div class="col-md-12">
                                                <button type="button" id="remove-data" class="btn btn-success" style="margin-left: 2px"><i class="fa fa-check"></i> Yes</button>
                                                <button type="button" class="btn btn-default" style="margin-left: 2px" onclick="hideModalDelete()"><i class="fa fa-times"></i> No</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="modal-send-to-next" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-filter_ modal-dialog modal-lg">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="hideModalSendToOrder()"></a>
                    <h4 class="modal-title" style="padding-left: 12px">Send to Purchase Offer</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-top: 8px">
                        <div class="col-md-12" style="padding: 0px">
                            <div class="card" style="margin: 0px 0px 5px 1px !important">
                                <div class="card-body">
                                    <form id="form-modal-stn">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">User</label>
                                                    <div class="col-md-8">
                                                        <input type="hidden" id="idtypetranc-stn">
                                                        <input type="text" class="form-control" value="{{$sessionname}}" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Supplier</label>
                                                    <div class="col-md-8">
                                                        <select class="form-control select2" name="idcardsupplier-stn" id="idcardsupplier-stn" onchange="changeCardSupplier('idcardsupplier', 'idcardsupplier-modal')">
                                                            <option value="">[Choose Supplier]</option>
                                                            @foreach($mcardsupplier as $v)
                                                                <option 
                                                                    value="{{$v->mcardsupplier_noid}}" 
                                                                    data-mlokasiprop-noid="{{$v->mlokasiprop_noid}}" 
                                                                    data-mlokasiprop-nama="{{$v->mlokasiprop_nama}}" 
                                                                    data-mlokasikab-noid="{{$v->mlokasikab_noid}}" 
                                                                    data-mlokasikab-nama="{{$v->mlokasikab_nama}}" 
                                                                    data-mlokasikec-noid="{{$v->mlokasikec_noid}}" 
                                                                    data-mlokasikec-nama="{{$v->mlokasikec_nama}}" 
                                                                    data-mlokasikel-noid="{{$v->mlokasikel_noid}}" 
                                                                    data-mlokasikel-nama="{{$v->mlokasikel_nama}}" 
                                                                >{{$v->mcardsupplier_nama}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4"></div>
                                            <div class="col-md-8">
                                                <button type="button" class="btn btn-success" id="send-to-next" style="margin-left: 2px"><i class="fa fa-save"></i> <span>Send</span></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-snst" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-filter_ modal-dialog modal-lg">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="hideModalSNST()"></a>
                    <h4 class="modal-title" style="padding-left: 12px">SET NEXT STATUS TRANC</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-top: 8px">
                        <div class="col-md-12" style="padding: 0px">
                            <div class="card" style="margin: 0px 0px 5px 1px !important">
                                <div class="card-body">
                                    <form id="form-modal-snst">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">User</label>
                                                    <div class="col-md-8">
                                                        <input type="hidden" id="idtypetranc-snst">
                                                        <input type="text" class="form-control" value="{{$sessionname}}" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" id="div-spknumber" style="display: none">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Nomor SPK</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="spknumber" id="spknumber-snst" class="form-control" placeholder="Nomor SPK">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Keterangan</label>
                                                    <div class="col-md-8">
                                                        <textarea name="keterangan-modal" id="keterangan-snst" cols="3" rows="5" class="form-control" placeholder="Keterangan"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4"></div>
                                            <div class="col-md-8">
                                                <button type="button" class="btn btn-success" id="btn-snst" style="margin-left: 2px"><i class="fa fa-save"></i> <span>Set Next</span></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modal-div" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-filter_ modal-dialog modal-lg">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="hideModalSAPCF()"></a>
                    <h4 class="modal-title" style="padding-left: 12px">SET STATUS TRANC</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-top: 8px">
                        <div class="col-md-12" style="padding: 0px">
                            <div class="card" style="margin: 0px 0px 5px 1px !important">
                                <div class="card-body">
                                    <form id="form-modal-div">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">User</label>
                                                    <div class="col-md-8">
                                                        <input type="hidden" id="idtypetranc-div">
                                                        <input type="text" class="form-control" value="{{$sessionname}}" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Status Tranc.</label>
                                                    <div class="col-md-8" id="select-div">
                                                        <select class="form-control select2" name="idstatustranc-div" id="idstatustranc-div">
                                                            <option value="">[Choose Status]</option>
                                                            @foreach($mtypetranctypestatus as $v)
                                                                @if($v->noid > 0 && $v->noid <= 3)
                                                                    <option 
                                                                        value="{{$v->noid}}"
                                                                        data-mtypetranctypestatus-nama="{{$v->nama}}"
                                                                    >{{$v->nama}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Keterangan</label>
                                                    <div class="col-md-8">
                                                        <textarea name="keterangan-div" id="keterangan-div" cols="3" rows="5" class="form-control" placeholder="Keterangan"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4"></div>
                                            <div class="col-md-8">
                                                <button type="button" class="btn btn-success" id="btn-div" style="margin-left: 2px"><i class="fa fa-save"></i> <span>Set Status</span></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-sapcf" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-filter_ modal-dialog modal-lg">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="hideModalSAPCF()"></a>
                    <h4 class="modal-title" style="padding-left: 12px">SET STATUS TRANC</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-top: 8px">
                        <div class="col-md-12" style="padding: 0px">
                            <div class="card" style="margin: 0px 0px 5px 1px !important">
                                <div class="card-body">
                                    <form id="form-modal-sapcf">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">User</label>
                                                    <div class="col-md-8">
                                                        <input type="hidden" id="idtypetranc-sapcf">
                                                        <input type="text" class="form-control" value="{{$sessionname}}" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Status Tranc. <span class="text-danger">*</span></label>
                                                    <div class="col-md-8">
                                                        <select class="form-control select2" name="idstatustranc-sapcf" id="idstatustranc-sapcf">
                                                            <option value="">[Choose Status]</option>
                                                            @foreach($mtypetranctypestatus as $v)
                                                                @if($v->isinternal == 0 && $v->isexternal != 0)
                                                                    <option 
                                                                        value="{{$v->noid}}"
                                                                        data-mtypetranctypestatus-nama="{{$v->nama}}"
                                                                    >{{$v->nama}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Keterangan <span class="text-danger">*</span></label>
                                                    <div class="col-md-8">
                                                        <textarea name="keterangan-sapcf" id="keterangan-sapcf" cols="3" rows="5" class="form-control" placeholder="Keterangan"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4"></div>
                                            <div class="col-md-8">
                                                <button type="button" class="btn btn-success" id="btn-sapcf" style="margin-left: 2px"><i class="fa fa-save"></i> <span>Set Status</span></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-set-pending" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-filter_ modal-dialog modal-lg">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="closeModalFilter()"></a>
                    <h4 class="modal-title" style="padding-left: 12px">SET PENDING</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-top: 8px">
                        <div class="col-md-12" style="padding: 0px">
                            <div class="card" style="margin: 0px 0px 5px 1px !important">
                                <div class="card-body">
                                    <form id="form-modal-set-pending">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">User</label>
                                                    <div class="col-md-8">
                                                        <input type="hidden" id="idtypetranc-pending">
                                                        <input type="text" class="form-control" value="{{$sessionname}}" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Keterangan</label>
                                                    <div class="col-md-8">
                                                        <textarea name="keterangan-modal" id="keterangan-pending" cols="3" rows="5" class="form-control" placeholder="Keterangan"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4"></div>
                                            <div class="col-md-8">
                                                <button type="button" class="btn btn-success" id="set-pending" style="margin-left: 2px"><i class="fa fa-save"></i> Set Pending</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-set-canceled" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-filter_ modal-dialog modal-lg">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="closeModalFilter()"></a>
                    <h4 class="modal-title" style="padding-left: 12px">SET CANCELED</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-top: 8px">
                        <div class="col-md-12" style="padding: 0px">
                            <div class="card" style="margin: 0px 0px 5px 1px !important">
                                <div class="card-body">
                                    <form id="form-modal-set-canceled">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">User</label>
                                                    <div class="col-md-8">
                                                        <input type="hidden" id="idtypetranc-canceled">
                                                        <input type="text" class="form-control" value="{{$sessionname}}" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Keterangan</label>
                                                    <div class="col-md-8">
                                                        <textarea name="keterangan-modal" id="keterangan-canceled" cols="3" rows="5" class="form-control" placeholder="Keterangan"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4"></div>
                                            <div class="col-md-8">
                                                <button type="button" class="btn btn-success" id="set-canceled" style="margin-left: 2px"><i class="fa fa-save"></i> Set Canceled</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-log" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-filter modal-dialog modal-lg">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="hideModalLog()"></a>
                    <h4 class="modal-title" style="padding-left: 12px">Log: <span id="titlelog-log"></span>, Kode: <span id="titlekode-log"></span></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-top: 8px">
                        <div class="col-md-12" style="padding: 0px">
                            <div class="card" style="margin: 0px 0px 5px 1px !important">
                                <div class="card-body">
                                    <div id="appendInfoDatatableLog" style="border-top: 2px solid #3598dc; border-right: 3px solid #3598dc; border-left: 2px solid #3598dc;">
                                    </div>
                                    <div id="ts-9921050102" class="table-scrollable" style="border: 1px solid #3598dc; background-color: #3598dc; margin-top: 0px !important">
                                        <table bg-color="blue" class="nowrap table table-striped table-bordered table-hover  dataTable no-footer" id="table-log" style="background-color:#BBDEFB !important; margin: 0 !important; border: 1px solid #3598dc"  aria-describedby="table_master_info" role="grid">
                                            <thead>
                                                <!-- <tr id="tab-thead-9921050102" class="bg-main"><th style="text-align:center" colspan="2">#</th><th style="text-align:center" colspan="26"></th><th colspan="1" style="text-align:center">---</th></tr> -->
                                                <tr id="tab-thead-columns-d-9921050102">
                                                    <th>---</th>
                                                    <th>No</th>
                                                    <th>Type Action</th>
                                                    <th>Log Subject</th>
                                                    <th>Keterangan</th>
                                                    <th>By</th>
                                                    <th>On</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-costcontrol" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-filter modal-dialog modal-lg">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="hideModalCostControl()"></a>
                    <h4 class="modal-title" style="padding-left: 12px">COST CONTROL</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-top: 8px">
                        <div class="col-md-12" style="padding: 0px">
                            <div class="card" style="margin: 0px 0px 5px 1px !important">
                                <div class="card-body">
                                    <div id="appendInfoDatatableCostControl" style="border-top: 2px solid #3598dc; border-right: 3px solid #3598dc; border-left: 2px solid #3598dc;">
                                    </div>
                                    <div id="ts-9921050102" class="table-scrollable" style="border: 1px solid #3598dc; background-color: #3598dc; margin-top: 0px !important">
                                        <table bg-color="blue" class="nowrap table table-striped table-bordered table-hover  dataTable no-footer" id="table-rab" style="background-color:#BBDEFB !important; margin: 0 !important; border: 1px solid #3598dc"  aria-describedby="table_master_info" role="grid">
                                            <thead>
                                                <tr id="tab-thead-columns-d-9921050102">
                                                    <th style="width: 30px;">
                                                        <input type="checkbox" id="checkbox-cc-all" onchange="selectAllCheckboxCC()" class="checkbox-cc" name="checkbox-cc-all" value="FALSE">
                                                    </th>
                                                    <th>No</th>
                                                    <th>Kode</th>
                                                    <th>Type Cost Control</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th class="text-right" colspan="5" style="border: 1px solid #95c9ed"></th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-detailprev" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-filter modal-dialog modal-lg">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="hideModalDetailPrev()"></a>
                    <h4 class="modal-title" id="title-prev" style="padding-left: 12px">PREVIEOUS: PURCHASE REQUEST DETAIL, SUPPLIER: <span class="supplier-title"></span></h4>
                    <h4 class="modal-title" id="title-inv" style="padding-left: 12px">INVENTORY</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-top: 8px">
                        <div class="col-md-12" style="padding: 0px">
                            <div class="card" style="margin: 0px 0px 5px 1px !important">
                                <div class="card-body">
                                    <div id="appendInfoDatatableDetailPrev" style="width: 100%; border-top: 2px solid #3598dc; border-right: 3px solid #3598dc; border-left: 2px solid #3598dc;">
                                    </div>
                                    <div id="ts-9921050102" class="table-scrollable" style="border: 1px solid #3598dc; background-color: #3598dc; margin-top: 0px !important">
                                        <table bg-color="blue" class="nowrap table table-striped table-bordered table-hover  dataTable no-footer" id="table-detailprev" style="background-color:#BBDEFB !important; margin: 0 !important; border: 1px solid #3598dc"  aria-describedby="table_master_info" role="grid">
                                            <thead>
                                                <!-- <tr id="tab-thead-9921050102" class="bg-main"><th style="text-align:center" colspan="2">#</th><th style="text-align:center" colspan="26"></th><th colspan="1" style="text-align:center">---</th></tr> -->
                                                <tr id="tab-thead-columns-d-9921050102">
                                                    <th style="width: 30px;">
                                                        <input type="checkbox" id="select_all_9921050102" onchange="selectAllCheckboxTab(9921050102)" class="checkbox-9921050102 checkbox-9921050102-0" name="checkbox-9921050102[]" value="FALSE">
                                                    </th>
                                                    <th>No</th>
                                                    <th>Kode PR</th>
                                                    <th>Kode Inv.</th>
                                                    <th>Nama</th>
                                                    <th>Inventor</th>
                                                    <th>Keterangan</th>
                                                    <!-- <th>ID Gudang</th>
                                                    <th>ID Company</th>
                                                    <th>ID Department</th>
                                                    <th>ID Satuan</th>
                                                    <th>Konv Satuan</th> -->
                                                    <th id="thead-qty">Qty</th>
                                                    <th id="thead-qtysisa">Qty Sisa</th>
                                                    <th>Unit</th>
                                                    <th id="thead-price">Price</th>
                                                    <th id="thead-total">Total</th>
                                                    <th>Currency</th>
                                                    <!-- <th>Subtotal</th>
                                                    <th>Discount Var</th>
                                                    <th>Discount</th>
                                                    <th>Nilai Disc</th>
                                                    <th>ID Type Tax</th>
                                                    <th>ID Tax</th> -->
                                                    <!-- <th>Harga Total</th> -->
                                                    <!-- <th>ID Akun HPP</th>
                                                    <th>ID Akun Sales</th>
                                                    <th>ID Akun Persediaan</th>
                                                    <th>ID Akun Sales Return</th>
                                                    <th>ID Akun Purchase Return</th>
                                                    <th>Serial Number</th>
                                                    <th>Garansi Tanggal</th>
                                                    <th>Garansi Expired</th> -->
                                                    <!-- <th>Type Tranc Prev</th>
                                                    <th>Tranc Prev</th>
                                                    <th>Tranc Prev D</th>
                                                    <th>Type Tranc Order</th>
                                                    <th>Tranc Order</th>
                                                    <th>Tranc Order D</th> -->
                                                    <th id="th-action">Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th></th>
                                                    <th></th>
                                                    <th class="tfoot-kode"></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <!-- <th class="text-right" colspan="2" style="border: 1px solid #95c9ed"></th> -->
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div id="modal-tanggal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-filter_ modal-dialog modal-lg">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="hideModalTanggal()"></a>
                    <h4 class="modal-title" style="padding-left: 12px">DETAIL TANGGAL</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-top: 8px">
                        <div class="col-md-12" style="padding: 0px">
                            <div class="card" style="margin: 0px 0px 5px 1px !important">
                                <div class="card-body">
                                    <form id="form-modal-tanggal">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Tanggal</label>
                                                    <div class="col-md-8">
                                                        <input type="text" value="{{$defaulttanggal}}" id="tanggal-modal" class="form-control datepicker" placeholder="Tanggal" autocomplete="off" onclick="changeTanggal('tanggal-modal', 'tanggal')" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Tanggal Tax</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="tanggaltax" id="tanggaltax-modal" class="form-control datepicker" placeholder="Tanggal Tax" autocomplete="off">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Tanggal Due</label>
                                                    <div class="col-md-8">
                                                        <input type="text" value="{{$defaulttanggaldue}}" id="tanggaldue-modal" class="form-control datepicker" placeholder="Tanggal Due" autocomplete="off" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-supplier" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-filter_ modal-dialog modal-lg">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="hideModalSupplier()"></a>
                    <h4 class="modal-title" style="padding-left: 12px">DETAIL SUPPLIER</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-top: 8px">
                        <div class="col-md-12" style="padding: 0px">
                            <div class="card" style="margin: 0px 0px 5px 1px !important">
                                <div class="card-body">
                                    <form id="form-modal-supplier">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Card Supplier</label>
                                                    <div class="col-md-8">
                                                        <input type="text" value="{{@$lastsupplier->suppliernama}}" id="idcardsupplier-modal" class="form-control" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Akun Supplier</label>
                                                    <div class="col-md-8">
                                                        <select class="form-control select2" name="idakunsupplier" id="idakunsupplier-modal">
                                                            @foreach($accmkodeakun as $v)
                                                                <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Supplier Nama</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="suppliernama" id="suppliernama-modal" class="form-control" placeholder="Supplier Nama">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Supplier Addr. 1</label>
                                                    <div class="col-md-8">
                                                        <textarea name="supplieraddress1" id="supplieraddress1-modal" cols="3" rows="3" class="form-control" placeholder="Supplier Address 1"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Supplier Addr. 2</label>
                                                    <div class="col-md-8">
                                                        <textarea name="supplieraddress2" id="supplieraddress2-modal" cols="3" rows="3" class="form-control" placeholder="Supplier Address 2"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">RT</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="supplieraddressrt" id="supplieraddressrt-modal" class="form-control" placeholder="RT">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">RW</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="supplieraddressrw" id="supplieraddressrw-modal" class="form-control" placeholder="RW">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Kelurahan</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="supplierlokasikel" id="supplierlokasikel-modal" class="form-control" placeholder="Kelurahan">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Kecamatan</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="supplierlokasikec" id="supplierlokasikec-modal" class="form-control" placeholder="Kecamatan">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Kabupaten</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="supplierlokasikab" id="supplierlokasikab-modal" class="form-control" placeholder="Kabupaten">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Propinsi</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="supplierlokasiprop" id="supplierlokasiprop-modal" class="form-control" placeholder="Propinsi">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Negara</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="supplierlokasineg" id="supplierlokasineg-modal" class="form-control" placeholder="Negara">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-detail" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-detail modal-dialog modal-lg">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="hideModalDetail()"></a>
                    <h4 class="modal-title" style="padding-left: 12px">DETAIL DETAIL</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-top: 8px">
                        <div class="col-md-12" style="padding: 0px">
                            <div class="card" style="margin: 0px 0px 5px 1px !important">
                                <div class="card-body">
                                    <form id="form-modal-detail">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label">Inventor</label>
                                                    <div class="col-md-5">
                                                        <select class="form-control select2" name="idinventor" id="idinventor-modal" onchange="changeKodeInventorModal()">
                                                            <option value="">[Choose Inventor]</option>
                                                            @foreach($invminventory as $v)
                                                                <option 
                                                                    value="{{$v->noid}}"
                                                                    data-invminventory-kode="{{$v->kode}}"
                                                                    data-invminventory-nama="{{$v->nama}}"
                                                                    data-invminventory-idsatuan="{{$v->idsatuan}}"
                                                                    data-invminventory-konversi="{{$v->konversi}}"
                                                                    data-invminventory-idtax="{{$v->idtax}}"
                                                                    data-invminventory-taxprocent="{{$v->taxprocent}}"
                                                                    data-invminventory-purchaseprice="{{$v->purchaseprice}}"
                                                                >{{$v->kode}} - {{$v->nama}} - {{$v->purchaseprice2}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="text" id="namainventorori-modal" class="form-control" style="background-color: #E9ECEF !important;" placeholder="Nama Inventor" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label"></label>
                                                    <div class="col-md-9">
                                                        <input type="text" name="namainventor" id="namainventor-modal" class="form-control" placeholder="Nama Inventor">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label">Keterangan</label>
                                                    <div class="col-md-9">
                                                        <textarea name="keterangan" id="keterangan-modal" cols="3" rows="6" class="form-control" placeholder="Keterangan"></textarea>
                                                    </div>
                                                </div>
                                                <!-- <div class="form-group row">
                                                    <label class="col-md-3 col-form-label">Company / Dept.</label>
                                                    <div class="col-md-4">
                                                        <select class="form-control idcompany" style="background-color: #E9ECEF !important;" name="idcompany" id="idcompany-modal" readonly>
                                                            <option value="" style="display:none">[Choose Company]</option>
                                                            @foreach($genmcompany as $v)
                                                                <option value="{{$v->noid}}" style="display:none">{{$v->kode}} - {{$v->nama}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <select class="form-control iddepartment" style="background-color: #E9ECEF !important;" name="iddepartment" id="iddepartment-modal" readonly>
                                                            <option value="" style="display:none">[Choose Department]</option>
                                                            @foreach($genmdepartment as $v)
                                                                <option value="{{$v->noid}}" style="display:none">{{$v->kode}} - {{$v->nama}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div> -->
                                                <div class="form-group row" style="display: none">
                                                    <label class="col-md-3 col-form-label">Gudang</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control" style="background-color: #E9ECEF !important;" name="idgudang" id="idgudang-modal" readonly>
                                                            <option value="" style="display:none">[Choose Gudang]</option>
                                                            @foreach($genmgudang as $v)
                                                                <option value="{{$v->noid}}" style="display:none">{{$v->kode}} - {{$v->namalias}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group row" style="display: none">
                                                    <label class="col-md-3 col-form-label">ID Currency</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control select2" id="idcurrency-modal">
                                                            <option value="">[Choose Currency]</option>
                                                            @foreach($accmcurrency as $v)
                                                                <option 
                                                                    value="{{$v->noid}}" 
                                                                    id="idcurrency-{{$v->noid}}"
                                                                    data-accmcurrency-nama="{{$v->nama}}" 
                                                                    <?= $v->noid == 1 ? 'selected' : '' ?>
                                                                >{{$v->kode}} - {{$v->nama}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row" style="display: none">
                                                    <label class="col-md-4 col-form-label">Satuan / Konv.</label>
                                                    <div class="col-md-6">
                                                        <select class="form-control select2" name="idsatuan" id="idsatuan-modal">
                                                            <option value="" >[Choose Satuan]</option>
                                                            @foreach($invmsatuan as $v)
                                                                <option 
                                                                    value="{{$v->noid}}"
                                                                    data-invmsatuan-nama="{{$v->nama}}"
                                                                    data-invmsatuan-konversi="{{$v->konversi}}"
                                                                >{{$v->nama}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <!-- <input type="text" name="idsatuan" id="idsatuan-modal" class="form-control" placeholder="ID Satuan"> -->
                                                        <input type="text" name="konvsatuan" id="konvsatuan-modal" class="form-control" style="background-color: #E9ECEF !important;" placeholder="Konv Satuan" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group row" style="display: none">
                                                    <label class="col-md-4 col-form-label">Unit Qty / Sisa</label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="unitqty" id="unitqty-modal" class="form-control unitqty" placeholder="Unit Qty">
                                                    </div><div class="col-md-4">
                                                        <input type="text" name="unitqtysisa" id="unitqtysisa-modal" class="form-control" style="background-color: #E9ECEF !important;" placeholder="Unit Qty Sisa" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group row" style="display: none">
                                                    <label class="col-md-4 col-form-label">Unit Price</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="unitprice" id="unitprice-modal" class="form-control input-mask" placeholder="Unit Price">
                                                    </div>
                                                </div>
                                                <div class="form-group row" style="display: none">
                                                    <label class="col-md-4 col-form-label">Subtotal</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="subtotal" id="subtotal-modal" class="form-control input-mask" style="background-color: #E9ECEF !important;" placeholder="Subtotal" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group row" style="display: none">
                                                    <label class="col-md-4 col-form-label">Discount Var</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="discountvar" id="discountvar-modal" class="form-control" placeholder="Discount Var">
                                                    </div>
                                                </div>
                                                <div class="form-group row" style="display: none">
                                                    <label class="col-md-4 col-form-label">Discount / Nilai</label>
                                                    <div class="col-md-3">
                                                        <div class="input-group">
                                                            <input type="text" name="discount" id="discount-modal" class="form-control" placeholder="Discount">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">%</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <input type="text" name="nilaidisc" id="nilaidisc-modal" class="form-control input-mask" style="background-color: #E9ECEF !important;" placeholder="Nilai Disc" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group row" style="display: none">
                                                    <label class="col-md-4 col-form-label">ID Type Tax</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="idtypetax" id="idtypetax-modal" class="form-control" placeholder="ID Type Tax">
                                                    </div>
                                                </div>
                                                <div class="form-group row" style="display: none">
                                                    <label class="col-md-4 col-form-label">ID Tax / Prosentax</label>
                                                    <div class="col-md-5">
                                                        <select class="form-control select2" name="idtax" id="idtax-modal">
                                                            <option value="" data-accmpajak-prosentax="0">[Choose Tax]</option>
                                                            @foreach($accmpajak as $v)
                                                                <option 
                                                                    value="{{$v->noid}}"
                                                                    data-accmpajak-prosentax="{{$v->prosentax}}"
                                                                >{{$v->nama}}</option>
                                                            @endforeach
                                                        </select>
                                                        <!-- <input type="text" name="idtax" id="idtax-modal" class="form-control" placeholder="ID Tax"> -->
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="input-group">
                                                            <input type="text" name="prosentax" id="prosentax-modal" class="form-control" style="background-color: #E9ECEF !important;" placeholder="Prosentax" readonly>
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">%</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row" style="display: none">
                                                    <label class="col-md-4 col-form-label">Nilai Tax</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="nilaitax" id="nilaitax-modal" class="form-control input-mask" style="background-color: #E9ECEF !important;" placeholder="Nilai Tax" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group row" style="display: none">
                                                    <label class="col-md-4 col-form-label">Harga Total</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="hargatotal" id="hargatotal-modal" class="form-control input-mask" style="background-color: #E9ECEF !important;" placeholder="Harga Total" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row pull-right">
                                            <div class="col-md-12">
                                                <button type="button" id="update-close" title="Close" class="btn btn-default mr-2" onclick="hideModalDetail()">
                                                    <i class="fa fa-times"></i> Close
                                                </button>
                                                <button type="button" id="update-detail" title="Update Detail" class="btn btn-success mr-2" onclick="updateDetail()">
                                                    <i class="fa fa-save"></i> Update
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-filter" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-filter modal-dialog">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="closeModalFilter()"></a>
                    <h4 class="modal-title">Config Datatable</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="formfilter">
                        <div class="row" id="filtercontent">
                            <div class="col-md-12">
                                <div class="portlet box blue-steel" style="margin: 8px;">
                                    <div class="portlet-title">
                                        <div class="caption" style="font-weight: bold">
                                            <i class="fa fa-filter"></i>
                                            Datatable Filter
                                        </div>
                                        <div class="actions">
                                            <a onclick="saveFilter()" href="javascript:;" class="btn green btn-sm">
                                                <i class="fa fa-save text-white"></i> Save
                                            </a>
                                            <a id="addgroup" href="javascript:;" class="btn yellow btn-sm">
                                                <i class="fa fa-indent text-white"></i> Add Group
                                            </a>
                                            <a id="delgroup" href="javascript:;" class="btn red btn-sm">
                                                <i class="fa fa-trash text-white"></i> Delete Group
                                            </a>
                                            <a onclick="addRow()" href="javascript:;" class="btn purple btn-sm">
                                                <i class="fa fa-list text-white"></i> Add Row
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row" style="margin:0;">
                                            <div class="col-md-12" style="padding: 0 0 15px 0;">
                                                <div id="filtercontentgroup">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin:0;">
                                            <div class="col-md-12">
                                                <div id="filtercontentbody">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal CKEditor -->
    <div id="modal-ckeditor" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-filter modal-dialog">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="closeModalFilter()"></a>
                    <h4 class="modal-title">[0] PHPSOURCE</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <textarea class="form-control" id="summary-ckeditor" name="summary-ckeditor"></textarea>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
@section('scripts')
<!-- <script src="//www.amcharts.com/lib/3/amcharts.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/serial.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/radar.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/pie.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/plugins/animate/animate.min.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/plugins/export/export.min.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/themes/light.js?v=7.0.6"></script> -->
<!-- <script src="https://cdn.amcharts.com/lib/4/core.js"></script> -->
<!-- <script src="https://cdn.amcharts.com/lib/4/charts.js"></script> -->
<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
<script src="{{ asset('js/pages/crud/forms/widgets/bootstrap-datepicker.js?v=7.0.6') }}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.3.1/js/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.21/pagination/input.js"></script>
<script  src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.0.2/js/bootstrap-datetimepicker.min.js"></script>
<!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script> -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-loading-overlay/2.1.7/loadingoverlay.min.js"></script>
<script src="{{ asset('js/jquery-currency.js') }}"></script>
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(".datepicker").datepicker({
        format: 'dd-M-yyyy',
        autoclose: true,
        container: '#pagepanel'
    });

    $(".datepicker2").datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true,
        container: '#pagepanel'
    });

    $('.select2').select2({
        dropdownParent: $('#pagepanel')
    });
    $('.select2').css('width', '100%');
    $('.select2 .selection .select2-selection').css('height', '31px');
    $('.select2 .selection .select2-selection .select2-selection__rendered').css('margin-top', '-6px');
    $('.select2 .selection .select2-selection .select2-selection__arrow').css('margin-top', '4px');

    $('.input-mask').inputmask('decimal', {
        'alias': 'numeric',
        'groupSeparator': ',',
        'autoGroup': true,
        // 'digits': 0,
        // 'radixPoint': ",",
        // 'digitsOptional': false,
        'allowMinus': false,
        'prefix': 'Rp.',
        'placeholder': '0'
    });

    CKEDITOR.replace('summary-ckeditor');
</script>
<script type="text/javascript">

    let main = {
        'table': 'salesorder',
        'tabledetail': 'salesorderdetail',
        'sendtonext': 'Send to Purchase Order',
        'urlcode': '_so_ctmd',
        'datefilterdefault': 'tm',
        'condition': {
            'create': {
                'c': true,
                'r': true,
                'u': true,
                'd': true,
                'v': false,
                'formdetail': true,
            },
            'read': {
                'c': true,
                'r': true,
                'u': true,
                'd': true,
                'v': true,
                'formdetail': true,
            },
            'update': {
                'c': true,
                'r': true,
                'u': true,
                'd': true,
                'v': false,
                'formdetail': true,
            },
            'delete': {
                'c': false,
                'r': false,
                'u': false,
                'd': true,
                'v': false,
                'formdetail': true,
            },
            'view': {
                'c': false,
                'r': true,
                'u': false,
                'd': false,
                'v': true,
                'formdetail': false,
            },
        }
    };

    let status = {
        'crudmaster': 'read',
        'cruddetail': 'read',
    };

    function conditionDetail() {
        main.condition[status.crudmaster]['formdetail'] ? $('#form-detail').show() : $('#form-detail').hide();
    }

    var statusgetlog = false;

    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        datefilter2.from = start.format('YYYY-MM-DD');
        datefilter2.to = end.format('YYYY-MM-DD');
        getDatatable();
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    var daterangepicker = $('#reportrange').daterangepicker({
        opened: true,
        // expanded: true,
        // standalone: true,
        // parentElement: document.getElementById('pagepanel'),
        // anchorElement: document.getElementById('pagepanel'),
        // container: '#pagepanel',
        startDate: start,
        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    console.log('daterangepicker')
    console.log(document.getElementById('pagepanel'))

    var qty_tr_detail = 0;

    function setTab(element, title) {
        if (title) {
            $('#tab-title').text(title);
        }
        $('#tab-form li a').removeClass('active');
        $('#tab-form li #'+element).addClass('active');
        $('#tab-content div').removeClass('show active');
        $('#tab-content #'+element+'-content').addClass('show active');
    }

    function changeTanggal(elfrom, elto) {
        $('#'+elto).val($('#'+elfrom).val())
    }

    function changeCardSupplier(elfrom, elto) {
        let idcardsupplier = $('#'+elfrom+' :selected');
        let prop = $('#'+elfrom+' :selected').attr('data-mlokasiprop-nama');
        let kab = $('#'+elfrom+' :selected').attr('data-mlokasikab-nama');
        let kec = $('#'+elfrom+' :selected').attr('data-mlokasikec-nama');
        let kel = $('#'+elfrom+' :selected').attr('data-mlokasikel-nama');

        $('#idcardsupplier-modal').val(idcardsupplier.val() ? idcardsupplier.text() : '');
        $('#suppliernama-modal').val(idcardsupplier.val() ? idcardsupplier.text() : '');
        $('#supplierlokasiprop-modal').val(prop);
        $('#supplierlokasikab-modal').val(kab);
        $('#supplierlokasikec-modal').val(kec);
        $('#supplierlokasikel-modal').val(kel);
        $('#idcardsupplier-modal').val(idcardsupplier.text())
    }

    function changeKodeInventor() {
        let idinventor = $('#idinventor :selected');
        $('#form-detail #namainventor').val(idinventor.val() ? idinventor.attr('data-invminventory-nama') : '');
        $('#form-detail #unitprice').val(idinventor.val() ? idinventor.attr('data-invminventory-purchaseprice') : '');
        $('#form-modal-detail #idinventor-modal').val(idinventor.val() ? idinventor.text() : '');
        $('#form-modal-detail #namainventor-modal').val(idinventor.val() ? idinventor.attr('data-invminventory-nama') : '');
    }

    function changeKodeInventorModal() {
        let idinventor = $('#idinventor-modal :selected');
        $('#form-modal-detail #namainventorori-modal').val(idinventor.val() ? idinventor.attr('data-invminventory-nama') : '');
        $('#form-modal-detail #namainventor-modal').val(idinventor.val() ? idinventor.attr('data-invminventory-nama') : '');
    }

    function changeNamaInventor() {
        let namainventor = $('#form-detail #namainventor').val();
        $('#form-modal-detail #namainventor-modal').val(namainventor);
    }

    // $('#form-header #idcardsupplier').on('change', function() {
    //     $('#form-modal-supplier #idcardsupplier-modal').val($('#form-header #idcardsupplier :selected').val()).trigger('change')
    // });
    // $('#form-modal-supplier #idcardsupplier-modal').on('custom', function(idcardsupplier) {
    //     $('#form-modal-supplier #idcardsupplier-modal').val(idcardsupplier)
    // });

    $('#form-header #idcompany').on('change', function() {
        $('#form-modal-detail #idcompany-modal').val($('#form-header #idcompany :selected').val()).trigger('change')
    });
    // $('#form-modal-detail #idcompany-modal').on('change', function() {
    //     $('#form-header #idcompany').val($('#form-modal-detail #idcompany-modal :selected').val()).trigger('change')
    // });

    $('#form-header #iddepartment').on('change', function() {
        $('#form-modal-detail #iddepartment-modal').val($('#form-header #iddepartment :selected').val()).trigger('change')
    });
    // $('#form-modal-detail #iddepartment-modal').on('change', function() {
    //     $('#form-header #iddepartment').val($('#form-modal-detail #iddepartment-modal :selected').val()).trigger('change')
    // });
    
    function changeUnitQty(elfrom, elto) {
        let unitqty = $('#'+elfrom).val();
        $('#'+elto).val(unitqty);
        $('#form-modal-detail [name="unitprice"]').val(13000);
        $('#form-modal-detail [name="hargatotal"]').val(unitqty*13000);
    }

    function chooseDetailPrev(
        noid,
        idmaster,
        kodeprev,
        idinventor,
        kodeinventor,
        namainventor,
        namainventor2,
        keterangan,
        idgudang,
        idcompany,
        iddepartment,
        idsatuan,
        namasatuan,
        konvsatuan,
        unitqty,
        unitqtysisa,
        unitprice,
        subtotal,
        discountvar,
        discount,
        nilaidisc,
        idtypetax,
        idtax,
        prosentax,
        nilaitax,
        hargatotal,
        namacurrency
    ) {
        $('#form-detail #noiddetail').val(noid);
        $('#form-detail #idmaster').val(idmaster);
        $('#form-detail #kodeprev').val(kodeprev);
        $('#form-detail #idinventor').val(idinventor).trigger('change');
        $('#form-detail #kodeinventor').val(kodeinventor).trigger('change');
        $('#form-detail #namainventor').val(namainventor);
        $('#form-detail #namainventor2').val(namainventor2);
        $('#form-detail #unitqtyoriginal').val(unitqtysisa);
        $('#form-detail #unitqty').val($('[name=searchtypeprev]:checked').val() == 1 ? unitqtysisa : '');
        $('#form-modal-detail #keterangandetail').val(keterangan);
        $('#form-modal-detail #keterangan-modal').val(keterangan);
        $('#form-modal-detail #idgudang-modal').val(idgudang).trigger('change');
        $('#form-modal-detail #idcompany-modal').val(idcompany).trigger('change');
        $('#form-modal-detail #iddepartment-modal').val(iddepartment).trigger('change');
        $('#form-modal-detail #idsatuan-modal').val(idsatuan).trigger('change');
        $('#form-modal-detail #unitprice-modal').val(unitprice);
        $('#form-modal-detail #subtotal-modal').val(subtotal);
        $('#form-modal-detail #discount-modal').val(discount);
        $('#form-modal-detail #nilaidisc-modal').val(nilaidisc);
        $('#form-modal-detail #idtax-modal').val(idtax).trigger('change');
        $('#form-modal-detail #prosentax-modal').val(prosentax);
        $('#form-modal-detail #nilaitax-modal').val(nilaitax);
        $('#form-modal-detail #hargatotal-modal').val(hargatotal);

        hideModalDetailPrev();
        // setTimeout(function(){ $('#form-detail div').slideDown(); }, 100);
        // $('#form-detail #unitqty').focus();

        setTimeout(function(){ $('#form-detail div').slideDown(100, function() {
            $('#form-detail #unitqty').focus();
        }); }, 100);
        return false;

        let detailprev = new Array();
        detailprev.push({name:'noid',value:noid});
        detailprev.push({name:'idmaster',value:idmaster});
        detailprev.push({name:'idinventor',value:idinventor});
        detailprev.push({name:'kodeinventor',value:kodeinventor});
        detailprev.push({name:'namainventor',value:namainventor});
        detailprev.push({name:'namainventor2',value:namainventor2});
        detailprev.push({name:'keterangan',value:keterangan});
        detailprev.push({name:'idgudang',value:idgudang});
        detailprev.push({name:'idcompany',value:idcompany});
        detailprev.push({name:'iddepartment',value:iddepartment});
        detailprev.push({name:'idsatuan',value:idsatuan});
        detailprev.push({name:'namasatuan',value:namasatuan});
        detailprev.push({name:'konvsatuan',value:konvsatuan});
        detailprev.push({name:'unitqty',value:unitqty});
        detailprev.push({name:'unitqtysisa',value:unitqtysisa});
        detailprev.push({name:'unitprice',value:unitprice});
        detailprev.push({name:'subtotal',value:subtotal});
        detailprev.push({name:'discountvar',value:discountvar});
        detailprev.push({name:'discount',value:discount});
        detailprev.push({name:'nilaidisc',value:nilaidisc});
        detailprev.push({name:'idtypetax',value:idtypetax});
        detailprev.push({name:'idtax',value:idtax});
        detailprev.push({name:'prosentax',value:prosentax});
        detailprev.push({name:'nilaitax',value:nilaitax});
        detailprev.push({name:'hargatotal',value:hargatotal});
        detailprev.push({name:'namacurrency',value:hargatotal});
        console.log(detailprev)

        let dataprd = localStorage.getItem(main.tabledetail);

        if (dataprd) {
            let datamerge = JSON.parse(dataprd);
            datamerge.push(detailprev);
            localStorage.setItem(main.tabledetail, JSON.stringify(datamerge));
        } else {
            let firstdata = new Array();
            firstdata.push(detailprev);
            localStorage.setItem(main.tabledetail, JSON.stringify(firstdata));
        }

        getDatatableDetailPrev()
        getDatatableDetail(localStorage.getItem(main.tabledetail))
        cancelDetail()
    }

    function saveDetail() {
        let noiddetail = $('#form-detail #noiddetail').val();
        let idmaster = $('#form-detail #idmaster').val();
        let kodeprev = $('#form-detail #kodeprev').val();
        let idinventor = $('#form-detail #idinventor').val();
        let kodeinventor = $('#form-detail #idinventor :selected').attr('data-invminventory-kode');
        let namainventor = $('#form-detail #idinventor :selected').attr('data-invminventory-nama');
        let namainventor2 = $('#form-detail #namainventor').val();
        let unitprice = parseInt($('#form-modal-detail #unitprice-modal').val().replace(/[^0-9]/gi, ''));
        let unitqtyoriginal = $('#form-detail #unitqtyoriginal').val();
        let unitqty = $('#form-detail #unitqty').val();
        let keterangan = $('#form-detail #keterangandetail').val();
        let subtotal = parseInt(unitqty)*parseInt(unitprice);
            
        let idcompany = $('#form-header #idcompany').val();
        let iddepartment = $('#form-header #iddepartment').val();
        let idgudang = $('#form-header #idgudang').val();
        let idcurrency = $('#form-header #idcurrency :selected').val();
        let namacurrency = $('#form-header #idcurrency :selected').attr('data-accmcurrency-nama');

        // let keterangan = $('#form-modal-detail #keterangan-modal').val();
        let idsatuan = $('#form-modal-detail #idsatuan-modal :selected').val();
        let namasatuan = $('#form-modal-detail #idsatuan-modal :selected').attr('data-invmsatuan-nama');
        let konvsatuan = $('#form-detail #idinventor :selected').attr('data-invminventory-konversi');
        let unitqtysisa = unitqty;
        let discountvar = 0;
        let discount = 0;
        let nilaidisc = 0;
        let idtypetax = 0;
        let idtax = $('#form-detail #idinventor :selected').attr('data-invminventory-idtax');
        let prosentax = $('#form-detail #idinventor :selected').attr('data-invminventory-taxprocent')
        let nilaitax = parseInt(subtotal)*parseInt(prosentax)/100;
        let hargatotal = parseInt(subtotal)-parseInt(nilaidisc)+parseInt(nilaitax);

        let dataprd = localStorage.getItem(main.tabledetail);
        let formdetail = new Array();
        formdetail.push({name:"noid",value:noiddetail});
        formdetail.push({name:"kodeprev",value:kodeprev});
        formdetail.push({name:"idinventor",value:idinventor});
        formdetail.push({name:"kodeinventor",value:kodeinventor});
        formdetail.push({name:"namainventor",value:namainventor});
        formdetail.push({name:"namainventor2",value:namainventor2});
        formdetail.push({name:"keterangan",value:keterangan});
        let formheader = new Array();
        // formheader.push({name:"idcompany",value:idcompany});
        // formheader.push({name:"iddepartment",value:iddepartment});
        formheader.push({name:"idgudang",value:idgudang});
        formheader.push({name:"idcurrency",value:idcurrency});
        formheader.push({name:"namacurrency",value:namacurrency});
        let others = new Array();
        others.push({name:"keterangan",value:keterangan});
        others.push({name:"idsatuan",value:idsatuan});
        others.push({name:"namasatuan",value:namasatuan});
        others.push({name:"konvsatuan",value:konvsatuan});
        others.push({name:"unitqty",value:unitqty});
        others.push({name:"unitqtysisa",value:unitqtysisa});
        others.push({name:"unitprice",value:currency(unitprice)});
        others.push({name:"subtotal",value:currency(subtotal)});
        others.push({name:"discountvar",value:discountvar});
        others.push({name:"discount",value:discount});
        others.push({name:"nilaidisc",value:nilaidisc});
        others.push({name:"idtypetax",value:idtypetax});
        others.push({name:"idtax",value:idtax});
        others.push({name:"prosentax",value:prosentax});
        others.push({name:"nilaitax",value:nilaitax});
        others.push({name:"hargatotal",value:currency(hargatotal)});
        // others.push({name:"idtypetrancprev",value:502});
        // others.push({name:"idtrancprev",value:idmaster});
        // others.push({name:"idtrancprevd",value:noiddetail});
        // others.push({name:"unitqtysisaprev",value:parseInt(unitqtyoriginal)});
        let allprd = formdetail.concat(formheader, others);
        // console.log(formheader);return false;

        let condition = {
            status: '',
            message: {
                title: '',
                text: ''
            },
            type: '',
        };
        if (unitqty == '') {
            condition.status = 'notif';
            condition.message.title = 'Complete data, please!';
            condition.type = 'warning';
        } else if (parseInt(unitqty) > parseInt(unitqtyoriginal)) {
            condition.status = 'confirm';
            condition.message.title = 'Qty Updated melebihi Qty Sisa!';
            condition.message.text = 'Apakah ingin tetap melanjutkan transaksi?';
            condition.type = 'warning';
        } else if (parseInt(unitqty) < 1) {
            condition.status = 'notif';
            condition.message.title = 'Qty Updated minimal 1!'
            condition.type = 'warning';
        } else if (isNaN(parseInt(unitqty))) {
            condition.status = 'notif';
            condition.message.title = 'Qty Updated harus angka!'
            condition.type = 'warning';
        } else {
            if (dataprd) {
                let datamerge = JSON.parse(dataprd);
                datamerge.push(allprd);
                // datamerge[noiddetail] = allprd;
                localStorage.setItem(main.tabledetail, JSON.stringify(datamerge));
            } else {
                let firstdata = new Array();
                firstdata.push(allprd);
                // firstdata[noiddetail] = allprd;
                localStorage.setItem(main.tabledetail, JSON.stringify(firstdata));
            }
            $('#searchprev').focus();
            $('#form-detail div').slideUp();
            getDatatableDetail(localStorage.getItem(main.tabledetail))
            cancelDetail()
        }
            console.log(condition);
        
        if (condition.status == 'confirm') {
            Swal.fire({
                title: condition.message.title,
                text: condition.message.text,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                target: '#pagepanel'
            }).then((result) => {
                if (result.isConfirmed) {        
                    
                    // qty_tr_detail = qty_tr_detail+1;
                    // let tr;

                    if (dataprd) {
                        let datamerge = JSON.parse(dataprd);
                        datamerge.push(allprd);
                        // datamerge[noiddetail] = allprd;
                        localStorage.setItem(main.tabledetail, JSON.stringify(datamerge));
                    } else {
                        let firstdata = new Array();
                        firstdata.push(allprd);
                        // firstdata[noiddetail] = allprd;
                        localStorage.setItem(main.tabledetail, JSON.stringify(firstdata));
                    }
                    $('#searchprev').focus();
                    $('#form-detail div').slideUp();
                    getDatatableDetail(localStorage.getItem(main.tabledetail))
                    cancelDetail()
                    return false;
                }
            })
            // if (!confirm(condition.message)) {
                // return false;
            // }
        } else if (condition.status == 'notif') {
            $.notify({
                message: condition.message.title
            },{
                type: condition.type
            });
            return false;
        }
    }

    function updateDetail() {
        let formmodaldetail = $('#form-modal-detail').serializeArray();
        let id = $('#noiddetail').val();
        let data = new Array();

        console.log(formmodaldetail)
        $.each(JSON.parse(localStorage.getItem(main.tabledetail)), function(k, v) {
            if (id == k) {
                let record = new Array();
                $.each(formmodaldetail, function(k2, v2) {
                    record.push({name:'kodeprev',value:'-'})
                    if (v2.name == 'namainventor') {
                        record.push({name:'kodeinventor',value:$('#form-modal-detail #idinventor-modal :selected').attr('data-invminventory-kode')})
                        record.push({name:'namainventor',value:$('#form-modal-detail #idinventor-modal :selected').attr('data-invminventory-nama')})
                        record.push({name:'namainventor2',value:v2.value})
                    } else if (v2.name == 'idsatuan') {
                        record.push({name:'idcurrency',value:$('#form-modal-detail #idcurrency-modal :selected').val()})
                        record.push({name:'namacurrency',value:$('#form-modal-detail #idcurrency-modal :selected').attr('data-accmcurrency-nama')})
                        record.push({name:'idsatuan',value:v2.value})
                        record.push({name:'namasatuan',value:$('#form-modal-detail #idsatuan-modal :selected').attr('data-invmsatuan-nama')})
                    } else {
                        record.push({name:v2.name,value:v2.value})
                    }
                })
                data.push(record);
            } else {
                data.push(v);
            }
        })

        localStorage.removeItem(main.tabledetail);
        localStorage.setItem(main.tabledetail, JSON.stringify(data));
        getDatatableDetail(localStorage.getItem(main.tabledetail))
        cancelDetail()
    }

    function defaultOperation() {
        let subtotal = 0;
        let total = 0;
        $.each(JSON.parse(localStorage.getItem(main.tabledetail)), function(k, v) {
            $.each(v, function(k2, v2) {
                if (v2.name == 'hargatotal') {
                    subtotal += $.isNumeric(v2.value) ? v2.value : parseInt(v2.value.replace(/[^0-9]/gi, ''))
                    total += $.isNumeric(v2.value) ? v2.value : parseInt(v2.value.replace(/[^0-9]/gi, ''))
                }
            })
        });
        // alert(subtotal)
        $('#subtotal').val(subtotal);
        $('#total').val(total);
    }

    function editDetail(id, isupdate) {
        status.cruddetail = 'update';

        isupdate
            ? $('#update-detail').show()
            : $('#update-detail').hide();

        $('#noiddetail').val(id);

        $.each(JSON.parse(localStorage.getItem(main.tabledetail)), function(k, v) {
            if (id == k) {
                $.each(v, function(k2, v2) {
                    if (v2.name == 'idinventor') {
                        $('#'+v2.name+'-modal').val(v2.value).trigger('change');
                    } else if (v2.name == 'namainventor') {
                        $('#namainventorori-modal').val(v2.value);
                    } else if (v2.name == 'namainventor2') {
                        $('#namainventor-modal').val(v2.value);
                    } else if (v2.name == 'idgudang' || v2.name == 'idcompany' || v2.name == 'iddepartment' || v2.name == 'idsatuan' || v2.name == 'idtax') {
                        $('#'+v2.name+'-modal').val(v2.value).trigger('change');
                    } else if (v2.name == 'unitqty') {
                        $('#'+v2.name+'-modal').val(v2.value);
                        $('#'+v2.name+'sisa-modal').val(v2.value);
                    } else if (v2.name == 'unitqtysisa') {
                    } else {
                        $('#'+v2.name+'-modal').val(v2.value);
                    }

                    // $('#idinventor').val(v2.value).trigger('change');
                    // $('#unitqty').val(v2.value);
                    // $('#namainventor-modal').val(v2.value);
                    // $('#keterangan-modal').val(v2.value);
                    // $('#idgudang-modal').val(v2.value).trigger('change');
                    // $('#idsatuan-modal').val(v2.value);
                    // $('#konvsatuan-modal').val(v2.value);
                    // $('#unitqtysisa-modal').val(v2.value);
                    // $('#unitprice-modal').val(v2.value);
                    // $('#subtotal-modal').val(v2.value);
                    // $('#discountvar-modal').val(v2.value);
                    // $('#discount-modal').val(v2.value);
                    // $('#idtypetax-modal').val(v2.value);
                    // $('#idtax-modal').val(v2.value);
                    // $('#prosentax-modal').val(v2.value);
                    // $('#nilaitax-modal').val(v2.value);
                    // $('#hargatotal-modal').val(v2.value);
                })
            }
        })
        showModalDetail()
    }

    function cancelDetail() {
        unsetFormDetail();
        unsetFormModalDetail();
        $('#noiddetail').val('');
        $('#cancel-detail').hide();
        if (statusdetail) {
            $('#update-detail').hide();
            $('#save-detail').hide();
        } else {
            $('#update-detail').hide();
            $('#save-detail').show();
        }
        hideModalDetail()
    }

    function unsetFormDetail() {
        $('#searchprev').val('');
        if (!$('#form-detail #idinventor').val() == '') {
            $('#form-detail #idinventor').val('').trigger('change')
        }
        $('#form-detail #namainventor').val('');
        $('#form-detail #unitprice').val('');
        $('#form-detail #unitqty').val('');
    }

    function unsetFormModalDetail() {
        $('#form-modal-detail #namainventor-modal').val('');
        $('#form-modal-detail #keterangan-modal').val('');
        // if (!$('#form-modal-detail #idgudang-modal').val() == '') {
        //     $('#form-modal-detail #idgudang-modal').val('')
        // }
        // if (!$('#form-modal-detail #idcompany-modal').val() == '') {
        //     $('#form-modal-detail #idcompany-modal').val('')
        // }
        // if (!$('#form-modal-detail #iddepartment-modal').val() == '') {
        //     $('#form-modal-detail #iddepartment-modal').val('')
        // }
        $('#form-modal-detail #idsatuan-modal').val('');
        $('#form-modal-detail #konvsatuan-modal').val('');
        $('#form-modal-detail #unitqty-modal').val('');
        $('#form-modal-detail #unitqtysisa-modal').val('');
        $('#form-modal-detail #unitprice-modal').val('');
        $('#form-modal-detail #subtotal-modal').val('');
        $('#form-modal-detail #discountvar-modal').val('');
        $('#form-modal-detail #discount-modal').val('');
        $('#form-modal-detail #nilaidisc-modal').val('');
        $('#form-modal-detail #idtypetax-modal').val('');
        $('#form-modal-detail #idtax-modal').val('');
        $('#form-modal-detail #prosentax-modal').val('');
        $('#form-modal-detail #nilaitax-modal').val('');
        $('#form-modal-detail #hargatotal-modal').val('');
    }

    function setDatatableDetail() {
        $('#table-detail').DataTable();
    }

    function saveData(next_idstatustranc=null, next_nama=null, next_keterangan=null) {
        status.crudmaster = 'read';
        status.cruddetail = 'read';

        let noid = $('#form-header #noid').val();
        let formheader = {};
        let forminternal = {};
        let formcdf = {};
        let formfooter = {};
        let formmodaltanggal = {};
        let formmodalsupplier = {};
        let purchasedetail = JSON.parse(localStorage.getItem(main.tabledetail));
        let _validation = {
            tanggal: {
                required: {
                    status: true,
                    message: "'Tanggal' is required!"
                }
            },
            tanggaldue: {
                required: {
                    status: true,
                    message: "'Tanggal Due' is required!"
                }
            },
            idtypecostcontrol: {
                required: {
                    status: true,
                    message: "'Type Cost Control' is required!"
                }
            },
            projectname: {
                required: {
                    status: true,
                    message: "'Nama Pekerjaan' is required!"
                }
            },
            projectlocation: {
                required: {
                    status: true,
                    message: "'Lokasi Pekerjaan' is required!"
                }
            },
            idcardsales: {
                required: {
                    status: true,
                    message: "'Marketing' is required!"
                }
            },
            idcardcashier: {
                required: {
                    status: true,
                    message: "'Admin' is required!"
                }
            },
            idcardcustomer: {
                required: {
                    status: true,
                    message: "'Customer' is required!"
                }
            },
            // idcompany: {
            //     required: {
            //         status: true,
            //         message: "'Company' is required!"
            //     }
            // },
            // iddepartment: {
            //     required: {
            //         status: true,
            //         message: "'Department' is required!"
            //     }
            // },
            idgudang: {
                required: {
                    status: true,
                    message: "'Gudang' is required!"
                }
            },
            // idcardpj: {
            //     required: {
            //         status: true,
            //         message: "'Project Manager' is required!"
            //     }
            // },
            // idcardkoor: {
            //     required: {
            //         status: true,
            //         message: "'Project Koordinator' is required!"
            //     }
            // },
            // idcardsitekoor: {
            //     required: {
            //         status: true,
            //         message: "'Admin Site Project' is required!"
            //     }
            // },
            // idcarddrafter: {
            //     required: {
            //         status: true,
            //         message: "'Drafter' is required!"
            //     }
            // },
            // idcardsponsor: {
            //     required: {
            //         status: true,
            //         message: "'Sponsor' is required!"
            //     }
            // },
        };
        let _showvalidation = {
            status: false,
            message: ''
        };

        $('#btn-save-data').attr('disabled','disabled');

        $.each($('#form-header').serializeArray(), function(k, v){
            if (v.name in _validation) {
                if (_validation[v.name].required.status) {
                    if (v.value == '') {
                        _showvalidation.status = true;
                        _showvalidation.message += _validation[v.name].required.message+'<br>'
                    }
                }
            }
            formheader[v.name] = v.value;
        });
        $.each($('#form-internal').serializeArray(), function(k, v){
            forminternal[v.name] = v.value;
        });
        $.each($('#form-cdfdetail').serializeArray(), function(k, v){
            formcdf[v.name] = v.value;
        });
        $.each($('#form-footer').serializeArray(), function(k, v){
            formfooter[v.name] = v.value;
        });
        $.each($('#form-modal-tanggal').serializeArray(), function(k, v){
            formmodaltanggal[v.name] = v.value;
        });
        $.each($('#form-modal-supplier').serializeArray(), function(k, v){
            formmodalsupplier[v.name] = v.value;
        });

        if (_showvalidation.status) {
            $.notify({
                message: _showvalidation.message
            },{
                type: 'danger'
            });
            return false;
        }

        // if (purchasedetail == null || purchasedetail.length < 1) {
        //     $.notify({
        //         message: 'Detail minimal 1!'
        //     },{
        //         type: 'danger'
        //     });
        //     return false;
        // }
        // console.log(forminternal);return false;
        $.ajax({
            type: 'POST',
            header:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/save'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'statusadd': JSON.stringify(statusadd),
                'next_idstatustranc': JSON.stringify(next_idstatustranc),
                'next_nama': JSON.stringify(next_nama),
                'next_keterangan': JSON.stringify(next_keterangan),
                'noid': noid,
                'formheader': formheader,
                'forminternal': forminternal,
                'formfooter': formfooter,
                'formmodaltanggal': formmodaltanggal,
                'formmodalsupplier': formmodalsupplier,
                'purchasedetail': purchasedetail,

                'cdf_checks': JSON.stringify(cdf_checks),
                'cdf_selecteds': JSON.stringify(cdf_selecteds),
                'formcdf': formcdf
            },
            success: function(response) {
                !response.success
                    ? $.notify({message: response.message},{type: 'danger'})
                    : $.notify({message: response.message},{type: 'success'});

                getDatatable()
                hideModalSNST();
                cancelData()
            }
        });

        console.log('form-modal-detail')
        console.log($('#form-modal-detail').serializeArray())
        return false;
    }

    function viewData(noid) {
        status.crudmaster = 'view';
        status.cruddetail = 'read';
        conditionDetail();

        statusadd = false;
        statusdetail = true;
        setTab('tab-general', 'Header')

        $.ajax({
            type: 'POST',
            header:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/find'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'noid': noid
            },
            success: function(response) {
                let pr = response.data.master;
                let prd = response.data.detail;
                let mttts = response.data.mttts;
                let cdf = response.data.cdf;

                $('#form-header #noid').val(noid);
                $('#form-header #kode').val(pr.kode);
                $('#form-header #kodereff').val(pr.kodereff);
                $('#idstatustranc').removeAttr('style');
                $('#idstatustranc-info').css('background-color', pr.mtypetranctypestatus_classcolor);
                $('.idstatustranc-info').text(pr.mtypetranctypestatus_nama);
                $('#idstatuscard').val(pr.idstatuscard);
                $('#idstatuscard-info').text(pr.mcarduser_myusername);
                $('#dostatus').val(pr.dostatus);
                $('#dostatus-info').text(pr.dostatus);
                $('#form-header #tanggal').val(pr.tanggal);
                $('#form-header #tanggaldue').val(pr.tanggaldue);
                $('#form-modal-tanggal #tanggal-modal').val(pr.tanggal);
                $('#form-modal-tanggal #tanggaltax-modal').val(pr.tanggaltax);
                $('#form-modal-tanggal #tanggaldue-modal').val(pr.tanggaldue);

                $('#form-header #idcardsales').removeAttr('onchange');
                $('#form-header #idcardsales').val(pr.idcardsales).trigger('change')
                $('#form-header #idcardsales').attr('onchange','thenIDCardSales(true)').select2('close')

                $('#form-header #idcardcashier').removeAttr('onchange');
                $('#form-header #idcardcashier').val(pr.idcardcashier).trigger('change')
                $('#form-header #idcardcashier').attr('onchange','thenIDCardCashier(true)').select2('close')

                $('#form-header #idcardcustomer').removeAttr('onchange');
                $('#form-header #idcardcustomer').val(pr.idcardcustomer).trigger('change')
                $('#form-header #idcardcustomer').attr('onchange','thenIDCardCustomer(true)').select2('close')
                
                // $('#form-header #idcompany').removeAttr('onchange');
                // $('#form-header #idcompany').val(pr.idcompany).trigger('change')
                // $('#form-header #idcompany').attr('onchange','thenIDCompany(true)').select2('close')
                
                // $('#form-header #iddepartment').removeAttr('onchange');
                // $('#form-header #iddepartment').val(pr.iddepartment).trigger('change')
                // $('#form-header #iddepartment').attr('onchange','thenIDDepartment(true)').select2('close')

                $('#form-header #idgudang').removeAttr('onchange');
                $('#form-header #idgudang').val(pr.idgudang).trigger('change')
                $('#form-header #idgudang').attr('onchange','thenTanggalDue(true)').select2('close')

                $('#form-header #idcardpj').removeAttr('onchange');
                $('#form-header #idcardpj').val(pr.idcardpj).trigger('change')
                $('#form-header #idcardpj').attr('onchange','thenTanggalDue(true)').select2('close')

                $('#form-header #idcardkoor').removeAttr('onchange');
                $('#form-header #idcardkoor').val(pr.idcardkoor).trigger('change')
                $('#form-header #idcardkoor').attr('onchange','thenTanggalDue(true)').select2('close')

                $('#form-header #idcardsitekoor').removeAttr('onchange');
                $('#form-header #idcardsitekoor').val(pr.idcardsitekoor).trigger('change')
                $('#form-header #idcardsitekoor').attr('onchange','thenTanggalDue(true)').select2('close')

                $('#form-header #idcarddrafter').removeAttr('onchange');
                $('#form-header #idcarddrafter').val(pr.idcarddrafter).trigger('change')
                $('#form-header #idcarddrafter').attr('onchange','thenTanggalDue(true)').select2('close')

                $('#form-header #idcardsponsor').removeAttr('onchange');
                $('#form-header #idcardsponsor').val(pr.idcardsponsor).trigger('change')
                $('#form-header #idcardsponsor').attr('onchange','thenTanggalDue(true)').select2('close')

                // $('#form-header #idcompany').val(pr.idcompany).trigger('change');
                // $('#form-header #iddepartment').val(pr.iddepartment).trigger('change');
                // $('#form-header #idgudang').val(pr.idgudang).trigger('change');

                $('#form-header #idtypecostcontrol').removeAttr('onchange')
                $('#form-header #idtypecostcontrol').val(pr.idtypecostcontrol).trigger('change');
                $('#form-header #idtypecostcontrol').attr('onchange', 'getGenerateCode()');
                
                $('#form-header #kodecostcontrol').val(pr.kodecostcontrol);
                $('#form-header #projectname').val(pr.projectname);
                $('#form-header #projectlocation').val(pr.projectlocation);
                $('#form-header #projectvalue').val(pr.projectvalue);
                $('#form-header #projecttype').val(pr.projecttype).trigger('change');

                $('#form-internal #idcashbankbayar #idcashbankbayar-'+(pr.idcashbankbayar ? pr.idcashbankbayar : 'empty')).attr('selected', 'selected');
                $('#select2-idcashbankbayar-container').text($('#form-internal #idcashbankbayar #idcashbankbayar-'+(pr.idcashbankbayar ? pr.idcashbankbayar : 'empty')).text());
                $('#form-internal #idakunpersediaan #idakunpersediaan-'+(pr.idakunpersediaan ? pr.idakunpersediaan : 'empty')).attr('selected', 'selected');
                $('#select2-idakunpersediaan-container').text($('#form-internal #idakunpersediaan #idakunpersediaan-'+(pr.idakunpersediaan ? pr.idakunpersediaan : 'empty')).text());
                $('#form-internal #idakunpurchase #idakunpurchase-'+(pr.idakunpurchase ? pr.idakunpurchase : 'empty')).attr('selected', 'selected');
                $('#select2-idakunpurchase-container').text($('#form-internal #idakunpurchase #idakunpurchase-'+(pr.idakunpurchase ? pr.idakunpurchase : 'empty')).text());

                localStorage.setItem(main.tabledetail, JSON.stringify(prd));
                getDatatableDetail(localStorage.getItem(main.tabledetail));
                
                $('#form-footer #keterangan').val(pr.keterangan);


                $('#form-header #kode').attr('disabled', 'disabled');
                $('#form-header #generatekode').attr('disabled', 'disabled');
                $('#form-header #kodereff').attr('disabled', 'disabled');
                $('#form-header #tanggal').attr('disabled', 'disabled');
                $('#form-header #tanggaldue').attr('disabled', 'disabled');
                $('#form-header #idtypecostcontrol').attr('disabled', 'disabled');
                $('#form-header #projectname').attr('disabled', 'disabled');
                $('#form-header #projectlocation').attr('disabled', 'disabled');
                $('#form-header #projectvalue').attr('disabled', 'disabled');
                $('#form-header #projecttype').attr('disabled', 'disabled');
                // $('#form-header #cost-control').hide();
                $('#form-modal-tanggal #tanggal-modal').attr('disabled', 'disabled');
                $('#form-modal-tanggal #tanggaltax-modal').attr('disabled', 'disabled');
                $('#form-modal-tanggal #tanggaldue-modal').attr('disabled', 'disabled');
                $('#form-detail #idinventor').attr('disabled', 'disabled');
                $('#form-detail #namainventor').attr('disabled', 'disabled');
                $('#form-detail #unitqty').attr('disabled', 'disabled');
                $('#form-detail #update-detail').hide();
                $('#form-detail #save-detail').hide();
                $('#form-modal-detail #idinventor-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #namainventor-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #keterangan-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #idgudang-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #idcompany-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #iddepartment-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #idsatuan-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #konvsatuan-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #unitqty-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #unitqtysisa-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #unitprice-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #subtotal-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #discountvar-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #discount-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #nilaidisc-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #idtypetax-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #idtax-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #prosentax-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #nilaitax-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #hargatotal-modal').attr('disabled', 'disabled');
                $('#form-header #idcardsales').attr('disabled', 'disabled');
                $('#form-header #idcardcashier').attr('disabled', 'disabled');
                $('#form-header #idcardcustomer').attr('disabled', 'disabled');
                $('#form-header #idcompany').attr('disabled', 'disabled');
                $('#form-header #iddepartment').attr('disabled', 'disabled');
                $('#form-header #idgudang').attr('disabled', 'disabled');
                $('#form-header #idcardpj').attr('disabled', 'disabled');
                $('#form-header #idcardkoor').attr('disabled', 'disabled');
                $('#form-header #idcardsitekoor').attr('disabled', 'disabled');
                $('#form-header #idcarddrafter').attr('disabled', 'disabled');
                $('#form-header #idcardsponsor').attr('disabled', 'disabled');
                $('#form-footer #keterangan').attr('disabled', 'disabled');
                $('#form-internal #idcashbankbayar').attr('disabled', 'disabled');
                $('#form-internal #idakunpersediaan').attr('disabled', 'disabled');
                $('#form-internal #idakunpurchase').attr('disabled', 'disabled');
                
                // $('#btn-saveto').html(getButtonSaveTo(pr,mttts,true));

                cdf_show = 0;
                let _cdf_checks = [];
                let _cdf_selecteds = {};
                $.each(cdf, function(k,v) {
                    _cdf_checks.push(v.cdf_iddmsfile);
                    _cdf_selecteds['cdf-'+v.cdf_iddmsfile] = {
                        noid: v.cdf_iddmsfile,
                        nama: v.cdf_nama,
                        cdf_nama: v.cdf_nama,
                        cdf_keterangan: v.cdf_keterangan,
                        cdf_ishardcopy: v.cdf_ishardcopy,
                        cdf_ispublic: v.cdf_ispublic,
                        cdf_doexpired: v.cdf_doexpired,
                        cdf_doreviewlast: v.cdf_doreviewlast,
                        cdf_doreviewnext: v.cdf_doreviewnext,
                        urlfile: v.dmf_urlfilepreview,
                    }
                })

                cdf_checks = _cdf_checks;
                cdf_selecteds = _cdf_selecteds;

                CDFsave({elementid:'flowchart'})

                // $('#form-detail').hide();
                $('#tools-dropdown').hide();
                $('#from-departement').show();
                $('#tabledepartemen').hide();
                $('#tool-kedua').show();
                $('#tool-pertama').hide();
                $('#space_filter_button').hide();
                $('#tool_refresh').hide();
                $('#tool_filter').hide()
                $('#tool_refresh_up').hide();
            }
        });
        return false;
    }

    function getButtonSaveTo(pr,mttts,fromview) {
        let btnsaveto = '';
        if (fromview) {
            if (mttts.isinternal == 1 && mttts.isexternal == 0) {
                if (mttts.prev_idstatusbase != null && mttts.next_idstatusbase != null) {
                    if (mttts.next_idstatusbase == 4) {
                        btnsaveto = `<a class="btn btn-sm btn-success flat btn-view" style="background-color: `+mttts.next_classcolor+`; color: white; margin-right: -4px" onclick="showModalSendToOrder(`+pr.noid+`,`+pr.idtypetranc+`, true)">
                                        <i class="icon-save text-light"></i> Send to `+mttts.next_table+`
                                    </a>`;
                                    // <div class="btn-group btn-view">
                                    //     <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-sm flat" style="background-color: `+mttts.next_classcolor+`; color: white">
                                    //         <i class="icon-angle-down pr-0 text-light"></i>
                                    //     </button>
                                    //     <div class="dropdown-menu dropdown-menu-right" style="min-width: 200px; padding: 5px">
                                    //         <div class="row">
                                    //             <button type="button" class="btn col-md-12" style="background-color: `+mttts.prev_classcolor+`; color: white" onclick="showModalSNST(`+pr.noid+`,`+pr.idtypetranc+`, `+mttts.prev_noid+`, '`+mttts.prev_nama+`', true)"><i class="icon-save text-light"></i> Save to `+mttts.prev_nama+`</button>
                                    //         </div>
                                    //     </div>
                                    // </div>`;
                    } else {
                        btnsaveto = `<a class="btn btn-sm flat btn-view" style="background-color: `+mttts.next_classcolor+`; color: white; margin-right: -4px" onclick="showModalSNST(`+pr.noid+`,`+pr.idtypetranc+`, `+mttts.next_noid+`, '`+mttts.next_nama+`', true)">
                                        <i class="icon-save text-light"></i> Save to `+mttts.next_nama+`
                                    </a>`;
                                    // <div class="btn-group btn-view">
                                    //     <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-sm flat" style="background-color: `+mttts.next_classcolor+`; color: white">
                                    //         <i class="icon-angle-down pr-0 text-light"></i>
                                    //     </button>
                                    //     <div class="dropdown-menu dropdown-menu-right" style="min-width: 200px; padding: 5px">
                                    //         <div class="row">
                                    //             <button type="button" class="btn col-md-12" style="background-color: `+mttts.prev_classcolor+`; color: white" onclick="showModalSNST(`+pr.noid+`,`+pr.idtypetranc+`, `+mttts.prev_noid+`, '`+mttts.prev_nama+`', true)"><i class="icon-save text-light"></i> Save to `+mttts.prev_nama+`</button>
                                    //         </div>
                                    //     </div>
                                    // </div>`;
                    }
                } else if (mttts.prev_idstatusbase == null && mttts.next_idstatusbase != null) {
                    btnsaveto = `<a class="btn btn-sm flat btn-view" style="background-color: `+mttts.next_classcolor+`; color: white;" onclick="showModalSNST(`+pr.noid+`,`+pr.idtypetranc+`, `+mttts.next_noid+`, '`+mttts.next_nama+`', true)">
                                    <i class="icon-save text-light"></i> Save to `+mttts.next_nama+`
                                </a>`;
                }
            } else {
                btnsaveto += `<div class="btn-group">
                                    <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-success btn-sm flat">
                                        Set Status <i class="icon-angle-down pr-0"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" style="min-width: 200px; padding: 0px 5px 5px 5px">`;

                $.each(mttts.btnexternal, function(k,v) {
                    btnsaveto += `<div class="row" style="margin-top: 5px">
                                        <button type="button" class="btn col-md-12" style="background-color: `+v.classcolor+`; color: white;" onmouseover="mouseOver('save-to-issued', '#1f8e87')" onmouseout="mouseOut('save-to-issued', '`+v.classcolor+`')" id="save-to-issued" onclick="showModalSNST(`+pr.noid+`, 501, `+v.noid+`, '`+v.nama+`')"><i class="`+v.classicon+` text-light"></i> <span>Set to `+v.nama+`<span></button>
                                    </div>`;
                })

                btnsaveto += `</div>
                            </div>`;
            }
        } else {
            btnsaveto += `<a class="btn btn-sm btn-success flat" id="save-standard" style="margin-right: -4px" onclick="saveData()">
                        <i class="icon-save"></i> Save
                    </a>`;
                    // <div class="btn-group btn-view">
                    //     <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-sm btn-success flat" style="color: white">
                    //         <i class="icon-angle-down pr-0 text-light"></i>
                    //     </button>
                    //     <div class="dropdown-menu dropdown-menu-right" style="min-width: 200px; padding: 5px">
                    //         <div class="row">
                    //             <button type="button" class="btn col-md-12" style="background-color: `+mttts.next_classcolor+`; color: white" onclick="showModalSNST(`+pr.noid+`,`+pr.idtypetranc+`, `+mttts.next_noid+`, '`+mttts.next_nama+`', `+fromview+`)"><i class="icon-save text-light"></i> Save to `+mttts.next_nama+`</button>
                    //         </div>
                    //     </div>
                    // </div>`;
        }
        return btnsaveto;
    }

    function viewLog(noid) {
        alert(noid)
    }

    function printData(noid, urlprint) {
        window.open('http://'+$(location).attr('host')+'/'+noid+'/'+urlprint, '_blank');
    }

    const sendToNext = (noid, kodereff) => {
        Swal.fire({
            title: 'Are you sure?',
            text: "You want Sent to RAP?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            target: '#pagepanel'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: 'POST',
                    header: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/send_to_next'+main.urlcode,
                    dataType: 'json',
                    data: {
                        '_token': $('meta[name="csrf-token"]').attr('content'),
                        'noid': noid,
                        'kodereff': kodereff,
                    },
                    success: function(response) {
                        if (response.success) {
                            $.notify({
                                message: response.message 
                            },{
                                type: 'success'
                            });
                        } else{
                            $.notify({
                                message: response.message 
                            },{
                                type: 'danger'
                            });
                        }
                        getDatatable();
                        hideModalSendToOrder();
                    }
                });
            }
        })
    }

    function SNST(noid, idstatustranc, statustranc, fromview) {
        $.ajax({
            type: 'POST',
            header: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/snst'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'noid': noid,
                'idstatustranc': idstatustranc,
                'statustranc': statustranc,
                'idtypetranc': $('#form-modal-snst #idtypetranc-snst').val(),
                'spknumber': $('#form-modal-snst #spknumber-snst').val(),
                'keterangan': $('#form-modal-snst #keterangan-snst').val(),
            },
            success: function(response) {
                !response.success
                    ? $.notify({message: response.message},{type: 'danger'})
                    : $.notify({message: response.message},{type: 'success'});

                getDatatable();
                hideModalSNST();

                // if (fromview) {
                    // cancelData();
                // }
            }
        });
    }

    function DIV(noid, idstatustranc) {
        let ist = $('#form-modal-div #idstatustranc-div :selected').val();

        $.ajax({
            type: 'POST',
            header:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/div'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'noid': noid,
                'idtypetranc': $('#form-modal-div #idtypetranc-div').val(),
                'idstatustranc': ist,
                'statustranc': $('#form-modal-div #idstatustranc-div :selected').attr('data-mtypetranctypestatus-nama'),
                'keterangan': $('#form-modal-div #keterangan-div').val(),
            },
            success: function(response) {
                getDatatable();
                if (response.success) {
                    $.notify({
                        message: response.message 
                    },{
                        type: 'success'
                    });
                } else{
                    $.notify({
                        message: response.message 
                    },{
                        type: 'danger'
                    });
                }
                hideModalDIV();
            }
        });
    }

    function SAPCF(noid) {
        let idtypetranc = $('#modal-sapcf #idtypetranc-sapcf').val();
        let idstatustranc = $('#modal-sapcf #idstatustranc-sapcf :selected').val();
        let statustranc = $('#modal-sapcf #idstatustranc-sapcf :selected').attr('data-mtypetranctypestatus-nama');
        let keterangan = $('#modal-sapcf #keterangan-sapcf').val();

        if (idstatustranc == '') {
            alert('Status Tranc is required');
            $('#modal-sapcf #idstatustranc-sapcf').select2('focus');
            return false;
        }
        if (keterangan == '') {
            alert('Keterangan is required');
            $('#modal-sapcf #keterangan-sapcf').focus();
            return false;
        }

        $.ajax({
            type: 'POST',
            header:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/sapcf'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'noid': noid,
                'idtypetranc': $('#form-modal-sapcf #idtypetranc-sapcf').val(),
                'idstatustranc': idstatustranc,
                'statustranc': statustranc,
                'keterangan': keterangan,
            },
            success: function(response) {
                if (!response.success) {
                    $.notify({message: response.message},{type: 'danger'});
                } else{
                    $.notify({message: response.message},{type: 'success'});
                }
                
                getDatatable();
                hideModalSAPCF();
            }
        });
    }

    function setPending(noid) {
        $.ajax({
            type: 'POST',
            header:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/set_pending'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'noid': noid,
                'idtypetranc': $('#form-modal-set-pending #idtypetranc-pending').val(),
                'keterangan': $('#form-modal-set-pending #keterangan-pending').val(),
            },
            success: function(response) {
                getDatatable();
                if (response.success) {
                    $.notify({
                        message: response.message 
                    },{
                        type: 'success'
                    });
                } else{
                    $.notify({
                        message: response.message 
                    },{
                        type: 'danger'
                    });
                }
                hideModalSetPending();
            }
        });
    }

    function setCanceled(noid) {
        $.ajax({
            type: 'POST',
            header:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/set_canceled'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'noid': noid,
                'idtypetranc': $('#form-modal-set-canceled #idtypetranc-canceled').val(),
                'keterangan': $('#form-modal-set-canceled #keterangan-canceled').val(),
            },
            success: function(response) {
                getDatatable();
                if (response.success) {
                    $.notify({
                        message: response.message 
                    },{
                        type: 'success'
                    });
                } else{
                    $.notify({
                        message: response.message 
                    },{
                        type: 'danger'
                    });
                }
                hideModalSetCanceled();
            }
        });
    }

    function editData(noid) {
        ready = false;
        status.crudmaster = 'update';
        status.cruddetail = 'read';
        statusadd = false;
        statusdetail = false;

        showLoading();
        setTab('tab-general', 'Header')
        conditionDetail();

        $('#idtypecostcontrol').removeAttr('onchange');

        let find = $.ajax({
            type: 'POST',
            header: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/find'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'noid': noid
            },
            success: function(response) {
                let pr = response.data.master;
                let prd = response.data.detail;
                let mttts = response.data.mttts;
                let cdf = response.data.cdf;

                $('#form-header #noid').val(noid);
                $('#form-header #nourut').val(pr.nourut);
                $('#form-header #kode').val(pr.kode);
                $('#form-header #kodereff').val(pr.kodereff);
                $('#idstatustranc').val(pr.idstatustranc);
                $('#idstatustranc').removeAttr('style');
                $('#idstatustranc-info').css('background-color', pr.mtypetranctypestatus_classcolor);
                $('.idstatustranc-info').text(pr.mtypetranctypestatus_nama);
                $('#idstatuscard').val(pr.idstatuscard);
                $('#idstatuscard-info').text(pr.mcarduser_myusername);
                $('#dostatus').val(pr.dostatus);
                $('#dostatus-info').text(pr.dostatus);
                $('#form-header #tanggal').val(pr.tanggal);
                $('#form-header #tanggaldue').val(pr.tanggaldue);
                $('#form-modal-tanggal #tanggal-modal').val(pr.tanggal);
                $('#form-modal-tanggal #tanggaltax-modal').val(pr.tanggaltax);
                $('#form-modal-tanggal #tanggaldue-modal').val(pr.tanggaldue);

                $('#form-header #idtypecostcontrol').removeAttr('onchange')
                $('#form-header #idtypecostcontrol').val(pr.idtypecostcontrol).trigger('change');
                $('#form-header #idtypecostcontrol').attr('onchange', 'getGenerateCode()');

                $('#form-header #projectname').val(pr.projectname).trigger('change');
                $('#form-header #projectlocation').val(pr.projectlocation).trigger('change');
                $('#form-header #projectvalue').val(pr.projectvalue);
                $('#form-header #projecttype').val(pr.projecttype).trigger('change');
                $('#form-header #idcostcontrol').val(pr.idcostcontrol);
                $('#form-header #kodecostcontrol').val(pr.kodecostcontrol);
                
                $('#form-header #idcardsales').removeAttr('onchange');
                $('#form-header #idcardsales').val(pr.idcardsales).trigger('change')
                $('#form-header #idcardsales').attr('onchange','thenIDCardSales(true)').select2('close')

                $('#form-header #idcardcashier').removeAttr('onchange');
                $('#form-header #idcardcashier').val(pr.idcardcashier).trigger('change')
                $('#form-header #idcardcashier').attr('onchange','thenIDCardCashier(true)').select2('close')

                $('#form-header #idcardcustomer').removeAttr('onchange');
                $('#form-header #idcardcustomer').val(pr.idcardcustomer).trigger('change')
                $('#form-header #idcardcustomer').attr('onchange','thenIDCardCustomer(true)').select2('close')

                // $('#form-header #idcompany').removeAttr('onchange');
                // $('#form-header #idcompany').val(pr.idcompany).trigger('change')
                // $('#form-header #idcompany').attr('onchange','thenIDCompany(true)').select2('close')
                
                // $('#form-header #iddepartment').removeAttr('onchange');
                // $('#form-header #iddepartment').val(pr.iddepartment).trigger('change')
                // $('#form-header #iddepartment').attr('onchange','thenIDDepartment(true)').select2('close')

                $('#form-header #idgudang').removeAttr('onchange');
                $('#form-header #idgudang').val(pr.idgudang).trigger('change')
                $('#form-header #idgudang').attr('onchange','thenTanggalDue(true)').select2('close')

                $('#form-header #idcardpj').removeAttr('onchange');
                $('#form-header #idcardpj').val(pr.idcardpj).trigger('change')
                $('#form-header #idcardpj').attr('onchange','thenTanggalDue(true)').select2('close')

                $('#form-header #idcardkoor').removeAttr('onchange');
                $('#form-header #idcardkoor').val(pr.idcardkoor).trigger('change')
                $('#form-header #idcardkoor').attr('onchange','thenTanggalDue(true)').select2('close')

                $('#form-header #idcardsitekoor').removeAttr('onchange');
                $('#form-header #idcardsitekoor').val(pr.idcardsitekoor).trigger('change')
                $('#form-header #idcardsitekoor').attr('onchange','thenTanggalDue(true)').select2('close')

                $('#form-header #idcarddrafter').removeAttr('onchange');
                $('#form-header #idcarddrafter').val(pr.idcarddrafter).trigger('change')
                $('#form-header #idcarddrafter').attr('onchange','thenTanggalDue(true)').select2('close')

                $('#form-header #idcardsponsor').removeAttr('onchange');
                $('#form-header #idcardsponsor').val(pr.idcardsponsor).trigger('change')
                $('#form-header #idcardsponsor').attr('onchange','thenTanggalDue(true)').select2('close')
                
                
                $('#form-internal #idcashbankbayar #idcashbankbayar-'+(pr.idcashbankbayar ? pr.idcashbankbayar : 'empty')).attr('selected', 'selected');
                $('#select2-idcashbankbayar-container').text($('#form-internal #idcashbankbayar #idcashbankbayar-'+(pr.idcashbankbayar ? pr.idcashbankbayar : 'empty')).text());
                $('#form-internal #idakunpersediaan #idakunpersediaan-'+(pr.idakunpersediaan ? pr.idakunpersediaan : 'empty')).attr('selected', 'selected');
                $('#select2-idakunpersediaan-container').text($('#form-internal #idakunpersediaan #idakunpersediaan-'+(pr.idakunpersediaan ? pr.idakunpersediaan : 'empty')).text());
                $('#form-internal #idakunpurchase #idakunpurchase-'+(pr.idakunpurchase ? pr.idakunpurchase : 'empty')).attr('selected', 'selected');
                $('#select2-idakunpurchase-container').text($('#form-internal #idakunpurchase #idakunpurchase-'+(pr.idakunpurchase ? pr.idakunpurchase : 'empty')).text());

                localStorage.setItem(main.tabledetail, JSON.stringify(prd));
                getDatatableDetail(localStorage.getItem(main.tabledetail));
                setTab('tab-general', 'Header')

                $('#form-footer #keterangan').val(pr.keterangan);

                $('#btn-saveto').html(getButtonSaveTo(pr,mttts,false));


                cdf_show = 0;
                let _cdf_checks = [];
                let _cdf_selecteds = {};
                $.each(cdf, function(k,v) {
                    _cdf_checks.push(v.cdf_iddmsfile);
                    _cdf_selecteds['cdf-'+v.cdf_iddmsfile] = {
                        noid: v.cdf_iddmsfile,
                        nama: v.cdf_nama,
                        cdf_nama: v.cdf_nama,
                        cdf_keterangan: v.cdf_keterangan,
                        cdf_ishardcopy: v.cdf_ishardcopy,
                        cdf_ispublic: v.cdf_ispublic,
                        cdf_doexpired: v.cdf_doexpired,
                        cdf_doreviewlast: v.cdf_doreviewlast,
                        cdf_doreviewnext: v.cdf_doreviewnext,
                        urlfile: v.dmf_urlfilepreview,
                    }
                })

                cdf_checks = _cdf_checks;
                cdf_selecteds = _cdf_selecteds;

                CDFsave({elementid:'flowchart'})


                $('#tools-dropdown').hide(); // Hide button tools
                $('#tabledepartemen').hide(); // Hide datatable
                $('#tool-pertama').hide(); // Show buton add data
                $('#space_filter_button').hide(); // Hide filter di datatable
                $('#tool_refresh').hide(); // Hide button refresh
                $('#tool_refresh_up').hide(); // Hide button refresh
                $('#from-departement').show(); // Show form
                $('#tool-kedua').show(); // Show button cancel
                hideLoading();
                
                ready = true;
                // $('#form-detail #update-detail').hide();
            }
        });
    }

    function removeData(noid) {
        $.ajax({
            type: 'POST',
            header:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/delete'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'noid': noid
            },
            success: function(response) {
                $.notify({
                    message: response.message 
                },{
                    type: 'success'
                });
                getDatatable()
                hideModalDelete()
            }
        });
    }

    function removeDetail(noid) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You want to delete this detail?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            target: '#pagepanel'
        }).then((result) => {
            if (result.isConfirmed) {
                let data = new Array();
                $.each(JSON.parse(localStorage.getItem(main.tabledetail)), function(k, v) {
                    if (noid != k) {
                        data.push(v);
                    }
                })
                localStorage.removeItem(main.tabledetail)
                localStorage.setItem(main.tabledetail, JSON.stringify(data))

                Swal.fire({
                    icon: 'success',
                    title: 'Deleted!',
                    text: 'Delete data successfully.',
                    target: '#pagepanel',
                });

                getDatatableDetail(localStorage.getItem(main.tabledetail))
            }
        })
    }

    function errorHandler(res) {
        if (res.status == 419) {
            Swal.fire({
                icon: 'warning',
                title: 'Warning!',
                text: 'Mohon refresh browser anda.',
                target: '#pagepanel',
            });
        } else {
            Swal.fire({
                icon: 'warning',
                title: 'Warning!',
                text: 'Mohon refresh ulang.',
                target: '#pagepanel',
            });
        }
    }

    function getDatatable() {
        var columns = <?= $columns ?>;
        var columndefs = <?= $columndefs ?>;

        $('#table_master').DataTable().destroy();
        mytable = $('#table_master').DataTable({
            "autoWidth": false,
            "pageLength": 10,
            "searching": true,
            "processing": true,
            "language": {
                'loadingRecords': '&nbsp;',
                'processing': 'Loading...'
            },
            "serverSide": true,
            "bInfo": true,
            "bFilter": false,
            "bLengthChange": false,
            "columns": [
                {mData:'checkbox',orderable:false,class:'text-center'},
                {mData:'no',orderable:false,class:'text-right'},
                // {mData:'idtypetranc'},
                {mData:'idstatustranc',class:'text-center'},
                {mData:'kode'},
                {mData:'kodereff'},
                {mData:'tanggal'},
                {mData:'tanggaldue'},
                // {mData:'totalsubtotal',class:'text-right',render:$.fn.dataTable.render.number(',','.',0,'Rp.')},
                {mData:'projectvalue',class:'text-right',render:$.fn.dataTable.render.number(',','.',0,'Rp.')},
                {mData:'projectname'},
                {mData:'idcardcustomer_nama'},
                {mData:'idcardsales_nama'},
                // {mData:'idcompany'},
                // {mData:'iddepartment'},
                // {mData:'idgudang'},
                {mData:'action',orderable:false},
            ],
            "columnDefs": columndefs,
            "infoCallback": function( settings, start, end, max, total, pre ) {
                var options = '';
                $.each(settings.aLengthMenu, function(k, v) {
                    options += '<option value="'+v+'" id="alm'+v+'" class="alm">'+v+'</option>'
                })
                if ((end-start) == settings._iDisplayLength-1) {
                    var valuepage = end/settings._iDisplayLength;
                } else {
                    var valuepage = ((start-1)/settings._iDisplayLength)+1;
                }
                if (start == 1) {
                    var prevdisabled = 'disabled'
                } else {
                    var prevdisabled = '';
                }
                if (end == total) {
                    var nextdisabled = 'disabled'
                } else {
                    var nextdisabled = '';
                }
                // if (settings.oPreviousSearch.sSearch == $('#searchform').val()) {
                //     alert($('#searchform').val())
                //     var valuesearch = $('#searchform').val()
                // } else {
                //     var valuesearch = $('#searchform').val()
                //     alert('else'+$('#searchform').val())
                // }
                if (settings.oPreviousSearch.sSearch != '') {
                    statussearch = true
                    searchtablewidth = 200
                    valuesearch = settings.oPreviousSearch.sSearch
                } else {
                    statussearch = false
                    searchtablewidth = 200
                    valuesearch = ''
                }
                $('#appendInfoDatatable').html(`
                <div class="row" style="background: #90CAF9; padding-left: 10px; padding-right: 10px;">
                    <div class="col-md-12">
                        Page
                        <button id="prevpage" onclick="prevpage()" class="btn btn-sm default prev `+prevdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-left">
                            </i>
                        </button>
                        <input type="text" id="valuepage" onkeyup="valuepage()" class="input-mini" value="`+valuepage+`" style="text-align: center; border: 0px; margin: 5px 5px 5px 5px">
                        <button id="nextpage" onclick="nextpage()" class="btn btn-sm default next `+nextdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-right">
                            </i>
                        </button> of <span class="pagination-panel-total">`+
                            (settings._iRecordsTotal == settings._iDisplayLength ? 1 : parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)
                        +`</span>
                        <span class="seperator">|</span>
                        Rec/Page 
                        <select id="alengthmenu" onchange="alengthmenu()" name="table_master_length" aria-controls="table_master" class="input-inline input-xsmall input-sm recofdt" style="border: 0px; margin: 5px 5px 5px 5px">
                            `+options+`
                        </select>
                        <span class="seperator">|</span>Total `+settings._iRecordsTotal+` records
                        <input id="searchform" onkeyup="searchform_()" value="`+valuesearch+`" type="search" class="form-control input-inline input-sm clicked" placeholder="" aria-controls="table_master" style="width: `+searchtablewidth+`px; position: absolute; right: 0px; top: 4px; height: 80%; border: 0;">
                        <div onclick="searchtable()" class="btn fa fa-search text-right" style="position: absolute; right: 0px; top: 4px; height: 80%; background: transparent; margin: 0px 0px 0px 0px; padding: 5px 0px 5px 0px">
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                </div>`)

                $("#searchform").keyup(e => {
                    if(e.keyCode == 13) {
                        mytable.search($("#searchform").val()).page(1).draw(true);
                    };
                })

                console.log('settings')
                console.log(settings)
                console.log(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)
                $('.alm').removeAttr('selected');
                $('#alm'+settings._iDisplayLength).attr('selected', 'selected');
                // console.log(valuepage)
                $('.dataTables_info').css('display', 'none')
                $('.dataTables_paginate').css('display', 'none')
            },
            "ajax": {
                "url": "/get"+main.urlcode,
                "type": "POST",
                "dataType":'json',
                "data": {
                    "_token":$('meta[name="csrf-token"]').attr('content'),
                    "filter":selectedfilter,
                    "datefilter":datefilter,
                    "datefilter2":datefilter2,
                },
                "dataSrc": function(json) {
                    selectcheckboxman = json.allnoid;
                    localStorage.setItem('sc-m-an', JSON.stringify(json.allnoid))
                    if (json.datefilter.type == 'a') {
                        $('#dropdown-date-filter span').text('All')
                    } else {
                        $('#dropdown-date-filter span').text(json.datefilter.from+' - '+json.datefilter.to)
                    }
                    return json.data;
                },
                "error": function(res) {
                    errorHandler(res);
                }
            },
            "pagingType": "input",
            "createdRow": function(row, data, dataIndex) {
                // console.log('datarowwww')
                // console.log(data.noid)
                $(row).addClass('my_row');
                $(row).addClass('row-m-'+data.noid);
                // // $(row).css('height', '5px');
                $(row).attr('onmouseover', 'showButtonAction(this, '+dataIndex+')');
                $(row).attr('onmouseout', 'resetCss()');
                // $(row).attr('onmouseover', 'getPositionXY(this)');

                let scm = JSON.parse(localStorage.getItem('sc-m'));
                if (scm.includes(data.noid)) {
                    console.log('rowwwww')
                    console.log($(row).children('td').children('input').val('TRUE'))
                    $(row).css('background-color', '#90CAF9');
                    $(row).children('td:first').html('<input type="checkbox" value="TRUE" class="checkbox-m" id="checkbox-m-'+data.noid+'" onchange="selectCheckboxM('+data.noid+')" checked>');
                }
            },
            "oLanguage":{
                "loadingRecords": "Please wait - loading...",
                "sProcessing": "<div id='loadernya'></div>",
                "sInfo": "&nbsp;|&nbsp;Total&nbsp;_TOTAL_ records",
                "sLengthMenu": "&nbsp;|&nbsp;Rec/Page&nbsp;_MENU_",
                "sInfoFiltered": "",
                "sInfoEmpty": "| No records found to show",
                "sEmptyTable": "No data available in table",
                "sZeroRecords": "No matching records found",
                "sSearch": "<button onclick='toggleButtonSearch()' class='btn flat dtfilter_search_btn' style='background-color: white'><i class='fa fa-search'></i></button>",
                "oPaginate": {
                    "sPrevious": "<i class='fa fa-angle-left'></i>",
                    "sNext": "<i class='fa fa-angle-right'></i> ",
                }
            },
            footerCallback: function ( row, data, start, end, display ) {
                var api = this.api(), data;
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
    
                var sum = function ( i ) {
                    var value = 0;
                    $.each(api.column( i ).data(), function(k, v) {
                        var _split = v.split('.');
                        var value2 = '';
                        $.each(_split, function(k2, v2) {
                            value2 += v2
                        })
                        value += Number(value2)
                    })
                    return value
                }
                var addcommas = function addCommas(nStr) {
                    nStr += '';
                    var x = nStr.split('.');
                    var x1 = x[0];
                    var x2 = x.length > 1 ? '.' + x[1] : '';
                    var rgx = /(\d+)(\d{3})/;
                    while (rgx.test(x1)) {
                        x1 = x1.replace(rgx, '$1' + '.' + '$2');
                    }
                    return x1 + x2;
                }

                pageTotal = api
                    .column( 9, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                // Update footer
                // console.log('akak')
                // console.log(eval(api.column(9).data()[0]))
                $( api.column(1).footer() ).html(data.length);
                $( api.column(8).footer() ).html(api.column(8).data().reduce( function (a, b) { return parseInt(a)+parseInt(b); }, 0 ));
                var $th8 = $(row).find('th').eq(8);
                $th8.text( $.fn.dataTable.render.number(',', '.', 0, 'Rp.').display( $th8.text() )) 
                $( api.column(9).footer() ).html(api.column(9).data().reduce( function (a, b) { return parseInt(a)+parseInt(b); }, 0 ));
                var $th9 = $(row).find('th').eq(9);
                $th9.text( $.fn.dataTable.render.number(',', '.', 0, 'Rp.').display( $th9.text() )) 
                // $( api.column(6).footer() ).html(data.length);
            }
        });

        new $.fn.dataTable.Buttons( mytable, {
            buttons: [
                {
                    autoFilter: true,
                    text:      '<i class="fa fa-trash"></i> Delete Selected',
                    titleAttr: 'Delete Selected',
                    className: 'btn btn-danger col-md-12',
                    action:  function(e, dt, button, config) {
                        let scm = JSON.parse(localStorage.getItem('sc-m'));
                        if (scm.length == 0) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'There\'s no selected!',
                                target: '#pagepanel'
                            })
                        } else {
                            Swal.fire({
                                title: 'Are you sure?',
                                text: "You want to delete this selected?",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                target: '#pagepanel'
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    removeSelected(scm)
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'Deleted!',
                                        text: 'Delete data successfully.',
                                        target: '#pagepanel',
                                    });
                                }
                            })
                        }
                    },
                },{
                    autoFilter: true,
                    text:      '<i class="fa fa-trash"></i> Delete All',
                    titleAttr: 'Delete All',
                    className: 'btn btn-danger col-md-12',
                    action:  function(e, dt, button, config) {
                        let scma = JSON.parse(localStorage.getItem('sc-m-a'));
                        let scman = JSON.parse(localStorage.getItem('sc-m-an'));
                        let scm = JSON.parse(localStorage.getItem('sc-m'));
                        // if (scma) {
                            Swal.fire({
                                title: 'Are you sure?',
                                text: "You want to delete all data?",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                target: '#pagepanel'
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    removeSelected(scman)
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'Deleted!',
                                        text: 'Delete data successfully.',
                                        target: '#pagepanel',
                                    });
                                }
                            })
                        // } else {
                        //     Swal.fire({
                        //         icon: 'error',
                        //         title: 'Oops...',
                        //         text: 'Something went wrong!',
                        //         footer: '<a href>Why do I have this issue?</a>'
                        //     })
                        // }
                    },
                },{
                    extend:    'excel',
                    autoFilter: true,
                    text:      '<i class="fa fa-file-excel"></i> Export to Excel',
                    titleAttr: 'Export to Excel',
                    title: $("#judul_excel").val(),
                    className: 'btn btn-success col-md-12 mb-0',
                    exportOptions: {
                        modifier: {
                            search: 'applied',
                            order: 'applied'
                        },
                        columns: [ 1,2,3, ':visible' ]
                        //                  <a style="font-size: 14px;min-width: 175px;
                        // text-align: left;" href="javascript:;"  class="dropdown-item">
                        // <i style="margin-top:10px;margin-right: 5px;" class="fa fa-file-excel"></i> Export to Excel </a>
                                        // columns: [ 0, ':visible' ]
                    },
                    action:  function(e, dt, button, config) {
                    // $('.loading').fadeIn();
                        var that = this;
                        setTimeout(function () {
                            $.fn.dataTable.ext.buttons.excelHtml5.action.call(that,e, dt, button, config);
                            // console.log(that);
                            // console.log(e);
                            // console.log(dt);
                            // console.log(button);
                            // console.log(config);
                            $('.loading').fadeOut();
                        },500);
                    },
                }
            ]
            // infoDatatable()
        });
        // infoDatatable()

        mytable.buttons().container().appendTo('#drop_export2');

        $('#pagepanel-body').css('min-height', screen.height-(screen.height/3))
    }

    function deleteSelected() {
        let scm = JSON.parse(localStorage.getItem('sc-m'));
        if (scm.length == 0) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'There\'s no selected!',
                target: '#pagepanel'
            })
        } else {
            Swal.fire({
                title: 'Are you sure?',
                text: "You want to delete this selected?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                target: '#pagepanel'
            }).then((result) => {
                if (result.isConfirmed) {
                    removeSelected(scm)
                    Swal.fire({
                        icon: 'success',
                        title: 'Deleted!',
                        text: 'Delete data successfully.',
                        target: '#pagepanel',
                    });
                }
            })
        }
    }

    function deleteAll() {
        let scma = JSON.parse(localStorage.getItem('sc-m-a'));
        let scman = JSON.parse(localStorage.getItem('sc-m-an'));
        let scm = JSON.parse(localStorage.getItem('sc-m'));

        Swal.fire({
            title: 'Are you sure?',
            text: "You want to delete all data?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            target: '#pagepanel'
        }).then((result) => {
            if (result.isConfirmed) {
                removeSelected(scman)
                Swal.fire({
                    icon: 'success',
                    title: 'Deleted!',
                    text: 'Delete data successfully.',
                    target: '#pagepanel',
                });
            }
        })
    }

    function exportExcel() {

    }

    function chooseCostControl(noid, kode, idtypecostcontrol) {
        $('#form-header #idtypecostcontrol').val(idtypecostcontrol).trigger('change');
        $('#form-header #idcostcontrol').val(noid);
        $('#form-header #kodecostcontrol').val(kode);
        getGenerateCode();
        hideModalCostControl()
        $('#form-header #idcardsupplier').select2('focus')
    }

    function format(d) {
        // `d` is the original data object for the row
        return '<b>Log Subject : </b><span>'+(d.logsubjectfull ? d.logsubjectfull : '')+'</span><br>'+
                '<b>Keterangan : </b><span>'+(d.keteranganfull ? d.keteranganfull : '')+'</span>';
    }

    function getDatatableLog(noid, kode, noidmore='', fieldmore='') {
        $('#table-log').DataTable().destroy();
        mytablelog = $('#table-log').DataTable({
            "pageLength": 10,
            "searching": true,
            "processing": true,
            "serverSide": true,
            "bInfo": true,
            "bFilter": false,
            "bLengthChange": false,
            "bAutoWidth": false,
            "order": [[ 6, "desc" ]],
            "columns": [
                {className:'details-control',orderable:false,data:null,defaultContent:''},
                {mData:'no',orderable:false,class:'text-right'},
                {mData:'idtypeaction'},
                {mData:'logsubject'},
                {mData:'keterangan'},
                {mData:'idcreate'},
                {mData:'docreate'},
            ],
            "columnDefs": [
                {targets:0,width:"15px"},
                {targets:1,width:"15px"},
                {targets:2,width:"100px"},
                {targets:3,width:"350px"},
                {targets:4,width:"350px"},
                {targets:5,width:"100px"},
                {targets:6,width:"100px"},
            ],
            // "fixedColumns": true,
            "infoCallback": function( settings, start, end, max, total, pre ) {
                var options = '';
                $.each(settings.aLengthMenu, function(k, v) {
                    options += '<option value="'+v+'" id="alm'+v+'" class="alm">'+v+'</option>'
                })
                if ((end-start) == settings._iDisplayLength-1) {
                    var valuepage = end/settings._iDisplayLength;
                } else {
                    var valuepage = ((start-1)/settings._iDisplayLength)+1;
                }
                if (start == 1) {
                    var prevdisabled = 'disabled'
                } else {
                    var prevdisabled = '';
                }
                if (end == total) {
                    var nextdisabled = 'disabled'
                } else {
                    var nextdisabled = '';
                }
                // if (settings.oPreviousSearch.sSearch == $('#searchform').val()) {
                //     alert($('#searchform').val())
                //     var valuesearch = $('#searchform').val()
                // } else {
                //     var valuesearch = $('#searchform').val()
                //     alert('else'+$('#searchform').val())
                // }
                if (settings.oPreviousSearch.sSearch != '') {
                    statussearch = true
                    searchtablewidth = 200
                    valuesearch = settings.oPreviousSearch.sSearch
                } else {
                    statussearch = false
                    searchtablewidth = 200
                    valuesearch = ''
                }
                $('#appendInfoDatatableLog').html(`
                <div class="row" style="background: #90CAF9; padding-left: 15px; padding-right: 15px;">
                    <div class="col-md-12">
                        Page
                        <button id="prevpagelog" onclick="prevpagelog()" class="btn btn-sm default prev `+prevdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-left">
                            </i>
                        </button>
                        <input type="text" id="valuepagelog" onkeyup="valuepagelog()" class="input-mini" value="`+valuepage+`" style="text-align: center; border: 0px; margin: 5px 5px 5px 5px">
                        <button id="nextpagelog" onclick="nextpagelog()" class="btn btn-sm default next `+nextdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-right">
                            </i>
                        </button> of <span class="pagination-panel-total">`+
                            (settings._iRecordsTotal == settings._iDisplayLength ? 1 : parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)
                        +`</span>
                        <span class="seperator">|</span>
                        Rec/Page 
                        <select id="alengthmenulog" onchange="alengthmenulog()" name="table_master_length" aria-controls="table_master" class="input-inline input-xsmall input-sm recofdt" style="border: 0px; margin: 5px 5px 5px 5px">
                            `+options+`
                        </select>
                        <span class="seperator">|</span>Total `+settings._iRecordsTotal+` records
                        <input id="searchformlog" onkeyup="searchformlog()" value="`+valuesearch+`" type="search" class="form-control input-inline input-sm clicked" placeholder="" aria-controls="table_master" style="width: `+searchtablewidth+`px; position: absolute; right: 0px; top: 4px; height: 80%; border: 0;">
                        <div onclick="searchtablelog()" class="btn fa fa-search text-right" style="position: absolute; right: 0px; top: 4px; height: 80%; background: transparent; margin: 0px 0px 0px 0px; padding: 5px 0px 5px 0px">
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                </div>`)
                console.log('settings')
                console.log(settings)
                console.log(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)
                $('.alm').removeAttr('selected');
                $('#alm'+settings._iDisplayLength).attr('selected', 'selected');
                // console.log(valuepage)
                $('.dataTables_info').css('display', 'none')
                $('.dataTables_paginate').css('display', 'none')
            },
            "ajax": {
                "url": "/get_log"+main.urlcode,
                "type": "POST",
                "dataType":'json',
                "data": {
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    'statusadd': statusadd,
                    'noid': noid,
                    'kode': kode,
                    'noidmore': noidmore,
                    'fieldmore': fieldmore,
                },
                "error": function(res) {
                    errorHandler(res);
                }
            },
            "pagingType": "input",
            "createdRow": function(row, data, dataIndex) {
                $(row).attr('onmouseover', 'showButtonAction(this, '+dataIndex+', 100)');
                $(row).attr('onmouseout', 'resetCss()');
            },
            "oLanguage":{
                "loadingRecords": "Please wait - loading...",
                "sProcessing": "<div id='loadernya'></div>",
                "sInfo": "&nbsp;|&nbsp;Total&nbsp;_TOTAL_ records",
                "sLengthMenu": "&nbsp;|&nbsp;Rec/Page&nbsp;_MENU_",
                "sInfoFiltered": "",
                "sInfoEmpty": "| No records found to show",
                "sEmptyTable": "No data available in table",
                "sZeroRecords": "No matching records found",
                "sSearch": "<button onclick='toggleButtonSearch()' class='btn flat dtfilter_search_btn' style='background-color: white'><i class='fa fa-search'></i></button>",
                "oPaginate": {
                    "sPrevious": "<i class='fa fa-angle-left'></i>",
                    "sNext": "<i class='fa fa-angle-right'></i> ",
                }
            },
            footerCallback: function ( row, data, start, end, display ) {
            }
        });
        
        if (!statusgetlog) {
            $('#table-log tbody').on('click', 'td.details-control', function(){
                var tr = $(this).closest('tr');
                var row = mytablelog.row( tr );

                if(row.child.isShown()){
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                } else {
                    // Open this row
                    row.child(format(row.data())).show();
                    tr.addClass('shown');
                }
            });
            statusgetlog = true;
        }
    }

    function moreLog(noid, field) {
        $('#table-log #'+field+'-'+noid).show();
        $('#table-log #morelog-'+field+'-'+noid).attr('onclick', 'lessLog('+noid+', \''+field+'\')');
        $('#table-log #morelog-'+field+'-'+noid+' i').attr('class', 'icon-angle-left');
        // $('#table-log #morelog-'+field+'-'+noid+' span').text('Less');
    }

    function lessLog(noid, field) {
        $('#table-log #'+field+'-'+noid).hide();
        $('#table-log #morelog-'+field+'-'+noid).attr('onclick', 'moreLog('+noid+', \''+field+'\')');
        $('#table-log #morelog-'+field+'-'+noid+' i').attr('class', 'icon-angle-right');
        // $('#table-log #morelog-'+field+'-'+noid+' span').text('More');
    }

    function getDatatableRAB(_statusdetail) {
        let columns = <?= $columnscostcontrol ?>;
        let columndefs = <?= $columndefscostcontrol ?>;

        $('#table-rab').DataTable().destroy();
        mytablecostcontrol = $('#table-rab').DataTable({
            "autoWidth": false,
            "pageLength": 10,
            "searching": true,
            "processing": true,
            "serverSide": true,
            "bInfo": true,
            "bFilter": false,
            "bLengthChange": false,
            "columns": [
                {mData:'checkbox',orderable:false,class:'text-center'},
                {mData:'no',orderable:false,class:'text-right'},
                {mData:'kode'},
                {mData:'costcontrol_nama'},
                {mData:'action',orderable:false},
            ],
            "columnDefs": columndefs,
            "infoCallback": function( settings, start, end, max, total, pre ) {
                var options = '';
                $.each(settings.aLengthMenu, function(k, v) {
                    options += '<option value="'+v+'" id="alm'+v+'" class="alm">'+v+'</option>'
                })
                if ((end-start) == settings._iDisplayLength-1) {
                    var valuepage = end/settings._iDisplayLength;
                } else {
                    var valuepage = ((start-1)/settings._iDisplayLength)+1;
                }
                if (start == 1) {
                    var prevdisabled = 'disabled'
                } else {
                    var prevdisabled = '';
                }
                if (end == total) {
                    var nextdisabled = 'disabled'
                } else {
                    var nextdisabled = '';
                }
                // if (settings.oPreviousSearch.sSearch == $('#searchform').val()) {
                //     alert($('#searchform').val())
                //     var valuesearch = $('#searchform').val()
                // } else {
                //     var valuesearch = $('#searchform').val()
                //     alert('else'+$('#searchform').val())
                // }
                if (settings.oPreviousSearch.sSearch != '') {
                    statussearch = true
                    searchtablewidth = 200
                    valuesearch = settings.oPreviousSearch.sSearch
                } else {
                    statussearch = false
                    searchtablewidth = 200
                    valuesearch = ''
                }
                $('#appendInfoDatatableCostControl').html(`
                <div class="row" style="background: #90CAF9; padding-left: 15px; padding-right: 15px;">
                    <div class="col-md-12">
                        Page
                        <button id="prevpagecostcontrol" onclick="prevpagecostcontrol()" class="btn btn-sm default prev `+prevdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-left">
                            </i>
                        </button>
                        <input type="text" id="valuepagecostcontrol" onkeyup="valuepagecostcontrol()" class="input-mini" value="`+valuepage+`" style="text-align: center; border: 0px; margin: 5px 5px 5px 5px">
                        <button id="nextpagecostcontrol" onclick="nextpagecostcontrol()" class="btn btn-sm default next `+nextdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-right">
                            </i>
                        </button> of <span class="pagination-panel-total">`+(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)+`</span>
                        <span class="seperator">|</span>
                        Rec/Page 
                        <select id="alengthmenucostcontrol" onchange="alengthmenucostcontrol()" name="table_master_length" aria-controls="table_master" class="input-inline input-xsmall input-sm recofdt" style="border: 0px; margin: 5px 5px 5px 5px">
                            `+options+`
                        </select>
                        <span class="seperator">|</span>Total `+settings._iRecordsTotal+` records
                        <input id="searchformcostcontrol" onkeyup="searchformcostcontrol()" value="`+valuesearch+`" type="search" class="form-control input-inline input-sm clicked" placeholder="" aria-controls="table_master" style="width: `+searchtablewidth+`px; position: absolute; right: 0px; top: 4px; height: 80%; border: 0;">
                        <div onclick="searchtablecostcontrol()" class="btn fa fa-search text-right" style="position: absolute; right: 0px; top: 4px; height: 80%; background: transparent; margin: 0px 0px 0px 0px; padding: 5px 0px 5px 0px">
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                </div>`)
                console.log('settings')
                console.log(settings)
                console.log(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)
                $('.alm').removeAttr('selected');
                $('#alm'+settings._iDisplayLength).attr('selected', 'selected');
                // console.log(valuepage)
                $('.dataTables_info').css('display', 'none')
                $('.dataTables_paginate').css('display', 'none')
            },
            "ajax": {
                "url": "/get_rab"+main.urlcode,
                "type": "POST",
                "dataType":'json',
                "data": {
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    'statusadd': statusadd,
                    'statusdetail': _statusdetail
                },
                "error": function(res) {
                    errorHandler(res);
                }
            },
            "pagingType": "input",
            "createdRow": function(row, data, dataIndex) {
                $(row).attr('onmouseover', 'showButtonAction(this, '+dataIndex+', 100)');
                $(row).attr('onmouseout', 'resetCss()');
            },
            "oLanguage":{
                "loadingRecords": "Please wait - loading...",
                "sProcessing": "<div id='loadernya'></div>",
                "sInfo": "&nbsp;|&nbsp;Total&nbsp;_TOTAL_ records",
                "sLengthMenu": "&nbsp;|&nbsp;Rec/Page&nbsp;_MENU_",
                "sInfoFiltered": "",
                "sInfoEmpty": "| No records found to show",
                "sEmptyTable": "No data available in table",
                "sZeroRecords": "No matching records found",
                "sSearch": "<button onclick='toggleButtonSearch()' class='btn flat dtfilter_search_btn' style='background-color: white'><i class='fa fa-search'></i></button>",
                "oPaginate": {
                    "sPrevious": "<i class='fa fa-angle-left'></i>",
                    "sNext": "<i class='fa fa-angle-right'></i> ",
                }
            },
            footerCallback: function ( row, data, start, end, display ) {
            }
        });
    }

    function getDatatableDetailPrev() {
        let idcardsupplier = $('#form-header #idcardsupplier').val();
        let idcompany = $('#form-header #idcompany').val();
        let iddepartment = $('#form-header #iddepartment').val();
        let idgudang = $('#form-header #idgudang').val();
        // let searchtypeprev = $('[name=searchtypeprev]:checked').val();
        let searchtypeprev = 2;
        let searchprev = $('#searchprev').val();
        let detailprev = new Array();
        $.each(JSON.parse(localStorage.getItem(main.tabledetail)), function(k, v) {
            let dponoid = '';
            let dpounitqty = '';
            $.each(v, function(k2, v2) {
                if (v2.name == 'noid') {
                    dponoid = v2.value;
                }
                if (v2.name == 'unitqty') {
                    dpounitqty = v2.value;
                }
            });
            detailprev.push({
                noid: dponoid,
                unitqty: dpounitqty,
            })
        });
        // console.log('detailprev')
        // console.log(detailprev)

        defaultOperation()
        let columns = <?= $columnsdetail ?>;
        let columndefs = <?= $columndefsdetail ?>;

        $('#table-detailprev').DataTable().destroy();
        mytabledetailprev = $('#table-detailprev').DataTable({
            "autoWidth": false,
            "pageLength": 10,
            "searching": true,
            "processing": true,
            "serverSide": true,
            "bInfo": true,
            "bFilter": false,
            "bLengthChange": false,
            "columns": [
                {mData:'checkbox',orderable:false,class:'text-center'},
                {mData:'no',orderable:false,class:'text-right'},
                {mData:'kodeprev'},
                {mData:'kodeinventor'},
                {mData:'namainventor'},
                {mData:'namainventor2'},
                {mData:'keterangan'},
                {mData:'unitqty',class:'text-right'},
                {mData:'unitqtysisa',class:'text-right'},
                {mData:'namasatuan'},
                {mData:'unitprice',class:'text-right',render:$.fn.dataTable.render.number(',','.',0,'Rp.')},
                {mData:'hargatotal',class:'text-right',render:$.fn.dataTable.render.number(',','.',0,'Rp.')},
                {mData:'namacurrency'},
                {mData:'action',orderable:false},
            ],
            "columnDefs": columndefs,
            "infoCallback": function( settings, start, end, max, total, pre ) {
                var options = '';
                $.each(settings.aLengthMenu, function(k, v) {
                    options += '<option value="'+v+'" id="alm'+v+'" class="alm">'+v+'</option>'
                })
                if ((end-start) == settings._iDisplayLength-1) {
                    var valuepage = end/settings._iDisplayLength;
                } else {
                    var valuepage = ((start-1)/settings._iDisplayLength)+1;
                }
                if (start == 1) {
                    var prevdisabled = 'disabled'
                } else {
                    var prevdisabled = '';
                }
                if (end == total) {
                    var nextdisabled = 'disabled'
                } else {
                    var nextdisabled = '';
                }
                // if (settings.oPreviousSearch.sSearch == $('#searchform').val()) {
                //     alert($('#searchform').val())
                //     var valuesearch = $('#searchform').val()
                // } else {
                //     var valuesearch = $('#searchform').val()
                //     alert('else'+$('#searchform').val())
                // }
                if (settings.oPreviousSearch.sSearch != '') {
                    statussearch = true
                    searchtablewidth = 200
                    valuesearch = settings.oPreviousSearch.sSearch
                } else {
                    statussearch = false
                    searchtablewidth = 200
                    valuesearch = ''
                }
                $('#appendInfoDatatableDetailPrev').html(`
                <div class="row" style="background: #90CAF9; padding-left: 15px; padding-right: 15px;">
                    <div class="col-md-12">
                        Page
                        <button type="button" onclick="prevpagedetailprev()" class="btn btn-sm default prev `+prevdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-left">
                            </i>
                        </button>
                        <input type="text" onkeyup="valuepagedetailprev()" class="input-mini" value="`+valuepage+`" style="text-align: center; border: 0px; margin: 5px 5px 5px 5px">
                        <button type="button" onclick="nextpagedetailprev()" class="btn btn-sm default next `+nextdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-right">
                            </i>
                        </button> of <span class="pagination-panel-total">`+(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)+`</span>
                        <span class="seperator">|</span>
                        Rec/Page 
                        <select id="alengthmenudetailprev" onchange="alengthmenudetailprev()" name="table_master_length" aria-controls="table_master" class="input-inline input-xsmall input-sm recofdt" style="border: 0px; margin: 5px 5px 5px 5px">
                            `+options+`
                        </select>
                        <span class="seperator">|</span>Total `+settings._iRecordsTotal+` records
                        <input id="searchformdetailprev" onkeyup="searchformdetailprev()" value="`+valuesearch+`" type="search" class="form-control input-inline input-sm clicked" placeholder="" aria-controls="table_master" style="width: `+searchtablewidth+`px; position: absolute; right: 0px; top: 4px; height: 80%; border: 0;">
                        <div onclick="searchtabledetailprev()" class="btn fa fa-search text-right" style="position: absolute; right: 0px; top: 4px; height: 80%; background: transparent; margin: 0px 0px 0px 0px; padding: 5px 0px 5px 0px">
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                </div>`)
                console.log('settings')
                console.log(settings)
                console.log(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)
                $('.alm').removeAttr('selected');
                $('#alm'+settings._iDisplayLength).attr('selected', 'selected');
                // console.log(valuepage)
                $('.dataTables_info').css('display', 'none')
                $('.dataTables_paginate').css('display', 'none')
            },
            "ajax": {
                "url": "/get_detailpi"+main.urlcode,
                "type": "POST",
                "dataType":'json',
                "data": {
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    'statusdetail': statusdetail,
                    'idcardsupplier': idcardsupplier,
                    'idcompany': idcompany,
                    'iddepartment': iddepartment,
                    'idgudang': idgudang,
                    'detailprev': JSON.stringify(detailprev),
                    'searchtypeprev': searchtypeprev,
                    'searchprev': searchprev,
                },
                "dataSrc": function(json) {
                    if (json.data.length == 1) {
                        chooseDetailPrev(
                            json.data[0].noid,
                            json.data[0].idmaster,
                            json.data[0].kodeprev,
                            json.data[0].idinventor,
                            json.data[0].kodeinventor,
                            json.data[0].namainventor,
                            json.data[0].namainventor2,
                            json.data[0].keterangan,
                            json.data[0].idgudang,
                            json.data[0].idcompany,
                            json.data[0].iddepartment,
                            json.data[0].idsatuan,
                            json.data[0].namasatuan,
                            json.data[0].konvsatuan,
                            json.data[0].unitqty,
                            json.data[0].unitqtysisa,
                            json.data[0].unitprice,
                            json.data[0].subtotal,
                            json.data[0].discountvar,
                            json.data[0].discount,
                            json.data[0].nilaidisc,
                            json.data[0].idtypetax,
                            json.data[0].idtax,
                            json.data[0].prosentax,
                            json.data[0].nilaitax,
                            json.data[0].hargatotal,
                            json.data[0].namacurrency
                        );
                    } else {
                        $('#modal-detailprev').modal('show')
                        return json.data;
                    }
                },
                "error": function(res) {
                    errorHandler(res);
                }
            },
            "pagingType": "input",
            "createdRow": function(row, data, dataIndex) {
                let right = statusdetail ? 128 : 128;
                $(row).attr('onmouseover', 'showButtonActionTabPrev(this, '+dataIndex+', '+right+')');
                $(row).attr('onmouseout', 'resetCssTabPrev()');
            },
            "oLanguage":{
                "loadingRecords": "Please wait - loading...",
                "sProcessing": "<div id='loadernya'></div>",
                "sInfo": "&nbsp;|&nbsp;Total&nbsp;_TOTAL_ records",
                "sLengthMenu": "&nbsp;|&nbsp;Rec/Page&nbsp;_MENU_",
                "sInfoFiltered": "",
                "sInfoEmpty": "| No records found to show",
                "sEmptyTable": "No data available in table",
                "sZeroRecords": "No matching records found",
                "sSearch": "<button onclick='toggleButtonSearch()' class='btn flat dtfilter_search_btn' style='background-color: white'><i class='fa fa-search'></i></button>",
                "oPaginate": {
                    "sPrevious": "<i class='fa fa-angle-left'></i>",
                    "sNext": "<i class='fa fa-angle-right'></i> ",
                }
            },
            footerCallback: function ( row, data, start, end, display ) {
                var api = this.api(), data;
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
    
                var sum = function ( i ) {
                    var value = 0;
                    $.each(api.column( i ).data(), function(k, v) {
                        var _split = v.split('.');
                        var value2 = '';
                        $.each(_split, function(k2, v2) {
                            value2 += v2
                        })
                        value += Number(value2)
                    })
                    return value
                }
                var addcommas = function addCommas(nStr) {
                    nStr += '';
                    var x = nStr.split('.');
                    var x1 = x[0];
                    var x2 = x.length > 1 ? '.' + x[1] : '';
                    var rgx = /(\d+)(\d{3})/;
                    while (rgx.test(x1)) {
                        x1 = x1.replace(rgx, '$1' + '.' + '$2');
                    }
                    return x1 + x2;
                }

                pageTotal = api
                    .column( 11, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                // Update footer
                // console.log('akak')
                // console.log(eval(api.column(9).data()[0]))
                $( api.column(2).footer() ).html(data.length);
                $( api.column(6).footer() ).html(api.column(6).data().reduce( function (a, b) { return parseInt(a)+parseInt(b); }, 0 ));
                $( api.column(7).footer() ).html(api.column(7).data().reduce( function (a, b) { return parseInt(a)+parseInt(b); }, 0 ));
                $( api.column(11).footer() ).html(api.column(11).data().reduce( function (a, b) { return pageTotal; }, 0 ));
                var $th = $(row).find('th').eq(11);
                $th.text( $.fn.dataTable.render.number(',', '.', 0, 'Rp.').display( $th.text() )) 
                // $( api.column(6).footer() ).html(data.length);
            }
        });
    }

    function getDatatableDetail(data) {
        defaultOperation()
        let columns = <?= $columnsdetail ?>;
        let columndefs = <?= $columndefsdetail ?>;

        $('#table-detail').DataTable().destroy();
        mytabledetail = $('#table-detail').DataTable({
            "autoWidth": false,
            "pageLength": 10,
            "searching": true,
            "processing": true,
            "serverSide": true,
            "bInfo": true,
            "bFilter": false,
            "bLengthChange": false,
            "columns": [
                {mData:'checkbox',orderable:false,class:'text-center'},
                {mData:'no',orderable:false,class:'text-right'},
                // {mData:'kodeprev'},
                {mData:'kodeinventor'},
                {mData:'namainventor'},
                {mData:'namainventor2'},
                {mData:'keterangan'},
                // {mData:'unitqty',class:'text-right'},
                // {mData:'unitqtysisa',class:'text-right'},
                // {mData:'namasatuan'},
                // {mData:'unitprice',class:'text-right',render:$.fn.dataTable.render.number(',','.',0,'Rp.')},
                // {mData:'hargatotal',class:'text-right',render:$.fn.dataTable.render.number(',','.',0,'Rp.')},
                // {mData:'namacurrency'},
                {mData:'action',orderable:false},
            ],
            "columnDefs": columndefs,
            "infoCallback": function( settings, start, end, max, total, pre ) {
                var options = '';
                $.each(settings.aLengthMenu, function(k, v) {
                    options += '<option value="'+v+'" id="alm'+v+'" class="alm">'+v+'</option>'
                })
                if ((end-start) == settings._iDisplayLength-1) {
                    var valuepage = end/settings._iDisplayLength;
                } else {
                    var valuepage = ((start-1)/settings._iDisplayLength)+1;
                }
                if (start == 1) {
                    var prevdisabled = 'disabled'
                } else {
                    var prevdisabled = '';
                }
                if (end == total) {
                    var nextdisabled = 'disabled'
                } else {
                    var nextdisabled = '';
                }
                // if (settings.oPreviousSearch.sSearch == $('#searchform').val()) {
                //     alert($('#searchform').val())
                //     var valuesearch = $('#searchform').val()
                // } else {
                //     var valuesearch = $('#searchform').val()
                //     alert('else'+$('#searchform').val())
                // }
                if (settings.oPreviousSearch.sSearch != '') {
                    statussearch = true
                    searchtablewidth = 200
                    valuesearch = settings.oPreviousSearch.sSearch
                } else {
                    statussearch = false
                    searchtablewidth = 200
                    valuesearch = ''
                }
                $('#appendInfoDatatableDetail').html(`
                <div class="row" style="background: #90CAF9; padding-left: 15px; padding-right: 15px;">
                    <div class="col-md-12">
                        Page
                        <button id="prevpagedetail" onclick="prevpagedetail()" class="btn btn-sm default prev `+prevdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-left">
                            </i>
                        </button>
                        <input type="text" id="valuepagedetail" onkeyup="valuepagedetail()" class="input-mini" value="`+valuepage+`" style="text-align: center; border: 0px; margin: 5px 5px 5px 5px">
                        <button id="nextpagedetail" onclick="nextpagedetail()" class="btn btn-sm default next `+nextdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-right">
                            </i>
                        </button> of <span class="pagination-panel-total">`+(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)+`</span>
                        <span class="seperator">|</span>
                        Rec/Page 
                        <select id="alengthmenudetail" onchange="alengthmenudetail()" name="table_master_length" aria-controls="table_master" class="input-inline input-xsmall input-sm recofdt" style="border: 0px; margin: 5px 5px 5px 5px">
                            `+options+`
                        </select>
                        <span class="seperator">|</span>Total `+settings._iRecordsTotal+` records
                        <input id="searchformdetail" onkeyup="searchformdetail()" value="`+valuesearch+`" type="search" class="form-control input-inline input-sm clicked" placeholder="" aria-controls="table_master" style="width: `+searchtablewidth+`px; position: absolute; right: 0px; top: 4px; height: 80%; border: 0;">
                        <div onclick="searchtabledetail()" class="btn fa fa-search text-right" style="position: absolute; right: 0px; top: 4px; height: 80%; background: transparent; margin: 0px 0px 0px 0px; padding: 5px 0px 5px 0px">
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                </div>`)
                console.log('settings')
                console.log(settings)
                console.log(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)
                $('.alm').removeAttr('selected');
                $('#alm'+settings._iDisplayLength).attr('selected', 'selected');
                // console.log(valuepage)
                $('.dataTables_info').css('display', 'none')
                $('.dataTables_paginate').css('display', 'none')
            },
            "ajax": {
                "url": "/get_detail"+main.urlcode,
                "type": "POST",
                "dataType":'json',
                "data": {
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    'data': data ? data : '[]',
                    'statusdetail': statusdetail,
                    'condition': JSON.stringify(main.condition[status.crudmaster])
                },
                "error": function(res) {
                    errorHandler(res);
                }
            },
            "pagingType": "input",
            "createdRow": function(row, data, dataIndex) {
                let right = statusdetail ? 45 : 45;
                $(row).attr('onmouseover', 'showButtonActionTab(this, '+dataIndex+', '+right+')');
                $(row).attr('onmouseout', 'resetCssTab()');
            },
            "oLanguage":{
                "loadingRecords": "Please wait - loading...",
                "sProcessing": "<div id='loadernya'></div>",
                "sInfo": "&nbsp;|&nbsp;Total&nbsp;_TOTAL_ records",
                "sLengthMenu": "&nbsp;|&nbsp;Rec/Page&nbsp;_MENU_",
                "sInfoFiltered": "",
                "sInfoEmpty": "| No records found to show",
                "sEmptyTable": "No data available in table",
                "sZeroRecords": "No matching records found",
                "sSearch": "<button onclick='toggleButtonSearch()' class='btn flat dtfilter_search_btn' style='background-color: white'><i class='fa fa-search'></i></button>",
                "oPaginate": {
                    "sPrevious": "<i class='fa fa-angle-left'></i>",
                    "sNext": "<i class='fa fa-angle-right'></i> ",
                }
            },
            footerCallback: function ( row, data, start, end, display ) {
                var api = this.api(), data;
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
    
                var sum = function ( i ) {
                    var value = 0;
                    $.each(api.column( i ).data(), function(k, v) {
                        var _split = v.split('.');
                        var value2 = '';
                        $.each(_split, function(k2, v2) {
                            value2 += v2
                        })
                        value += Number(value2)
                    })
                    return value
                }
                var addcommas = function addCommas(nStr) {
                    nStr += '';
                    var x = nStr.split('.');
                    var x1 = x[0];
                    var x2 = x.length > 1 ? '.' + x[1] : '';
                    var rgx = /(\d+)(\d{3})/;
                    while (rgx.test(x1)) {
                        x1 = x1.replace(rgx, '$1' + '.' + '$2');
                    }
                    return x1 + x2;
                }

                // pageTotal = api
                //     .column( 9, { page: 'current'} )
                //     .data()
                //     .reduce( function (a, b) {
                //         return intVal(a) + intVal(b);
                //     }, 0 );
                // Update footer
                // console.log('akak')
                // console.log(eval(api.column(9).data()[0]))
                $( api.column(2).footer() ).html(data.length);
                // $( api.column(7).footer() ).html(api.column(7).data().reduce( function (a, b) { return parseInt(a)+parseInt(b); }, 0 ));
                // $( api.column(8).footer() ).html(api.column(8).data().reduce( function (a, b) { return parseInt(a)+parseInt(b); }, 0 ));
                // $( api.column(11).footer() ).html(api.column(11).data().reduce( function (a, b) { return pageTotal; }, 0 ));
                // var $th = $(row).find('th').eq(11);
                // $th.text( $.fn.dataTable.render.number(',', '.', 0, 'Rp.').display( $th.text() )) 
                // $( api.column(6).footer() ).html(data.length);
            }
        });
    }

    function showModalDelete(noid) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You want to delete this?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            target: '#pagepanel'
        }).then((result) => {
            if (result.isConfirmed) {
                removeData(noid);
                Swal.fire({
                    icon: 'success',
                    title: 'Deleted!',
                    text: 'Delete data successfully.',
                    target: '#pagepanel',
                });
            }
        })
        return false;
        $('#remove-data').attr('onclick', 'removeData('+noid+')')
        $('#modal-delete').modal('show')
    }

    function hideModalDelete() {
        $('#modal-delete').modal('hide')
    }

    function showModalSendToOrder(noid, fromview=false) {
        // Swal.fire({
        //     title: 'Are you sure?',
        //     text: 'You wanna '+main.sendtonext+'?',
        //     icon: 'warning',
        //     showCancelButton: true,
        //     confirmButtonColor: '#3085d6',
        //     cancelButtonColor: '#d33',
        //     confirmButtonText: 'Yes',
        //     target: '#pagepanel'
        // }).then((result) => {
        //     if (result.isConfirmed) {
        //         sendToNext(noid);
        //         Swal.fire({
        //             icon: 'success',
        //             title: 'Success!',
        //             text: main.sendtonext+' successfully.',
        //             target: '#pagepanel',
        //         });
        //         if (fromview) {
        //             cancelData()
        //         }
        //     }
        // })
        // return false;
        $('#form-modal-stn #idcardsupplier-stn').val('').trigger('change')
        $('#send-to-next').attr('onclick', 'sendToNext('+noid+')')
        $('#modal-send-to-next').modal('show')
    }

    function hideModalSendToOrder() {
        $('#modal-send-to-next').modal('hide')
    }

    function showModalSNST(noid, idtypetranc, idstatustranc, statustranc, fromview=true) {
        if (idstatustranc == 4022) {
            $('#div-spknumber').show();
        } else {
            $('#div-spknumber').hide();
        }

        if (fromview) {
            $('#modal-snst .modal-title').text('SET TO '+statustranc);
            $('#btn-snst span').text('Set to '+statustranc);
            $('#btn-snst').attr('onclick', 'SNST('+noid+','+idstatustranc+',\''+statustranc+'\','+fromview+')')
        } else {
            $('#modal-snst .modal-title').text('SAVE TO '+statustranc);
            $('#btn-snst span').text('Save to '+statustranc);
            $('#btn-snst').attr('onclick', `saveData(`+idstatustranc+`, '`+statustranc+`', '`+$('#keterangan-snst').val()+`')`)
        }

        // Rules
        $('#modal-snst #keterangan-snst').on('keypress',function(e){if(e.keyCode==13){$('#modal-snst #btn-snst').focus();}})

        $('#idtypetranc-snst').val(idtypetranc);
        $('#form-modal-snst #keterangan-snst').val('');
        $('#modal-snst').modal('show')
        $('#modal-snst').on('shown.bs.modal', () => $('#modal-snst #keterangan-snst').focus())
    }

    function hideModalSNST() {
        $('#modal-snst').modal('hide')
        $('#form-modal-snst #idtypetranc-snst').val('').trigger('change')
        $('#form-modal-snst #keterangan-snst').val('');
    }

    function showModalDIV(noid, idtypetranc, idstatustranc) {
        $('#modal-div .modal-title').text('SET STATUS');
        $('#btn-div span').text('Set Status');
        $('#btn-div').attr('onclick', 'DIV('+noid+', '+idstatustranc+')');  
        $('#idtypetranc-div').val(idtypetranc);
        $('#modal-div #select-div').show();
        $('#modal-div #select-spcf').hide();
        $('#form-modal-div #idstatustranc-div').val('').trigger('change');
        $('#form-modal-div #keterangan-div').val('');
        $('#modal-div').modal('show')
    }

    function hideModalDIV() {
        $('#modal-div').modal('hide')
    }

    function showModalSAPCF(params) {
        let noid = params.noid;
        let idtypetranc = params.idtypetranc;
        let idstatustranc = params.idstatustranc;

        // Reset Select2
        $('#modal-sapcf #idstatustranc-sapcf').select2({
            templateResult: function(option) {
                var myOption = $('#modal-sapcf #idstatustranc-sapcf').find('option[value="' + option.id + '"]');
                if (option.id != idstatustranc) {
                        return option.text;
                }
            }
        });
        $('.select2').css('width', '100%');
        $('.select2 .selection .select2-selection').css('height', '35px');
        $('.select2 .selection .select2-selection .select2-selection__rendered').css('margin-top', '-6px');
        $('.select2 .selection .select2-selection .select2-selection__arrow').css('margin-top', '4px');

        
        $('#modal-sapcf').modal('show')
        $('#modal-sapcf #idstatustranc-sapcf').select2('focus')
        $('#modal-sapcf #idstatustranc-sapcf').on('select2:close',function(e){$('#modal-sapcf #keterangan-sapcf').focus()})
        
        $('#modal-sapcf .modal-title').text('SET STATUS');
        $('#btn-sapcf span').text('Set Status');
        $('#btn-sapcf').attr('onclick', 'SAPCF('+noid+', '+idstatustranc+')');  
        $('#idtypetranc-sapcf').val(idtypetranc);
        $('#modal-sapcf #idstatustranc-sapcf').val('').trigger('change');
        $('#modal-sapcf #keterangan-sapcf').val('');
    }

    function hideModalSAPCF() {
        $('#modal-sapcf').modal('hide')
    }

    function showModalSetPending(noid, idtypetranc) {
        $('#idtypetranc-pending').val(idtypetranc);
        $('#set-pending').attr('onclick', 'setPending('+noid+')')
        $('#modal-set-pending').modal('show')
    }

    function hideModalSetPending() {
        $('#modal-set-pending').modal('hide')
    }

    function showModalSetCanceled(noid, idtypetranc) {
        $('#idtypetranc-canceled').val(idtypetranc);
        $('#set-canceled').attr('onclick', 'setCanceled('+noid+')')
        $('#modal-set-canceled').modal('show')
    }

    function hideModalSetCanceled() {
        $('#modal-set-canceled').modal('hide')
    }

    function showModalLog(noid, kode, typetranc) {
        getDatatableLog(noid, kode)
        $('#modal-log #titlelog-log').text(typetranc)
        $('#modal-log #titlekode-log').text(kode)
        $('#modal-log').modal('show')
    }

    function hideModalLog() {
        $('#modal-log').modal('hide')
    }

    function showModalRAB() {
        getDatatableRAB(statusdetail)
        $('#modal-rab').modal('show')
    }

    function hideModalRAB() {
        $('#modal-rab').modal('hide')
    }

    function showModalDetailPrev() {
        let searchtypeprev = $('[name=searchtypeprev]:checked');
        let idcardsupplier = $('#form-header #idcardsupplier :selected');

        if (searchtypeprev.val() == 1) {
            if (idcardsupplier.val()) {
                $('#modal-detailprev .supplier-title').text(idcardsupplier.text())
                $('#modal-detailprev #title-prev').show();
                $('#modal-detailprev #title-inv').hide();
                getDatatableDetailPrev()
                $('#modal-detailprev').modal('show')
            } else {
                $('#form-header #idcardsupplier').select2('focus');
                $.notify({
                    message: 'Supplier harus dipilih!'
                },{
                    type: 'warning'
                });
            }
        } else {
            $('#modal-detailprev #title-inv').show();
            $('#modal-detailprev #title-prev').hide();
            getDatatableDetailPrev()
        }
    }

    function hideModalDetailPrev() {
        $('#modal-detailprev').modal('hide')
    }

    function showModalTanggal() {
        $('#form-modal-tanggal #tanggaltax-modal').focus()
        $('#modal-tanggal').modal('show')
    }

    function hideModalTanggal() {
        $('#modal-tanggal').modal('hide')
    }

    function showModalSupplier() {
        $('#modal-supplier').modal('show')
    }

    function hideModalSupplier() {
        $('#modal-supplier').modal('hide')
    }

    function showModalDetail() {
        $('#modal-detail').modal('show')
        $('#form-modal-detail #idinventor-modal').select2('focus')
    }

    function hideModalDetail() {
        $('#modal-detail').modal('hide')
    }
    
    function currency(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return 'Rp.'+ x1 + x2;
    }

    /////////

    var maincms = {};
    var ready = false;
    var statusadd = true;
    var statusdetail = false;
    var statusaddtab = [];
    var editid = '';
    var nextnoid = '';
    var nextnoidtab = [];
    var nownoid = '';
    var nownoidtab = '';
    var nowmaintabletab = '';
    var fieldnametab = new Array();
    var tablelookuptab = new Array();
    var input_data = new Array();
    var mainonchange = new Array();
    var maintable = new Array();
    var values = $("input[name='panel[]']").map(function(){return $(this).val();}).get();
    var values2 = $("#slider").val();
    var values3 = $("#tabel1").val();
    var select_all = false;
    var select_all_tab = new Array();
    var cursor_top = 0;
    var selecteds = new Array();
    var another_selecteds = new Array();

    var selectcheckboxmall = false;
    var selectcheckboxm = new Array();
    var selectcheckboxman = new Array();
    localStorage.setItem('sc-m-a', 'false');
    localStorage.setItem('sc-m', JSON.stringify(new Array()));
    localStorage.setItem('sc-m-an', JSON.stringify(new Array()));
    // var divform = '';
    var my_table = '';
    var mytable = '';
    var mytabtable = new Array();
    var groupbtn = '';
    var selected_filter = {
        operand: [],
        fieldname: [],
        operator: [],
        value: [],
    };
    var selectedfilter = {
        'idstatustranc': null,
        'idcompany': null,
    };
    var datefilter = {
        'code': 'a',
        'custom': {
            'status': false,
            'from': $('#did-from').val(),
            'to': $('#did-to').val(),
        }
    };
    var datefilter2 = {
        'from': null,
        'to': null
    };
    var filter_tabs = new Array();
    var selected_filter_tabs = 0;
    var filtercontenttabs = {
        filtercontentgroup: new Array(),
        filtercontentbody: new Array(),
        filtercontentrow: new Array(),
    }
    var colsorted = [0,1];
    var roles = {
        fieldname: new Array(),
        newhidden: new Array(),
        edithidden: new Array(),
    };
    var cmsfilterbutton = new Array()
    var tab_datatable = new Array()

    var functionfooter = new Array();
    var functionfootertab = new Array();
    var searchtablewidth = 200;
    var tab_searchtablewidth = new Array();
    var statussearch = '';
    var tab_statussearch = new Array();
    var valuesearch = '';
    var tab_valuesearch = new Array();

    var maincms_widget = new Array();
    var maincms_widgetgrid = new Array();

    var noids = new Array();
    var noids_tab = new Array();

    function selectCheckboxMAll() {
        let mselectall = $('#checkbox-m-all').is(':checked');
        let mselect = $('.checkbox-m');
        mselect.prop('checked', mselectall);

        if (mselectall == true) {
            $('#table_master .odd').css('background-color', '#90CAF9');
            $('#table_master .even').css('background-color', '#90CAF9');
            $('#table_master .checkbox-m').val('TRUE');
            // selectcheckboxmall = true;
            // selectcheckboxm = new Array();
            // selectcheckboxm = selectcheckboxman;
            localStorage.setItem('sc-m-a', 'true');
            localStorage.setItem('sc-m', localStorage.getItem('sc-m-an'));
        } else {
            $('#table_master .odd').css('background-color', '#ffffff');
            $('#table_master .even').css('background-color', '#BBDEFB');
            $('#table_master .checkbox-m').val('FALSE');
            localStorage.setItem('sc-m-a', 'false');
            localStorage.setItem('sc-m', JSON.stringify(new Array()));
        }
    }

    function selectCheckboxM(index) {
        let checkbox = $('#checkbox-m-'+index);

        if (checkbox.val() == 'FALSE') {
            $('#table_master .row-m-'+index).css('background-color', '#90CAF9');
            $('#table_master #checkbox-m-'+index).val('TRUE');
            let scm = JSON.parse(localStorage.getItem('sc-m'));
            selectcheckboxm.push(index);
            scm.push(index);
            localStorage.setItem('sc-m', JSON.stringify(scm));
        } else {
            if (index % 2 == 0) {
                $('#table_master .row-m-'+index).css('background-color', '#ffffff');
            } else {
                $('#table_master .row-m-'+index).css('background-color', '#BBDEFB');
            }
            $('#table_master #checkbox-m-'+index).val('FALSE')
            var replaceselecteds = new Array();
            $.each(selectcheckboxm, function(k, v) {
                if (v != index) {
                    replaceselecteds.push(v)
                }
            })
            selectcheckboxm = replaceselecteds

            let replacescm = new Array();
            $.each(JSON.parse(localStorage.getItem('sc-m')), function(k, v) {
                if (v != index) {
                    replacescm.push(v)
                }
            })
            localStorage.setItem('sc-m', JSON.stringify(replacescm));
        }
        selectcheckboxmall = false;
        localStorage.setItem('sc-m-a', 'false');
        $('#checkbox-m-all').prop('checked', false);
        // console.log(selecteds)
    }

    function selectAllCheckboxTab(panel_noid) {
        var checkboxes = $('[name="checkbox-'+panel_noid+'[]"]');
        checkboxes.prop('checked', $('#select_all_'+panel_noid).is(':checked'));

        if (select_all_tab[panel_noid] == false) {
            select_all_tab[panel_noid] = true;
            $('#tab-table-'+panel_noid+' .odd').css('background-color', '#90CAF9');
            $('#tab-table-'+panel_noid+' .even').css('background-color', '#90CAF9');
            $('#tab-table-'+panel_noid+' .checkbox-'+panel_noid).val('TRUE');
            another_selecteds[panel_noid] = noids_tab[panel_noid];
        } else {
            select_all_tab[panel_noid] = false;
            $('#tab-table-'+panel_noid+' .odd').css('background-color', '#ffffff');
            $('#tab-table-'+panel_noid+' .even').css('background-color', '#BBDEFB');
            $('#tab-table-'+panel_noid+' .checkbox-'+panel_noid).val('FALSE');
            another_selecteds[panel_noid] = new Array();
        }
        console.log('another_selecteds')
        console.log(another_selecteds)
    }
    
    function changeCheckbox(index, id) {
        var checkbox = $('.checkbox'+index);
        if (checkbox.val() == 'FALSE') {
            $('.row'+index).css('background-color', '#90CAF9');
            $('.checkbox'+index).val('TRUE');
            selecteds.push(id);
        } else {
            if (index % 2 == 0) {
                $('.row'+index).css('background-color', '#ffffff');
            } else {
                $('.row'+index).css('background-color', '#BBDEFB');
            }
            $('.checkbox'+index).val('FALSE')
            var replace_selecteds = new Array();
            $.each(selecteds, function(k, v) {
                if (v != id) {
                    replace_selecteds.push(v)
                }
            })
            selecteds = replace_selecteds
        }
        select_all = false;
        $('#select_all').prop('checked', false);
        // console.log(selecteds)
    }

    function changeCheckboxTab(panel_noid, index, id) {
        var checkbox = $('.checkbox-'+panel_noid+'-'+index);
        // alert(checkbox.val())
        if (checkbox.val() == 'FALSE') {
            $('#tab-table-'+panel_noid+' .row'+index).css('background-color', '#90CAF9');
            $('#tab-table-'+panel_noid+' .checkbox-'+panel_noid+'-'+index).val('TRUE');
            another_selecteds[panel_noid].push(id);
        } else {
            if (index % 2 == 0) {
                $('#tab-table-'+panel_noid+' .row'+index).css('background-color', '#ffffff');
            } else {
                $('#tab-table-'+panel_noid+' .row'+index).css('background-color', '#BBDEFB');
            }
            $('#tab-table-'+panel_noid+' .checkbox-'+panel_noid+'-'+index).val('FALSE')
            // another_selecteds.pop(id);
            // delete another_selecteds[index]
            var replace_another_selecteds = new Array();
            $.each(another_selecteds[panel_noid], function(k, v) {
                if (v != id) {
                    replace_another_selecteds.push(v)
                }
            })
            another_selecteds[panel_noid] = replace_another_selecteds
        }
        select_all_tab[panel_noid] = false;
        $('#select_all_'+panel_noid).prop('checked', false);
        console.log('another_selecteds')
        console.log(another_selecteds)
    }

    function mouseOver(element_id, color) {
        $('#'+element_id).css('background-color', color);
        $('#'+element_id).css('color', 'white');
    }

    function mouseOut(element_id, color) {
        $('#'+element_id).removeAttr('style');
        $('#'+element_id).css('background-color', color);
        $('#'+element_id).css('color', 'white');
    }

    function showButtonAction(element, index, right=28) {
        var rect = element.getBoundingClientRect();

        this.resetCss()

        // $('.dataTable tbody td').css('height', '100px !important')
        // $('.dataTable tbody td:nth-child('+index+')').css('background', 'black')
        // $('.my_row').css('height', 35)
        $('.button-action-'+index).css('position', 'fixed')
        $('.button-action-'+index).css('top', parseInt(rect.y+(5)))
        $('.button-action-'+index).css('right', right+'px')
        // $('.button-action-'+index).css('overflow', 'hidden')

        // console.log(parseInt(rect.y+8))
        // console.log('Element is ' + offset + ' vertical pixels from <body>');
    }

    function showButtonActionTab(element, index, right=38) {
        var rect = element.getBoundingClientRect();

        this.resetCssTab()

        $('.button-action-tab-'+index).css('position', 'fixed')
        $('.button-action-tab-'+index).css('top', parseInt(rect.y+5))
        $('.button-action-tab-'+index).css('right', right+'px')
        // $('.button-action-'+index).css('overflow', 'hidden')

        // console.log(parseInt(rect.y+8))
        // console.log('Element is ' + offset + ' vertical pixels from <body>');
    }

    function showButtonActionTabPrev(element, index, right=38) {
        var rect = element.getBoundingClientRect();

        this.resetCssTab()

        $('.button-action-dprev-'+index).css('position', 'fixed')
        $('.button-action-dprev-'+index).css('top', parseInt(rect.y+5))
        $('.button-action-dprev-'+index).css('right', right+'px')
        // $('.button-action-'+index).css('overflow', 'hidden')

        // console.log(parseInt(rect.y+8))
        // console.log('Element is ' + offset + ' vertical pixels from <body>');
    }

    function resetCss() {
        $('.button-action').removeAttr('style')
    }

    function resetCssTab() {
        $('.button-action-tab').removeAttr('style')
    }

    function resetCssTabPrev() {
        $('.button-action-dprev').removeAttr('style')
    }

    function mouseoverAddData(element_id) {
        $('#'+element_id).css('background', '#337AB7')
    }

    function mouseoutAddData(element_id) {
        $('#'+element_id).css('background', '#3598dc')
    }
    
    function openFullscreen() {
        document.documentElement.requestFullscreen().catch((e) => {console.log(e)})
    }

    function getFullscreenElement() {
        return document.fullscreenElement
            || document.webkitFullscreenElement
            || document.mozFullscreenElement
            || document.msFullscreenElement;
    }

    function readyFullscreen() {
        if (localStorage.getItem(main.table+'-fullscreen') == 'true') {
            $('#tool_fullscreen i').removeClass('icon-fullscreen');
            $('#tool_fullscreen i').addClass('icon-remove-sign');
            document.getElementById('pagepanel').requestFullscreen().catch((e) => {})
        } else {
            $('#tool_fullscreen i').removeClass('icon-remove-sign');
            $('#tool_fullscreen i').addClass('icon-fullscreen');
            document.exitFullscreen();
        }
    }

    function toggleFullscreen() {
        if (localStorage.getItem(main.table+'-fullscreen') == 'true') {
            localStorage.setItem(main.table+'-fullscreen', 'false');
        } else {
            localStorage.setItem(main.table+'-fullscreen', 'true');
        }
        readyFullscreen();
        
        return false;

        if (getFullscreenElement() || localStorage.setItem(main.table+'-fullscreen', 'false') == 'false') {
            $('#tool_fullscreen i').removeClass('icon-remove-sign');
            $('#tool_fullscreen i').addClass('icon-fullscreen');
            localStorage.setItem(main.table+'-fullscreen', 'false');
            document.exitFullscreen();
        } else {
            $('#tool_fullscreen i').removeClass('icon-fullscreen');
            $('#tool_fullscreen i').addClass('icon-remove-sign');
            localStorage.setItem(main.table+'-fullscreen', 'true');
            document.getElementById('pagepanel').requestFullscreen().catch((e) => {})
        }
    }

    function filterDate(code) {
        datefilter.code = code;
        if (code == 'cr') {
            datefilter.custom.status = true;
            let getfrom = $('#did-from').val().split('-');
            let getto = $('#did-to').val().split('-');
            let from = getfrom[2]+'-'+getfrom[1]+'-'+getfrom[0];
            let to = getto[2]+'-'+getto[1]+'-'+getto[0];
            let f = moment(from).format('MMM DD, YYYY');
            let t = moment(to).format('MMM DD, YYYY');
            let span = f+' - '+t;        
            $('#dropdown-date-filter span').text(span);
        } else {
            $('#dropdown-date-filter span').text($('#did-'+code).attr('data-date'));
        }
        main.datefilterdefault = code;
        hoverFilterDate(code);
        getDatatable();
    }

    function changeCustomRange() {
        datefilter.custom.from = $('#did-from').val();
        datefilter.custom.to = $('#did-to').val();
    }

    $('#did-from').on('change',function(e){$('#did-to').focus()})

    function hoverFilterDate(code) {
        $('.dropdown-item-date span').css('background-color','#f5f5f5');
        $('.dropdown-item-date span').css('color','#08c');
        $('#did-'+code+' span').css('background-color','#4b8df8')
        $('#did-'+code+' span').css('color','white')

        $('#did-'+main.datefilterdefault+' span').css('background-color','#4b8df8')
        $('#did-'+main.datefilterdefault+' span').css('color','white')
    }

    function hoverOutFilterDate(code) {
        if (code != main.datefilterdefault) {
            $('#did-'+code+' span').css('background-color','#f5f5f5')
            $('#did-'+code+' span').css('color','#08c')
        }
    }
    
    $(document).ready(() => {
        showLoading();
        // setDefaultToggleAside();

        // $('#kt_breadcrumb').attr('style', `width: ${screen.width-265}px`);

        $('#from-departement').hide(); // Hide form
        $('#tool-kedua').hide(); // Hide cancel
        // $('.datepicker').datepicker({ format: 'd-m-yyyy' });
        // $('.datepicker2').datepicker({ format: 'd-m-yyyy' });
        $('.container').css('display', 'block'); // Show data
        
        customDocumentForm(); // Build CDF
        $('.modal.fade').appendTo('#pagepanel'); // Add modal on mode fullscreen
        ('<?= $triggercreate ?>') ? $('#btn-add-data').trigger('click') : hideLoading(); // Trigger click
        getDatatable();
        autoSearch();
        readyFullscreen();
        
        ready = true;
    });

    function autoSearch() {
        mytable.search('<?= @$_GET["s"]?>').draw('page');
    }

    window.onscroll = function() {
        fSticky1()
        fSticky2()
        // fSticky3()
    };

    let headersticky1 = document.getElementById("header-sticky-1");
    let headersticky2 = document.getElementById("header-sticky-2");
    let headersticky3 = document.getElementById("header-sticky-3");
    let sticky1 = headersticky1.offsetTop;
    let sticky2 = headersticky2.offsetTop;
    let sticky3 = headersticky3.offsetTop;

    function fSticky1() {
        if (window.pageYOffset > sticky1+150) {
            headersticky1.classList.add("sticky-1");
        } else {
            headersticky1.classList.remove("sticky-1");
        }
    }

    function fSticky2() {
        if (window.pageYOffset > sticky2+450) {
            headersticky2.classList.add("sticky-2");
        } else {
            headersticky2.classList.remove("sticky-2");
        }
    }

    function fSticky3() {
        if (window.pageYOffset > sticky3+600) {
            headersticky3.classList.add("sticky-3");
        } else {
            headersticky3.classList.remove("sticky-3");
        }
    }

    const setDefaultToggleAside = () => {
        localStorage.setItem('minimize-aside', JSON.stringify(false));
        $('#kt_breadcrumb').css('width', `${screen.width-265}px`);
    }
    
    const toggleAside = () => {
        if (JSON.parse(localStorage.getItem('minimize-aside')) == true) {
            localStorage.setItem('minimize-aside', JSON.stringify(false));
            $('#kt_breadcrumb').css('width', `${screen.width-265}px`);
        } else {
            localStorage.setItem('minimize-aside', JSON.stringify(true));
            $('#kt_breadcrumb').css('width', `${screen.width-70}px`);
        }
    }

    let cdf_istabinfo = ''; //details/attributes
    let cdf_checks = [];
    let cdf_selecteds = {};
    let cdf_show = 0;
    let cdf_btn_show = `<a id="cdf_btn_show_prev" class="btn btn-primary btn-sm" onclick="CDFshow({type:'prev'})"><i class="icon-arrow-left"></i></a>
                        <a id="cdf_btn_show_next" class="btn btn-primary btn-sm" onclick="CDFshow({type:'next'})"><i class="icon-arrow-right"></i></a>`;
    let cdf_onselect2 = false;

    function CDFshowButton(params) {
        $('#fm-btn-action-'+params.elementid).animate({top: '0px'});
    }
    
    function CDFhideButton(params) {
        // $('#fm-btn-action-'+params.elementid).animate({top: '-40px'});
    }

    function CDFshowModalDocumentViewer(params) {
        if (cdf_checks.length > 0) {
            $.each(cdf_checks, function(k, v) {
                $('#fm_contentitems-'+params.elementid+'-'+v).css('box-shadow', '0px 0px 5px 2px #9a12b3');
            })
        } else {
            if (params.refresh) {
                $('#cdf-upload-'+params.elementid).trigger('click')
            }
        }

        $('#modal-document-viewer-'+params.elementid).modal('show');
    }

    function CDFhideModalDocumentViewer(params) {
        $('#modal-document-viewer-'+params.elementid).modal('hide');
    }

    function CDFtoggleFmBtn(params) {
        let elementid = params.elementid;
        let type = params.type;
        let where = params.where ? params.where : 0;
        CDFgetFile({elementid:elementid,whereformat:where,wheresearch:$('#cdf-search').val()});
        $('.fm_panelcontent').hide();
        // $('#fm_meta-'+elementid).hide();

        var id = '';
        var id2 = '';
        if (type == 'upload') {
            id = '#content_upload-'+elementid;
            id2 = '#cdf-upload-'+elementid+' span';
        } else if (type == 'myfiles') {
            id = '#content_myfile-'+elementid;
            id2 = '#cdf-myfiles-'+elementid+' span';
        } else if (type == 'library') {
            id = '#content_cdf-library-'+elementid;
            id2 = '#cdf-library-'+elementid+' span';
        }

        $('.fm_btnlibrary a span').hide()
        $(id2).css('display', 'inline-block');
        $(id).show();
    }

    function CDFchooseFile(params) {
        $('#fm-btn-action-'+params.elementid).html(`<div class="fm-btn-action">
            <div class="btn btn-sm green btn_hide" id="cdf-change" onclick="CDFshowModalDocumentViewer({elementid:'`+params.elementid+`',refresh:true})">
                <span id="icon">
                    <i class="fa fa-edit text-light"></i>
                </span> 
                <span id="caption">Change</span>
            </div>
            <div class="btn btn-sm blue btn_hide" id="view_img" style="" onclick="CDFshowPreviewFile({elementid:'`+params.elementid+`',urlfile:'`+params.urlfile+`',title:'`+params.title+`'})">
                <i class="fa fa-search text-light"></i>
            </div>
            <div class="btn btn-sm red btn_hide" id="clear_img" style="" onclick="CDFremoveFile({elementid:'`+params.elementid+`'})">
                <i class="fa fa-trash text-light"></i>
            </div>
        </div>`);
        $('#content_image-'+params.elementid+' .fm-image-content img').attr('src', params.urlfile);
        $('#title_img-'+params.elementid).text(params.stitle);
        $('#modal-document-viewer-'+params.elementid).modal('hide');
        $('#'+params.elementid).val(params._noid);
    }

    function CDFremoveFile(params) {
        $('#fm-btn-action-'+params.elementid).html(`<div class="btn btn-sm green btn_hide" id="iddmslogo" onclick="CDFshowModalDocumentViewer({elementid:'`+params.elementid+`'})">
            <span id="icon">
                <i class="fa fa-plus text-light"></i>
            </span> 
            <span id="caption">File Manager</span>
        </div>`);
        $('#content_image-'+params.elementid).html(`<div class="modal-image-content" style="width: 100%; height: inherit; text-align: -webkit-center">
            <div class="fm-image-content" style="vertical-align: middle; display: table-cell; width: 100%; height: inherit; text-align: -webkit-center">
                <img src="`+location.origin+`/filemanager/default/noimage.png" style="max-width:100%;max-height:200px;">
            </div>
        </div>`);
        $('#title_img-'+params.elementid).text('File Manager');
        $('#'+params.elementid).val('');
    }

    function CDFgetFile(params) {
        let elementid = params.elementid;
        let whereformat = params.whereformat;
        let wheresearch = params.wheresearch;
        var domain = location.origin;

        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            },
            url: "/cdf_get"+main.urlcode,
            dataType: "JSON",
            data:{
                "_token":$('meta[name="csrf-token"]').attr('content'),
                "table": values3,
                "whereformat": whereformat,
                "wheresearch": wheresearch
            },
            success: function(response) {
                console.log('asek');
                console.log(response);
                var html = '';
                for (var i = 0; i<response.length; i++) {
                    let preview = '';
                    let thumbnail = '';
                    if (response[i]['fileextention'] == 'PNG' || response[i]['fileextention'] == 'JPG' || response[i]['fileextention'] == 'JPEG' || response[i]['fileextention'] == 'GIF') {
                        preview = domain+`/filemanager/preview/`+response[i]['filename'];
                        thumbnail = domain+`/filemanager/thumb/`+response[i]['filename'];
                    } else {
                        preview = domain+`/`+response[i]['fileimage'];
                        thumbnail = domain+`/`+response[i]['fileimage'];
                    }
                    html += `<div class="col-md-2 col-xs-4 fm_content_grid_items" id="fm_content_grid_item" style="margin-bottom: 4px; padding: 4px; text-align: -webkit-center; height: 100%" onmouseover="CDFmouseoverGridItem('#content_myfile-`+elementid+` .row .fm_content .row #panelcontent .row .fm_content_grid_items #fm_contenttitle-`+response[i]['noid']+`', '#content_myfile-`+elementid+` .row .fm_content .row #panelcontent .row .fm_content_grid_items #fm_contenttitle_action-`+response[i]['noid']+`')" onmouseout="CDFmouseoutGridItem('#content_myfile-`+elementid+` .row .fm_content .row #panelcontent .row .fm_content_grid_items #fm_contenttitle-`+response[i]['noid']+`', '#content_myfile-`+elementid+` .row .fm_content .row #panelcontent .row .fm_content_grid_items #fm_contenttitle_action-`+response[i]['noid']+`')">
                                <div class="fm_contentitems" id="fm_contentitems-`+elementid+`-`+response[i]['noid']+`" style="height: 139px; overflow: hidden;  border: 1px solid grey" onclick="CDFselectContentItem({elementid:'`+elementid+`',_noid:`+response[i]['noid']+`,urlfile:'`+thumbnail+`',nama:'`+response[i]['nama']+`',cdf_nama:'`+response[i]['cdf_nama']+`',cdf_keterangan:'`+response[i]['cdf_keterangan']+`',cdf_ishardcopy:'`+response[i]['cdf_ishardcopy']+`',cdf_ispublic:'`+response[i]['cdf_ispublic']+`',cdf_doexpired:'`+response[i]['cdf_doexpired']+`',cdf_doreviewlast:'`+response[i]['cdf_doreviewlast']+`',cdf_doreviewnext:'`+response[i]['cdf_doreviewnext']+`',title:'`+response[i]['filename']+`',size:'`+response[i]['filesize']+`',type:'`+response[i]['fileextention']+`',modified:'`+response[i]['lastupdate']+`',created:'`+response[i]['docreate']+`',ispublic:'`+response[i]['ispublic']+`',classicon:'`+response[i]['classicon']+`',classcolor:'`+response[i]['classcolor']+`',filetag:'`+(response[i]['filetag']==null?'':response[i]['filetag'])+`'})">
                                    <span class="badge badge-null badge-roundless" style="display: none;">UNSPECIFIED</span>
                                    <img src="`+thumbnail+`" class="image_ori" style="height: 130px" onclick="CDFselectContentItem({elementid:'`+elementid+`',_noid:`+response[i]['noid']+`,urlfile:'`+thumbnail+`',nama:'`+response[i]['nama']+`',cdf_nama:'`+response[i]['cdf_nama']+`',cdf_keterangan:'`+response[i]['cdf_keterangan']+`',cdf_ishardcopy:'`+response[i]['cdf_ishardcopy']+`',cdf_ispublic:'`+response[i]['cdf_ispublic']+`',cdf_doexpired:'`+response[i]['cdf_doexpired']+`',cdf_doreviewlast:'`+response[i]['cdf_doreviewlast']+`',cdf_doreviewnext:'`+response[i]['cdf_doreviewnext']+`',title:'`+response[i]['filename']+`',size:'`+response[i]['filesize']+`',type:'`+response[i]['fileextention']+`',modified:'`+response[i]['lastupdate']+`',created:'`+response[i]['docreate']+`',ispublic:'`+response[i]['ispublic']+`',classicon:'`+response[i]['classicon']+`',classcolor:'`+response[i]['classcolor']+`',filetag:'`+(response[i]['filetag']==null?'':response[i]['filetag'])+`'})">
                                </div>
                                <div class="fm_contenttitle" id="fm_contenttitle-`+response[i]['noid']+`" onclick="CDFselectContentItem({elementid:'`+elementid+`',_noid:`+response[i]['noid']+`,urlfile:'`+thumbnail+`',nama:'`+response[i]['nama']+`',cdf_nama:'`+response[i]['cdf_nama']+`',cdf_keterangan:'`+response[i]['cdf_keterangan']+`',cdf_ishardcopy:'`+response[i]['cdf_ishardcopy']+`',cdf_ispublic:'`+response[i]['cdf_ispublic']+`',cdf_doexpired:'`+response[i]['cdf_doexpired']+`',cdf_doreviewlast:'`+response[i]['cdf_doreviewlast']+`',cdf_doreviewnext:'`+response[i]['cdf_doreviewnext']+`',title:'`+response[i]['filename']+`',size:'`+response[i]['filesize']+`',type:'`+response[i]['fileextention']+`',modified:'`+response[i]['lastupdate']+`',created:'`+response[i]['docreate']+`',ispublic:'`+response[i]['ispublic']+`',classicon:'`+response[i]['classicon']+`',classcolor:'`+response[i]['classcolor']+`',filetag:'`+(response[i]['filetag']==null?'':response[i]['filetag'])+`'})" style="text-align: center; height: 23px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis; color: #fff; bottom: 4px; position: absolute; width: calc(100% - 8px); padding: 4px; background: rgba(0, 0, 0, .7); display: block; font-size: 80%; -webkit-transition: all 0.5s ease;">
                                    `+response[i]['nama']+`
                                    <div class="fm_contenttitle_action-`+response[i]['noid']+`" id="fm_contenttitle_action-`+response[i]['noid']+`" style="position: absolute; bottom: 4px; left: -200%; right: 0; transition: all 0.5s ease 0s;">
                                        <a type="button" class="btn btn-sm purple tooltips" data-container="body" data-placement="top" data-original-title="Preview" id="preview" onclick="CDFshowPreviewFile({elementid:'`+elementid+`',urlfile:'`+preview+`',title:'`+response[i]['filename']+`'})">
                                            <i class="fa fa-desktop text-light"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>`
                }
                $('#content_myfile-'+elementid+' .row .fm_content .row #panelcontent .row').html(html)
                // alert(response)
                // console.log('response')
                // console.log(response)

                $.each(cdf_checks, function(k, v) {
                    $('#fm_contentitems-'+params.elementid+'-'+v).css('box-shadow', '0px 0px 5px 2px #9a12b3');
                })
                
            }
        })
    }

    function CDFeditFile(params) {
        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            },
            url: "/cdf_edit"+main.urlcode,
            dataType: "JSON",
            data:{
                "_token":$('meta[name="csrf-token"]').attr('content'),
                "table": values3,
                "noid": params._noid,
                "filename": $('#attribute_nama-'+params.elementid).val(),
                "filestatus": $('#attribute_status-'+params.elementid).val(),
                "filetag": $('#attribute_tag-'+params.elementid).val(),
            },
            success: function(response) {
                alert(response.message)
                CDFgetFile({elementid:params.elementid,whereformat:0,wheresearch:$('#cdf-search').val()});
            }
        })
        // alert($('#attribute_tag-'+noid).val())
    }

    function CDFafterPickFiles(params) {
        var filename = $('#'+params.id).val().split('\\')[2];
        // console.log('afterpickfiles')
        // console.log($('#'+id).val())
        $("#content_upload_files").hide();
        $("#content_upload_files2").show();
        $("#content_upload_files2").html(`<div class="col-md-12" style="padding:20px;">
            <div class="row">
                <div class="col-md-9">
                    <div class="form-group">
                        <label class="control-label ">Filename</label>
                        <div class="">
                            <input name="" class="falignleft form-text form-control valid" value="`+filename+`" id="filename" type="text" placeholder="Filename">
                        </div>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <label class="control-label ">Public</label>
                        <div class="">
                            <div class="checker" id="uniform-ispublic">
                                <span>
                                    <input name="" class="form-checkbox" id="ispublic" type="checkbox" placeholder="Public">
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <div class="btn btn-sm purple" id="cdf_upload" onclick="CDFuploadFile()" style="margin-top:30px;">
                            <i class="fa fa-reply text-light"></i> Upload
                        </div>
                        <div class="btn btn-sm red" id="cdf_clear" onclick="CDFclearFile()" style="margin-top:30px;">
                            <i class="fa fa-trash text-light"></i> Cancel
                        </div>
                    </div>
                </div>
            </div>
        </div>`);
    }

    function CDFselectContentItem(params) {
        if (cdf_istabinfo == '') {
            $('#cdf-btntabdetails').trigger('click');
            cdf_istabinfo = 'details';
        }

        $('#fm_meta_filename-'+params.elementid+' strong').text(params.title);
        $('#fm_meta_image-'+params.elementid+' img').attr('src', params.urlfile);
        $('#fm_meta_size-'+params.elementid).text(params.size != 'null' ? params.size+' KB' : '-');
        $('#fm_meta_type-'+params.elementid).text(params.type != 'null' ? params.type : '-');
        $('#fm_meta_modified-'+params.elementid).text(params.modified != 'null' ? params.modified : '-');
        $('#fm_meta_created-'+params.elementid).text(params.created != 'null' ? params.created : '-');
        $('.fm_meta_status').hide();
        if (params.ispublic == '1') {
            $('#fm_meta_status_public-'+params.elementid).show();
            $('#fm_meta_attributes_ispublic-'+params.elementid).text('PUBLIC');
            $('#fm_meta_attributes_ispublic-'+params.elementid).attr('class', 'btn btn-xs green tooltips');
            $('#attribute_status-'+params.elementid).val(1);
        } else {
            $('#fm_meta_status_private-'+params.elementid).show();
            $('#fm_meta_attributes_ispublic-'+params.elementid).text('PRIVATE');
            $('#fm_meta_attributes_ispublic-'+params.elementid).attr('class', 'btn btn-xs yellow tooltips');
            $('#attribute_status-'+params.elementid).val(0);
        }

        $('#fm_meta_details_download-'+params.elementid).attr('href', params.urlfile);
        $('#fm_meta_details_preview-'+params.elementid).attr('onclick', `CDFshowPreviewFile({elementid:'`+params.elementid+`',urlfile:'`+params.urlfile+`',title:'`+params.title+`'})`);
        // $('#fm_meta_details_choose-'+params.elementid).attr('onclick', `CDFchooseFile({elementid:'`+params.elementid+`',_noid:`+params._noid+`,urlfile:'`+params.urlfile+`',title:'`+params.title+`'})`);

        $('#fm_meta_attributes_fileextention-'+params.elementid).html(`<i class="`+params.classicon+`"></i> `+params.type);
        $('#fm_meta_attributes_fileextention-'+params.elementid).attr('class', 'btn btn-xs tooltips '+params.classcolor);
        $('#fm_meta_attributes_size-'+params.elementid).text(params.size != 'null' ? params.size+' KB' : '-');
        $('#fm_meta_attributes_created-'+params.elementid).text(params.created != 'null' ? params.created : '-');
        $('#fm_meta_attributes_updated-'+params.elementid).text(params.modified != 'null' ? params.modified : '-');

        $('#editfile-'+params.elementid).attr('onclick', `CDFeditFile({elementid:'`+params.elementid+`',_noid:`+params._noid+`})`);
        $('#attribute_nama-'+params.elementid).val(params.nama);
        

        $('#attribute_tag-'+params.elementid).select2({
            tags: true,
            tokenSeparators: [',', ' ']
        });
            
        if (cdf_onselect2) {
            $('#attribute_tag-'+params.elementid).select2('destroy')
            $('#attribute_tag-'+params.elementid).html('')
        }
        if (params.filetag!='') {
            if (params.filetag.split(',').length > 0) {
                let tags_data = [];
                $.each(params.filetag.split(','), function(k,v) {
                    tags_data.push({id:v,text:v,selected:true});
                });

                $('#attribute_tag-'+params.elementid).select2({
                    tags: true,
                    tokenSeparators: [',', ' '],
                    data: tags_data
                });
            }
        } else {
            $('#attribute_tag-'+params.elementid).select2({
                tags: true,
                tokenSeparators: [',', ' '],
                data: []
            });
        }
        cdf_onselect2 = true;

        // $('.fm_contentitems').attr('style', 'height: 139px; overflow: hidden;  border: 1px solid grey');
        // $('#fm_contentitems-'+params.elementid+'-'+params._noid).css('box-shadow', '0px 0px 5px 2px #9a12b3');
        $('#fm_meta-'+params.elementid).show();

        // alert(cdf_checks.indexOf(params._noid))
        let idx = cdf_checks.indexOf(params._noid);
        if (idx > -1) {
            cdf_checks.splice(idx, 1)
            // cdf_selecteds.splice('cdf-'+params._noid, 1)
            delete cdf_selecteds['cdf-'+params];
            $('#fm_contentitems-'+params.elementid+'-'+params._noid).attr('style', 'height: 139px; overflow: hidden;  border: 1px solid grey')
        } else {
            cdf_checks.push(params._noid)
            cdf_selecteds['cdf-'+params._noid] = {
                noid: params._noid,
                nama: params.nama,
                cdf_nama: params.cdf_nama,
                cdf_keterangan: params.cdf_keterangan,
                cdf_ishardcopy: params.cdf_ishardcopy,
                cdf_ispublic: params.cdf_nama,
                cdf_doexpired: params.cdf_doexpired,
                cdf_doreviewlast: params.cdf_doreviewlast,
                cdf_doreviewnext: params.cdf_doreviewnext,
                urlfile: params.urlfile,
            }
            $('#fm_contentitems-'+params.elementid+'-'+params._noid).css('box-shadow', '0px 0px 5px 2px #9a12b3');
        }
    }

    function CDFshowPreviewFile(params) {
        $('#modal-preview-file-'+params.elementid+' div div div div .fm-image-content img').attr('src', params.urlfile);
        $('#modal-preview-file-'+params.elementid+' div div div .modal-title').text(params.title);
        $('#modal-preview-file-'+params.elementid).modal('show');
    }

    function CDFmouseoverGridItem(id1, id2) {
        $(id1).css('height', '95%');
        $(id2).css('left', '0%');
    }

    function CDFmouseoutGridItem(id1, id2) {
        $(id1).css('height', '23px');
        $(id2).css('left', '-200%');
    }

    function CDFuploadFile() {
        $('#cdf_upload').addClass('disabled')
        $('#cdf_clear').addClass('disabled')
        var formdata = new FormData();
        var files = $('#files')[0].files[0];
        var ispublic = $('#ispublic').prop('checked') ? 1 : 0;
        formdata.append('_token', $('meta[name="csrf-token"]').attr('content'));
        formdata.append('file', files);
        formdata.append('ispublic', ispublic);
        formdata.append('filename', $('#filename').val());

        $.ajax({
            url: "/cdf_upload"+main.urlcode,
            type: "POST",
            method: "POST",
            data: formdata,
            processData: false,  // Important!
            contentType: false,
            cache: false,
            mimeType: 'multipart/form-data',
            header:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            success: function(response){
                var response = JSON.parse(response);

                if(response['success']){ 
                    alert(response['message'])
                    $.notify({
                        message: response['message']
                    },{
                        type: 'success'
                    });
                    $('#cdf_clear').trigger('click')
                } else {
                    alert(response['message'])
                    $.notify({
                        message: response['message']
                    },{
                        type: 'danger'
                    });
                }

                $('#cdf_upload').removeClass('disabled')
                $('#cdf_clear').removeClass('disabled')
            }, 
        });
    }

    function CDFclearFile() {
        $("#content_upload_files").show();
        $("#content_upload_files2").hide();
    }

    function CDFshow(params) {
        if (params.type == 'prev') {
            // if (cdf_show < 1) {
            //     // alert('Mentok.')
            // } else {
                cdf_show = cdf_show-1;
                let _cdf_show_title = (cdf_show+1)+'. '+cdf_selecteds['cdf-'+cdf_checks[cdf_show]].nama;
                $('#cdf-show-title').text(_cdf_show_title);
                $('.cdf-show').hide();
                $('#cdf-show-'+cdf_show).show();

                (cdf_show < 1)
                    ? $('#cdf_btn_show_prev').addClass('disabled')
                    : $('#cdf_btn_show_prev').removeClass('disabled');
                $('#cdf_btn_show_next').removeClass('disabled');
            // }
        } else if (params.type == 'next') {
            // if (parseInt(cdf_show) == parseInt(cdf_checks.length)-parseInt(1)) {
            //     // alert('Mentok.')
            // } else {
                cdf_show = cdf_show+1;
                let _cdf_show_title = (cdf_show+1)+'. '+cdf_selecteds['cdf-'+cdf_checks[cdf_show]].nama;
                $('#cdf-show-title').text(_cdf_show_title);
                $('.cdf-show').hide();
                $('#cdf-show-'+cdf_show).show();

                $('#cdf_btn_show_prev').removeClass('disabled');
                (parseInt(cdf_show) == parseInt(cdf_checks.length)-parseInt(1)) 
                    ? $('#cdf_btn_show_next').addClass('disabled') 
                    : $('#cdf_btn_show_next').removeClass('disabled');
            // }
        }
        
        $('.cdf-detail').hide();
        $('#cdf-detail-'+cdf_show).show();
        // alert(cdf_show)
    }

    function CDFunset() {
        cdf_istabinfo = ''; //details/attributes
        cdf_checks = [];
        cdf_selecteds = {};
        cdf_show = 0;
    }

    function CDFsave(params) {
        let html = ``;

        if (cdf_checks.length > 0) {
            $('#cdf-show-title').text('1. '+cdf_selecteds['cdf-'+cdf_checks[0]].nama);
            $.each(cdf_checks, function(k,v) {
                let _show = k == 0 ? 'display:block;' : 'display:none;';
                
                html += `<img src="`+cdf_selecteds['cdf-'+v].urlfile+`" id="cdf-show-`+k+`" class="cdf-show" style="max-width:100%;max-height:200px;`+_show+`">`
            })

            $('#content_image-'+params.elementid+' div div').html(html)

            $('#cdf-btn-show').html(cdf_checks.length > 0 ? cdf_btn_show : '');
        } else {
            if (!statusadd) {
                html += `<img src="`+location.origin+`/filemanager/default/noimage.png" id="cdf-show-2" class="cdf-show" style="max-width:100%;max-height:200px;">`;
                $('#content_image-'+params.elementid+' div div').html(html)
                $('#cdf-show-title').text('File Manager');
                $('#cdf-btn-show').html('');
            }
        }
        
        if (cdf_checks.length == 1) {
            $('#cdf_btn_show_prev').addClass('disabled');
            $('#cdf_btn_show_next').addClass('disabled');
        } else if (cdf_checks.length > 1) {
            $('#cdf_btn_show_prev').addClass('disabled');
            $('#cdf_btn_show_next').removeClass('disabled');
        }

        
        let _html_detail = ``;
        if (cdf_checks.length > 0) {
            $.each(cdf_checks, function(k,v) {
                let ishardcopy = {'true': '', 'false': 'checked'};
                let ispublic = {'true': '', 'false': 'checked'};
                let doexpired = `${new Date().getDate()}-${new Date().toLocaleString('default', { month: 'short' })}-${new Date().getFullYear()+1}`;
                let doreviewlast = `${new Date().getDate()}-${new Date().toLocaleString('default', { month: 'short' })}-${new Date().getFullYear()}`;
                let doreviewnext = `${new Date().getDate()}-${new Date().toLocaleString('default', { month: 'short' })}-${new Date().getFullYear()+1}`;

                if (cdf_selecteds['cdf-'+v].cdf_ishardcopy != undefined && cdf_selecteds['cdf-'+v].cdf_ishardcopy == 1) {
                    ishardcopy.true = 'checked';
                    ishardcopy.false = '';
                }

                if (cdf_selecteds['cdf-'+v].cdf_ispublic != undefined && cdf_selecteds['cdf-'+v].cdf_ispublic == 1) {
                    ispublic.true = 'checked';
                    ispublic.false = '';
                }

                _html_detail += `<div id="cdf-detail-`+k+`" class="cdf-detail" style="display: `+(k==0?'block':'none')+`">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Nama</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="cdf_detail_`+k+`_nama" value="`+(cdf_selecteds['cdf-'+v].cdf_nama?cdf_selecteds['cdf-'+v].cdf_nama:cdf_selecteds['cdf-'+v].nama)+`" class="form-control" placeholder="Nama">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Keterangan</label>
                                                <div class="col-md-9">
                                                    <textarea name="cdf_detail_`+k+`_keterangan" cols="3" rows="5" class="form-control" placeholder="Keterangan">`+(cdf_selecteds['cdf-'+v].cdf_keterangan?cdf_selecteds['cdf-'+v].cdf_keterangan:'')+`</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Is Hard Copy</label>
                                                <div class="col-md-9" style="padding-top: 8px">
                                                    <input type="radio" name="cdf_detail_`+k+`_ishardcopy" value="1" `+(ishardcopy.true)+`> True
                                                    <input type="radio" name="cdf_detail_`+k+`_ishardcopy" value="0" `+(ishardcopy.false)+`> False
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Is Public</label>
                                                <div class="col-md-9" style="padding-top: 8px">
                                                    <input type="radio" name="cdf_detail_`+k+`_ispublic" value="1" `+(ispublic.true)+`> True
                                                    <input type="radio" name="cdf_detail_`+k+`_ispublic" value="0" `+(ispublic.false)+`> False
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Do Expired</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="cdf_detail_`+k+`_doexpired" value="`+(doexpired)+`" class="form-control datepicker" placeholder="Do Expired" autocomplete="off">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Do Review Last</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="cdf_detail_`+k+`_doreviewlast" value="`+(doreviewlast)+`" class="form-control datepicker" placeholder="Do Review Last" autocomplete="off">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Do Review Next</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="cdf_detail_`+k+`_doreviewnext" value="`+(doreviewnext)+`" class="form-control datepicker" placeholder="Do Review Next" autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>`;
            })
        }

        console.log('_html_detail')
        console.log(_html_detail)
        $('#cdf-detail').html(_html_detail);
        $('.datepicker').datepicker({
            format: 'dd-M-yyyy',
            autoclose: true,
            // startDate: '-3d'
        });

        $('#qty-attachment').text(cdf_checks.length)
        CDFhideModalDocumentViewer({elementid:params.elementid});
    }

    function customDocumentForm() {
        CDFunset();
        let params = new Array();
        params.push({
            elementid: 'flowchart',
            caption: 'Document',
        });

        $.each(params, function(k, v) {
            $('#'+v.elementid).html(`<div style="display:block;" class="col-md-12 pull-left">
                <div class="form-group">
                    <label class="control-label ">`+v.caption+`</label>
                    <input type="hidden" id="'.$v->fieldsource.'-`+v.elementid+`"/>
                    <div class="">
                        <div class="popup-image_preview" style="width: 100%; height: 100%; text-align: -webkit-center; border: 1px solid #f0f0f0; padding: 8px; cursor: pointer; position: relative; min-height: 40px; overflow: hidden">
                            <div id="fm-btn-action-`+v.elementid+`" class="fm-btn-action" style="overflow: hidden; display: block; position: absolute; top: 0px; left: 0px; right: 0px; padding: 6px 0px; background: rgba(154, 18, 179, .7); height: 38px">
                                <div class="btn btn-sm green btn_hide" id="iddmslogo" onclick="CDFshowModalDocumentViewer({elementid:'`+v.elementid+`',refresh:true})">
                                    <span id="icon">
                                        <i class="fa fa-plus text-light"></i>
                                    </span> 
                                    <span id="caption">File Manager</span>
                                </div>
                            </div>
                            <div id="content_image-`+v.elementid+`" class="fm-btn-content" style="height: 200px;">
                                <div class="modal-image-content" style="width: 100%; height: inherit; text-align: -webkit-center">
                                    <div class="fm-image-content" style="vertical-align: middle; display: table-cell; width: 100%; height: inherit; text-align: -webkit-center">
                                        <img src="`+location.origin+`/filemanager/default/noimage.png" id="cdf-show-2" class="cdf-show" style="max-width:100%;max-height:200px;">
                                    </div>
                                </div>
                            </div>
                            <div class="fm-btn-title" id="title_img-`+v.elementid+`" style="position: absolute; left: 0; right: 0; bottom: 0; color: #fff; padding: 10px; background: rgba(154, 18, 179, .7); overflow: hidden; white-space: nowrap; text-overflow: ellipsis; font-weight: bold; text-align: center">
                                <div class="row">
                                    <div class="col-md-9 text-left">
                                        <span id="cdf-show-title">File Manager</span>
                                    </div>
                                    <div class="col-md-3 text-right" id="cdf-btn-show">
                                        `+(cdf_checks.length > 0 ? cdf_btn_show : '')+`
                                    </div>
                                </div>
                            </div>
                        </div>
                        <span class="help-block"></span>
                    </div>
                </div>
                <div id="modal-document-viewer-`+v.elementid+`" class="modal fade" aria-hidden="true" style="display: none;">
                    <div class="modal-lg_ modal-dialog" style="max-width: 95%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <div class="modal-title">Document Viewer</div>
                            </div>
                            <div class="modal-body_" style="padding: 0 5px; margin-top: 5px !important">
                                <div class="col-md-12 fm_panel">
                                    <div class="fm_panel_body" style="overflow: hidden; border: 4px solid #03A9F4; margin: 0 -5px 0 -4px">
                                        <div class="col-md-12 fm_panel_toolbar" style="background: #fff; padding: 4px 0 0 0; display: inline-block; border-bottom: 4px solid #03A9F4">
                                            <div class="row">
                                                <div class="col-md-6 col-xs-12 fm_btnfilter" id="filter">
                                                    <a type="button" class="btn btn-sm default" onclick="CDFtoggleFmBtn({elementid:'`+v.elementid+`',type:'myfiles'})"><i class="fa fa-copy text-dark"></i> ALL</a>
                                                    <a type="button" class="btn btn-sm blue" onclick="CDFtoggleFmBtn({elementid:'`+v.elementid+`',type:'myfiles',where:'doc'})"><i class="fa fa-file text-light"></i> DOC</a>
                                                    <a type="button" class="btn btn-sm green" onclick="CDFtoggleFmBtn({elementid:'`+v.elementid+`',type:'myfiles',where:'img'})"><i class="fa fa-file-image text-light"></i> IMG</a>
                                                    <a type="button" class="btn btn-sm blue-hoki" onclick="CDFtoggleFmBtn({elementid:'`+v.elementid+`',type:'myfiles',where:'av'})"><i class="fa fa-file-audio text-light"></i> AV</a>
                                                    <a type="button" class="btn btn-sm yellow" onclick="CDFtoggleFmBtn({elementid:'`+v.elementid+`',type:'myfiles',where:'zip'})"><i class="fa fa-file-archive text-light"></i> ZIP</a>
                                                    <a type="button" class="btn btn-sm red" onclick="CDFtoggleFmBtn({elementid:'`+v.elementid+`',type:'myfiles',where:'bin'})"><i class="fa fa-file-archive text-light"></i> BIN</a>
                                                    <a type="button" class="btn btn-sm purple" onclick="CDFtoggleFmBtn({elementid:'`+v.elementid+`',type:'myfiles',where:'other'})"><i class="fa fa-file text-light"></i> OTHER</a>
                                                </div>
                                                <div class="col-md-3 col-xs-6 fm_btnlibrary" style="text-align: right;">
                                                    <a type="button" class="btn btn-sm purple tooltips" data-container="body" data-placement="bottom" data-original-title="Upload" id="cdf-upload-`+v.elementid+`" onclick="CDFtoggleFmBtn({elementid:'`+v.elementid+`',type:'upload'})">
                                                        <i class="fa fa-cloud text-light"></i> <span style="display: inline-block">Upload</span>
                                                    </a type="button">
                                                    <a type="button" class="btn btn-sm green tooltips" data-container="body" data-placement="bottom" data-original-title="My Files" id="cdf-myfiles-`+v.elementid+`" onclick="CDFtoggleFmBtn({elementid:'`+v.elementid+`',type:'myfiles'})">
                                                        <i class="fa fa-user text-light"></i> <span style="display: none">My Files</span>
                                                    </a type="button">
                                                    <a type="button" class="btn btn-sm blue tooltips" data-container="body" data-placement="bottom" data-original-title="Save" id="cdf-save-`+v.elementid+`" onclick="CDFsave({elementid:'`+v.elementid+`'})">
                                                        <i class="fa fa-save text-light"></i> <span style="display: none">Save</span>
                                                    </a type="button">
                                                    <a type="button" class="btn btn-sm blue tooltips" data-container="body" data-placement="bottom" data-original-title="Tile View" style="display: none;">
                                                        <i class="fa fa-th text-light"></i>
                                                    </a type="button">
                                                    <a type="button" class="btn btn-sm grey tooltips" data-container="body" data-placement="bottom" data-original-title="List View" style="display: none;">
                                                        <i class="fa fa-th-list text-dark"></i>
                                                    </a type="button">
                                                </div>
                                                <div class="col-md-3 col-xs-6 fm_search">
                                                    <div class="input-icon input-icon-sm right">
                                                        <i class="fa fa-search"></i>
                                                        <input type="text" id="cdf-search" class="form-control input-sm" placeholder="Search..." style="height: 27px; margin-bottom: 4px">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 fm_panel_container" id="container" style="padding: 0px">
                                            <div class="fm_panelcontent" id="content_upload-`+v.elementid+`" style="display: block;">
                                                <div class="col-md-12">
                                                    <div id="content_upload_files" style="height: 450px;">
                                                        <br><br>
                                                        <center style="padding:0px;">
                                                            <div>
                                                                <div class="btn btn-sm green fileinput-button" style="position: relative; overflow: hidden">
                                                                    <i class="fa fa-plus text-light"></i>
                                                                    <span> Upload</span>
                                                                    <form method="post" enctype="multipart/form-data">
                                                                        <input id="files" type="file" name="files" multiple="" style="position: absolute; top: 0; right: 0; margin: 0; opacity: 0; font-size: 200px; direction: ltr; cursor: pointer" onchange="CDFafterPickFiles({id:'files'})">
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </center>
                                                    </div>
                                                    <div id="content_upload_files2" style="height: 450px; display: none">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="fm_panelcontent" id="content_myfile-`+v.elementid+`" style="display: none;">
                                                <div class="row">
                                                    <div class="col-md-9 col-xs-7 fm_content" style="padding: 4px">
                                                        <div class="row">
                                                            <div class="col-md-12 fm_content_list modal-body" id="panelcontent" style="padding: 0px; margin: 0px !important; height: 480px">
                                                                <div class="row">
                                                                    <div class="col-md-2 col-xs-4 fm_content_grid_items" id="fm_content_grid_item" style="margin-bottom: 4px; padding: 4px; text-align: -webkit-center; height: 100%" onmouseover="CDFmouseoverGridItem(\'fm_contenttitle\', \'fm_contenttitle_action\')" onmouseout="CDFmouseoutGridItem(\'fm_contenttitle\', \'fm_contenttitle_action\')">
                                                                        <div class="fm_contentitems" id="fm_contentitems-`+v.elementid+`" style="height: 139px; overflow: hidden;  border: 1px solid grey" onclick="CDFselectContentItem({elementid:'`+v.elementid+`'})">
                                                                            <span class="badge badge-null badge-roundless" style="display: none;">UNSPECIFIED</span>
                                                                            <img src="http://dashboard.dev.lambada.id/data_lycon/filemanager/thumb/cmspage-pagecaption.jpg" class="image_ori">
                                                                        </div>
                                                                        <div class="fm_contenttitle" id="fm_contenttitle" onclick="CDFselectContentItem({elementid:'`+v.elementid+`'})" style="text-align: center; height: 23px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis; color: #fff; bottom: 4px; position: absolute; width: calc(100% - 8px); padding: 4px; background: rgba(0, 0, 0, .7); display: block; font-size: 80%; -webkit-transition: all 0.5s ease;">
                                                                            cmspage-pagecaption
                                                                            <div class="fm_contenttitle_action" id="fm_contenttitle_action" style="position: absolute; bottom: 4px; left: -200%; right: 0; transition: all 0.5s ease 0s;">
                                                                                <a type="button" class="btn btn-sm purple tooltips" data-container="body" data-placement="top" data-original-title="Preview" id="preview" onclick="CDFshowPreviewFile({elementid:'`+v.elementid+`',urlfile:'http://dashboard.dev.lambada.id/data_lycon/filemanager/thumb/cmspage-pagecaption.jpg',title:'cmspage-pagecaption.jpg'})">
                                                                                    <i class="fa fa-desktop text-light"></i>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-xs-5 fm_meta" id="fm_meta-`+v.elementid+`" style="display: none">
                                                        <ul class="nav">
                                                            <li class="nav-item active">
                                                                <a class="nav-link" href="#tab-details" id="cdf-btntabdetails" data-toggle="tab" aria-expanded="true">Details</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" href="#tab-attributes" id="cdf-btntabattributes" data-toggle="tab" aria-expanded="false">Attributes</a>
                                                            </li>
                                                            <li class="nav-item" style="display:none;">
                                                                <a class="nav-link" href="#tab-comments" id="cdf-btntabcomments" data-toggle="tab" aria-expanded="false">Comments</a>
                                                            </li>
                                                        </ul>
                                                        <div class="tabbable-custom tabs-below nav-justified modal-body" style="margin: 0px !important; height: 450px">
                                                            <div class="tab-content">
                                                                <div class="tab-pane" id="tab-details">
                                                                    <div>
                                                                        <div class="col-md-12 fm_meta_tag_function" style="text-align: center; padding: 4px; background: #000; width: 100%">
                                                                            <a href="http://dashboard.dev.lambada.id/data_lycon/filemanager/original/Screen Shot 2021-02-24 at 09.53.03.png" id="fm_meta_details_download-`+v.elementid+`" download target="_blank" class="btn btn-sm blue tooltips">
                                                                                <i class="fa fa-download text-light"></i>
                                                                            </a>
                                                                            <span class="btn btn-sm purple tooltips" data-container="body" data-placement="top" data-original-title="Preview" id="fm_meta_details_preview-`+v.elementid+`" onclick="CDFshowPreviewFile({elementid:'`+v.elementid+`',urlfile:'http://dashboard.dev.lambada.id/data_lycon/filemanager/thumb/cmspage-pagecaption.jpg',title:'cmspage-pagecaption.jpg'})">
                                                                                <i class="fa fa-desktop text-light"></i>
                                                                            </span>
                                                                        </div>
                                                                        <div class="col-md-12 fm_meta_filename" id="fm_meta_filename-`+v.elementid+`" style="padding: 8px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis; background: rgba(0, 0, 0, .7); width: 100%; color: #fff">
                                                                            <span class="btn btn-xs green">
                                                                                <i class="fa fa-file-image text-light"></i>
                                                                            </span>
                                                                            <strong>
                                                                                Screen Shot 2021-02-24 at 09.53.03.png
                                                                            </strong>
                                                                        </div>
                                                                        <div class="col-md-12" style="text-align: -webkit-center;padding: 8px;">
                                                                            <div class="fm_meta_image" id="fm_meta_image-`+v.elementid+`">
                                                                                <img src="http://dashboard.dev.lambada.id/data_lycon/filemanager/thumb/cmspage-pagecaption.jpg" style="height: 200px; width: 100%">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                            <div class="row">
                                                                                <div class="col-md-5 col-xs-5">Size:</div>
                                                                                <div class="col-md-7 col-xs-7" id="fm_meta_size-`+v.elementid+`">297 KB</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                            <div class="row">
                                                                                <div class="col-md-5 col-xs-5">Type:</div>
                                                                                <div class="col-md-7 col-xs-7" id="fm_meta_type-`+v.elementid+`">IMG Document</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                            <div class="row">
                                                                                <div class="col-md-5 col-xs-5">Date Modified:</div>
                                                                                <div class="col-md-7 col-xs-7" id="fm_meta_modified-`+v.elementid+`">-</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                            <div class="row">
                                                                                <div class="col-md-5 col-xs-5">Date Created:</div>
                                                                                <div class="col-md-7 col-xs-7" id="fm_meta_created-`+v.elementid+`">2021-03-02 01:42:34</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                            <div class="row">
                                                                                <div class="col-md-5 col-xs-5">Status:</div>
                                                                                <div class="col-md-7 col-xs-7">
                                                                                    <span class="badge badge-null badge-roundless fm_meta_status" id="fm_meta_status_unspecified-`+v.elementid+`" style="display: none;">UNSPECIFIED</span> 
                                                                                    <span class="badge badge-success badge-roundless fm_meta_status" id="fm_meta_status_public-`+v.elementid+`">PUBLIC</span>
                                                                                    <span class="badge badge-warning badge-roundless fm_meta_status text-light" id="fm_meta_status_private-`+v.elementid+`" style="display: none;">PRIVATE</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 fm_meta_tag_tags_list">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="tab-pane" id="tab-attributes" style="position: relative; zoom: 1;">
                                                                    <div style="display: inline-block_;">
                                                                        <div class="col-md-12 col-xs-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                            <div class="row">
                                                                                <div class="col-md-12 col-xs-12">
                                                                                    <span class="btn btn-xs green tooltips" id="fm_meta_attributes_ispublic-`+v.elementid+`" data-container="body" data-placement="top" data-original-title="Status" style="font-size: 10px; font-weight: bold; padding: 3px">
                                                                                        PUBLIC
                                                                                    </span> 
                                                                                    <span class="btn btn-xs green tooltips" id="fm_meta_attributes_fileextention-`+v.elementid+`" data-container="body" data-placement="top" data-original-title="File Type" style="font-size: 10px; font-weight: bold; padding: 3px">
                                                                                        <i class="fa fa-file-image text-light"></i> IMG
                                                                                    </span> 
                                                                                    <span class="btn btn-xs grey tooltips" id="fm_meta_attributes_size-`+v.elementid+`" data-container="body" data-placement="top" data-original-title="File Size" style="font-size: 10px; font-weight: bold; padding: 3px">
                                                                                        297 KB
                                                                                    </span> 
                                                                                    <span class="btn btn-xs blue pull-right tooltips" id="editfile-`+v.elementid+`" data-container="body" data-placement="top" data-original-title="Save Document" style="font-size: 10px; font-weight: bold; padding: 3px">
                                                                                        <i class="fa fa-save"></i> Save
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 col-xs-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                            <div class="row">
                                                                                <div class="col-md-12 col-xs-12">
                                                                                    <span class="btn btn-xs green tooltips" id="fm_meta_attributes_created-`+v.elementid+`" data-container="body" data-placement="top" data-original-title="Created By" style="width:100%;text-align:left; font-size: 10px; font-weight: bold; padding: 3px">
                                                                                        2021-03-02 01:42:34 (GUEST)
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 col-xs-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                            <div class="row">
                                                                                <div class="col-md-12 col-xs-12">
                                                                                    <span class="btn btn-xs yellow tooltips" id="fm_meta_attributes_updated-`+v.elementid+`" data-container="body" data-placement="top" data-original-title="Updated by" style="width:100%;text-align:left; font-size: 10px; font-weight: bold; padding: 3px">- (GUEST)</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 col-xs-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                            <div class="row">
                                                                                <div class="col-md-12 col-xs-12">Name:</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 col-xs-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                            <div class="row">
                                                                                <div class="col-md-12 col-xs-12">
                                                                                    <input id="attribute_nama-`+v.elementid+`" type="text" style="width:100%;">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 col-xs-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                            <div class="row">
                                                                                <div class="col-md-12 col-xs-12">Status:</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 col-xs-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                            <div class="row">
                                                                                <div class="col-md-12 col-xs-12">
                                                                                    <select id="attribute_status-`+v.elementid+`">
                                                                                        <option value="0">Private</option>
                                                                                        <option value="1">Public</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 col-xs-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                            <div class="row">
                                                                                <div class="col-md-12 col-xs-12">Tags:</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 col-xs-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                            <div class="row">
                                                                                <div class="col-md-12 col-xs-12" style="display:table;">
                                                                                    <select multiple class="form-control form-control-solid select2tag" id="attribute_tag-`+v.elementid+`" style="width: 100%">
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="tab-pane" id="tab-comments">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer" style="padding: 5px 0 0 0 !important">
                                <a href="javascript:;" title="Close" class="fancybox-item fancybox-close" data-dismiss="modal" aria-hidden="true"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal" id="modal-preview-file-`+v.elementid+`" data-backdrop="static" aria-hidden="false" style="display: none; padding-left: 0px;">
                    <div class="modal-lg modal-dialog" style="width:90%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <div class="modal-title">
                                    Anjas_CV.jpg
                                </div>
                            </div>
                            <div class="modal-body" style="margin-top: 5px !important">
                                <div class="modal-image-content" style="width: 100%; height: inherit; text-align: -webkit-center">
                                    <div class="fm-image-content" style="vertical-align: middle; display: table-cell; width: 100%; height: inherit;">
                                        <img src="http://dashboard.dev.lambada.id/data_lycon/filemanager/preview/cmspage-pagecaption.jpg" data-testing="testing" style="max-width:100%;max-height:100%;">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                            </div>
                            <a href="javascript:;" title="Close" class="fancybox-item fancybox-close" data-dismiss="modal" aria-hidden="true"></a>
                        </div>
                    </div>
                </div>
            </div>`);

            $('#cdf-search').on('keydown', function(e) {
                if (e.keyCode == 13) {
                    CDFgetFile({elementid:v.elementid,whereformat:0,wheresearch:$('#cdf-search').val()});
                }
            })

            $("#modal-document-viewer-"+v.elementid).on('hidden.bs.modal', function () {
                CDFsave({elementid:v.elementid})
            });
        });

        $('#qty-attachment').text(cdf_checks.length)
    }

    function getCmsFilterButton(noid) {
        var buttonfilter = new Array();

        if (noid) {
            var each = JSON.parse(maincms_widget[noid]['configwidget'])['WidgetFilter'];
            var configwidget = JSON.parse(maincms_widget[noid]['configwidget']);
            var vnoid = noid;
        } else {
            var each = JSON.parse(maincms['widget'][0]['configwidget'])['WidgetFilter'];
            var configwidget = JSON.parse(maincms['widget'][0]['configwidget']);
            var vnoid = maincms['widget'][0]['noid'];
        }
        
        $.each(each, function(k, v) {
            // buttonfilter[k] = new Array();
            buttonfilter[k] = {
                'groupcaption' : v.groupcaption,
                'groupcaptionshow' : v.groupcaptionshow,
                'groupdropdown' : v.groupdropdown,
                'data' : new Array()
            }
            
            $.each(v.button, function(k2, v2) {
                var icon = '';
                if (v2.classicon != '') {
                    icon = '<i class="fa '+v2.classicon+'"></i> ';
                }
                var pecah = v2.condition[0].querywhere[0].fieldname.split('.');
                var buttoncaption = v2.buttoncaption.split(' ');
                var uniqeidbutton = '';
                if (buttoncaption.length > 1) {
                    $.each(buttoncaption, function(k3, v3) {
                        uniqeidbutton += v3
                    })
                } else {
                    uniqeidbutton = v2.buttoncaption
                }
                var oncl = 'onclick="filterData(\'btn-filter-'+uniqeidbutton+'-'+vnoid+'\', \'btn-filter-'+k+'-'+vnoid+'\', '+k+', \''+v2.buttoncaption+'\', \''+v2.classcolor+'\', '+v.groupdropdown+', false, \''+maincms['widget'][0]['maintable']+'\', \''+v2.condition[0].querywhere[0].operand+'\', \''+pecah[1]+'\', \''+v2.condition[0].querywhere[0].operator+'\', \''+v2.condition[0].querywhere[0].value+'\')" ';
                var button = '<button class="btn btn-outline-secondary btn-sm flat btn-filter-'+k+'-'+vnoid+'" id="btn-filter-'+uniqeidbutton+'-'+vnoid+'" style="background-color:#E3F2FD; color: '+v2.classcolor+'"'+oncl+'>'+icon+v2.buttoncaption+' </button>';
                if (v.groupdropdown == 1) {
                    button = '<button class="flat dropdown-item" id="btn-filter-'+uniqeidbutton+'-'+vnoid+'" '+oncl+'>'+icon+v2.buttoncaption+' </button>';
                }
                buttonfilter[k].data[k2] = {
                    'nama' : v2.buttoncaption,
                    'buttonactive' : v2.buttonactive,
                    'taga' : button,
                    'operand' : v2.condition[0].querywhere[0].operand,
                    'fieldname' : pecah[1],
                    'operator' : v2.condition[0].querywhere[0].operator,
                    'value' : v2.condition[0].querywhere[0].value,
                };
            })
        })

        
        var json_data = {
            "filterButton" : buttonfilter,
            "configWidget" : configwidget,
        };
        
        // console.log('test')
        $.each(json_data.filterButton, function(k, v) {
            $.each(v.data, function(k2, v2) {
                if (v2 != undefined) {
                    // console.log(v2.buttonactive)  
                }
            })
        })

        cmsfilterbutton = json_data;
        // return json_data;
    }

    function getFilterButton(values3, noid) {
        // $.ajax({
        //     type: "POST",
        //     header:{
        //         'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
        //     },
        //     url: "/get_filter_button_tmd",
        //     dataType: "json",
        //     data:{
        //         "_token":$('meta[name="csrf-token"]').attr('content'),
        //         table: maincms['menu']['noid'],
        //         maincms: JSON.stringify(maincms)
        //     },
        //     success: function(response) {
                getCmsFilterButton(noid)
                console.log('cmsfilterbutton')
                console.log(cmsfilterbutton)
                var response = cmsfilterbutton;

                if (noid) {
                    var vnoid = noid;
                } else {
                    var vnoid = maincms['widget'][0]['noid'];
                }
                // if (response == response2) {
                //     console.log('COCOKKKKK')
                // }
                // console.log(response)
                // console.log(response2)
                // return false

                var buttonactive = new Array();
                var filter_button = '';
                var groupdropdown = new Array();
                $.each(response.filterButton, function(k, v) {
                    // console.log(v.groupdropdown)
                    buttonactive.push('all-'+k+'-'+vnoid);
                    if (v.groupdropdown == 1) {
                        // alert(1)
                        filter_button += `<button class="btn btn-outline-secondary btn-sm flat btn-filter-all btn-filter-`+k+`-`+vnoid+`" id="btn-filter-all-`+k+`-`+vnoid+`" style="background-color:#90CAF9; font-weight: bold; color: black;">`+v.groupcaption+`</button>`;
                        filter_button += `<div class="dropdown btn-group" style="padding-right: 5px">`
                    } else {
                        // alert(0)
                        filter_button += `<button class="btn btn-outline-secondary btn-sm flat btn-filter-all btn-filter-`+k+`-`+vnoid+`" id="btn-filter-all-`+k+`-`+vnoid+`" style="background-color:#E3F2FD;" onclick="filterData('btn-filter-all-`+k+`-`+vnoid+`', 'btn-filter-`+k+`-`+vnoid+`', `+k+`, 'ALL', 'grey', 0, true)">ALL</button>`;
                    }
                    selected_filter.operand.push('')
                    selected_filter.fieldname.push('')
                    selected_filter.operator.push('')
                    selected_filter.value.push('')

                    $.each(v.data, function(k2, v2) {
                        if (v2 != undefined) {
                            if (v.groupdropdown == 1) {
                                if (k2 == 0) {
                                    if (v2.buttonactive) {
                                        buttonactive[k] = v2.nama;
                                        selected_filter.operand[k] = v2.operand;
                                        selected_filter.fieldname[k] = v2.fieldname;
                                        selected_filter.operator[k] = v2.operator;
                                        selected_filter.value[k] = v2.value;
                                        filter_button += `<button class="btn btn-secondary btn-sm flat dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <span id="filter-dropdown-active-`+k+`-`+vnoid+`">`+v2.nama+`</span>
                                                        </button>`;
                                    } else {
                                        filter_button += `<button class="btn btn-secondary btn-sm flat dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <span id="filter-dropdown-active-`+k+`-`+vnoid+`">ALL</span>
                                                        </button>`;
                                    }
                                    filter_button += `<div class="dropdown-menu flat" aria-labelledby="dropdownMenuButton">
                                                        <button class="dropdown-item btn-filter-all btn-filter-`+k+`-`+vnoid+`" id="btn-filter-all-`+k+`-`+vnoid+`"  onclick="filterData('btn-filter-all-`+k+`-`+vnoid+`', 'btn-filter-`+k+`-`+vnoid+`', `+k+`, 'ALL', 'grey', 1, true)">ALL</button>`;
                                }
                                groupdropdown.push(true);
                            } else {
                                if (v2.buttonactive) {
                                    buttonactive[k] = v2.nama;
                                    selected_filter.operand[k] = v2.operand;
                                    selected_filter.fieldname[k] = v2.fieldname;
                                    selected_filter.operator[k] = v2.operator;
                                    selected_filter.value[k] = v2.value;
                                }
                                groupdropdown.push(false);
                            }
                            filter_button += v2.taga;
                        }
                    })
                    
                    if (v.groupdropdown == 1) {
                        filter_button += `</div>
                                        </div>`;
                    }
                    filter_button += `<div class="btn" style="padding: 0px; width: 5px"></div>`
                })
                
                if (filter_button.length > 0) {
                    if (noid) {
                        $('#space_filter_button-'+noid).html(filter_button)
                    } else {
                        $('#space_filter_button').html(filter_button)
                    }
                    // $('#space_filter_button').css('margin-bottom', '20px')
                }
                
                console.log('buttonactive')
                console.log(buttonactive)
                console.log(groupdropdown)
                $.each(buttonactive, function(k, v) {
                    if (groupdropdown[k] == false) {
                        // alert('#btn-filter-'+buttonactive[k]+'-'+vnoid)
                        $('#btn-filter-'+buttonactive[k]).css('background', '#90CAF9');
                        $('#btn-filter-'+buttonactive[k]).css('color', 'black');
                        $('#btn-filter-'+buttonactive[k]).css('font-weight', 'bold');
                    }
                    // console.log(groupdropdown[k])
                    // console.log(buttonactive[k])
                })

                if (buttonactive.length > 0) {
                    destroyDatatable()
                    getDatatable()
                }
                
                
                //Datatable filter
                $.each(response.configWidget.WidgetFilter, function(k, v) {
                    var filtercontentgroup = '';
                    var filtercontentbody = '';

                    var classcolor = 'default';
                    var textcolor = 'text-dark';
                    var display = 'none';
                    if (k == 0) {
                        classcolor = 'purple';
                        textcolor = 'text-white'
                        display = 'block';
                        filter_tabs.push(true)
                    } else {
                        filter_tabs.push(false)
                    }
                    filtercontentgroup += `<input type="hidden" name="tabs[]" value="`+k+`">
                                            <div style="margin:5px 5px 0 0;" id="filter-content-group-`+k+`" onclick="changeTabFilter(`+k+`)" class="filter-content-group formbtn btn default `+classcolor+`">
                                                <span class="`+textcolor+`">
                                                    <i class="fa fa-indent `+textcolor+`"></i> `+k+`
                                                </span>
                                            </div>`;
                    filtercontenttabs.filtercontentgroup.push(filtercontentgroup)

                    var groupcaptionshow = `<option value="1">True</option>
                                            <option value="0" selected>False</option>`;
                    if (v.groupcaptionshow) {
                        groupcaptionshow = `<option value="1" selected>True</option>
                                            <option value="0">False</option>`
                    }

                    var groupdropdown = '';
                    if (v.groupdropdown == -1) {
                        groupdropdown = `<option value="-1" selected>Auto</option>
                                            <option value="0">False</option>
                                            <option value="1">True</option>`
                    } else if (v.groupdropdown == 0) {
                        groupdropdown = `<option value="-1">Auto</option>
                                            <option value="0" selected>False</option>
                                            <option value="1">True</option>`
                    } else if (v.groupdropdown == 1) {
                        groupdropdown = `<option value="-1">Auto</option>
                                            <option value="0">False</option>
                                            <option value="1" selected>True</option>`
                    }
                    filtercontentbody += `<div style="display: `+display+`" id="filter-content-body-`+k+`" class="filter-content-body formgroup col-md-12">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Group Caption</label>
                                                        <input name="groupcaption[]" id="groupcaption-`+k+`" class="form-control input-sm" value="`+v.groupcaption+`" placeholder="Group Caption">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Group Caption Showed</label>
                                                        <select name="groupcaptionshow[]" id="groupcaptionshow-`+k+`" class="form-control input-sm">
                                                            `+groupcaptionshow+`
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Group Dropdown Showed</label>
                                                        <select name="groupdropdown[]" id="groupdropdown-`+k+`" class="form-control input-sm">
                                                            `+groupdropdown+`
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="filter-content-row-`+k+`">
                                            </div>
                                            </div>`
                    filtercontenttabs.filtercontentbody.push(filtercontentbody)

                    filtercontenttabs.filtercontentrow[k] = new Array()
                    var _k2 = 0;
                    $.each(v.button, function(k2, v2) {
                        var filtercontentrow = '';

                        var condition = '';
                        $.each(v2.condition, function(k3, v3) {
                            condition += v3.groupoperand+'#'
                            $.each(v3.querywhere, function(k4, v4) {
                                condition += v4.fieldname+';'
                                condition += v4.operator+';'
                                condition += v4.value+';'
                                condition += v4.operand
                            })
                        })

                        var buttonactive = `<option value="1">True</option>
                                            <option value="0" selected>False</option>`
                        if (v2.buttonactive) {
                            buttonactive += `<option value="1" selected>True</option>
                                                <option value="0">False</option>`
                        }

                        filtercontentrow += `<input type="hidden" name="tabsrow[]" value="`+k+`">
                                            <div class="row filter-content-row-`+k+`">
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Caption</label>
                                                        <input class="form-control input-sm" name="buttoncaption[]" id="buttoncaption-`+k+`-`+_k2+`" value="`+v2.buttoncaption+`" placeholder="Caption">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Condition</label>
                                                        <input class="form-control input-sm" name="condition[]" id="condition-`+k+`-`+_k2+`" value="`+condition+`" placeholder="groupoperand#fieldname;operator;value;operand">
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Active</label>
                                                        <select class="form-control input-sm" name="buttonactive[]" id="buttonactive-`+k+`-`+_k2+`" value="1">
                                                            `+buttonactive+`
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Icon</label>
                                                        <input class="form-control input-sm" name="classicon[]" id="classicon-`+k+`-`+_k2+`" value="`+v2.classicon+`" placeholder="Icon">
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Color</label>
                                                        <input class="form-control input-sm" name="classcolor[]" id="classcolor-`+k+`-`+_k2+`" value="`+v2.classcolor+`" placeholder="Color">
                                                    </div>
                                                </div>
                                                <div class="col-md-1" style="text-align:center;padding:21px 0 0 0">
                                                    <div class="form-group">
                                                        <button id="del" onclick="delRow(`+_k2+`)" type="button" class="btn btn-sm red" style="height: 27px; width: 38px">
                                                            <i class="fa fa-minus-circle text-white"></i>
                                                        </button>
                                                        <button id="add" onclick="addRow()" type="button" class="btn btn-sm green" style="height: 27px; width: 38px">
                                                            <i class="fa fa-plus text-white"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>`
                        filtercontenttabs.filtercontentrow[k].push(filtercontentrow)
                        _k2++;
                    })
                    // filtercontentbody += `</div>
                    //                     </div>`
                })

                //Set to HTML
                var html_filtercontentgroup = '';
                var html_filtercontentbody = '';
                $.each(filtercontenttabs.filtercontentgroup, function(k, v) {
                    html_filtercontentgroup += v
                    html_filtercontentbody += filtercontenttabs.filtercontentbody[k]
                })
                $('#filtercontentgroup').html(html_filtercontentgroup);
                $('#filtercontentbody').html(html_filtercontentbody);

                $.each(filtercontenttabs.filtercontentrow, function(k, v) {
                    $('#filter-content-row-'+k).html(v);
                })
                //End Set to HTML

                // console.log(html_filtercontentrow)
                // console.log(filtercontenttabs)
            // },
        // });
    }

    // function addCommas(nStr) {
    //     nStr += '';
    //     var x = nStr.split('.');
    //     var x1 = x[0];
    //     var x2 = x.length > 1 ? '.' + x[1] : '';
    //     var rgx = /(\d+)(\d{3})/;
    //     while (rgx.test(x1)) {
    //         x1 = x1.replace(rgx, '$1' + ',' + '$2');
    //     }
    //     return x1 + x2;
    // }

    function getCmsTabColumns() {
        $.each(maincms['widget'], function(k, v) {
            if (k > 0) {
                var theadcolumns = `<th><input type="checkbox" id="select_all" onchange="selectAllCheckboxTab()"></th>
                                <th>No</th>`;
                var tfootcolumns = `<th></th>
                                    <th></th>`
                // console.log(maincms['widgetgridfield'])
                $.each(maincms['widgetgridfield'][k], function(k2, v2) {
                    if (v2.colshow == 1) {
                        theadcolumns += `<th class="text-`+v2.colheaderalign+`" style="width: `+v2.colwidth+`px; height: `+v2.colheight+`px;">`+v2.fieldcaption+`</th>`
                        tfootcolumns += `<th></th>`
                    }
                })
                theadcolumns += `<th>Action</th>`
                tfootcolumns += `<th></th>`
                $('#tab-thead-columns-'+v.maintable).html(theadcolumns)
                $('#tab-tfoot-columns'+v.maintable).html(tfootcolumns)
            }
        })
    }

    function getForm(isadd, widget_noid, element_id, responsefind=null) {
        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            },
            url: "/get_form_tmd",
            dataType: "json",
            data:{
                "_token":$('meta[name="csrf-token"]').attr('content'),
                table: maincms['menu']['noid'],
                maincms: JSON.stringify(maincms),
                isadd: isadd,
                key: 0,
                widget_noid: widget_noid,
            },
            beforeSend: function () {
                $('#'+element_id).LoadingOverlay("show");
            },
            success: function (response) {
                // console.log(response);
                var divform = ''
                divform += '<input type="hidden" id="noid" name="noid" value="">';
                // var colgroupname = [
                //     0: 'Administrasi'
                // ];
                // var colgroupname_data = [
                //     0: [
                //         //,
                //         //,
                //         //
                //     ]
                // ];
                // var colgroupname = new Array();
                // var colgroupname_data = new Array();
                $.each(response.field, function(key, value) {
                    // roles.fieldname.push(value.fieldname);
                    // roles.newhidden.push(value.newhidden);
                    // roles.edithidden.push(value.edithidden);

                    // if (value.colgroupname) {
                    //     if (colgroupname.indexOf(value.colgroupname) === -1) {
                    //         colgroupname.push(value.colgroupname);
                    //     }
                    //     colgroupname_data.push(value.field);
                    // } else {
                    //     // alert(key)
                        if (value.fieldname != 'idmaster') {
                            divform += value.field;
                        }
                    // }

                    // if (value.colsorted == 0) {
                    //     colsorted.push(key+1);
                    // }
                    
                });
                // colsorted[colsorted.length] = roles.fieldname.length+2;
                // colsorted.push(roles.fieldname.length+2);
                
                var divgroup = '';
                $.each(response.formgroupname, function(k, v) {
                    var formgroupname = k.split('-')

                    divgroup += `<div class="col-md-12">
                                    <div class="sub-card-custom">
                                        <div class="card-header card-header_ bg-main-1 d-flex">
                                            <div class="text-white" style="font-size: 14px">`+formgroupname[1].split(';')[1]+`</div>
                                        </div>
                                        <div class="card-body card-body-custom card-body-custom-group flat">
                                            <div class="right-tabs-group">`;
                                           
                    var key2 = 0;
                    var tab_widget = `<ul class="nav nav-tabs" id="tab-widget-`+k+`" role="tablist">`;
                    var tab_widget_content = `<div class="tab-content" id="tab-widget-content-`+k+`">`;
                    $.each(v, function(k2, v2) {
                        var active = '';
                        if (key2 == 0) {
                            active = 'active';
                        }

                        tab_widget += `<li class="nav-item">
                                        <a class="nav-link nav-link-custom flat `+active+`" id="tab-`+k2+`" data-toggle="tab" href="#`+k2+`" role="tab" aria-controls="`+k2+`"
                                        aria-selected="true" onclick="setTabActive(`+k2+`, '#tab-widget-`+k+` li a', '#tab-widget-`+k+` li #tab-`+k2+`', '#tab-widget-content-`+k+` .tab-widget-content-`+k+`', '#tab-widget-content-`+k+` #`+k2+`')">`+k2+`</a>
                                    </li>`;

                        tab_widget_content += `<div class="tab-pane tab-widget-content-`+k+` fade show `+active+`" id="`+k2+`" role="tabpanel" aria-labelledby="`+k2+`-tab">
                                                    <div class="row">`;
                        $.each(v2, function(k3, v3) {
                            tab_widget_content += v3;
                        })
                        tab_widget_content += `     </div>
                                                </div>`;
                        key2++;
                    });
                    tab_widget += `</ul>`;
                    tab_widget_content += `</div>`;
                    divgroup += tab_widget;
                    divgroup += tab_widget_content;

                    divgroup +=             `</div>
                                        </div>
                                    </div>
                                </div>`;
                });
                divform += divgroup;
                // console.log(colgroupname);
                // console.log(colgroupname_data);

                $('#'+element_id).html(divform);

                if (statusadd) {
                    $('#'+element_id).LoadingOverlay("hide");
                    // $('#another-card').show();
                } else {
                    if (element_id == 'divform') {
                        // console.log(widget_noid)
                        // console.log(divform)
                        
                        // $.ajax({
                        //     url: '/find_tmd',
                        //     type: "POST",
                        //     dataType: 'json',
                        //     header:{
                        //         'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                        //     },
                        //     data: {
                        //         "id" : nownoid,
                        //         "namatable" : $("#namatable").val(),
                        //         "_token": $('meta[name="csrf-token"]').attr('content'),
                        //         maincms: JSON.stringify(maincms)
                        //     },
                        //     success: function (response) {
                                // alert(response.length);return false;
                                if (responsefind != null) {
                                    nownoid = responsefind.nownoid;

                                    var maincms_panel = new Array();
                                    $.each(maincms['panel'], function(k, v) {
                                        $.each(maincms['widget'], function(k2, v2) {
                                            if (v.idwidget == v2.noid) {
                                                maincms_panel[v.noid] = v2;
                                            }
                                        })
                                    })

                                    // console.log(responsefind.result_detail)
                                    var key = 0;
                                    $.each(responsefind.result_detail, function(k, v) {
                                        if (key == 0) {
                                            nownoidtab = maincms_panel[k].noid
                                        }
                                        // alert(k)
                                        // console.log(v)
                                        // localStorage.removeItem('input-data-'+k)
                                        localStorage.setItem('input-data-'+k, JSON.stringify(v))
                                        localStorage.setItem('tablename-'+k, maincms_panel[k].maintable)
                                        // console.log(v)
                                        key++;
                                    })
                                    // $('#noid').val(id)
                                    // console.log('responsefind')
                                    // console.log(responsefind)
                                    $.each(responsefind, function(k, v) {
                                        if (v.formtypefield == 11) {
                                            $("#"+v.tablename+'-'+v.field+' input').val(v.value);
                                        } else if (v.formtypefield == 31){
                                            if (v.value == '1') {
                                                $("#"+v.tablename+'-'+v.field).attr('checked', true);
                                            }
                                        } else {
                                            $("#"+v.tablename+'-'+v.field).val(v.value);
                                        }

                                        if (v.disabled) {
                                            $("#"+v.tablename+'-'+v.field).attr('disabled', 'disabled');
                                        }
                                        if (statusdetail) {
                                            $("#"+v.tablename+'-'+v.field).attr('disabled', 'disabled');
                                        }
                                        if (v.edithidden) {
                                            $("#div-"+v.field).css('display', 'none');
                                        }
                                    });
                                    $('#'+element_id).LoadingOverlay("hide");
                                    console.log(divform)
                                }
                            // }
                        // });
                    }
                }

                $.each(response.field, function(k, v) {
                    if (v.newhidden == 1) {
                        $('#div-'+v.fieldname).css('display', 'none');
                    }
                })    

                $.each(response.allfield, function(k, v) {
                    // if (v.tablename == 'fincashbankdetail' && v.fieldname == 'nominal') {
                    //     console.log('cekkkkkk')
                    //     console.log(JSON.parse(v.onchange))
                    // }
                    if (v.onchange) {
                        var onchange = JSON.parse(v.onchange);
                        $.each(onchange, function(k2, v2) {
                            if (v2.TriggerOn == 'OnChange') {
                                var variables = new Array();
                                $.each(v2.sourcefield, function(k3, v3) {
                                    variables.push(v3.alias)
                                })

                                $.each(v2.sourcefield, function(k3, v3) {
                                    localStorage.removeItem(v.tablename+'-'+v2.targetfield+'-$'+v3.alias);
                                    // localStorage.setItem('$'+v3.alias, 0);
                                    // alert(v3.fieldname)
                                    // if (v3.fieldname == 'nominal') {
                                    //     console.log('////////////////')
                                    //     console.log(v2.targetfield)
                                    // }
                                    if (v3.sourcetype == 'EditBox') {
                                        var value_onkeyup = document.querySelector('#'+v.tablename+'-'+v3.fieldname).getAttribute('onKeyUp');
                                        if (value_onkeyup == null) {
                                            value_onkeyup = ''
                                        }
                                        $('#'+v.tablename+'-'+v3.fieldname).attr('onKeyUp', value_onkeyup+`triggerOn(this, 'EditBox', '`+v.tablename+`', '`+JSON.stringify(variables)+`', '`+v2.throwtype+`', '`+v2.targettable+`', '`+v2.targetfield+`', '`+v3.alias+`', '`+v3.fieldname+`', '`+v3.fieldvalue+`', '`+v2.formula+`');`);
                                    } else {
                                        var value_onchange = document.querySelector('#'+v.tablename+'-'+v3.fieldname).getAttribute('onChange');
                                        if (value_onchange == null) {
                                            value_onchange = ''
                                        }
                                        $('#'+v.tablename+'-'+v3.fieldname).attr(v2.TriggerOn, value_onchange+`triggerOn(this, 'NotEditBox', '`+v.tablename+`', '`+JSON.stringify(variables)+`', '`+v2.throwtype+`', '`+v2.targettable+`', '`+v2.targetfield+`', '`+v3.alias+`', '`+v3.fieldname+`', '`+v3.fieldvalue+`', '`+v2.formula+`');`);
                                        $('#'+v.tablename+'-'+v3.fieldname).trigger('change')
                                        // console.log(v.tablename+'-'+v3.fieldname);
                                    }
                                })
                            } else if (v2.TriggerOn == 'OnSave') {
                                mainonchange.push(v2)
                            }
                        })
                    }

                    // if (v.newreadonly) {
                    //     $('#'+v.tablename+'-'+v.fieldname).
                    // }
                })    
                
                // console.log(mainonchange)

                // $.each(response.addonchange, function(k, v) {
                //     $('#'+v).attr('onChange', 'alert("add_onchange")')
                // })

                $('.datepicker').datepicker({
                    format: 'd-M-yyyy',
                    autoclose: true
                    // startDate: '-3d'
                });

                //select2
                $('.select2').select2();
                $('.select2').css('width', '100%');
                $('.select2 .selection .select2-selection').css('height', '35px');
                $('.select2 .selection .select2-selection .select2-selection__rendered').css('margin-top', '-6px');
                $('.select2 .selection .select2-selection .select2-selection__arrow').css('margin-top', '4px');

                $.each(response.allfield, function(k, v) {
                    if (v.newreadonly) {
                        // alert('#select2-'+v.tablename+'-'+v.fieldname+'-container');
                        $('#div-'+v.tablename+'-'+v.fieldname+' span span .select2-selection--single').css('background-color', '#E5E5E5');
                    }
                })
                //select2readonly
                // $('.select2readonly').select2();
                // $('.select2readonly').css('width', '100%');
                // $('.currency').currency({region:'IDR'});
                // console.log(roles)  
                if (element_id != 'divform') {
                    $('#'+element_id).LoadingOverlay("hide");
                }
            },
        });
    }

    function triggerOn(context, sourcetype, tablename, variables, throwtype, targettable, targetfield, alias, fieldname, fieldvalue, formula) {
        // alert($('#idcurrency').val())
        // alert('#'+tablename+'-'+fieldname)
        var tt = tablename+'-'+targetfield;
        var tf = tablename+'-'+fieldname;
        
        if (sourcetype == 'EditBox') {
            localStorage.setItem(tt+'-$'+alias, $('#'+tf).val());
            // console.log(formula)
            // localStorage.removeItem('$'+alias);

            var replaceformula = formula;
            var f = formula.split('*');
            $.each(JSON.parse(variables), function(k, v) {
                var ls = localStorage.getItem(tt+'-$'+v);
                console.log('ls-'+tt+'-$'+v+' = '+localStorage.getItem(tt+'-$'+v))
                if (ls) {
                    // for (var i=0; i<10; i++) {
                        replaceformula = replaceformula.replace('$'+v, ls);
                        // console.log('ceking = '+ls)
                    // }
                } else {
                    replaceformula = replaceformula.replace('$'+v, 0);
                }
            })
            
            if (formula.split(' ')[0] != 'if') {
                $('#'+tt).val(eval(replaceformula))
            }
            // console.log('#'+tt)
            // console.log(replaceformula)
            // console.log(eval(replaceformula))
            // console.log(fieldname)
            // console.log(fieldvalue)
            // console.log(formula)
        } else {
            localStorage.setItem(tt+'-$'+alias, $('#'+tf+' :selected').val());
            // console.log('isi = '+$('#'+tf+' :selected').val())

            var getattribute = 'data-'+tf+'-'+$('#'+tf+' :selected').val();
            // console.log('testing = '+context.getAttribute(getattribute));
            // console.log('#'+tt)
            // alert(1)
            if (throwtype == 'FILTER') {
                $('#'+tt).attr('data-'+tf, $('#'+tf+' :selected').val())
                
                // console.log(context)
                // console.log('data-'+tt+'-querylookup')
                // console.log(context.getAttribute(getattribute))
                // console.log(document.querySelector('#'+tt))

                var frml = formula.split(';');
                var frml_where = '';
                $.each(frml, function(k, v) {
                    var _v = v.split(':');
                    if (_v[0] == 'WHERE') {
                        __v = _v[1].split(',');
                        frml_where += __v[0]+' '+__v[1]+' '+__v[2]+' '+__v[3]+' ';
                    }  
                })

                var querylookup = document.querySelector('#'+tt).getAttribute('data-'+tt+'-querylookup').split(';');
                var select = querylookup[0].split(':')[1];
                var from = querylookup[1].split(':')[1];
                var where = querylookup[2].split(':')[1].split('AND');
                var order = querylookup[3].split(':')[1].split(',');
        
                var query = 'SELECT '+select+' FROM '+from+' WHERE ';
                var add_frml = false;
                $.each(where, function(k, v) {
                    if (v) {
                        if (k > 0) {
                            query += 'AND ';
                            add_frml = true;
                        }
                        var _v = v.split(',');
                        query += _v[0]+' '+_v[1]+' '+_v[2]+' ';
                    }
                })
                if (add_frml) {
                    frml_where = frml_where.substr(4);
                }
                query += frml_where;
                
                query += ' ORDER BY '+order[0]+' '+order[1];
                query = query.replace('$'+alias, document.querySelector('#'+tt).getAttribute('data-'+tf))
                // console.log(frml_where.substr(4))

                getSelectChild(query, '#'+tt)

                // console.log('query = '+query)
                // console.log('filter = '+document.querySelector('#'+tt).getAttribute('data-'+tf))

            } else {
                $('#'+tt).val(context.getAttribute(getattribute))
            }
        }


        if (formula.split(' ')[0] == 'if') {
            var replaceformula = formula;
            $.each(JSON.parse(variables), function(k, v) {
                var ls = localStorage.getItem(tt+'-$'+v);
                console.log('ls-'+tt+'-$'+v+' = '+localStorage.getItem(tt+'-$'+v))
                if (ls) {
                    // for (var i=0; i<10; i++) {
                        replaceformula = replaceformula.replace('$'+v, ls);
                        // console.log('ceking = '+ls)
                    // }
                } else {
                    replaceformula = replaceformula.replace('$'+v, 0);
                }
            })
            eval("var fn = function(){ "+replaceformula+" }")
            // alert(replaceformula)
            // alert(fn())
            $('#'+tt).val(fn())
        }

        $('#'+tt).trigger('change')
        $('#'+tt).trigger('keyup')

        // console.log(targetfield)
        // console.log(context.getAttribute(getattribute))
        // console.log(context.getAttribute(getattribute))
        // if (targettable == 'self') {
        //     targettable = 'accmcurrency';
        //     $.ajax({
        //         type: "POST",
        //         header:{
        //             'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
        //         },
        //         url: "/get_trigger_on_tmd",
        //         dataType: "json",
        //         data:{
        //             "_token":$('meta[name="csrf-token"]').attr('content'),
        //             query: `SELECT `+fieldvalue+` AS `+alias+` FROM `+targettable+` WHERE noid='`+context.getAttribute(getattribute)+`' LIMIT 1`,
        //             alias: alias,
        //             formula: formula,
        //         },
        //         success: function(response) {
        //             $('#'+targetfield).val(response)
        //             console.log(response)
        //         }
        //     })
        // }
    }

    function getSelectChild(query, element_target) {
        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            },
            url: "/get_select_child_tmd",
            data:{
                "_token":$('meta[name="csrf-token"]').attr('content'),
                query: query,
            },
            success: function (response) {
                $(element_target).html(response)
            }
        })
    }

    function getFormTab() {
        var tab_form = '';
        var tab_form_content = '';
        $.each(maincms['widget'], function(k0, v0) {
            if (k0 > 0) {
                var active = '';
                if (k0 == 1) {
                    active = 'active';
                }

                $.ajax({
                    type: "POST",
                    header:{
                        'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                    },
                    url: "/get_form_tmd",
                    dataType: "json",
                    data:{
                        "_token":$('meta[name="csrf-token"]').attr('content'),
                        table: maincms['menu']['noid'],
                        maincms: JSON.stringify(maincms),
                        isadd: true,
                        key: 1,
                    },
                    success: function (response) {
                        console.log('DDDDD');
                        console.log(response);
                        tab_form = `<li class="nav-item">
                                        <a class="nav-link flat `+active+`" id="tab-`+v0.maintable+`" data-toggle="tab" href="#`+v0.maintable+`" role="tab" aria-controls="`+v0.maintable+`"
                                        aria-selected="true" onclick="setTabActive(`+v0.maintable+`, '#tab-widget-`+v0.maintable+` li a', '#tab-widget-`+v0.maintable+` li #tab-`+v0.maintable+`', '#tab-widget-content-`+v0.maintable+` div', '#tab-widget-content-`+v0.maintable+` #`+v0.maintable+`')">`+k0+`</a>
                                    </li>`;
                        tab_form_content += `<form id="form-`+v0.maintable+`">
                                        <div class="row">`;
                        $.each(response.field, function(key, value) {
                            if (value.tablemaster != maincms['widget'][0]['maintable']) {
                                tab_form_content += value.field;
                            }
                        });
                        tab_form_content += `   </div>
                                    </form>`;

                        var tab_action = `<li class="nav-item" style="padding-top: 5px">
                                                <button class="btn btn-sm btn-primary"><i class="fa fa-plus"></i></button>
                                                <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                          </li>`;

                        $('#tab-form-'+v0.maintable).append(tab_form+tab_action);
                        $('#tab-form-content-'+v0.maintable).append(tab_form_content);

                        $('.datepicker').datepicker({
                            format: 'd-M-yyyy',
                            autoclose: true
                        });
                    },
                });
            }
        });
    }

    function setTabActive(panel_noid, element_class, element_id, element_content_class, element_content_id) {
        // alert('setTabActive('+element_class+', '+element_id+', '+element_content_class+', '+element_content_id+')')
        nownoidtab = panel_noid
        another_selecteds = [];
        $(element_class).removeClass('active')
        $(element_id).addClass('active')
        $(element_content_class).removeClass('active')
        $(element_content_id).addClass('active')
        // alert(element_content_id)
    }

    function getCmsTab(statusadd) {
        var tab_widget = '';
        var tab_widget_content = '';
        // var maincms_widget = new Array();
        // $.each(maincms['widget'], function(k, v) {
        //     maincms_widget[v.noid] = v;
        // })
        var maincms_widgetgridfield = new Array();
        $.each(maincms['widgetgridfield'], function(k, v) {
            $.each(v, function(k2, v2) {
                maincms_widgetgridfield[v2.idcmswidgetgrid] = v;
            })
        })
        // console.log(maincms_widgetgridfield)
        $.each(maincms['panel'], function(k, v) {
        // console.log(maincms_widgetgridfield[v.idwidget].colshow)
            var v_widget = maincms_widget[v.idwidget];
            if (k > 0) {
                statusaddtab[v.noid] = true;

                var active = '';
                if (k == 1) {
                    active = 'active';
                }

                var tabcaption = v.tabcaption.split(';');

                tab_widget += `<li class="nav-item">
                                    <a class="nav-link nav-link-custom flat `+active+`" id="btn-tab-`+v.noid+`" data-toggle="tab" href="#`+v.noid+`" role="tab" aria-controls="`+v.noid+`"
                                    aria-selected="true" onclick="setTabActive(`+v.noid+`, '#tab-widget li a', '#tab-widget-`+v.noid+` li #btn-tab-`+v.noid+`', '#tab-widget-content .tab-widget-content', '#tab-widget-content-`+v.noid+`')">`+tabcaption[1]+`</a>
                                </li>`;

                tab_widget_content += `<div class="tab-pane tab-widget-content fade show `+active+`" id="tab-widget-content-`+v.noid+`" role="tabpanel" aria-labelledby="`+v.noid+`-tab">
                                            <div class="card card-custom flat">
                                                <div class="card-header card-header_ bg-main-1 d-flex flat" >
                                                    <div class="card-title">
                                                        <span class="card-icon">
                                                            <i class="`+v_widget.widgeticon+` text-white"></i>
                                                        </span>
                                                        <h3 class="card-label text-white">
                                                            `+v_widget.widgetcaption+`
                                                        </h3>
                                                    </div>`;

                // if (!statusadd) {
                    tab_widget_content += `         <div class="card-toolbar" >
                                                        <div id="add-data-`+v.noid+`">
                                                            <a onclick="addDataTab('`+v.noid+`')" href="javascript:;" id="btn-add-data-`+v.noid+`" style="color: #FFFFFF;margin-right: 5px;" class="btn btn-sm flat" onmouseover="mouseoverAddData('btn-add-data-`+v.noid+`')" onmouseout="mouseoutAddData('btn-add-data-`+v.noid+`')"><i style="color:#FFFFFF" class="fa fa fa-save"></i> Tambah Data</a>
                                                        </div>
                                                        <div id="save-data-`+v.noid+`" style="display: none">
                                                            <a onclick="saveDataTab(`+v.noid+`)" id="btn-save-data-`+v.noid+`" class="btn btn-sm btn-success flat" style="margin-right: 5px;">
                                                                <i class="fa fa-save"></i> Save
                                                            </a>
                                                        </div>
                                                        <div id="cancel-data-`+v.noid+`" style="display: none">
                                                            <a onclick="cancelDataTab('`+v.noid+`')" class="btn btn-sm btn-default flat" style="margin-right: 5px;">
                                                                <i class="fa fa-reply"></i> Cancel
                                                            </a>
                                                        </div>
                                                        <div id="back-data-`+v.noid+`" style="display: none">
                                                            <a id="btn-back-`+v.noid+`" class="btn btn-sm btn-success flat" onclick="cancelDataTab()"  style="margin-right: 5px;">
                                                                <i class="fa fa-reply"></i> Back
                                                            </a>
                                                        </div>
                                                        <div class="dropdown" style="margin-right: 5px;">
                                                            <a style="background-color: transparent !important; border: 1px solid #95c9ed; color: #aad4f0;" class="btn btn-default btn-sm flat" href="javascript:;" data-toggle="dropdown">
                                                                <i style="color:#FFFFFF" class="fa fa-cogs"></i>
                                                                    Tools
                                                                <i style="color:#FFFFFF" class="fa fa-angle-down"></i>
                                                            </a>
                                                            <ul style="" id="another-drop_export" class="dropdown-menu dropdown-menu-right">
                                                                <li id="another-space-button-export-`+v.noid+`"></li>
                                                            </ul>
                                                        </div>
                                                        <a style="background-image: none !important; margin-right: 5px; /* height: 27px; */ width: 27px !important; text-align: center; padding: 2px !important; border-radius: 50% !important; background-color: #2196F3; color: #FFF;" id="tool_refresh-`+v.noid+`" href="javascript:;" onclick="refresh(`+v.noid+`)" class="bg-main-1"><i style="color:#FFFFFF" class="fa fa-sync-alt"></i></a>
                                                        <a style="background-image: none !important; margin-right: 5px; /* height: 27px; */ width: 27px !important; text-align: center; padding: 2px !important; border-radius: 50% !important; background-color: #2196F3; color: #FFF;" id="tool_filter-`+v.noid+`" href="javascript:;" onclick="showModalFilter(`+v.noid+`)" class="bg-main-1"><i style="color:#FFFFFF" class="fa fa-filter"></i></a>
                                                        <a style="background-image: none !important; /* height: 27px; */ width: 27px !important; text-align: center; padding: 2px !important; border-radius: 50% !important; background-color: #2196F3; color: #FFF;" id="tool_refresh_up-`+v.noid+`" onClick="toggleButtonCollapse(`+v.noid+`)" href="javascript:;" class="bg-main-1"><i style="color:#FFFFFF" class="fa fa-sort"></i></a>
                                                    </div>`;
                // }

                         tab_widget_content += `</div>
                                                <div class="card-body card-body-tab-custom">
                                                    <div class="row" style="display: block">
                                                        <div id="space_filter_button-`+v.noid+`" style="margin-bottom: 10px">
                                                        </div>
                                                    </div>`
                             tab_widget_content += `<form data-toggle="validator" class="form" id="tab-form-`+v.noid+`" style="display: none">
                                                        <div class="row" id="tab-form-div-`+v.noid+`">
                                                        </div>
                                                    </form>`

                            // comment
                            //  tab_widget_content += `<table id="tab-table-`+v.noid+`" class="table table-striped table-bordered table-condensed nowrap" cellspacing="0" width="100%" style="background-color:#BBDEFB !important; border-collapse: collapse !important;">
                            //                             <thead>
                            //                                 <tr id="tab-thead-`+v.noid+`" class="bg-main">`

                            //             var colspan = 0;
                            //             $.each(maincms_widgetgridfield[v.idwidget], function(k2, v2) {
                            //                 if (v2.colshow == 1) {
                            //                     colspan++
                            //                 }
                            //             })
                            //             tab_widget_content += `<th style="text-align:center" colspan="2">#</th>`;
                            //             tab_widget_content += `<th style="text-align:center" colspan="`+colspan+`"></th>`;
                            //             tab_widget_content += `<th colspan="1" style="text-align:center">---</th>`;

                            //          tab_widget_content += `</tr>
                            //                                 <tr id="tab-thead-columns-`+v.noid+`">`

                                    
                            //             tab_widget_content += `<th><input type="checkbox" id="select_all_`+v.noid+`" onchange="selectAllCheckboxTab(`+v.noid+`)" class="checkbox-`+v.noid+` checkbox-`+v.noid+`-0" name="checkbox-`+v.noid+`[]" value="FALSE"></th>
                            //                                    <th>No</th>`;
                            //      $.each(maincms_widgetgridfield[v.idwidget], function(k2, v2) {
                            //          if (v2.colshow == 1) {
                            //              tab_widget_content += `<th class="text-`+v2.colheaderalign+`" style="width: `+v2.colwidth+`px; height: `+v2.colheight+`px;">`+v2.fieldcaption+`</th>`
                            //          }
                            //      })

                            //             tab_widget_content += `<th>Action</th>`

                            //          tab_widget_content += `</tr>
                            //                             </thead>
                            //                         </table>`
                            // endcomment


                            tab_widget_content += `<div id="tab-appendInfoDatatable-`+v.noid+`" style="border: 1px solid #3598dc;"></div>
                                                    <div id="tab-appendButtonDatatable-`+v.noid+`"></div>
                                                    <div id="ts-`+v.noid+`" class="table-scrollable" style="border: 1px solid #3598dc;">`
                             tab_widget_content += `<table bg-color="blue" class="nowrap table table-striped table-bordered table-hover  dataTable no-footer" id="tab-table-`+v.noid+`" style="background-color:#BBDEFB !important;"  aria-describedby="table_master_info" role="grid">
                                                        <thead>
                                                            <tr id="tab-thead-`+v.noid+`" class="bg-main">`

                                        var colspan = 0;
                                        var colspanfooter = 0;
                                        var tfootcolumns = `<tfoot>
                                                                <tr>`
                                        var index = 2;
                                        var data_fft = new Array();
                                        $.each(maincms_widgetgridfield[v.idwidget], function(k2, v2) {
                                            if (v2.colshow == 1) {
                                                colspan++
                                                if (v2.colsummaryfield || v2.colsummaryfield != null) {
                                                    tfootcolumns += `<th class="text-right" colspan="`+colspanfooter+`" data-function="" style="border: 1px solid #95c9ed"></th>`
                                                    // console.log('collll '+colspanfooter)
                                                    colspanfooter = 1
                                                    var dataconfig = JSON.parse(v2.colsummaryfield).dataconfig[0].summarytype+'('+index+')'
                                                    data_fft.push({
                                                        "index": index,
                                                        "function": dataconfig,
                                                    })
                                                } else {
                                                    colspanfooter++
                                                }
                                                index++;
                                            }
                                            if (maincms_widgetgridfield[v.idwidget].length == k2+1) {
                                                // console.log('terakhir '+colspanfooter)
                                                tfootcolumns += `<th class="text-right" colspan="`+(colspanfooter)+`" style="border: 1px solid #95c9ed"></th>`
                                            }
                                        })
                                        
                                        functionfootertab[v.idwidget] = data_fft;
                                        
                                        tfootcolumns += `</tr>
                                                        </tfoot>`


                                        tab_widget_content += `<th style="text-align:center" colspan="2">#</th>`;
                                        tab_widget_content += `<th style="text-align:center" colspan="`+colspan+`"></th>`;
                                        tab_widget_content += `<th colspan="1" style="text-align:center">---</th>`;

                                     tab_widget_content += `</tr>
                                                            <tr id="tab-thead-columns-d-`+v.noid+`">`

                                    
                                        tab_widget_content += `<th style="width: 30px;"><input type="checkbox" id="select_all_`+v.noid+`" onchange="selectAllCheckboxTab(`+v.noid+`)" class="checkbox-`+v.noid+` checkbox-`+v.noid+`-0" name="checkbox-`+v.noid+`[]" value="FALSE"></th>
                                                               <th style="width: 30px;">No</th>`;
                                 $.each(maincms_widgetgridfield[v.idwidget], function(k2, v2) {
                                     if (v2.colshow == 1) {
                                         tab_widget_content += `<th class="text-`+v2.colheaderalign+`" style="width: `+v2.colwidth+`px; height: `+v2.colheight+`px;">`+v2.fieldcaption+`</th>`
                                     }
                                 })

                                        tab_widget_content += `<th style="width: auto;">Action</th>`

                                     tab_widget_content += `</tr>
                                                        </thead>`
                                     tab_widget_content += tfootcolumns
                                     tab_widget_content += `</table>`
                                                        
                         tab_widget_content += `</div>
                                            </div>
                                        </div>
                                    </div>`
            }
        })
        $('#tab-widget').html(tab_widget);
        $('#tab-widget-content').html(tab_widget_content);
    }

    function saveDataTab(panel_noid) {
        // localStorage.removeItem('input-data-'+panel_noid);

        // alert('nextnoid='+nextnoid);
        // alert('nownoid='+nownoid);
        // return false;
        var onrequired = false;
        var errormessage = '';
        if (statusaddtab[panel_noid] == true) {
            var idd = {};
            var idd_fix = new Array();
            $.each($('#tab-form-'+panel_noid).serializeArray(), function(k, v) {
                if (k == 0) {
                    idd['idmaster'] = statusadd ? nextnoid : nownoid;
                    idd['noid'] = nextnoidtab[panel_noid];
                    nextnoidtab[panel_noid]++;
                } else {
                    idd[v.name] = v.value
                    var tf = maintable[panel_noid]+'-'+v.name;
                    var coltypefield = document.querySelector('#'+tf).getAttribute('data-'+tf+'-coltypefield');
                    if (coltypefield == 96) {
                        idd[v.name+'_lookup'] = $('#'+tf+' :selected').text();
                        // alert(idd[v.name+'_lookup'])
                    }

                    var formcaption = $('#'+maintable[panel_noid]+'-'+v.name).attr('data-'+maintable[panel_noid]+'-formcaption');
                    if ($('#'+tf).attr('data-'+maintable[panel_noid]+'-newreq') == 1) {
                        if (v.value == '') {
                            onrequired = true;
                            errormessage += formcaption+' is required!<br>'
                        }
                    }
                }
            })

            if (onrequired) {
                $.notify({
                    message: errormessage
                },{
                    type: 'danger'
                });
                return false;
            }

            idd_fix[0] = idd;
            current_idd = localStorage.getItem('input-data-'+panel_noid);

            if (current_idd == null) {
                localStorage.setItem('input-data-'+panel_noid, JSON.stringify(idd_fix))
            } else {
                merge_idd = JSON.parse(current_idd).concat(idd_fix)
                localStorage.removeItem('input-data-'+panel_noid);
                localStorage.setItem('input-data-'+panel_noid, JSON.stringify(merge_idd))
            }   
            // console.log('SAVEDATATAB')
            // console.log(JSON.parse(localStorage.getItem('input-data-'+panel_noid)))
            // console.log(idd)
            // return false
            // refreshSumTab(0, panel_noid)
        } else {
            var idd = {};
            var idd_fix = new Array();
            $.each($('#tab-form-'+panel_noid).serializeArray(), function(k, v) {
                if (k == 0) {
                    idd['idmaster'] = nextnoid
                    idd['noid'] = nextnoidtab[panel_noid];
                    nextnoidtab[panel_noid]++;
                } else {
                    idd[v.name] = v.value
                    var tf = maintable[panel_noid]+'-'+v.name;

                    var formcaption = $('#'+maintable[panel_noid]+'-'+v.name).attr('data-'+maintable[panel_noid]+'-formcaption');
                    if ($('#'+tf).attr('data-'+maintable[panel_noid]+'-editreq') == 1) {
                        if (v.value == '') {
                            onrequired = true;
                            errormessage += formcaption+' is required!<br>'
                        }
                    }
                }
            })

            if (onrequired) {
                $.notify({
                    message: errormessage
                },{
                    type: 'danger'
                });
                return false;
            }

            idd_fix[0] = idd;
            current_idd = localStorage.getItem('input-data-'+panel_noid);

            var replace_current_idd = new Array();
            $.each(JSON.parse(current_idd), function(k, v) {
                if (v.noid == nownoidtab) {
                    replace_current_idd[k] = idd;
                    replace_current_idd[k]['noid'] = nownoidtab;
                    replace_current_idd[k]['idmaster'] = nownoid;
                } else {
                    replace_current_idd[k] = v;
                }
            })
            localStorage.removeItem('input-data-'+panel_noid);
            localStorage.setItem('input-data-'+panel_noid, JSON.stringify(replace_current_idd))
            // console.log(replace_current_idd)
            // console.log(idd)
        }

        $.each(mainonchange, function(k, v) {
            var current_idd_mo = JSON.parse(localStorage.getItem('input-data-'+panel_noid))
            // console.log(current_idd_mo)
            $.each(v.sourcefield, function(k2, v2) {
                if (v2.sourcetype == 'Summary') {
                    var summary = 0;
                    $.each(current_idd_mo, function(k3, v3) {
                        // console.log(v3)
                        summary = parseInt(summary)+parseInt(v3[v2.fieldname]);
                    })
                    $('#'+maintable[v.targettable]+'-'+v.targetfield).val(summary)
                    $('#'+maintable[v.targettable]+'-'+v.targetfield).trigger('change')
                    $('#'+maintable[v.targettable]+'-'+v.targetfield).trigger('keyup')
                    console.log('#'+maintable[v.targettable]+'-'+v.targetfield+' = '+summary)
                }
            })
        })


        refreshTab()
        cancelDataTab(panel_noid)
        // localStorage.removeItem('input-data-'+panel_noid);
        // console.log(JSON.parse(localStorage.getItem('input-data-'+panel_noid)))
        
        // console.log('aku cek ini')
        // console.log(JSON.parse(localStorage.getItem('input-data-'+panel_noid)))
        // console.log(panel_noid)
        // return false
    }

    function refreshSumTab(id, panel_noid) {

///////////////////

        // var maincms_widgetgrid = new Array();
        // $.each(maincms['widgetgrid'], function(k, v) {
        //     maincms_widgetgrid[v.idcmswidget] = v;
        // })

        if (maincms_widgetgrid[panel_noid]['gridconfig']) {
            // var wgb_onsave = maincms_widgetgrid[panel_noid]['gridconfig']['buttonevent']['OnSave'];
            var wgb_ondelete = JSON.parse(maincms_widgetgrid[panel_noid]['gridconfig'])['buttonevent']['OnDelete'];
            if (wgb_ondelete['status']) {
                $.each(wgb_ondelete['Parameter'], function(k, v) {
                console.log(v.Source['sourcetype']);
                // return false;
                    if (v.typeaction == 'EXEC_QUERY') {
                        // return false;
                        if (v['Source']['sourcetype'] == 'sum') {
                            var current_idd_mo = JSON.parse(localStorage.getItem('input-data-'+panel_noid))
                            var summary = 0;
                            var idtypetranc = '';
                            var idmaster = '';
                            var noid = '';
                            var globaluser = 59;
                            $.each(current_idd_mo, function(k2, v2) {
                                summary = parseInt(summary)+parseInt(v2[v['Source']['fieldname']]);
                                if (k2 == id) {
                                    idtypetranc = v2['idtypetranc'];
                                    idmaster = v2['idmaster'];
                                    noid = parseInt(nownoidtab)+parseInt(k2)+1;
                                }
                            })
                            if (v['Target']['targettype'] == 'EXEC_QUERY') {
                                var query = v['Query']['query'].replace('$idtypetranc', idtypetranc).replace('$idmaster', idmaster).replace('$noid', noid).replace('$GLOBAL_USER', globaluser)
                                // console.log(query)
                                // return false;
                                
                                $.ajax({
                                    type: "POST",
                                    header:{
                                        'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                                    },
                                    url: "/save_log_tmd",
                                    data:{
                                        "_token": $('meta[name="csrf-token"]').attr('content'),
                                        "idconnection": v['Query']['idconnection'],
                                        "query": query,
                                    },
                                    success: function(response) {
                                    }
                                })
                            }
                        }
                    }
                })
            }
        }
    }
    
    function removeSelected(selecteds, scma=null) {
        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            },
            url: "/delete_select"+main.urlcode,
            dataType: "json",
            data:{
                "_token": $('meta[name="csrf-token"]').attr('content'),
                "selecteds": selecteds,
                "selectcheckboxmall": scma
            },
            success: function(response) {
                $.notify({
                    message: 'Delete data has been succesfullly.' 
                },{
                    type: 'success'
                });
                selectcheckboxm = new Array();
                refresh()
                // var filter_button = '';
                // for (var i=0; i<response.filterButton[0].length; i++) {
                //     filter_button += response.filterButton[0][i].taga;
                // }
                // $('#space_filter_button').html(filter_button)
                // console.log(filter_button);
            },
        });
    }

    function removeSelectedTab(selecteds) {
        var maincms_panel = new Array();
        $.each(maincms['panel'], function(k, v) {
            $.each(maincms['widget'], function(k2, v2) {
                if (v.idwidget == v2.noid) {
                    maincms_panel[v.noid] = v2;
                }
            })
        })

        // $.ajax({
        //     type: "POST",
        //     header:{
        //         'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
        //     },
        //     url: "/delete_selected_tmd",
        //     dataType: "json",
        //     data:{
        //         "_token": $('meta[name="csrf-token"]').attr('content'),
        //         "table" : maincms_panel[nownoidtab].maintable,
        //         "selecteds": selecteds,
        //     },
        //     success: function(response) {
                // var replace_input_data = localStorage.getItem('input-data-'+nownoidtab);
                var replaceinputdata = new Array();
                $.each(JSON.parse(localStorage.getItem('input-data-'+nownoidtab)), function(k, v) {
                    var notinput = false;
                    
                    $.each(selecteds, function(k2, v2) {
                        if (v.noid == v2) {
                            notinput = true
                        }
                    })

                    if (!notinput) {
                        replaceinputdata.push(v);
                    }
                })
                // console.log('data tidak terhapus')
                // console.log(replaceinputdata)
                // return false
                localStorage.setItem('input-data-'+nownoidtab, JSON.stringify(replaceinputdata))

                // console.log(replaceinputdata);
                // return false;

                $.notify({
                    message: 'Delete data has been succesfullly.' 
                },{
                    type: 'success'
                });
                refreshTab()
                // var filter_button = '';
                // for (var i=0; i<response.filterButton[0].length; i++) {
                //     filter_button += response.filterButton[0][i].taga;
                // }
                // $('#space_filter_button').html(filter_button)
                // console.log(filter_button);
        //     },
        // });
    }

    function getCmsThead() {
        var count_wgf = 0;
        var colgroupname = new Array();
        $.each(maincms['widgetgridfield'][0], function(k, v) {
            if (v.colshow == 1) {
                if (v.colgroupname) {
                    colgroupname.push(v.colgroupname);
                }
                count_wgf++;
            }
        })
        const unique = (value, index, self) => {
            return self.indexOf(value) === index
        }
        colgroupname = colgroupname.filter(unique)
        console.log(colgroupname)

        if (maincms['widgetgrid'][0]['colcheckbox']) {
            var thead = `<th style="text-align:center" colspan="2">#</th>`;
        } else {
            var thead = `<th style="text-align:center" colspan="1">#</th>`;
        }
        // $.each(maincms['widgetgridfield'], function(k, v) {
            thead += `<th style="text-align:center" colspan="`+count_wgf+`"></th>`;
        // })
        thead += `<th colspan="1" style="text-align:center">---</th>`;
        $('#thead').html(thead);
    }

    function getCmsTabThead(panel_idwidget) {
        $.each(maincms['widgetgridfield'], function(k, v) {
            if (k > 0) {
                if (v.idcmswidgetgrid == panel_idwidget)
                    if (v.colshow == 1) {
                        var thead = `<th style="text-align:center" colspan="2">#</th>`;
                        // $.each(maincms['widgetgridfield'], function(k, v) {
                            thead += `<th style="text-align:center" colspan="`+v.length+`"></th>`;
                        // })
                        thead += `<th colspan="1" style="text-align:center">---</th>`;
                        return thead;
                        // $('#tab-thead-'+maincms['widget'][k].maintable).html(thead);
                        // console.log('#tab-thead-'+maincms['widget'][k].maintable)
                    }
            }
        })
    }

    function saveFilter() {
        // $('#modal-notif').css('display', 'none')
        // var config_widget = {
        //     WidgetFilter: '',
        //     WidgetConfig: '',
        // }
        var formdata = $('#formfilter').serialize();

        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            },
            url: "/save_filter_tmd",
            dataType: "json",
            data:{
                "_token":$('meta[name="csrf-token"]').attr('content'),
                "data" :{
                    'data': formdata,
                    'url_code': $('#url_code').val()
                }
            },
            success:function(response){
                // console.log(response)
                if (response.result) {
                    // alert('Success')
                    closeModalFilter()
                    $.notify({
                        message: 'Save data has been succesfullly.' 
                    },{
                        type: 'success'
                    });
                    setInterval(function(){
                        location.reload()
                    }, 3000)
                    // location.reload()
                } else {
                    // $('#modal-notif').css('display', 'block')
                    // setInterval(function(){
                    //     $('#modal-notif').css('display', 'none')
                    // }, 3000)
                    
                    $.notify({
                        message: 'Save data has been error.' 
                    },{
                        type: 'danger'
                    });
                }
            }
        });
    }

    function delRow(key) {
        filtercontenttabs.filtercontentrow[selected_filter_tabs][key] = '';
        getFilterContentTabs()
    }

    function addRow() {
        filtercontenttabs.filtercontentrow[selected_filter_tabs].push(`<input type="hidden" name="tabsrow[]" value="`+selected_filter_tabs+`">
                                                                        <div class="row filter-content-row-`+selected_filter_tabs+`">
                                                                            <div class="col-md-2">
                                                                                <div class="form-group">
                                                                                    <label class="col-md-12 control-label">Caption</label>
                                                                                    <input class="form-control input-sm" name="buttoncaption[]" id="buttoncaption-`+selected_filter_tabs+`-`+filtercontenttabs.filtercontentrow[selected_filter_tabs].length+`" value="" placeholder="Caption">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label class="col-md-12 control-label">Condition</label>
                                                                                    <input class="form-control input-sm" name="condition[]" id="condition-`+selected_filter_tabs+`-`+filtercontenttabs.filtercontentrow[selected_filter_tabs].length+`" value="" placeholder="groupoperand#fieldname;operator;value;operand">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1">
                                                                                <div class="form-group">
                                                                                    <label class="col-md-12 control-label">Active</label>
                                                                                    <select class="form-control input-sm" name="buttonactive[]" id="buttonactive-`+selected_filter_tabs+`-`+filtercontenttabs.filtercontentrow[selected_filter_tabs].length+`" value="1">
                                                                                        <option value="1">True</option>
                                                                                        <option value="0" selected>False</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1">
                                                                                <div class="form-group">
                                                                                    <label class="col-md-12 control-label">Icon</label>
                                                                                    <input class="form-control input-sm" name="classicon[]" id="classicon-`+selected_filter_tabs+`-`+filtercontenttabs.filtercontentrow[selected_filter_tabs].length+`" value="" placeholder="Icon">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1">
                                                                                <div class="form-group">
                                                                                    <label class="col-md-12 control-label">Color</label>
                                                                                    <input class="form-control input-sm" name="classcolor[]" id="classcolor-`+selected_filter_tabs+`-`+filtercontenttabs.filtercontentrow[selected_filter_tabs].length+`" value="" placeholder="Color">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1" style="text-align:center;padding:21px 0 0 0">
                                                                                <div class="form-group">
                                                                                    <button id="del" onclick="delRow(`+filtercontenttabs.filtercontentrow[selected_filter_tabs].length+`)" type="button" class="btn btn-sm red" style="height: 27px; width: 38px">
                                                                                        <i class="fa fa-minus-circle text-white"></i>
                                                                                    </button>
                                                                                    <button id="add" onclick="addRow()" type="button" class="btn btn-sm green" style="height: 27px; width: 38px">
                                                                                        <i class="fa fa-plus text-white"></i>
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </div>`)
        getFilterContentTabs()
        // console.log(filtercontenttabs)
    }

    $('#delgroup').on('click', function() {
        filter_tabs.pop()
        filtercontenttabs.filtercontentgroup.pop()
        filtercontenttabs.filtercontentbody.pop()
        filtercontenttabs.filtercontentrow.pop()
        getFilterContentTabs()
    })

    $('#addgroup').on('click', function() {
        filter_tabs.push(false)

        filtercontenttabs.filtercontentgroup.push(`<input type="hidden" name="tabs[]" value="`+filtercontenttabs.filtercontentgroup.length+`">
                                                    <div style="margin:5px 5px 0 0;" id="filter-content-group-`+filtercontenttabs.filtercontentgroup.length+`" onclick="changeTabFilter(`+filtercontenttabs.filtercontentgroup.length+`)" class="filter-content-group formbtn btn default default">
                                                        <span class="text-dark">
                                                            <i class="fa fa-indent text-dark"></i> `+filtercontenttabs.filtercontentgroup.length+`
                                                        </span>
                                                    </div>`)

        filtercontenttabs.filtercontentbody.push(`<div style="display: none" id="filter-content-body-`+filtercontenttabs.filtercontentbody.length+`" class="filter-content-body formgroup col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="col-md-12 control-label">Group Caption</label>
                                                                <input name="groupcaption[]" id="groupcaption-`+filtercontenttabs.filtercontentbody.length+`" class="form-control input-sm" value="" placeholder="Group Caption">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="col-md-12 control-label">Group Caption Showed</label>
                                                                <select name="groupcaptionshow[]" id="groupcaptionshow-`+filtercontenttabs.filtercontentbody.length+`" class="form-control input-sm">
                                                                    <option value="1">True</option>
                                                                    <option value="0" selected>False</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="col-md-12 control-label">Group Dropdown Showed</label>
                                                                <select name="groupdropdown[]" id="groupdropdown-`+filtercontenttabs.filtercontentbody.length+`" class="form-control input-sm">
                                                                    <option value="-1" selected>Auto</option>
                                                                    <option value="0" selected>False</option>
                                                                    <option value="1">True</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="filter-content-row-`+filtercontenttabs.filtercontentbody.length+`">
                                                    </div>
                                                </div>`)
        filtercontenttabs.filtercontentrow.push(new Array())
        getFilterContentTabs()
    })

    function getFilterContentTabs() {
        //Set to HTML
        var html_filtercontentgroup = '';
        var html_filtercontentbody = '';
        var value = {
            groupcaption: new Array(),
            groupcaptionshow: new Array(),
            groupdropdown: new Array(),
            buttoncaption: new Array(),
            condition: new Array(),
            buttonactive: new Array(),
            classicon: new Array(),
        };

        //SET
        $.each(filtercontenttabs.filtercontentgroup, function(k, v) {
            value.buttoncaption[k] = new Array();
            value.condition[k] = new Array();
            value.buttonactive[k] = new Array();
            value.classicon[k] = new Array();

            if ($('#groupcaption-'+k).val() == undefined) { value.groupcaption.push('') } else { value.groupcaption.push($('#groupcaption-'+k).val()) }
            if ($('#groupcaptionshow-'+k).val() == undefined) { value.groupcaptionshow.push(0) } else { value.groupcaptionshow.push($('#groupcaptionshow-'+k).val()) }
            if ($('#groupdropdown-'+k).val() == undefined) { value.groupdropdown.push(-1) } else { value.groupdropdown.push($('#groupdropdown-'+k).val()) }
            
            $.each(filtercontenttabs.filtercontentrow[k], function(k2, v2) {
                if ($('#buttoncaption-'+k+'-'+k2).val() == undefined) { value.buttoncaption[k][k2] = '' } else { value.buttoncaption[k][k2] = $('#buttoncaption-'+k+'-'+k2).val() }
                if ($('#condition-'+k+'-'+k2).val() == undefined) { value.condition[k][k2] = '' } else { value.condition[k][k2] = $('#condition-'+k+'-'+k2).val() }
                if ($('#buttonactive-'+k+'-'+k2).val() == undefined) { value.buttonactive[k][k2] = 0 } else { value.buttonactive[k][k2] = $('#buttonactive-'+k+'-'+k2).val() }
                if ($('#classicon-'+k+'-'+k2).val() == undefined) { value.classicon[k][k2] = '' } else { value.classicon[k][k2] = $('#classicon-'+k+'-'+k2).val() }
            })

            html_filtercontentgroup += v
            html_filtercontentbody += filtercontenttabs.filtercontentbody[k]
        })

        $('#filtercontentgroup').html(html_filtercontentgroup);
        $('#filtercontentbody').html(html_filtercontentbody);

        $.each(filtercontenttabs.filtercontentbody, function(k, v) {
            if (filter_tabs[k] == true) {
                changeTabFilter(k)
            }
            $('#groupcaption-'+k).val(value.groupcaption[k])
            $('#groupcaptionshow-'+k).val(value.groupcaptionshow[k])
            $('#groupdropdown-'+k).val(value.groupdropdown[k])
        })

        $.each(filtercontenttabs.filtercontentrow, function(k, v) {
            $('#filter-content-row-'+k).html(v);

            $.each(v, function(k2, v2) {
                $('#buttoncaption-'+k+'-'+k2).val(value.buttoncaption[k][k2])
                $('#condition-'+k+'-'+k2).val(value.condition[k][k2])
                $('#buttonactive-'+k+'-'+k2).val(value.buttonactive[k][k2])
                $('#classicon-'+k+'-'+k2).val(value.classicon[k][k2])
            })
        })
        //End Set to HTML
        // console.log(value)
    }
    
    function changeTabFilter(key) {
        $('.filter-content-group').removeClass('purple').addClass('default')
        $('.filter-content-group span').removeClass('text-white').addClass('text-dark')
        $('.filter-content-group span i').removeClass('text-white').addClass('text-dark')
        $('#filter-content-group-'+key).removeClass('default').addClass('purple')
        $('#filter-content-group-'+key+' span').removeClass('text-dark').addClass('text-white')
        $('#filter-content-group-'+key+' span i').removeClass('text-dark').addClass('text-white')
        $('.filter-content-body').css('display', 'none')
        $('#filter-content-body-'+key).css('display', 'block')
        selected_filter_tabs = key;
        
        $.each(filter_tabs, function(k, v) {
            if (k == key) {
                filter_tabs[k] = true
            } else {
                filter_tabs[k] = false
            }
        })
    }

    function closeModalFilter() {
        $('#modal-filter').modal('hide')
    }

    function showModalFilter() {
        $('#modal-filter').modal('show')
    }

    function showModalCKEditor() {
        $('#modal-ckeditor').modal('show')
    }
    
    function getData() {
        // if (!JSON.parse(localStorage.getItem('maincms-'+values3))) {
            // localStorage.removeItem('maincms');
            $.ajax({
                type: "POST",
                header:{
                    'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                },
                url: "/get_data_tmd",
                dataType: "JSON",
                data:{
                    "_token":$('meta[name="csrf-token"]').attr('content'),
                    "table": values3
                },
                success: function(response) {
                    localStorage.setItem('maincms-'+values3, JSON.stringify(response))
                }
            })
        // }
        maincms = JSON.parse(localStorage.getItem('maincms-'+values3))
        if (maincms == null) {
            location.reload()
        }
    }
    
    function searchform() {
        $("#searchform").keyup(function(e) {
            if(e.keyCode == 13) {
                mytable.search($("#searchform").val()).draw('page');
            }
        })
    }

    function searchformdetail() {
        $("#searchformdetail").keyup(function(e) {
            if(e.keyCode == 13) {
                mytabledetail.search($("#searchformdetail").val()).draw('page');
            }
        })
    }

    function searchformlog() {
        $("#searchformlog").keyup(function(e) {
            if(e.keyCode == 13) {
                mytablelog.search($("#searchformlog").val()).draw('page');
            }
        })
    }

    function searchformcostcontrol() {
        $("#searchformcostcontrol").keyup(function(e) {
            if(e.keyCode == 13) {
                mytablecostcontrol.search($("#searchformcostcontrol").val()).draw('page');
            }
        })
    }

    function searchtable() {
        if (searchtablewidth == 30) {
            searchtablewidth = 200
        } else {
            searchtablewidth = 200
        }
        $("#searchform").animate({ 
            width: searchtablewidth+'px' 
        }, 100); 
    }

    function searchtabledetail() {
        if (searchtablewidth == 30) {
            searchtablewidth = 200
        } else {
            searchtablewidth = 200
        }
        $("#searchformdetail").animate({ 
            width: searchtablewidth+'px' 
        }, 100); 
    }

    function searchtablelog() {
        if (searchtablewidth == 30) {
            searchtablewidth = 200
        } else {
            searchtablewidth = 200
        }
        $("#searchformlog").animate({ 
            width: searchtablewidth+'px' 
        }, 100); 
    }

    function searchtablecostcontrol() {
        if (searchtablewidth == 30) {
            searchtablewidth = 200
        } else {
            searchtablewidth = 200
        }
        $("#searchformcostcontrol").animate({ 
            width: searchtablewidth+'px' 
        }, 100); 
    }

    // function valuepage() {
    //     $("#valuepage").keyup(function(e) {
    //         if(e.keyCode == 13) {
    //             alert('masuk')
    //             mytable.page(3).draw('page');
    //         }
    //     })
    // }

    function prevpage() {
        mytable.page('previous').draw('page');
    }

    function prevpagedetail() {
        mytabledetail.page('previous').draw('page');
    }

    function prevpagelog() {
        mytablelog.page('previous').draw('page');
    }

    function prevpagecostcontrol() {
        mytablecostcontrol.page('previous').draw('page');
    }

    function nextpage() {
        mytable.page('next').draw('page');
    }

    function nextpagedetail() {
        mytabledetail.page('next').draw('page');
    }

    function nextpagelog() {
        mytablelog.page('next').draw('page');
    }

    function nextpagecostcontrol() {
        mytablecostcontrol.page('next').draw('page');
    }

    function alengthmenu() {
        mytable.page.len($('#alengthmenu :selected').val()).draw();
    }

    function alengthmenudetail() {
        mytabledetail.page.len($('#alengthmenudetail :selected').val()).draw();
    }

    function alengthmenulog() {
        mytablelog.page.len($('#alengthmenulog :selected').val()).draw();
    }

    function alengthmenucostcontrol() {
        mytablecostcontrol.page.len($('#alengthmenucostcontrol :selected').val()).draw();
    }

    function getCmsTabDatatable() {
        var maincms_widgetgridfield = new Array();
        
        $.each(maincms['widgetgridfield'], function(k, v) {
            $.each(v, function(k2, v2) {
                maincms_widgetgridfield[v2.idcmswidgetgrid] = v;
            })
        })

        $.each(maincms['panel'], function(k, v) {
            if (k > 0) {
                // console.log('aku cek ini')
                // console.log(JSON.parse(localStorage.getItem('input-data-'+v.noid)))
                // return false
                // console.log('maincms_widgetgrid')
                // console.log(maincms_widgetgrid[v.noid])
                if (!maincms_widgetgrid[v.noid]['panelfilter']) {
                    $('#space_filter_button-'+v.noid).hide()
                    $('#tool_filter'+v.noid).hide()
                } else {
                    getFilterButton(values3, v.noid)
                }

                // console.log('idd_fix')
                // console.log(JSON.parse(localStorage.getItem('input-data-'+v.noid)))
                // return false
                console.log(localStorage.getItem('input-data-'+v.noid))
                $.each(maincms_widgetgridfield[v.idwidget], function(k2, v2) {
                    if (k2 == 0) {
                        fieldnametab[v.noid] = new Array();
                        tablelookuptab[v.noid] = new Array();
                    }
                        // alert(v3.idcmswidgetgrid+' '+v.idwidget)
                    if (v2.colshow == 1) {
                        if (v2.coltypefield == 96) {
                            fieldnametab[v.noid].push(v2.fieldname+'_lookup');
                        } else {
                            fieldnametab[v.noid].push(v2.fieldname);
                        }
                        tablelookuptab[v.noid].push(v2.tablelookup);
                    }
                })
                // console.log('cekkkk')
                // console.log(fieldnametab[v.noid])
                // tab_datatable.push(v.maintable);


                var columns = new Array();
                var columndefs = new Array();
                var index = 1;
                var noncolsorted = new Array();
                var noncolsearch = new Array();
                if (maincms['widgetgrid'][k]['colcheckbox']) {
                    // columndefs.push({"targets": [0,1,21],"orderable": false})
                    noncolsorted.push(0)
                    noncolsorted.push(1)
                    noncolsearch.push(0)
                    noncolsearch.push(1)
                    columns.push({"data": 0,"class": "text-left"})
                    columns.push({"data": 1,"class": "text-left"})
                    index = 2;
                } else {
                    noncolsorted.push(0)
                    noncolsearch.push(0)
                    columndefs.push({"targets": [0,20],"orderable": false})
                    columns.push({"data": 0,"class": "text-left"})
                }
                $.each(maincms['widgetgridfield'][k], function(k, v) {
                    if (k == 0) {
                        if (maincms['widgetgrid'][k]['colcheckbox']) {
                            columndefs.push({
                                "name": v.fieldsource,
                                "height": '60px',
                                "targets": index
                            })
                        }
                        columndefs.push({
                            "name": v.fieldsource,
                            "height": '60px',
                            "targets": index
                        })
                    }
                    if (v.colshow) {
                        if (v.colwidth != 0) {
                            var colwidth = v.colwidth+'px';
                        } else {
                            var colwidth = '100px';
                        }
                        if (!v.colsorted) {
                            noncolsorted.push(index)
                        }
                        if (!v.colsearch) {
                            noncolsearch.push(index)
                        }
                        columns.push({
                            "data": index,
                            "width": colwidth,
                            "class": "text-"+v.colalign.toLowerCase()
                        })
                        columndefs.push({
                            "name": v.fieldsource,
                            "height": '60px',
                            "targets": index
                        })
                        index++
                    }
                })

                noncolsorted.push(index)
                noncolsearch.push(index)
                columndefs.push({"targets": noncolsorted,"orderable": false})
                columndefs.push({"targets": noncolsearch,"searchable": false})
                columns.push({"data": index,"class": "text-left"})
                columndefs.push({"height": '60px',"targets": index})
                console.log('columndefstabs')
                console.log(columns)
                // console.log(maincms['widgetgrid'][0].recperpage)
                // console.log('columndefs')
                // console.log(columndefs)
                // return false

                // alert('#tab-table-'+v.noid)
                my_tab_table = $('#tab-table-'+v.noid).DataTable({
                    "autoWidth": false,
                    "pageLength": maincms['widgetgrid'][k].recperpage,
                    "searching": true,
                    "processing": true,
                    "serverSide": true,
                    "bInfo": true,
                    "bFilter": false,
                    "bLengthChange": false,
                    "columns": columns,
                    "columnDefs": columndefs,
                    // "dom": '<"top dtfiler"<"">>',
                    // "dom": '<"info1 top dtfiler"pli<""f><"clear">>rt ',
                    // "dom": '<lf<t>ip>',
                    "infoCallback": function( settings, start, end, max, total, pre ) {
                        var options = '';
                        $.each(settings.aLengthMenu, function(k, v) {
                            options += '<option value="'+v+'" id="alm'+v+'" class="alm">'+v+'</option>'
                        })
                        if ((end-start) == settings._iDisplayLength-1) {
                            var valuepage = end/settings._iDisplayLength;
                        } else {
                            var valuepage = ((start-1)/settings._iDisplayLength)+1;
                        }
                        if (start == 1) {
                            var prevdisabled = 'disabled'
                        } else {
                            var prevdisabled = '';
                        }
                        if (end == total) {
                            var nextdisabled = 'disabled'
                        } else {
                            var nextdisabled = '';
                        }
                        // if (settings.oPreviousSearch.sSearch == $('#searchform').val()) {
                        //     alert($('#searchform').val())
                        //     var valuesearch = $('#searchform').val()
                        // } else {
                        //     var valuesearch = $('#searchform').val()
                        //     alert('else'+$('#searchform').val())
                        // }
                        if (settings.oPreviousSearch.sSearch != '') {
                            tab_statussearch[v.noid] = true
                            tab_searchtablewidth[v.noid] = 200
                            tab_valuesearch[v.noid] = settings.oPreviousSearch.sSearch
                        } else {
                            tab_statussearch[v.noid] = false
                            tab_searchtablewidth[v.noid] = 30
                            tab_valuesearch[v.noid] = ''
                        }
                        $('#tab-appendInfoDatatable-'+v.noid).html(`
                        <div class="row" style="background: #90CAF9; padding-left: 15px; padding-right: 15px;">
                            <div class="col-md-12">
                                Page
                                <button id="prevpage" onclick="prevpage(`+v.noid+`)" class="btn btn-sm default prev `+prevdisabled+`" style="background-color: #2196F3;">
                                    <i class="fa fa-angle-left">
                                    </i>
                                </button>
                                <input type="text" id="valuepage" onkeyup="valuepage()" class="input-mini" value="`+valuepage+`" style="text-align: center; border: 0px; margin: 5px 5px 5px 5px">
                                <button id="nextpage" onclick="nextpage(`+v.noid+`)" class="btn btn-sm default next `+nextdisabled+`" style="background-color: #2196F3;">
                                    <i class="fa fa-angle-right">
                                    </i>
                                </button> of <span class="pagination-panel-total">`+(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)+`</span>
                                <span class="seperator">|</span>
                                Rec/Page 
                                <select id="alengthmenu-`+v.noid+`" onchange="alengthmenu(`+v.noid+`)" name="table_master_length" aria-controls="table_master" class="input-inline input-xsmall input-sm recofdt" style="border: 0px; margin: 5px 5px 5px 5px">
                                    `+options+`
                                </select>
                                <span class="seperator">|</span>Total `+settings._iRecordsTotal+` records
                                <input id="searchform-`+v.noid+`" onkeyup="searchform(`+v.noid+`)" value="`+tab_valuesearch[v.noid]+`" type="search" class="form-control input-inline input-sm clicked" placeholder="" aria-controls="table_master" style="width: `+tab_searchtablewidth[v.noid]+`px; position: absolute; right: 0px; top: 4px; height: 80%; border: 0;">
                                <div onclick="searchtable(`+v.noid+`)" class="btn fa fa-search text-right" style="position: absolute; right: 0px; top: 4px; height: 80%; background: transparent; margin: 0px 0px 0px 0px; padding: 5px 0px 5px 0px">
                                    <i class="fa fa-search"></i>
                                </div>
                            </div>
                        </div>`)
                        console.log('settings '+v.noid)
                        console.log(settings)
                        console.log(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)
                        $('.alm').removeAttr('selected');
                        $('#alm'+settings._iDisplayLength).attr('selected', 'selected');
                        // console.log(valuepage)
                        $('.dataTables_info').css('display', 'none')
                        $('.dataTables_paginate').css('display', 'none')
                        // return '<"top dtfiler"<"">>';
                    },
                    "ajax": {
                        "url": "/get_tab_data_tmd",
                        "type": "POST",
                        "dataType":'json',
                        "data": {
                            "_token":$('meta[name="csrf-token"]').attr('content'),
                            key: k-1,
                            id: v.idwidget,
                            table: maintable[v.idwidget],
                            selected_filter: selected_filter,
                            maincms: JSON.stringify(maincms),
                            data: localStorage.getItem('input-data-'+v.noid),
                            tablename: localStorage.getItem('tablename-'+v.noid),
                            panelnoid: v.noid,
                            masternoid: nownoid,
                            fieldnametab: fieldnametab[v.noid],
                            tablelookuptab: tablelookuptab[v.noid],
                            statusadd: statusadd,
                        },
                        beforeSend: function () {
                            $(".dataTables_scroll").LoadingOverlay("show");
                        },
                        complete: function (response) {
                            $(".dataTables_scroll").LoadingOverlay("hide");
                            
                            if (response.responseJSON.noids_tab[v.noid].length > 0) {
                                noids_tab[v.noid] = response.responseJSON.noids_tab[v.noid];
                                select_all_tab[v.noid] = response.responseJSON.select_all_tab[v.noid];
                            }
                            // alert('asd')
                            // $.each(maincms['widgetgridfield'][0], function(k, v) {
                            //     $('#example_baru th:nth-child('+k+')').attr('style', 'padding: 0px 5px 0px 5px; width: '+v.colwidth+'px;')
                            //     $('#example_baru td:nth-child('+k+')').attr('style', 'padding: 0px 5px 0px 5px; width: '+v.colwidth+'px;')
                            //     alert(document.querySelector('.dataTables_scrollHeadInner #thead-columns th:nth-child('+k+')').getAttribute('style'))
                            // })
                            // $('#example_baru .sorting_1').attr('style', 'width: 103px')
                        },
                        "error": function(res) {
                            errorHandler(res);
                        }
                    },
                    "pagingType": "input",
                    "createdRow": function(row, data, dataIndex) {
                        // console.log(data)
                        $(row).addClass('my_row');
                        $(row).addClass('row'+dataIndex);
                        
                        console.log('hwgf')
                        $(row).css('height', '35px');
                        $(row).attr('onmouseover', 'showButtonActionTab(this, '+dataIndex+')');
                        // alert(dataindex)
                        $(row).attr('onmouseout', 'resetCssTab()');
                        // $(row).attr('onmouseover', 'getPositionXY(this)');
                    },
                    "oLanguage":{
                        "loadingRecords": "Please wait - loading...",
                        "sProcessing": "<div id='loadernya'></div>",
                        "sInfo": "&nbsp;|&nbsp;Total&nbsp;_TOTAL_ records",
                        "sLengthMenu": "&nbsp;|&nbsp;Rec/Page&nbsp;_MENU_",
                        "sInfoFiltered": "",
                        "sInfoEmpty": "| No records found to show",
                        "sEmptyTable": "No data available in table",
                        "sZeroRecords": "No matching records found",
                        "sSearch": "<button onclick='toggleButtonSearch()' class='btn flat dtfilter_search_btn' style='background-color: white'><i class='fa fa-search'></i></button>",
                        // "oPaginate": {
                        //   "previous": "<i class='fa fa-angle-left'></i>",
                        //   "next": "<i class='fa fa-angle-right'></i>",
                        //   "page": "Page",
                        //   "pageOf": "of"
                        // },
                        "oPaginate": {
                            "sPrevious": "<i class='fa fa-angle-left'></i>",
                            "sNext": "<i class='fa fa-angle-right'></i> ",
                        // "sPage": "<i id='page_prev'>Page</i>",
                        // "sPageOf": "<span id='page_next'> of </span>"
                        }
                    },
                    footerCallback: function ( row, data, start, end, display ) {
                        var api = this.api(), data;
                        var intVal = function ( i ) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '')*1 :
                                typeof i === 'number' ?
                                    i : 0;
                        };
                        // var _split = function ( i ) {
                        //     return typeof i === 'string' ?
                        //         parseInt(i.replace('.', '')).toString() :
                        //         typeof i === 'number' ?
                        //             i : 0;
                        // };
                        var count = function ( i ) {
                            return api.column( 5 )
                                    .data()
                                    .reduce( function (a, b) {
                                        return intVal(end);
                                    }, 0 )
                        }
                        var sum = function ( i ) {
                            return i
                            var value = 0;
                            $.each(api.column( i ).data(), function(k, v) {
                                var _split = v.split('.');
                                var value2 = '';
                                $.each(_split, function(k2, v2) {
                                    value2 += v2
                                })
                                value += Number(value2)
                            })
                            return value
                        }
                        var addcommas = function addCommas(nStr) {
                            nStr += '';
                            var x = nStr.split('.');
                            var x1 = x[0];
                            var x2 = x.length > 1 ? '.' + x[1] : '';
                            var rgx = /(\d+)(\d{3})/;
                            while (rgx.test(x1)) {
                                x1 = x1.replace(rgx, '$1' + '.' + '$2');
                            }
                            return x1 + x2;
                        }

                        $.each(functionfootertab[v.noid], function(k, v) {
                            $( api.column(v.index).footer() ).html(api.column(v.index).data().reduce( function (a, b) { return addcommas(eval(v.function)) }, 0 ));
                        })
                        // console.log('try')
                        // console.log(row)
                        console.log('functionfootertab')
                        console.log(functionfootertab)
                    }
                });

                mytabtable[v.noid] = my_tab_table

                // my_tab_table = $('#tab-table-'+v.noid).DataTable({
                //     "scrollX": true,
                //     "processing": true,
                //     "serverSide": true,
                //     "info": false,
                //     // "order": [[ 0, "asc" ]],
                //     "columnDefs": [ 
                //         { 
                //             orderable: false,
                //             targets: colsorted
                //             // targets: [0,1,30]
                //         }
                //     ],
                //     "pagingType": "input",
                //     "searching": false,
                //     "ajax": {
                //         "url": "/get_tab_data_tmd",
                //         "type": "POST",
                //         "dataType":'json',
                //         "data": {
                //             "_token":$('meta[name="csrf-token"]').attr('content'),
                //             key: k-1,
                //             id: v.idwidget,
                //             table: 'fincashbankdetail',
                //             selected_filter: selected_filter,
                //             maincms: JSON.stringify(maincms),
                //             data: localStorage.getItem('input-data-'+v.noid),
                //             tablename: localStorage.getItem('tablename-'+v.noid),
                //             panelnoid: v.noid,
                //             masternoid: nownoid,
                //             fieldnametab: fieldnametab[v.noid],
                //             statusadd: statusadd,
                //         },
                //         beforeSend: function () {
                //             $(".dataTables_scroll").LoadingOverlay("show");
                //         },
                //         complete: function () {
                //             $(".dataTables_scroll").LoadingOverlay("hide");
                //         },
                //     },
                //     // "sPaginationType":"input",
                //     "dom": '<"top dtfiler"pli<""f><"clear">>rt ',
                //     "oLanguage":{
                //         "loadingRecords": "Please wait - loading...",
                //         "sProcessing": "<div id='loadernya'></div>",
                //         "sInfo": "&nbsp;|&nbsp;Total&nbsp;_TOTAL_ records",
                //         "sLengthMenu": "&nbsp;|&nbsp;Rec/Page&nbsp;_MENU_",
                //         "sInfoFiltered": "",
                //         "sInfoEmpty": "| No records found to show",
                //         "sEmptyTable": "No data available in table",
                //         "sZeroRecords": "No matching records found",
                //         "sSearch": `<button onclick="toggleButtonSearchTab('`+v.noid+`')" class="btn flat dtfilter_search_btn" style="background-color: white"><i class="fa fa-search"></i></button>`,
                //         // "oPaginate": {
                //         //   "previous": "<i class='fa fa-angle-left'></i>",
                //         //   "next": "<i class='fa fa-angle-right'></i>",
                //         //   "page": "Page",
                //         //   "pageOf": "of"
                //         // },
                //         "oPaginate": {
                //             "sPrevious": "<i class='fa fa-angle-left'></i>",
                //             "sNext": "<i class='fa fa-angle-right'></i> ",
                //         // "sPage": "<i id='page_prev'>Page</i>",
                //         // "sPageOf": "<span id='page_next'> of </span>"
                //         }
                //     },
                //     "createdRow": function(row, data, dataIndex) {
                //         // console.log(data)
                //         $(row).addClass('my_row');
                //         $(row).addClass('row'+dataIndex);
                //         // $(row).css('height', '5px');
                //         $(row).attr('onmouseover', 'showButtonActionTab(this, '+dataIndex+')');
                //         $(row).attr('onmouseout', 'resetCssTab()');
                //         // $(row).attr('onmouseover', 'getPositionXY(this)');
                //     },
                //     deferRender: true,
                //     // footerCallback: function ( row, data, start, end, display ) {
                //     //     var api = this.api(), data;
                //     //     var intVal = function ( i ) {
                //     //         return typeof i === 'string' ?
                //     //             i.replace(/[\$,]/g, '')*1 :
                //     //             typeof i === 'number' ?
                //     //                 i : 0;
                //     //     };
                //     //     var monTotal = api.column( 1 )
                //     //         .data()
                //     //         .reduce( function (a, b) {
                //     //             return intVal(a) + intVal(b);
                //     //         }, 0 );

                //     //     $( api.column( 3 ).footer() ).html(monTotal);
                //     // }
                // });
                
                new $.fn.dataTable.Buttons( my_tab_table, {
                    buttons: [
                        {
                            autoFilter: true,
                            text:      '<i class="fa fa-trash"></i> Delete Selected',
                            titleAttr: 'Delete Selected',
                            className: ' dropdown-item flat',
                            action:  function(e, dt, button, config) {
                                if (another_selecteds[v.noid].length == 0) {
                                    alert("There's no selected")
                                    // console.log(another_selecteds)
                                } else {
                                        // alert(another_selecteds)
                                        // alert(v.maintable)
                                    if (confirm('Are you sure you want to delete this?')) {
                                        removeSelectedTab(another_selecteds[v.noid])
                                    }
                                }
                            },
                        },{
                            autoFilter: true,
                            text:      '<i class="fa fa-trash"></i> Delete All',
                            titleAttr: 'Delete All',
                            className: ' dropdown-item flat',
                            action:  function(e, dt, button, config) {
                                if (noids_tab[v.noid].length == 0) {
                                    alert("There's no selected")
                                } else {
                                    if (confirm('Are you sure you want to delete this?')) {
                                        removeSelectedTab(noids_tab[v.noid])
                                    }
                                }
                            },
                        },{
                            extend:    'excel',
                            autoFilter: true,
                            text:      '<i class="fa fa-file-excel"></i> Export to Excel',
                            titleAttr: 'Export to Excel',
                            title: v.widgetcaption,
                            className: ' dropdown-item flat',
                            exportOptions: {
                                modifier: {
                                    search: 'applied',
                                    order: 'applied'
                                },
                                columns: [ 1,2,3, ':visible' ]
                                //                  <a style="font-size: 14px;min-width: 175px;
                                // text-align: left;" href="javascript:;"  class="dropdown-item">
                                // <i style="margin-top:10px;margin-right: 5px;" class="fa fa-file-excel"></i> Export to Excel </a>
                                                // columns: [ 0, ':visible' ]
                            },
                            action:  function(e, dt, button, config) {
                            // $('.loading').fadeIn();
                                var that = this;
                                setTimeout(function () {
                                    $.fn.dataTable.ext.buttons.excelHtml5.action.call(that,e, dt, button, config);
                                    // console.log(that);
                                    // console.log(e);
                                    // console.log(dt);
                                    // console.log(button);
                                    // console.log(config);
                                    $('.loading').fadeOut();
                                },500);
                            },
                        }
                    ]
                    // infoDatatable()
                });
                // infoDatatable()
                $('#tab-table-'+v.noid+'_first').hide();
                $('#tab-table-'+v.noid+'_last').hide();
                
                my_tab_table.buttons().container().appendTo('#another-space-button-export-'+v.noid);

                getForm(statusadd, v.idwidget, 'tab-form-div-'+v.noid);

                // $('#'+element_id).LoadingOverlay("hide");
                $('#another-card').show();
            }
        })

        if (!statusadd) {
            // $('#another-card').show();
        }
    }

    function destroyDatatable() {
        // $('#example_baru').DataTable().destroy();
        $('#table_master').DataTable().destroy();
    }

    function destroyTabDatatable() {
        $.each(maincms['panel'], function(k, v) {
            if (k > 0) {
                $('#tab-table-'+v.noid).DataTable().destroy();
            }
        })
    }
// 

    function toggleButtonCollapse(noid) {
        if (noid) {
            $('#tab-widget-content-'+noid+' .card .card-body').toggleClass('collapse');
        } else {
            $('#kt_content .container .card .card-body').toggleClass('collapse');
        }
        // $('#kt_content .container .card .card-body').toggleClass('collapse').delay('slow').fadeOut();
    }

    function toggleButtonSearch() {
        $('#example_baru_filter label input').addClass('form-control flat')
        $('#example_baru_filter label input').toggleClass('clicked');
    }

    function toggleButtonSearchTab(element_id) {
        $('#tab-table-'+element_id+'_filter label input').addClass('form-control flat')
        $('#tab-table-'+element_id+'_filter label input').toggleClass('clicked');
    }

    function filterData(element_id, element_class, key, buttoncaption, classcolor, groupdropdown, is_all, table='', operand='', fieldname='', operator='', value='') {
        if (is_all) {
            selectedfilter[fieldname] = null;
        } else {
            selectedfilter[fieldname] = value;
        }

        if (groupdropdown == 1) {
            $('#filter-dropdown-active-'+key).text(buttoncaption)
            // alert(buttoncaption)
        } else {
            $('.'+element_class).css('background', '#E3F2FD');
            $('.'+element_class).css('color', classcolor);
            $('.'+element_class).css('font-weight', 'normal');
            $('#'+element_id).css('background', '#90CAF9');
            $('#'+element_id).css('color', 'black');
            $('#'+element_id).css('font-weight', 'bold');
        }
        
        getDatatable();
    }

    function getSlider(id) {
        var owl = $('.owl-carousel').owlCarousel({
            loop:true,
            smartSpeed: 100,
            autoplay: true,
            autoplaySpeed: 100,
            mouseDrag: false,
            margin:10,
            animateIn: 'slideInUp',
            animateOut: 'fadeOut',
            nav:false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });

        $.ajax({
            type: "POST",
            header:{
            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            },
            url: "/get_slide",
            dataType: "json",
            data:{
                "_token":$('meta[name="csrf-token"]').attr('content'),
                "id_slider": id
            },
            success: function (response) {
                // console.log(response.hasil);
                var base_url = '{{ url('/') }}';
                if(isNaN(base_url)) {
                    base_url = '';
                }
                var name1 = '';
                var name2 = '';

                $.each(response.hasil, function(i, item) {
                    // console.log(item.mainimage);
                    name1 = 'public/'+item.mainimage;

                    // console.log(name1);
                    var random_num =  Math.floor(Math.random()*60);
                    owl.trigger('add.owl.carousel', [jQuery('<div class="notification-message"> <img src="'+base_url+name1+'" class="user-image" alt=""> <div class="user-name">'+ item.newstitle+' <span class="lighter">from '+item.idnews+'</span></div> <div class="bought-details">Bought This <br>'+random_num+' minutes ago</div> </div>')]);
                });
                owl.trigger('refresh.owl.carousel');
            },
        });
    }

    function chartBatang(response,id_cart) {
        var dataatas = response.hasil;
        var dataProvideratas = [];// this variable you have to pass in dataProvider inside chart
        for(var key in dataatas) {
            dataProvideratas.push({
                category: dataatas[key]['categories'],
                visits: dataatas[key]['jumlahdata'],
            });
        }
        var chart = AmCharts.makeChart("kt_amcharts_"+id_cart, {
            "rtl": KTUtil.isRTL(),
            "type": "serial",
            "hideCredits":true,
            "theme": "light",
            "dataProvider": dataProvideratas,
            "valueAxes": [{
                "gridColor": "#FFFFFF",
                "gridAlpha": 0.2,
                "dashLength": 0
            }],
            "series": [{
                "type": "LineSeries",
                "stroke": "#ff0000",
                "strokeWidth": 3
            }],
            "valueAxes": [ {
                "position": "left",
                "title": "Jumlah Data"
            }, {
                "position": "bottom",
                "title": response.widget.widget_widgetcaption
            } ],
            "legend": {
                "useGraphSettings": true
            },
            "titles": [
                {
                    "size": 15,
                    "text":response.widget.widget_widgetcaption
                },
                {
                    "text": response.widget.widget_widgetsubcaption,
                    "bold": false
                }
            ],
            "gridAboveGraphs": true,
            "startDuration": 1,
            "graphs": [{
                "balloonText": "[[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "visits",
                "title": response.widget.widget_widgetcaption
            }],
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "category",
            "categoryAxis": {
                "gridPosition": "start",
            },
            "export": {
                "enabled": true
            }
        });
    }

    function chartPie(response,id_cart) {
        var data = response.hasil;
        var dataProvider = [];// this variable you have to pass in dataProvider inside chart
        for(var key in data) {
            dataProvider.push({
            country: data[key]['messagestatus'],
            litres: data[key]['jumlahdata'],
            });
        }

        var chart = AmCharts.makeChart("kt_amcharts_"+id_cart,{
            "type"    : "pie",
            "hideCredits"    : true,
            "theme"    : "light",
            "addClassNames": true,
            "titles": [
                {
                    "size": 15,
                    "text": response.widget.widget_widgetcaption
                },
                {
                    "text": response.widget.widget_widgetsubcaption,
                    "bold": false
                }
            ],
            // "pullOutDuration": 0,
            // "pullOutRadius": 0,

            "labelsEnabled": false,
            "titleField"  : "country",
            "valueField"  : "litres",
            "dataProvider"  : dataProvider,
            "numberFormatter": {
                "precision": -1,
                "decimalSeparator": ",",
                "thousandsSeparator": ""
            },
            "legend": {
                "align": "center",
                "position": "bottom",
                "marginRight": 0,
                "labelText": "[[title]]",
                "valueText": "",
                "valueWidth": 50,
                "textClickEnabled": true
            },
            "export": {
                "enabled": true
            }

        });
    }

    function setCheckbox(element_id) {
        if ($('#'+element_id).attr('checked')) {
            $('#'+element_id).val(0);
        } else {
            $('#'+element_id).val(1);
        }
    }

    $("#savedata__").click(function(e){
        e.preventDefault();
        // alert(nextnoid)
        // return false
        // localStorage.setItem('input-data', response)
        // console.log($("#formdepartement").serializeArray())
        var input_data_master = {};
        var mstrtbl = maincms['widget'][0].maintable;
        var onrequired = false;
        var errormessage = '';
        $.each($("#formdepartement").serializeArray(), function(k, v) {
            var formcaption = $('#'+mstrtbl+'-'+v.name).attr('data-'+mstrtbl+'-formcaption');
            if (v.name == 'noid') {
                input_data_master[v.name] = nextnoid
            } else {
                input_data_master[v.name] = v.value

                if (statusadd) {
                    if ($('#'+mstrtbl+'-'+v.name).attr('data-'+mstrtbl+'-newreq') == 1) {
                        if (v.value == '') {
                            onrequired = true;
                            errormessage += formcaption+' is required!<br>'
                        }
                    }
                } else {
                    if ($('#'+mstrtbl+'-'+v.name).attr('data-'+mstrtbl+'-editreq') == 1) {
                        if (v.value == '') {
                            onrequired = true;
                            errormessage += formcaption+' is required!<br>'
                        }
                    }
                }
            }
        })
        
        if (onrequired) {
            $.notify({
                message: errormessage
            },{
                type: 'danger'
            });
            return false;
        }


        if (!statusadd) {
            input_data_master['noid'] = nownoid;
        }
        
        localStorage.setItem('input-data', JSON.stringify(input_data_master))

        // var maincms_widget = new Array();
        // $.each(maincms['widget'], function(k, v) {
        //     maincms_widget[v.noid] = v;
        // })

        var input_data_detail = {};
        $.each(maincms['panel'], function(k, v) {
            if (k > 0) {
                input_data_detail[v.noid] = {
                    'data': JSON.parse(localStorage.getItem('input-data-'+v.noid)),
                    'table': maincms_widget[v.idwidget].maintable
                }
            }
        })
        // console.log(JSON.stringify(maincms))
        // console.log(input_data_detail)
        // return false
        
        /////////////////////////////////

        var kode = $("#kode").val();
        var nama = $("#nama").val();
        var namatable =$('#namatable').val();
        var formdata = $("#formdepartement").serialize();
        var formdatatab = [];
        
        if (statusadd) {
            $.each(maincms['widget'], function(k, v) {
                if (k > 0) {
                    formdatatab.push($('#form-'+v.maintable).serialize())
                }
            })
        }
        // console.log('input_data_detail')
        // console.log(input_data_detail)
        // return false

        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            },
            url: "/save_tmd",
            dataType: "json",
            data:{
                "_token":$('meta[name="csrf-token"]').attr('content'),
                "data" :{
                    'data' : formdata,
                    'datatab' : formdatatab,
                    'tabel': namatable,
                },
                "table": namatable,
                "editid": editid,
                "statusadd": statusadd,
                "masternoid": nownoid,
                maincms: JSON.stringify(maincms),
                "input_data": {
                    "master": input_data_master,
                    "detail": JSON.stringify(input_data_detail),
                }
            },
            success:function(result){
                // return false
                if (result.status == 'add') {
                    // $("#notiftext").text('Data  Berhasil di Tambah');
                    $.notify({
                        message: 'Add data has been succesfullly.' 
                    },{
                        type: 'success'
                    });
                } else {
                    // $("#notiftext").text('Data  Berhasil di Edit');
                    $.notify({
                        message: 'Edit data has been succesfullly.' 
                    },{
                        type: 'success'
                    });
                }

                //
                $('#another-card').hide();
                divform = '';
                another_selecteds = [];
                destroyTabDatatable()

                document.getElementById("formdepartement").reset();
                resetData()
                refresh();
                // $('#divsucces').show(0).delay(5000).hide(0);
                var x = document.getElementById("from-departement");
                var x2 = document.getElementById("tabledepartemen");
                var y = document.getElementById("tool-kedua");
                var y2 = document.getElementById("tool-pertama");
                x.style.display = "none";
                x2.style.display = "block";
                y.style.display = "none";
                y2.style.display = "block";
            }
        });
    });

    function refresh(noid) {
        if (noid) {
            $('#tab-table-'+noid).DataTable().ajax.reload();
        } else {
            $('#table_master').DataTable().ajax.reload();
        }
    }

    function refreshTab() {
        destroyTabDatatable()
        getCmsTabDatatable()
    }

    function changeInput() {
        // console.log($('#formdepartement').serialize())
    }

    function getNextNoid() {
        $.ajax({
            url: '/get_next_noid_tmd',
            type: "POST",
            dataType: 'json',
            header:{
                'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            },
            data: {
                "table" : maincms['widget'][0]['maintable'],
                "_token": $('meta[name="csrf-token"]').attr('content'),
                maincms: JSON.stringify(maincms)
            },
            success: function (response) {
                nextnoid = response
                localStorage.setItem('input-data-noid', response)
                // console.log(localStorage.getItem('input-data-noid'))
                // alert(nextnoid)
            }
        })
    }

    function getNextNoidTab(tablename, panel_noid) {
        $.ajax({
            url: '/get_next_noid_tmd',
            type: "POST",
            dataType: 'json',
            header:{
                'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            },
            data: {
                "table" : tablename,
                "_token": $('meta[name="csrf-token"]').attr('content'),
                maincms: JSON.stringify(maincms)
            },
            success: function (response) {
                nextnoidtab[panel_noid] = response
                // console.log(nextnoidtab[panel_noid])
            }
        })
    }

    function cancelDataTab(panel_noid) {
        $('#form-detail_').hide();

        $('#tab-form-'+panel_noid).hide()
        $('#add-data-'+panel_noid).show()
        $('#save-data-'+panel_noid).hide()
        $('#cancel-data-'+panel_noid).hide()
        $('#tab-table-'+panel_noid+'_wrapper').show()
        $('#tab-appendInfoDatatable-'+panel_noid).show()
        $('#ts-'+panel_noid).show()
        $('#space_filter_button-'+panel_noid).show();
        $('#tool_refresh-'+panel_noid).show();
        $('#tool_filter-'+panel_noid).show()
        $('#tool_refresh_up-'+panel_noid).show();
    }

    function addDataTab(panel_noid) {
        console.log(nextnoidtab)
        resetDataTab(panel_noid)
        statusaddtab[panel_noid] = true;

        $('#form-detail_').show();
        $('#tab-form-'+panel_noid).show()
        $('#add-data-'+panel_noid).hide()
        $('#save-data-'+panel_noid).show()
        $('#cancel-data-'+panel_noid).show()
        $('#tab-table-'+panel_noid+'_wrapper').hide()
        $('#tab-appendInfoDatatable-'+panel_noid).hide()
        $('#ts-'+panel_noid).hide()
        $('#space_filter_button-'+panel_noid).hide();
        $('#tool_refresh-'+panel_noid).hide();
        $('#tool_filter-'+panel_noid).hide()
        $('#tool_refresh_up-'+panel_noid).hide();
    }

    function unFocus() {
        $('.select2-container--focus').removeClass('select2-container--focus');
        $(':focus').blur();
    }

    function addData() {
        status.crudmaster = 'create';
        status.cruddetail = 'read';
        statusadd = true;
        statusdetail = false;

        showLoading(); // Show loading
        setTab('tab-general', 'Header') // Set default tab
        conditionDetail(); // Check konsisi detail
        localStorage.removeItem(main.tabledetail); // Remove detail

        getInfo().done(() => {
            getGenerateCode();
            unFocus();
            $('#form-header #idtypecostcontrol').select2('focus');
            getDatatableDetail(localStorage.getItem(main.tabledetail))
            customDocumentForm();

            $('#tools-dropdown').hide(); // Hide button tools
            $('#tabledepartemen').hide(); // Hide datatable
            $('#tool-pertama').hide(); // Show buton add data
            $('#space_filter_button').hide(); // Hide filter di datatable
            $('#tool_refresh').hide(); // Hide button refresh
            $('#from-departement').show(); // Show form
            $('#tool-kedua').show(); // Show button cancel

            hideLoading();
        })
        
    }

    //DOCUMENT
    // $('#pagepanel').keyup(function(e) {
    //     if (e.keyCode === 13) {
    //         alert('asd');
    //         localStorage.setItem(main.table+'-fullscreen', 'false');
    //     }
    // });

    //FOCUS 
    $('#form-header #kode').on('keydown',function(e){if(e.keyCode==13){$('#form-header #kodereff').focus();}})
    // $('#form-header #kodereff').on('keydown',function(e){if(e.keyCode==13){$('#form-header #tanggal').focus();}})
    $('#form-header #tanggal').on('change',function(e){
        changeTanggal('tanggal', 'tanggal-modal')
        $('#form-header #tanggaldue').focus()
    })
    $('#form-header #tanggaldue').on('change',function(e){
        let _tanggal = new Date($('#form-header #tanggal').val());
        let _tanggaldue = new Date($('#form-header #tanggaldue').val());

        if (_tanggal > _tanggaldue) {
            alert("'Tanggal Due' harus lebih besar dari 'Tanggal'");
            $('#form-header #tanggaldue').val('')
            return false
        }

        changeTanggal('tanggaldue', 'tanggaldue-modal')
        // $('#form-header #idtypecostcontrol').select2('focus').trigger('change')
        thenTanggalDue(ready ? true : false)
    })
    // $('#form-header #idtypecostcontrol').on('select2:close',function(e){$('#form-header #projectname').focus()})
    $('#form-header #projectname').on('keypress',function(e){if(e.keyCode==13){$('#form-header #projectlocation').focus();}})
    $('#form-header #projectlocation').on('keypress',function(e){if(e.keyCode==13){$('#form-header #projectvalue').focus();}})
    $('#form-header #projectvalue').on('keypress',function(e){if(e.keyCode==13){$('#form-header #projecttype').select2('focus');}})
    $('#form-header #projecttype').on('select2:close',function(e){$('#form-header #idcardcustomer').select2('focus')})
    $('#form-header #idcardcustomer').on('select2:close',function(e){$('#form-header #idcardkoor').select2('focus')})
    // $('#form-header #idcardsales').on('select2:close',function(e){$('#form-header #idcardkoor').select2('focus')})
    // $('#form-header #idgudang').on('select2:close',function(e){$('#form-header #idcardpjkoor').select2('focus')})
    $('#form-header #idcardkoor').on('select2:close',function(e){$('#form-header #idcardpj').select2('focus')})
    $('#form-header #idcardpj').on('select2:close',function(e){$('#form-header #idcardsitekoor').select2('focus')})
    $('#form-header #idcardsitekoor').on('select2:close',function(e){$('#form-header #idcardsponsor').select2('focus')})
    // $('#form-header #idcarddrafter').on('select2:close',function(e){$('#searchprev').focus()})
    $('#form-header #idcardsponsor').on('select2:close',function(e){$('#searchprev').focus()})

    // $('#form-internal #idcashbankbayar').on('select2:close',function(e){$('#form-internal #idakunpersediaan').select2('focus')})
    // $('#form-internal #idakunpersediaan').on('select2:close',function(e){$('#form-internal #idakunpurchase').select2('focus')})
    // $('#form-internal #idakunpurchase').on('select2:close',function(e){$('#form-detail #idinventor').select2('focus')})

    function thenIDCardSales(ready) {
        ready ? $('#form-header #idcardsales').select2('focus') : $('#form-header #idcardsales').select2('close');
    }

    function thenIDCardCashier(ready) {
        ready ? $('#form-header #idcardcashier').select2('focus') : $('#form-header #idcardcashier').select2('close');
    }

    function thenIDCardCustomer(ready) {
        ready ? $('#form-header #idcardcustomer').select2('focus') : $('#form-header #idcardcustomer').select2('close');
    }

    function thenIDCompany(ready) {
        ready ? $('#form-header #idcompany').select2('focus') : $('#form-header #idcompany').select2('close');
    }

    function thenIDDepartment(ready) {
        ready ? $('#form-header #idgudang').select2('focus') : $('#form-header #idgudang').select2('close');
    }
    
    function thenIDCashbankBayar(ready) {
        ready ? $('#form-header #idakunpersediaan').select2('focus') : $('#form-header #idakunpersediaan').select2('close');
    }

    function thenTanggalDue(ready) {
        // ready ? showModalCostControl() : hideModalCostControl();
        // $('#form-header #idtypecostcontrol').select2('focus')
        $('#form-header #projectname').focus()
    }

    $('#form-header #kodecostcontrol').on('keydown',function(e){if(e.keyCode==13){$('#form-header #cost-control').focus();}})

    $('#tab-internal').on('click',function(e){
        if (!$('#form-internal #idcashbankbayar').val()) {
            $('#form-internal #idcashbankbayar').select2('focus')
        } else {
            if (!$('#form-internal #idakunpersediaan').val()) {
                $('#form-internal #idakunpersediaan').select2('focus')
            } else {
                if (!$('#form-internal #idakunpurchase').val()) {
                    $('#form-internal #idakunpurchase').select2('focus')
                }
            }
        }
    })

    $('#searchprev').on('keydown',function(e){if(e.keyCode==13){
        showModalDetailPrev();
    }})

    $('#form-detail #idinventor').on("select2:close", function(e) {
        changeKodeInventor();
        $('#form-detail #namainventor').focus();
    });
    $('#form-detail #namainventor').on('keydown',function(e){if(e.keyCode==13){$('#form-detail #unitprice').focus();}})
    $('#form-detail #unitprice').on('keydown',function(e){if(e.keyCode==13){$('#form-detail #unitqty').focus();}})
    $('#form-detail #unitqty').on('keypress',function(e){if(e.keyCode==13){$('#form-detail #keterangandetail').focus();}})
    $('#form-detail #keterangandetail').on('keypress',function(e){if(e.keyCode==13){
        if ($('#form-detail #unitprice').val() || $('#form-detail #unitqty').val()) {
            $('#form-detail #save-detail').focus();
            saveDetail();
            $('#form-detail #idinventor').select2('focus');
        } else {
            $.notify({
                message: 'Complete data, please!'
            },{
                type: 'danger'
            });
        }
    }})
    $('#form-detail #save-detail').on('click',function(e){if(e.keyCode==13){$('#form-detail #idinventor').select2('focus');}})
    $('#form-modal-detail #idsatuan-modal').on('change',function(e){
        let konversi = $('#form-modal-detail #idsatuan-modal :selected').attr('data-invmsatuan-konversi')
        $('#form-modal-detail #konvsatuan-modal').val(konversi)
    })
    $('#form-modal-detail #idtax-modal').on('change',function(e){
        let prosentax = $('#form-modal-detail #idtax-modal :selected').attr('data-accmpajak-prosentax')
        sumTotal();
    })

    //MODAL DETAIL
    let midinventor = $('#form-modal-detail #idinventor-modal');
    let mnamainventor = $('#form-modal-detail #namainventor-modal');
    let mketerangan = $('#form-modal-detail #keterangan-modal');
    let midcompany = $('#form-modal-detail #idcompany-modal');
    let middepartment = $('#form-modal-detail #iddepartment-modal');
    let midgudang = $('#form-modal-detail #idgudang-modal');
    let midsatuan = $('#form-modal-detail #idsatuan-modal');
    let mkonvsatuan = $('#form-modal-detail #konvsatuan-modal');
    let munitqty = $('#form-modal-detail #unitqty-modal');
    let munitqtysisa = $('#form-modal-detail #unitqtysisa-modal');
    let munitprice = $('#form-modal-detail #unitprice-modal');
    let msubtotal = $('#form-modal-detail #subtotal-modal');
    let mdiscountvar = $('#form-modal-detail #discountvar-modal');
    let mdiscount = $('#form-modal-detail #discount-modal');
    let mnilaidisc = $('#form-modal-detail #nilaidisc-modal');
    let midtypetax = $('#form-modal-detail #idtypetax-modal');
    let midtax = $('#form-modal-detail #idtax-modal');
    let mprosentax = $('#form-modal-detail #prosentax-modal');
    let mnilaitax = $('#form-modal-detail #nilaitax-modal');
    let mhargatotal = $('#form-modal-detail #hargatotal-modal');
    let mupdatedetail = $('#form-modal-detail #update-detail');


    midinventor.on('select2:close', function(e){
        sumTotal();
        midsatuan.val($('#form-modal-detail #idinventor-modal :selected').attr('data-invminventory-idsatuan')).trigger('change');
        munitprice.val($('#form-modal-detail #idinventor-modal :selected').attr('data-invminventory-purchaseprice'))
        mnamainventor.focus();
    });

    mnamainventor.on('keypress', function(e){
        if (e.keyCode == 13) {
            mketerangan.focus();
        }
    });

    midcompany.on('select2:close', function(e){
        middepartment.select2('focus');
    });

    middepartment.on('select2:close', function(e){
        midgudang.select2('focus');
    });

    midgudang.on('select2:close', function(e){
        midsatuan.select2('focus');
    });

    midsatuan.on('select2:close', function(e){
        sumTotal();
        munitqty.focus();
    });

    // mkonvsatuan.on('keyup', function(e){
    //     sumTotal();
    //     if (e.keyCode == 13) {
    //         munitqty.focus();
    //     }
    // });

    munitqty.on('keyup', function(e){
        sumTotal();
        if (e.keyCode == 13) {
            munitprice.focus();
        }
    });
    
    // munitqtysisa.on('keyup', function(e){
    //     sumTotal();
    //     if (e.keyCode == 13) {
    //         munitprice.focus();
    //     }
    // });

    munitprice.on('keyup', function(e){
        sumTotal();
        if (e.keyCode == 13) {
            mdiscount.focus();
        }
    });

    // mdiscountvar.on('keyup', function(e){
    //     sumTotal();
    //     if (e.keyCode == 13) {
    //         mdiscount.focus();
    //     }
    // });
    
    mdiscount.on('keyup', function(e){
        sumTotal();
        if (e.keyCode == 13) {
            midtax.select2('focus');
        }
    });

    // mnilaidisc.on('keyup', function(e){
    //     sumTotal();
    //     if (e.keyCode == 13) {
    //         midtypetax.focus();
    //     }
    // });

    // midtypetax.on('keyup', function(e){
    //     sumTotal();
    //     if (e.keyCode == 13) {
    //         midtax.focus();
    //     }
    // });

    midtax.on('select2:close', function(e){
        sumTotal();
        mupdatedetail.focus();
    });

    // mprosentax.on('keyup', function(e){
    //     sumTotal();
    //     if (e.keyCode == 13) {
    //         mnilaitax.focus();
    //     }
    // });

    // mnilaitax.on('keyup', function(e){
    //     sumTotal();
    //     if (e.keyCode == 13) {
    //         mupdatedetail.focus();
    //     }
    // });

    function sumTotal() {
        if ($('[name=searchtypeprev]:checked').val() == 1) {
            let unitqty = $('#form-modal-detail #unitqty-modal');
            let unitprice = $('#form-modal-detail #unitprice-modal');
            let subtotal = $('#form-modal-detail #subtotal-modal');
            let discount = $('#form-modal-detail #discount-modal');
            let nilaidisc = $('#form-modal-detail #nilaidisc-modal');
            let prosentax = $('#form-modal-detail #prosentax-modal');
            let _prosentax = $('#form-modal-detail #idtax-modal :selected').attr('data-accmpajak-prosentax');
            let nilaitax = $('#form-modal-detail #nilaitax-modal');
            let hargatotal = $('#form-modal-detail #hargatotal-modal');
            let resultsubtotal = parseInt(unitqty.val().replace(/[^0-9]/gi, ''))*parseInt(unitprice.val().replace(/[^0-9]/gi, ''));
            let resultnilaidisc = parseInt(resultsubtotal)*parseInt(discount.val().replace(/[^0-9]/gi, ''))/100;
            let resultprosentax = parseInt(_prosentax);
            let resultnilaitax = parseInt(resultsubtotal)*parseInt(_prosentax.replace(/[^0-9]/gi, ''))/100;
            let resulthargatotal = resultsubtotal-resultnilaidisc+resultnilaitax;

            subtotal.val(resultsubtotal);
            nilaidisc.val(resultnilaidisc);
            prosentax.val(resultprosentax);
            nilaitax.val(resultnilaitax);
            hargatotal.val(resulthargatotal);
        }
    }

    function getGenerateCode() {
        $.ajax({
            type: 'POST',
            header:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/generate_code'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'idtypecostcontrol': $('#form-header #idtypecostcontrol').val(),
            },
            success: function(response) {
                $('#form-header #nourut').val(response.data.nourut);
                $('#form-header #kode').val(response.data.kode);
            }
        });
    }

    function getInfo() {
        return $.ajax({
            type: 'POST',
            header:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/get_info'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
            },
            success: function(response) {
                let pr = response.data.pr;
                let mttts = response.data.mttts;

                $('#btn-saveto').html(getButtonSaveTo(pr,mttts,false));

                $('#idstatustranc').val(response.data.mtypetranctypestatus.noid);
                $('#idstatustranc-info').css('background-color', response.data.mtypetranctypestatus.classcolor);
                $('.idstatustranc-info').text(response.data.mtypetranctypestatus.nama);
                $('#idstatuscard').val(response.data.mcarduser.noid);
                $('#idstatuscard-info').text(response.data.mcarduser.nama);
                $('#dostatus').val(response.data.dostatus);
                $('#dostatus-info').text(response.data.dostatus);
                $('#form-header #idtypecostcontrol').val(response.data.idtypecostcontrol).trigger('change');
                $('#form-header #idcardsales').val(response.data.idcardsales).trigger('change');
                $('#form-header #idcardcashier').val(response.data.idcardcashier).trigger('change');
                $('#form-header #idcardcustomer').val(response.data.idcardcustomer).trigger('change');
            }
        })
    }

    function getLastSupplier() {
        // $.ajax({
        //     type: 'POST',
        //     header:{
        //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //     },
        //     url: '/find_last_supplier'+main.urlcode,
        //     dataType: 'json',
        //     data: {
        //         '_token': $('meta[name="csrf-token"]').attr('content'),
        //     },
        //     success: function(response) {
        //         $('#form-header #idcardsupplier').val(response.data.idcardsupplier).trigger('change');
        //         $('#form-modal-supplier #idakunsupplier-modal').val(response.data.idakunsupplier).trigger('change');
        //         $('#form-modal-supplier #suppliernama-modal').val(response.data.suppliernama);
        //         $('#form-modal-supplier #supplieraddress1-modal').val(response.data.supplieraddress1);
        //         $('#form-modal-supplier #supplieraddress2-modal').val(response.data.supplieraddress2);
        //         $('#form-modal-supplier #supplieraddressrt-modal').val(response.data.supplieraddressrt);
        //         $('#form-modal-supplier #supplieraddressrw-modal').val(response.data.supplieraddressrw);
        //         $('#form-modal-supplier #supplierlokasikel-modal').val(response.data.supplierlokasikel);
        //         $('#form-modal-supplier #supplierlokasikec-modal').val(response.data.supplierlokasikec);
        //         $('#form-modal-supplier #supplierlokasikab-modal').val(response.data.supplierlokasikab);
        //         $('#form-modal-supplier #supplierlokasiprop-modal').val(response.data.supplierlokasiprop);
        //         $('#form-modal-supplier #supplierlokasineg-modal').val(response.data.supplierlokasineg);
        //     }
        // });

        $('#form-header #idcardsupplier').val('').trigger('change');
        $('#form-modal-supplier #idakunsupplier-modal').val(0).trigger('change');
        $('#form-modal-supplier #suppliernama-modal').val('');
        $('#form-modal-supplier #supplieraddress1-modal').val('');
        $('#form-modal-supplier #supplieraddress2-modal').val('');
        $('#form-modal-supplier #supplieraddressrt-modal').val('');
        $('#form-modal-supplier #supplieraddressrw-modal').val('');
        $('#form-modal-supplier #supplierlokasikel-modal').val('');
        $('#form-modal-supplier #supplierlokasikec-modal').val('');
        $('#form-modal-supplier #supplierlokasikab-modal').val('');
        $('#form-modal-supplier #supplierlokasiprop-modal').val('');
        $('#form-modal-supplier #supplierlokasineg-modal').val('');
    }

    function resetForm() {
        $('#divform').html('')
    }

    function resetData() {
        $.each(maincms['panel'], function(k, v) {
            if (k == 0) {
                localStorage.removeItem('input-data');
            } else {
                localStorage.removeItem('input-data-'+v.noid);
            }
        })
    }

    function resetDataTab(panel_noid) {
        var maincms_widgetgridfield = new Array();
        $.each(maincms['widgetgridfield'], function(k, v) {
            $.each(v, function(k2, v2) {
                if (k2 == 0) {
                    maincms_widgetgridfield[panel_noid] = new Array();    
                }
                maincms_widgetgridfield[panel_noid][v2.fieldname] = v2;
            })
        })
        console.log('default')
        console.log(maincms_widgetgridfield)
        
        $.each(JSON.parse(localStorage.getItem('input-data-'+panel_noid)), function(k, v) {
            $.each(fieldnametab[panel_noid], function(k2, v2) {
                if (!v2.search('_lookup')) {
                    $("#tab-form-div-"+panel_noid+" div div #"+v2).attr('disabled', false);
                    $("#tab-form-div-"+panel_noid+" div div #"+v2).val(maincms_widgetgridfield[panel_noid][v2].coldefault);
                }
            })
        })
    }

    const showLoading = () => {
        localStorage.getItem(main.table+'-fullscreen') == 'true'
            ? $('#loading-fullscreen').show()
            : $('#loading').show();
    }

    const hideLoading = () => {
        localStorage.getItem(main.table+'-fullscreen') == 'true'
            ? $('#loading-fullscreen').hide()
            : $('#loading').hide();
    }

    function cancelData() {
        showLoading()

        localStorage.removeItem(main.tabledetail);
        undisableForm();
        unsetFormHeader();
        unsetFormModalTanggal();
        unsetFormDetail();
        unsetFormModalDetail();
        unsetFormFooter();


        $('#from-departement').hide(); // Show form
        $('#tool-kedua').hide(); // Show button cancel
        $('#tool_refresh').show(); // Hide button refresh
        $('#tools-dropdown').show(); // Hide button tools
        $('#tabledepartemen').show(); // Hide datatable
        if (<?= $isadd ?> == 1) {
            $('#tool-pertama').show(); // Show buton add data
        }
        $('#space_filter_button').show(); // Hide filte
        $('#btn-saveto').html(''); // Reset button save to

        $('#formheader #idtypecostcontrol').on("select2-close", function () {
            setTimeout(function() {
                $('.select2-container-active').removeClass('select2-container-active');
                $(':focus').blur();
            }, 1);
        });

        hideLoading()
    }

    function undisableForm() {
        $('#form-header #kode').removeAttr('disabled');
        $('#form-header #generatekode').removeAttr('disabled');
        $('#form-header #kodereff').removeAttr('disabled');
        $('#form-header #tanggal').removeAttr('disabled');
        $('#form-header #tanggaldue').removeAttr('disabled');
        // $('#form-header #idtypecostcontrol').removeAttr('disabled');
        // $('#form-header #projectname').removeAttr('disabled');
        // $('#form-header #projectlocation').removeAttr('disabled');
        // $('#form-header #projectvalue').removeAttr('disabled');
        // $('#form-header #projecttype').removeAttr('disabled');
        // $('#form-modal-tanggal #tanggal-modal').removeAttr('disabled');
        $('#form-modal-tanggal #tanggaltax-modal').removeAttr('disabled');
        // $('#form-modal-tanggal #tanggaldue-modal').removeAttr('disabled');
        $('#form-detail #idinventor').removeAttr('disabled');
        $('#form-detail #namainventor').removeAttr('disabled');
        $('#form-detail #unitqty').removeAttr('disabled');
        $('#form-detail #update-detail').hide();
        $('#form-detail #save-detail').show();
        // $('#form-modal-detail #namainventor-modal').removeAttr('disabled');
        $('#form-modal-detail #idinventor-modal').removeAttr('disabled');
        $('#form-modal-detail #namainventor-modal').removeAttr('disabled');
        $('#form-modal-detail #keterangan-modal').removeAttr('disabled');
        $('#form-modal-detail #idgudang-modal').removeAttr('disabled');
        $('#form-modal-detail #idcompany-modal').removeAttr('disabled');
        $('#form-modal-detail #iddepartment-modal').removeAttr('disabled');
        $('#form-modal-detail #idsatuan-modal').removeAttr('disabled');
        $('#form-modal-detail #konvsatuan-modal').removeAttr('disabled');
        $('#form-modal-detail #unitqty-modal').removeAttr('disabled');
        $('#form-modal-detail #unitqtysisa-modal').removeAttr('disabled');
        $('#form-modal-detail #unitprice-modal').removeAttr('disabled');
        $('#form-modal-detail #subtotal-modal').removeAttr('disabled');
        $('#form-modal-detail #discountvar-modal').removeAttr('disabled');
        $('#form-modal-detail #discount-modal').removeAttr('disabled');
        $('#form-modal-detail #nilaidisc-modal').removeAttr('disabled');
        $('#form-modal-detail #idtypetax-modal').removeAttr('disabled');
        $('#form-modal-detail #idtax-modal').removeAttr('disabled');
        $('#form-modal-detail #prosentax-modal').removeAttr('disabled');
        $('#form-modal-detail #nilaitax-modal').removeAttr('disabled');
        $('#form-modal-detail #hargatotal-modal').removeAttr('disabled');
        // $('#form-header #idcardsales').removeAttr('disabled');
        $('#form-header #idcardcashier').removeAttr('disabled');
        // $('#form-header #idcardcustomer').removeAttr('disabled');
        $('#form-header #idcompany').removeAttr('disabled');
        $('#form-header #iddepartment').removeAttr('disabled');
        $('#form-header #idgudang').removeAttr('disabled');
        $('#form-header #idcardpj').removeAttr('disabled');
        $('#form-header #idcardkoor').removeAttr('disabled');
        $('#form-header #idcardsitekoor').removeAttr('disabled');
        $('#form-header #idcarddrafter').removeAttr('disabled');
        $('#form-header #idcardsponsor').removeAttr('disabled');
        $('#form-footer #keterangan').removeAttr('disabled');
        $('#form-internal #idcashbankbayar').removeAttr('disabled');
        $('#form-internal #idakunpersediaan').removeAttr('disabled');
        $('#form-internal #idakunpurchase').removeAttr('disabled');
    }

    function unsetFormHeader() {
        $('#form-header #kode').val('');
        $('#form-header #kodereff').val('');
        $('#form-header #idstatustranc').val(1).trigger('change');
        $('#form-header #idstatustranc').removeAttr('style');
        $('#form-header #idstatustranc-info').css('background-color', '<?= $color[$mtypetranctypestatus_classcolor] ?>');
        $('#form-header #idstatustranc-info b').text('<?= $mtypetranctypestatus_nama ?>');
        if (!$('#form-header #idstatuscard').val() == '') {
            $('#form-header #idstatuscard').val('').trigger('change');
        }
        $('#form-header #dostatus').val('');
        $('#form-header #tanggal').val('<?= $defaulttanggal ?>');
        $('#form-header #tanggaldue').val('<?= $defaulttanggaldue ?>');
        // if (!$('#form-header #idcardsupplier').val() == '') {
        //     $('#form-header #idcardsupplier').val('').trigger('change');
        // }
        if (!$('#form-header #idcardsales').val() == '') {
            $('#form-header #idcardsales #idcardsales-empty').attr('selected', 'selected');
            $('#select2-idcardsales-container').text($('#form-header #idcardsales #idcardsales-empty').text());
        }
        if (!$('#form-header #idcardcashier').val() == '') {
            $('#form-header #idcardcashier #idcardcashier-empty').attr('selected', 'selected');
            $('#select2-idcardcashier-container').text($('#form-header #idcardcashier #idcardcashier-empty').text());
        }
        if (!$('#form-header #idcardcustomer').val() == '') {
            $('#form-header #idcardcustomer #idcardcustomer-empty').attr('selected', 'selected');
            $('#select2-idcardcustomer-container').text($('#form-header #idcardcustomer #idcardcustomer-empty').text());
        }
        if (!$('#form-header #idcompany').val() == '') {
            $('#form-header #idcompany #idcompany-empty').attr('selected', 'selected');
            $('#select2-idcompany-container').text($('#form-header #idcompany #idcompany-empty').text());
        }
        if (!$('#form-header #iddepartment').val() == '') {
            $('#form-header #iddepartment #iddepartment-empty').attr('selected', 'selected');
            $('#select2-iddepartment-container').text($('#form-header #iddepartment #iddepartment-empty').text());
        }
        if (!$('#form-header #idgudang').val() == '') {
            $('#form-header #idgudang #idgudang-empty').attr('selected', 'selected');
            $('#select2-idgudang-container').text($('#form-header #idgudang #idgudang-empty').text());
        }
        if (!$('#form-header #idcardpj').val() == '') {
            $('#form-header #idcardpj #idcardpj-empty').attr('selected', 'selected');
            $('#select2-idcardpj-container').text($('#form-header #idcardpj #idcardpj-empty').text());
        }
        if (!$('#form-header #idcardkoor').val() == '') {
            $('#form-header #idcardkoor #idcardkoor-empty').attr('selected', 'selected');
            $('#select2-idcardkoor-container').text($('#form-header #idcardkoor #idcardkoor-empty').text());
        }
        if (!$('#form-header #idcardsitekoor').val() == '') {
            $('#form-header #idcardsitekoor #idcardsitekoor-empty').attr('selected', 'selected');
            $('#select2-idcardsitekoor-container').text($('#form-header #idcardsitekoor #idcardsitekoor-empty').text());
        }
        if (!$('#form-header #idcarddrafter').val() == '') {
            $('#form-header #idcarddrafter #idcarddrafter-empty').attr('selected', 'selected');
            $('#select2-idcarddrafter-container').text($('#form-header #idcarddrafter #idcarddrafter-empty').text());
        }
        if (!$('#form-header #idcardsponsor').val() == '') {
            $('#form-header #idcardsponsor #idcardsponsor-empty').attr('selected', 'selected');
            $('#select2-idcardsponsor-container').text($('#form-header #idcardsponsor #idcardsponsor-empty').text());
        }
        $('#form-header #idtypecostcontrol').val('').trigger('change');
        $('#form-header #projectname').val('');
        $('#form-header #projectlocation').val('');
        $('#form-header #idcostcontrol').val('');
        $('#form-header #kodecostcontrol').val('');
    }

    function unsetFormInternal() {
        if (!$('#form-internal #idcashbankbayar').val() == '') {
            $('#form-header #idcashbankbayar #idcashbankbayar-empty').attr('selected', 'selected');
            $('#select2-idcashbankbayar-container').text($('#form-header #idcashbankbayar #idcashbankbayar-empty').text());
        }
        if (!$('#form-internal #idakunpersediaan').val() == '') {
            $('#form-header #idakunpersediaan #idakunpersediaan-empty').attr('selected', 'selected');
            $('#select2-idakunpersediaan-container').text($('#form-header #idakunpersediaan #idakunpersediaan-empty').text());
        }
        if (!$('#form-internal #idakunpurchase').val() == '') {
            $('#form-header #idakunpurchase #idakunpurchase-empty').attr('selected', 'selected');
            $('#select2-idakunpurchase-container').text($('#form-header #idakunpurchase #idakunpurchase-empty').text());
        }
    }

    function unsetFormModalTanggal() {
        $('#form-modal-tanggal #tanggal-modal').val('<?= $defaulttanggal ?>');
        $('#form-modal-tanggal #tanggaltax-modal').val('');
        $('#form-modal-tanggal #tanggaldue-modal').val('<?= $defaulttanggal ?>');
    }

    function unsetFormModalSupplier() {
        $('#form-modal-supplier #idcardsupplier-modal').val('');
        if (!$('#form-modal-supplier #idakunsupplier').val() == '') {
            $('#form-modal-supplier #idakunsupplier').val('').trigger('change');
        }
        $('#form-modal-supplier #suppliernama-modal').val('');
        $('#form-modal-supplier #supplieraddress1-modal').val('');
        $('#form-modal-supplier #supplieraddress2-modal').val('');
        $('#form-modal-supplier #supplieraddressrt-modal').val('');
        $('#form-modal-supplier #supplieraddressrw-modal').val('');
        $('#form-modal-supplier #supplierlokasikel-modal').val('');
        $('#form-modal-supplier #supplierlokasikec-modal').val('');
        $('#form-modal-supplier #supplierlokasikab-modal').val('');
        $('#form-modal-supplier #supplierlokasiprop-modal').val('');
        $('#form-modal-supplier #supplierlokasineg-modal').val('');
    }
    
    function unsetFormFooter() {
        $('#form-footer #keterangan').val('');
        $('#form-footer #subtotal').val(0);
        $('#form-footer #total').val(0);
    }

    function view(id) {
        $.when(
            $.ajax({
                url: '/find_tmd',
                type: "POST",
                dataType: 'json',
                header:{
                    'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    "id" : id,
                    "namatable" : $("#namatable").val(),
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    maincms: JSON.stringify(maincms)
                },
                success: function (response) {
                    return response;
                }
            })
        ).then(function(responsefind) {
            statusadd = false;
            statusdetail = false;
            nownoid = id;
            $.each(maincms['widget'], function(k, v) {
                getNextNoidTab(v.maintable, v.noid)
            })
            resetForm();
            resetData();
            getCmsTab(statusadd);
            getForm(statusadd, maincms['widget'][0]['noid'], 'divform', responsefind);
            // alert(response)
            // getCmsTabDatatable();


            editid = id;

            // var x = document.getElementById("from-departement");
            // var x2 = document.getElementById("tabledepartemen");
            // var y = document.getElementById("tool-kedua");
            // var y2 = document.getElementById("tool-pertama");
            // var space_filter_button = $('#space_filter_button');

            // if (x.style.display === "none") {
            //     x.style.display = "block";
            //     x2.style.display = "none";
            //     y.style.display = "block";
            //     y2.style.display = "none";
            //     space_filter_button.css('display', 'none');
            // } else {
            //     x.style.display = "none";
            //     x2.style.display = "block";
            //     space_filter_button.css('display', 'block');
            // }

            $('#from-departement').show();
            $('#tabledepartemen').hide();
            $('#tool-kedua').show();
            $('#tool-pertama').hide();
            $('#space_filter_button').hide();
            $('#tool_refresh').hide();
            $('#tool_filter').hide()
            $('#tool_refresh_up').hide();

            // $.ajax({
            //     url: '/find_tmd',
            //     type: "POST",
            //     dataType: 'json',
            //     header:{
            //         'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            //     },
            //     data: {
            //         "id" : id,
            //         "namatable" : $("#namatable").val(),
            //         "_token": $('meta[name="csrf-token"]').attr('content'),
            //         maincms: JSON.stringify(maincms)
            //     },
            //     success: function (response) {
                    nownoid = responsefind.nownoid;

                    var maincms_panel = new Array();
                    $.each(maincms['panel'], function(k, v) {
                        $.each(maincms['widget'], function(k2, v2) {
                            if (v.idwidget == v2.noid) {
                                maincms_panel[v.noid] = v2;
                            }
                        })
                    })

                    // console.log(response.result_detail)
                    var key = 0;
                    $.each(responsefind.result_detail, function(k, v) {
                        if (key == 0) {
                            nownoidtab = maincms_panel[k].noid
                        }
                        // alert(k)
                        // console.log(v)
                        // localStorage.removeItem('input-data-'+k)
                        localStorage.setItem('input-data-'+k, JSON.stringify(v))
                        localStorage.setItem('tablename-'+k, maincms_panel[k].maintable)
                        // console.log('aku cek localstoragenya')
                        // console.log(JSON.parse(localStorage.getItem('input-data-'+k)))
                        // console.log('input-data-'+k)
                        key++;
                    })
                    
                    // destroyTabDatatable('fincashbankdetail')
                    getCmsTabDatatable()
                    // alert(response['noid'])
                    // $('#noid').val(id)
                    // $.each(response, function(k, v) {
                    //     $("#"+v.field).val(v.value);
                    //     if (v.disabled) {
                    //         $("#"+v.field).attr('disabled', 'disabled');
                    //     }
                    //     if (v.edithidden) {
                    //         $("#div-"+v.field).css('display', 'none');
                    //     }
                    // });
                    // console.log(response)
                // }
            // });
        });
    }

    function viewTab(another_table, another_id, id, panel_noid) {
        resetDataTab(panel_noid)
        statusaddtab[panel_noid] = false;
        
        var maincms_panel = new Array();
        $.each(maincms['panel'], function(k, v) {
            $.each(maincms['widget'], function(k2, v2) {
                if (v.idwidget == v2.noid) {
                    maincms_panel[v.noid] = v2;
                }
            })
        })
        
        $.each(JSON.parse(localStorage.getItem('input-data-'+panel_noid)), function(k, v) {
            if (v.noid == another_id) {
                nownoidtab = v['noid'];
                $.each(fieldnametab[panel_noid], function(k2, v2) {
                    $("#tab-form-div-"+panel_noid+" div div #"+v2).attr('disabled', true);
                    $("#tab-form-div-"+panel_noid+" div div #"+v2).val(v[v2]);
                })
            }
        })

        $('#tab-form-'+panel_noid).show()
        $('#add-data-'+panel_noid).hide()
        $('#save-data-'+panel_noid).show()
        $('#cancel-data-'+panel_noid).show()
        $('#tab-table-'+panel_noid+'_wrapper').hide()
        $('#tab-appendInfoDatatable-'+panel_noid).hide()
        $('#ts-'+panel_noid).hide()
        $('#tool_refresh-'+panel_noid).hide();
        $('#tool_filter-'+panel_noid).hide()
        $('#tool_refresh_up-'+panel_noid).hide();
    }

    function editTab(another_table, another_id, id, panel_noid) {
        resetDataTab(panel_noid)
        statusaddtab[panel_noid] = false;
        // console.log(statusaddtab)
        // alert(another_table+' '+another_id+' '+id)
        var maincms_panel = new Array();
        $.each(maincms['panel'], function(k, v) {
            $.each(maincms['widget'], function(k2, v2) {
                if (v.idwidget == v2.noid) {
                    maincms_panel[v.noid] = v2;
                }
            })
        })
        // console.log(maincms_panel)
        // console.log(panel_noid)
        // console.log(maincms_panel[panel_noid].noid)

        // console.log(JSON.parse(localStorage.getItem('input-data-'+panel_noid)))
        // console.log(fieldnametab)
        $.each(JSON.parse(localStorage.getItem('input-data-'+panel_noid)), function(k, v) {
            if (v.noid == another_id) {
                nownoidtab = v['noid'];
                $.each(fieldnametab[panel_noid], function(k2, v2) {
                    var fieldsplit = v2.split('_lookup');

                    if (fieldsplit.length == 2) {
                        $("#tab-form-div-"+panel_noid+" div div div #"+localStorage.getItem('tablename-'+panel_noid)+'-'+fieldsplit[0]).val('disabled', false);
                        $("#tab-form-div-"+panel_noid+" div div div #"+localStorage.getItem('tablename-'+panel_noid)+'-'+fieldsplit[0]).val(v[fieldsplit[0]]).trigger('change');
                    } else {
                        $("#tab-form-div-"+panel_noid+" div div #"+localStorage.getItem('tablename-'+panel_noid)+'-'+v2).attr('disabled', false);
                        $("#tab-form-div-"+panel_noid+" div div #"+localStorage.getItem('tablename-'+panel_noid)+'-'+v2).val(v[v2]);
                    }

                    // console.log("#"+v2+'     '+v[v2])
                    // if (v.disabled) {
                    //     $("#"+v.field).attr('disabled', 'disabled');
                    // }
                    // if (v.edithidden) {
                    //     $("#div-"+v.field).css('display', 'none');
                    // }
                })
            }
        })
        // getForm(statusadd, maincms_panel[panel_noid].noid, 'tab-form-div-'+panel_noid);

        $('#tab-form-'+panel_noid).show()
        $('#add-data-'+panel_noid).hide()
        $('#save-data-'+panel_noid).show()
        $('#cancel-data-'+panel_noid).show()
        $('#tab-table-'+panel_noid+'_wrapper').hide()
        $('#tab-appendInfoDatatable-'+panel_noid).hide()
        $('#ts-'+panel_noid).hide()
        $('#space_filter_button-'+panel_noid).hide();
        $('#tool_refresh-'+panel_noid).hide();
        $('#tool_filter-'+panel_noid).hide()
        $('#tool_refresh_up-'+panel_noid).hide();
        console.log('masok jos')
    }

    function edit(id) {
        $.when(
            $.ajax({
                url: '/find_tmd',
                type: "POST",
                dataType: 'json',
                header:{
                    'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    "id" : id,
                    "namatable" : $("#namatable").val(),
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    maincms: JSON.stringify(maincms)
                },
                success: function (response) {
                    return response;
                }
            })
        ).then(function(responsefind) {
            statusadd = false;
            statusdetail = false;
            nownoid = id;
            $.each(maincms['widget'], function(k, v) {
                getNextNoidTab(v.maintable, v.noid)
            })
            resetForm();
            resetData();
            getCmsTab(statusadd);
            getForm(statusadd, maincms['widget'][0]['noid'], 'divform', responsefind);
            // alert(response)
            // getCmsTabDatatable();


            editid = id;

            // var x = document.getElementById("from-departement");
            // var x2 = document.getElementById("tabledepartemen");
            // var y = document.getElementById("tool-kedua");
            // var y2 = document.getElementById("tool-pertama");
            // var space_filter_button = $('#space_filter_button');

            // if (x.style.display === "none") {
            //     x.style.display = "block";
            //     x2.style.display = "none";
            //     y.style.display = "block";
            //     y2.style.display = "none";
            //     space_filter_button.css('display', 'none');
            // } else {
            //     x.style.display = "none";
            //     x2.style.display = "block";
            //     space_filter_button.css('display', 'block');
            // }

            $('#from-departement').show();
            $('#tabledepartemen').hide();
            $('#tool-kedua').show();
            $('#tool-pertama').hide();
            $('#space_filter_button').hide();
            $('#tool_refresh').hide();
            $('#tool_filter').hide()
            $('#tool_refresh_up').hide();

            // $.ajax({
            //     url: '/find_tmd',
            //     type: "POST",
            //     dataType: 'json',
            //     header:{
            //         'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            //     },
            //     data: {
            //         "id" : id,
            //         "namatable" : $("#namatable").val(),
            //         "_token": $('meta[name="csrf-token"]').attr('content'),
            //         maincms: JSON.stringify(maincms)
            //     },
            //     success: function (response) {
                    nownoid = responsefind.nownoid;

                    var maincms_panel = new Array();
                    $.each(maincms['panel'], function(k, v) {
                        $.each(maincms['widget'], function(k2, v2) {
                            if (v.idwidget == v2.noid) {
                                maincms_panel[v.noid] = v2;
                            }
                        })
                    })

                    // console.log(response.result_detail)
                    var key = 0;
                    $.each(responsefind.result_detail, function(k, v) {
                        if (key == 0) {
                            nownoidtab = maincms_panel[k].noid
                        }
                        // alert(k)
                        // console.log(v)
                        // localStorage.removeItem('input-data-'+k)
                        localStorage.setItem('input-data-'+k, JSON.stringify(v))
                        localStorage.setItem('tablename-'+k, maincms_panel[k].maintable)
                        select_all_tab[k] = new Array();
                        // console.log('aku cek localstoragenya')
                        // console.log(JSON.parse(localStorage.getItem('input-data-'+k)))
                        // console.log('input-data-'+k)
                        key++;
                    })
                    
                    // destroyTabDatatable('fincashbankdetail')
                    getCmsTabDatatable()
                    // alert(response['noid'])
                    // $('#noid').val(id)
                    // $.each(response, function(k, v) {
                    //     $("#"+v.field).val(v.value);
                    //     if (v.disabled) {
                    //         $("#"+v.field).attr('disabled', 'disabled');
                    //     }
                    //     if (v.edithidden) {
                    //         $("#div-"+v.field).css('display', 'none');
                    //     }
                    // });
                    // console.log(response)
                // }
            // });
        });
    }

    function removeTab(another_table, another_id, id, panel_noid) {
        if (confirm('Are you sure you want to delete this?')) {
            nownoidtab = another_id;
            current_idd = localStorage.getItem('input-data-'+panel_noid);

            var replace_current_idd = new Array();
            var key = 0;
            $.each(JSON.parse(current_idd), function(k, v) {
                if (v.noid != nownoidtab) {
                    replace_current_idd[key] = v;
                    key++;
                }
            })

            refreshSumTab(id, panel_noid)

            localStorage.removeItem('input-data-'+panel_noid);
            localStorage.setItem('input-data-'+panel_noid, JSON.stringify(replace_current_idd))

            $.notify({
                message: 'Delete data has been succesfullly.' 
            },{
                type: 'success'
            });
            refreshTab();
        }

        console.log(localStorage.getItem('input-data-'+panel_noid))
        return false





        // console.log(id);return false;
        if (confirm('Are you sure you want to delete this?')) {
            $.ajax({
                url: '/delete_tab_tmd',
                type: "POST",
                header:{
                    'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    "id" : id,
                    "another_id" : another_id,
                    "another_table" : another_table,
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                },
                success: function () {
                    // $("#notiftext").text('Data  Berhasil di hapus');
                    $.notify({
                        message: 'Delete data has been succesfullly.' 
                    },{
                        type: 'success'
                    });
                    refreshTab();
                    // $('#divsucces').show(0).delay(5000).hide(0);
                    // var x = document.getElementById("from-departement");
                    // var x2 = document.getElementById("tabledepartemen");
                    // var y = document.getElementById("tool-kedua");
                    // var y2 = document.getElementById("tool-pertama");
                    // x.style.display = "none";
                    // x2.style.display = "block";
                    // y.style.display = "none";
                    // y2.style.display = "block";
                }
            });
        }
    }

    function remove(id) {
        // console.log(id);return false;
        if (confirm('Are you sure you want to delete this?')) {
            $.ajax({
                url: '/delete_tmd',
                type: "POST",
                header:{
                    'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    "id" : id,
                    "table" : maincms['widget'][0]['maintable'],
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    maincms: JSON.stringify(maincms),
                },
                success: function () {
                    // $("#notiftext").text('Data  Berhasil di hapus');
                    $.notify({
                        message: 'Delete data has been succesfullly.' 
                    },{
                        type: 'success'
                    });
                    refresh();
                    // $('#divsucces').show(0).delay(5000).hide(0);
                    var x = document.getElementById("from-departement");
                    var x2 = document.getElementById("tabledepartemen");
                    var y = document.getElementById("tool-kedua");
                    var y2 = document.getElementById("tool-pertama");
                    x.style.display = "none";
                    x2.style.display = "block";
                    y.style.display = "none";
                    y2.style.display = "block";
                }
            });
        }
    }
    
    function print(noid, urlprint) {
        window.open('http://ux.lambada.id/'+noid+'/'+urlprint, '_blank');
        return false;

        $.ajax({
                url: panelnoid+'/print/'+noid,
                type: "GET",
                header:{
                    'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    return response
                }
        })
    }

    (function ($) {
    // function infoDatatable() {
        // console.log('tes gaes')
        
        function calcDisableClasses(oSettings) {
            var start = oSettings._iDisplayStart;
            var length = oSettings._iDisplayLength;
            var visibleRecords = oSettings.fnRecordsDisplay();
            var all = length === -1;

            // Gordey Doronin: Re-used this code from main jQuery.dataTables source code. To be consistent.
            var page = all ? 0 : Math.ceil(start / length);
            var pages = all ? 1 : Math.ceil(visibleRecords / length);

            var disableFirstPrevClass = (page > 0 ? '' : oSettings.oClasses.sPageButtonDisabled);
            var disableNextLastClass = (page < pages - 1 ? '' : oSettings.oClasses.sPageButtonDisabled);

            return {
                'first': disableFirstPrevClass,
                'previous': disableFirstPrevClass,
                'next': disableNextLastClass,
                'last': disableNextLastClass
            };
        }

        function calcCurrentPage(oSettings) {
            return Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength) + 1;
        }

        function calcPages(oSettings) {
            return Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength);
        }

        var firstClassName = 'first';
        var previousClassName = 'previous';
        var nextClassName = 'next';
        var lastClassName = 'last';

        var paginateClassName = 'paginate';
        var paginatePageClassName = 'paginate_page';
        var paginateInputClassName = 'paginate_input';
        var paginateTotalClassName = 'paginate_total';

        $.fn.dataTableExt.oPagination.input = {
            'fnInit': function (oSettings, nPaging, fnCallbackDraw) {
                var nFirst = document.createElement('span');
                var nPrevious = document.createElement('span');
                var nNext = document.createElement('span');
                var nLast = document.createElement('span');
                var nInput = document.createElement('input');
                var nTotal = document.createElement('span');
                var nInfo = document.createElement('span');

                var language = oSettings.oLanguage.oPaginate;
                var classes = oSettings.oClasses;
                var info = language.info || `&nbsp;&nbsp;
                                                <label style="padding-top: 8px">Page</label>&nbsp;
                                                _INPUT_&nbsp;
                                                <label style="padding-top: 8px">of&nbsp;_TOTAL_</label>&nbsp;`;

                nFirst.innerHTML = language.sFirst;
                nPrevious.innerHTML = language.sPrevious;
                nNext.innerHTML = language.sNext;
                nLast.innerHTML = language.sLast;

                nFirst.className = firstClassName + ' ' + classes.sPageButton;
                nPrevious.className = previousClassName + ' ' + classes.sPageButton;
                nNext.className = nextClassName + ' ' + classes.sPageButton;
                nLast.className = lastClassName + ' ' + classes.sPageButton;

                nInput.className = paginateInputClassName;
                nTotal.className = paginateTotalClassName;

                if (oSettings.sTableId !== '') {
                    nPaging.setAttribute('id', oSettings.sTableId + '_' + paginateClassName);
                    nFirst.setAttribute('id', oSettings.sTableId + '_' + firstClassName);
                    nPrevious.setAttribute('id', oSettings.sTableId + '_' + previousClassName);
                    nNext.setAttribute('id', oSettings.sTableId + '_' + nextClassName);
                    nLast.setAttribute('id', oSettings.sTableId + '_' + lastClassName);
                }

                nInput.type = 'text';

                info = info.replace(/_INPUT_/g, '</span>' + nInput.outerHTML + '<span>');
                info = info.replace(/_TOTAL_/g, '</span>' + nTotal.outerHTML + '<span>');
                nInfo.innerHTML = '<span>' + info + '</span>';

                nPaging.appendChild(nFirst);
                nPaging.appendChild(nPrevious);
                $(nInfo).children().each(function (i, n) {
                    nPaging.appendChild(n);
                });
                nPaging.appendChild(nNext);
                nPaging.appendChild(nLast);

                $(nFirst).click(function() {
                    var iCurrentPage = calcCurrentPage(oSettings);
                    if (iCurrentPage !== 1) {
                        oSettings.oApi._fnPageChange(oSettings, 'first');
                        fnCallbackDraw(oSettings);
                    }
                });

                $(nPrevious).click(function() {
                    var iCurrentPage = calcCurrentPage(oSettings);
                    if (iCurrentPage !== 1) {
                        oSettings.oApi._fnPageChange(oSettings, 'previous');
                        fnCallbackDraw(oSettings);
                    }
                });

                $(nNext).click(function() {
                    var iCurrentPage = calcCurrentPage(oSettings);
                    if (iCurrentPage !== calcPages(oSettings)) {
                        oSettings.oApi._fnPageChange(oSettings, 'next');
                        fnCallbackDraw(oSettings);
                    }
                });

                $(nLast).click(function() {
                    var iCurrentPage = calcCurrentPage(oSettings);
                    if (iCurrentPage !== calcPages(oSettings)) {
                        oSettings.oApi._fnPageChange(oSettings, 'last');
                        fnCallbackDraw(oSettings);
                    }
                });

                $(nPaging).find('.' + paginateInputClassName).keyup(function (e) {
                    // 38 = up arrow, 39 = right arrow
                    if (e.which === 38 || e.which === 39) {
                        this.value++;
                    }
                    // 37 = left arrow, 40 = down arrow
                    else if ((e.which === 37 || e.which === 40) && this.value > 1) {
                        this.value--;
                    }

                    if (this.value === '' || this.value.match(/[^0-9]/)) {
                        /* Nothing entered or non-numeric character */
                        this.value = this.value.replace(/[^\d]/g, ''); // don't even allow anything but digits
                        return;
                    }

                    var iNewStart = oSettings._iDisplayLength * (this.value - 1);
                    if (iNewStart < 0) {
                        iNewStart = 0;
                    }
                    if (iNewStart >= oSettings.fnRecordsDisplay()) {
                        iNewStart = (Math.ceil((oSettings.fnRecordsDisplay()) / oSettings._iDisplayLength) - 1) * oSettings._iDisplayLength;
                    }

                    oSettings._iDisplayStart = iNewStart;
                    oSettings.oInstance.trigger("page.dt", oSettings);
                    fnCallbackDraw(oSettings);
                });

                // Take the brutal approach to cancelling text selection.
                $('span', nPaging).bind('mousedown', function () { return false; });
                $('span', nPaging).bind('selectstart', function() { return false; });

                // If we can't page anyway, might as well not show it.
                var iPages = calcPages(oSettings);
                if (iPages <= 1) {
                    $(nPaging).hide();
                }
            },

            'fnUpdate': function (oSettings) {
                if (!oSettings.aanFeatures.p) {
                    return;
                }

                var iPages = calcPages(oSettings);
                var iCurrentPage = calcCurrentPage(oSettings);

                var an = oSettings.aanFeatures.p;
                if (iPages <= 1) // hide paging when we can't page
                {
                    $(an).hide();
                    return;
                }

                var disableClasses = calcDisableClasses(oSettings);

                $(an).show();

                // Enable/Disable `first` button.
                $(an).children('.' + firstClassName)
                    .removeClass(oSettings.oClasses.sPageButtonDisabled)
                    .addClass(disableClasses[firstClassName]);

                // Enable/Disable `prev` button.
                $(an).children('.' + previousClassName)
                    .removeClass(oSettings.oClasses.sPageButtonDisabled)
                    .addClass(disableClasses[previousClassName]);

                // Enable/Disable `next` button.
                $(an).children('.' + nextClassName)
                    .removeClass(oSettings.oClasses.sPageButtonDisabled)
                    .addClass(disableClasses[nextClassName]);

                // Enable/Disable `last` button.
                $(an).children('.' + lastClassName)
                    .removeClass(oSettings.oClasses.sPageButtonDisabled)
                    .addClass(disableClasses[lastClassName]);

                // Paginate of N pages text
                $(an).find('.' + paginateTotalClassName).html(iPages);

                // Current page number input value
                $(an).find('.' + paginateInputClassName).val(iCurrentPage);
            }
        };
    // }
    })(jQuery);
</script>
@endsection
