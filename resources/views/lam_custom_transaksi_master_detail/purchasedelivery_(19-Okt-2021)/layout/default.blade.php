<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" {{ Metronic::printAttrs('html') }} {{ Metronic::printClasses('html') }}>
    <head>
        <meta charset="utf-8"/>
        {{-- Title Section --}}
        <title>{{ config('app.name') }} | {{$tabletitle}}</title>
        {{-- Meta Data --}}
        <meta name="description" content="@yield('page_description', $page_description ?? '')"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
        <!-- <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" /> -->
        <!-- <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet" type="text/css"/> -->
        <!-- <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/> -->
        <!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
        <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
        <link href="{{ asset('css/component.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('css/layout.min.css') }}" rel="stylesheet" type="text/css"/>
        {{-- Favicon --}}
        <link rel="shortcut icon" href="{{ asset('media/logos/lambada.png') }}" />
        {{-- Fonts --}}
        <!-- {{ Metronic::getGoogleFontsInclude() }} -->
        <link href="{{ asset('css/font/font-poppins.css') }}" rel="stylesheet" type="text/css"/>
        {{-- Global Theme Styles (used by all pages) --}}
        @foreach(config('layout.resources.css') as $style)
            <link href="{{ config('layout.self.rtl') ? asset(Metronic::rtlCssPath($style)) : asset($style) }}" rel="stylesheet" type="text/css"/>
        @endforeach
        {{-- Layout Themes (used by all pages) --}}
        @foreach (Metronic::initThemes() as $theme)
            <link href="{{ config('layout.self.rtl') ? asset(Metronic::rtlCssPath($theme)) : asset($theme) }}" rel="stylesheet" type="text/css"/>
        @endforeach
        {{-- Includable CSS --}}


		<!--begin::Global Theme Styles(used by all pages)-->
		<link href="https://preview.keenthemes.com/metronic/theme/html/demo1/dist/assets/plugins/global/plugins.bundle.css?v=7.1.8" rel="stylesheet" type="text/css" />
		<link href="https://preview.keenthemes.com/metronic/theme/html/demo1/dist/assets/plugins/custom/prismjs/prismjs.bundle.css?v=7.1.8" rel="stylesheet" type="text/css" />
		<link href="https://preview.keenthemes.com/metronic/theme/html/demo1/dist/assets/css/style.bundle.css?v=7.1.8" rel="stylesheet" type="text/css" />
		<!--end::Global Theme Styles-->
		<!--begin::Layout Themes(used by all pages)-->
		<link href="https://preview.keenthemes.com/metronic/theme/html/demo1/dist/assets/css/themes/layout/header/base/light.css?v=7.1.8" rel="stylesheet" type="text/css" />
		<link href="https://preview.keenthemes.com/metronic/theme/html/demo1/dist/assets/css/themes/layout/header/menu/light.css?v=7.1.8" rel="stylesheet" type="text/css" />
		<link href="https://preview.keenthemes.com/metronic/theme/html/demo1/dist/assets/css/themes/layout/brand/dark.css?v=7.1.8" rel="stylesheet" type="text/css" />
		<link href="https://preview.keenthemes.com/metronic/theme/html/demo1/dist/assets/css/themes/layout/aside/dark.css?v=7.1.8" rel="stylesheet" type="text/css" />

        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet"></link>
        @yield('styles')
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <style>
            /* .flat {
                border-radius: 0px !important;
            } */

            #pagecaption {
                font-size: 28px;
                letter-spacing: -1px;
                color: #666;
                font-weight: 300;
            }

            #pagesubcaption {
                font-size: 14px;
                letter-spacing: 0px;
                font-weight: 300;
                color: #888;
            }

            #kt_content {
                max-width: 100% !important;
                background: #F1F3FA !important;
                padding: 5px 5px 45px 5px !important;
            }

            #kt_content #kt_subheader {
                padding: 55px 10px 5px 10px !important;
            }
            
            #kt_content #kt_subheader h3 {
                margin-bottom: 0px;
            }
            
            #kt_content #kt_breadcrumb {
                padding: 5px !important;
            }
            
            #kt_content #kt_breadcrumb nav ol {
                margin: 0px;
            }

            #kt_content .container {
                padding: 5px 0px 5px 0px !important;
            }
            
            #kt_content .container .row {
                margin-right: 0px !important;
                margin-left: 0px !important;
            }

            #kt_content .container .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12{
                padding-right:5px;
                padding-left:5px;
            }

            #kt_content .container .card{
                margin: 0px 0px 0px 0px !important;
            }

            #kt_content .container #page_panel .card-body {
                padding: 0px !important;
            }
            
            .card-header_ {
                min-height: 38px !important;
            }
            /* #kt_content .container {
                padding: 5px !important
            } */

            /* #kt_content .container .card .card-body {
                padding: 1% !important
            } */
        </style>
        <style data="tmd">
            .dt-button.dropdown-item {
                font-size: 14px;min-width: 175px;
                text-align: left;
                background-color:
                color: black;
                outline: none;
                background: transparent;
                border: 0;
            }

            .dt-button:hover{
                background-color: blue;
            }

            /* .dropdown-item{
                font-size: 14px;min-width: 175px;
                text-align: left;

            } */

            #example_baru_previous i span{
                display: none;
            }

            .table tbody tr:nth-of-type(odd) {
                background-color: #ffffff;
            }

            .table tbody tr:nth-of-type(even) {
                background-color: #BBDEFB;
            }

            .table tbody tr:hover {
                color: #ffffff;
                background-color: #90CAF9 !important;
                height: 43px;
            }

            .dataTables_wrapper .dataTables_filter  input{
                width: 43px;
                transition: all 0.5s ease;
                -o-transition: all 0.5s ease;
                -ms-transition: all 0.5s ease;
                -moz-transition: all 0.5s ease;
                -webkit-transition: all 0.5s ease;
                /* margin-top: -30px; */
                right: 9px;
            }
            .dataTables_wrapper .dataTables_filter  .clicked{
                width: calc(20vw - 10px);
                transition: all 0.5s ease;
                -o-transition: all 0.5s ease;
                -ms-transition: all 0.5s ease;
                -moz-transition: all 0.5s ease;
                -webkit-transition: all 0.5s ease;
            }

            #example_baru_last{
                display: none;
            }
            #example_baru_first{
                display: none;
            }
            #example_baru_previous span {
                margin: inherit;
            }
            #example_baru>tbody>td {
                /* height: 10px; */
            }
            .bg-main{
                background-color: #90CAF9 !important;
            }

            /* .my_row {
                height: 35px !important
            } */

            .modal-filter {
                /* margin-top: 2.5%;
                margin-left: 5%;
                margin-right: 5%; */
                max-width: 90%;
            }

            .modal-header {
                display: block !important;
                padding-bottom: 0px !important;
            }

            .modal-body {
                margin-top: -12px !important;
                margin-left: -1px !important;
            }

            #formfilter {
                margin-bottom: 0px !important;
            }

            .fancybox-close {
                position: absolute;
                top: -18px;
                right: -18px;
                width: 36px;
                height: 36px;
                cursor: pointer;
                z-index: 8040;background-image: url(assets/fancybox_sprite.png);
            }

            /* [data-notify="container"][class*="alert-pastel-"] {
                background-color: rgb(255, 255, 238) !important;
                border-width: 0px !important;
                border-left: 15px solid rgb(255, 240, 106) !important;
                border-radius: 0px !important;
                box-shadow: 0px 0px 5px rgba(51, 51, 51, 0.3) !important;
                font-family: 'Old Standard TT', serif !important;
                letter-spacing: 1px !important;
            }
            [data-notify="container"].alert-pastel-info {
                border-left-color: rgb(255, 179, 40) !important;
            }
            [data-notify="container"].alert-pastel-danger {
                border-left-color: rgb(255, 103, 76) !important;
            }
            [data-notify="container"][class*="alert-pastel-"] > [data-notify="title"] {
                color: rgb(80, 80, 57) !important;
                display: block !important;
                font-weight: 700 !important;
                margin-bottom: 5px !important;
            }
            [data-notify="container"][class*="alert-pastel-"] > [data-notify="message"] {
                font-weight: 400 !important;
            } */
            .bg-main-1 {
                background-color: #3598dc !important
            }

            #example_baru_previous {
                /* background: #2196F3 */
                /* background: #65aee8 */
            }

            #from-departement {
                /* padding: 1%; */
                margin-top: -2%
            }



            /*  */

            /* #thead th {
                border: 1px solid #3598dc;
            }

            #thead-columns th {
                border: 1px solid #3598dc;
            }

            #example_baru_wrapper .top {
                border: 1px solid #3598dc;
            }

            #example_baru thead tr th {
                border: 1px solid #3598dc;
            }

            #example_baru tbody tr td {
                border: 1px solid #3598dc;
            } */

            /*  */


            /* CUSTOM DATATABLE */
            .paginate_input{
                width: 50px;
                text-align: center;
                border: 1px solid black !important;
            }

            .dtfilter_search_btn{
                position: absolute;
                right: -3px;
                margin: 4px;
                top: auto;
            }

            .paginate_total{
                padding-top: 8px
            }

            .dataTables_wrapper .dataTables_scroll {
                margin-top: 1% !important
            }
            

            .dataTables_wrapper .top {
                border: 1px solid #3598dc ;
            }

            .dataTables_wrapper .dataTables_scroll {
                border-collapse: collapse ;
            }

            .dataTables_scrollHead .dataTables_scrollHeadInner table thead tr th {
                border: 1px solid #3598dc ;
            }

            .dataTables_wrapper .dataTables_scroll .dataTables_scrollBody {
                margin-top: -2px;
            }

            .dataTables_wrapper .dataTables_scroll .dataTables_scrollBody table tbody tr td {
                border: 1px solid #3598dc ;
            }
            
            .dataTables_length label select {
                height: 35px;
                margin-top: 2px
            }

            .dataTables_filter {
                margin-top:-41px;
                margin-right:5px;
            }

            .dataTables_filter label input {
                height: 35px;
                border: 1px solid black;
            }

            .dataTables_filter label button {
                margin-top: 2px !important; 
                margin-right: 10px !important;
                width: 40px;
                height: 30px;
            }
            /* END CUSTOM DATATABLE */
            
            .right-tabs-group .nav {
                float: right;
                border-bottom: 0px;
                z-index: 1;
                position:
                absolute;
                right: 0; 
                margin-top: -4.5%;
                margin-right: 1%; 
                
            }

            .right-tabs .nav {
                float: right;
                border-bottom: 0px;
                z-index: 1;
                position:
                absolute;
                right: 0; 
                margin-right: 5%; 
                margin-top: 1%;
            }

            .right-tabs-widget .nav {
                float: right;
                border-bottom: 0px;
                z-index: 1;
                position:
                absolute;
                right: 0; 
                margin-right: 5%; 
                margin-top: 1%;
                margin-top: -4%;
                margin-right: 3%; 
                z-index: 0;
            }
            
            .card-body-custom {
                margin-top: 0% !important;
                border: 1px solid #3598dc !important;
                padding: 1% !important;
            }

            .card-body-custom-group {
                margin-top: 0% !important;
                border: 1px solid #3598dc !important;
                padding: 1% !important;
            }

            .card-body-tab-custom {
                padding: 1% !important;
                border: 1px solid #3598dc !important;
            }

            .nav-link-custom {
                background: #E9ECEF
            }

            .card-custom {
                margin-bottom: 1% !important; 
                margin-top: 0.5% !important;
                padding: 0px 0px 0px 0px;
            }

            .sub-card-custom {
                margin-bottom: 1% !important; 
                /* margin-top: 0.5% !important; */
                margin: 5px 0px 5px 0px !important;
            }
        </style>
    </head>
    <body>
    
            @include('lam_custom_transaksi_master_detail.purchasedelivery.layout._layout')

        <script>
            function currentTime() {
                var date = new Date(); /* creating object of Date class */
                var hour = date.getHours();
                var min = date.getMinutes();
                var sec = date.getSeconds();
                hour = updateTime(hour);
                min = updateTime(min);
                sec = updateTime(sec);
                document.getElementById("clock").innerText = hour + " : " + min + " : " + sec; /* adding time to the div */
                var t = setTimeout(function(){ currentTime() }, 1000); /* setting timer */
            }

            function updateTime(k) {
                if (k < 10) {
                    return "0" + k;
                }
                else {
                    return k;
                }
            }

            currentTime(); /* calling currentTime() function to initiate the process */
        </script>
        <script>var HOST_URL = "{{ route('quick-search') }}";</script>
        {{-- Global Config (global config for global JS scripts) --}}
        <script>
            var KTAppSettings = {!! json_encode(config('layout.js'), JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES) !!};
        </script>

        {{-- Global Theme JS Bundle (used by all pages)  --}}
        @foreach(config('layout.resources.js') as $script)
            <script src="{{ asset($script) }}" type="text/javascript"></script>
        @endforeach

        {{-- Includable JS --}}
        @yield('scripts')

    </body>
</html>
