{{-- Extends layout --}}

@extends('lam_custom_layouts.default')

@section('styles')
    <!-- <link href="{{ asset('css/component.css') }}" rel="stylesheet" type="text/css"/> -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet"></link>
    <link href="//www.amcharts.com/lib/3/plugins/export/export.css?v=7.0.6" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.0.2/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.0.2/css/bootstrap-datetimepicker.css">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/tabel.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet" type="text/css"/>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <style media="screen">
        .container {
            display: none
        }

        .dataTable thead .sorting_asc,
        .dataTable thead .sorting_desc,
        .dataTable thead .sorting_disabled,
        .dataTable thead .sorting {
            padding: 3px 3px 3px 3px !important;
            /* color: black !important; */
        }

        .dataTable thead th {
            text-align: center;
        }
        
        .dataTable tbody td,
        .dataTable tbody td:hover,
        .dataTable tbody tr:hover {
            height: 26px;
            padding: 3px 3px 3px 3px !important;
        }

        .dataTable thead tr{
            height: 40px !important;
        }

        .dataTable tbody tr{
            height: 0px !important;
        }

        /* .my_row { */
            /* height: 35px; */
        /* } */

        /* .my_row:hover { */
            /* display: none; */
            /* padding: 0px 0px 0px 0px !important; */
        /* } */

        .table-bordered th,
        .table-bordered td,
        .table-bordered tfoot th {
            border: 1px solid #95c9ed !important
        }

        .dataTables_wrapper {
            margin-top: -1px !important
        }
        
        #table_pagepanel_9921050101 {
            /* width:2500px !important; */
            border: 0;
        }

        /* tbody tr td {
            padding: 0px 5px 0px 5px !important;f
            width: 100px !important;
        } */

        /* .dataTable thead th:nth-child(10) {
            width: 140px !important;
        } */

        /* .dataTable tbody tr td:nth-child(10) {
            width: 140px !important;
        } */


        /* tbody:nth-child(odd) tr td:nth-child(odd) {
            width: 100px !important;
        } */

        #btn-dropdown-info button::after {
            color: white;
        }

        #pagepanel:fullscreen {
            overflow: scroll !important;
        }
        #pagepanel:-ms-fullscreen {
            overflow: scroll !important;
        }
        #pagepanel:-webkit-full-screen {
            overflow: scroll !important;
        }
        #pagepanel:-moz-full-screen {
            overflow: scroll !important;
        }

        .modal-detail {
            min-width: 950px;
        }

        table.dataTable tfoot th {
            padding: 10px 3px 10px 3px !important;
        }

        .tfoot-kode {
            text-align: right;
        }

        #table-detail #thead-qty,
        #table-detail #thead-price,
        #table-detail #thead-total {
            text-align: center !important;
        }
        
        #th-action {
            width: 60px !important;
        }

        #table-log {
            width: 1104px !important;
        }

        td.details-control {
            background: url('https://cdn.rawgit.com/DataTables/DataTables/6c7ada53ebc228ea9bc28b1b216e793b1825d188/examples/resources/details_open.png') no-repeat center center;
            cursor: pointer;
        }
        tr.shown td.details-control {
            background: url('https://cdn.rawgit.com/DataTables/DataTables/6c7ada53ebc228ea9bc28b1b216e793b1825d188/examples/resources/details_close.png') no-repeat center center;
        }

        .dataTables_processing {
            padding-bottom: 40px !important;
        }
    </style>
@endsection


{{-- Content --}}
@section('content')
    <div class="container">
    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
    <div id="pagepanel" class="card card-custom flat table-scrollable pagepanel">
        <div class="card-header card-header_ bg-main-1 d-flex flat" >
            <div class="card-title">
                <span class="card-icon">
                    <i id="widgeticon" class="text-white icon-briefcase"></i>
                </span>
                <h3 id="widgetcaption" class="card-label text-white">
                    Purchase Request
                </h3>
            </div>
            <div class="card-toolbar" >
                <div id="tool-pertama">
                    <a  onclick="addData()" href="javascript:;" id="btn-add-data" style="color: #FFFFFF;margin-right: 5px;" class="btn btn-sm flat" onmouseover="mouseoverAddData('btn-add-data')" onmouseout="mouseoutAddData('btn-add-data')"><i style="color:#FFFFFF" class="icon-save"></i> Tambah Data</a>
                </div>
                <div id="tool-kedua" style="display: none">
                    <!-- <a class="btn btn-sm btn-success flat" style="margin-right: 5px;" id="btn-save-data" onclick="saveData()">
                        <i class="fa fa-save"></i> Save
                    </a> -->
                    <a onclick="cancelData()" class="btn btn-sm btn-default flat" style="margin-right: 5px;">
                        <i class="fa fa-reply"></i> Cancel
                    </a>
                </div>
                
                <div id="btn-saveto">
                </div>

                <div id="tool-ketiga" style="display: none">
                    <a id="back" class="btn btn-sm btn-success flat" onclick="cancelData()"  style="margin-right: 5px;">
                        <i class="fa fa-reply"></i> Back
                    </a>
                </div>
                <div class="dropdown" style="margin-right: 5px;" id="tools-dropdown">
                    <a style="background-color: transparent !important; border: 1px solid white; color: white;" class="btn btn-default btn-sm flat" href="javascript:;" data-toggle="dropdown">
                        <i style="color:#FFFFFF" class="icon-cogs"></i>
                            Tools
                        <i style="color:#FFFFFF" class="icon-angle-down"></i>
                    </a>
                    <!-- <ul style="" id="drop_export" class="dropdown-menu dropdown-menu-right">
                        <li id="drop_export2"></li>
                    </ul> -->

                    <div class="dropdown-menu dropdown-menu-right" style="min-width: 220px; padding: 5px">
                        <div id="drop_export2">
                        </div>
                    </div>

                    <!-- <div class="dropdown-menu dropdown-menu-right" style="min-width: 220px; padding: 5px">
                        <div class="row">
                            <button type="button" class="btn btn-danger col-md-12" onclick="deleteSelected()"><i class="fa fa-trash"></i> Delete Selected</button>
                        </div>
                        <div class="row mt-2">
                            <button type="button" class="btn btn-danger col-md-12" onclick="deleteAll()"><i class="fa fa-trash"></i> Delete All</button>
                        </div>
                        <div class="row mt-2">
                            <button type="button" class="btn btn-success col-md-12" onclick="exportExcel()"><i class="fa fa-file-excel"></i> Export Excel</button>
                        </div>
                    </div> -->
                </div>
                <a style="background-image: none !important; margin-right: 5px; /* height: 27px; */ width: 27px !important; text-align: center; padding: 2px 2px 0px 2px !important; border-radius: 50% !important; background-color: #2196F3; color: #FFF;" id="tool_refresh" href="javascript:;" onclick="refresh()" class="bg-main-1"><i style="color:#FFFFFF" class="icon-refresh"></i></a>
                <!-- <a style="background-image: none !important; margin-right: 5px; /* height: 27px; */ width: 27px !important; text-align: center; padding: 2px 2px 0px 2px !important; border-radius: 50% !important; background-color: #2196F3; color: #FFF;" id="tool_filter" href="javascript:;" onclick="showModalFilter()" class="bg-main-1"><i style="color:#FFFFFF" class="icon-filter"></i></a> -->
                <a style="background-image: none !important; margin-right: 5px; /* height: 27px; */ width: 27px !important; text-align: center; padding: 2px 2px 0px 2px !important; border-radius: 50% !important; background-color: #2196F3; color: #FFF;" id="tool_refresh_up" onClick="toggleButtonCollapse()" href="javascript:;" class="bg-main-1"><i style="color:#FFFFFF" class="icon-sort"></i></a>
                <a class="bg-main-1" id="tool_fullscreen" onclick="toggleFullscreen()" style="background-image: none !important; /* height: 27px; */ width: 27px !important; text-align: center; padding: 2px 2px 0px 2px !important; border-radius: 50% !important; background-color: #2196F3; color: #FFF;">
                    <i class="icon-fullscreen" style="color:#FFFFFF;"></i>   
                </a>
            </div>
            </div>
            <div class="card-body card-body-custom flat" id="pagepanel-body" style="padding: 10px !important">
                <input type="hidden" id="judul_excel">
                <input type="hidden"  id="tabel1" value="">
                <input type="hidden"  id="namatable">
                <input type="hidden"  id="url_code">
                
                <div class="row">
                    <div class="btn-toolbar" role="toolbar" aria-label="...">
                        <div id="filterlain">
                        </div>
                    </div>
                </div>

                <div class="cursor"></div>
                <!-- <input type="text" id="datetime" readonly> -->
                <div id="from-departement">
                    <!-- <form data-toggle="validator" class="form" id="formdepartement"> -->
                        
                        <!-- <div class="row" style="margin-top: 20px">
                            <div class="col-md-12">
                                <ul class="nav nav-tabs mb-3 justify-content-end" id="tab-form" role="tablist">
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link active" id="tab-general" data-mdb-toggle="tab" role="tab" aria-controls="tab-general" aria-selected="true" onclick="setTab('tab-general')">General</a>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link" id="tab-others" data-mdb-toggle="tab" role="tab" aria-controls="tab-others" aria-selected="false" onclick="setTab('tab-others')">Others</a>
                                    </li>
                                </ul>

                                <div class="tab-content" id="tab-content">
                                    <div class="tab-pane fade show active" id="tab-general-content" role="tabpanel" aria-labelledby="tab-general-content">
                                        <div class="card">
                                            <div class="card-body">
                                                Content General
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab-others-content" role="tabpanel" aria-labelledby="tab-others-content">
                                        Content Others
                                    </div>
                                </div>
                            </div>
                        </div> -->

                        <!-- Header -->
                        <div class="row" style="margin-top: 20px">
                            <div class="col-md-12" style="padding: 0px !important">
                                <div class="card" style="margin: 0px !important">
                                    <div class="card-header" style="padding-top: 0px; padding-bottom: 0px;">
                                        <div class="row">
                                            <div class="col-md-1" style="padding-top: 8px; padding-left: 0px">
                                                <b style="font-size: 16px" id="tab-title">Header</b>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group pull-left" style="margin-top: 5px">
                                                    <button type="button" class="btn btn-md btn-outline-light" id="idtypetranc-info" style="background-color: {{$color['blue-madison']}};  border-radius: 5px !important">
                                                        <b class="text-light">{{$mtypetranc_nama}}</b>
                                                    </button>
                                                </div>
                                                <div class="form-group pull-left" style="margin-top: 5px; margin-left: 5px;">
                                                    <!-- <div class="col-md-6"></div> -->
                                                    <div class="btn-group" id="btn-dropdown-info">
                                                        <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-md btn-outline-light dropdown-toggle" id="idstatustranc-info" style="background-color: {{$color[$mtypetranctypestatus_classcolor]}}; color: white; border-radius: 5px !important">
                                                            <b class="text-light idstatustranc-info"></b>
                                                        </button>
                                                        <div class="dropdown-menu dropdown-menu-right" style="min-width: 220px">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    Status Transaksi: <span class="idstatustranc-info"></span>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <b>By: <span id="idstatuscard-info"></span></b>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <b>On: <span id="dostatus-info"></span></b>
                                                                </div>
                                                            </div>
                                                            <!-- <div class="form-group row">
                                                                <label class="col-md-4">User</label>
                                                                <div class="col-md-8">
                                                                    <button type="button" class="btn btn-md btn-outline-light" id="idstatuscard-info" style="background-color: #606060; color: white; border-radius: 5px !important">
                                                                        <b></b>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row" style="margin-bottom: 0px !important">
                                                                <label class="col-md-4">Tanggal</label>
                                                                <div class="col-md-8">
                                                                    <button type="button" class="btn btn-md btn-outline-light" id="dostatus-info" style="background-color: #606060; color: white; border-radius: 5px !important">
                                                                        <b></b>
                                                                    </button>
                                                                </div>
                                                            </div> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <ul class="nav nav-tabs mb-3 justify-content-end" id="tab-form" role="tablist" style="margin-bottom: 0px !important">
                                                    <li></li>
                                                    <li class="nav-item" role="presentation">
                                                        <a class="nav-link active" id="tab-general" data-mdb-toggle="tab" role="tab" aria-controls="tab-general" aria-selected="true" onclick="setTab('tab-general', 'Header')">General</a>
                                                    </li>
                                                    <!-- <li class="nav-item" role="presentation">
                                                        <a class="nav-link" id="tab-internal" data-mdb-toggle="tab" role="tab" aria-controls="tab-internal" aria-selected="false" onclick="setTab('tab-internal', 'Internal')">Internal</a>
                                                    </li> -->
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="tab-content" id="tab-content">
                                            <div class="tab-pane fade show active" id="tab-general-content" role="tabpanel" aria-labelledby="tab-general-content">
                                                <form id="form-header">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <!-- <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Type Tranc</label>
                                                                <div class="col-md-9">
                                                                    <input type="hidden" name="idtypetranc" value="{{$mtypetranc_noid}}" class="form-control">
                                                                    <input type="text" value="{{$mtypetranc_nama}}" class="form-control" disabled>
                                                                </div>
                                                            </div> -->
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Kode PR.</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="hidden" id="noid">
                                                                        <input type="hidden" name="nourut" id="nourut">
                                                                        <input type="text" name="kode" id="kode" class="form-control" style="background-color: #E9ECEF !important" placeholder="Kode" readonly>

                                                                        <input type="hidden" name="idtypetranc" value="{{$mtypetranc_noid}}" class="form-control">
                                                                        <input type="hidden" name="kodetypetranc" value="{{$mtypetranc_kode}}" class="form-control">

                                                                        <input type="hidden" name="idstatustranc" id="idstatustranc">
                                                                        <input type="hidden" name="idstatuscard" id="idstatuscard">
                                                                        <input type="hidden" name="dostatus" id="dostatus">

                                                                        <div class="input-group-append">
                                                                            <button type="button" id="generatekode" class="btn btn-outline-secondary" onclick="getGenerateCode()">
                                                                                <i class="icon-refresh" style="padding: 0px"></i>
                                                                            </button>
                                                                        </div>
                                                                        <!-- <input type="text" name="kodereff" id="kodereff" class="form-control" placeholder="Kode Reff."> -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Tgl. <span class="text-danger">*</span> / Tgl. Due <span class="text-danger">*</span></label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="tanggal" value="{{$defaulttanggal}}" id="tanggal" class="form-control datepicker" placeholder="Tanggal" autocomplete="off" onchange="getGenerateCode()">
                                                                        <div class="input-group-append">
                                                                            <span class="input-group-text">/</span>
                                                                        </div>
                                                                        <input type="text" name="tanggaldue" value="{{$defaulttanggaldue}}" id="tanggaldue" class="form-control datepicker" placeholder="Tanggal Due" autocomplete="off">
                                                                        <div class="input-group-append">
                                                                            <button type="button" class="btn btn-outline-secondary" onclick="showModalTanggal()">
                                                                                <i class="icon-eye-open" style="padding: 0px"></i>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Typ. <span class="text-danger">*</span> / Cst. Ctr.<span class="text-danger">*</span></label>
                                                                <div class="col-md-4" id="div-idtypecostcontrol">
                                                                    <select class="form-control select2" name="idtypecostcontrol" id="idtypecostcontrol">
                                                                        <option value="">[Choose Type Cost Center]</option>
                                                                        @foreach($mtypecostcontrol as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <div class="input-group">
                                                                        <input type="hidden" name="idcostcontrol" id="idcostcontrol">
                                                                        <input type="text" id="kodecostcontrol" class="form-control" style="background-color: #E9ECEF !important;" placeholder="Cost Center" readonly>
                                                                        <div class="input-group-append">
                                                                            <button type="button" class="btn btn-outline-secondary" id="cost-control" onclick="showModalCostControl()">
                                                                                <i class="icon-hand-up" style="padding: 0px"></i>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Request by <span class="text-danger">*</span></label>
                                                                <div class="col-md-9" id="div-idcardrequest">
                                                                    <select class="form-control select2" name="idcardrequest" id="idcardrequest">
                                                                        <option value="" id="idcardrequest-empty">[Choose Request by]</option>
                                                                        @foreach($mcard as $v)
                                                                            @if($v->isemployee == 1)
                                                                                <option value="{{$v->noid}}"
                                                                                    <?= $sessionnoid == $v->noid ? 'selected' : '' ?>
                                                                                >{{$v->kode}} - {{$v->nama}}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row" style="display: none">
                                                                <label class="col-md-3 col-form-label">ID Currency</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="idcurrency" id="idcurrency">
                                                                        <option value="" id="idcurrency-empty">[Choose Currency]</option>
                                                                        @foreach($accmcurrency as $v)
                                                                            <option 
                                                                                value="{{$v->noid}}" 
                                                                                id="idcurrency-{{$v->noid}}"
                                                                                data-accmcurrency-nama="{{$v->nama}}" 
                                                                                <?= $v->noid == 1 ? 'selected' : '' ?>
                                                                            >{{$v->kode}} - {{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <!-- <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Supplier</label>
                                                                <div class="col-md-8">
                                                                    <select class="form-control select2" name="idcardsupplier" id="idcardsupplier" onchange="changeCardSupplier('idcardsupplier', 'idcardsupplier-modal')">
                                                                        <option value="">[Choose Supplier]</option>
                                                                        @foreach($mcardsupplier as $v)
                                                                            <option 
                                                                                value="{{$v->mcardsupplier_noid}}" 
                                                                                data-mlokasiprop-noid="{{$v->mlokasiprop_noid}}" 
                                                                                data-mlokasiprop-nama="{{$v->mlokasiprop_nama}}" 
                                                                                data-mlokasikab-noid="{{$v->mlokasikab_noid}}" 
                                                                                data-mlokasikab-nama="{{$v->mlokasikab_nama}}" 
                                                                                data-mlokasikec-noid="{{$v->mlokasikec_noid}}" 
                                                                                data-mlokasikec-nama="{{$v->mlokasikec_nama}}" 
                                                                                data-mlokasikel-noid="{{$v->mlokasikel_noid}}" 
                                                                                data-mlokasikel-nama="{{$v->mlokasikel_nama}}" 
                                                                            >{{$v->mcardsupplier_nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-1">
                                                                    <button type="button" class="btn btn-outline-secondary" onclick="showModalSupplier()">
                                                                        <i class="icon-eye-open" style="padding: 0px"></i>
                                                                    </button>
                                                                </div>
                                                            </div> -->
                                                            <!-- <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Comp. <span class="text-danger">*</span> - Dept. <span class="text-danger">*</span></label>
                                                                <div class="col-md-4">
                                                                    <select class="form-control select2" name="idcompany" id="idcompany" onchange="getGenerateCode()">
                                                                        <option value="" id="idcompany-empty">[Choose Company]</option>
                                                                        @foreach($genmcompany as $v)
                                                                            <option 
                                                                                value="{{$v->noid}}" id="idcompany-{{$v->noid}}"
                                                                                <?= $sessionidcompany == $v->noid ? 'selected' : '' ?>
                                                                            >{{$v->kode}} - {{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <select class="form-control select2" name="iddepartment" id="iddepartment" onclick="changeDepartment()">
                                                                        <option value="" id="iddepartment-empty">[Choose Department]</option>
                                                                        @foreach($genmdepartment as $v)
                                                                            <option 
                                                                                value="{{$v->noid}}" id="iddepartment-{{$v->noid}}"
                                                                            >{{$v->kode}} - {{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div> -->
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Ship to <span class="text-danger">*</span></label>
                                                                <div class="col-md-9" id="div-idgudang">
                                                                    <select class="form-control select2" name="idgudang" id="idgudang">
                                                                        <option value="" id="idgudang-empty">[Choose Gudang]</option>
                                                                        @foreach($genmgudang as $v)
                                                                            <option 
                                                                                value="{{$v->noid}}" id="idgudang-{{$v->noid}}"
                                                                                <?= $sessionidgudang == $v->noid ? 'selected' : '' ?>
                                                                            >{{$v->kode}} - {{$v->namalias}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Delivery Point <span class="text-danger">*</span></label>
                                                                <div class="col-md-9">
                                                                    <textarea name="deliverypoint" id="deliverypoint" cols="3" rows="5" class="form-control" placeholder="Delivery Point"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="tab-pane fade" id="tab-internal-content" role="tabpanel" aria-labelledby="tab-internal-content">
                                                <form id="form-internal">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Cash Bayar</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="idcashbankbayar" id="idcashbankbayar">
                                                                        <option value="" id="idcashbankbayar-empty">[Choose Cash Bayar]</option>
                                                                        @foreach($accmcashbank as $v)
                                                                            <option value="{{$v->noid}}" id="idcashbankbayar-{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Akun Persediaan</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="idakunpersediaan" id="idakunpersediaan">
                                                                        <option value="" id="idakunpersediaan-empty">[Choose Akun Persediaan]</option>
                                                                        @foreach($accmcashbank as $v)
                                                                            <option value="{{$v->noid}}" id="idakunpersediaan-{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Akun Purchase</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="idakunpurchase" id="idakunpurchase">
                                                                        <option value="" id="idakunpurchase-empty">[Choose Akun Purchase]</option>
                                                                        @foreach($accmcashbank as $v)
                                                                            <option value="{{$v->noid}}" id="idakunpurchase-{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Detail -->
                        <div class="row" style="margin-top: 10px">
                            <div class="col-md-12" style="padding: 0px !important">
                                <div class="card" style="margin: 0px !important">
                                    <div class="card-header">
                                        <div class="row">                                        
                                            <b class="col-md-1" style="font-size: 16px">Detail</b>
                                            <div class="col-md-3 create-detail" style="display: none">
                                                <div style="margin-top: 5px">
                                                    <input type="radio" name="searchtypeprev" id="searchtypeprev-1" value="1"> <label for="searchtypeprev-1">Previeous</label>
                                                    <input type="radio" name="searchtypeprev" id="searchtypeprev-2" value="2" checked> <label for="searchtypeprev-2">Inventory</label>
                                                </div>
                                            </div>
                                            <div class="col-md-3 create-detail">
                                                <input type="text" name="searchprev" id="searchprev" class="form-control" placeholder="Search">
                                            </div>
                                            <div class="col-md-2 create-detail">
                                                <button type="button" class="btn btn-primary" onclick="showModalDetailPrev()"><i class="icon-search"></i> Search</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <form id="form-detail">
                                            <div class="card" style="margin: 0px 0px 10px 0px !important; display: none">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label>Kode Inventor</label>
                                                                <input type="hidden" name="kodeprev" id="kodeprev" class="form-control" style="background-color: #E9ECEF !important" placeholder="Kode Purchase Offer" readonly>
                                                                <input type="hidden" id="keydetail">
                                                                <input type="hidden" id="noiddetail">
                                                                <input type="hidden" id="idmaster">
                                                                <select class="form-control" style="background-color: #E9ECEF !important" name="idinventor" id="idinventor" readonly>
                                                                    <option value="" style="display:none">[Choose Inventor]</option>
                                                                    @foreach($invminventory as $v)
                                                                        <option 
                                                                            value="{{$v->noid}}"
                                                                            data-invminventory-kode="{{$v->kode}}"
                                                                            data-invminventory-nama="{{$v->nama}}"
                                                                            data-invminventory-idsatuan="{{$v->idsatuan}}"
                                                                            data-invminventory-namasatuan="{{$v->namasatuan}}"
                                                                            data-invminventory-konversi="{{$v->konversi}}"
                                                                            data-invminventory-idtax="{{$v->idtax}}"
                                                                            data-invminventory-taxprocent="{{$v->taxprocent}}"
                                                                            data-invminventory-purchaseprice="{{$v->purchaseprice}}"
                                                                            style="display:none"
                                                                        >{{$v->kode}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label>Nama Inventor Original</label>
                                                                <input type="text" name="namainventor" id="namainventor" class="form-control" style="background-color: #E9ECEF !important" placeholder="Nama Inventor Original" readonly>
                                                                <input type="hidden" name="namainventor2" id="namainventor2" class="form-control" style="background-color: #E9ECEF !important" placeholder="Nama Inventor Updated" readonly>
                                                                <input type="hidden" id="unitqtyoriginal" class="form-control" style="background-color: #E9ECEF !important" placeholder="Qty" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label>Qty Updated <span class="text-danger">*</span></label>
                                                                <input type="text" name="unitqty" id="unitqty" class="form-control" placeholder="Qty Updated">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label>Type Package <span class="text-danger">*</span></label>
                                                                <select class="form-control select2" name="idtypepackage" id="idtypepackage">
                                                                    <option value="" style="display:none">[Choose Inventor]</option>
                                                                    @foreach($mtypepackage as $v)
                                                                        <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label>Group Inventor <span class="text-danger">*</span></label>
                                                                <select class="form-control select2" name="idgroupinv" id="idgroupinv">
                                                                    <option value="" style="display:none">[Choose Group Inventor]</option>
                                                                    @foreach($invmgroup as $v)
                                                                        <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label>Keterangan</label>
                                                                <textarea name="keterangan" id="keterangandetail" cols="3" rows="5" class="form-control" placeholder="Keterangan"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row pull-right">
                                                        <div class="col-md-12">
                                                            <button type="button" id="cancel-form-detail" class="btn btn-default" onclick="cancelFormDetail()">
                                                                <i class="fa fa-arrow-left"></i> Cancel
                                                            </button>
                                                            <button type="button" id="save-form-detail" class="btn btn-success" onclick="saveDetail()">
                                                                <i class="fa fa-save"></i> Add
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                        <div id="appendInfoDatatableDetail" class="mt-2" style="border-top: 0px solid #3598dc; border-right: 0px solid #3598dc; border-left: 0px solid #3598dc;">
                                        </div>
                                        <div id="ts-9921050102" class="table-scrollable" style="border: 0px solid #3598dc; background-color: #3598dc; margin-top: 0px !important">
                                            <table bg-color="blue" class="nowrap table table-striped table-bordered table-hover  dataTable no-footer" id="table-detail" style="background-color:#BBDEFB !important; margin: 0 !important; border: 0px solid #3598dc"  aria-describedby="table_pagepanel_9921050101_info" role="grid">
                                                <thead>
                                                    <!-- <tr id="tab-thead-9921050102" class="bg-main"><th style="text-align:center" colspan="2">#</th><th style="text-align:center" colspan="26"></th><th colspan="1" style="text-align:center">---</th></tr> -->
                                                    <tr id="tab-thead-columns-d-9921050102">
                                                        <th style="width: 30px;">
                                                            <input type="checkbox" id="select_all_9921050102" onchange="selectAllCheckboxTab(9921050102)" class="checkbox-9921050102 checkbox-9921050102-0" name="checkbox-9921050102[]" value="FALSE">
                                                        </th>
                                                        <th>No</th>
                                                        <th>Kode Prev.</th>
                                                        <th>Kode Inv.</th>
                                                        <th>Nama</th>
                                                        <th>Inventor</th>
                                                        <th>Keterangan</th>
                                                        <!-- <th>ID Gudang</th>
                                                        <th>ID Company</th>
                                                        <th>ID Department</th>
                                                        <th>ID Satuan</th>
                                                        <th>Konv Satuan</th> -->
                                                        <th id="thead-qty">Qty</th>
                                                        <th id="thead-qtysisa">Qty Sisa</th>
                                                        <th>Unit</th>
                                                        <!-- <th id="thead-price">Price</th>
                                                        <th id="thead-total">Total</th> -->
                                                        <!-- <th>Currency</th> -->
                                                        <!-- <th>Subtotal</th>
                                                        <th>Discount Var</th>
                                                        <th>Discount</th>
                                                        <th>Nilai Disc</th>
                                                        <th>ID Type Tax</th>
                                                        <th>ID Tax</th> -->
                                                        <!-- <th>Harga Total</th> -->
                                                        <!-- <th>ID Akun HPP</th>
                                                        <th>ID Akun Sales</th>
                                                        <th>ID Akun Persediaan</th>
                                                        <th>ID Akun Sales Return</th>
                                                        <th>ID Akun Purchase Return</th>
                                                        <th>Serial Number</th>
                                                        <th>Garansi Tanggal</th>
                                                        <th>Garansi Expired</th> -->
                                                        <!-- <th>Type Tranc Prev</th>
                                                        <th>Tranc Prev</th>
                                                        <th>Tranc Prev D</th>
                                                        <th>Type Tranc Order</th>
                                                        <th>Tranc Order</th>
                                                        <th>Tranc Order D</th> -->
                                                        <th id="th-action">Action</th>
                                                    </tr>
                                                </thead>
                                                <tfoot>
                                                    <tr>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th class="tfoot-kode"></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <!-- <th></th>
                                                        <th></th> -->
                                                        <!-- <th></th> -->
                                                        <th></th>
                                                        <!-- <th class="text-right" colspan="2" style="border: 1px solid #95c9ed"></th> -->
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Footer -->
                        <div class="row" style="margin-top: 10px">
                            <div class="col-md-12" style="padding: 0px !important">
                                <div class="card" style="margin: 0px !important">
                                    <div class="card-header">
                                        <b style="font-size: 16px">Footer</b>
                                    </div>
                                    <div class="card-body">
                                        <form id="form-footer">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label">Keterangan</label>
                                                        <div class="col-md-9">
                                                            <textarea name="keterangan" id="keterangan" cols="3" rows="5" class="form-control" placeholder="Keterangan"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6" style="display: none">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label">Subtotal</label>
                                                        <div class="col-md-9">
                                                            <input type="text" name="subtotal" id="subtotal" class="form-control input-mask" style="background-color: #E9ECEF !important" placeholder="Subtotal" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label">Total</label>
                                                        <div class="col-md-9">
                                                            <input type="text" name="total" id="total" class="form-control input-mask" style="background-color: #E9ECEF !important" placeholder="Total" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <!-- <ul class="nav nav-tabs mb-3" id="tab-form_" role="tablist">
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link active" id="tab-transaksi" data-mdb-toggle="tab" role="tab" aria-controls="tab-transaksi" aria-selected="true" onclick="setTab('tab-transaksi')">Transaksi</a>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link" id="tab-supplier" data-mdb-toggle="tab" role="tab" aria-controls="tab-supplier" aria-selected="false" onclick="setTab('tab-supplier')">Supplier</a>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link" id="tab-delivery" data-mdb-toggle="tab" role="tab" aria-controls="tab-delivery" aria-selected="false" onclick="setTab('tab-delivery')">Delivery</a>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link" id="tab-akun" data-mdb-toggle="tab" role="tab" aria-controls="tab-akun" aria-selected="false" onclick="setTab('tab-akun')">Akun</a>
                                    </li>
                                </ul> -->

                                <div class="tab-content" id="tab-content_">
                                    <div class="tab-pane fade" id="tab-transaksi-content" role="tabpanel" aria-labelledby="tab-transaksi-content">
                                        <form id="form-transaksi">
                                            <div class="card">
                                                <!-- <div class="card-header" style="background-color: #8E44AD; color: white"> -->
                                                <div class="card-header">
                                                    <b style="font-size: 18px">Data Transaksi</b>
                                                </div>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Status Tranc</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Status Card</label>
                                                                <select class="form-control select2">
                                                                <option value="">[Choose Status Card]</option>
                                                                    @foreach($mcard as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Kode</label>
                                                                <input type="text" name="kode" class="form-control" placeholder="Kode">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Kode Reff</label>
                                                                <input type="text" name="kodereff" class="form-control" placeholder="Kode Reff">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Tanggal</label>
                                                                <input type="text" class="form-control datepicker" placeholder="Tanggal" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Tanggal Tax</label>
                                                                <input type="text" name="tanggaltax" class="form-control datepicker" placeholder="Tanggal Tax" autocomplete="off">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Tanggal Due</label>
                                                                <input type="text" class="form-control datepicker" placeholder="Tanggal Due" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <!-- <div class="form-group">
                                                                <label>Tanggal Tax</label>
                                                                <input type="text" class="form-control datepicker" placeholder="Tanggal Tax">
                                                            </div> -->
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Keterangan</label>
                                                                <textarea name="keterangan" id="" cols="3" rows="5" class="form-control" placeholder="Keterangan"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>ID Currency</label>
                                                                <select class="form-control select2">
                                                                    @foreach($accmcurrency as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Konv Curr</label>
                                                                <input type="text" name="konvcurr" class="form-control" placeholder="Konv Curr">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>ID Gudang</label>
                                                                <select class="form-control select2">
                                                                    @foreach($genmgudang as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->namalias}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>ID Company</label>
                                                                <!-- <select class="form-control select2" name="idcompany">
                                                                    @foreach($genmcompany as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                    @endforeach
                                                                </select> -->
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>ID Department</label>
                                                                <!-- <select class="form-control select2" name="iddepartment">
                                                                    @foreach($genmdepartment as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                    @endforeach
                                                                </select> -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>ID Type Tranc Prev</label>
                                                                <select class="form-control select2" name="idtypetrancprev">
                                                                    @foreach($mtypetranc as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>ID Tranc Prev</label>
                                                                <input type="text" name="idtrancprev" class="form-control" placeholder="ID Tranc Prev">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>ID Type Tranc Order</label>
                                                                <select class="form-control select2" name="idtypetrancorder">
                                                                    @foreach($mtypetranc as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>ID Tranc Order</label>
                                                                <input type="text" name="idtrancorder" class="form-control" placeholder="ID Tranc Order">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane fade" id="tab-supplier-content" role="tabpanel" aria-labelledby="tab-supplier-content">
                                        
                                    </div>
                                    <div class="tab-pane fade" id="tab-delivery-content" role="tabpanel" aria-labelledby="tab-delivery-content">
                                        <form id="form-delivery">
                                            <div class="card">
                                                <!-- <div class="card-header" style="background-color: #32C5D2; color: white"> -->
                                                <div class="card-header">
                                                    <b style="font-size: 18px">Data Delivery</b>
                                                </div>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label>Card Delivery</label>
                                                                <select class="form-control select2" name="idcarddelivery">
                                                                    @foreach($mcard as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Delivery Name</label>
                                                                <input type="text" name="deliveryname" class="form-control" placeholder="Delivery Name">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Delivery Tanggal</label>
                                                                <input type="text" name="deliverytanggal" class="form-control" placeholder="Delivery Tanggal">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Delivery Vehicle</label>
                                                                <input type="text" name="deliveryvehicle" class="form-control" placeholder="Delivery Vehicle">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Delivery Vehicle Plat</label>
                                                                <input type="text" name="deliveryvehicleplat" class="form-control" placeholder="Delivery Vehicle Plat">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane fade" id="tab-akun-content" role="tabpanel" aria-labelledby="tab-akun-content">
                                        <form id="form-akun">
                                            <div class="card">
                                                <!-- <div class="card-header" style="background-color: #C49F47; color: white"> -->
                                                <div class="card-header">
                                                    <b style="font-size: 18px">Data Akun</b>
                                                </div>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Akun Bayar</label>
                                                                <select class="form-control select2" name="idakunbayar">
                                                                    @foreach($accmcashbank as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Akun Deposit</label>
                                                                <select class="form-control select2" name="idakundeposit">
                                                                    @foreach($accmcashbank as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Akun Biaya Lain</label>
                                                                <select class="form-control select2" name="idakunbiayalain">
                                                                    @foreach($accmcashbank as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Akun Hutang</label>
                                                                <select class="form-control select2" name="idakunhutang">
                                                                    @foreach($accmcashbank as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Akun Purchase</label>
                                                                <select class="form-control select2" name="idakunpurchase">
                                                                    @foreach($accmcashbank as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Akun Persediaan</label>
                                                                <select class="form-control select2" name="idakunpersediaan">
                                                                    @foreach($accmcashbank as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Akun Sales Return</label>
                                                                <select class="form-control select2" name="idakunsalesreturn">
                                                                    @foreach($accmcashbank as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Akun Discount</label>
                                                                <select class="form-control select2" name="idakundiscount">
                                                                    @foreach($accmcashbank as $v)
                                                                        <option value="{{$v->noid}}">{{$v->kode}} - {{$v->nama}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="row" style="margin-top: 20px">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body" style="display: none">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>ID Create</label>
                                                    <select class="form-control" disabled>
                                                        @foreach($mcard as $v)
                                                            <option value="{{$v->noid}}" <?= $v->noid == $sessionnoid ? 'selected' : '' ?>>{{$v->kode}} - {{$v->nama}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Do Create</label>
                                                    <input type="text" class="form-control" value="<?= date('d-M-Y H:i:s')?>" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>ID Update</label>
                                                    <select class="form-control" disabled>
                                                        @foreach($mcard as $v)
                                                            <option value="{{$v->noid}}" <?= $v->noid == $sessionnoid ? 'selected' : '' ?>>{{$v->kode}} - {{$v->nama}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Last Update</label>
                                                    <input type="text" class="form-control" value="<?= date('d-M-Y H:i:s')?>" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    <!-- </form> -->
                </div>
                <div id="tabledepartemen" class="table table-bordered " style="width: 100%; border: 0px; margin: -10px 0px -10px 0px !important">
                    <!-- <div id="divsucces" class="alert alert-custom alert-light-success fade show mb-5" role="alert">
                        <p id="notiftext"></p>
                    </div> -->
                <!-- <table id="tabletest" class="nowrap" style="width 100px; table-layout: auto">
                    <thead>
                        <tr>
                            <th style="width: 10% !important">Halaman 1</th>
                            <th style="width: 10% !important">Halaman 2</th>
                            <th style="width: 10% !important">Halaman 3</th>
                            <th style="width: 10% !important">Halaman 4</th>
                        </tr>
                    </thead>
                </table> -->
                <!-- <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12 col-xs-12 dtfiler">
                            <div class="col-md-4" id="table_pagepanel_9921050101_paginate">
                                <div class="pagination-panel row">
                                    Page
                                    <a href="#" class="btn btn-sm default prev disabled" title="<i class='fa fa-angle-left'></i>">
                                        <i class="fa fa-angle-left"></i>
                                    </a>
                                    <input type="text" class="pagination-panel-input form-control input-mini input-inline input-sm" maxlenght="5" style="text-align:center; margin: 0 5px;">
                                    <a href="#" class="btn btn-sm default next disabled" title="<i class='fa fa-angle-right'> </i>">
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                     of 
                                    <span class="pagination-panel-total">1</span>
                                </div>
                            </div>
                            <div class="col-md-4" id="table_pagepanel_9921050101_length">
                                <label class="">
                                    <span class="seperator">|</span>
                                    Rec/Page 
                                    <select name="table_pagepanel_9921050101_length" aria-controls="table_pagepanel_9921050101" class="form-control input-inline input-xsmall input-sm recofdt">
                                        <option value="10">10</option>
                                        <option value="50">50</option>
                                        <option value="150">150</option>
                                        <option value="1000">All</option>
                                    </select>
                                </label>
                            </div>
                            <div class="col-md-4" id="table_pagepanel_9921050101_info" role="status" aria-live="polite">
                                <span class="seperator">|</span>Total 6 records 
                            </div>
                        </div>
                        <!-- <div class="dtfilter_search">
                            <div id="table_pagepanel_9921050101_filter" class="dataTables_filter">
                                <label>
                                    <input type="search" class="form-control input-inline input-sm" placeholder="" aria-controls="table_pagepanel_9921050101">
                                </label>
                            </div>
                            <div class="btn fa fa-search dtfilter_search_btn">
                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="row" style="margin-top: 10px">
                    <!-- <div class="col"> -->
                        <div id="space_filter_button" style="width: 100%">
                            <div class="row">
                                <div class="btn-group">
                                    <button type="button" id="dropdown-date-filter" class="btn btn-sm dropdown-toggle text-right" style="height: 31px; background-color: #a1b1bc; color: white; margin-right: 0px;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="icon-calendar pull-left" style="margin-top: 2px; color: white"></i>
                                        <span>{{$datefilter['tm']}}</span>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" style="width: 215px">
                                        <a id="did-a" data-date="{{$datefilter['a']}}" class="dropdown-item dropdown-item-date" style="padding-left: 10px; padding-right: 10px;" onclick="filterDate('a')" onmouseover="hoverFilterDate('a')" onmouseout="hoverOutFilterDate('a')">
                                            <span style="background-color: {{ $datefilterdefault == 'a' ? '#4b8df8' : '#f5f5f5' }}; width: 100%; padding: 2px 5px 2px 5px; color: {{ $datefilterdefault == 'a' ? 'white' : '#08c' }}">All</span>
                                        </a>
                                        <a id="did-t"data-date="{{$datefilter['t']}}" class="dropdown-item dropdown-item-date" style="padding-left: 10px; padding-right: 10px;" onclick="filterDate('t')" onmouseover="hoverFilterDate('t')" onmouseout="hoverOutFilterDate('t')">
                                            <span style="background-color: {{ $datefilterdefault == 't' ? '#4b8df8' : '#f5f5f5' }}; width: 100%; padding: 2px 5px 2px 5px; color: {{ $datefilterdefault == 't' ? 'white' : '#08c' }}">Today</span>
                                        </a>
                                        <a id="did-y" data-date="{{$datefilter['y']}}" class="dropdown-item dropdown-item-date" style="padding-left: 10px; padding-right: 10px;" onclick="filterDate('y')" onmouseover="hoverFilterDate('y')" onmouseout="hoverOutFilterDate('y')">
                                            <span style="background-color: {{ $datefilterdefault == 'y' ? '#4b8df8' : '#f5f5f5' }}; width: 100%; padding: 2px 5px 2px 5px; color: {{ $datefilterdefault == 'y' ? 'white' : '#08c' }};">Yesterday</span>
                                        </a>
                                        <a id="did-l7d" data-date="{{$datefilter['l7d']}}" class="dropdown-item dropdown-item-date" style="padding-left: 10px; padding-right: 10px;" onclick="filterDate('l7d')" onmouseover="hoverFilterDate('l7d')" onmouseout="hoverOutFilterDate('l7d')">
                                            <span style="background-color: {{ $datefilterdefault == 'l7d' ? '#4b8df8' : '#f5f5f5' }}; width: 100%; padding: 2px 5px 2px 5px; color: {{ $datefilterdefault == 'l7d' ? 'white' : '#08c' }};">Last 7 Days</span>
                                        </a>
                                        <a id="did-l30d" data-date="{{$datefilter['l30d']}}" class="dropdown-item dropdown-item-date" style="padding-left: 10px; padding-right: 10px;" onclick="filterDate('l30d')" onmouseover="hoverFilterDate('l30d')" onmouseout="hoverOutFilterDate('30d')">
                                            <span style="background-color: {{ $datefilterdefault == 'l30d' ? '#4b8df8' : '#f5f5f5' }}; width: 100%; padding: 2px 5px 2px 5px; color: {{ $datefilterdefault == 'l30d' ? 'white' : '#08c' }};">Last 30 Days</span>
                                        </a>
                                        <a id="did-tm" data-date="{{$datefilter['tm']}}" class="dropdown-item dropdown-item-date" style="padding-left: 10px; padding-right: 10px;" onclick="filterDate('tm')" onmouseover="hoverFilterDate('tm')" onmouseout="hoverOutFilterDate('tm')">
                                            <span style="background-color: {{ $datefilterdefault == 'tm' ? '#4b8df8' : '#f5f5f5' }}; width: 100%; padding: 2px 5px 2px 5px; color: {{ $datefilterdefault == 'tm' ? 'white' : '#08c' }};">This Month</span>
                                        </a>
                                        <a id="did-lm" data-date="{{$datefilter['lm']}}" class="dropdown-item dropdown-item-date" style="padding-left: 10px; padding-right: 10px;" onclick="filterDate('lm')" onmouseover="hoverFilterDate('lm')" onmouseout="hoverOutFilterDate('lm')">
                                            <span style="background-color: {{ $datefilterdefault == 'lm' ? '#4b8df8' : '#f5f5f5' }}; width: 100%; padding: 2px 5px 2px 5px; color: {{ $datefilterdefault == 'lm' ? 'white' : '#08c' }};">Last Month</span>
                                        </a>
                                        <a id="did-cr" class="dropdown-item dropdown-item-date" style="padding-left: 10px; padding-right: 10px;" onclick="filterDate('cr')" onmouseover="hoverFilterDate('cr')" onmouseout="hoverOutFilterDate('cr')" disabled>
                                            <span style="background-color: {{ $datefilterdefault == 'cr' ? '#4b8df8' : '#f5f5f5' }}; width: 100%; padding: 2px 5px 2px 5px; color: {{ $datefilterdefault == 'cr' ? 'white' : '#08c' }};">Custom Range</span>
                                        </a>
                                        <div class="row" style="padding-left: 5px; padding-right: 5px;">
                                            <div class="col-md-6">
                                                <label>From</label>
                                                <input type="text" name="did-from" value="{{date('d-m-Y', mktime (0, 0, 0, date('m'), 1, date('Y')))}}" id="did-from" class="form-control datepicker2" onchange="changeCustomRange()" style="font-size: 12px" placeholder="From">
                                            </div>
                                            <div class="col-md-6">
                                                <label>To</label>
                                                <input type="text" name="did-to" value="{{date('d-m-Y', mktime (0, 0, 0, date('m'), date('t'), date('Y')))}}" id="did-to" class="form-control datepicker2" onchange="changeCustomRange()" style="font-size: 12px" placeholder="To">
                                            </div>
                                        </div>
                                        <div class="row" style="padding-left: 5px; padding-right: 5px; margin-top: 8px; margin-bottom: 2px">
                                            <div class="col-md-12">
                                                <button class="btn btn-success" onclick="filterDate('cr')">Apply</button>
                                                <div class="btn btn-default">Cancel</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="btn" style="padding: 0px; width: 5px"></div>
                                @foreach($mtypetranctypestatus as $k => $v)
                                    @if($k == 0)
                                        <button class="btn btn-outline-secondary btn-sm flat btn-filter-all btn-filter-idstatustranc" id="btn-filter-idstatustranc-all" style="background: rgb(144, 202, 249); color: black; font-weight: bold; margin-right: 0px; margin-bottom: 5px; height: 31px;" onclick="filterData('btn-filter-idstatustranc-all', 'btn-filter-idstatustranc', 1, 'ALL', 'grey', 0, true, '', '', 'idstatustranc')">ALL</button>
                                    @else
                                        <button class="btn btn-outline-secondary btn-sm flat btn-filter-idstatustranc" id="btn-filter-idstatustranc-{{$v->noid}}" style="background-color:#E3F2FD; color: grey; margin-right: 0px; margin-bottom: 5px; height: 31px;" onclick="filterData('btn-filter-idstatustranc-{{$v->noid}}', 'btn-filter-idstatustranc', 1, 'DRAFT', 'grey', -1, false, 'fincashbank', 'AND', 'idstatustranc', '=', '{{$v->noid}}')">{{$v->nama}}</button>
                                    @endif
                                @endforeach

                                <div class="btn" style="padding: 0px; width: 5px"></div>
                                @foreach($mtypecostcontrol as $k => $v)
                                    @if($k == 0)
                                        <button class="btn btn-outline-secondary btn-sm flat btn-filter-all btn-filter-idtypecostcontrol" id="btn-filter-idtypecostcontrol-all" style="background: rgb(144, 202, 249); color: black; font-weight: bold; margin-right: 0px; margin-bottom: 5px; height: 31px;" onclick="filterData('btn-filter-idtypecostcontrol-all', 'btn-filter-idtypecostcontrol', 1, 'ALL', 'grey', 0, true, '', '', 'idtypecostcontrol')">ALL</button>
                                    @else
                                        <button class="btn btn-outline-secondary btn-sm flat btn-filter-idtypecostcontrol" id="btn-filter-idtypecostcontrol-{{$v->noid}}" style="background-color:#E3F2FD; color: grey; margin-right: 0px; margin-bottom: 5px; height: 31px;" onclick="filterData('btn-filter-idtypecostcontrol-{{$v->noid}}', 'btn-filter-idtypecostcontrol', 1, 'DRAFT', 'grey', -1, false, 'fincashbank', 'AND', 'idtypecostcontrol', '=', '{{$v->noid}}')">{{$v->nama}}</button>
                                    @endif
                                @endforeach

                                <div class="btn" style="padding: 0px; width: 5px"></div>
                                @foreach($genmcompany as $k => $v)
                                    @if($k == 0)
                                        <button class="btn btn-outline-secondary btn-sm flat btn-filter-all btn-filter-idcompany" id="btn-filter-idcompany-all" style="background: rgb(144, 202, 249); color: black; font-weight: bold; margin-right: 0px; margin-bottom: 5px; height: 31px;" onclick="filterData('btn-filter-idcompany-all', 'btn-filter-idcompany', 1, 'ALL', 'grey', 0, true, '', '', 'idcompany')">ALL</button>
                                    @else
                                        <button class="btn btn-outline-secondary btn-sm flat btn-filter-idcompany" id="btn-filter-idcompany-{{$v->noid}}" style="background-color:#E3F2FD; color: grey; margin-right: 0px; margin-bottom: 5px; height: 31px;" onclick="filterData('btn-filter-idcompany-{{$v->noid}}', 'btn-filter-idcompany', 1, 'DRAFT', 'grey', -1, false, 'fincashbank', 'AND', 'idcompany', '=', '{{$v->noid}}')">{{$v->nama}}</button>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                            <!-- <button class="btn btn-outline-secondary btn-sm flat btn-filter-1-9921050101" id="btn-filter-NEEDAPPROVED-9921050101" style="background-color:#E3F2FD; color: grey; margin-right: 0px" onclick="filterData('btn-filter-NEEDAPPROVED-9921050101', 'btn-filter-1-9921050101', 1, 'NEED APPROVED', 'grey', -1, false, 'fincashbank', 'AND', 'idstatustranc', '=', '2')">NEED APPROVED </button><button class="btn btn-outline-secondary btn-sm flat btn-filter-1-9921050101" id="btn-filter-APPROVED-9921050101" style="background-color:#E3F2FD; color: grey; margin-right: 0px" onclick="filterData('btn-filter-APPROVED-9921050101', 'btn-filter-1-9921050101', 1, 'APPROVED', 'grey', -1, false, 'fincashbank', 'AND', 'idstatustranc', '=', '3')">APPROVED </button><div class="btn" style="padding: 0px; width: 5px"></div></div> -->
                    <!-- </div> -->
                </div>
                <div id="appendInfoDatatable" style="border-top: 0px solid #3598dc; border-right: 0px solid #3598dc; border-left: 0px solid #3598dc;">
                </div>
                <!-- <div id="appendButtonDatatable"></div> -->
                <div class="table-scrollable" style="border: 0px solid #3598dc; margin-top: 0px !important">
                    <table bg-color="blue" class="nowrap table table-striped table-bordered table-hover  dataTable no-footer" style="background-color:#BBDEFB !important; margin: 0 !important; border: 0px solid #3598dc" id="table_pagepanel_9921050101"  aria-describedby="table_pagepanel_9921050101_info" role="grid">
                        <thead id="thead_pagepanel_9921050101">
                            <tr id="thead-columns-d" style="background: #BBDEFC">
                                <th style="width: 30px;">
                                    <input type="checkbox" id="checkbox-m-all" onchange="selectCheckboxMAll()" class="checkbox-m" name="checkbox-m-all" value="FALSE">
                                </th>
                                <th class="text-center">No</th>
                                <!-- <th>Noid</th> -->
                                <!-- <th>Type Tranc.</th> -->
                                <th class="text-center">Status</th>
                                <!-- <th>ID Status Card</th> -->
                                <!-- <th>Do Status</th> -->
                                <th class="text-center">Kode</th>
                                <!-- <th>Kode Reff.</th> -->
                                <!-- <th>Tanggal</th> -->
                                <th class="text-center">Tanggal Due</th>
                                <!-- <th>Sub Total</th>
                                <th>General Total</th> -->
                                <!-- <th>Keterangan</th> -->
                                <!-- <th>Project</th> -->
                                <!-- <th>Supplier</th> -->
                                <!-- <th>Company</th>
                                <th>Department</th> -->
                                <!-- <th>Gudang</th> -->
                                <th class="text-center">Project Koor.</th>
                                <th class="text-center">Project Manager</th>
                                <!-- <th>Admin Site Project</th> -->
                                <!-- <th>Drafter</th> -->
                                <th class="text-center">Request By</th>
                                <th class="text-center">Action</th>
                                <!-- <th>ID Card Supplier</th>
                                <th>ID Akun Supplier</th>
                                <th>Supplier Nama</th>
                                <th>Supplier Address 1</th>
                                <th>Supplier Address 2</th>
                                <th>Supplier Address RT</th>
                                <th>Supplier Address RW</th>
                                <th>Supplier Lokasi Neg</th>
                                <th>Supplier Lokasi Prop</th>
                                <th>Supplier Lokasi Kab</th>
                                <th>Supplier Lokasi Kec</th>
                                <th>Supplier Lokasi Kel</th>
                                <th>ID Card Delivery</th>
                                <th>Delivery Name</th>
                                <th>Delivery Vehicle</th>
                                <th>Delivery Vehicle Plat</th>
                                <th>Delivery Tanggal</th>
                                <th>Total QTY</th>
                                <th>Total Item</th>
                                <th>Total Subtotal</th>
                                <th>Total Disc</th>
                                <th>Total Tax</th>
                                <th>Disc Nota</th>
                                <th>Total Disc Nota</th>
                                <th>General Price</th>
                                <th>Biaya Lain</th>
                                <th>Kode Voucher</th>
                                <th>General Total</th>
                                <th>Total Bayar</th>
                                <th>Total Deposit</th>
                                <th>Total Hutang</th>
                                <th>Total Sisa Hutang</th>
                                <th>ID Type Payment</th>
                                <th>ID Cashbank Bayar</th>
                                <th>ID Akun Bayar</th>
                                <th>ID Akun Deposit</th>
                                <th>ID Akun Biaya Lain</th>
                                <th>ID Akun Hutang</th>
                                <th>ID Akun Purchase</th>
                                <th>ID Akun Persediaan</th>
                                <th>ID Akun Sales Return</th>
                                <th>ID Akun Discount</th>
                                <th>Keterangan</th>
                                <th>ID Currency</th>
                                <th>Konv Curr</th>
                                <th>ID Gudang</th>
                                <th>ID Cost Center</th>
                                <th>No Urut</th>
                                <th>ID Company</th>
                                <th>ID Department</th>
                                <th>ID Type Tranc Prev</th>
                                <th>ID Tranc Prev</th>
                                <th>ID Type Tranc Order</th>
                                <th>ID Tranc Order</th>
                                <th>ID Create</th>
                                <th>Do Create</th>
                                <th>ID Update</th>
                                <th>Last Update</th> -->
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th class="tfoot-kode"></th>
                                <th></th>
                                <!-- <th></th> -->
                                <!-- <th></th>
                                <th></th> -->
                                <!-- <th></th>
                                <th></th> -->
                                <th></th>
                                <!-- <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th> -->
                                <!-- <th class="text-right" colspan="2" style="border: 1px solid #95c9ed"></th> -->
                            </tr>
                        </tfoot>
                    </table>
                </div>

                <!-- <div class="table-scrollable" style="border: 1px solid #3598dc;">
                    <table id="example_baru" class="table table-striped table-bordered table-hover nowrap dataTable no-footer" style="background-color: #BBDEFB; border-collapse: collapse;">
                        <thead>
                            <tr id="thead" class="bg-main"></tr>
                            <tr id="thead-columns"></tr>
                        </thead>
                        <tfoot>
                            <tr id="tfoot-columns">
                            </tr>
                        </tfoot>
                    </table>
                </div> -->
            </div>
        </div>
    </div>

    <br>
    <!-- <div class="card card-custom flat">
        <div class="card-body flat">
            <div class="row" id="another-form">
            </div>
        </div>
    </div> -->

    <div id="modal-delete" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-filter_ modal-dialog modal-lg">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="hideModalDelete()"></a>
                    <h4 class="modal-title" style="padding-left: 12px">Are you sure you want to delete this?</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-top: 8px">
                        <div class="col-md-12" style="padding: 0px">
                            <div class="card" style="margin: 0px 0px 5px 1px !important">
                                <div class="card-body">
                                    <form id="">
                                        <div class="row text-center">
                                            <div class="col-md-12">
                                                <button type="button" id="remove-data" class="btn btn-success" style="margin-left: 2px"><i class="fa fa-check"></i> Yes</button>
                                                <button type="button" class="btn btn-default" style="margin-left: 2px" onclick="hideModalDelete()"><i class="fa fa-times"></i> No</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="modal-send-to-next" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-filter_ modal-dialog modal-lg">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="hideModalSendToOrder()"></a>
                    <h4 class="modal-title" style="padding-left: 12px">Send to Purchase Offer</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-top: 8px">
                        <div class="col-md-12" style="padding: 0px">
                            <div class="card" style="margin: 0px 0px 5px 1px !important">
                                <div class="card-body">
                                    <form id="form-modal-stn">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">User</label>
                                                    <div class="col-md-8">
                                                        <input type="hidden" id="idtypetranc-stn">
                                                        <input type="text" class="form-control" value="{{$sessionname}}" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Supplier</label>
                                                    <div class="col-md-8">
                                                        <select class="form-control select2" name="idcardsupplier-stn" id="idcardsupplier-stn" onchange="changeCardSupplier('idcardsupplier', 'idcardsupplier-modal')">
                                                            <option value="">[Choose Supplier]</option>
                                                            @foreach($mcardsupplier as $v)
                                                                <option 
                                                                    value="{{$v->mcardsupplier_noid}}" 
                                                                    data-mlokasiprop-noid="{{$v->mlokasiprop_noid}}" 
                                                                    data-mlokasiprop-nama="{{$v->mlokasiprop_nama}}" 
                                                                    data-mlokasikab-noid="{{$v->mlokasikab_noid}}" 
                                                                    data-mlokasikab-nama="{{$v->mlokasikab_nama}}" 
                                                                    data-mlokasikec-noid="{{$v->mlokasikec_noid}}" 
                                                                    data-mlokasikec-nama="{{$v->mlokasikec_nama}}" 
                                                                    data-mlokasikel-noid="{{$v->mlokasikel_noid}}" 
                                                                    data-mlokasikel-nama="{{$v->mlokasikel_nama}}" 
                                                                >{{$v->mcardsupplier_nama}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4"></div>
                                            <div class="col-md-8">
                                                <button type="button" class="btn btn-success" id="send-to-next" style="margin-left: 2px"><i class="fa fa-save"></i> <span>Send</span></button>
                                                <button type="button" class="btn btn-default" style="margin-left: 2px" onclick="hideModalSendToOrder()"><i class="fa fa-reply"></i> <span>Cancel</span></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-snst" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-filter_ modal-dialog modal-lg">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="hideModalSNST()"></a>
                    <h4 class="modal-title" style="padding-left: 12px">SET NEXT STATUS TRANC</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-top: 8px">
                        <div class="col-md-12" style="padding: 0px">
                            <div class="card" style="margin: 0px 0px 5px 1px !important">
                                <div class="card-body">
                                    <form id="form-modal-snst">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">User</label>
                                                    <div class="col-md-8">
                                                        <input type="hidden" id="idtypetranc-snst">
                                                        <input type="text" class="form-control" value="{{$sessionname}}" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Keterangan</label>
                                                    <div class="col-md-8">
                                                        <textarea name="keterangan-modal" id="keterangan-snst" cols="3" rows="5" class="form-control" placeholder="Keterangan"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4"></div>
                                            <div class="col-md-8">
                                                <button type="button" class="btn btn-success" id="btn-snst" style="margin-left: 2px"><i class="fa fa-save"></i> <span>Set Next</span></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modal-div" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-filter_ modal-dialog modal-lg">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="hideModalSAPCF()"></a>
                    <h4 class="modal-title" style="padding-left: 12px">SET STATUS TRANC</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-top: 8px">
                        <div class="col-md-12" style="padding: 0px">
                            <div class="card" style="margin: 0px 0px 5px 1px !important">
                                <div class="card-body">
                                    <form id="form-modal-div">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">User</label>
                                                    <div class="col-md-8">
                                                        <input type="hidden" id="idtypetranc-div">
                                                        <input type="text" class="form-control" value="{{$sessionname}}" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Status Tranc.</label>
                                                    <div class="col-md-8" id="select-div">
                                                        <select class="form-control select2" name="idstatustranc-div" id="idstatustranc-div">
                                                            <option value="">[Choose Status]</option>
                                                            @foreach($mtypetranctypestatus as $v)
                                                                @if($v->noid > 0 && $v->noid <= 3)
                                                                    <option 
                                                                        value="{{$v->noid}}"
                                                                        data-mtypetranctypestatus-nama="{{$v->nama}}"
                                                                    >{{$v->nama}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Keterangan</label>
                                                    <div class="col-md-8">
                                                        <textarea name="keterangan-div" id="keterangan-div" cols="3" rows="5" class="form-control" placeholder="Keterangan"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4"></div>
                                            <div class="col-md-8">
                                                <button type="button" class="btn btn-success" id="btn-div" style="margin-left: 2px"><i class="fa fa-save"></i> <span>Set Status</span></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-sapcf" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-filter_ modal-dialog modal-lg">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="hideModalSAPCF()"></a>
                    <h4 class="modal-title" style="padding-left: 12px">SET STATUS TRANC</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-top: 8px">
                        <div class="col-md-12" style="padding: 0px">
                            <div class="card" style="margin: 0px 0px 5px 1px !important">
                                <div class="card-body">
                                    <form id="form-modal-sapcf">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">User</label>
                                                    <div class="col-md-8">
                                                        <input type="hidden" id="idtypetranc-sapcf">
                                                        <input type="text" class="form-control" value="{{$sessionname}}" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Status Tranc.</label>
                                                    <div class="col-md-8">
                                                        <select class="form-control select2" name="idstatustranc-sapcf" id="idstatustranc-sapcf">
                                                            <option value="">[Choose Status]</option>
                                                            @foreach($mtypetranctypestatus as $v)
                                                                @if($v->isinternal == 0 && $v->isexternal != 0)
                                                                    <option 
                                                                        value="{{$v->noid}}"
                                                                        data-mtypetranctypestatus-nama="{{$v->nama}}"
                                                                    >{{$v->nama}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Keterangan</label>
                                                    <div class="col-md-8">
                                                        <textarea name="keterangan-sapcf" id="keterangan-sapcf" cols="3" rows="5" class="form-control" placeholder="Keterangan"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4"></div>
                                            <div class="col-md-8">
                                                <button type="button" class="btn btn-success" id="btn-sapcf" style="margin-left: 2px"><i class="fa fa-save"></i> <span>Set Status</span></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-set-pending" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-filter_ modal-dialog modal-lg">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="closeModalFilter()"></a>
                    <h4 class="modal-title" style="padding-left: 12px">SET PENDING</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-top: 8px">
                        <div class="col-md-12" style="padding: 0px">
                            <div class="card" style="margin: 0px 0px 5px 1px !important">
                                <div class="card-body">
                                    <form id="form-modal-set-pending">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">User</label>
                                                    <div class="col-md-8">
                                                        <input type="hidden" id="idtypetranc-pending">
                                                        <input type="text" class="form-control" value="{{$sessionname}}" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Keterangan</label>
                                                    <div class="col-md-8">
                                                        <textarea name="keterangan-modal" id="keterangan-pending" cols="3" rows="5" class="form-control" placeholder="Keterangan"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4"></div>
                                            <div class="col-md-8">
                                                <button type="button" class="btn btn-success" id="set-pending" style="margin-left: 2px"><i class="fa fa-save"></i> Set Pending</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-set-canceled" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-filter_ modal-dialog modal-lg">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="closeModalFilter()"></a>
                    <h4 class="modal-title" style="padding-left: 12px">SET CANCELED</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-top: 8px">
                        <div class="col-md-12" style="padding: 0px">
                            <div class="card" style="margin: 0px 0px 5px 1px !important">
                                <div class="card-body">
                                    <form id="form-modal-set-canceled">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">User</label>
                                                    <div class="col-md-8">
                                                        <input type="hidden" id="idtypetranc-canceled">
                                                        <input type="text" class="form-control" value="{{$sessionname}}" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Keterangan</label>
                                                    <div class="col-md-8">
                                                        <textarea name="keterangan-modal" id="keterangan-canceled" cols="3" rows="5" class="form-control" placeholder="Keterangan"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4"></div>
                                            <div class="col-md-8">
                                                <button type="button" class="btn btn-success" id="set-canceled" style="margin-left: 2px"><i class="fa fa-save"></i> Set Canceled</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-log" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-filter modal-dialog modal-lg">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="hideModalLog()"></a>
                    <h4 class="modal-title" style="padding-left: 12px">Log: <span id="titlelog-log"></span>, Kode: <span id="titlekode-log"></span></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-top: 8px">
                        <div class="col-md-12" style="padding: 0px">
                            <div class="card" style="margin: 0px 0px 5px 1px !important">
                                <div class="card-body">
                                    <div id="appendInfoDatatableLog" style="border-top: 0px solid #3598dc; border-right: 0px solid #3598dc; border-left: 0px solid #3598dc;">
                                    </div>
                                    <div id="ts-9921050102" class="table-scrollable" style="border: 0px solid #3598dc; background-color: #3598dc; margin-top: 0px !important">
                                        <table bg-color="blue" class="nowrap table table-striped table-bordered table-hover  dataTable no-footer" id="table-log" style="background-color:#BBDEFB !important; margin: 0 !important; border: 0px solid #3598dc"  aria-describedby="table_pagepanel_9921050101_info" role="grid">
                                            <thead>
                                                <!-- <tr id="tab-thead-9921050102" class="bg-main"><th style="text-align:center" colspan="2">#</th><th style="text-align:center" colspan="26"></th><th colspan="1" style="text-align:center">---</th></tr> -->
                                                <tr id="tab-thead-columns-d-9921050102">
                                                    <th>---</th>
                                                    <th>No</th>
                                                    <th>Type Action</th>
                                                    <th>Log Subject</th>
                                                    <th>Keterangan</th>
                                                    <th>By</th>
                                                    <th>On</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-costcontrol" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-filter modal-dialog modal-lg">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="hideModalCostControl()"></a>
                    <h4 class="modal-title" style="padding-left: 12px">COST CONTROL</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-top: 8px">
                        <div class="col-md-12" style="padding: 0px">
                            <div class="card" style="margin: 0px 0px 5px 1px !important">
                                <div class="card-body">
                                    <div id="appendInfoDatatableCostControl" style="border-top: 2px solid #3598dc; border-right: 3px solid #3598dc; border-left: 2px solid #3598dc;">
                                    </div>
                                    <div id="ts-9921050102" class="table-scrollable" style="border: 1px solid #3598dc; background-color: #3598dc; margin-top: 0px !important">
                                        <table bg-color="blue" class="nowrap table table-striped table-bordered table-hover  dataTable no-footer" id="table-costcontrol" style="background-color:#BBDEFB !important; margin: 0 !important; border: 1px solid #3598dc"  aria-describedby="table_pagepanel_9921050101_info" role="grid">
                                            <thead>
                                                <tr id="tab-thead-columns-d-9921050102">
                                                    <th style="width: 30px;">
                                                        <input type="checkbox" id="checkbox-cc-all" onchange="selectAllCheckboxCC()" class="checkbox-cc" name="checkbox-cc-all" value="FALSE">
                                                    </th>
                                                    <th>No</th>
                                                    <!-- <th>Type Tranc</th>
                                                    <th>Status Tranc</th> -->
                                                    <th>Kode</th>
                                                    <!-- <th>Kode Reff</th> -->
                                                    <th>Nama Pekerjaan</th>
                                                    <th>Lokasi Pekerjaan</th>
                                                    <th>Tanggal</th>
                                                    <th>Project Manager</th>
                                                    <th>Project Koor.</th>
                                                    <th>Admin Site Project</th>
                                                    <th>Drafter</th>
                                                    <!-- <th>Type Cost Center</th>
                                                    <th>General Total</th>
                                                    <th>Keterangan</th>
                                                    <th>Customer</th>
                                                    <th>Company</th>
                                                    <th>Department</th> -->
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th class="text-right" colspan="11" style="border: 1px solid #95c9ed"></th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-detailprev" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-filter modal-dialog modal-lg">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="hideModalDetailPrev()"></a>
                    <h4 class="modal-title" id="title-prev" style="padding-left: 12px">PREVIEOUS: PURCHASE REQUEST DETAIL, SUPPLIER: <span class="supplier-title"></span></h4>
                    <h4 class="modal-title" id="title-inv" style="padding-left: 12px">INVENTORY</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-top: 8px">
                        <div class="col-md-12" style="padding: 0px">
                            <div class="card" style="margin: 0px 0px 5px 1px !important">
                                <div class="card-body">
                                    <div id="appendInfoDatatableDetailPrev" style="width: 100%; border-top: 2px solid #3598dc; border-right: 3px solid #3598dc; border-left: 2px solid #3598dc;">
                                    </div>
                                    <div id="ts-9921050102" class="table-scrollable" style="border: 1px solid #3598dc; background-color: #3598dc; margin-top: 0px !important">
                                        <table bg-color="blue" class="nowrap table table-striped table-bordered table-hover  dataTable no-footer" id="table-detailprev" style="background-color:#BBDEFB !important; margin: 0 !important; border: 1px solid #3598dc"  aria-describedby="table_pagepanel_9921050101_info" role="grid">
                                            <thead>
                                                <!-- <tr id="tab-thead-9921050102" class="bg-main"><th style="text-align:center" colspan="2">#</th><th style="text-align:center" colspan="26"></th><th colspan="1" style="text-align:center">---</th></tr> -->
                                                <tr id="tab-thead-columns-d-9921050102">
                                                    <th style="width: 30px;">
                                                        <input type="checkbox" id="select_all_9921050102" onchange="selectAllCheckboxTab(9921050102)" class="checkbox-9921050102 checkbox-9921050102-0" name="checkbox-9921050102[]" value="FALSE">
                                                    </th>
                                                    <th>No</th>
                                                    <th>Kode PR</th>
                                                    <th>Kode Inv.</th>
                                                    <th>Nama</th>
                                                    <th>Inventor</th>
                                                    <th>Keterangan</th>
                                                    <!-- <th>ID Gudang</th>
                                                    <th>ID Company</th>
                                                    <th>ID Department</th>
                                                    <th>ID Satuan</th>
                                                    <th>Konv Satuan</th> -->
                                                    <th id="thead-qty">Qty</th>
                                                    <th id="thead-qtysisa">Qty Sisa</th>
                                                    <th>Unit</th>
                                                    <th id="thead-price">Price</th>
                                                    <th id="thead-total">Total</th>
                                                    <th>Currency</th>
                                                    <!-- <th>Subtotal</th>
                                                    <th>Discount Var</th>
                                                    <th>Discount</th>
                                                    <th>Nilai Disc</th>
                                                    <th>ID Type Tax</th>
                                                    <th>ID Tax</th> -->
                                                    <!-- <th>Harga Total</th> -->
                                                    <!-- <th>ID Akun HPP</th>
                                                    <th>ID Akun Sales</th>
                                                    <th>ID Akun Persediaan</th>
                                                    <th>ID Akun Sales Return</th>
                                                    <th>ID Akun Purchase Return</th>
                                                    <th>Serial Number</th>
                                                    <th>Garansi Tanggal</th>
                                                    <th>Garansi Expired</th> -->
                                                    <!-- <th>Type Tranc Prev</th>
                                                    <th>Tranc Prev</th>
                                                    <th>Tranc Prev D</th>
                                                    <th>Type Tranc Order</th>
                                                    <th>Tranc Order</th>
                                                    <th>Tranc Order D</th> -->
                                                    <th id="th-action">Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th></th>
                                                    <th></th>
                                                    <th class="tfoot-kode"></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <!-- <th class="text-right" colspan="2" style="border: 1px solid #95c9ed"></th> -->
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div id="modal-tanggal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-filter_ modal-dialog modal-lg">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="hideModalTanggal()"></a>
                    <h4 class="modal-title" style="padding-left: 12px">DETAIL TANGGAL</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-top: 8px">
                        <div class="col-md-12" style="padding: 0px">
                            <div class="card" style="margin: 0px 0px 5px 1px !important">
                                <div class="card-body">
                                    <form id="form-modal-tanggal">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Tanggal</label>
                                                    <div class="col-md-8">
                                                        <input type="text" value="{{$defaulttanggal}}" id="tanggal-modal" class="form-control datepicker" placeholder="Tanggal" autocomplete="off" onclick="changeTanggal('tanggal-modal', 'tanggal')" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Tanggal Tax</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="tanggaltax" id="tanggaltax-modal" class="form-control datepicker" placeholder="Tanggal Tax" autocomplete="off">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Tanggal Due</label>
                                                    <div class="col-md-8">
                                                        <input type="text" value="{{$defaulttanggaldue}}" id="tanggaldue-modal" class="form-control datepicker" placeholder="Tanggal Due" autocomplete="off" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-supplier" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-filter_ modal-dialog modal-lg">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="hideModalSupplier()"></a>
                    <h4 class="modal-title" style="padding-left: 12px">DETAIL SUPPLIER</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-top: 8px">
                        <div class="col-md-12" style="padding: 0px">
                            <div class="card" style="margin: 0px 0px 5px 1px !important">
                                <div class="card-body">
                                    <form id="form-modal-supplier">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Card Supplier</label>
                                                    <div class="col-md-8">
                                                        <input type="text" value="{{@$lastsupplier->suppliernama}}" id="idcardsupplier-modal" class="form-control" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Akun Supplier</label>
                                                    <div class="col-md-8">
                                                        <select class="form-control select2" name="idakunsupplier" id="idakunsupplier-modal">
                                                            @foreach($accmkodeakun as $v)
                                                                <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Supplier Nama</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="suppliernama" id="suppliernama-modal" class="form-control" placeholder="Supplier Nama">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Supplier Addr. 1</label>
                                                    <div class="col-md-8">
                                                        <textarea name="supplieraddress1" id="supplieraddress1-modal" cols="3" rows="3" class="form-control" placeholder="Supplier Address 1"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Supplier Addr. 2</label>
                                                    <div class="col-md-8">
                                                        <textarea name="supplieraddress2" id="supplieraddress2-modal" cols="3" rows="3" class="form-control" placeholder="Supplier Address 2"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">RT</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="supplieraddressrt" id="supplieraddressrt-modal" class="form-control" placeholder="RT">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">RW</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="supplieraddressrw" id="supplieraddressrw-modal" class="form-control" placeholder="RW">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Kelurahan</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="supplierlokasikel" id="supplierlokasikel-modal" class="form-control" placeholder="Kelurahan">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Kecamatan</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="supplierlokasikec" id="supplierlokasikec-modal" class="form-control" placeholder="Kecamatan">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Kabupaten</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="supplierlokasikab" id="supplierlokasikab-modal" class="form-control" placeholder="Kabupaten">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Propinsi</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="supplierlokasiprop" id="supplierlokasiprop-modal" class="form-control" placeholder="Propinsi">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Negara</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="supplierlokasineg" id="supplierlokasineg-modal" class="form-control" placeholder="Negara">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-detail" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-detail modal-dialog modal-lg">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="hideModalDetail()"></a>
                    <h4 class="modal-title" style="padding-left: 12px">DETAIL DETAIL</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-top: 8px">
                        <div class="col-md-12" style="padding: 0px">
                            <div class="card" style="margin: 0px 0px 5px 1px !important">
                                <div class="card-body">
                                    <form id="form-modal-detail">
                                        <div class="row">
                                            <div class="col-md-7">
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label">Inventor</label>
                                                    <div class="col-md-5">
                                                        <select class="form-control select2" name="idinventor" id="idinventor-modal" onchange="changeKodeInventorModal()">
                                                            <option value="">[Choose Inventor]</option>
                                                            @foreach($invminventory as $v)
                                                                <option 
                                                                    value="{{$v->noid}}"
                                                                    data-invminventory-kode="{{$v->kode}}"
                                                                    data-invminventory-nama="{{$v->nama}}"
                                                                    data-invminventory-idsatuan="{{$v->idsatuan}}"
                                                                    data-invminventory-konversi="{{$v->konversi}}"
                                                                    data-invminventory-idtax="{{$v->idtax}}"
                                                                    data-invminventory-taxprocent="{{$v->taxprocent}}"
                                                                    data-invminventory-purchaseprice="{{$v->purchaseprice}}"
                                                                >{{$v->kode}} - {{$v->nama}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="text" id="namainventorori-modal" class="form-control" style="background-color: #E9ECEF !important;" placeholder="Nama Inventor" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label"></label>
                                                    <div class="col-md-9">
                                                        <input type="text" name="namainventor" id="namainventor-modal" class="form-control" placeholder="Nama Inventor">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label">Type Package</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control select2" name="idtypepackage" id="idtypepackage-modal">
                                                            <option value="" style="display:none">[Choose Inventor]</option>
                                                            @foreach($mtypepackage as $v)
                                                                <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label">Group Inventor</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control select2" name="idgroupinv" id="idgroupinv-modal">
                                                            <option value="" style="display:none">[Choose Group Inventor]</option>
                                                            @foreach($invmgroup as $v)
                                                                <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label">Keterangan</label>
                                                    <div class="col-md-9">
                                                        <textarea name="keterangan" id="keterangan-modal" cols="3" rows="6" class="form-control" placeholder="Keterangan"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label">Company / Dept.</label>
                                                    <div class="col-md-4">
                                                        <select class="form-control idcompany" style="background-color: #E9ECEF !important;" name="idcompany" id="idcompany-modal" readonly>
                                                            <option value="" style="display:none">[Choose Company]</option>
                                                            @foreach($genmcompany as $v)
                                                                <option value="{{$v->noid}}" style="display:none">{{$v->kode}} - {{$v->nama}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <select class="form-control iddepartment" style="background-color: #E9ECEF !important;" name="iddepartment" id="iddepartment-modal" readonly>
                                                            <option value="" style="display:none">[Choose Department]</option>
                                                            @foreach($genmdepartment as $v)
                                                                <option value="{{$v->noid}}" style="display:none">{{$v->kode}} - {{$v->nama}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label">Gudang</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control" style="background-color: #E9ECEF !important;" name="idgudang" id="idgudang-modal" readonly>
                                                            <option value="" style="display:none">[Choose Gudang]</option>
                                                            @foreach($genmgudang as $v)
                                                                <option value="{{$v->noid}}" style="display:none">{{$v->kode}} - {{$v->namalias}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group row" style="display: none">
                                                    <label class="col-md-3 col-form-label">ID Currency</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control select2" id="idcurrency-modal">
                                                            <option value="">[Choose Currency]</option>
                                                            @foreach($accmcurrency as $v)
                                                                <option 
                                                                    value="{{$v->noid}}" 
                                                                    id="idcurrency-{{$v->noid}}"
                                                                    data-accmcurrency-nama="{{$v->nama}}" 
                                                                    <?= $v->noid == 1 ? 'selected' : '' ?>
                                                                >{{$v->kode}} - {{$v->nama}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Satuan / Konv.</label>
                                                    <div class="col-md-6">
                                                        <select class="form-control select2" name="idsatuan" id="idsatuan-modal">
                                                            <option value="" >[Choose Satuan]</option>
                                                            @foreach($invmsatuan as $v)
                                                                <option 
                                                                    value="{{$v->noid}}"
                                                                    data-invmsatuan-nama="{{$v->nama}}"
                                                                    data-invmsatuan-konversi="{{$v->konversi}}"
                                                                >{{$v->nama}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <!-- <input type="text" name="idsatuan" id="idsatuan-modal" class="form-control" placeholder="ID Satuan"> -->
                                                        <input type="text" name="konvsatuan" id="konvsatuan-modal" class="form-control" style="background-color: #E9ECEF !important;" placeholder="Konv Satuan" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label">Unit Qty / Sisa</label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="unitqty" id="unitqty-modal" class="form-control unitqty" placeholder="Unit Qty">
                                                    </div><div class="col-md-4">
                                                        <input type="text" name="unitqtysisa" id="unitqtysisa-modal" class="form-control" style="background-color: #E9ECEF !important;" placeholder="Unit Qty Sisa" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group row" style="display: none">
                                                    <label class="col-md-4 col-form-label">Unit Price</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="unitprice" id="unitprice-modal" class="form-control input-mask" placeholder="Unit Price">
                                                    </div>
                                                </div>
                                                <div class="form-group row" style="display: none">
                                                    <label class="col-md-4 col-form-label">Subtotal</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="subtotal" id="subtotal-modal" class="form-control input-mask" style="background-color: #E9ECEF !important;" placeholder="Subtotal" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group row" style="display: none">
                                                    <label class="col-md-4 col-form-label">Discount Var</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="discountvar" id="discountvar-modal" class="form-control" placeholder="Discount Var">
                                                    </div>
                                                </div>
                                                <div class="form-group row" style="display: none">
                                                    <label class="col-md-4 col-form-label">Discount / Nilai</label>
                                                    <div class="col-md-3">
                                                        <div class="input-group">
                                                            <input type="text" name="discount" id="discount-modal" class="form-control" placeholder="Discount">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">%</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <input type="text" name="nilaidisc" id="nilaidisc-modal" class="form-control input-mask" style="background-color: #E9ECEF !important;" placeholder="Nilai Disc" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group row" style="display: none">
                                                    <label class="col-md-4 col-form-label">ID Type Tax</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="idtypetax" id="idtypetax-modal" class="form-control" placeholder="ID Type Tax">
                                                    </div>
                                                </div>
                                                <div class="form-group row" style="display: none">
                                                    <label class="col-md-4 col-form-label">ID Tax / Prosentax</label>
                                                    <div class="col-md-5">
                                                        <select class="form-control select2" name="idtax" id="idtax-modal">
                                                            <option value="" data-accmpajak-prosentax="0">[Choose Tax]</option>
                                                            @foreach($accmpajak as $v)
                                                                <option 
                                                                    value="{{$v->noid}}"
                                                                    data-accmpajak-prosentax="{{$v->prosentax}}"
                                                                >{{$v->nama}}</option>
                                                            @endforeach
                                                        </select>
                                                        <!-- <input type="text" name="idtax" id="idtax-modal" class="form-control" placeholder="ID Tax"> -->
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="input-group">
                                                            <input type="text" name="prosentax" id="prosentax-modal" class="form-control" style="background-color: #E9ECEF !important;" placeholder="Prosentax" readonly>
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">%</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row" style="display: none">
                                                    <label class="col-md-4 col-form-label">Nilai Tax</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="nilaitax" id="nilaitax-modal" class="form-control input-mask" style="background-color: #E9ECEF !important;" placeholder="Nilai Tax" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group row" style="display: none">
                                                    <label class="col-md-4 col-form-label">Harga Total</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="hargatotal" id="hargatotal-modal" class="form-control input-mask" style="background-color: #E9ECEF !important;" placeholder="Harga Total" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row pull-right">
                                            <div class="col-md-12">
                                                <button type="button" id="close-modal-detail" title="Close" class="btn btn-default mr-2" onclick="hideModalDetail()">
                                                    <i class="fa fa-times"></i> Close
                                                </button>
                                                <button type="button" id="update-modal-detail" title="Update Detail" class="btn btn-success mr-2" onclick="updateDetail()">
                                                    <i class="fa fa-save"></i> Update
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-filter" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-filter modal-dialog">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="closeModalFilter()"></a>
                    <h4 class="modal-title">Config Datatable</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="formfilter">
                        <div class="row" id="filtercontent">
                            <div class="col-md-12">
                                <div class="portlet box blue-steel" style="margin: 8px;">
                                    <div class="portlet-title">
                                        <div class="caption" style="font-weight: bold">
                                            <i class="fa fa-filter"></i>
                                            Datatable Filter
                                        </div>
                                        <div class="actions">
                                            <a onclick="saveFilter()" href="javascript:;" class="btn green btn-sm">
                                                <i class="fa fa-save text-white"></i> Save
                                            </a>
                                            <a id="addgroup" href="javascript:;" class="btn yellow btn-sm">
                                                <i class="fa fa-indent text-white"></i> Add Group
                                            </a>
                                            <a id="delgroup" href="javascript:;" class="btn red btn-sm">
                                                <i class="fa fa-trash text-white"></i> Delete Group
                                            </a>
                                            <a onclick="addRow()" href="javascript:;" class="btn purple btn-sm">
                                                <i class="fa fa-list text-white"></i> Add Row
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row" style="margin:0;">
                                            <div class="col-md-12" style="padding: 0 0 15px 0;">
                                                <div id="filtercontentgroup">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin:0;">
                                            <div class="col-md-12">
                                                <div id="filtercontentbody">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal CKEditor -->
    <div id="modal-ckeditor" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-filter modal-dialog">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="closeModalFilter()"></a>
                    <h4 class="modal-title">[0] PHPSOURCE</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <textarea class="form-control" id="summary-ckeditor" name="summary-ckeditor"></textarea>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
@section('scripts')
<!-- <script src="//www.amcharts.com/lib/3/amcharts.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/serial.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/radar.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/pie.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/plugins/animate/animate.min.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/plugins/export/export.min.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/themes/light.js?v=7.0.6"></script> -->
<!-- <script src="https://cdn.amcharts.com/lib/4/core.js"></script> -->
<!-- <script src="https://cdn.amcharts.com/lib/4/charts.js"></script> -->
<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
<script src="{{ asset('js/pages/crud/forms/widgets/bootstrap-datepicker.js?v=7.0.6') }}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.3.1/js/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.21/pagination/input.js"></script>
<script  src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.0.2/js/bootstrap-datetimepicker.min.js"></script>
<!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script> -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-loading-overlay/2.1.7/loadingoverlay.min.js"></script>
<script src="{{ asset('js/jquery-currency.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>

    $(".datepicker").datepicker({
        format: 'dd-M-yyyy',
        autoclose: true,
        container: '#pagepanel'
    });

    $(".datepicker2").datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true,
        container: '#pagepanel'
    });

    //select2-standard
    $('.select2').select2({
        dropdownParent: $('#pagepanel')
    });
    $('.select2').css('width', '100%');
    $('.select2 .selection .select2-selection').css('height', '31px');
    $('.select2 .selection .select2-selection .select2-selection__rendered').css('margin-top', '-6px');
    $('.select2 .selection .select2-selection .select2-selection__arrow').css('margin-top', '4px');

    //select2-readonly
    // $('.select2-readonly').select2({
    //     dropdownParent: $('#pagepanel'),
    //     readonly: true
    // });
    // $('.select2-readonly').css('width', '100%');
    // $('.select2-readonly .selection .select2-readonly-selection').css('height', '31px');
    // $('.select2-readonly .selection .select2-readonly-selection .select2-readonly-selection__rendered').css('margin-top', '-6px');
    // $('.select2-readonly .selection .select2-readonly-selection .select2-readonly-selection__arrow').css('margin-top', '4px');



    $('.input-mask').inputmask('decimal', {
        'alias': 'numeric',
        'groupSeparator': ',',
        'autoGroup': true,
        // 'digits': 0,
        // 'radixPoint': ",",
        // 'digitsOptional': false,
        'allowMinus': false,
        'prefix': 'Rp.',
        'placeholder': '0'
    });

    CKEDITOR.replace('summary-ckeditor');
</script>
<script type="text/javascript">

    let main = {
        'table': 'purchaserequest',
        'tabledetail': 'purchaserequestdetail',
        'sendtonext': 'Send to Purchase Order',
        'urlcode': '_prq_ctmd',
        'datefilterdefault': 'tm',
        'condition': {
            'create': {
                'c': true,
                'r': true,
                'u': true,
                'd': true,
                'v': false,
                'formdetail': true,
            },
            'read': {
                'c': true,
                'r': true,
                'u': true,
                'd': true,
                'v': true,
                'formdetail': true,
            },
            'update': {
                'c': true,
                'r': true,
                'u': true,
                'd': true,
                'v': false,
                'formdetail': true,
            },
            'delete': {
                'c': false,
                'r': false,
                'u': false,
                'd': true,
                'v': false,
                'formdetail': true,
            },
            'view': {
                'c': false,
                'r': true,
                'u': false,
                'd': false,
                'v': true,
                'formdetail': false,
            },
        }
    };

    let status = {
        'crudmaster': 'read',
        'cruddetail': 'read',
    };

    function conditionDetail() {
        main.condition[status.crudmaster]['formdetail'] ? $('#form-detail').show() : $('#form-detail').hide();
    }

    var statusgetlog = false;

    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        datefilter2.from = start.format('YYYY-MM-DD');
        datefilter2.to = end.format('YYYY-MM-DD');
        getDatatable();
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    var daterangepicker = $('#reportrange').daterangepicker({
        opened: true,
        // expanded: true,
        // standalone: true,
        // parentElement: document.getElementById('pagepanel'),
        // anchorElement: document.getElementById('pagepanel'),
        // container: '#pagepanel',
        startDate: start,
        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);


    console.log('daterangepicker')
    console.log(document.getElementById('pagepanel'))

    var qty_tr_detail = 0;

    function setTab(element, title) {
        if (title) {
            $('#tab-title').text(title);
        }
        $('#tab-form li a').removeClass('active');
        $('#tab-form li #'+element).addClass('active');
        $('#tab-content div').removeClass('show active');
        $('#tab-content #'+element+'-content').addClass('show active');
    }

    function changeTanggal(elfrom, elto) {
        $('#'+elto).val($('#'+elfrom).val())
    }

    function changeCardSupplier(elfrom, elto) {
        let idcardsupplier = $('#'+elfrom+' :selected');
        let prop = $('#'+elfrom+' :selected').attr('data-mlokasiprop-nama');
        let kab = $('#'+elfrom+' :selected').attr('data-mlokasikab-nama');
        let kec = $('#'+elfrom+' :selected').attr('data-mlokasikec-nama');
        let kel = $('#'+elfrom+' :selected').attr('data-mlokasikel-nama');

        $('#idcardsupplier-modal').val(idcardsupplier.val() ? idcardsupplier.text() : '');
        $('#suppliernama-modal').val(idcardsupplier.val() ? idcardsupplier.text() : '');
        $('#supplierlokasiprop-modal').val(prop);
        $('#supplierlokasikab-modal').val(kab);
        $('#supplierlokasikec-modal').val(kec);
        $('#supplierlokasikel-modal').val(kel);
        $('#idcardsupplier-modal').val(idcardsupplier.text())
    }

    function changeKodeInventor() {
        let idinventor = $('#idinventor :selected');
        $('#form-detail #namainventor').val(idinventor.val() ? idinventor.attr('data-invminventory-nama') : '');
        $('#form-detail #unitprice').val(idinventor.val() ? idinventor.attr('data-invminventory-purchaseprice') : '');
        $('#form-modal-detail #idinventor-modal').val(idinventor.val() ? idinventor.text() : '');
        $('#form-modal-detail #namainventor-modal').val(idinventor.val() ? idinventor.attr('data-invminventory-nama') : '');
    }

    function changeKodeInventorModal() {
        let idinventor = $('#idinventor-modal :selected');
        $('#form-modal-detail #namainventorori-modal').val(idinventor.val() ? idinventor.attr('data-invminventory-nama') : '');
        $('#form-modal-detail #namainventor-modal').val(idinventor.val() ? idinventor.attr('data-invminventory-nama') : '');
    }

    function changeNamaInventor() {
        let namainventor = $('#form-detail #namainventor').val();
        $('#form-modal-detail #namainventor-modal').val(namainventor);
    }

    // $('#form-header #idcardsupplier').on('change', function() {
    //     $('#form-modal-supplier #idcardsupplier-modal').val($('#form-header #idcardsupplier :selected').val()).trigger('change')
    // });
    // $('#form-modal-supplier #idcardsupplier-modal').on('custom', function(idcardsupplier) {
    //     $('#form-modal-supplier #idcardsupplier-modal').val(idcardsupplier)
    // });

    $('#form-header #idcompany').on('change', function() {
        $('#form-modal-detail #idcompany-modal').val($('#form-header #idcompany :selected').val()).trigger('change')
    });
    // $('#form-modal-detail #idcompany-modal').on('change', function() {
    //     $('#form-header #idcompany').val($('#form-modal-detail #idcompany-modal :selected').val()).trigger('change')
    // });

    $('#form-header #iddepartment').on('change', function() {
        $('#form-modal-detail #iddepartment-modal').val($('#form-header #iddepartment :selected').val()).trigger('change')
    });
    // $('#form-modal-detail #iddepartment-modal').on('change', function() {
    //     $('#form-header #iddepartment').val($('#form-modal-detail #iddepartment-modal :selected').val()).trigger('change')
    // });
    
    function changeUnitQty(elfrom, elto) {
        let unitqty = $('#'+elfrom).val();
        $('#'+elto).val(unitqty);
        $('#form-modal-detail [name="unitprice"]').val(13000);
        $('#form-modal-detail [name="hargatotal"]').val(unitqty*13000);
    }

    function chooseDetailPrev(
        noid,
        idmaster,
        kodeprev,
        idinventor,
        kodeinventor,
        namainventor,
        namainventor2,
        keterangan,
        idgudang,
        idcompany,
        iddepartment,
        idsatuan,
        namasatuan,
        konvsatuan,
        unitqty,
        unitqtysisa,
        idinvmgroup,
        unitprice,
        subtotal,
        discountvar,
        discount,
        nilaidisc,
        idtypetax,
        idtax,
        prosentax,
        nilaitax,
        hargatotal,
        namacurrency
    ) {
        $('#form-detail #noiddetail').val(noid);
        $('#form-detail #idmaster').val(idmaster);
        $('#form-detail #kodeprev').val(kodeprev);
        $('#form-detail #idinventor').val(idinventor).trigger('change');
        $('#form-detail #kodeinventor').val(kodeinventor).trigger('change');
        $('#form-detail #namainventor').val(namainventor);
        $('#form-detail #namainventor2').val(namainventor2);
        $('#form-detail #unitqtyoriginal').val(unitqtysisa);
        $('#form-detail #idgroupinv').val(idinvmgroup).trigger('change');
        $('#form-detail #unitqty').val($('[name=searchtypeprev]:checked').val() == 1 ? unitqtysisa : '');
        $('#form-modal-detail #keterangan-modal').val(keterangan);
        $('#form-modal-detail #idgudang-modal').val(idgudang).trigger('change');
        $('#form-modal-detail #idcompany-modal').val(idcompany).trigger('change');
        $('#form-modal-detail #iddepartment-modal').val(iddepartment).trigger('change');
        $('#form-modal-detail #idsatuan-modal').val(idsatuan).trigger('change');
        $('#form-modal-detail #unitprice-modal').val(unitprice);
        $('#form-modal-detail #subtotal-modal').val(subtotal);
        $('#form-modal-detail #discount-modal').val(discount);
        $('#form-modal-detail #nilaidisc-modal').val(nilaidisc);
        $('#form-modal-detail #idtax-modal').val(idtax).trigger('change');
        $('#form-modal-detail #prosentax-modal').val(prosentax);
        $('#form-modal-detail #nilaitax-modal').val(nilaitax);
        $('#form-modal-detail #hargatotal-modal').val(hargatotal);

        hideModalDetailPrev();
        // setTimeout(function(){ $('#form-detail div').slideDown(); }, 100);
        // $('#form-detail #unitqty').focus();

        setTimeout(function(){ $('#form-detail div').slideDown(100, function() {
            $('#form-detail #unitqty').focus();
        }); }, 100);
        return false;

        let detailprev = new Array();
        detailprev.push({name:'noid',value:noid});
        detailprev.push({name:'idmaster',value:idmaster});
        detailprev.push({name:'idinventor',value:idinventor});
        detailprev.push({name:'kodeinventor',value:kodeinventor});
        detailprev.push({name:'namainventor',value:namainventor});
        detailprev.push({name:'namainventor2',value:namainventor2});
        detailprev.push({name:'keterangan',value:keterangan});
        detailprev.push({name:'idgudang',value:idgudang});
        detailprev.push({name:'idcompany',value:idcompany});
        detailprev.push({name:'iddepartment',value:iddepartment});
        detailprev.push({name:'idsatuan',value:idsatuan});
        detailprev.push({name:'namasatuan',value:namasatuan});
        detailprev.push({name:'konvsatuan',value:konvsatuan});
        detailprev.push({name:'unitqty',value:unitqty});
        detailprev.push({name:'unitqtysisa',value:unitqtysisa});
        detailprev.push({name:'unitprice',value:unitprice});
        detailprev.push({name:'subtotal',value:subtotal});
        detailprev.push({name:'discountvar',value:discountvar});
        detailprev.push({name:'discount',value:discount});
        detailprev.push({name:'nilaidisc',value:nilaidisc});
        detailprev.push({name:'idtypetax',value:idtypetax});
        detailprev.push({name:'idtax',value:idtax});
        detailprev.push({name:'prosentax',value:prosentax});
        detailprev.push({name:'nilaitax',value:nilaitax});
        detailprev.push({name:'hargatotal',value:hargatotal});
        detailprev.push({name:'namacurrency',value:hargatotal});
        console.log(detailprev)

        let dataprd = localStorage.getItem(main.tabledetail);

        if (dataprd) {
            let datamerge = JSON.parse(dataprd);
            datamerge.push(detailprev);
            localStorage.setItem(main.tabledetail, JSON.stringify(datamerge));
        } else {
            let firstdata = new Array();
            firstdata.push(detailprev);
            localStorage.setItem(main.tabledetail, JSON.stringify(firstdata));
        }

        getDatatableDetailPrev()
        getDatatableDetail(localStorage.getItem(main.tabledetail))
        cancelModalDetail()
    }

    function saveDetail() {
        let noiddetail = $('#form-detail #noiddetail').val();
        let idmaster = $('#form-detail #idmaster').val();
        let kodeprev = $('#form-detail #kodeprev').val();
        let idinventor = $('#form-detail #idinventor').val();
        let kodeinventor = $('#form-detail #idinventor :selected').attr('data-invminventory-kode');
        let namainventor = $('#form-detail #idinventor :selected').attr('data-invminventory-nama');
        let namainventor2 = $('#form-detail #namainventor').val();
        let unitprice = parseInt($('#form-modal-detail #unitprice-modal').val().replace(/[^0-9]/gi, ''));
        let unitqtyoriginal = $('#form-detail #unitqtyoriginal').val();
        let unitqty = $('#form-detail #unitqty').val();
        let keterangan = $('#form-detail #keterangandetail').val();
        let subtotal = parseInt(unitqty)*parseInt(unitprice);
        let idtypepackage = $('#form-detail #idtypepackage').val();
        let idgroupinv = $('#form-detail #idgroupinv').val();
            
        let idcompany = $('#form-header #idcompany').val();
        let iddepartment = $('#form-header #iddepartment').val();
        let idgudang = $('#form-header #idgudang').val();
        let idcurrency = $('#form-header #idcurrency :selected').val();
        let namacurrency = $('#form-header #idcurrency :selected').attr('data-accmcurrency-nama');

        // let keterangan = $('#form-modal-detail #keterangan-modal').val();
        let idsatuan = $('#form-modal-detail #idsatuan-modal :selected').val();
        let namasatuan = $('#form-modal-detail #idsatuan-modal :selected').attr('data-invmsatuan-nama');
        let konvsatuan = $('#form-detail #idinventor :selected').attr('data-invminventory-konversi');
        let unitqtysisa = unitqty;
        let discountvar = 0;
        let discount = 0;
        let nilaidisc = 0;
        let idtypetax = 0;
        let idtax = $('#form-detail #idinventor :selected').attr('data-invminventory-idtax');
        let prosentax = $('#form-detail #idinventor :selected').attr('data-invminventory-taxprocent')
        let nilaitax = parseInt(subtotal)*parseInt(prosentax)/100;
        let hargatotal = parseInt(subtotal)-parseInt(nilaidisc)+parseInt(nilaitax);

        let dataprd = localStorage.getItem(main.tabledetail);
        let formdetail = new Array();
        formdetail.push({name:"noid",value:noiddetail});
        formdetail.push({name:"kodeprev",value:kodeprev});
        formdetail.push({name:"idinventor",value:idinventor});
        formdetail.push({name:"kodeinventor",value:kodeinventor});
        formdetail.push({name:"namainventor",value:namainventor});
        formdetail.push({name:"namainventor2",value:namainventor2});
        formdetail.push({name:"idtypepackage",value:idtypepackage});
        formdetail.push({name:"idgroupinv",value:idgroupinv});
        formdetail.push({name:"keterangan",value:keterangan});
        let formheader = new Array();
        formheader.push({name:"idcompany",value:idcompany});
        formheader.push({name:"iddepartment",value:iddepartment});
        formheader.push({name:"idgudang",value:idgudang});
        formheader.push({name:"idcurrency",value:idcurrency});
        formheader.push({name:"namacurrency",value:namacurrency});
        let others = new Array();
        others.push({name:"keterangan",value:keterangan});
        others.push({name:"idsatuan",value:idsatuan});
        others.push({name:"namasatuan",value:namasatuan});
        others.push({name:"konvsatuan",value:konvsatuan});
        others.push({name:"unitqty",value:unitqty});
        others.push({name:"unitqtysisa",value:unitqtysisa});
        others.push({name:"unitprice",value:currency(unitprice)});
        others.push({name:"subtotal",value:currency(subtotal)});
        others.push({name:"discountvar",value:discountvar});
        others.push({name:"discount",value:discount});
        others.push({name:"nilaidisc",value:nilaidisc});
        others.push({name:"idtypetax",value:idtypetax});
        others.push({name:"idtax",value:idtax});
        others.push({name:"prosentax",value:prosentax});
        others.push({name:"nilaitax",value:nilaitax});
        others.push({name:"hargatotal",value:currency(hargatotal)});
        // others.push({name:"idtypetrancprev",value:502});
        // others.push({name:"idtrancprev",value:idmaster});
        // others.push({name:"idtrancprevd",value:noiddetail});
        // others.push({name:"unitqtysisaprev",value:parseInt(unitqtyoriginal)});
        let allprd = formdetail.concat(formheader, others);
        // console.log(formheader);return false;

        let condition = {
            status: '',
            message: {
                title: '',
                text: ''
            },
            type: '',
        };
        if (unitqty == '') {
            condition.status = 'notif';
            condition.message.title = 'Complete data, please!';
            condition.type = 'warning';
        } else if (parseInt(unitqty) > parseInt(unitqtyoriginal)) {
            condition.status = 'confirm';
            condition.message.title = 'Qty Updated melebihi Qty Sisa!';
            condition.message.text = 'Apakah ingin tetap melanjutkan transaksi?';
            condition.type = 'warning';
        } else if (parseInt(unitqty) < 1) {
            condition.status = 'notif';
            condition.message.title = 'Qty Updated minimal 1!'
            condition.type = 'warning';
        } else if (isNaN(parseInt(unitqty))) {
            condition.status = 'notif';
            condition.message.title = 'Qty Updated harus angka!'
            condition.type = 'warning';
        } else {
            if (dataprd) {
                let datamerge = JSON.parse(dataprd);
                datamerge.push(allprd);
                // datamerge[noiddetail] = allprd;
                localStorage.setItem(main.tabledetail, JSON.stringify(datamerge));
            } else {
                let firstdata = new Array();
                firstdata.push(allprd);
                // firstdata[noiddetail] = allprd;
                localStorage.setItem(main.tabledetail, JSON.stringify(firstdata));
            }
            $('#searchprev').focus();
            $('#form-detail div').slideUp();
            getDatatableDetail(localStorage.getItem(main.tabledetail))
            cancelFormDetail()
        }
            console.log(condition);
        
        if (condition.status == 'confirm') {
            Swal.fire({
                title: condition.message.title,
                text: condition.message.text,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                target: '#pagepanel'
            }).then((result) => {
                if (result.isConfirmed) {        
                    
                    // qty_tr_detail = qty_tr_detail+1;
                    // let tr;

                    if (dataprd) {
                        let datamerge = JSON.parse(dataprd);
                        datamerge.push(allprd);
                        // datamerge[noiddetail] = allprd;
                        localStorage.setItem(main.tabledetail, JSON.stringify(datamerge));
                    } else {
                        let firstdata = new Array();
                        firstdata.push(allprd);
                        // firstdata[noiddetail] = allprd;
                        localStorage.setItem(main.tabledetail, JSON.stringify(firstdata));
                    }
                    $('#searchprev').focus();
                    $('#form-detail div').slideUp();
                    getDatatableDetail(localStorage.getItem(main.tabledetail))
                    cancelFormDetail()
                    return false;
                }
            })
            // if (!confirm(condition.message)) {
                // return false;
            // }
        } else if (condition.status == 'notif') {
            $.notify({
                message: condition.message.title
            },{
                type: condition.type
            });
            return false;
        }
    }

    function updateDetail() {
        let formmodaldetail = $('#form-modal-detail').serializeArray();
        let id = $('#noiddetail').val();
        let data = new Array();

        $.each(JSON.parse(localStorage.getItem(main.tabledetail)), function(k, v) {
            if (id == k) {
                let record = new Array();
                let noid = formmodaldetail.filter(i => i.name == 'noid')
                let kodeprev = formmodaldetail.filter(i => i.name == 'kodeprev')

                if (noid.length == 0 && kodeprev.length == 0) {
                    record.push({name:'noid',value:''});
                    record.push({name:'kodeprev',value:''});
                }

                $.each(formmodaldetail, function(k2, v2) {
                    if (v2.name == 'namainventor') {
                        record.push({name:'kodeinventor',value:$('#form-modal-detail #idinventor-modal :selected').attr('data-invminventory-kode')})
                        record.push({name:'namainventor',value:$('#form-modal-detail #idinventor-modal :selected').attr('data-invminventory-nama')})
                        record.push({name:'namainventor2',value:v2.value})
                    } else if (v2.name == 'idsatuan') {
                        record.push({name:'idcurrency',value:$('#form-modal-detail #idcurrency-modal :selected').val()})
                        record.push({name:'namacurrency',value:$('#form-modal-detail #idcurrency-modal :selected').attr('data-accmcurrency-nama')})
                        record.push({name:'idsatuan',value:v2.value})
                        record.push({name:'namasatuan',value:$('#form-modal-detail #idsatuan-modal :selected').attr('data-invmsatuan-nama')})
                    } else {
                        record.push({name:v2.name,value:v2.value})
                    }
                })
                data.push(record);
            } else {
                data.push(v);
            }
        })

        localStorage.removeItem(main.tabledetail);
        localStorage.setItem(main.tabledetail, JSON.stringify(data));
        getDatatableDetail(localStorage.getItem(main.tabledetail))
        cancelModalDetail()
    }

    function defaultOperation() {
        let subtotal = 0;
        let total = 0;
        $.each(JSON.parse(localStorage.getItem(main.tabledetail)), function(k, v) {
            $.each(v, function(k2, v2) {
                if (v2.name == 'hargatotal') {
                    subtotal += $.isNumeric(v2.value) ? v2.value : parseInt(v2.value.replace(/[^0-9]/gi, ''))
                    total += $.isNumeric(v2.value) ? v2.value : parseInt(v2.value.replace(/[^0-9]/gi, ''))
                }
            })
        });
        // alert(subtotal)
        $('#subtotal').val(subtotal);
        $('#total').val(total);
    }

    function editDetail(params) {
        let id = params.id;
        let update = params.update;

        status.cruddetail = update ? 'update' : 'view';

        update ? $('#update-detail').show() : $('#update-detail').hide();
        $('#noiddetail').val(id);

        $.each(JSON.parse(localStorage.getItem(main.tabledetail)), function(k, v) {
            if (id == k) {
                $.each(v, function(k2, v2) {
                    if (v2.name == 'idinventor' || v2.name == 'idtypepackage' || v2.name == 'idgroupinv') {
                        $('#'+v2.name+'-modal').val(v2.value).trigger('change');
                    } else if (v2.name == 'namainventor') {
                        $('#namainventorori-modal').val(v2.value);
                    } else if (v2.name == 'namainventor2') {
                        $('#namainventor-modal').val(v2.value);
                    } else if (v2.name == 'idgudang' || v2.name == 'idcompany' || v2.name == 'iddepartment' || v2.name == 'idsatuan' || v2.name == 'idtax') {
                        $('#'+v2.name+'-modal').val(v2.value).trigger('change');
                    } else if (v2.name == 'unitqty') {
                        $('#'+v2.name+'-modal').val(v2.value);
                        $('#'+v2.name+'sisa-modal').val(v2.value);
                    } else if (v2.name == 'unitqtysisa') {
                    } else {
                        $('#'+v2.name+'-modal').val(v2.value);
                    }

                    // $('#idinventor').val(v2.value).trigger('change');
                    // $('#unitqty').val(v2.value);
                    // $('#namainventor-modal').val(v2.value);
                    // $('#keterangan-modal').val(v2.value);
                    // $('#idgudang-modal').val(v2.value).trigger('change');
                    // $('#idsatuan-modal').val(v2.value);
                    // $('#konvsatuan-modal').val(v2.value);
                    // $('#unitqtysisa-modal').val(v2.value);
                    // $('#unitprice-modal').val(v2.value);
                    // $('#subtotal-modal').val(v2.value);
                    // $('#discountvar-modal').val(v2.value);
                    // $('#discount-modal').val(v2.value);
                    // $('#idtypetax-modal').val(v2.value);
                    // $('#idtax-modal').val(v2.value);
                    // $('#prosentax-modal').val(v2.value);
                    // $('#nilaitax-modal').val(v2.value);
                    // $('#hargatotal-modal').val(v2.value);
                })
            }
        })
        showModalDetail()
    }

    function cancelFormDetail() {
        unsetFormDetail();
        $('#noiddetail').val('');
        $('#form-detail div').slideUp();
    }

    function cancelModalDetail() {
        unsetFormModalDetail();
        $('#noiddetail').val('');
        $('#cancel-modal-detail').hide();
        // if (statusdetail) {
        //     $('#update-modal-detail').hide();
        //     $('#save-detail').hide();
        // } else {
        //     $('#update-detail').hide();
        //     $('#save-detail').show();
        // }
        hideModalDetail()
    }

    function unsetFormDetail() {
        $('#searchprev').val('');
        if (!$('#form-detail #idinventor').val() == '') {
            $('#form-detail #idinventor').val('').trigger('change')
        }
        $('#form-detail #namainventor').val('');
        $('#form-detail #unitprice').val('');
        $('#form-detail #unitqty').val('');
        if (!$('#form-detail #idtypepackage').val() == '') {
            $('#form-detail #idtypepackage').val('').trigger('change')
        }
        if (!$('#form-detail #idgroupinv').val() == '') {
            $('#form-detail #idgroupinv').val('').trigger('change')
        }
        $('#form-detail #keterangandetail').val('');
    }

    function unsetFormModalDetail() {
        $('#form-modal-detail #namainventor-modal').val('');
        $('#form-modal-detail #keterangan-modal').val('');
        // if (!$('#form-modal-detail #idgudang-modal').val() == '') {
        //     $('#form-modal-detail #idgudang-modal').val('')
        // }
        // if (!$('#form-modal-detail #idcompany-modal').val() == '') {
        //     $('#form-modal-detail #idcompany-modal').val('')
        // }
        // if (!$('#form-modal-detail #iddepartment-modal').val() == '') {
        //     $('#form-modal-detail #iddepartment-modal').val('')
        // }
        $('#form-modal-detail #idsatuan-modal').val('');
        $('#form-modal-detail #konvsatuan-modal').val('');
        $('#form-modal-detail #unitqty-modal').val('');
        $('#form-modal-detail #unitqtysisa-modal').val('');
        $('#form-modal-detail #unitprice-modal').val('');
        $('#form-modal-detail #subtotal-modal').val('');
        $('#form-modal-detail #discountvar-modal').val('');
        $('#form-modal-detail #discount-modal').val('');
        $('#form-modal-detail #nilaidisc-modal').val('');
        $('#form-modal-detail #idtypetax-modal').val('');
        $('#form-modal-detail #idtax-modal').val('');
        $('#form-modal-detail #prosentax-modal').val('');
        $('#form-modal-detail #nilaitax-modal').val('');
        $('#form-modal-detail #hargatotal-modal').val('');
    }

    function setDatatableDetail() {
        $('#table-detail').DataTable();
    }

    function saveData(next_idstatustranc=null, next_nama=null, next_keterangan=null) {
        status.crudmaster = 'read';
        status.cruddetail = 'read';

        let noid = $('#form-header #noid').val();
        let formheader = {};
        let forminternal = {};
        let formfooter = {};
        let formmodaltanggal = {};
        let formmodalsupplier = {};
        let purchasedetail = JSON.parse(localStorage.getItem(main.tabledetail));
        let _validation = {
            tanggal: {
                required: {
                    status: true,
                    message: "'Tanggal' is required!",
                },
                isselect2: false
            },
            tanggaldue: {
                required: {
                    status: true,
                    message: "'Tanggal Due' is required!"
                },
                isselect2: false
            },
            idtypecostcontrol: {
                required: {
                    status: true,
                    message: "'Type Cost Center' is required!"
                },
                isselect2: true
            },
            idcostcontrol: {
                required: {
                    status: true,
                    message: "'Cost Center' is required!"
                },
                isselect2: false
            },
            idcardrequest: {
                required: {
                    status: true,
                    message: "'Request by' is required!"
                },
                isselect2: true
            },
            // idcompany: {
            //     required: {
            //         status: true,
            //         message: "'Company' is required!"
            //     }
            // },
            // iddepartment: {
            //     required: {
            //         status: true,
            //         message: "'Department' is required!"
            //     }
            // },
            idgudang: {
                required: {
                    status: true,
                    message: "'Ship to' is required!"
                },
                isselect2: true
            },
            deliverypoint: {
                required: {
                    status: true,
                    message: "'Delivery Point' is required!"
                },
                isselect2: false
            },
        };
        let _showvalidation = {
            status: false,
            message: ''
        };

        $('#btn-save-data').attr('disabled','disabled');

        $.each($('#form-header').serializeArray(), function(k, v){
            if (v.name in _validation) {
                if (_validation[v.name].required.status) {
                    if (v.value == '') {
                        _showvalidation.status = true;
                        _showvalidation.message += _validation[v.name].required.message+'<br>';
                        
                        if (v.isselect2) {
                            $(`#div-${k} `)
                        }
                    }
                }
            }
            formheader[v.name] = v.value;
        });
        $.each($('#form-internal').serializeArray(), function(k, v){
            forminternal[v.name] = v.value;
        });
        $.each($('#form-footer').serializeArray(), function(k, v){
            formfooter[v.name] = v.value;
        });
        $.each($('#form-modal-tanggal').serializeArray(), function(k, v){
            formmodaltanggal[v.name] = v.value;
        });
        $.each($('#form-modal-supplier').serializeArray(), function(k, v){
            formmodalsupplier[v.name] = v.value;
        });

        if (_showvalidation.status) {
            $.notify({
                message: _showvalidation.message
            },{
                type: 'danger'
            });
            return false;
        }

        if (purchasedetail == null || purchasedetail.length < 1) {
            $.notify({
                message: 'Detail minimal 1!'
            },{
                type: 'danger'
            });
            return false;
        }
        // console.log(forminternal);return false;
        $.ajax({
            type: 'POST',
            header:{
                'X-CSRF-TOKEN': $('#token').val()
            },
            url: '/save'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('#token').val(),
                'statusadd': JSON.stringify(statusadd),
                'next_idstatustranc': JSON.stringify(next_idstatustranc),
                'next_nama': JSON.stringify(next_nama),
                'next_keterangan': JSON.stringify(next_keterangan),
                'noid': noid,
                'formheader': formheader,
                'forminternal': forminternal,
                'formfooter': formfooter,
                'formmodaltanggal': formmodaltanggal,
                'formmodalsupplier': formmodalsupplier,
                'purchasedetail': purchasedetail
            },
            success: function(response) {
                $.notify({
                    message: response.message 
                },{
                    type: 'success'
                });
                // if (statusissued) {
                //     SNST(noid, 2, 'ISSUED')
                // }
                socket.emit('sendNotifToServer', true);
                getDatatable()
                hideModalSNST();
                cancelData()
            }
        });

        console.log('form-modal-detail')
        console.log($('#form-modal-detail').serializeArray())
        return false;
    }

    function viewData(noid) {
        status.crudmaster = 'view';
        status.cruddetail = 'read';
        conditionDetail();

        statusadd = false;
        statusdetail = true;
        setTab('tab-general', 'Header')
        $('#update-modal-detail').hide();

        $.ajax({
            type: 'POST',
            header:{
                'X-CSRF-TOKEN': $('#token').val()
            },
            url: '/find'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('#token').val(),
                'noid': noid
            },
            success: function(response) {
                let pr = response.data.purchase;
                let prd = response.data.purchasedetail;
                let mttts = response.data.mttts;

                $('#form-header #noid').val(noid);
                $('#form-header #kode').val(pr.kode);
                $('#form-header #kodereff').val(pr.kodereff);
                $('#idstatustranc').removeAttr('style');
                $('#idstatustranc-info').css('background-color', pr.mtypetranctypestatus_classcolor);
                $('.idstatustranc-info').text(pr.mtypetranctypestatus_nama);
                $('#idstatuscard').val(pr.idstatuscard);
                $('#idstatuscard-info').text(pr.mcarduser_myusername);
                $('#dostatus').val(pr.dostatus);
                $('#dostatus-info').text(pr.dostatus);
                $('#form-header #tanggal').val(pr.tanggal);
                $('#form-header #tanggaldue').val(pr.tanggaldue);
                $('#form-modal-tanggal #tanggal-modal').val(pr.tanggal);
                $('#form-modal-tanggal #tanggaltax-modal').val(pr.tanggaltax);
                $('#form-modal-tanggal #tanggaldue-modal').val(pr.tanggaldue);
                $('#form-header #idcardsupplier').val(pr.idcardsupplier).trigger('change');

                $('#form-modal-supplier #idakunsupplier-modal').val(pr.idakunsupplier).trigger('change');
                $('#form-modal-supplier #suppliernama-modal').val(pr.suppliernama);
                $('#form-modal-supplier #supplieraddress1-modal').val(pr.supplieraddress1);
                $('#form-modal-supplier #supplieraddress2-modal').val(pr.supplieraddress2);
                $('#form-modal-supplier #supplieraddressrt-modal').val(pr.supplieraddressrt);
                $('#form-modal-supplier #supplieraddressrw-modal').val(pr.supplieraddressrw);
                $('#form-modal-supplier #supplierlokasikel-modal').val(pr.supplierlokasikel);
                $('#form-modal-supplier #supplierlokasikec-modal').val(pr.supplierlokasikec);
                $('#form-modal-supplier #supplierlokasikab-modal').val(pr.supplierlokasikab);
                $('#form-modal-supplier #supplierlokasikab-modal').val(pr.supplierlokasikab);
                $('#form-modal-supplier #supplierlokasiprop-modal').val(pr.supplierlokasiprop);
                $('#form-modal-supplier #supplierlokasineg-modal').val(pr.supplierlokasineg);

                $('#form-header #idcompany').removeAttr('onchange');
                $('#form-header #idcompany').val(pr.idcompany).trigger('change')
                $('#form-header #idcompany').attr('onchange','thenIDCompany(true)').select2('close')
                
                $('#form-header #iddepartment').removeAttr('onchange');
                $('#form-header #iddepartment').val(pr.iddepartment).trigger('change')
                $('#form-header #iddepartment').attr('onchange','thenIDDepartment(true)').select2('close')

                $('#form-header #idgudang').removeAttr('onchange');
                $('#form-header #idgudang').val(pr.idgudang).trigger('change')
                $('#form-header #idgudang').attr('onchange','thenTanggalDue(true)').select2('close')

                $('#form-header #deliverypoint').val(pr.deliverypoint);

                // $('#form-header #idcompany').val(pr.idcompany).trigger('change');
                // $('#form-header #iddepartment').val(pr.iddepartment).trigger('change');
                // $('#form-header #idgudang').val(pr.idgudang).trigger('change');
                $('#form-header #idtypecostcontrol').val(pr.idtypecostcontrol).trigger('change');
                $('#form-header #kodecostcontrol').val(pr.kodecostcontrol);

                $('#form-internal #idcashbankbayar #idcashbankbayar-'+(pr.idcashbankbayar ? pr.idcashbankbayar : 'empty')).attr('selected', 'selected');
                $('#select2-idcashbankbayar-container').text($('#form-internal #idcashbankbayar #idcashbankbayar-'+(pr.idcashbankbayar ? pr.idcashbankbayar : 'empty')).text());
                $('#form-internal #idakunpersediaan #idakunpersediaan-'+(pr.idakunpersediaan ? pr.idakunpersediaan : 'empty')).attr('selected', 'selected');
                $('#select2-idakunpersediaan-container').text($('#form-internal #idakunpersediaan #idakunpersediaan-'+(pr.idakunpersediaan ? pr.idakunpersediaan : 'empty')).text());
                $('#form-internal #idakunpurchase #idakunpurchase-'+(pr.idakunpurchase ? pr.idakunpurchase : 'empty')).attr('selected', 'selected');
                $('#select2-idakunpurchase-container').text($('#form-internal #idakunpurchase #idakunpurchase-'+(pr.idakunpurchase ? pr.idakunpurchase : 'empty')).text());

                localStorage.setItem(main.tabledetail, JSON.stringify(prd));
                getDatatableDetail(localStorage.getItem(main.tabledetail));
                
                $('#form-footer #keterangan').val(pr.keterangan);


                $('#form-header #kode').attr('disabled', 'disabled');
                $('#form-header #generatekode').attr('disabled', 'disabled');
                $('#form-header #kodereff').attr('disabled', 'disabled');
                $('#form-header #tanggal').attr('disabled', 'disabled');
                $('#form-header #tanggaldue').attr('disabled', 'disabled');
                $('#form-header #idtypecostcontrol').attr('disabled', 'disabled');
                // $('#form-header #cost-control').hide();
                $('#form-modal-tanggal #tanggal-modal').attr('disabled', 'disabled');
                $('#form-modal-tanggal #tanggaltax-modal').attr('disabled', 'disabled');
                $('#form-modal-tanggal #tanggaldue-modal').attr('disabled', 'disabled');
                $('#form-header #idcardsupplier').attr('disabled', 'disabled');
                $('#form-modal-supplier #idakunsupplier-modal').attr('disabled', 'disabled');
                $('#form-modal-supplier #suppliernama-modal').attr('disabled', 'disabled');
                $('#form-modal-supplier #supplieraddress1-modal').attr('disabled', 'disabled');
                $('#form-modal-supplier #supplieraddress2-modal').attr('disabled', 'disabled');
                $('#form-modal-supplier #supplieraddressrt-modal').attr('disabled', 'disabled');
                $('#form-modal-supplier #supplieraddressrw-modal').attr('disabled', 'disabled');
                $('#form-modal-supplier #supplierlokasikel-modal').attr('disabled', 'disabled');
                $('#form-modal-supplier #supplierlokasikec-modal').attr('disabled', 'disabled');
                $('#form-modal-supplier #supplierlokasikab-modal').attr('disabled', 'disabled');
                $('#form-modal-supplier #supplierlokasikab-modal').attr('disabled', 'disabled');
                $('#form-modal-supplier #supplierlokasiprop-modal').attr('disabled', 'disabled');
                $('#form-modal-supplier #supplierlokasineg-modal').attr('disabled', 'disabled');
                $('#form-detail #idinventor').attr('disabled', 'disabled');
                $('#form-detail #namainventor').attr('disabled', 'disabled');
                $('#form-detail #unitqty').attr('disabled', 'disabled');
                $('#form-detail #update-detail').hide();
                $('#form-detail #save-detail').hide();
                $('#form-modal-detail #idinventor-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #namainventor-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #idtypepackage-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #idgroupinv-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #keterangan-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #idgudang-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #idcompany-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #iddepartment-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #idsatuan-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #konvsatuan-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #unitqty-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #unitqtysisa-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #unitprice-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #subtotal-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #discountvar-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #discount-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #nilaidisc-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #idtypetax-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #idtax-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #prosentax-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #nilaitax-modal').attr('disabled', 'disabled');
                $('#form-modal-detail #hargatotal-modal').attr('disabled', 'disabled');
                $('#form-header #idcompany').attr('disabled', 'disabled');
                $('#form-header #iddepartment').attr('disabled', 'disabled');
                $('#form-header #idcardrequest').attr('disabled', 'disabled');
                $('#form-header #idgudang').attr('disabled', 'disabled');
                $('#form-header #deliverypoint').attr('disabled', 'disabled');
                $('#form-footer #keterangan').attr('disabled', 'disabled');
                $('#form-internal #idcashbankbayar').attr('disabled', 'disabled');
                $('#form-internal #idakunpersediaan').attr('disabled', 'disabled');
                $('#form-internal #idakunpurchase').attr('disabled', 'disabled');
                
                $('#btn-saveto').html(getButtonSaveTo(pr,mttts,true));

                // $('#form-detail').hide();
                $('#tools-dropdown').hide();
                $('#from-departement').show();
                $('#tabledepartemen').hide();
                $('#tool-kedua').show();
                $('#tool-pertama').hide();
                $('#space_filter_button').hide();
                $('#tool_refresh').hide();
                $('#tool_filter').hide()
                $('#tool_refresh_up').hide();
            }
        });
        return false;
    }

    function getButtonSaveTo(pr,mttts,fromview) {
        let btnsaveto = '';
        if (fromview) {
            if (mttts.isinternal == 1 && mttts.isexternal == 0) {
                if (mttts.prev_idstatusbase != null && mttts.next_idstatusbase != null) {
                    if (mttts.next_idstatusbase == 4) {
                        btnsaveto = `<a class="btn btn-sm btn-success flat btn-view" style="background-color: `+mttts.next_classcolor+`; color: white; margin-right: ${<?= Session::get('groupuser') ?> == 0 ? '-4px' : '0px'}" onclick="showModalSendToNext({noid:${pr.noid},kode:'${pr.kode}',idtypetranc:${pr.idtypetranc},fromview:true})">
                                        <i class="icon-save text-light"></i> Send to `+mttts.next_table+`
                                    </a>`;
                        if (<?= Session::get('groupuser') ?> == 0) {
                            btnsaveto += `<div class="btn-group btn-view">
                                <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-sm flat" style="background-color: `+mttts.next_classcolor+`; color: white">
                                    <i class="icon-angle-down pr-0 text-light"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" style="min-width: 200px; padding: 5px">
                                    <div class="row">
                                        <button type="button" class="btn col-md-12" style="background-color: `+mttts.prev_classcolor+`; color: white" onclick="showModalSNST({noid:${pr.noid},idtypetranc:${pr.idtypetranc},idstatustranc:${mttts.prev_noid},statustranc:${mttts.prev_nama}',fromview:1,idtypecostcontrol:${pr.idtypecostcontrol},idcostcontrol:'${pr.idcostcontrol}'})"><i class="icon-save text-light"></i> Save to `+mttts.prev_nama+`</button>
                                    </div>
                                </div>
                            </div>`;
                        }
                    } else {
                        if (<?= Session::get('groupuser') ?> == 0) {
                            btnsaveto += `<a class="btn btn-sm flat btn-view" style="background-color: `+mttts.next_classcolor+`; color: white; margin-right: ${<?= Session::get('groupuser') ?> == 0 ? '-4px' : '0px'}" onclick="showModalSNST({noid:${pr.noid},idtypetranc:${pr.idtypetranc},idstatustranc:${mttts.next_noid},statustranc:${mttts.next_nama}',fromview:1,idtypecostcontrol:${pr.idtypecostcontrol},idcostcontrol:'${pr.idcostcontrol}'})">
                                            <i class="icon-save text-light"></i> Save to `+mttts.next_nama+`
                                        </a>`;
                            btnsaveto += `<div class="btn-group btn-view">
                                <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-sm flat" style="background-color: `+mttts.next_classcolor+`; color: white">
                                    <i class="icon-angle-down pr-0 text-light"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" style="min-width: 200px; padding: 5px">
                                    <div class="row">
                                        <button type="button" class="btn col-md-12" style="background-color: `+mttts.prev_classcolor+`; color: white" onclick="showModalSNST({noid:${pr.noid},idtypetranc:${pr.idtypetranc},idstatustranc:${mttts.prev_noid},statustranc:${mttts.prev_nama}',fromview:1,idtypecostcontrol:${pr.idtypecostcontrol},idcostcontrol:'${pr.idcostcontrol}'})"><i class="icon-save text-light"></i> Save to `+mttts.prev_nama+`</button>
                                    </div>
                                </div>
                            </div>`;
                        }
                    }
                } else if (mttts.prev_idstatusbase == null && mttts.next_idstatusbase != null) {
                    if (<?= Session::get('groupuser') ?> == 0) {
                        btnsaveto = `<a class="btn btn-sm flat btn-view" style="background-color: `+mttts.next_classcolor+`; color: white;" onclick="showModalSNST({noid:${pr.noid},idtypetranc:${pr.idtypetranc},idstatustranc:${mttts.next_noid},statustranc:${mttts.next_nama}',fromview:1,idtypecostcontrol:${pr.idtypecostcontrol},idcostcontrol:'${pr.idcostcontrol}'})">
                                        <i class="icon-save text-light"></i> Save to `+mttts.next_nama+`
                                    </a>`;
                    }
                }
            } else {
                btnsaveto += `<div class="btn-group">
                                    <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-success btn-sm flat">
                                        Set Status <i class="icon-angle-down pr-0"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" style="min-width: 200px; padding: 0px 5px 5px 5px">`;

                $.each(mttts.btnexternal, function(k,v) {
                    btnsaveto += `<div class="row" style="margin-top: 5px">
                                        <button type="button" class="btn col-md-12" style="background-color: `+v.classcolor+`; color: white;" onmouseover="mouseOver('save-to-issued', '#1f8e87')" onmouseout="mouseOut('save-to-issued', '`+v.classcolor+`')" id="save-to-issued" onclick="showModalSNST({noid:${pr.noid},idtypetranc:501,idstatustranc:${v.noid},statustranc:${v.nama}',fromview:1,idtypecostcontrol:${pr.idtypecostcontrol},idcostcontrol:'${pr.idcostcontrol}'})"><i class="`+v.classicon+` text-light"></i> <span>Set to `+v.nama+`<span></button>
                                    </div>`;
                })

                btnsaveto += `</div>
                            </div>`;
            }
        } else {
            btnsaveto += `<a class="btn btn-sm btn-success flat" id="save-standard" style="margin-right: ${<?= Session::get('groupuser') ?> == 0 ? '-4px' : '0px'}" onclick="saveData()">
                        <i class="icon-save"></i> Save
                    </a>`;
            if (<?= Session::get('groupuser') ?> == 0) {
                btnsaveto += `<div class="btn-group btn-view">
                    <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-sm btn-success flat" style="color: white">
                        <i class="icon-angle-down pr-0 text-light"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" style="min-width: 200px; padding: 5px">
                        <div class="row">
                            <button type="button" class="btn col-md-12" style="background-color: `+mttts.next_classcolor+`; color: white" onclick="showModalSNST({noid:${pr.noid},idtypetranc:${pr.idtypetranc},idstatustranc:${mttts.next_noid},statustranc:${mttts.next_nama}',fromview:${fromview?1:0},idtypecostcontrol:${pr.idtypecostcontrol},idcostcontrol:'${pr.idcostcontrol}'})"><i class="icon-save text-light"></i> Save to `+mttts.next_nama+`</button>
                        </div>
                    </div>
                </div>`;
            }
        }
        return btnsaveto;
    }

    function viewLog(noid) {
        alert(noid)
    }

    function printData(noid, urlprint) {
        window.open('http://'+$(location).attr('host')+'/'+noid+'/'+urlprint, '_blank');
    }

    function sendToNext(noid) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You want Sent to Purchase Offer?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            target: '#pagepanel'
        }).then((result) => {
            if (result.isConfirmed) {
                let formmodalstn = $('#form-modal-stn').serializeArray();
                $.ajax({
                    type: 'POST',
                    header:{
                        'X-CSRF-TOKEN': $('#token').val()
                    },
                    url: '/send_to_next'+main.urlcode,
                    dataType: 'json',
                    data: {
                        '_token': $('#token').val(),
                        'noid': noid,
                        // 'idcardsupplier': formmodalstn[0].value,
                        'idcardsupplier': 0,
                    },
                    success: function(response) {
                        if (response.success) {
                            socket.emit('sendNotifToServer', true);
                            Swal.fire({
                                icon: 'success',
                                title: 'Success!',
                                text: 'Sent to Purchase Offer successfuly.',
                                target: '#pagepanel',
                            });
                            $.notify({
                                message: response.message 
                            },{
                                type: 'success'
                            });
                        } else{
                            $.notify({
                                message: response.message 
                            },{
                                type: 'danger'
                            });
                        }
                        getDatatable();
                        hideModalSendToOrder();
                    }
                });
            }
        })
    }

    function SNST(noid, idstatustranc, statustranc, fromview) {
        $.ajax({
            type: 'POST',
            header:{
                'X-CSRF-TOKEN': $('#token').val()
            },
            url: '/snst'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('#token').val(),
                'noid': noid,
                'idstatustranc': idstatustranc,
                'statustranc': statustranc,
                'idtypetranc': $('#form-modal-snst #idtypetranc-snst').val(),
                'keterangan': $('#form-modal-snst #keterangan-snst').val(),
            },
            success: function(response) {
                // if (idstatustranc == 5014) {
                //     sendToNext(noid)
                // }

                getDatatable();
                if (response.success) {
                    socket.emit('sendNotifToServer', true);
                    $.notify({
                        message: response.message 
                    },{
                        type: 'success'
                    });
                    // recreateNotification()
                } else{
                    $.notify({
                        message: response.message 
                    },{
                        type: 'danger'
                    });
                }
                hideModalSNST();
                if (fromview) {
                    cancelData();
                }
            }
        });
    }

    function DIV(noid, idstatustranc) {
        let ist = $('#form-modal-div #idstatustranc-div :selected').val();

        $.ajax({
            type: 'POST',
            header:{
                'X-CSRF-TOKEN': $('#token').val()
            },
            url: '/div'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('#token').val(),
                'noid': noid,
                'idtypetranc': $('#form-modal-div #idtypetranc-div').val(),
                'idstatustranc': ist,
                'statustranc': $('#form-modal-div #idstatustranc-div :selected').attr('data-mtypetranctypestatus-nama'),
                'keterangan': $('#form-modal-div #keterangan-div').val(),
            },
            success: function(response) {
                getDatatable();
                if (response.success) {
                    $.notify({
                        message: response.message 
                    },{
                        type: 'success'
                    });
                } else{
                    $.notify({
                        message: response.message 
                    },{
                        type: 'danger'
                    });
                }
                hideModalDIV();
            }
        });
    }

    function SAPCF(noid, idstatustranc) {
        let ist = $('#form-modal-sapcf #idstatustranc-sapcf :selected').val();
        let st = $('#form-modal-sapcf #idstatustranc-sapcf :selected').attr('data-mtypetranctypestatus-nama');
        let k = $('#form-modal-sapcf #keterangan-sapcf').val();

        if (ist == '' || k == '') {
            $.notify({
                message: 'Complete data, please!'
            },{
                type: 'danger'
            });
            return false;
        }

        $.ajax({
            type: 'POST',
            header:{
                'X-CSRF-TOKEN': $('#token').val()
            },
            url: '/sapcf'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('#token').val(),
                'noid': noid,
                'idtypetranc': $('#form-modal-sapcf #idtypetranc-sapcf').val(),
                'idstatustranc': ist,
                'statustranc': st,
                'keterangan': k,
            },
            success: function(response) {
                getDatatable();
                if (response.success) {
                    $.notify({
                        message: response.message 
                    },{
                        type: 'success'
                    });
                } else{
                    $.notify({
                        message: response.message 
                    },{
                        type: 'danger'
                    });
                }
                hideModalSAPCF();
            }
        });
    }

    function setPending(noid) {
        $.ajax({
            type: 'POST',
            header:{
                'X-CSRF-TOKEN': $('#token').val()
            },
            url: '/set_pending'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('#token').val(),
                'noid': noid,
                'idtypetranc': $('#form-modal-set-pending #idtypetranc-pending').val(),
                'keterangan': $('#form-modal-set-pending #keterangan-pending').val(),
            },
            success: function(response) {
                getDatatable();
                if (response.success) {
                    $.notify({
                        message: response.message 
                    },{
                        type: 'success'
                    });
                } else{
                    $.notify({
                        message: response.message 
                    },{
                        type: 'danger'
                    });
                }
                hideModalSetPending();
            }
        });
    }

    function setCanceled(noid) {
        $.ajax({
            type: 'POST',
            header:{
                'X-CSRF-TOKEN': $('#token').val()
            },
            url: '/set_canceled'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('#token').val(),
                'noid': noid,
                'idtypetranc': $('#form-modal-set-canceled #idtypetranc-canceled').val(),
                'keterangan': $('#form-modal-set-canceled #keterangan-canceled').val(),
            },
            success: function(response) {
                getDatatable();
                if (response.success) {
                    $.notify({
                        message: response.message 
                    },{
                        type: 'success'
                    });
                } else{
                    $.notify({
                        message: response.message 
                    },{
                        type: 'danger'
                    });
                }
                hideModalSetCanceled();
            }
        });
    }

    function editData(noid) {
        status.crudmaster = 'update';
        status.cruddetail = 'read';
        conditionDetail();

        ready = false;
        statusadd = false;
        statusdetail = false;
        setTab('tab-general', 'Header')
        $('#update-modal-detail').show();

        $.ajax({
            type: 'POST',
            header:{
                'X-CSRF-TOKEN': $('#token').val()
            },
            url: '/find'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('#token').val(),
                'noid': noid
            },
            success: function(response) {
                let pr = response.data.purchase;
                let prd = response.data.purchasedetail;
                let mttts = response.data.mttts;

                $('#form-header #noid').val(noid);
                $('#form-header #kode').val(pr.kode);
                $('#form-header #kodereff').val(pr.kodereff);
                $('#idstatustranc').val(pr.idstatustranc);
                $('#idstatustranc').removeAttr('style');
                $('#idstatustranc-info').css('background-color', pr.mtypetranctypestatus_classcolor);
                $('.idstatustranc-info').text(pr.mtypetranctypestatus_nama);
                $('#idstatuscard').val(pr.idstatuscard);
                $('#idstatuscard-info').text(pr.mcarduser_myusername);
                $('#dostatus').val(pr.dostatus);
                $('#dostatus-info').text(pr.dostatus);
                $('#form-header #tanggal').val(pr.tanggal);
                $('#form-header #tanggaldue').val(pr.tanggaldue);
                // $('#form-header #cost-control').show();
                $('#form-modal-tanggal #tanggal-modal').val(pr.tanggal);
                $('#form-modal-tanggal #tanggaltax-modal').val(pr.tanggaltax);
                $('#form-modal-tanggal #tanggaldue-modal').val(pr.tanggaldue);
                $('#form-header #idcardsupplier').val(pr.idcardsupplier).trigger('change');
                // $('#form-modal-supplier #idakunsupplier-modal').val(pr.idakunsupplier).trigger('change');
                $('#form-modal-supplier #suppliernama-modal').val(pr.suppliernama);
                $('#form-modal-supplier #supplieraddress1-modal').val(pr.supplieraddress1);
                $('#form-modal-supplier #supplieraddress2-modal').val(pr.supplieraddress2);
                $('#form-modal-supplier #supplieraddressrt-modal').val(pr.supplieraddressrt);
                $('#form-modal-supplier #supplieraddressrw-modal').val(pr.supplieraddressrw);
                $('#form-modal-supplier #supplierlokasikel-modal').val(pr.supplierlokasikel);
                $('#form-modal-supplier #supplierlokasikec-modal').val(pr.supplierlokasikec);
                $('#form-modal-supplier #supplierlokasikab-modal').val(pr.supplierlokasikab);
                $('#form-modal-supplier #supplierlokasikab-modal').val(pr.supplierlokasikab);
                $('#form-modal-supplier #supplierlokasiprop-modal').val(pr.supplierlokasiprop);
                $('#form-modal-supplier #supplierlokasineg-modal').val(pr.supplierlokasineg);
                
                $('#form-header #idcompany').removeAttr('onchange');
                $('#form-header #idcompany').val(pr.idcompany).trigger('change')
                $('#form-header #idcompany').attr('onchange','thenIDCompany(true)').select2('close')
                
                $('#form-header #iddepartment').removeAttr('onchange');
                $('#form-header #iddepartment').val(pr.iddepartment).trigger('change')
                $('#form-header #iddepartment').attr('onchange','thenIDDepartment(true)').select2('close')

                $('#form-header #idgudang').removeAttr('onchange');
                $('#form-header #idgudang').val(pr.idgudang).trigger('change')
                $('#form-header #idgudang').attr('onchange','thenTanggalDue(true)').select2('close')

                $('#form-header #idcardrequest').removeAttr('onchange');
                $('#form-header #idcardrequest').val(pr.idcardrequest).trigger('change')
                $('#form-header #idcardrequest').attr('onchange','thenTanggalDue(true)').select2('close')
                
                $('#form-header #deliverypoint').val(pr.deliverypoint);

                $('#form-header #idtypecostcontrol').val(pr.idtypecostcontrol).trigger('change');
                $('#form-header #idcostcontrol').val(pr.idcostcontrol);
                $('#form-header #kodecostcontrol').val(pr.kodecostcontrol);
                
                $('#form-internal #idcashbankbayar #idcashbankbayar-'+(pr.idcashbankbayar ? pr.idcashbankbayar : 'empty')).attr('selected', 'selected');
                $('#select2-idcashbankbayar-container').text($('#form-internal #idcashbankbayar #idcashbankbayar-'+(pr.idcashbankbayar ? pr.idcashbankbayar : 'empty')).text());
                $('#form-internal #idakunpersediaan #idakunpersediaan-'+(pr.idakunpersediaan ? pr.idakunpersediaan : 'empty')).attr('selected', 'selected');
                $('#select2-idakunpersediaan-container').text($('#form-internal #idakunpersediaan #idakunpersediaan-'+(pr.idakunpersediaan ? pr.idakunpersediaan : 'empty')).text());
                $('#form-internal #idakunpurchase #idakunpurchase-'+(pr.idakunpurchase ? pr.idakunpurchase : 'empty')).attr('selected', 'selected');
                $('#select2-idakunpurchase-container').text($('#form-internal #idakunpurchase #idakunpurchase-'+(pr.idakunpurchase ? pr.idakunpurchase : 'empty')).text());

                localStorage.setItem(main.tabledetail, JSON.stringify(prd));
                getDatatableDetail(localStorage.getItem(main.tabledetail));
                setTab('tab-general', 'Header')
                
                $('#form-footer #keterangan').val(pr.keterangan);

                $('#btn-saveto').html(getButtonSaveTo(pr,mttts,false));

                $('#tools-dropdown').hide();
                // $('#form-detail').show();
                $('#form-detail #update-detail').hide();
                $('#from-departement').show();
                $('#tabledepartemen').hide();
                $('#tool-kedua').show();
                $('#tool-pertama').hide();
                $('#space_filter_button').hide();
                $('#tool_refresh').hide();
                $('#tool_filter').hide()
                $('#tool_refresh_up').hide();
            }
        });
        ready = true;
    }

    function removeData(noid) {
        $.ajax({
            type: 'POST',
            header:{
                'X-CSRF-TOKEN': $('#token').val()
            },
            url: '/delete'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('#token').val(),
                'noid': noid
            },
            success: function(response) {
                $.notify({
                    message: response.message 
                },{
                    type: 'success'
                });
                getDatatable()
                hideModalDelete()
            }
        });
    }

    function removeDetail(noid) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You want to delete this detail?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            target: '#pagepanel'
        }).then((result) => {
            if (result.isConfirmed) {
                let data = new Array();
                $.each(JSON.parse(localStorage.getItem(main.tabledetail)), function(k, v) {
                    if (noid != k) {
                        data.push(v);
                    }
                })
                localStorage.removeItem(main.tabledetail)
                localStorage.setItem(main.tabledetail, JSON.stringify(data))

                Swal.fire({
                    icon: 'success',
                    title: 'Deleted!',
                    text: 'Delete data successfully.',
                    target: '#pagepanel',
                });

                getDatatableDetail(localStorage.getItem(main.tabledetail))
            }
        })
    }

    function getDatatable() {
        var columns = <?= $columns ?>;
        var columndefs = <?= $columndefs ?>;

        $('#table_pagepanel_9921050101').DataTable().destroy();
        mytable = $('#table_pagepanel_9921050101').DataTable({
            "autoWidth": false,
            "pageLength": 10,
            "searching": true,
            "processing": true,
            "language": {
                'loadingRecords': '&nbsp;',
                'processing': 'Loading...'
            },
            "serverSide": true,
            "bInfo": true,
            "bFilter": false,
            "bLengthChange": false,
            "columns": [
                {mData:'checkbox',orderable:false,class:'text-center'},
                {mData:'no',orderable:false,class:'text-right'},
                // {mData:'idtypetranc'},
                {mData:'idstatustranc',class:'text-center'},
                {mData:'kode'},
                // {mData:'kodereff'},
                // {mData:'tanggal'},
                {mData:'tanggaldue'},
                // {mData:'totalsubtotal',class:'text-right',render:$.fn.dataTable.render.number(',','.',0,'Rp.')},
                // {mData:'generaltotal',class:'text-right',render:$.fn.dataTable.render.number(',','.',0,'Rp.')},
                // {mData:'keterangan'},
                // {mData:'kodecostcontrol'},
                // {mData:'idcardsupplier'},
                // {mData:'idcompany'},
                // {mData:'iddepartment'},
                // {mData:'idgudang'},
                {mData:'idcardkoor_nama'},
                {mData:'idcardpj_nama'},
                // {mData:'idcardsitekoor_nama'},
                // {mData:'idcarddrafter_nama'},
                {mData:'idcardrequest_nama'},
                {mData:'action',orderable:false},
            ],
            "columnDefs": columndefs,   
            "infoCallback": function( settings, start, end, max, total, pre ) {
                var options = '';
                $.each(settings.aLengthMenu, function(k, v) {
                    options += '<option value="'+v+'" id="alm'+v+'" class="alm">'+v+'</option>'
                })
                if ((end-start) == settings._iDisplayLength-1) {
                    var valuepage = end/settings._iDisplayLength;
                } else {
                    var valuepage = ((start-1)/settings._iDisplayLength)+1;
                }
                if (start == 1) {
                    var prevdisabled = 'disabled'
                } else {
                    var prevdisabled = '';
                }
                if (end == total) {
                    var nextdisabled = 'disabled'
                } else {
                    var nextdisabled = '';
                }
                // if (settings.oPreviousSearch.sSearch == $('#searchform').val()) {
                //     alert($('#searchform').val())
                //     var valuesearch = $('#searchform').val()
                // } else {
                //     var valuesearch = $('#searchform').val()
                //     alert('else'+$('#searchform').val())
                // }
                if (settings.oPreviousSearch.sSearch != '') {
                    statussearch = true
                    searchtablewidth = 200
                    valuesearch = settings.oPreviousSearch.sSearch
                } else {
                    statussearch = false
                    searchtablewidth = 30
                    valuesearch = ''
                }
                $('#appendInfoDatatable').html(`
                <div class="row" style="background: #90CAF9; padding-left: 10px; padding-right: 10px; height: 40px; height: 45px;">
                    <div class="col-md-12" style="top: 3px;">
                        Page
                        <button id="prevpage" onclick="prevpage()" class="btn btn-sm default prev `+prevdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-left">
                            </i>
                        </button>
                        <input type="text" id="valuepage" onkeyup="valuepage()" class="input-mini" value="`+valuepage+`" style="text-align: center; border: 0px; margin: 5px 5px 5px 5px">
                        <button id="nextpage" onclick="nextpage()" class="btn btn-sm default next `+nextdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-right">
                            </i>
                        </button> of <span class="pagination-panel-total">`+
                            (settings._iRecordsTotal == settings._iDisplayLength ? 1 : parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)
                        +`</span>
                        <span class="seperator">|</span>
                        Rec/Page 
                        <select id="alengthmenu" onchange="alengthmenu()" name="table_pagepanel_9921050101_length" aria-controls="table_pagepanel_9921050101" class="input-inline input-xsmall input-sm recofdt" style="border: 0px; margin: 5px 5px 5px 5px">
                            `+options+`
                        </select>
                        <span class="seperator">|</span>Total `+settings._iRecordsTotal+` records
                        <input id="searchform" onkeyup="searchform()" value="`+valuesearch+`" type="search" class="form-control input-inline input-sm clicked" placeholder="" aria-controls="table_pagepanel_9921050101" style="width: `+searchtablewidth+`px; position: absolute; right: 0px; top: 4px; height: 70%; border: 0;">
                        <div onclick="searchtable()" class="btn fa fa-search text-right" style="position: absolute; right: 0px; top: 4px; height: 70%; background: transparent; margin: 0px 0px 0px 0px; padding: 5px 0px 5px 0px">
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                </div>`)
                console.log('settings')
                console.log(settings)
                console.log(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)
                $('.alm').removeAttr('selected');
                $('#alm'+settings._iDisplayLength).attr('selected', 'selected');
                // console.log(valuepage)
                $('.dataTables_info').css('display', 'none')
                $('.dataTables_paginate').css('display', 'none')
            },
            "ajax": {
                "url": "/get"+main.urlcode,
                "type": "POST",
                "dataType":'json',
                "data": {
                    "_token":$('#token').val(),
                    "filter":selectedfilter,
                    "datefilter":datefilter,
                    "datefilter2":datefilter2,
                },
                "dataSrc": function(json) {
                    selectcheckboxman = json.allnoid;
                    localStorage.setItem('sc-m-an', JSON.stringify(json.allnoid))
                    if (json.datefilter.type == 'a') {
                        $('#dropdown-date-filter span').text('All')
                    } else {
                        $('#dropdown-date-filter span').text(json.datefilter.from+' - '+json.datefilter.to)
                    }
                    return json.data;
                },
                "error": function(res) {
                    if (res.status == 419) {
                        Swal.fire({
                            icon: 'warning',
                            title: 'Warning!',
                            text: 'Silahkan login kembali.',
                            target: '#pagepanel',
                        });
                    }
                }
            },
            "pagingType": "input",
            "createdRow": function(row, data, dataIndex) {
                // console.log('datarowwww')
                // console.log(data.noid)
                $(row).addClass('my_row');
                $(row).addClass('row-m-'+data.noid);
                // // $(row).css('height', '5px');
                $(row).attr('onmouseover', 'showButtonAction(this, '+dataIndex+')');
                $(row).attr('onmouseout', 'resetCss()');
                // $(row).attr('onmouseover', 'getPositionXY(this)');

                let scm = JSON.parse(localStorage.getItem('sc-m'));
                if (scm.includes(data.noid)) {
                    console.log('rowwwww')
                    console.log($(row).children('td').children('input').val('TRUE'))
                    $(row).css('background-color', '#90CAF9');
                    $(row).children('td:first').html('<input type="checkbox" value="TRUE" class="checkbox-m" id="checkbox-m-'+data.noid+'" onchange="selectCheckboxM('+data.noid+')" checked>');
                }
            },
            "oLanguage":{
                "loadingRecords": "Please wait - loading...",
                "sProcessing": "<div id='loadernya'></div>",
                "sInfo": "&nbsp;|&nbsp;Total&nbsp;_TOTAL_ records",
                "sLengthMenu": "&nbsp;|&nbsp;Rec/Page&nbsp;_MENU_",
                "sInfoFiltered": "",
                "sInfoEmpty": "| No records found to show",
                "sEmptyTable": "No data available in table",
                "sZeroRecords": "No matching records found",
                "sSearch": "<button onclick='toggleButtonSearch()' class='btn flat dtfilter_search_btn' style='background-color: white'><i class='fa fa-search'></i></button>",
                "oPaginate": {
                    "sPrevious": "<i class='fa fa-angle-left'></i>",
                    "sNext": "<i class='fa fa-angle-right'></i> ",
                }
            },
            footerCallback: function ( row, data, start, end, display ) {
                var api = this.api(), data;
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
    
                var sum = function ( i ) {
                    var value = 0;
                    $.each(api.column( i ).data(), function(k, v) {
                        var _split = v.split('.');
                        var value2 = '';
                        $.each(_split, function(k2, v2) {
                            value2 += v2
                        })
                        value += Number(value2)
                    })
                    return value
                }
                var addcommas = function addCommas(nStr) {
                    nStr += '';
                    var x = nStr.split('.');
                    var x1 = x[0];
                    var x2 = x.length > 1 ? '.' + x[1] : '';
                    var rgx = /(\d+)(\d{3})/;
                    while (rgx.test(x1)) {
                        x1 = x1.replace(rgx, '$1' + '.' + '$2');
                    }
                    return x1 + x2;
                }

                // pageTotal = api
                //     .column( 9, { page: 'current'} )
                //     .data()
                //     .reduce( function (a, b) {
                //         return intVal(a) + intVal(b);
                //     }, 0 );
                // Update footer
                // console.log('akak')
                // console.log(eval(api.column(9).data()[0]))
                $( api.column(4).footer() ).html(data.length);
                // $( api.column(8).footer() ).html(api.column(8).data().reduce( function (a, b) { return parseInt(a)+parseInt(b); }, 0 ));
                // var $th8 = $(row).find('th').eq(8);
                // $th8.text( $.fn.dataTable.render.number(',', '.', 0, 'Rp.').display( $th8.text() )) 
                // $( api.column(9).footer() ).html(api.column(9).data().reduce( function (a, b) { return parseInt(a)+parseInt(b); }, 0 ));
                // var $th9 = $(row).find('th').eq(9);
                // $th9.text( $.fn.dataTable.render.number(',', '.', 0, 'Rp.').display( $th9.text() )) 
                
                // $( api.column(6).footer() ).html(data.length);
            }
        });

        new $.fn.dataTable.Buttons( mytable, {
            buttons: [
                {
                    autoFilter: true,
                    text:      '<i class="fa fa-trash"></i> Delete Selected',
                    titleAttr: 'Delete Selected',
                    className: 'btn btn-danger col-md-12',
                    action:  function(e, dt, button, config) {
                        let scm = JSON.parse(localStorage.getItem('sc-m'));
                        if (scm.length == 0) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'There\'s no selected!',
                                target: '#pagepanel'
                            })
                        } else {
                            Swal.fire({
                                title: 'Are you sure?',
                                text: "You want to delete this selected?",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                target: '#pagepanel'
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    removeSelected(scm)
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'Deleted!',
                                        text: 'Delete data successfully.',
                                        target: '#pagepanel',
                                    });
                                }
                            })
                        }
                    },
                },{
                    autoFilter: true,
                    text:      '<i class="fa fa-trash"></i> Delete All',
                    titleAttr: 'Delete All',
                    className: 'btn btn-danger col-md-12',
                    action:  function(e, dt, button, config) {
                        let scma = JSON.parse(localStorage.getItem('sc-m-a'));
                        let scman = JSON.parse(localStorage.getItem('sc-m-an'));
                        let scm = JSON.parse(localStorage.getItem('sc-m'));
                        // if (scma) {
                            Swal.fire({
                                title: 'Are you sure?',
                                text: "You want to delete all data?",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                target: '#pagepanel'
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    removeSelected(scman)
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'Deleted!',
                                        text: 'Delete data successfully.',
                                        target: '#pagepanel',
                                    });
                                }
                            })
                        // } else {
                        //     Swal.fire({
                        //         icon: 'error',
                        //         title: 'Oops...',
                        //         text: 'Something went wrong!',
                        //         footer: '<a href>Why do I have this issue?</a>'
                        //     })
                        // }
                    },
                },{
                    extend:    'excel',
                    autoFilter: true,
                    text:      '<i class="fa fa-file-excel"></i> Export to Excel',
                    titleAttr: 'Export to Excel',
                    title: $("#judul_excel").val(),
                    className: 'btn btn-success col-md-12 mb-0',
                    exportOptions: {
                        modifier: {
                            search: 'applied',
                            order: 'applied'
                        },
                        columns: [ 1,2,3, ':visible' ]
                        //                  <a style="font-size: 14px;min-width: 175px;
                        // text-align: left;" href="javascript:;"  class="dropdown-item">
                        // <i style="margin-top:10px;margin-right: 5px;" class="fa fa-file-excel"></i> Export to Excel </a>
                                        // columns: [ 0, ':visible' ]
                    },
                    action:  function(e, dt, button, config) {
                    // $('.loading').fadeIn();
                        var that = this;
                        setTimeout(function () {
                            $.fn.dataTable.ext.buttons.excelHtml5.action.call(that,e, dt, button, config);
                            // console.log(that);
                            // console.log(e);
                            // console.log(dt);
                            // console.log(button);
                            // console.log(config);
                            $('.loading').fadeOut();
                        },500);
                    },
                }
            ]
            // infoDatatable()
        });
        // infoDatatable()

        mytable.buttons().container().appendTo('#drop_export2');

        $('#pagepanel-body').css('min-height', screen.height-(screen.height/3))
    }

    function deleteSelected() {
        let scm = JSON.parse(localStorage.getItem('sc-m'));
        if (scm.length == 0) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'There\'s no selected!',
                target: '#pagepanel'
            })
        } else {
            Swal.fire({
                title: 'Are you sure?',
                text: "You want to delete this selected?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                target: '#pagepanel'
            }).then((result) => {
                if (result.isConfirmed) {
                    removeSelected(scm)
                    Swal.fire({
                        icon: 'success',
                        title: 'Deleted!',
                        text: 'Delete data successfully.',
                        target: '#pagepanel',
                    });
                }
            })
        }
    }

    function deleteAll() {
        let scma = JSON.parse(localStorage.getItem('sc-m-a'));
        let scman = JSON.parse(localStorage.getItem('sc-m-an'));
        let scm = JSON.parse(localStorage.getItem('sc-m'));

        Swal.fire({
            title: 'Are you sure?',
            text: "You want to delete all data?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            target: '#pagepanel'
        }).then((result) => {
            if (result.isConfirmed) {
                removeSelected(scman)
                Swal.fire({
                    icon: 'success',
                    title: 'Deleted!',
                    text: 'Delete data successfully.',
                    target: '#pagepanel',
                });
            }
        })
    }

    function exportExcel() {

    }

    function chooseCostControl(params) {
        let noid = params.noid; 
        let kode = params.kode;
        let idtypecostcontrol = params.idtypecostcontrol;
        let idcardsitekoor = params.idcardsitekoor;

        showLoading();
        $('#form-header #idtypecostcontrol').val(idtypecostcontrol).trigger('change');
        $('#form-header #idcostcontrol').val(noid);
        $('#form-header #kodecostcontrol').val(kode);
        getGenerateCode();
        // getDepartmentByCC({kodecostcontrol:kode});
        hideModalCostControl();
        $('#form-header #idcardsupplier').select2('focus');
        $('#idcardrequest').val(idcardsitekoor).trigger('change');
    }

    function format(d) {
        // `d` is the original data object for the row
        return '<b>Log Subject : </b><span>'+(d.logsubjectfull ? d.logsubjectfull : '')+'</span><br>'+
                '<b>Keterangan : </b><span>'+(d.keteranganfull ? d.keteranganfull : '')+'</span>';
    }

    function getDatatableLog(noid, kode, noidmore='', fieldmore='') {
        $('#table-log').DataTable().destroy();
        mytablelog = $('#table-log').DataTable({
            "pageLength": 10,
            "searching": true,
            "processing": true,
            "serverSide": true,
            "bInfo": true,
            "bFilter": false,
            "bLengthChange": false,
            "bAutoWidth": false,
            "order": [[ 6, "desc" ]],
            "columns": [
                {className:'details-control',orderable:false,data:null,defaultContent:''},
                {mData:'no',orderable:false,class:'text-right'},
                {mData:'idtypeaction'},
                {mData:'logsubject'},
                {mData:'keterangan'},
                {mData:'idcreate'},
                {mData:'docreate'},
            ],
            "columnDefs": [
                {targets:0,width:"15px"},
                {targets:1,width:"15px"},
                {targets:2,width:"100px"},
                {targets:3,width:"350px"},
                {targets:4,width:"350px"},
                {targets:5,width:"100px"},
                {targets:6,width:"100px"},
            ],
            // "fixedColumns": true,
            "infoCallback": function( settings, start, end, max, total, pre ) {
                var options = '';
                $.each(settings.aLengthMenu, function(k, v) {
                    options += '<option value="'+v+'" id="alm'+v+'" class="alm">'+v+'</option>'
                })
                if ((end-start) == settings._iDisplayLength-1) {
                    var valuepage = end/settings._iDisplayLength;
                } else {
                    var valuepage = ((start-1)/settings._iDisplayLength)+1;
                }
                if (start == 1) {
                    var prevdisabled = 'disabled'
                } else {
                    var prevdisabled = '';
                }
                if (end == total) {
                    var nextdisabled = 'disabled'
                } else {
                    var nextdisabled = '';
                }
                // if (settings.oPreviousSearch.sSearch == $('#searchform').val()) {
                //     alert($('#searchform').val())
                //     var valuesearch = $('#searchform').val()
                // } else {
                //     var valuesearch = $('#searchform').val()
                //     alert('else'+$('#searchform').val())
                // }
                if (settings.oPreviousSearch.sSearch != '') {
                    statussearch = true
                    searchtablewidth = 200
                    valuesearch = settings.oPreviousSearch.sSearch
                } else {
                    statussearch = false
                    searchtablewidth = 30
                    valuesearch = ''
                }
                $('#appendInfoDatatableLog').html(`
                <div class="row" style="background: #90CAF9; padding-left: 10px; padding-right: 10px; height: 40px; height: 45px;">
                    <div class="col-md-12" style="top: 3px;">
                        Page
                        <button id="prevpagelog" onclick="prevpagelog()" class="btn btn-sm default prev `+prevdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-left">
                            </i>
                        </button>
                        <input type="text" id="valuepagelog" onkeyup="valuepagelog()" class="input-mini" value="`+valuepage+`" style="text-align: center; border: 0px; margin: 5px 5px 5px 5px">
                        <button id="nextpagelog" onclick="nextpagelog()" class="btn btn-sm default next `+nextdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-right">
                            </i>
                        </button> of <span class="pagination-panel-total">`+
                            (settings._iRecordsTotal == settings._iDisplayLength ? 1 : parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)
                        +`</span>
                        <span class="seperator">|</span>
                        Rec/Page 
                        <select id="alengthmenulog" onchange="alengthmenulog()" name="table_pagepanel_9921050101_length" aria-controls="table_pagepanel_9921050101" class="input-inline input-xsmall input-sm recofdt" style="border: 0px; margin: 5px 5px 5px 5px">
                            `+options+`
                        </select>
                        <span class="seperator">|</span>Total `+settings._iRecordsTotal+` records
                        <input id="searchformlog" onkeyup="searchformlog()" value="`+valuesearch+`" type="search" class="form-control input-inline input-sm clicked" placeholder="" aria-controls="table_pagepanel_9921050101" style="width: `+searchtablewidth+`px; position: absolute; right: 0px; top: 4px; height: 70%; border: 0;">
                        <div onclick="searchtablelog()" class="btn fa fa-search text-right" style="position: absolute; right: 0px; top: 4px; height: 70%; background: transparent; margin: 0px 0px 0px 0px; padding: 5px 0px 5px 0px">
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                </div>`)
                console.log('settings')
                console.log(settings)
                console.log(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)
                $('.alm').removeAttr('selected');
                $('#alm'+settings._iDisplayLength).attr('selected', 'selected');
                // console.log(valuepage)
                $('.dataTables_info').css('display', 'none')
                $('.dataTables_paginate').css('display', 'none')
            },
            "ajax": {
                "url": "/get_log"+main.urlcode,
                "type": "POST",
                "dataType":'json',
                "data": {
                    "_token": $('#token').val(),
                    'statusadd': statusadd,
                    'noid': noid,
                    'kode': kode,
                    'noidmore': noidmore,
                    'fieldmore': fieldmore,
                },
            },
            "pagingType": "input",
            "createdRow": function(row, data, dataIndex) {
                $(row).attr('onmouseover', 'showButtonAction(this, '+dataIndex+', 100)');
                $(row).attr('onmouseout', 'resetCss()');
            },
            "oLanguage":{
                "loadingRecords": "Please wait - loading...",
                "sProcessing": "<div id='loadernya'></div>",
                "sInfo": "&nbsp;|&nbsp;Total&nbsp;_TOTAL_ records",
                "sLengthMenu": "&nbsp;|&nbsp;Rec/Page&nbsp;_MENU_",
                "sInfoFiltered": "",
                "sInfoEmpty": "| No records found to show",
                "sEmptyTable": "No data available in table",
                "sZeroRecords": "No matching records found",
                "sSearch": "<button onclick='toggleButtonSearch()' class='btn flat dtfilter_search_btn' style='background-color: white'><i class='fa fa-search'></i></button>",
                "oPaginate": {
                    "sPrevious": "<i class='fa fa-angle-left'></i>",
                    "sNext": "<i class='fa fa-angle-right'></i> ",
                }
            },
            footerCallback: function ( row, data, start, end, display ) {
            }
        });
        
        if (!statusgetlog) {
            $('#table-log tbody').on('click', 'td.details-control', function(){
                var tr = $(this).closest('tr');
                var row = mytablelog.row( tr );

                if(row.child.isShown()){
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                } else {
                    // Open this row
                    row.child(format(row.data())).show();
                    tr.addClass('shown');
                }
            });
            statusgetlog = true;
        }
    }

    function moreLog(noid, field) {
        $('#table-log #'+field+'-'+noid).show();
        $('#table-log #morelog-'+field+'-'+noid).attr('onclick', 'lessLog('+noid+', \''+field+'\')');
        $('#table-log #morelog-'+field+'-'+noid+' i').attr('class', 'icon-angle-left');
        // $('#table-log #morelog-'+field+'-'+noid+' span').text('Less');
    }

    function lessLog(noid, field) {
        $('#table-log #'+field+'-'+noid).hide();
        $('#table-log #morelog-'+field+'-'+noid).attr('onclick', 'moreLog('+noid+', \''+field+'\')');
        $('#table-log #morelog-'+field+'-'+noid+' i').attr('class', 'icon-angle-right');
        // $('#table-log #morelog-'+field+'-'+noid+' span').text('More');
    }

    function getDatatableCostControl(_statusdetail) {
        let columns = <?= $columnscostcontrol ?>;
        let columndefs = <?= $columndefscostcontrol ?>;

        $('#table-costcontrol').DataTable().destroy();
        mytablecostcontrol = $('#table-costcontrol').DataTable({
            "autoWidth": false,
            "pageLength": 10,
            "searching": true,
            "processing": true,
            "serverSide": true,
            "bInfo": true,
            "bFilter": false,
            "bLengthChange": false,
            "columns": [
                {mData:'checkbox',orderable:false,class:'text-center'},
                {mData:'no',orderable:false,class:'text-right'},
                // {mData:'mtt_nama'},
                // {mData:'mttts_nama'},
                {mData:'kode'},
                // {mData:'kodereff'},
                {mData:'projectname'},
                {mData:'projectlocation'},
                {mData:'tanggal'},
                {mData:'idcardpj_nama'},
                {mData:'idcardkoor_nama'},
                {mData:'idcardsitekoor_nama'},
                {mData:'idcarddrafter_nama'},
                // {mData:'mtcc_nama'},
                // {mData:'generaltotal',class:'text-right',render:$.fn.dataTable.render.number(',','.',0,'Rp.')},
                // {mData:'keterangan'},
                // {mData:'mcu_myusername'},
                // {mData:'gmc_nama'},
                // {mData:'gmd_nama'},
                {mData:'action',orderable:false},
            ],
            "columnDefs": [
                {targets:0,width:"15px"},
                {targets:1,width:"15px"},
                {targets:2,width:"100px"},
                {targets:2,width:"100px"},
                {targets:2,width:"100px"},
                {targets:3,width:"350px"},
                {targets:4,width:"350px"},
            ],
            "infoCallback": function( settings, start, end, max, total, pre ) {
                var options = '';
                $.each(settings.aLengthMenu, function(k, v) {
                    options += '<option value="'+v+'" id="alm'+v+'" class="alm">'+v+'</option>'
                })
                if ((end-start) == settings._iDisplayLength-1) {
                    var valuepage = end/settings._iDisplayLength;
                } else {
                    var valuepage = ((start-1)/settings._iDisplayLength)+1;
                }
                if (start == 1) {
                    var prevdisabled = 'disabled'
                } else {
                    var prevdisabled = '';
                }
                if (end == total) {
                    var nextdisabled = 'disabled'
                } else {
                    var nextdisabled = '';
                }
                // if (settings.oPreviousSearch.sSearch == $('#searchform').val()) {
                //     alert($('#searchform').val())
                //     var valuesearch = $('#searchform').val()
                // } else {
                //     var valuesearch = $('#searchform').val()
                //     alert('else'+$('#searchform').val())
                // }
                if (settings.oPreviousSearch.sSearch != '') {
                    statussearch = true
                    searchtablewidth = 200
                    valuesearch = settings.oPreviousSearch.sSearch
                } else {
                    statussearch = false
                    searchtablewidth = 30
                    valuesearch = ''
                }
                $('#appendInfoDatatableCostControl').html(`
                <div class="row" style="background: #90CAF9; padding-left: 15px; padding-right: 15px;">
                    <div class="col-md-12">
                        Page
                        <button id="prevpagecostcontrol" onclick="prevpagecostcontrol()" class="btn btn-sm default prev `+prevdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-left">
                            </i>
                        </button>
                        <input type="text" id="valuepagecostcontrol" onkeyup="valuepagecostcontrol()" class="input-mini" value="`+valuepage+`" style="text-align: center; border: 0px; margin: 5px 5px 5px 5px">
                        <button id="nextpagecostcontrol" onclick="nextpagecostcontrol()" class="btn btn-sm default next `+nextdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-right">
                            </i>
                        </button> of <span class="pagination-panel-total">`+(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)+`</span>
                        <span class="seperator">|</span>
                        Rec/Page 
                        <select id="alengthmenucostcontrol" onchange="alengthmenucostcontrol()" name="table_pagepanel_9921050101_length" aria-controls="table_pagepanel_9921050101" class="input-inline input-xsmall input-sm recofdt" style="border: 0px; margin: 5px 5px 5px 5px">
                            `+options+`
                        </select>
                        <span class="seperator">|</span>Total `+settings._iRecordsTotal+` records
                        <input id="searchformcostcontrol" onkeyup="searchformcostcontrol()" value="`+valuesearch+`" type="search" class="form-control input-inline input-sm clicked" placeholder="" aria-controls="table_pagepanel_9921050101" style="width: `+searchtablewidth+`px; position: absolute; right: 0px; top: 4px; height: 80%; border: 0;">
                        <div onclick="searchtablecostcontrol()" class="btn fa fa-search text-right" style="position: absolute; right: 0px; top: 4px; height: 80%; background: transparent; margin: 0px 0px 0px 0px; padding: 5px 0px 5px 0px">
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                </div>`)
                console.log('settings')
                console.log(settings)
                console.log(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)
                $('.alm').removeAttr('selected');
                $('#alm'+settings._iDisplayLength).attr('selected', 'selected');
                // console.log(valuepage)
                $('.dataTables_info').css('display', 'none')
                $('.dataTables_paginate').css('display', 'none')
            },
            "ajax": {
                "url": "/get_costcontrol"+main.urlcode,
                "type": "POST",
                "dataType":'json',
                "data": {
                    "_token": $('#token').val(),
                    'statusadd': statusadd,
                    'statusdetail': _statusdetail,
                    'idtypecostcontrol': $('#idtypecostcontrol').val()
                },
            },
            "pagingType": "input",
            "createdRow": function(row, data, dataIndex) {
                $(row).attr('onmouseover', 'showButtonAction(this, '+dataIndex+', 100)');
                $(row).attr('onmouseout', 'resetCss()');
            },
            "oLanguage":{
                "loadingRecords": "Please wait - loading...",
                "sProcessing": "<div id='loadernya'></div>",
                "sInfo": "&nbsp;|&nbsp;Total&nbsp;_TOTAL_ records",
                "sLengthMenu": "&nbsp;|&nbsp;Rec/Page&nbsp;_MENU_",
                "sInfoFiltered": "",
                "sInfoEmpty": "| No records found to show",
                "sEmptyTable": "No data available in table",
                "sZeroRecords": "No matching records found",
                "sSearch": "<button onclick='toggleButtonSearch()' class='btn flat dtfilter_search_btn' style='background-color: white'><i class='fa fa-search'></i></button>",
                "oPaginate": {
                    "sPrevious": "<i class='fa fa-angle-left'></i>",
                    "sNext": "<i class='fa fa-angle-right'></i> ",
                }
            },
            footerCallback: function ( row, data, start, end, display ) {
            }
        });
    }

    function getDatatableDetailPrev() {
        let idcardsupplier = $('#form-header #idcardsupplier').val();
        let idcompany = $('#form-header #idcompany').val();
        let iddepartment = $('#form-header #iddepartment').val();
        let idgudang = $('#form-header #idgudang').val();
        // let searchtypeprev = $('[name=searchtypeprev]:checked').val();
        let searchtypeprev = 2;
        let searchprev = $('#searchprev').val();
        let detailprev = new Array();
        $.each(JSON.parse(localStorage.getItem(main.tabledetail)), function(k, v) {
            let dponoid = '';
            let dpounitqty = '';
            $.each(v, function(k2, v2) {
                if (v2.name == 'noid') {
                    dponoid = v2.value;
                }
                if (v2.name == 'unitqty') {
                    dpounitqty = v2.value;
                }
            });
            detailprev.push({
                noid: dponoid,
                unitqty: dpounitqty,
            })
        });
        // console.log('detailprev')
        // console.log(detailprev)

        defaultOperation()
        let columns = <?= $columnsdetail ?>;
        let columndefs = <?= $columndefsdetail ?>;

        $('#table-detailprev').DataTable().destroy();
        mytabledetailprev = $('#table-detailprev').DataTable({
            "autoWidth": false,
            "pageLength": 10,
            "searching": true,
            "processing": true,
            "serverSide": true,
            "bInfo": true,
            "bFilter": false,
            "bLengthChange": false,
            "columns": [
                {mData:'checkbox',orderable:false,class:'text-center'},
                {mData:'no',orderable:false,class:'text-right'},
                {mData:'kodeprev'},
                {mData:'kodeinventor'},
                {mData:'namainventor'},
                {mData:'namainventor2'},
                {mData:'keterangan'},
                {mData:'unitqty',class:'text-right'},
                {mData:'unitqtysisa',class:'text-right'},
                {mData:'namasatuan'},
                {mData:'unitprice',class:'text-right',render:$.fn.dataTable.render.number(',','.',0,'Rp.')},
                {mData:'hargatotal',class:'text-right',render:$.fn.dataTable.render.number(',','.',0,'Rp.')},
                {mData:'namacurrency'},
                {mData:'action',orderable:false},
            ],
            "columnDefs": columndefs,
            "infoCallback": function( settings, start, end, max, total, pre ) {
                var options = '';
                $.each(settings.aLengthMenu, function(k, v) {
                    options += '<option value="'+v+'" id="alm'+v+'" class="alm">'+v+'</option>'
                })
                if ((end-start) == settings._iDisplayLength-1) {
                    var valuepage = end/settings._iDisplayLength;
                } else {
                    var valuepage = ((start-1)/settings._iDisplayLength)+1;
                }
                if (start == 1) {
                    var prevdisabled = 'disabled'
                } else {
                    var prevdisabled = '';
                }
                if (end == total) {
                    var nextdisabled = 'disabled'
                } else {
                    var nextdisabled = '';
                }
                // if (settings.oPreviousSearch.sSearch == $('#searchform').val()) {
                //     alert($('#searchform').val())
                //     var valuesearch = $('#searchform').val()
                // } else {
                //     var valuesearch = $('#searchform').val()
                //     alert('else'+$('#searchform').val())
                // }
                if (settings.oPreviousSearch.sSearch != '') {
                    statussearch = true
                    searchtablewidth = 200
                    valuesearch = settings.oPreviousSearch.sSearch
                } else {
                    statussearch = false
                    searchtablewidth = 30
                    valuesearch = ''
                }
                $('#appendInfoDatatableDetailPrev').html(`
                <div class="row" style="background: #90CAF9; padding-left: 15px; padding-right: 15px;">
                    <div class="col-md-12">
                        Page
                        <button type="button" onclick="prevpagedetailprev()" class="btn btn-sm default prev `+prevdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-left">
                            </i>
                        </button>
                        <input type="text" onkeyup="valuepagedetailprev()" class="input-mini" value="`+valuepage+`" style="text-align: center; border: 0px; margin: 5px 5px 5px 5px">
                        <button type="button" onclick="nextpagedetailprev()" class="btn btn-sm default next `+nextdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-right">
                            </i>
                        </button> of <span class="pagination-panel-total">`+(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)+`</span>
                        <span class="seperator">|</span>
                        Rec/Page 
                        <select id="alengthmenudetailprev" onchange="alengthmenudetailprev()" name="table_pagepanel_9921050101_length" aria-controls="table_pagepanel_9921050101" class="input-inline input-xsmall input-sm recofdt" style="border: 0px; margin: 5px 5px 5px 5px">
                            `+options+`
                        </select>
                        <span class="seperator">|</span>Total `+settings._iRecordsTotal+` records
                        <input id="searchformdetailprev" onkeyup="searchformdetailprev()" value="`+valuesearch+`" type="search" class="form-control input-inline input-sm clicked" placeholder="" aria-controls="table_pagepanel_9921050101" style="width: `+searchtablewidth+`px; position: absolute; right: 0px; top: 4px; height: 80%; border: 0;">
                        <div onclick="searchtabledetailprev()" class="btn fa fa-search text-right" style="position: absolute; right: 0px; top: 4px; height: 80%; background: transparent; margin: 0px 0px 0px 0px; padding: 5px 0px 5px 0px">
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                </div>`)
                console.log('settings')
                console.log(settings)
                console.log(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)
                $('.alm').removeAttr('selected');
                $('#alm'+settings._iDisplayLength).attr('selected', 'selected');
                // console.log(valuepage)
                $('.dataTables_info').css('display', 'none')
                $('.dataTables_paginate').css('display', 'none')
            },
            "ajax": {
                "url": "/get_detailpi"+main.urlcode,
                "type": "POST",
                "dataType":'json',
                "data": {
                    "_token": $('#token').val(),
                    'statusdetail': statusdetail,
                    'idcardsupplier': idcardsupplier,
                    'idcompany': idcompany,
                    'iddepartment': iddepartment,
                    'idgudang': idgudang,
                    'detailprev': JSON.stringify(detailprev),
                    'searchtypeprev': searchtypeprev,
                    'searchprev': searchprev,
                },
                "dataSrc": function(json) {
                    if (json.data.length == 1) {
                        chooseDetailPrev(
                            json.data[0].noid,
                            json.data[0].idmaster,
                            json.data[0].kodeprev,
                            json.data[0].idinventor,
                            json.data[0].kodeinventor,
                            json.data[0].namainventor,
                            json.data[0].namainventor2,
                            json.data[0].keterangan,
                            json.data[0].idgudang,
                            json.data[0].idcompany,
                            json.data[0].iddepartment,
                            json.data[0].idsatuan,
                            json.data[0].namasatuan,
                            json.data[0].konvsatuan,
                            json.data[0].unitqty,
                            json.data[0].unitqtysisa,
                            json.data[0].idinvmgroup,
                            json.data[0].unitprice,
                            json.data[0].subtotal,
                            json.data[0].discountvar,
                            json.data[0].discount,
                            json.data[0].nilaidisc,
                            json.data[0].idtypetax,
                            json.data[0].idtax,
                            json.data[0].prosentax,
                            json.data[0].nilaitax,
                            json.data[0].hargatotal,
                            json.data[0].namacurrency
                        );
                    } else {
                        $('#modal-detailprev').modal('show')
                        return json.data;
                    }
                }
            },
            "pagingType": "input",
            "createdRow": function(row, data, dataIndex) {
                let right = statusdetail ? 128 : 128;
                $(row).attr('onmouseover', 'showButtonActionTabPrev(this, '+dataIndex+', '+right+')');
                $(row).attr('onmouseout', 'resetCssTabPrev()');
            },
            "oLanguage":{
                "loadingRecords": "Please wait - loading...",
                "sProcessing": "<div id='loadernya'></div>",
                "sInfo": "&nbsp;|&nbsp;Total&nbsp;_TOTAL_ records",
                "sLengthMenu": "&nbsp;|&nbsp;Rec/Page&nbsp;_MENU_",
                "sInfoFiltered": "",
                "sInfoEmpty": "| No records found to show",
                "sEmptyTable": "No data available in table",
                "sZeroRecords": "No matching records found",
                "sSearch": "<button onclick='toggleButtonSearch()' class='btn flat dtfilter_search_btn' style='background-color: white'><i class='fa fa-search'></i></button>",
                "oPaginate": {
                    "sPrevious": "<i class='fa fa-angle-left'></i>",
                    "sNext": "<i class='fa fa-angle-right'></i> ",
                }
            },
            footerCallback: function ( row, data, start, end, display ) {
                var api = this.api(), data;
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
    
                var sum = function ( i ) {
                    var value = 0;
                    $.each(api.column( i ).data(), function(k, v) {
                        var _split = v.split('.');
                        var value2 = '';
                        $.each(_split, function(k2, v2) {
                            value2 += v2
                        })
                        value += Number(value2)
                    })
                    return value
                }
                var addcommas = function addCommas(nStr) {
                    nStr += '';
                    var x = nStr.split('.');
                    var x1 = x[0];
                    var x2 = x.length > 1 ? '.' + x[1] : '';
                    var rgx = /(\d+)(\d{3})/;
                    while (rgx.test(x1)) {
                        x1 = x1.replace(rgx, '$1' + '.' + '$2');
                    }
                    return x1 + x2;
                }

                pageTotal = api
                    .column( 11, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                // Update footer
                // console.log('akak')
                // console.log(eval(api.column(9).data()[0]))
                $( api.column(2).footer() ).html(data.length);
                $( api.column(6).footer() ).html(api.column(6).data().reduce( function (a, b) { return parseInt(a)+parseInt(b); }, 0 ));
                $( api.column(7).footer() ).html(api.column(7).data().reduce( function (a, b) { return parseInt(a)+parseInt(b); }, 0 ));
                $( api.column(11).footer() ).html(api.column(11).data().reduce( function (a, b) { return pageTotal; }, 0 ));
                var $th = $(row).find('th').eq(11);
                $th.text( $.fn.dataTable.render.number(',', '.', 0, 'Rp.').display( $th.text() )) 
                // $( api.column(6).footer() ).html(data.length);
            }
        });
    }

    function getDatatableDetail(data) {
        defaultOperation()
        let columns = <?= $columnsdetail ?>;
        let columndefs = <?= $columndefsdetail ?>;

        $('#table-detail').DataTable().destroy();
        mytabledetail = $('#table-detail').DataTable({
            "autoWidth": false,
            "pageLength": 10,
            "searching": true,
            "processing": true,
            "serverSide": true,
            "bInfo": true,
            "bFilter": false,
            "bLengthChange": false,
            "columns": [
                {mData:'checkbox',orderable:false,class:'text-center'},
                {mData:'no',orderable:false,class:'text-right'},
                {mData:'kodeprev'},
                {mData:'kodeinventor'},
                {mData:'namainventor'},
                {mData:'namainventor2'},
                {mData:'keterangan'},
                {mData:'unitqty',class:'text-right'},
                {mData:'unitqtysisa',class:'text-right'},
                {mData:'namasatuan'},
                // {mData:'unitprice',class:'text-right',render:$.fn.dataTable.render.number(',','.',0,'Rp.')},
                // {mData:'hargatotal',class:'text-right',render:$.fn.dataTable.render.number(',','.',0,'Rp.')},
                // {mData:'namacurrency'},
                {mData:'action',orderable:false},
            ],
            "columnDefs": [
                {targets:0,width:"15px"},
                {targets:1,width:"15px"},
                {targets:2,width:"100px"},
                {targets:3,width:"350px"},
                {targets:4,width:"350px"},
                {targets:5,width:"100px"},
                {targets:6,width:"100px"},
                {targets:7,width:"100px"},
                {targets:8,width:"100px"},
                {targets:9,width:"100px"},
                {targets:10,width:"100px"},
            ],
            "infoCallback": function( settings, start, end, max, total, pre ) {
                var options = '';
                $.each(settings.aLengthMenu, function(k, v) {
                    options += '<option value="'+v+'" id="alm'+v+'" class="alm">'+v+'</option>'
                })
                if ((end-start) == settings._iDisplayLength-1) {
                    var valuepage = end/settings._iDisplayLength;
                } else {
                    var valuepage = ((start-1)/settings._iDisplayLength)+1;
                }
                if (start == 1) {
                    var prevdisabled = 'disabled'
                } else {
                    var prevdisabled = '';
                }
                if (end == total) {
                    var nextdisabled = 'disabled'
                } else {
                    var nextdisabled = '';
                }
                // if (settings.oPreviousSearch.sSearch == $('#searchform').val()) {
                //     alert($('#searchform').val())
                //     var valuesearch = $('#searchform').val()
                // } else {
                //     var valuesearch = $('#searchform').val()
                //     alert('else'+$('#searchform').val())
                // }
                if (settings.oPreviousSearch.sSearch != '') {
                    statussearch = true
                    searchtablewidth = 200
                    valuesearch = settings.oPreviousSearch.sSearch
                } else {
                    statussearch = false
                    searchtablewidth = 30
                    valuesearch = ''
                }
                $('#appendInfoDatatableDetail').html(`
                <div class="row" style="background: #90CAF9; padding-left: 10px; padding-right: 10px; height: 40px; height: 45px;">
                    <div class="col-md-12" style="top: 3px;">
                        Page
                        <button id="prevpagedetail" onclick="prevpagedetail()" class="btn btn-sm default prev `+prevdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-left">
                            </i>
                        </button>
                        <input type="text" id="valuepagedetail" onkeyup="valuepagedetail()" class="input-mini" value="`+valuepage+`" style="text-align: center; border: 0px; margin: 5px 5px 5px 5px">
                        <button id="nextpagedetail" onclick="nextpagedetail()" class="btn btn-sm default next `+nextdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-right">
                            </i>
                        </button> of <span class="pagination-panel-total">`+(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)+`</span>
                        <span class="seperator">|</span>
                        Rec/Page 
                        <select id="alengthmenudetail" onchange="alengthmenudetail()" name="table_pagepanel_9921050101_length" aria-controls="table_pagepanel_9921050101" class="input-inline input-xsmall input-sm recofdt" style="border: 0px; margin: 5px 5px 5px 5px">
                            `+options+`
                        </select>
                        <span class="seperator">|</span>Total `+settings._iRecordsTotal+` records
                        <input id="searchformdetail" onkeyup="searchformdetail()" value="`+valuesearch+`" type="search" class="form-control input-inline input-sm clicked" placeholder="" aria-controls="table_pagepanel_9921050101" style="width: `+searchtablewidth+`px; position: absolute; right: 0px; top: 4px; height: 70%; border: 0;">
                        <div onclick="searchtabledetail()" class="btn fa fa-search text-right" style="position: absolute; right: 0px; top: 4px; height: 70%; background: transparent; margin: 0px 0px 0px 0px; padding: 5px 0px 5px 0px">
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                </div>`)
                console.log('settings')
                console.log(settings)
                console.log(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)
                $('.alm').removeAttr('selected');
                $('#alm'+settings._iDisplayLength).attr('selected', 'selected');
                // console.log(valuepage)
                $('.dataTables_info').css('display', 'none')
                $('.dataTables_paginate').css('display', 'none')
            },
            "ajax": {
                "url": "/get_detail"+main.urlcode,
                "type": "POST",
                "dataType":'json',
                "data": {
                    "_token": $('#token').val(),
                    'data': data ? data : '[]',
                    'statusdetail': statusdetail,
                    'condition': JSON.stringify(main.condition[status.crudmaster])
                },
            },
            "pagingType": "input",
            "createdRow": function(row, data, dataIndex) {
                let right = statusdetail ? 45 : 45;
                $(row).attr('onmouseover', 'showButtonActionTab(this, '+dataIndex+', '+right+')');
                $(row).attr('onmouseout', 'resetCssTab()');
            },
            "oLanguage":{
                "loadingRecords": "Please wait - loading...",
                "sProcessing": "<div id='loadernya'></div>",
                "sInfo": "&nbsp;|&nbsp;Total&nbsp;_TOTAL_ records",
                "sLengthMenu": "&nbsp;|&nbsp;Rec/Page&nbsp;_MENU_",
                "sInfoFiltered": "",
                "sInfoEmpty": "| No records found to show",
                "sEmptyTable": "No data available in table",
                "sZeroRecords": "No matching records found",
                "sSearch": "<button onclick='toggleButtonSearch()' class='btn flat dtfilter_search_btn' style='background-color: white'><i class='fa fa-search'></i></button>",
                "oPaginate": {
                    "sPrevious": "<i class='fa fa-angle-left'></i>",
                    "sNext": "<i class='fa fa-angle-right'></i> ",
                }
            },
            footerCallback: function ( row, data, start, end, display ) {
                var api = this.api(), data;
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
    
                var sum = function ( i ) {
                    var value = 0;
                    $.each(api.column( i ).data(), function(k, v) {
                        var _split = v.split('.');
                        var value2 = '';
                        $.each(_split, function(k2, v2) {
                            value2 += v2
                        })
                        value += Number(value2)
                    })
                    return value
                }
                var addcommas = function addCommas(nStr) {
                    nStr += '';
                    var x = nStr.split('.');
                    var x1 = x[0];
                    var x2 = x.length > 1 ? '.' + x[1] : '';
                    var rgx = /(\d+)(\d{3})/;
                    while (rgx.test(x1)) {
                        x1 = x1.replace(rgx, '$1' + '.' + '$2');
                    }
                    return x1 + x2;
                }

                pageTotal = api
                    .column( 9, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                // Update footer
                // console.log('akak')
                // console.log(eval(api.column(9).data()[0]))
                $( api.column(2).footer() ).html(data.length);
                $( api.column(7).footer() ).html(api.column(7).data().reduce( function (a, b) { return parseInt(a)+parseInt(b); }, 0 ));
                $( api.column(8).footer() ).html(api.column(8).data().reduce( function (a, b) { return parseInt(a)+parseInt(b); }, 0 ));
                // $( api.column(11).footer() ).html(api.column(11).data().reduce( function (a, b) { return pageTotal; }, 0 ));
                // var $th = $(row).find('th').eq(11);
                // $th.text( $.fn.dataTable.render.number(',', '.', 0, 'Rp.').display( $th.text() )) 
                // $( api.column(6).footer() ).html(data.length);
            }
        });
    }

    function showModalDelete(noid) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You want to delete this?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            target: '#pagepanel'
        }).then((result) => {
            if (result.isConfirmed) {
                removeData(noid);
                Swal.fire({
                    icon: 'success',
                    title: 'Deleted!',
                    text: 'Delete data successfully.',
                    target: '#pagepanel',
                });
            }
        })
        return false;
        $('#remove-data').attr('onclick', 'removeData('+noid+')')
        $('#modal-delete').modal('show')
    }

    function hideModalDelete() {
        $('#modal-delete').modal('hide')
    }

    function showModalSendToNext(params) {
        $.ajax({
            type: 'POST',
            header: {
                'X-CSRF-TOKEN': $('#token').val()
            },
            url: '/get_supplier_to_next'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('#token').val(),
                'noid': params.noid,
                'kode': params.kode,
                'idtypetranc': params.idtypetranc,
            },
            success: function (response) {
                let html = '<option value="">[Choose Supplier]</option>';
                                                 
                $.each(response.data, function(k,v){
                    html += `<option 
                            value="${v.mcardsupplier_noid}" 
                            data-mlokasiprop-noid="${v.mlokasiprop_noid}" 
                            data-mlokasiprop-nama="${v.mlokasiprop_nama}" 
                            data-mlokasikab-noid="${v.mlokasikab_noid}" 
                            data-mlokasikab-nama="${v.mlokasikab_nama}" 
                            data-mlokasikec-noid="${v.mlokasikec_noid}" 
                            data-mlokasikec-nama="${v.mlokasikec_nama}" 
                            data-mlokasikel-noid="${v.mlokasikel_noid}" 
                            data-mlokasikel-nama="${v.mlokasikel_nama}" 
                        >${v.mcardsupplier_nama}</option>`;
                })
                $('#idcardsupplier-stn').html(html);
                
                $('.select2').select2();
                $('.select2').css('width', '100%');
                $('.select2 .selection .select2-selection').css('height', '35px');
                $('.select2 .selection .select2-selection .select2-selection__rendered').css('margin-top', '-6px');
                $('.select2 .selection .select2-selection .select2-selection__arrow').css('margin-top', '4px');
            }
        })

        $('#form-modal-stn #idcardsupplier-stn').val('').trigger('change')
        $('#send-to-next').attr('onclick', 'sendToNext('+params.noid+')')
        $('#modal-send-to-next').modal('show')
    }

    function hideModalSendToOrder() {
        $('#modal-send-to-next').modal('hide')
    }

    function showModalSNST(params) {
        let noid = params.noid;
        let idtypetranc = params.idtypetranc;
        let idstatustranc = params.idstatustranc;
        let statustranc = params.statustranc;
        let fromview = params.fromview;
        let idtypecostcontrol = params.idtypecostcontrol;
        let idcostcontrol = params.idcostcontrol;

        // if (idstatustranc == 1 || idstatustranc == 2) {
        //     $('#form-modal-snst #keterangan-snst').focus();
        //     alert('masuk')
        // }

        if (idtypecostcontrol == 0 || idcostcontrol == 0) {
            $.notify({message: 'Kode Cost Center tidak boleh berupa UNSPECIFIED. Silahkan klik edit data!'},{type: 'danger'}); return;
        }

        if (fromview) {
            $('#modal-snst .modal-title').text('SET TO '+statustranc);
            $('#btn-snst span').text('Set to '+statustranc);
            $('#btn-snst').attr('onclick', 'SNST('+noid+','+idstatustranc+',\''+statustranc+'\','+fromview+')')
        } else {
            $('#modal-snst .modal-title').text('SAVE TO '+statustranc);
            $('#btn-snst span').text('Save to '+statustranc);
            $('#btn-snst').attr('onclick', `saveData(`+idstatustranc+`, '`+statustranc+`', '`+$('#keterangan-snst').val()+`')`)
        }

        $('#idtypetranc-snst').val(idtypetranc);
        $('#form-modal-snst #keterangan-snst').val('');
        $('#modal-snst').modal('show')
    }

    function hideModalSNST() {
        $('#modal-snst').modal('hide')
        $('#form-modal-snst #idtypetranc-snst').val('').trigger('change')
        $('#form-modal-snst #keterangan-snst').val('');
    }

    function showModalDIV(noid, idtypetranc, idstatustranc) {
        $('#modal-div .modal-title').text('SET STATUS');
        $('#btn-div span').text('Set Status');
        $('#btn-div').attr('onclick', 'DIV('+noid+', '+idstatustranc+')');  
        $('#idtypetranc-div').val(idtypetranc);
        $('#modal-div #select-div').show();
        $('#modal-div #select-spcf').hide();
        $('#form-modal-div #idstatustranc-div').val('').trigger('change');
        $('#form-modal-div #keterangan-div').val('');
        $('#modal-div').modal('show')
    }

    function hideModalDIV() {
        $('#modal-div').modal('hide')
    }

    function showModalSAPCF(noid, idtypetranc, idstatustranc) {
        $('#modal-sapcf .modal-title').text('SET STATUS');
        $('#btn-sapcf span').text('Set Status');
        $('#btn-sapcf').attr('onclick', 'SAPCF('+noid+', '+idstatustranc+')');  
        $('#idtypetranc-sapcf').val(idtypetranc);
        $('#form-modal-sapcf #idstatustranc-sapcf').val('').trigger('change');
        $('#form-modal-sapcf #keterangan-sapcf').val('');
        $('#modal-sapcf').modal('show')
    }

    function hideModalSAPCF() {
        $('#modal-sapcf').modal('hide')
    }

    function showModalSetPending(noid, idtypetranc) {
        $('#idtypetranc-pending').val(idtypetranc);
        $('#set-pending').attr('onclick', 'setPending('+noid+')')
        $('#modal-set-pending').modal('show')
    }

    function hideModalSetPending() {
        $('#modal-set-pending').modal('hide')
    }

    function showModalSetCanceled(noid, idtypetranc) {
        $('#idtypetranc-canceled').val(idtypetranc);
        $('#set-canceled').attr('onclick', 'setCanceled('+noid+')')
        $('#modal-set-canceled').modal('show')
    }

    function hideModalSetCanceled() {
        $('#modal-set-canceled').modal('hide')
    }

    function showModalLog(noid, kode, typetranc) {
        getDatatableLog(noid, kode)
        $('#modal-log #titlelog-log').text(typetranc)
        $('#modal-log #titlekode-log').text(kode)
        $('#modal-log').modal('show')
    }

    function hideModalLog() {
        $('#modal-log').modal('hide')
    }

    function showModalCostControl() {
        getDatatableCostControl(statusdetail)
        $('#modal-costcontrol').modal('show')
    }

    function hideModalCostControl() {
        $('#modal-costcontrol').modal('hide')
    }

    function showModalDetailPrev() {
        let searchtypeprev = $('[name=searchtypeprev]:checked');
        let idcardsupplier = $('#form-header #idcardsupplier :selected');

        if (searchtypeprev.val() == 1) {
            if (idcardsupplier.val()) {
                $('#modal-detailprev .supplier-title').text(idcardsupplier.text())
                $('#modal-detailprev #title-prev').show();
                $('#modal-detailprev #title-inv').hide();
                getDatatableDetailPrev()
                $('#modal-detailprev').modal('show')
            } else {
                $('#form-header #idcardsupplier').select2('focus');
                $.notify({
                    message: 'Supplier harus dipilih!'
                },{
                    type: 'warning'
                });
            }
        } else {
            $('#modal-detailprev #title-inv').show();
            $('#modal-detailprev #title-prev').hide();
            getDatatableDetailPrev()
        }
    }

    function hideModalDetailPrev() {
        $('#modal-detailprev').modal('hide')
    }

    function showModalTanggal() {
        $('#form-modal-tanggal #tanggaltax-modal').focus()
        $('#modal-tanggal').modal('show')
    }

    function hideModalTanggal() {
        $('#modal-tanggal').modal('hide')
    }

    function showModalSupplier() {
        $('#modal-supplier').modal('show')
    }

    function hideModalSupplier() {
        $('#modal-supplier').modal('hide')
    }

    function showModalDetail() {
        $('#modal-detail').modal('show')
        $('#form-modal-detail #idinventor-modal').select2('focus')
    }

    function hideModalDetail() {
        $('#modal-detail').modal('hide')
    }
    
    function currency(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return 'Rp.'+ x1 + x2;
    }

    /////////

    var maincms = {};
    var ready = false;
    var statusadd = true;
    var statusdetail = false;
    var statusaddtab = [];
    var editid = '';
    var nextnoid = '';
    var nextnoidtab = [];
    var nownoid = '';
    var nownoidtab = '';
    var nowmaintabletab = '';
    var fieldnametab = new Array();
    var tablelookuptab = new Array();
    var input_data = new Array();
    var mainonchange = new Array();
    var maintable = new Array();
    var values = $("input[name='panel[]']").map(function(){return $(this).val();}).get();
    var values2 = $("#slider").val();
    var values3 = $("#tabel1").val();
    var select_all = false;
    var select_all_tab = new Array();
    var cursor_top = 0;
    var selecteds = new Array();
    var another_selecteds = new Array();

    var selectcheckboxmall = false;
    var selectcheckboxm = new Array();
    var selectcheckboxman = new Array();
    localStorage.setItem('sc-m-a', 'false');
    localStorage.setItem('sc-m', JSON.stringify(new Array()));
    localStorage.setItem('sc-m-an', JSON.stringify(new Array()));
    // var divform = '';
    var my_table = '';
    var mytable = '';
    var mytabtable = new Array();
    var groupbtn = '';
    var selected_filter = {
        operand: [],
        fieldname: [],
        operator: [],
        value: [],
    };
    var selectedfilter = {
        'idstatustranc': null,
        'idcompany': null,
    };
    var datefilter = {
        'code': 'tm',
        'custom': {
            'status': false,
            'from': $('#did-from').val(),
            'to': $('#did-to').val(),
        }
    };
    var datefilter2 = {
        'from': null,
        'to': null
    };
    var filter_tabs = new Array();
    var selected_filter_tabs = 0;
    var filtercontenttabs = {
        filtercontentgroup: new Array(),
        filtercontentbody: new Array(),
        filtercontentrow: new Array(),
    }
    var colsorted = [0,1];
    var roles = {
        fieldname: new Array(),
        newhidden: new Array(),
        edithidden: new Array(),
    };
    var cmsfilterbutton = new Array()
    var tab_datatable = new Array()

    var functionfooter = new Array();
    var functionfootertab = new Array();
    var searchtablewidth = 30;
    var tab_searchtablewidth = new Array();
    var statussearch = '';
    var tab_statussearch = new Array();
    var valuesearch = '';
    var tab_valuesearch = new Array();

    var maincms_widget = new Array();
    var maincms_widgetgrid = new Array();

    var noids = new Array();
    var noids_tab = new Array();

    function selectCheckboxMAll() {
        let mselectall = $('#checkbox-m-all').is(':checked');
        let mselect = $('.checkbox-m');
        mselect.prop('checked', mselectall);

        if (mselectall == true) {
            $('#table_pagepanel_9921050101 .odd').css('background-color', '#90CAF9');
            $('#table_pagepanel_9921050101 .even').css('background-color', '#90CAF9');
            $('#table_pagepanel_9921050101 .checkbox-m').val('TRUE');
            // selectcheckboxmall = true;
            // selectcheckboxm = new Array();
            // selectcheckboxm = selectcheckboxman;
            localStorage.setItem('sc-m-a', 'true');
            localStorage.setItem('sc-m', localStorage.getItem('sc-m-an'));
        } else {
            $('#table_pagepanel_9921050101 .odd').css('background-color', '#ffffff');
            $('#table_pagepanel_9921050101 .even').css('background-color', '#BBDEFB');
            $('#table_pagepanel_9921050101 .checkbox-m').val('FALSE');
            localStorage.setItem('sc-m-a', 'false');
            localStorage.setItem('sc-m', JSON.stringify(new Array()));
        }
    }

    function selectCheckboxM(index) {
        let checkbox = $('#checkbox-m-'+index);

        if (checkbox.val() == 'FALSE') {
            $('#table_pagepanel_9921050101 .row-m-'+index).css('background-color', '#90CAF9');
            $('#table_pagepanel_9921050101 #checkbox-m-'+index).val('TRUE');
            let scm = JSON.parse(localStorage.getItem('sc-m'));
            selectcheckboxm.push(index);
            scm.push(index);
            localStorage.setItem('sc-m', JSON.stringify(scm));
        } else {
            if (index % 2 == 0) {
                $('#table_pagepanel_9921050101 .row-m-'+index).css('background-color', '#ffffff');
            } else {
                $('#table_pagepanel_9921050101 .row-m-'+index).css('background-color', '#BBDEFB');
            }
            $('#table_pagepanel_9921050101 #checkbox-m-'+index).val('FALSE')
            var replaceselecteds = new Array();
            $.each(selectcheckboxm, function(k, v) {
                if (v != index) {
                    replaceselecteds.push(v)
                }
            })
            selectcheckboxm = replaceselecteds

            let replacescm = new Array();
            $.each(JSON.parse(localStorage.getItem('sc-m')), function(k, v) {
                if (v != index) {
                    replacescm.push(v)
                }
            })
            localStorage.setItem('sc-m', JSON.stringify(replacescm));
        }
        selectcheckboxmall = false;
        localStorage.setItem('sc-m-a', 'false');
        $('#checkbox-m-all').prop('checked', false);
        // console.log(selecteds)
    }

    function selectAllCheckboxTab(panel_noid) {
        var checkboxes = $('[name="checkbox-'+panel_noid+'[]"]');
        checkboxes.prop('checked', $('#select_all_'+panel_noid).is(':checked'));

        if (select_all_tab[panel_noid] == false) {
            select_all_tab[panel_noid] = true;
            $('#tab-table-'+panel_noid+' .odd').css('background-color', '#90CAF9');
            $('#tab-table-'+panel_noid+' .even').css('background-color', '#90CAF9');
            $('#tab-table-'+panel_noid+' .checkbox-'+panel_noid).val('TRUE');
            another_selecteds[panel_noid] = noids_tab[panel_noid];
        } else {
            select_all_tab[panel_noid] = false;
            $('#tab-table-'+panel_noid+' .odd').css('background-color', '#ffffff');
            $('#tab-table-'+panel_noid+' .even').css('background-color', '#BBDEFB');
            $('#tab-table-'+panel_noid+' .checkbox-'+panel_noid).val('FALSE');
            another_selecteds[panel_noid] = new Array();
        }
        console.log('another_selecteds')
        console.log(another_selecteds)
    }
    
    function changeCheckbox(index, id) {
        var checkbox = $('.checkbox'+index);
        if (checkbox.val() == 'FALSE') {
            $('.row'+index).css('background-color', '#90CAF9');
            $('.checkbox'+index).val('TRUE');
            selecteds.push(id);
        } else {
            if (index % 2 == 0) {
                $('.row'+index).css('background-color', '#ffffff');
            } else {
                $('.row'+index).css('background-color', '#BBDEFB');
            }
            $('.checkbox'+index).val('FALSE')
            var replace_selecteds = new Array();
            $.each(selecteds, function(k, v) {
                if (v != id) {
                    replace_selecteds.push(v)
                }
            })
            selecteds = replace_selecteds
        }
        select_all = false;
        $('#select_all').prop('checked', false);
        // console.log(selecteds)
    }

    function changeCheckboxTab(panel_noid, index, id) {
        var checkbox = $('.checkbox-'+panel_noid+'-'+index);
        // alert(checkbox.val())
        if (checkbox.val() == 'FALSE') {
            $('#tab-table-'+panel_noid+' .row'+index).css('background-color', '#90CAF9');
            $('#tab-table-'+panel_noid+' .checkbox-'+panel_noid+'-'+index).val('TRUE');
            another_selecteds[panel_noid].push(id);
        } else {
            if (index % 2 == 0) {
                $('#tab-table-'+panel_noid+' .row'+index).css('background-color', '#ffffff');
            } else {
                $('#tab-table-'+panel_noid+' .row'+index).css('background-color', '#BBDEFB');
            }
            $('#tab-table-'+panel_noid+' .checkbox-'+panel_noid+'-'+index).val('FALSE')
            // another_selecteds.pop(id);
            // delete another_selecteds[index]
            var replace_another_selecteds = new Array();
            $.each(another_selecteds[panel_noid], function(k, v) {
                if (v != id) {
                    replace_another_selecteds.push(v)
                }
            })
            another_selecteds[panel_noid] = replace_another_selecteds
        }
        select_all_tab[panel_noid] = false;
        $('#select_all_'+panel_noid).prop('checked', false);
        console.log('another_selecteds')
        console.log(another_selecteds)
    }

    function mouseOver(element_id, color) {
        $('#'+element_id).css('background-color', color);
        $('#'+element_id).css('color', 'white');
    }

    function mouseOut(element_id, color) {
        $('#'+element_id).removeAttr('style');
        $('#'+element_id).css('background-color', color);
        $('#'+element_id).css('color', 'white');
    }

    function showButtonAction(element, index, right=28) {
        var rect = element.getBoundingClientRect();

        this.resetCss()

        // $('.dataTable tbody td').css('height', '100px !important')
        // $('.dataTable tbody td:nth-child('+index+')').css('background', 'black')
        // $('.my_row').css('height', 35)
        $('.button-action-'+index).css('position', 'fixed')
        $('.button-action-'+index).css('top', parseInt(rect.y+(5)))
        $('.button-action-'+index).css('right', right+'px')
        // $('.button-action-'+index).css('overflow', 'hidden')

        // console.log(parseInt(rect.y+8))
        // console.log('Element is ' + offset + ' vertical pixels from <body>');
    }

    function showButtonActionTab(element, index, right=38) {
        var rect = element.getBoundingClientRect();

        this.resetCssTab()

        $('.button-action-tab-'+index).css('position', 'fixed')
        $('.button-action-tab-'+index).css('top', parseInt(rect.y+5))
        $('.button-action-tab-'+index).css('right', right+'px')
        // $('.button-action-'+index).css('overflow', 'hidden')

        // console.log(parseInt(rect.y+8))
        // console.log('Element is ' + offset + ' vertical pixels from <body>');
    }

    function showButtonActionTabPrev(element, index, right=38) {
        var rect = element.getBoundingClientRect();

        this.resetCssTab()

        $('.button-action-dprev-'+index).css('position', 'fixed')
        $('.button-action-dprev-'+index).css('top', parseInt(rect.y+5))
        $('.button-action-dprev-'+index).css('right', right+'px')
        // $('.button-action-'+index).css('overflow', 'hidden')

        // console.log(parseInt(rect.y+8))
        // console.log('Element is ' + offset + ' vertical pixels from <body>');
    }

    function resetCss() {
        $('.button-action').removeAttr('style')
    }

    function resetCssTab() {
        $('.button-action-tab').removeAttr('style')
    }

    function resetCssTabPrev() {
        $('.button-action-dprev').removeAttr('style')
    }

    function mouseoverAddData(element_id) {
        $('#'+element_id).css('background', '#337AB7')
    }

    function mouseoutAddData(element_id) {
        $('#'+element_id).css('background', '#3598dc')
    }
    
    function openFullscreen() {
        document.documentElement.requestFullscreen().catch((e) => {console.log(e)})
    }

    function getFullscreenElement() {
        return document.fullscreenElement
            || document.webkitFullscreenElement
            || document.mozFullscreenElement
            || document.msFullscreenElement;
    }

    function readyFullscreen() {
        if (localStorage.getItem(main.table+'-fullscreen') == 'true') {
            $('#tool_fullscreen i').removeClass('icon-fullscreen');
            $('#tool_fullscreen i').addClass('icon-remove-sign');
            document.getElementById('pagepanel').requestFullscreen().catch((e) => {})
        } else {
            $('#tool_fullscreen i').removeClass('icon-remove-sign');
            $('#tool_fullscreen i').addClass('icon-fullscreen');
            document.exitFullscreen();
        }
    }

    function toggleFullscreen() {
        if (localStorage.getItem(main.table+'-fullscreen') == 'true') {
            localStorage.setItem(main.table+'-fullscreen', 'false');
        } else {
            localStorage.setItem(main.table+'-fullscreen', 'true');
        }
        readyFullscreen();
        
        return false;

        if (getFullscreenElement() || localStorage.setItem(main.table+'-fullscreen', 'false') == 'false') {
            $('#tool_fullscreen i').removeClass('icon-remove-sign');
            $('#tool_fullscreen i').addClass('icon-fullscreen');
            localStorage.setItem(main.table+'-fullscreen', 'false');
            document.exitFullscreen();
        } else {
            $('#tool_fullscreen i').removeClass('icon-fullscreen');
            $('#tool_fullscreen i').addClass('icon-remove-sign');
            localStorage.setItem(main.table+'-fullscreen', 'true');
            document.getElementById('pagepanel').requestFullscreen().catch((e) => {})
        }
    }

    function filterDate(code) {
        datefilter.code = code;
        if (code == 'cr') {
            datefilter.custom.status = true;
            let getfrom = $('#did-from').val().split('-');
            let getto = $('#did-to').val().split('-');
            let from = getfrom[2]+'-'+getfrom[1]+'-'+getfrom[0];
            let to = getto[2]+'-'+getto[1]+'-'+getto[0];
            let f = moment(from).format('MMM DD, YYYY');
            let t = moment(to).format('MMM DD, YYYY');
            let span = f+' - '+t;        
            $('#dropdown-date-filter span').text(span);
        } else {
            $('#dropdown-date-filter span').text($('#did-'+code).attr('data-date'));
        }
        main.datefilterdefault = code;
        hoverFilterDate(code);
        getDatatable();
    }

    function changeCustomRange() {
        datefilter.custom.from = $('#did-from').val();
        datefilter.custom.to = $('#did-to').val();
    }

    $('#did-from').on('change',function(e){$('#did-to').focus()})

    function hoverFilterDate(code) {
        $('.dropdown-item-date span').css('background-color','#f5f5f5');
        $('.dropdown-item-date span').css('color','#08c');
        $('#did-'+code+' span').css('background-color','#4b8df8')
        $('#did-'+code+' span').css('color','white')

        $('#did-'+main.datefilterdefault+' span').css('background-color','#4b8df8')
        $('#did-'+main.datefilterdefault+' span').css('color','white')
    }

    function hoverOutFilterDate(code) {
        if (code != main.datefilterdefault) {
            $('#did-'+code+' span').css('background-color','#f5f5f5')
            $('#did-'+code+' span').css('color','#08c')
        }
    }
    
    function recreateNotification() {
        $.ajax({
            type: 'POST',
            header: {
                'X-CSRF-TOKEN': $('#token').val()
            },
            url: '/get_notification'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('#token').val(),
            },
            success: function (response) {
                let color = response.data.color;
                let an = response.data.an;

                let html = `<div class="topbar-item text-hover-primary" data-toggle="dropdown" data-offset="10px,0px">
                        <i class="icon-bell text-primary" style="margin: 5px;"></i>`;
                
                html += an.length > 0 
                    ? `<span class="badge" style="background-color: red; color: white; margin: -15px 0px 0px -5px">${an.length}</span>`
                    : ``;
                    
                html += `<span class="pulse-ring"></span>
                    </div>
                    <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg" style="">
                        <form>
                            <div class="d-flex flex-column pt-12 pb-10 bgi-size-cover bgi-no-repeat rounded-top" style="background: #334EB5">
                                <h4 class="d-flex flex-center rounded-top">
                                    <span class="text-white">User Notifications</span>
                                    <span class="btn btn-text btn-success btn-sm font-weight-bold btn-font-md ml-2">${an.length} new</span>
                                </h4>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane active" id="topbar_notifications_events" role="tabpanel">
                                    <div class="navi navi-hover scroll my-4 ps ps--active-y" data-scroll="true" data-height="300" data-mobile-height="200" style="height: 300px; overflow: hidden;">`;

                console.log(an)
                if (an.length > 0) {
                    $.each(an, function(k,v){
                        html += `<a class="navi-item" onclick="readNotification({noid:${v.an_noid},urlreff:'${v.an_urlreff}'})">
                                <div class="navi-link" style="background: ${v.an_isread == 1 ? 'white' : '#F3F6F9'}">
                                    <div class="navi-icon mr-2">
                                        <i class="${v.amtn_classicon}" style="color: ${color[v.amtn_classcolor]}"></i>
                                    </div>
                                    <div class="navi-text">
                                        <div class="font-weight-bold">${v.an_keterangan}</div>
                                        <div class="text-muted">${v.an_docreate}</div>
                                    </div>
                                </div>
                            </a>`;
                    })
                } else {
                    html += `<div class="tab-pane" id="topbar_notifications_logs" role="tabpanel">
                            <div class="d-flex flex-center text-center text-muted min-h-200px">All caught up! 
                            <br>No new notifications.</div>
                        </div>`;
                }

                html += `<div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px; height: 300px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 109px;"></div></div></div>
                                </div>
                            </div>
                        </form>
                    </div>`;

                $('#n-notification').html(html)

                // alert(response)
                console.log(response)
            },
        });
    }

    $(document).ready(function() {
        socket.on('sendNotifToClient', (response) => {
            recreateNotification()
            getDatatable()
        });
        
        $('.modal.fade').appendTo('#pagepanel');
        // cb(start, end);

        // getData()
        // $.each(maincms['widget'], function(k, v) {
        //     maincms_widget[v.noid] = v;
        // })
        // $.each(maincms['widgetgrid'], function(k, v) {
        //     maincms_widgetgrid[v.idcmswidget] = v;
        // })
        // getCmsThead()
        // getCmsColumns()
        // $('#example_baru').attr('data-page-length', maincms['widgetgrid'][0]['recperpage']);
        // $('#widgeticon').addClass(maincms['widget'][0]['widgeticon']);
        // $('#widgetcaption').html(maincms['widget'][0]['widgetcaption']);
        // $('#judul_excel').attr('value', maincms['widget'][0]['widgetcaption']);
        // $('#tabel1').attr('value', maincms['menu']['noid']);
        // $('#namatable').attr('value', maincms['menu']['noid']);
        // $('#url_code').attr('value', maincms['menu']['noid']);
        // if (!maincms['widgetgrid'][0]['panelfilter']) {
        //     $('#space_filter_button').hide()
        //     $('#tool_filter').hide()
        // }
        // console.log(maincms['widgetgrid']['widgeticon'])
        // $('#purchase-request-detail').hide();

        $('#another-card').hide();
        $("#from-departement").hide();
        $("#tool-kedua").hide();
        $("#tool-ketiga").hide();
        $("#divsucces").hide();
        $('.datepicker').datepicker({
            format: 'd-m-yyyy'
            // startDate: '-3d'
        });
        $('.datepicker2').datepicker({
            format: 'd-m-yyyy'
            // startDate: '-3d'
        });
        // $(".first.paginate_button, .last.paginate_button").hide();

        // getChart(values);
        // getSlider(values2);
        // tabel(values3);
        // getDT(values3);
        // getForm()
        getDatatable();
        // setDatatableDetail();
        // getFilterButton(values3)

        function getChart(id) {
            $.each(id, function (key, val) {
                $.ajax({
                    type: "POST",
                    header:{
                        'X-CSRF-TOKEN':$('#token').val()
                    },
                    url: "/ambilchart",
                    dataType: "json",
                    data:{
                        "_token":$('#token').val(),
                        "id_cart": val
                    },
                    success: function (response) {
                        if (response.defaultchart == 'column') {
                            chartBatang(response,val);
                        } else if (response.defaultchart == 'pie') {
                            chartPie(response,val);
                        } else {
                            chartBatang(response,val);
                        }
                    },
                });
            });
        }

        function getCmsColumns() {
            var theadcolumns = '';
            var colspanfooter = 2;
            var index = 1;
            if (maincms['widgetgrid'][0]['colcheckbox']) {             
                colspanfooter = 3;
                index = 2;
                // theadcolumns += '<th></th>'
                theadcolumns += `<th style="width: 30px"><input type="checkbox" id="select_all" onchange="selectAllCheckbox()">&nbsp;&nbsp;&nbsp;&nbsp;</th>`;
            }
            theadcolumns += `<th style="width: 30px">No&nbsp;&nbsp;&nbsp;</th>`;
            var tfootcolumns = ``
            // console.log(maincms['widgetgridfield'])
            $.each(maincms['widgetgridfield'][0], function(k, v) {
                if (v.colwidth != 0) {
                    var colwidth = v.colwidth+'px';
                } else {
                    var colwidth = 'auto';
                }
                if (v.colshow == 1) {
                    theadcolumns += `<th class="text-`+v.colheaderalign.toLowerCase()+`" style="width: `+colwidth+`; height: `+v.colheight+`px;">`+v.fieldcaption+`</th>`
                    // theadcolumns += `<th>`+v.fieldcaption+`</th>`
                    // tfootcolumns += `<th></th>`
                    // var dataconfig = JSON.stringify(v.colsummaryfield)['dataconfig'];

                    if (v.colsummaryfield) {
                        var dataconfig = JSON.parse(v.colsummaryfield).dataconfig[0].summarytype+'('+index+')'
                        functionfooter.push({
                            "index": index,
                            "function": dataconfig,
                        })
                    } else {
                        var dataconfig = '';
                    }

                    if (v.colsummaryfield) {
                        tfootcolumns += `<th class="text-right" colspan="`+colspanfooter+`" data-function="`+dataconfig+`" style="border: 1px solid #95c9ed"></th>`
                        // console.log('collll '+colspanfooter)
                        colspanfooter = 1
                    } else {
                        colspanfooter++
                    }
                    index++
                }
                if (maincms['widgetgridfield'][0].length == k+1) {
                    // console.log('terakhir '+colspanfooter)
                    tfootcolumns += `<th class="text-right" colspan="`+(colspanfooter)+`" style="border: 1px solid #95c9ed"></th>`
                }
            })
            theadcolumns += `<th>Action</th>`
            console.log('tfootcolumns')
            console.log(tfootcolumns)
            $('#thead-columns').html(theadcolumns)
            $('#thead-columns-d').html(theadcolumns)
            $('#tfoot-columns').html(tfootcolumns)
            $('#tfoot-columns-d').html(tfootcolumns)
        }

        

        // console.log(my_table.context[0].jqXHR.responseJSON);
        $('#example_baru_filter input').unbind();

        // console.log(my_table.search($('#example_baru_filter input').val()).draw());
        // my_table.search('tes').draw();
        $('#example_baru_filter input').bind('keyup', function (e) {
            if (e.keyCode == 13) {
                my_table.search($('#example_baru_filter input').val()).draw();
            }
        });

        $('#formdepartement').on('keyup keypress', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });

        // infoDatatable()
        $(".container").css('display', 'block');

        // $.each(maincms['widget'], function(k, v) {
        //     maintable[v.noid] = v.maintable;
        // })
        ready = true;
        readyFullscreen()
        if ('<?= $triggercreate ?>') $('#btn-add-data').trigger('click');
    });

    function getCmsFilterButton(noid) {
        var buttonfilter = new Array();

        if (noid) {
            var each = JSON.parse(maincms_widget[noid]['configwidget'])['WidgetFilter'];
            var configwidget = JSON.parse(maincms_widget[noid]['configwidget']);
            var vnoid = noid;
        } else {
            var each = JSON.parse(maincms['widget'][0]['configwidget'])['WidgetFilter'];
            var configwidget = JSON.parse(maincms['widget'][0]['configwidget']);
            var vnoid = maincms['widget'][0]['noid'];
        }
        
        $.each(each, function(k, v) {
            // buttonfilter[k] = new Array();
            buttonfilter[k] = {
                'groupcaption' : v.groupcaption,
                'groupcaptionshow' : v.groupcaptionshow,
                'groupdropdown' : v.groupdropdown,
                'data' : new Array()
            }
            
            $.each(v.button, function(k2, v2) {
                var icon = '';
                if (v2.classicon != '') {
                    icon = '<i class="fa '+v2.classicon+'"></i> ';
                }
                var pecah = v2.condition[0].querywhere[0].fieldname.split('.');
                var buttoncaption = v2.buttoncaption.split(' ');
                var uniqeidbutton = '';
                if (buttoncaption.length > 1) {
                    $.each(buttoncaption, function(k3, v3) {
                        uniqeidbutton += v3
                    })
                } else {
                    uniqeidbutton = v2.buttoncaption
                }
                var oncl = 'onclick="filterData(\'btn-filter-'+uniqeidbutton+'-'+vnoid+'\', \'btn-filter-'+k+'-'+vnoid+'\', '+k+', \''+v2.buttoncaption+'\', \''+v2.classcolor+'\', '+v.groupdropdown+', false, \''+maincms['widget'][0]['maintable']+'\', \''+v2.condition[0].querywhere[0].operand+'\', \''+pecah[1]+'\', \''+v2.condition[0].querywhere[0].operator+'\', \''+v2.condition[0].querywhere[0].value+'\')" ';
                var button = '<button class="btn btn-outline-secondary btn-sm flat btn-filter-'+k+'-'+vnoid+'" id="btn-filter-'+uniqeidbutton+'-'+vnoid+'" style="background-color:#E3F2FD; color: '+v2.classcolor+'"'+oncl+'>'+icon+v2.buttoncaption+' </button>';
                if (v.groupdropdown == 1) {
                    button = '<button class="flat dropdown-item" id="btn-filter-'+uniqeidbutton+'-'+vnoid+'" '+oncl+'>'+icon+v2.buttoncaption+' </button>';
                }
                buttonfilter[k].data[k2] = {
                    'nama' : v2.buttoncaption,
                    'buttonactive' : v2.buttonactive,
                    'taga' : button,
                    'operand' : v2.condition[0].querywhere[0].operand,
                    'fieldname' : pecah[1],
                    'operator' : v2.condition[0].querywhere[0].operator,
                    'value' : v2.condition[0].querywhere[0].value,
                };
            })
        })

        
        var json_data = {
            "filterButton" : buttonfilter,
            "configWidget" : configwidget,
        };
        
        // console.log('test')
        $.each(json_data.filterButton, function(k, v) {
            $.each(v.data, function(k2, v2) {
                if (v2 != undefined) {
                    // console.log(v2.buttonactive)  
                }
            })
        })

        cmsfilterbutton = json_data;
        // return json_data;
    }

    function getFilterButton(values3, noid) {
        // $.ajax({
        //     type: "POST",
        //     header:{
        //         'X-CSRF-TOKEN':$('#token').val()
        //     },
        //     url: "/get_filter_button_tmd",
        //     dataType: "json",
        //     data:{
        //         "_token":$('#token').val(),
        //         table: maincms['menu']['noid'],
        //         maincms: JSON.stringify(maincms)
        //     },
        //     success: function(response) {
                getCmsFilterButton(noid)
                console.log('cmsfilterbutton')
                console.log(cmsfilterbutton)
                var response = cmsfilterbutton;

                if (noid) {
                    var vnoid = noid;
                } else {
                    var vnoid = maincms['widget'][0]['noid'];
                }
                // if (response == response2) {
                //     console.log('COCOKKKKK')
                // }
                // console.log(response)
                // console.log(response2)
                // return false

                var buttonactive = new Array();
                var filter_button = '';
                var groupdropdown = new Array();
                $.each(response.filterButton, function(k, v) {
                    // console.log(v.groupdropdown)
                    buttonactive.push('all-'+k+'-'+vnoid);
                    if (v.groupdropdown == 1) {
                        // alert(1)
                        filter_button += `<button class="btn btn-outline-secondary btn-sm flat btn-filter-all btn-filter-`+k+`-`+vnoid+`" id="btn-filter-all-`+k+`-`+vnoid+`" style="background-color:#90CAF9; font-weight: bold; color: black;">`+v.groupcaption+`</button>`;
                        filter_button += `<div class="dropdown btn-group" style="padding-right: 5px">`
                    } else {
                        // alert(0)
                        filter_button += `<button class="btn btn-outline-secondary btn-sm flat btn-filter-all btn-filter-`+k+`-`+vnoid+`" id="btn-filter-all-`+k+`-`+vnoid+`" style="background-color:#E3F2FD;" onclick="filterData('btn-filter-all-`+k+`-`+vnoid+`', 'btn-filter-`+k+`-`+vnoid+`', `+k+`, 'ALL', 'grey', 0, true)">ALL</button>`;
                    }
                    selected_filter.operand.push('')
                    selected_filter.fieldname.push('')
                    selected_filter.operator.push('')
                    selected_filter.value.push('')

                    $.each(v.data, function(k2, v2) {
                        if (v2 != undefined) {
                            if (v.groupdropdown == 1) {
                                if (k2 == 0) {
                                    if (v2.buttonactive) {
                                        buttonactive[k] = v2.nama;
                                        selected_filter.operand[k] = v2.operand;
                                        selected_filter.fieldname[k] = v2.fieldname;
                                        selected_filter.operator[k] = v2.operator;
                                        selected_filter.value[k] = v2.value;
                                        filter_button += `<button class="btn btn-secondary btn-sm flat dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <span id="filter-dropdown-active-`+k+`-`+vnoid+`">`+v2.nama+`</span>
                                                        </button>`;
                                    } else {
                                        filter_button += `<button class="btn btn-secondary btn-sm flat dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <span id="filter-dropdown-active-`+k+`-`+vnoid+`">ALL</span>
                                                        </button>`;
                                    }
                                    filter_button += `<div class="dropdown-menu flat" aria-labelledby="dropdownMenuButton">
                                                        <button class="dropdown-item btn-filter-all btn-filter-`+k+`-`+vnoid+`" id="btn-filter-all-`+k+`-`+vnoid+`"  onclick="filterData('btn-filter-all-`+k+`-`+vnoid+`', 'btn-filter-`+k+`-`+vnoid+`', `+k+`, 'ALL', 'grey', 1, true)">ALL</button>`;
                                }
                                groupdropdown.push(true);
                            } else {
                                if (v2.buttonactive) {
                                    buttonactive[k] = v2.nama;
                                    selected_filter.operand[k] = v2.operand;
                                    selected_filter.fieldname[k] = v2.fieldname;
                                    selected_filter.operator[k] = v2.operator;
                                    selected_filter.value[k] = v2.value;
                                }
                                groupdropdown.push(false);
                            }
                            filter_button += v2.taga;
                        }
                    })
                    
                    if (v.groupdropdown == 1) {
                        filter_button += `</div>
                                        </div>`;
                    }
                    filter_button += `<div class="btn" style="padding: 0px; width: 5px"></div>`
                })
                
                if (filter_button.length > 0) {
                    if (noid) {
                        $('#space_filter_button-'+noid).html(filter_button)
                    } else {
                        $('#space_filter_button').html(filter_button)
                    }
                    // $('#space_filter_button').css('margin-bottom', '20px')
                }
                
                console.log('buttonactive')
                console.log(buttonactive)
                console.log(groupdropdown)
                $.each(buttonactive, function(k, v) {
                    if (groupdropdown[k] == false) {
                        // alert('#btn-filter-'+buttonactive[k]+'-'+vnoid)
                        $('#btn-filter-'+buttonactive[k]).css('background', '#90CAF9');
                        $('#btn-filter-'+buttonactive[k]).css('color', 'black');
                        $('#btn-filter-'+buttonactive[k]).css('font-weight', 'bold');
                    }
                    // console.log(groupdropdown[k])
                    // console.log(buttonactive[k])
                })

                if (buttonactive.length > 0) {
                    destroyDatatable()
                    getDatatable()
                }
                
                
                //Datatable filter
                $.each(response.configWidget.WidgetFilter, function(k, v) {
                    var filtercontentgroup = '';
                    var filtercontentbody = '';

                    var classcolor = 'default';
                    var textcolor = 'text-dark';
                    var display = 'none';
                    if (k == 0) {
                        classcolor = 'purple';
                        textcolor = 'text-white'
                        display = 'block';
                        filter_tabs.push(true)
                    } else {
                        filter_tabs.push(false)
                    }
                    filtercontentgroup += `<input type="hidden" name="tabs[]" value="`+k+`">
                                            <div style="margin:5px 5px 0 0;" id="filter-content-group-`+k+`" onclick="changeTabFilter(`+k+`)" class="filter-content-group formbtn btn default `+classcolor+`">
                                                <span class="`+textcolor+`">
                                                    <i class="fa fa-indent `+textcolor+`"></i> `+k+`
                                                </span>
                                            </div>`;
                    filtercontenttabs.filtercontentgroup.push(filtercontentgroup)

                    var groupcaptionshow = `<option value="1">True</option>
                                            <option value="0" selected>False</option>`;
                    if (v.groupcaptionshow) {
                        groupcaptionshow = `<option value="1" selected>True</option>
                                            <option value="0">False</option>`
                    }

                    var groupdropdown = '';
                    if (v.groupdropdown == -1) {
                        groupdropdown = `<option value="-1" selected>Auto</option>
                                            <option value="0">False</option>
                                            <option value="1">True</option>`
                    } else if (v.groupdropdown == 0) {
                        groupdropdown = `<option value="-1">Auto</option>
                                            <option value="0" selected>False</option>
                                            <option value="1">True</option>`
                    } else if (v.groupdropdown == 1) {
                        groupdropdown = `<option value="-1">Auto</option>
                                            <option value="0">False</option>
                                            <option value="1" selected>True</option>`
                    }
                    filtercontentbody += `<div style="display: `+display+`" id="filter-content-body-`+k+`" class="filter-content-body formgroup col-md-12">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Group Caption</label>
                                                        <input name="groupcaption[]" id="groupcaption-`+k+`" class="form-control input-sm" value="`+v.groupcaption+`" placeholder="Group Caption">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Group Caption Showed</label>
                                                        <select name="groupcaptionshow[]" id="groupcaptionshow-`+k+`" class="form-control input-sm">
                                                            `+groupcaptionshow+`
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Group Dropdown Showed</label>
                                                        <select name="groupdropdown[]" id="groupdropdown-`+k+`" class="form-control input-sm">
                                                            `+groupdropdown+`
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="filter-content-row-`+k+`">
                                            </div>
                                            </div>`
                    filtercontenttabs.filtercontentbody.push(filtercontentbody)

                    filtercontenttabs.filtercontentrow[k] = new Array()
                    var _k2 = 0;
                    $.each(v.button, function(k2, v2) {
                        var filtercontentrow = '';

                        var condition = '';
                        $.each(v2.condition, function(k3, v3) {
                            condition += v3.groupoperand+'#'
                            $.each(v3.querywhere, function(k4, v4) {
                                condition += v4.fieldname+';'
                                condition += v4.operator+';'
                                condition += v4.value+';'
                                condition += v4.operand
                            })
                        })

                        var buttonactive = `<option value="1">True</option>
                                            <option value="0" selected>False</option>`
                        if (v2.buttonactive) {
                            buttonactive += `<option value="1" selected>True</option>
                                                <option value="0">False</option>`
                        }

                        filtercontentrow += `<input type="hidden" name="tabsrow[]" value="`+k+`">
                                            <div class="row filter-content-row-`+k+`">
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Caption</label>
                                                        <input class="form-control input-sm" name="buttoncaption[]" id="buttoncaption-`+k+`-`+_k2+`" value="`+v2.buttoncaption+`" placeholder="Caption">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Condition</label>
                                                        <input class="form-control input-sm" name="condition[]" id="condition-`+k+`-`+_k2+`" value="`+condition+`" placeholder="groupoperand#fieldname;operator;value;operand">
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Active</label>
                                                        <select class="form-control input-sm" name="buttonactive[]" id="buttonactive-`+k+`-`+_k2+`" value="1">
                                                            `+buttonactive+`
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Icon</label>
                                                        <input class="form-control input-sm" name="classicon[]" id="classicon-`+k+`-`+_k2+`" value="`+v2.classicon+`" placeholder="Icon">
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Color</label>
                                                        <input class="form-control input-sm" name="classcolor[]" id="classcolor-`+k+`-`+_k2+`" value="`+v2.classcolor+`" placeholder="Color">
                                                    </div>
                                                </div>
                                                <div class="col-md-1" style="text-align:center;padding:21px 0 0 0">
                                                    <div class="form-group">
                                                        <button id="del" onclick="delRow(`+_k2+`)" type="button" class="btn btn-sm red" style="height: 27px; width: 38px">
                                                            <i class="fa fa-minus-circle text-white"></i>
                                                        </button>
                                                        <button id="add" onclick="addRow()" type="button" class="btn btn-sm green" style="height: 27px; width: 38px">
                                                            <i class="fa fa-plus text-white"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>`
                        filtercontenttabs.filtercontentrow[k].push(filtercontentrow)
                        _k2++;
                    })
                    // filtercontentbody += `</div>
                    //                     </div>`
                })

                //Set to HTML
                var html_filtercontentgroup = '';
                var html_filtercontentbody = '';
                $.each(filtercontenttabs.filtercontentgroup, function(k, v) {
                    html_filtercontentgroup += v
                    html_filtercontentbody += filtercontenttabs.filtercontentbody[k]
                })
                $('#filtercontentgroup').html(html_filtercontentgroup);
                $('#filtercontentbody').html(html_filtercontentbody);

                $.each(filtercontenttabs.filtercontentrow, function(k, v) {
                    $('#filter-content-row-'+k).html(v);
                })
                //End Set to HTML

                // console.log(html_filtercontentrow)
                // console.log(filtercontenttabs)
            // },
        // });
    }

    // function addCommas(nStr) {
    //     nStr += '';
    //     var x = nStr.split('.');
    //     var x1 = x[0];
    //     var x2 = x.length > 1 ? '.' + x[1] : '';
    //     var rgx = /(\d+)(\d{3})/;
    //     while (rgx.test(x1)) {
    //         x1 = x1.replace(rgx, '$1' + ',' + '$2');
    //     }
    //     return x1 + x2;
    // }

    function getCmsTabColumns() {
        $.each(maincms['widget'], function(k, v) {
            if (k > 0) {
                var theadcolumns = `<th><input type="checkbox" id="select_all" onchange="selectAllCheckboxTab()"></th>
                                <th>No</th>`;
                var tfootcolumns = `<th></th>
                                    <th></th>`
                // console.log(maincms['widgetgridfield'])
                $.each(maincms['widgetgridfield'][k], function(k2, v2) {
                    if (v2.colshow == 1) {
                        theadcolumns += `<th class="text-`+v2.colheaderalign+`" style="width: `+v2.colwidth+`px; height: `+v2.colheight+`px;">`+v2.fieldcaption+`</th>`
                        tfootcolumns += `<th></th>`
                    }
                })
                theadcolumns += `<th>Action</th>`
                tfootcolumns += `<th></th>`
                $('#tab-thead-columns-'+v.maintable).html(theadcolumns)
                $('#tab-tfoot-columns'+v.maintable).html(tfootcolumns)
            }
        })
    }

    function getForm(isadd, widget_noid, element_id, responsefind=null) {
        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            url: "/get_form_tmd",
            dataType: "json",
            data:{
                "_token":$('#token').val(),
                table: maincms['menu']['noid'],
                maincms: JSON.stringify(maincms),
                isadd: isadd,
                key: 0,
                widget_noid: widget_noid,
            },
            beforeSend: function () {
                $('#'+element_id).LoadingOverlay("show");
            },
            success: function (response) {
                // console.log(response);
                var divform = ''
                divform += '<input type="hidden" id="noid" name="noid" value="">';
                // var colgroupname = [
                //     0: 'Administrasi'
                // ];
                // var colgroupname_data = [
                //     0: [
                //         //,
                //         //,
                //         //
                //     ]
                // ];
                // var colgroupname = new Array();
                // var colgroupname_data = new Array();
                $.each(response.field, function(key, value) {
                    // roles.fieldname.push(value.fieldname);
                    // roles.newhidden.push(value.newhidden);
                    // roles.edithidden.push(value.edithidden);

                    // if (value.colgroupname) {
                    //     if (colgroupname.indexOf(value.colgroupname) === -1) {
                    //         colgroupname.push(value.colgroupname);
                    //     }
                    //     colgroupname_data.push(value.field);
                    // } else {
                    //     // alert(key)
                        if (value.fieldname != 'idmaster') {
                            divform += value.field;
                        }
                    // }

                    // if (value.colsorted == 0) {
                    //     colsorted.push(key+1);
                    // }
                    
                });
                // colsorted[colsorted.length] = roles.fieldname.length+2;
                // colsorted.push(roles.fieldname.length+2);
                
                var divgroup = '';
                $.each(response.formgroupname, function(k, v) {
                    var formgroupname = k.split('-')

                    divgroup += `<div class="col-md-12">
                                    <div class="sub-card-custom">
                                        <div class="card-header card-header_ bg-main-1 d-flex">
                                            <div class="text-white" style="font-size: 14px">`+formgroupname[1].split(';')[1]+`</div>
                                        </div>
                                        <div class="card-body card-body-custom card-body-custom-group flat">
                                            <div class="right-tabs-group">`;
                                           
                    var key2 = 0;
                    var tab_widget = `<ul class="nav nav-tabs" id="tab-widget-`+k+`" role="tablist">`;
                    var tab_widget_content = `<div class="tab-content" id="tab-widget-content-`+k+`">`;
                    $.each(v, function(k2, v2) {
                        var active = '';
                        if (key2 == 0) {
                            active = 'active';
                        }

                        tab_widget += `<li class="nav-item">
                                        <a class="nav-link nav-link-custom flat `+active+`" id="tab-`+k2+`" data-toggle="tab" href="#`+k2+`" role="tab" aria-controls="`+k2+`"
                                        aria-selected="true" onclick="setTabActive(`+k2+`, '#tab-widget-`+k+` li a', '#tab-widget-`+k+` li #tab-`+k2+`', '#tab-widget-content-`+k+` .tab-widget-content-`+k+`', '#tab-widget-content-`+k+` #`+k2+`')">`+k2+`</a>
                                    </li>`;

                        tab_widget_content += `<div class="tab-pane tab-widget-content-`+k+` fade show `+active+`" id="`+k2+`" role="tabpanel" aria-labelledby="`+k2+`-tab">
                                                    <div class="row">`;
                        $.each(v2, function(k3, v3) {
                            tab_widget_content += v3;
                        })
                        tab_widget_content += `     </div>
                                                </div>`;
                        key2++;
                    });
                    tab_widget += `</ul>`;
                    tab_widget_content += `</div>`;
                    divgroup += tab_widget;
                    divgroup += tab_widget_content;

                    divgroup +=             `</div>
                                        </div>
                                    </div>
                                </div>`;
                });
                divform += divgroup;
                // console.log(colgroupname);
                // console.log(colgroupname_data);

                $('#'+element_id).html(divform);

                if (statusadd) {
                    $('#'+element_id).LoadingOverlay("hide");
                    // $('#another-card').show();
                } else {
                    if (element_id == 'divform') {
                        // console.log(widget_noid)
                        // console.log(divform)
                        
                        // $.ajax({
                        //     url: '/find_tmd',
                        //     type: "POST",
                        //     dataType: 'json',
                        //     header:{
                        //         'X-CSRF-TOKEN':$('#token').val()
                        //     },
                        //     data: {
                        //         "id" : nownoid,
                        //         "namatable" : $("#namatable").val(),
                        //         "_token": $('#token').val(),
                        //         maincms: JSON.stringify(maincms)
                        //     },
                        //     success: function (response) {
                                // alert(response.length);return false;
                                if (responsefind != null) {
                                    nownoid = responsefind.nownoid;

                                    var maincms_panel = new Array();
                                    $.each(maincms['panel'], function(k, v) {
                                        $.each(maincms['widget'], function(k2, v2) {
                                            if (v.idwidget == v2.noid) {
                                                maincms_panel[v.noid] = v2;
                                            }
                                        })
                                    })

                                    // console.log(responsefind.result_detail)
                                    var key = 0;
                                    $.each(responsefind.result_detail, function(k, v) {
                                        if (key == 0) {
                                            nownoidtab = maincms_panel[k].noid
                                        }
                                        // alert(k)
                                        // console.log(v)
                                        // localStorage.removeItem('input-data-'+k)
                                        localStorage.setItem('input-data-'+k, JSON.stringify(v))
                                        localStorage.setItem('tablename-'+k, maincms_panel[k].maintable)
                                        // console.log(v)
                                        key++;
                                    })
                                    // $('#noid').val(id)
                                    // console.log('responsefind')
                                    // console.log(responsefind)
                                    $.each(responsefind, function(k, v) {
                                        if (v.formtypefield == 11) {
                                            $("#"+v.tablename+'-'+v.field+' input').val(v.value);
                                        } else if (v.formtypefield == 31){
                                            if (v.value == '1') {
                                                $("#"+v.tablename+'-'+v.field).attr('checked', true);
                                            }
                                        } else {
                                            $("#"+v.tablename+'-'+v.field).val(v.value);
                                        }

                                        if (v.disabled) {
                                            $("#"+v.tablename+'-'+v.field).attr('disabled', 'disabled');
                                        }
                                        if (statusdetail) {
                                            $("#"+v.tablename+'-'+v.field).attr('disabled', 'disabled');
                                        }
                                        if (v.edithidden) {
                                            $("#div-"+v.field).css('display', 'none');
                                        }
                                    });
                                    $('#'+element_id).LoadingOverlay("hide");
                                    console.log(divform)
                                }
                            // }
                        // });
                    }
                }

                $.each(response.field, function(k, v) {
                    if (v.newhidden == 1) {
                        $('#div-'+v.fieldname).css('display', 'none');
                    }
                })    

                $.each(response.allfield, function(k, v) {
                    // if (v.tablename == 'fincashbankdetail' && v.fieldname == 'nominal') {
                    //     console.log('cekkkkkk')
                    //     console.log(JSON.parse(v.onchange))
                    // }
                    if (v.onchange) {
                        var onchange = JSON.parse(v.onchange);
                        $.each(onchange, function(k2, v2) {
                            if (v2.TriggerOn == 'OnChange') {
                                var variables = new Array();
                                $.each(v2.sourcefield, function(k3, v3) {
                                    variables.push(v3.alias)
                                })

                                $.each(v2.sourcefield, function(k3, v3) {
                                    localStorage.removeItem(v.tablename+'-'+v2.targetfield+'-$'+v3.alias);
                                    // localStorage.setItem('$'+v3.alias, 0);
                                    // alert(v3.fieldname)
                                    // if (v3.fieldname == 'nominal') {
                                    //     console.log('////////////////')
                                    //     console.log(v2.targetfield)
                                    // }
                                    if (v3.sourcetype == 'EditBox') {
                                        var value_onkeyup = document.querySelector('#'+v.tablename+'-'+v3.fieldname).getAttribute('onKeyUp');
                                        if (value_onkeyup == null) {
                                            value_onkeyup = ''
                                        }
                                        $('#'+v.tablename+'-'+v3.fieldname).attr('onKeyUp', value_onkeyup+`triggerOn(this, 'EditBox', '`+v.tablename+`', '`+JSON.stringify(variables)+`', '`+v2.throwtype+`', '`+v2.targettable+`', '`+v2.targetfield+`', '`+v3.alias+`', '`+v3.fieldname+`', '`+v3.fieldvalue+`', '`+v2.formula+`');`);
                                    } else {
                                        var value_onchange = document.querySelector('#'+v.tablename+'-'+v3.fieldname).getAttribute('onChange');
                                        if (value_onchange == null) {
                                            value_onchange = ''
                                        }
                                        $('#'+v.tablename+'-'+v3.fieldname).attr(v2.TriggerOn, value_onchange+`triggerOn(this, 'NotEditBox', '`+v.tablename+`', '`+JSON.stringify(variables)+`', '`+v2.throwtype+`', '`+v2.targettable+`', '`+v2.targetfield+`', '`+v3.alias+`', '`+v3.fieldname+`', '`+v3.fieldvalue+`', '`+v2.formula+`');`);
                                        $('#'+v.tablename+'-'+v3.fieldname).trigger('change')
                                        // console.log(v.tablename+'-'+v3.fieldname);
                                    }
                                })
                            } else if (v2.TriggerOn == 'OnSave') {
                                mainonchange.push(v2)
                            }
                        })
                    }

                    // if (v.newreadonly) {
                    //     $('#'+v.tablename+'-'+v.fieldname).
                    // }
                })    
                
                // console.log(mainonchange)

                // $.each(response.addonchange, function(k, v) {
                //     $('#'+v).attr('onChange', 'alert("add_onchange")')
                // })

                $('.datepicker').datepicker({
                    format: 'd-M-yyyy'
                    // startDate: '-3d'
                });

                //select2
                $('.select2').select2();
                $('.select2').css('width', '100%');
                $('.select2 .selection .select2-selection').css('height', '35px');
                $('.select2 .selection .select2-selection .select2-selection__rendered').css('margin-top', '-6px');
                $('.select2 .selection .select2-selection .select2-selection__arrow').css('margin-top', '4px');

                $.each(response.allfield, function(k, v) {
                    if (v.newreadonly) {
                        // alert('#select2-'+v.tablename+'-'+v.fieldname+'-container');
                        $('#div-'+v.tablename+'-'+v.fieldname+' span span .select2-selection--single').css('background-color', '#E5E5E5');
                    }
                })
                //select2readonly
                // $('.select2readonly').select2();
                // $('.select2readonly').css('width', '100%');
                // $('.currency').currency({region:'IDR'});
                // console.log(roles)  
                if (element_id != 'divform') {
                    $('#'+element_id).LoadingOverlay("hide");
                }
            },
        });
    }

    function triggerOn(context, sourcetype, tablename, variables, throwtype, targettable, targetfield, alias, fieldname, fieldvalue, formula) {
        // alert($('#idcurrency').val())
        // alert('#'+tablename+'-'+fieldname)
        var tt = tablename+'-'+targetfield;
        var tf = tablename+'-'+fieldname;
        
        if (sourcetype == 'EditBox') {
            localStorage.setItem(tt+'-$'+alias, $('#'+tf).val());
            // console.log(formula)
            // localStorage.removeItem('$'+alias);

            var replaceformula = formula;
            var f = formula.split('*');
            $.each(JSON.parse(variables), function(k, v) {
                var ls = localStorage.getItem(tt+'-$'+v);
                console.log('ls-'+tt+'-$'+v+' = '+localStorage.getItem(tt+'-$'+v))
                if (ls) {
                    // for (var i=0; i<10; i++) {
                        replaceformula = replaceformula.replace('$'+v, ls);
                        // console.log('ceking = '+ls)
                    // }
                } else {
                    replaceformula = replaceformula.replace('$'+v, 0);
                }
            })
            
            if (formula.split(' ')[0] != 'if') {
                $('#'+tt).val(eval(replaceformula))
            }
            // console.log('#'+tt)
            // console.log(replaceformula)
            // console.log(eval(replaceformula))
            // console.log(fieldname)
            // console.log(fieldvalue)
            // console.log(formula)
        } else {
            localStorage.setItem(tt+'-$'+alias, $('#'+tf+' :selected').val());
            // console.log('isi = '+$('#'+tf+' :selected').val())

            var getattribute = 'data-'+tf+'-'+$('#'+tf+' :selected').val();
            // console.log('testing = '+context.getAttribute(getattribute));
            // console.log('#'+tt)
            // alert(1)
            if (throwtype == 'FILTER') {
                $('#'+tt).attr('data-'+tf, $('#'+tf+' :selected').val())
                
                // console.log(context)
                // console.log('data-'+tt+'-querylookup')
                // console.log(context.getAttribute(getattribute))
                // console.log(document.querySelector('#'+tt))

                var frml = formula.split(';');
                var frml_where = '';
                $.each(frml, function(k, v) {
                    var _v = v.split(':');
                    if (_v[0] == 'WHERE') {
                        __v = _v[1].split(',');
                        frml_where += __v[0]+' '+__v[1]+' '+__v[2]+' '+__v[3]+' ';
                    }  
                })

                var querylookup = document.querySelector('#'+tt).getAttribute('data-'+tt+'-querylookup').split(';');
                var select = querylookup[0].split(':')[1];
                var from = querylookup[1].split(':')[1];
                var where = querylookup[2].split(':')[1].split('AND');
                var order = querylookup[3].split(':')[1].split(',');
        
                var query = 'SELECT '+select+' FROM '+from+' WHERE ';
                var add_frml = false;
                $.each(where, function(k, v) {
                    if (v) {
                        if (k > 0) {
                            query += 'AND ';
                            add_frml = true;
                        }
                        var _v = v.split(',');
                        query += _v[0]+' '+_v[1]+' '+_v[2]+' ';
                    }
                })
                if (add_frml) {
                    frml_where = frml_where.substr(4);
                }
                query += frml_where;
                
                query += ' ORDER BY '+order[0]+' '+order[1];
                query = query.replace('$'+alias, document.querySelector('#'+tt).getAttribute('data-'+tf))
                // console.log(frml_where.substr(4))

                getSelectChild(query, '#'+tt)

                // console.log('query = '+query)
                // console.log('filter = '+document.querySelector('#'+tt).getAttribute('data-'+tf))

            } else {
                $('#'+tt).val(context.getAttribute(getattribute))
            }
        }


        if (formula.split(' ')[0] == 'if') {
            var replaceformula = formula;
            $.each(JSON.parse(variables), function(k, v) {
                var ls = localStorage.getItem(tt+'-$'+v);
                console.log('ls-'+tt+'-$'+v+' = '+localStorage.getItem(tt+'-$'+v))
                if (ls) {
                    // for (var i=0; i<10; i++) {
                        replaceformula = replaceformula.replace('$'+v, ls);
                        // console.log('ceking = '+ls)
                    // }
                } else {
                    replaceformula = replaceformula.replace('$'+v, 0);
                }
            })
            eval("var fn = function(){ "+replaceformula+" }")
            // alert(replaceformula)
            // alert(fn())
            $('#'+tt).val(fn())
        }

        $('#'+tt).trigger('change')
        $('#'+tt).trigger('keyup')

        // console.log(targetfield)
        // console.log(context.getAttribute(getattribute))
        // console.log(context.getAttribute(getattribute))
        // if (targettable == 'self') {
        //     targettable = 'accmcurrency';
        //     $.ajax({
        //         type: "POST",
        //         header:{
        //             'X-CSRF-TOKEN':$('#token').val()
        //         },
        //         url: "/get_trigger_on_tmd",
        //         dataType: "json",
        //         data:{
        //             "_token":$('#token').val(),
        //             query: `SELECT `+fieldvalue+` AS `+alias+` FROM `+targettable+` WHERE noid='`+context.getAttribute(getattribute)+`' LIMIT 1`,
        //             alias: alias,
        //             formula: formula,
        //         },
        //         success: function(response) {
        //             $('#'+targetfield).val(response)
        //             console.log(response)
        //         }
        //     })
        // }
    }

    function getSelectChild(query, element_target) {
        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            url: "/get_select_child_tmd",
            data:{
                "_token":$('#token').val(),
                query: query,
            },
            success: function (response) {
                $(element_target).html(response)
            }
        })
    }

    function getFormTab() {
        var tab_form = '';
        var tab_form_content = '';
        $.each(maincms['widget'], function(k0, v0) {
            if (k0 > 0) {
                var active = '';
                if (k0 == 1) {
                    active = 'active';
                }

                $.ajax({
                    type: "POST",
                    header:{
                        'X-CSRF-TOKEN':$('#token').val()
                    },
                    url: "/get_form_tmd",
                    dataType: "json",
                    data:{
                        "_token":$('#token').val(),
                        table: maincms['menu']['noid'],
                        maincms: JSON.stringify(maincms),
                        isadd: true,
                        key: 1,
                    },
                    success: function (response) {
                        console.log('DDDDD');
                        console.log(response);
                        tab_form = `<li class="nav-item">
                                        <a class="nav-link flat `+active+`" id="tab-`+v0.maintable+`" data-toggle="tab" href="#`+v0.maintable+`" role="tab" aria-controls="`+v0.maintable+`"
                                        aria-selected="true" onclick="setTabActive(`+v0.maintable+`, '#tab-widget-`+v0.maintable+` li a', '#tab-widget-`+v0.maintable+` li #tab-`+v0.maintable+`', '#tab-widget-content-`+v0.maintable+` div', '#tab-widget-content-`+v0.maintable+` #`+v0.maintable+`')">`+k0+`</a>
                                    </li>`;
                        tab_form_content += `<form id="form-`+v0.maintable+`">
                                        <div class="row">`;
                        $.each(response.field, function(key, value) {
                            if (value.tablemaster != maincms['widget'][0]['maintable']) {
                                tab_form_content += value.field;
                            }
                        });
                        tab_form_content += `   </div>
                                    </form>`;

                        var tab_action = `<li class="nav-item" style="padding-top: 5px">
                                                <button class="btn btn-sm btn-primary"><i class="fa fa-plus"></i></button>
                                                <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                          </li>`;

                        $('#tab-form-'+v0.maintable).append(tab_form+tab_action);
                        $('#tab-form-content-'+v0.maintable).append(tab_form_content);

                        $('.datepicker').datepicker({
                            format: 'd-M-yyyy'
                        });
                    },
                });
            }
        });
    }

    function setTabActive(panel_noid, element_class, element_id, element_content_class, element_content_id) {
        // alert('setTabActive('+element_class+', '+element_id+', '+element_content_class+', '+element_content_id+')')
        nownoidtab = panel_noid
        another_selecteds = [];
        $(element_class).removeClass('active')
        $(element_id).addClass('active')
        $(element_content_class).removeClass('active')
        $(element_content_id).addClass('active')
        // alert(element_content_id)
    }

    function getCmsTab(statusadd) {
        var tab_widget = '';
        var tab_widget_content = '';
        // var maincms_widget = new Array();
        // $.each(maincms['widget'], function(k, v) {
        //     maincms_widget[v.noid] = v;
        // })
        var maincms_widgetgridfield = new Array();
        $.each(maincms['widgetgridfield'], function(k, v) {
            $.each(v, function(k2, v2) {
                maincms_widgetgridfield[v2.idcmswidgetgrid] = v;
            })
        })
        // console.log(maincms_widgetgridfield)
        $.each(maincms['panel'], function(k, v) {
        // console.log(maincms_widgetgridfield[v.idwidget].colshow)
            var v_widget = maincms_widget[v.idwidget];
            if (k > 0) {
                statusaddtab[v.noid] = true;

                var active = '';
                if (k == 1) {
                    active = 'active';
                }

                var tabcaption = v.tabcaption.split(';');

                tab_widget += `<li class="nav-item">
                                    <a class="nav-link nav-link-custom flat `+active+`" id="btn-tab-`+v.noid+`" data-toggle="tab" href="#`+v.noid+`" role="tab" aria-controls="`+v.noid+`"
                                    aria-selected="true" onclick="setTabActive(`+v.noid+`, '#tab-widget li a', '#tab-widget-`+v.noid+` li #btn-tab-`+v.noid+`', '#tab-widget-content .tab-widget-content', '#tab-widget-content-`+v.noid+`')">`+tabcaption[1]+`</a>
                                </li>`;

                tab_widget_content += `<div class="tab-pane tab-widget-content fade show `+active+`" id="tab-widget-content-`+v.noid+`" role="tabpanel" aria-labelledby="`+v.noid+`-tab">
                                            <div class="card card-custom flat">
                                                <div class="card-header card-header_ bg-main-1 d-flex flat" >
                                                    <div class="card-title">
                                                        <span class="card-icon">
                                                            <i class="`+v_widget.widgeticon+` text-white"></i>
                                                        </span>
                                                        <h3 class="card-label text-white">
                                                            `+v_widget.widgetcaption+`
                                                        </h3>
                                                    </div>`;

                // if (!statusadd) {
                    tab_widget_content += `         <div class="card-toolbar" >
                                                        <div id="add-data-`+v.noid+`">
                                                            <a onclick="addDataTab('`+v.noid+`')" href="javascript:;" id="btn-add-data-`+v.noid+`" style="color: #FFFFFF;margin-right: 5px;" class="btn btn-sm flat" onmouseover="mouseoverAddData('btn-add-data-`+v.noid+`')" onmouseout="mouseoutAddData('btn-add-data-`+v.noid+`')"><i style="color:#FFFFFF" class="fa fa fa-save"></i> Tambah Data</a>
                                                        </div>
                                                        <div id="save-data-`+v.noid+`" style="display: none">
                                                            <a onclick="saveDataTab(`+v.noid+`)" id="btn-save-data-`+v.noid+`" class="btn btn-sm btn-success flat" style="margin-right: 5px;">
                                                                <i class="fa fa-save"></i> Save
                                                            </a>
                                                        </div>
                                                        <div id="cancel-data-`+v.noid+`" style="display: none">
                                                            <a onclick="cancelDataTab('`+v.noid+`')" class="btn btn-sm btn-default flat" style="margin-right: 5px;">
                                                                <i class="fa fa-reply"></i> Cancel
                                                            </a>
                                                        </div>
                                                        <div id="back-data-`+v.noid+`" style="display: none">
                                                            <a id="btn-back-`+v.noid+`" class="btn btn-sm btn-success flat" onclick="cancelDataTab()"  style="margin-right: 5px;">
                                                                <i class="fa fa-reply"></i> Back
                                                            </a>
                                                        </div>
                                                        <div class="dropdown" style="margin-right: 5px;">
                                                            <a style="background-color: transparent !important; border: 1px solid #95c9ed; color: #aad4f0;" class="btn btn-default btn-sm flat" href="javascript:;" data-toggle="dropdown">
                                                                <i style="color:#FFFFFF" class="fa fa-cogs"></i>
                                                                    Tools
                                                                <i style="color:#FFFFFF" class="fa fa-angle-down"></i>
                                                            </a>
                                                            <ul style="" id="another-drop_export" class="dropdown-menu dropdown-menu-right">
                                                                <li id="another-space-button-export-`+v.noid+`"></li>
                                                            </ul>
                                                        </div>
                                                        <a style="background-image: none !important; margin-right: 5px; /* height: 27px; */ width: 27px !important; text-align: center; padding: 2px !important; border-radius: 50% !important; background-color: #2196F3; color: #FFF;" id="tool_refresh-`+v.noid+`" href="javascript:;" onclick="refresh(`+v.noid+`)" class="bg-main-1"><i style="color:#FFFFFF" class="fa fa-sync-alt"></i></a>
                                                        <a style="background-image: none !important; margin-right: 5px; /* height: 27px; */ width: 27px !important; text-align: center; padding: 2px !important; border-radius: 50% !important; background-color: #2196F3; color: #FFF;" id="tool_filter-`+v.noid+`" href="javascript:;" onclick="showModalFilter(`+v.noid+`)" class="bg-main-1"><i style="color:#FFFFFF" class="fa fa-filter"></i></a>
                                                        <a style="background-image: none !important; /* height: 27px; */ width: 27px !important; text-align: center; padding: 2px !important; border-radius: 50% !important; background-color: #2196F3; color: #FFF;" id="tool_refresh_up-`+v.noid+`" onClick="toggleButtonCollapse(`+v.noid+`)" href="javascript:;" class="bg-main-1"><i style="color:#FFFFFF" class="fa fa-sort"></i></a>
                                                    </div>`;
                // }

                         tab_widget_content += `</div>
                                                <div class="card-body card-body-tab-custom">
                                                    <div class="row" style="display: block">
                                                        <div id="space_filter_button-`+v.noid+`" style="margin-bottom: 10px">
                                                        </div>
                                                    </div>`
                             tab_widget_content += `<form data-toggle="validator" class="form" id="tab-form-`+v.noid+`" style="display: none">
                                                        <div class="row" id="tab-form-div-`+v.noid+`">
                                                        </div>
                                                    </form>`

                            // comment
                            //  tab_widget_content += `<table id="tab-table-`+v.noid+`" class="table table-striped table-bordered table-condensed nowrap" cellspacing="0" width="100%" style="background-color:#BBDEFB !important; border-collapse: collapse !important;">
                            //                             <thead>
                            //                                 <tr id="tab-thead-`+v.noid+`" class="bg-main">`

                            //             var colspan = 0;
                            //             $.each(maincms_widgetgridfield[v.idwidget], function(k2, v2) {
                            //                 if (v2.colshow == 1) {
                            //                     colspan++
                            //                 }
                            //             })
                            //             tab_widget_content += `<th style="text-align:center" colspan="2">#</th>`;
                            //             tab_widget_content += `<th style="text-align:center" colspan="`+colspan+`"></th>`;
                            //             tab_widget_content += `<th colspan="1" style="text-align:center">---</th>`;

                            //          tab_widget_content += `</tr>
                            //                                 <tr id="tab-thead-columns-`+v.noid+`">`

                                    
                            //             tab_widget_content += `<th><input type="checkbox" id="select_all_`+v.noid+`" onchange="selectAllCheckboxTab(`+v.noid+`)" class="checkbox-`+v.noid+` checkbox-`+v.noid+`-0" name="checkbox-`+v.noid+`[]" value="FALSE"></th>
                            //                                    <th>No</th>`;
                            //      $.each(maincms_widgetgridfield[v.idwidget], function(k2, v2) {
                            //          if (v2.colshow == 1) {
                            //              tab_widget_content += `<th class="text-`+v2.colheaderalign+`" style="width: `+v2.colwidth+`px; height: `+v2.colheight+`px;">`+v2.fieldcaption+`</th>`
                            //          }
                            //      })

                            //             tab_widget_content += `<th>Action</th>`

                            //          tab_widget_content += `</tr>
                            //                             </thead>
                            //                         </table>`
                            // endcomment


                            tab_widget_content += `<div id="tab-appendInfoDatatable-`+v.noid+`" style="border: 1px solid #3598dc;"></div>
                                                    <div id="tab-appendButtonDatatable-`+v.noid+`"></div>
                                                    <div id="ts-`+v.noid+`" class="table-scrollable" style="border: 1px solid #3598dc;">`
                             tab_widget_content += `<table bg-color="blue" class="nowrap table table-striped table-bordered table-hover  dataTable no-footer" id="tab-table-`+v.noid+`" style="background-color:#BBDEFB !important;"  aria-describedby="table_pagepanel_9921050101_info" role="grid">
                                                        <thead>
                                                            <tr id="tab-thead-`+v.noid+`" class="bg-main">`

                                        var colspan = 0;
                                        var colspanfooter = 0;
                                        var tfootcolumns = `<tfoot>
                                                                <tr>`
                                        var index = 2;
                                        var data_fft = new Array();
                                        $.each(maincms_widgetgridfield[v.idwidget], function(k2, v2) {
                                            if (v2.colshow == 1) {
                                                colspan++
                                                if (v2.colsummaryfield || v2.colsummaryfield != null) {
                                                    tfootcolumns += `<th class="text-right" colspan="`+colspanfooter+`" data-function="" style="border: 1px solid #95c9ed"></th>`
                                                    // console.log('collll '+colspanfooter)
                                                    colspanfooter = 1
                                                    var dataconfig = JSON.parse(v2.colsummaryfield).dataconfig[0].summarytype+'('+index+')'
                                                    data_fft.push({
                                                        "index": index,
                                                        "function": dataconfig,
                                                    })
                                                } else {
                                                    colspanfooter++
                                                }
                                                index++;
                                            }
                                            if (maincms_widgetgridfield[v.idwidget].length == k2+1) {
                                                // console.log('terakhir '+colspanfooter)
                                                tfootcolumns += `<th class="text-right" colspan="`+(colspanfooter)+`" style="border: 1px solid #95c9ed"></th>`
                                            }
                                        })
                                        
                                        functionfootertab[v.idwidget] = data_fft;
                                        
                                        tfootcolumns += `</tr>
                                                        </tfoot>`


                                        tab_widget_content += `<th style="text-align:center" colspan="2">#</th>`;
                                        tab_widget_content += `<th style="text-align:center" colspan="`+colspan+`"></th>`;
                                        tab_widget_content += `<th colspan="1" style="text-align:center">---</th>`;

                                     tab_widget_content += `</tr>
                                                            <tr id="tab-thead-columns-d-`+v.noid+`">`

                                    
                                        tab_widget_content += `<th style="width: 30px;"><input type="checkbox" id="select_all_`+v.noid+`" onchange="selectAllCheckboxTab(`+v.noid+`)" class="checkbox-`+v.noid+` checkbox-`+v.noid+`-0" name="checkbox-`+v.noid+`[]" value="FALSE"></th>
                                                               <th style="width: 30px;">No</th>`;
                                 $.each(maincms_widgetgridfield[v.idwidget], function(k2, v2) {
                                     if (v2.colshow == 1) {
                                         tab_widget_content += `<th class="text-`+v2.colheaderalign+`" style="width: `+v2.colwidth+`px; height: `+v2.colheight+`px;">`+v2.fieldcaption+`</th>`
                                     }
                                 })

                                        tab_widget_content += `<th style="width: auto;">Action</th>`

                                     tab_widget_content += `</tr>
                                                        </thead>`
                                     tab_widget_content += tfootcolumns
                                     tab_widget_content += `</table>`
                                                        
                         tab_widget_content += `</div>
                                            </div>
                                        </div>
                                    </div>`
            }
        })
        $('#tab-widget').html(tab_widget);
        $('#tab-widget-content').html(tab_widget_content);
    }

    function saveDataTab(panel_noid) {
        // localStorage.removeItem('input-data-'+panel_noid);

        // alert('nextnoid='+nextnoid);
        // alert('nownoid='+nownoid);
        // return false;
        var onrequired = false;
        var errormessage = '';
        if (statusaddtab[panel_noid] == true) {
            var idd = {};
            var idd_fix = new Array();
            $.each($('#tab-form-'+panel_noid).serializeArray(), function(k, v) {
                if (k == 0) {
                    idd['idmaster'] = statusadd ? nextnoid : nownoid;
                    idd['noid'] = nextnoidtab[panel_noid];
                    nextnoidtab[panel_noid]++;
                } else {
                    idd[v.name] = v.value
                    var tf = maintable[panel_noid]+'-'+v.name;
                    var coltypefield = document.querySelector('#'+tf).getAttribute('data-'+tf+'-coltypefield');
                    if (coltypefield == 96) {
                        idd[v.name+'_lookup'] = $('#'+tf+' :selected').text();
                        // alert(idd[v.name+'_lookup'])
                    }

                    var formcaption = $('#'+maintable[panel_noid]+'-'+v.name).attr('data-'+maintable[panel_noid]+'-formcaption');
                    if ($('#'+tf).attr('data-'+maintable[panel_noid]+'-newreq') == 1) {
                        if (v.value == '') {
                            onrequired = true;
                            errormessage += formcaption+' is required!<br>'
                        }
                    }
                }
            })

            if (onrequired) {
                $.notify({
                    message: errormessage
                },{
                    type: 'danger'
                });
                return false;
            }

            idd_fix[0] = idd;
            current_idd = localStorage.getItem('input-data-'+panel_noid);

            if (current_idd == null) {
                localStorage.setItem('input-data-'+panel_noid, JSON.stringify(idd_fix))
            } else {
                merge_idd = JSON.parse(current_idd).concat(idd_fix)
                localStorage.removeItem('input-data-'+panel_noid);
                localStorage.setItem('input-data-'+panel_noid, JSON.stringify(merge_idd))
            }   
            // console.log('SAVEDATATAB')
            // console.log(JSON.parse(localStorage.getItem('input-data-'+panel_noid)))
            // console.log(idd)
            // return false
            // refreshSumTab(0, panel_noid)
        } else {
            var idd = {};
            var idd_fix = new Array();
            $.each($('#tab-form-'+panel_noid).serializeArray(), function(k, v) {
                if (k == 0) {
                    idd['idmaster'] = nextnoid
                    idd['noid'] = nextnoidtab[panel_noid];
                    nextnoidtab[panel_noid]++;
                } else {
                    idd[v.name] = v.value
                    var tf = maintable[panel_noid]+'-'+v.name;

                    var formcaption = $('#'+maintable[panel_noid]+'-'+v.name).attr('data-'+maintable[panel_noid]+'-formcaption');
                    if ($('#'+tf).attr('data-'+maintable[panel_noid]+'-editreq') == 1) {
                        if (v.value == '') {
                            onrequired = true;
                            errormessage += formcaption+' is required!<br>'
                        }
                    }
                }
            })

            if (onrequired) {
                $.notify({
                    message: errormessage
                },{
                    type: 'danger'
                });
                return false;
            }

            idd_fix[0] = idd;
            current_idd = localStorage.getItem('input-data-'+panel_noid);

            var replace_current_idd = new Array();
            $.each(JSON.parse(current_idd), function(k, v) {
                if (v.noid == nownoidtab) {
                    replace_current_idd[k] = idd;
                    replace_current_idd[k]['noid'] = nownoidtab;
                    replace_current_idd[k]['idmaster'] = nownoid;
                } else {
                    replace_current_idd[k] = v;
                }
            })
            localStorage.removeItem('input-data-'+panel_noid);
            localStorage.setItem('input-data-'+panel_noid, JSON.stringify(replace_current_idd))
            // console.log(replace_current_idd)
            // console.log(idd)
        }

        $.each(mainonchange, function(k, v) {
            var current_idd_mo = JSON.parse(localStorage.getItem('input-data-'+panel_noid))
            // console.log(current_idd_mo)
            $.each(v.sourcefield, function(k2, v2) {
                if (v2.sourcetype == 'Summary') {
                    var summary = 0;
                    $.each(current_idd_mo, function(k3, v3) {
                        // console.log(v3)
                        summary = parseInt(summary)+parseInt(v3[v2.fieldname]);
                    })
                    $('#'+maintable[v.targettable]+'-'+v.targetfield).val(summary)
                    $('#'+maintable[v.targettable]+'-'+v.targetfield).trigger('change')
                    $('#'+maintable[v.targettable]+'-'+v.targetfield).trigger('keyup')
                    console.log('#'+maintable[v.targettable]+'-'+v.targetfield+' = '+summary)
                }
            })
        })


        refreshTab()
        cancelDataTab(panel_noid)
        // localStorage.removeItem('input-data-'+panel_noid);
        // console.log(JSON.parse(localStorage.getItem('input-data-'+panel_noid)))
        
        // console.log('aku cek ini')
        // console.log(JSON.parse(localStorage.getItem('input-data-'+panel_noid)))
        // console.log(panel_noid)
        // return false
    }

    function refreshSumTab(id, panel_noid) {

///////////////////

        // var maincms_widgetgrid = new Array();
        // $.each(maincms['widgetgrid'], function(k, v) {
        //     maincms_widgetgrid[v.idcmswidget] = v;
        // })

        if (maincms_widgetgrid[panel_noid]['gridconfig']) {
            // var wgb_onsave = maincms_widgetgrid[panel_noid]['gridconfig']['buttonevent']['OnSave'];
            var wgb_ondelete = JSON.parse(maincms_widgetgrid[panel_noid]['gridconfig'])['buttonevent']['OnDelete'];
            if (wgb_ondelete['status']) {
                $.each(wgb_ondelete['Parameter'], function(k, v) {
                console.log(v.Source['sourcetype']);
                // return false;
                    if (v.typeaction == 'EXEC_QUERY') {
                        // return false;
                        if (v['Source']['sourcetype'] == 'sum') {
                            var current_idd_mo = JSON.parse(localStorage.getItem('input-data-'+panel_noid))
                            var summary = 0;
                            var idtypetranc = '';
                            var idmaster = '';
                            var noid = '';
                            var globaluser = 59;
                            $.each(current_idd_mo, function(k2, v2) {
                                summary = parseInt(summary)+parseInt(v2[v['Source']['fieldname']]);
                                if (k2 == id) {
                                    idtypetranc = v2['idtypetranc'];
                                    idmaster = v2['idmaster'];
                                    noid = parseInt(nownoidtab)+parseInt(k2)+1;
                                }
                            })
                            if (v['Target']['targettype'] == 'EXEC_QUERY') {
                                var query = v['Query']['query'].replace('$idtypetranc', idtypetranc).replace('$idmaster', idmaster).replace('$noid', noid).replace('$GLOBAL_USER', globaluser)
                                // console.log(query)
                                // return false;
                                
                                $.ajax({
                                    type: "POST",
                                    header:{
                                        'X-CSRF-TOKEN':$('#token').val()
                                    },
                                    url: "/save_log_tmd",
                                    data:{
                                        "_token": $('#token').val(),
                                        "idconnection": v['Query']['idconnection'],
                                        "query": query,
                                    },
                                    success: function(response) {
                                    }
                                })
                            }
                        }
                    }
                })
            }
        }
    }
    
    function removeSelected(selecteds, scma=null) {
        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            url: "/delete_select"+main.urlcode,
            dataType: "json",
            data:{
                "_token": $('#token').val(),
                "selecteds": selecteds,
                "selectcheckboxmall": scma
            },
            success: function(response) {
                $.notify({
                    message: 'Delete data has been succesfullly.' 
                },{
                    type: 'success'
                });
                selectcheckboxm = new Array();
                refresh()
                // var filter_button = '';
                // for (var i=0; i<response.filterButton[0].length; i++) {
                //     filter_button += response.filterButton[0][i].taga;
                // }
                // $('#space_filter_button').html(filter_button)
                // console.log(filter_button);
            },
        });
    }

    function removeSelectedTab(selecteds) {
        var maincms_panel = new Array();
        $.each(maincms['panel'], function(k, v) {
            $.each(maincms['widget'], function(k2, v2) {
                if (v.idwidget == v2.noid) {
                    maincms_panel[v.noid] = v2;
                }
            })
        })

        // $.ajax({
        //     type: "POST",
        //     header:{
        //         'X-CSRF-TOKEN':$('#token').val()
        //     },
        //     url: "/delete_selected_tmd",
        //     dataType: "json",
        //     data:{
        //         "_token": $('#token').val(),
        //         "table" : maincms_panel[nownoidtab].maintable,
        //         "selecteds": selecteds,
        //     },
        //     success: function(response) {
                // var replace_input_data = localStorage.getItem('input-data-'+nownoidtab);
                var replaceinputdata = new Array();
                $.each(JSON.parse(localStorage.getItem('input-data-'+nownoidtab)), function(k, v) {
                    var notinput = false;
                    
                    $.each(selecteds, function(k2, v2) {
                        if (v.noid == v2) {
                            notinput = true
                        }
                    })

                    if (!notinput) {
                        replaceinputdata.push(v);
                    }
                })
                // console.log('data tidak terhapus')
                // console.log(replaceinputdata)
                // return false
                localStorage.setItem('input-data-'+nownoidtab, JSON.stringify(replaceinputdata))

                // console.log(replaceinputdata);
                // return false;

                $.notify({
                    message: 'Delete data has been succesfullly.' 
                },{
                    type: 'success'
                });
                refreshTab()
                // var filter_button = '';
                // for (var i=0; i<response.filterButton[0].length; i++) {
                //     filter_button += response.filterButton[0][i].taga;
                // }
                // $('#space_filter_button').html(filter_button)
                // console.log(filter_button);
        //     },
        // });
    }

    function getCmsThead() {
        var count_wgf = 0;
        var colgroupname = new Array();
        $.each(maincms['widgetgridfield'][0], function(k, v) {
            if (v.colshow == 1) {
                if (v.colgroupname) {
                    colgroupname.push(v.colgroupname);
                }
                count_wgf++;
            }
        })
        const unique = (value, index, self) => {
            return self.indexOf(value) === index
        }
        colgroupname = colgroupname.filter(unique)
        console.log(colgroupname)

        if (maincms['widgetgrid'][0]['colcheckbox']) {
            var thead = `<th style="text-align:center" colspan="2">#</th>`;
        } else {
            var thead = `<th style="text-align:center" colspan="1">#</th>`;
        }
        // $.each(maincms['widgetgridfield'], function(k, v) {
            thead += `<th style="text-align:center" colspan="`+count_wgf+`"></th>`;
        // })
        thead += `<th colspan="1" style="text-align:center">---</th>`;
        $('#thead').html(thead);
    }

    function getCmsTabThead(panel_idwidget) {
        $.each(maincms['widgetgridfield'], function(k, v) {
            if (k > 0) {
                if (v.idcmswidgetgrid == panel_idwidget)
                    if (v.colshow == 1) {
                        var thead = `<th style="text-align:center" colspan="2">#</th>`;
                        // $.each(maincms['widgetgridfield'], function(k, v) {
                            thead += `<th style="text-align:center" colspan="`+v.length+`"></th>`;
                        // })
                        thead += `<th colspan="1" style="text-align:center">---</th>`;
                        return thead;
                        // $('#tab-thead-'+maincms['widget'][k].maintable).html(thead);
                        // console.log('#tab-thead-'+maincms['widget'][k].maintable)
                    }
            }
        })
    }

    function saveFilter() {
        // $('#modal-notif').css('display', 'none')
        // var config_widget = {
        //     WidgetFilter: '',
        //     WidgetConfig: '',
        // }
        var formdata = $('#formfilter').serialize();

        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            url: "/save_filter_tmd",
            dataType: "json",
            data:{
                "_token":$('#token').val(),
                "data" :{
                    'data': formdata,
                    'url_code': $('#url_code').val()
                }
            },
            success:function(response){
                // console.log(response)
                if (response.result) {
                    // alert('Success')
                    closeModalFilter()
                    $.notify({
                        message: 'Save data has been succesfullly.' 
                    },{
                        type: 'success'
                    });
                    setInterval(function(){
                        location.reload()
                    }, 3000)
                    // location.reload()
                } else {
                    // $('#modal-notif').css('display', 'block')
                    // setInterval(function(){
                    //     $('#modal-notif').css('display', 'none')
                    // }, 3000)
                    
                    $.notify({
                        message: 'Save data has been error.' 
                    },{
                        type: 'danger'
                    });
                }
            }
        });
    }

    function delRow(key) {
        filtercontenttabs.filtercontentrow[selected_filter_tabs][key] = '';
        getFilterContentTabs()
    }

    function addRow() {
        filtercontenttabs.filtercontentrow[selected_filter_tabs].push(`<input type="hidden" name="tabsrow[]" value="`+selected_filter_tabs+`">
                                                                        <div class="row filter-content-row-`+selected_filter_tabs+`">
                                                                            <div class="col-md-2">
                                                                                <div class="form-group">
                                                                                    <label class="col-md-12 control-label">Caption</label>
                                                                                    <input class="form-control input-sm" name="buttoncaption[]" id="buttoncaption-`+selected_filter_tabs+`-`+filtercontenttabs.filtercontentrow[selected_filter_tabs].length+`" value="" placeholder="Caption">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label class="col-md-12 control-label">Condition</label>
                                                                                    <input class="form-control input-sm" name="condition[]" id="condition-`+selected_filter_tabs+`-`+filtercontenttabs.filtercontentrow[selected_filter_tabs].length+`" value="" placeholder="groupoperand#fieldname;operator;value;operand">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1">
                                                                                <div class="form-group">
                                                                                    <label class="col-md-12 control-label">Active</label>
                                                                                    <select class="form-control input-sm" name="buttonactive[]" id="buttonactive-`+selected_filter_tabs+`-`+filtercontenttabs.filtercontentrow[selected_filter_tabs].length+`" value="1">
                                                                                        <option value="1">True</option>
                                                                                        <option value="0" selected>False</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1">
                                                                                <div class="form-group">
                                                                                    <label class="col-md-12 control-label">Icon</label>
                                                                                    <input class="form-control input-sm" name="classicon[]" id="classicon-`+selected_filter_tabs+`-`+filtercontenttabs.filtercontentrow[selected_filter_tabs].length+`" value="" placeholder="Icon">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1">
                                                                                <div class="form-group">
                                                                                    <label class="col-md-12 control-label">Color</label>
                                                                                    <input class="form-control input-sm" name="classcolor[]" id="classcolor-`+selected_filter_tabs+`-`+filtercontenttabs.filtercontentrow[selected_filter_tabs].length+`" value="" placeholder="Color">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1" style="text-align:center;padding:21px 0 0 0">
                                                                                <div class="form-group">
                                                                                    <button id="del" onclick="delRow(`+filtercontenttabs.filtercontentrow[selected_filter_tabs].length+`)" type="button" class="btn btn-sm red" style="height: 27px; width: 38px">
                                                                                        <i class="fa fa-minus-circle text-white"></i>
                                                                                    </button>
                                                                                    <button id="add" onclick="addRow()" type="button" class="btn btn-sm green" style="height: 27px; width: 38px">
                                                                                        <i class="fa fa-plus text-white"></i>
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </div>`)
        getFilterContentTabs()
        // console.log(filtercontenttabs)
    }

    $('#delgroup').on('click', function() {
        filter_tabs.pop()
        filtercontenttabs.filtercontentgroup.pop()
        filtercontenttabs.filtercontentbody.pop()
        filtercontenttabs.filtercontentrow.pop()
        getFilterContentTabs()
    })

    $('#addgroup').on('click', function() {
        filter_tabs.push(false)

        filtercontenttabs.filtercontentgroup.push(`<input type="hidden" name="tabs[]" value="`+filtercontenttabs.filtercontentgroup.length+`">
                                                    <div style="margin:5px 5px 0 0;" id="filter-content-group-`+filtercontenttabs.filtercontentgroup.length+`" onclick="changeTabFilter(`+filtercontenttabs.filtercontentgroup.length+`)" class="filter-content-group formbtn btn default default">
                                                        <span class="text-dark">
                                                            <i class="fa fa-indent text-dark"></i> `+filtercontenttabs.filtercontentgroup.length+`
                                                        </span>
                                                    </div>`)

        filtercontenttabs.filtercontentbody.push(`<div style="display: none" id="filter-content-body-`+filtercontenttabs.filtercontentbody.length+`" class="filter-content-body formgroup col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="col-md-12 control-label">Group Caption</label>
                                                                <input name="groupcaption[]" id="groupcaption-`+filtercontenttabs.filtercontentbody.length+`" class="form-control input-sm" value="" placeholder="Group Caption">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="col-md-12 control-label">Group Caption Showed</label>
                                                                <select name="groupcaptionshow[]" id="groupcaptionshow-`+filtercontenttabs.filtercontentbody.length+`" class="form-control input-sm">
                                                                    <option value="1">True</option>
                                                                    <option value="0" selected>False</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="col-md-12 control-label">Group Dropdown Showed</label>
                                                                <select name="groupdropdown[]" id="groupdropdown-`+filtercontenttabs.filtercontentbody.length+`" class="form-control input-sm">
                                                                    <option value="-1" selected>Auto</option>
                                                                    <option value="0" selected>False</option>
                                                                    <option value="1">True</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="filter-content-row-`+filtercontenttabs.filtercontentbody.length+`">
                                                    </div>
                                                </div>`)
        filtercontenttabs.filtercontentrow.push(new Array())
        getFilterContentTabs()
    })

    function getFilterContentTabs() {
        //Set to HTML
        var html_filtercontentgroup = '';
        var html_filtercontentbody = '';
        var value = {
            groupcaption: new Array(),
            groupcaptionshow: new Array(),
            groupdropdown: new Array(),
            buttoncaption: new Array(),
            condition: new Array(),
            buttonactive: new Array(),
            classicon: new Array(),
        };

        //SET
        $.each(filtercontenttabs.filtercontentgroup, function(k, v) {
            value.buttoncaption[k] = new Array();
            value.condition[k] = new Array();
            value.buttonactive[k] = new Array();
            value.classicon[k] = new Array();

            if ($('#groupcaption-'+k).val() == undefined) { value.groupcaption.push('') } else { value.groupcaption.push($('#groupcaption-'+k).val()) }
            if ($('#groupcaptionshow-'+k).val() == undefined) { value.groupcaptionshow.push(0) } else { value.groupcaptionshow.push($('#groupcaptionshow-'+k).val()) }
            if ($('#groupdropdown-'+k).val() == undefined) { value.groupdropdown.push(-1) } else { value.groupdropdown.push($('#groupdropdown-'+k).val()) }
            
            $.each(filtercontenttabs.filtercontentrow[k], function(k2, v2) {
                if ($('#buttoncaption-'+k+'-'+k2).val() == undefined) { value.buttoncaption[k][k2] = '' } else { value.buttoncaption[k][k2] = $('#buttoncaption-'+k+'-'+k2).val() }
                if ($('#condition-'+k+'-'+k2).val() == undefined) { value.condition[k][k2] = '' } else { value.condition[k][k2] = $('#condition-'+k+'-'+k2).val() }
                if ($('#buttonactive-'+k+'-'+k2).val() == undefined) { value.buttonactive[k][k2] = 0 } else { value.buttonactive[k][k2] = $('#buttonactive-'+k+'-'+k2).val() }
                if ($('#classicon-'+k+'-'+k2).val() == undefined) { value.classicon[k][k2] = '' } else { value.classicon[k][k2] = $('#classicon-'+k+'-'+k2).val() }
            })

            html_filtercontentgroup += v
            html_filtercontentbody += filtercontenttabs.filtercontentbody[k]
        })

        $('#filtercontentgroup').html(html_filtercontentgroup);
        $('#filtercontentbody').html(html_filtercontentbody);

        $.each(filtercontenttabs.filtercontentbody, function(k, v) {
            if (filter_tabs[k] == true) {
                changeTabFilter(k)
            }
            $('#groupcaption-'+k).val(value.groupcaption[k])
            $('#groupcaptionshow-'+k).val(value.groupcaptionshow[k])
            $('#groupdropdown-'+k).val(value.groupdropdown[k])
        })

        $.each(filtercontenttabs.filtercontentrow, function(k, v) {
            $('#filter-content-row-'+k).html(v);

            $.each(v, function(k2, v2) {
                $('#buttoncaption-'+k+'-'+k2).val(value.buttoncaption[k][k2])
                $('#condition-'+k+'-'+k2).val(value.condition[k][k2])
                $('#buttonactive-'+k+'-'+k2).val(value.buttonactive[k][k2])
                $('#classicon-'+k+'-'+k2).val(value.classicon[k][k2])
            })
        })
        //End Set to HTML
        // console.log(value)
    }
    
    function changeTabFilter(key) {
        $('.filter-content-group').removeClass('purple').addClass('default')
        $('.filter-content-group span').removeClass('text-white').addClass('text-dark')
        $('.filter-content-group span i').removeClass('text-white').addClass('text-dark')
        $('#filter-content-group-'+key).removeClass('default').addClass('purple')
        $('#filter-content-group-'+key+' span').removeClass('text-dark').addClass('text-white')
        $('#filter-content-group-'+key+' span i').removeClass('text-dark').addClass('text-white')
        $('.filter-content-body').css('display', 'none')
        $('#filter-content-body-'+key).css('display', 'block')
        selected_filter_tabs = key;
        
        $.each(filter_tabs, function(k, v) {
            if (k == key) {
                filter_tabs[k] = true
            } else {
                filter_tabs[k] = false
            }
        })
    }

    function closeModalFilter() {
        $('#modal-filter').modal('hide')
    }

    function showModalFilter() {
        $('#modal-filter').modal('show')
    }

    function showModalCKEditor() {
        $('#modal-ckeditor').modal('show')
    }
    
    function getData() {
        // if (!JSON.parse(localStorage.getItem('maincms-'+values3))) {
            // localStorage.removeItem('maincms');
            $.ajax({
                type: "POST",
                header:{
                    'X-CSRF-TOKEN':$('#token').val()
                },
                url: "/get_data_tmd",
                dataType: "JSON",
                data:{
                    "_token":$('#token').val(),
                    "table": values3
                },
                success: function(response) {
                    localStorage.setItem('maincms-'+values3, JSON.stringify(response))
                }
            })
        // }
        maincms = JSON.parse(localStorage.getItem('maincms-'+values3))
        if (maincms == null) {
            location.reload()
        }
    }
    
    function searchform() {
        $("#searchform").keyup(function(e) {
            if(e.keyCode == 13) {
                mytable.search($("#searchform").val()).draw('page');
            }
        })
    }

    function searchformdetail() {
        $("#searchformdetail").keyup(function(e) {
            if(e.keyCode == 13) {
                mytabledetail.search($("#searchformdetail").val()).draw('page');
            }
        })
    }

    function searchformdetailprev() {
        $("#searchformdetailprev").keyup(function(e) {
            if(e.keyCode == 13) {
                mytabledetailprev.search($("#searchformdetailprev").val()).draw('page');
            }
        })
    }

    function searchformlog() {
        $("#searchformlog").keyup(function(e) {
            if(e.keyCode == 13) {
                mytablelog.search($("#searchformlog").val()).draw('page');
            }
        })
    }

    function searchformcostcontrol() {
        $("#searchformcostcontrol").keyup(function(e) {
            if(e.keyCode == 13) {
                mytablecostcontrol.search($("#searchformcostcontrol").val()).draw('page');
            }
        })
    }

    function searchtable() {
        if (searchtablewidth == 30) {
            searchtablewidth = 200
        } else {
            searchtablewidth = 30
        }
        $("#searchform").animate({ 
            width: searchtablewidth+'px' 
        }, 100); 
    }

    function searchtabledetail() {
        if (searchtablewidth == 30) {
            searchtablewidth = 200
        } else {
            searchtablewidth = 30
        }
        $("#searchformdetail").animate({ 
            width: searchtablewidth+'px' 
        }, 100); 
    }

    function searchtabledetailprev() {
        if (searchtablewidth == 30) {
            searchtablewidth = 200
        } else {
            searchtablewidth = 30
        }
        $("#searchformdetailprev").animate({ 
            width: searchtablewidth+'px' 
        }, 100); 
    }

    function searchtablelog() {
        if (searchtablewidth == 30) {
            searchtablewidth = 200
        } else {
            searchtablewidth = 30
        }
        $("#searchformlog").animate({ 
            width: searchtablewidth+'px' 
        }, 100); 
    }

    function searchtablecostcontrol() {
        if (searchtablewidth == 30) {
            searchtablewidth = 200
        } else {
            searchtablewidth = 30
        }
        $("#searchformcostcontrol").animate({ 
            width: searchtablewidth+'px' 
        }, 100); 
    }

    // function valuepage() {
    //     $("#valuepage").keyup(function(e) {
    //         if(e.keyCode == 13) {
    //             alert('masuk')
    //             mytable.page(3).draw('page');
    //         }
    //     })
    // }

    function prevpage() {
        mytable.page('previous').draw('page');
    }

    function prevpagedetail() {
        mytabledetail.page('previous').draw('page');
    }

    function prevpagedetailprev() {
        mytabledetailprev.page('previous').draw('page');
    }

    function prevpagelog() {
        mytablelog.page('previous').draw('page');
    }

    function prevpagecostcontrol() {
        mytablecostcontrol.page('previous').draw('page');
    }

    function nextpage() {
        mytable.page('next').draw('page');
    }

    function nextpagedetail() {
        mytabledetail.page('next').draw('page');
    }

    function nextpagedetailprev() {
        mytabledetailprev.page('next').draw('page');
    }

    function nextpagelog() {
        mytablelog.page('next').draw('page');
    }

    function nextpagecostcontrol() {
        mytablecostcontrol.page('next').draw('page');
    }

    function alengthmenu() {
        mytable.page.len($('#alengthmenu :selected').val()).draw();
    }

    function alengthmenudetail() {
        mytabledetail.page.len($('#alengthmenudetail :selected').val()).draw();
    }

    function alengthmenudetailprev() {
        mytabledetailprev.page.len($('#alengthmenudetailprev :selected').val()).draw();
    }

    function alengthmenulog() {
        mytablelog.page.len($('#alengthmenulog :selected').val()).draw();
    }

    function alengthmenucostcontrol() {
        mytablecostcontrol.page.len($('#alengthmenucostcontrol :selected').val()).draw();
    }

    function getCmsTabDatatable() {
        var maincms_widgetgridfield = new Array();
        
        $.each(maincms['widgetgridfield'], function(k, v) {
            $.each(v, function(k2, v2) {
                maincms_widgetgridfield[v2.idcmswidgetgrid] = v;
            })
        })

        $.each(maincms['panel'], function(k, v) {
            if (k > 0) {
                // console.log('aku cek ini')
                // console.log(JSON.parse(localStorage.getItem('input-data-'+v.noid)))
                // return false
                // console.log('maincms_widgetgrid')
                // console.log(maincms_widgetgrid[v.noid])
                if (!maincms_widgetgrid[v.noid]['panelfilter']) {
                    $('#space_filter_button-'+v.noid).hide()
                    $('#tool_filter'+v.noid).hide()
                } else {
                    getFilterButton(values3, v.noid)
                }

                // console.log('idd_fix')
                // console.log(JSON.parse(localStorage.getItem('input-data-'+v.noid)))
                // return false
                console.log(localStorage.getItem('input-data-'+v.noid))
                $.each(maincms_widgetgridfield[v.idwidget], function(k2, v2) {
                    if (k2 == 0) {
                        fieldnametab[v.noid] = new Array();
                        tablelookuptab[v.noid] = new Array();
                    }
                        // alert(v3.idcmswidgetgrid+' '+v.idwidget)
                    if (v2.colshow == 1) {
                        if (v2.coltypefield == 96) {
                            fieldnametab[v.noid].push(v2.fieldname+'_lookup');
                        } else {
                            fieldnametab[v.noid].push(v2.fieldname);
                        }
                        tablelookuptab[v.noid].push(v2.tablelookup);
                    }
                })
                // console.log('cekkkk')
                // console.log(fieldnametab[v.noid])
                // tab_datatable.push(v.maintable);


                var columns = new Array();
                var columndefs = new Array();
                var index = 1;
                var noncolsorted = new Array();
                var noncolsearch = new Array();
                if (maincms['widgetgrid'][k]['colcheckbox']) {
                    // columndefs.push({"targets": [0,1,21],"orderable": false})
                    noncolsorted.push(0)
                    noncolsorted.push(1)
                    noncolsearch.push(0)
                    noncolsearch.push(1)
                    columns.push({"data": 0,"class": "text-left"})
                    columns.push({"data": 1,"class": "text-left"})
                    index = 2;
                } else {
                    noncolsorted.push(0)
                    noncolsearch.push(0)
                    columndefs.push({"targets": [0,20],"orderable": false})
                    columns.push({"data": 0,"class": "text-left"})
                }
                $.each(maincms['widgetgridfield'][k], function(k, v) {
                    if (k == 0) {
                        if (maincms['widgetgrid'][k]['colcheckbox']) {
                            columndefs.push({
                                "name": v.fieldsource,
                                "height": '60px',
                                "targets": index
                            })
                        }
                        columndefs.push({
                            "name": v.fieldsource,
                            "height": '60px',
                            "targets": index
                        })
                    }
                    if (v.colshow) {
                        if (v.colwidth != 0) {
                            var colwidth = v.colwidth+'px';
                        } else {
                            var colwidth = '100px';
                        }
                        if (!v.colsorted) {
                            noncolsorted.push(index)
                        }
                        if (!v.colsearch) {
                            noncolsearch.push(index)
                        }
                        columns.push({
                            "data": index,
                            "width": colwidth,
                            "class": "text-"+v.colalign.toLowerCase()
                        })
                        columndefs.push({
                            "name": v.fieldsource,
                            "height": '60px',
                            "targets": index
                        })
                        index++
                    }
                })

                noncolsorted.push(index)
                noncolsearch.push(index)
                columndefs.push({"targets": noncolsorted,"orderable": false})
                columndefs.push({"targets": noncolsearch,"searchable": false})
                columns.push({"data": index,"class": "text-left"})
                columndefs.push({"height": '60px',"targets": index})
                console.log('columndefstabs')
                console.log(columns)
                // console.log(maincms['widgetgrid'][0].recperpage)
                // console.log('columndefs')
                // console.log(columndefs)
                // return false

                // alert('#tab-table-'+v.noid)
                my_tab_table = $('#tab-table-'+v.noid).DataTable({
                    "autoWidth": false,
                    "pageLength": maincms['widgetgrid'][k].recperpage,
                    "searching": true,
                    "processing": true,
                    "serverSide": true,
                    "bInfo": true,
                    "bFilter": false,
                    "bLengthChange": false,
                    "columns": columns,
                    "columnDefs": columndefs,
                    // "dom": '<"top dtfiler"<"">>',
                    // "dom": '<"info1 top dtfiler"pli<""f><"clear">>rt ',
                    // "dom": '<lf<t>ip>',
                    "infoCallback": function( settings, start, end, max, total, pre ) {
                        var options = '';
                        $.each(settings.aLengthMenu, function(k, v) {
                            options += '<option value="'+v+'" id="alm'+v+'" class="alm">'+v+'</option>'
                        })
                        if ((end-start) == settings._iDisplayLength-1) {
                            var valuepage = end/settings._iDisplayLength;
                        } else {
                            var valuepage = ((start-1)/settings._iDisplayLength)+1;
                        }
                        if (start == 1) {
                            var prevdisabled = 'disabled'
                        } else {
                            var prevdisabled = '';
                        }
                        if (end == total) {
                            var nextdisabled = 'disabled'
                        } else {
                            var nextdisabled = '';
                        }
                        // if (settings.oPreviousSearch.sSearch == $('#searchform').val()) {
                        //     alert($('#searchform').val())
                        //     var valuesearch = $('#searchform').val()
                        // } else {
                        //     var valuesearch = $('#searchform').val()
                        //     alert('else'+$('#searchform').val())
                        // }
                        if (settings.oPreviousSearch.sSearch != '') {
                            tab_statussearch[v.noid] = true
                            tab_searchtablewidth[v.noid] = 200
                            tab_valuesearch[v.noid] = settings.oPreviousSearch.sSearch
                        } else {
                            tab_statussearch[v.noid] = false
                            tab_searchtablewidth[v.noid] = 30
                            tab_valuesearch[v.noid] = ''
                        }
                        $('#tab-appendInfoDatatable-'+v.noid).html(`
                        <div class="row" style="background: #90CAF9; padding-left: 15px; padding-right: 15px;">
                            <div class="col-md-12">
                                Page
                                <button id="prevpage" onclick="prevpage(`+v.noid+`)" class="btn btn-sm default prev `+prevdisabled+`" style="background-color: #2196F3;">
                                    <i class="fa fa-angle-left">
                                    </i>
                                </button>
                                <input type="text" id="valuepage" onkeyup="valuepage()" class="input-mini" value="`+valuepage+`" style="text-align: center; border: 0px; margin: 5px 5px 5px 5px">
                                <button id="nextpage" onclick="nextpage(`+v.noid+`)" class="btn btn-sm default next `+nextdisabled+`" style="background-color: #2196F3;">
                                    <i class="fa fa-angle-right">
                                    </i>
                                </button> of <span class="pagination-panel-total">`+(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)+`</span>
                                <span class="seperator">|</span>
                                Rec/Page 
                                <select id="alengthmenu-`+v.noid+`" onchange="alengthmenu(`+v.noid+`)" name="table_pagepanel_9921050101_length" aria-controls="table_pagepanel_9921050101" class="input-inline input-xsmall input-sm recofdt" style="border: 0px; margin: 5px 5px 5px 5px">
                                    `+options+`
                                </select>
                                <span class="seperator">|</span>Total `+settings._iRecordsTotal+` records
                                <input id="searchform-`+v.noid+`" onkeyup="searchform(`+v.noid+`)" value="`+tab_valuesearch[v.noid]+`" type="search" class="form-control input-inline input-sm clicked" placeholder="" aria-controls="table_pagepanel_9921050101" style="width: `+tab_searchtablewidth[v.noid]+`px; position: absolute; right: 0px; top: 4px; height: 80%; border: 0;">
                                <div onclick="searchtable(`+v.noid+`)" class="btn fa fa-search text-right" style="position: absolute; right: 0px; top: 4px; height: 80%; background: transparent; margin: 0px 0px 0px 0px; padding: 5px 0px 5px 0px">
                                    <i class="fa fa-search"></i>
                                </div>
                            </div>
                        </div>`)
                        console.log('settings '+v.noid)
                        console.log(settings)
                        console.log(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)
                        $('.alm').removeAttr('selected');
                        $('#alm'+settings._iDisplayLength).attr('selected', 'selected');
                        // console.log(valuepage)
                        $('.dataTables_info').css('display', 'none')
                        $('.dataTables_paginate').css('display', 'none')
                        // return '<"top dtfiler"<"">>';
                    },
                    "ajax": {
                        "url": "/get_tab_data_tmd",
                        "type": "POST",
                        "dataType":'json',
                        "data": {
                            "_token":$('#token').val(),
                            key: k-1,
                            id: v.idwidget,
                            table: maintable[v.idwidget],
                            selected_filter: selected_filter,
                            maincms: JSON.stringify(maincms),
                            data: localStorage.getItem('input-data-'+v.noid),
                            tablename: localStorage.getItem('tablename-'+v.noid),
                            panelnoid: v.noid,
                            masternoid: nownoid,
                            fieldnametab: fieldnametab[v.noid],
                            tablelookuptab: tablelookuptab[v.noid],
                            statusadd: statusadd,
                        },
                        beforeSend: function () {
                            $(".dataTables_scroll").LoadingOverlay("show");
                        },
                        complete: function (response) {
                            $(".dataTables_scroll").LoadingOverlay("hide");
                            
                            if (response.responseJSON.noids_tab[v.noid].length > 0) {
                                noids_tab[v.noid] = response.responseJSON.noids_tab[v.noid];
                                select_all_tab[v.noid] = response.responseJSON.select_all_tab[v.noid];
                            }
                            // alert('asd')
                            // $.each(maincms['widgetgridfield'][0], function(k, v) {
                            //     $('#example_baru th:nth-child('+k+')').attr('style', 'padding: 0px 5px 0px 5px; width: '+v.colwidth+'px;')
                            //     $('#example_baru td:nth-child('+k+')').attr('style', 'padding: 0px 5px 0px 5px; width: '+v.colwidth+'px;')
                            //     alert(document.querySelector('.dataTables_scrollHeadInner #thead-columns th:nth-child('+k+')').getAttribute('style'))
                            // })
                            // $('#example_baru .sorting_1').attr('style', 'width: 103px')
                        },
                    },
                    "pagingType": "input",
                    "createdRow": function(row, data, dataIndex) {
                        // console.log(data)
                        $(row).addClass('my_row');
                        $(row).addClass('row'+dataIndex);
                        
                        console.log('hwgf')
                        $(row).css('height', '35px');
                        $(row).attr('onmouseover', 'showButtonActionTab(this, '+dataIndex+')');
                        // alert(dataindex)
                        $(row).attr('onmouseout', 'resetCssTab()');
                        // $(row).attr('onmouseover', 'getPositionXY(this)');
                    },
                    "oLanguage":{
                        "loadingRecords": "Please wait - loading...",
                        "sProcessing": "<div id='loadernya'></div>",
                        "sInfo": "&nbsp;|&nbsp;Total&nbsp;_TOTAL_ records",
                        "sLengthMenu": "&nbsp;|&nbsp;Rec/Page&nbsp;_MENU_",
                        "sInfoFiltered": "",
                        "sInfoEmpty": "| No records found to show",
                        "sEmptyTable": "No data available in table",
                        "sZeroRecords": "No matching records found",
                        "sSearch": "<button onclick='toggleButtonSearch()' class='btn flat dtfilter_search_btn' style='background-color: white'><i class='fa fa-search'></i></button>",
                        // "oPaginate": {
                        //   "previous": "<i class='fa fa-angle-left'></i>",
                        //   "next": "<i class='fa fa-angle-right'></i>",
                        //   "page": "Page",
                        //   "pageOf": "of"
                        // },
                        "oPaginate": {
                            "sPrevious": "<i class='fa fa-angle-left'></i>",
                            "sNext": "<i class='fa fa-angle-right'></i> ",
                        // "sPage": "<i id='page_prev'>Page</i>",
                        // "sPageOf": "<span id='page_next'> of </span>"
                        }
                    },
                    footerCallback: function ( row, data, start, end, display ) {
                        var api = this.api(), data;
                        var intVal = function ( i ) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '')*1 :
                                typeof i === 'number' ?
                                    i : 0;
                        };
                        // var _split = function ( i ) {
                        //     return typeof i === 'string' ?
                        //         parseInt(i.replace('.', '')).toString() :
                        //         typeof i === 'number' ?
                        //             i : 0;
                        // };
                        var count = function ( i ) {
                            return api.column( 5 )
                                    .data()
                                    .reduce( function (a, b) {
                                        return intVal(end);
                                    }, 0 )
                        }
                        var sum = function ( i ) {
                            return i
                            var value = 0;
                            $.each(api.column( i ).data(), function(k, v) {
                                var _split = v.split('.');
                                var value2 = '';
                                $.each(_split, function(k2, v2) {
                                    value2 += v2
                                })
                                value += Number(value2)
                            })
                            return value
                        }
                        var addcommas = function addCommas(nStr) {
                            nStr += '';
                            var x = nStr.split('.');
                            var x1 = x[0];
                            var x2 = x.length > 1 ? '.' + x[1] : '';
                            var rgx = /(\d+)(\d{3})/;
                            while (rgx.test(x1)) {
                                x1 = x1.replace(rgx, '$1' + '.' + '$2');
                            }
                            return x1 + x2;
                        }

                        $.each(functionfootertab[v.noid], function(k, v) {
                            $( api.column(v.index).footer() ).html(api.column(v.index).data().reduce( function (a, b) { return addcommas(eval(v.function)) }, 0 ));
                        })
                        // console.log('try')
                        // console.log(row)
                        console.log('functionfootertab')
                        console.log(functionfootertab)
                    }
                });

                mytabtable[v.noid] = my_tab_table

                // my_tab_table = $('#tab-table-'+v.noid).DataTable({
                //     "scrollX": true,
                //     "processing": true,
                //     "serverSide": true,
                //     "info": false,
                //     // "order": [[ 0, "asc" ]],
                //     "columnDefs": [ 
                //         { 
                //             orderable: false,
                //             targets: colsorted
                //             // targets: [0,1,30]
                //         }
                //     ],
                //     "pagingType": "input",
                //     "searching": false,
                //     "ajax": {
                //         "url": "/get_tab_data_tmd",
                //         "type": "POST",
                //         "dataType":'json',
                //         "data": {
                //             "_token":$('#token').val(),
                //             key: k-1,
                //             id: v.idwidget,
                //             table: 'fincashbankdetail',
                //             selected_filter: selected_filter,
                //             maincms: JSON.stringify(maincms),
                //             data: localStorage.getItem('input-data-'+v.noid),
                //             tablename: localStorage.getItem('tablename-'+v.noid),
                //             panelnoid: v.noid,
                //             masternoid: nownoid,
                //             fieldnametab: fieldnametab[v.noid],
                //             statusadd: statusadd,
                //         },
                //         beforeSend: function () {
                //             $(".dataTables_scroll").LoadingOverlay("show");
                //         },
                //         complete: function () {
                //             $(".dataTables_scroll").LoadingOverlay("hide");
                //         },
                //     },
                //     // "sPaginationType":"input",
                //     "dom": '<"top dtfiler"pli<""f><"clear">>rt ',
                //     "oLanguage":{
                //         "loadingRecords": "Please wait - loading...",
                //         "sProcessing": "<div id='loadernya'></div>",
                //         "sInfo": "&nbsp;|&nbsp;Total&nbsp;_TOTAL_ records",
                //         "sLengthMenu": "&nbsp;|&nbsp;Rec/Page&nbsp;_MENU_",
                //         "sInfoFiltered": "",
                //         "sInfoEmpty": "| No records found to show",
                //         "sEmptyTable": "No data available in table",
                //         "sZeroRecords": "No matching records found",
                //         "sSearch": `<button onclick="toggleButtonSearchTab('`+v.noid+`')" class="btn flat dtfilter_search_btn" style="background-color: white"><i class="fa fa-search"></i></button>`,
                //         // "oPaginate": {
                //         //   "previous": "<i class='fa fa-angle-left'></i>",
                //         //   "next": "<i class='fa fa-angle-right'></i>",
                //         //   "page": "Page",
                //         //   "pageOf": "of"
                //         // },
                //         "oPaginate": {
                //             "sPrevious": "<i class='fa fa-angle-left'></i>",
                //             "sNext": "<i class='fa fa-angle-right'></i> ",
                //         // "sPage": "<i id='page_prev'>Page</i>",
                //         // "sPageOf": "<span id='page_next'> of </span>"
                //         }
                //     },
                //     "createdRow": function(row, data, dataIndex) {
                //         // console.log(data)
                //         $(row).addClass('my_row');
                //         $(row).addClass('row'+dataIndex);
                //         // $(row).css('height', '5px');
                //         $(row).attr('onmouseover', 'showButtonActionTab(this, '+dataIndex+')');
                //         $(row).attr('onmouseout', 'resetCssTab()');
                //         // $(row).attr('onmouseover', 'getPositionXY(this)');
                //     },
                //     deferRender: true,
                //     // footerCallback: function ( row, data, start, end, display ) {
                //     //     var api = this.api(), data;
                //     //     var intVal = function ( i ) {
                //     //         return typeof i === 'string' ?
                //     //             i.replace(/[\$,]/g, '')*1 :
                //     //             typeof i === 'number' ?
                //     //                 i : 0;
                //     //     };
                //     //     var monTotal = api.column( 1 )
                //     //         .data()
                //     //         .reduce( function (a, b) {
                //     //             return intVal(a) + intVal(b);
                //     //         }, 0 );

                //     //     $( api.column( 3 ).footer() ).html(monTotal);
                //     // }
                // });
                
                new $.fn.dataTable.Buttons( my_tab_table, {
                    buttons: [
                        {
                            autoFilter: true,
                            text:      '<i class="fa fa-trash"></i> Delete Selected',
                            titleAttr: 'Delete Selected',
                            className: ' dropdown-item flat',
                            action:  function(e, dt, button, config) {
                                if (another_selecteds[v.noid].length == 0) {
                                    alert("There's no selected")
                                    // console.log(another_selecteds)
                                } else {
                                        // alert(another_selecteds)
                                        // alert(v.maintable)
                                    if (confirm('Are you sure you want to delete this?')) {
                                        removeSelectedTab(another_selecteds[v.noid])
                                    }
                                }
                            },
                        },{
                            autoFilter: true,
                            text:      '<i class="fa fa-trash"></i> Delete All',
                            titleAttr: 'Delete All',
                            className: ' dropdown-item flat',
                            action:  function(e, dt, button, config) {
                                if (noids_tab[v.noid].length == 0) {
                                    alert("There's no selected")
                                } else {
                                    if (confirm('Are you sure you want to delete this?')) {
                                        removeSelectedTab(noids_tab[v.noid])
                                    }
                                }
                            },
                        },{
                            extend:    'excel',
                            autoFilter: true,
                            text:      '<i class="fa fa-file-excel"></i> Export to Excel',
                            titleAttr: 'Export to Excel',
                            title: v.widgetcaption,
                            className: ' dropdown-item flat',
                            exportOptions: {
                                modifier: {
                                    search: 'applied',
                                    order: 'applied'
                                },
                                columns: [ 1,2,3, ':visible' ]
                                //                  <a style="font-size: 14px;min-width: 175px;
                                // text-align: left;" href="javascript:;"  class="dropdown-item">
                                // <i style="margin-top:10px;margin-right: 5px;" class="fa fa-file-excel"></i> Export to Excel </a>
                                                // columns: [ 0, ':visible' ]
                            },
                            action:  function(e, dt, button, config) {
                            // $('.loading').fadeIn();
                                var that = this;
                                setTimeout(function () {
                                    $.fn.dataTable.ext.buttons.excelHtml5.action.call(that,e, dt, button, config);
                                    // console.log(that);
                                    // console.log(e);
                                    // console.log(dt);
                                    // console.log(button);
                                    // console.log(config);
                                    $('.loading').fadeOut();
                                },500);
                            },
                        }
                    ]
                    // infoDatatable()
                });
                // infoDatatable()
                $('#tab-table-'+v.noid+'_first').hide();
                $('#tab-table-'+v.noid+'_last').hide();
                
                my_tab_table.buttons().container().appendTo('#another-space-button-export-'+v.noid);

                getForm(statusadd, v.idwidget, 'tab-form-div-'+v.noid);

                // $('#'+element_id).LoadingOverlay("hide");
                $('#another-card').show();
            }
        })

        if (!statusadd) {
            // $('#another-card').show();
        }
    }

    function destroyDatatable() {
        // $('#example_baru').DataTable().destroy();
        $('#table_pagepanel_9921050101').DataTable().destroy();
    }

    function destroyTabDatatable() {
        $.each(maincms['panel'], function(k, v) {
            if (k > 0) {
                $('#tab-table-'+v.noid).DataTable().destroy();
            }
        })
    }
// 

    function toggleButtonCollapse(noid) {
        if (noid) {
            $('#tab-widget-content-'+noid+' .card .card-body').toggleClass('collapse');
        } else {
            $('#kt_content .container .card .card-body').toggleClass('collapse');
        }
        // $('#kt_content .container .card .card-body').toggleClass('collapse').delay('slow').fadeOut();
    }

    function toggleButtonSearch() {
        $('#example_baru_filter label input').addClass('form-control flat')
        $('#example_baru_filter label input').toggleClass('clicked');
    }

    function toggleButtonSearchTab(element_id) {
        $('#tab-table-'+element_id+'_filter label input').addClass('form-control flat')
        $('#tab-table-'+element_id+'_filter label input').toggleClass('clicked');
    }

    function filterData(element_id, element_class, key, buttoncaption, classcolor, groupdropdown, is_all, table='', operand='', fieldname='', operator='', value='') {
        if (is_all) {
            selectedfilter[fieldname] = null;
        } else {
            selectedfilter[fieldname] = value;
        }

        if (groupdropdown == 1) {
            $('#filter-dropdown-active-'+key).text(buttoncaption)
            // alert(buttoncaption)
        } else {
            $('.'+element_class).css('background', '#E3F2FD');
            $('.'+element_class).css('color', classcolor);
            $('.'+element_class).css('font-weight', 'normal');
            $('#'+element_id).css('background', '#90CAF9');
            $('#'+element_id).css('color', 'black');
            $('#'+element_id).css('font-weight', 'bold');
        }
        
        getDatatable();
    }

    function getSlider(id) {
        var owl = $('.owl-carousel').owlCarousel({
            loop:true,
            smartSpeed: 100,
            autoplay: true,
            autoplaySpeed: 100,
            mouseDrag: false,
            margin:10,
            animateIn: 'slideInUp',
            animateOut: 'fadeOut',
            nav:false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });

        $.ajax({
            type: "POST",
            header:{
            'X-CSRF-TOKEN':$('#token').val()
            },
            url: "/get_slide",
            dataType: "json",
            data:{
                "_token":$('#token').val(),
                "id_slider": id
            },
            success: function (response) {
                // console.log(response.hasil);
                var base_url = '{{ url('/') }}';
                if(isNaN(base_url)) {
                    base_url = '';
                }
                var name1 = '';
                var name2 = '';

                $.each(response.hasil, function(i, item) {
                    // console.log(item.mainimage);
                    name1 = 'public/'+item.mainimage;

                    // console.log(name1);
                    var random_num =  Math.floor(Math.random()*60);
                    owl.trigger('add.owl.carousel', [jQuery('<div class="notification-message"> <img src="'+base_url+name1+'" class="user-image" alt=""> <div class="user-name">'+ item.newstitle+' <span class="lighter">from '+item.idnews+'</span></div> <div class="bought-details">Bought This <br>'+random_num+' minutes ago</div> </div>')]);
                });
                owl.trigger('refresh.owl.carousel');
            },
        });
    }

    function chartBatang(response,id_cart) {
        var dataatas = response.hasil;
        var dataProvideratas = [];// this variable you have to pass in dataProvider inside chart
        for(var key in dataatas) {
            dataProvideratas.push({
                category: dataatas[key]['categories'],
                visits: dataatas[key]['jumlahdata'],
            });
        }
        var chart = AmCharts.makeChart("kt_amcharts_"+id_cart, {
            "rtl": KTUtil.isRTL(),
            "type": "serial",
            "hideCredits":true,
            "theme": "light",
            "dataProvider": dataProvideratas,
            "valueAxes": [{
                "gridColor": "#FFFFFF",
                "gridAlpha": 0.2,
                "dashLength": 0
            }],
            "series": [{
                "type": "LineSeries",
                "stroke": "#ff0000",
                "strokeWidth": 3
            }],
            "valueAxes": [ {
                "position": "left",
                "title": "Jumlah Data"
            }, {
                "position": "bottom",
                "title": response.widget.widget_widgetcaption
            } ],
            "legend": {
                "useGraphSettings": true
            },
            "titles": [
                {
                    "size": 15,
                    "text":response.widget.widget_widgetcaption
                },
                {
                    "text": response.widget.widget_widgetsubcaption,
                    "bold": false
                }
            ],
            "gridAboveGraphs": true,
            "startDuration": 1,
            "graphs": [{
                "balloonText": "[[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "visits",
                "title": response.widget.widget_widgetcaption
            }],
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "category",
            "categoryAxis": {
                "gridPosition": "start",
            },
            "export": {
                "enabled": true
            }
        });
    }

    function chartPie(response,id_cart) {
        var data = response.hasil;
        var dataProvider = [];// this variable you have to pass in dataProvider inside chart
        for(var key in data) {
            dataProvider.push({
            country: data[key]['messagestatus'],
            litres: data[key]['jumlahdata'],
            });
        }

        var chart = AmCharts.makeChart("kt_amcharts_"+id_cart,{
            "type"    : "pie",
            "hideCredits"    : true,
            "theme"    : "light",
            "addClassNames": true,
            "titles": [
                {
                    "size": 15,
                    "text": response.widget.widget_widgetcaption
                },
                {
                    "text": response.widget.widget_widgetsubcaption,
                    "bold": false
                }
            ],
            // "pullOutDuration": 0,
            // "pullOutRadius": 0,

            "labelsEnabled": false,
            "titleField"  : "country",
            "valueField"  : "litres",
            "dataProvider"  : dataProvider,
            "numberFormatter": {
                "precision": -1,
                "decimalSeparator": ",",
                "thousandsSeparator": ""
            },
            "legend": {
                "align": "center",
                "position": "bottom",
                "marginRight": 0,
                "labelText": "[[title]]",
                "valueText": "",
                "valueWidth": 50,
                "textClickEnabled": true
            },
            "export": {
                "enabled": true
            }

        });
    }

    function setCheckbox(element_id) {
        if ($('#'+element_id).attr('checked')) {
            $('#'+element_id).val(0);
        } else {
            $('#'+element_id).val(1);
        }
    }

    $("#savedata__").click(function(e){
        e.preventDefault();
        // alert(nextnoid)
        // return false
        // localStorage.setItem('input-data', response)
        // console.log($("#formdepartement").serializeArray())
        var input_data_master = {};
        var mstrtbl = maincms['widget'][0].maintable;
        var onrequired = false;
        var errormessage = '';
        $.each($("#formdepartement").serializeArray(), function(k, v) {
            var formcaption = $('#'+mstrtbl+'-'+v.name).attr('data-'+mstrtbl+'-formcaption');
            if (v.name == 'noid') {
                input_data_master[v.name] = nextnoid
            } else {
                input_data_master[v.name] = v.value

                if (statusadd) {
                    if ($('#'+mstrtbl+'-'+v.name).attr('data-'+mstrtbl+'-newreq') == 1) {
                        if (v.value == '') {
                            onrequired = true;
                            errormessage += formcaption+' is required!<br>'
                        }
                    }
                } else {
                    if ($('#'+mstrtbl+'-'+v.name).attr('data-'+mstrtbl+'-editreq') == 1) {
                        if (v.value == '') {
                            onrequired = true;
                            errormessage += formcaption+' is required!<br>'
                        }
                    }
                }
            }
        })
        
        if (onrequired) {
            $.notify({
                message: errormessage
            },{
                type: 'danger'
            });
            return false;
        }


        if (!statusadd) {
            input_data_master['noid'] = nownoid;
        }
        
        localStorage.setItem('input-data', JSON.stringify(input_data_master))

        // var maincms_widget = new Array();
        // $.each(maincms['widget'], function(k, v) {
        //     maincms_widget[v.noid] = v;
        // })

        var input_data_detail = {};
        $.each(maincms['panel'], function(k, v) {
            if (k > 0) {
                input_data_detail[v.noid] = {
                    'data': JSON.parse(localStorage.getItem('input-data-'+v.noid)),
                    'table': maincms_widget[v.idwidget].maintable
                }
            }
        })
        // console.log(JSON.stringify(maincms))
        // console.log(input_data_detail)
        // return false
        
        /////////////////////////////////

        var kode = $("#kode").val();
        var nama = $("#nama").val();
        var namatable =$('#namatable').val();
        var formdata = $("#formdepartement").serialize();
        var formdatatab = [];
        
        if (statusadd) {
            $.each(maincms['widget'], function(k, v) {
                if (k > 0) {
                    formdatatab.push($('#form-'+v.maintable).serialize())
                }
            })
        }
        // console.log('input_data_detail')
        // console.log(input_data_detail)
        // return false

        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            url: "/save_tmd",
            dataType: "json",
            data:{
                "_token":$('#token').val(),
                "data" :{
                    'data' : formdata,
                    'datatab' : formdatatab,
                    'tabel': namatable,
                },
                "table": namatable,
                "editid": editid,
                "statusadd": statusadd,
                "masternoid": nownoid,
                maincms: JSON.stringify(maincms),
                "input_data": {
                    "master": input_data_master,
                    "detail": JSON.stringify(input_data_detail),
                }
            },
            success:function(result){
                // return false
                if (result.status == 'add') {
                    // $("#notiftext").text('Data  Berhasil di Tambah');
                    $.notify({
                        message: 'Add data has been succesfullly.' 
                    },{
                        type: 'success'
                    });
                } else {
                    // $("#notiftext").text('Data  Berhasil di Edit');
                    $.notify({
                        message: 'Edit data has been succesfullly.' 
                    },{
                        type: 'success'
                    });
                }

                //
                $('#another-card').hide();
                divform = '';
                another_selecteds = [];
                destroyTabDatatable()

                document.getElementById("formdepartement").reset();
                resetData()
                refresh();
                // $('#divsucces').show(0).delay(5000).hide(0);
                var x = document.getElementById("from-departement");
                var x2 = document.getElementById("tabledepartemen");
                var y = document.getElementById("tool-kedua");
                var y2 = document.getElementById("tool-pertama");
                x.style.display = "none";
                x2.style.display = "block";
                y.style.display = "none";
                y2.style.display = "block";
            }
        });
    });

    function refresh(noid) {
        if (noid) {
            $('#tab-table-'+noid).DataTable().ajax.reload();
        } else {
            $('#table_pagepanel_9921050101').DataTable().ajax.reload();
        }
    }

    function refreshTab() {
        destroyTabDatatable()
        getCmsTabDatatable()
    }

    function changeInput() {
        // console.log($('#formdepartement').serialize())
    }

    function getNextNoid() {
        $.ajax({
            url: '/get_next_noid_tmd',
            type: "POST",
            dataType: 'json',
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            data: {
                "table" : maincms['widget'][0]['maintable'],
                "_token": $('#token').val(),
                maincms: JSON.stringify(maincms)
            },
            success: function (response) {
                nextnoid = response
                localStorage.setItem('input-data-noid', response)
                // console.log(localStorage.getItem('input-data-noid'))
                // alert(nextnoid)
            }
        })
    }

    function getNextNoidTab(tablename, panel_noid) {
        $.ajax({
            url: '/get_next_noid_tmd',
            type: "POST",
            dataType: 'json',
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            data: {
                "table" : tablename,
                "_token": $('#token').val(),
                maincms: JSON.stringify(maincms)
            },
            success: function (response) {
                nextnoidtab[panel_noid] = response
                // console.log(nextnoidtab[panel_noid])
            }
        })
    }

    function cancelDataTab(panel_noid) {
        $('#form-detail_').hide();

        $('#tab-form-'+panel_noid).hide()
        $('#add-data-'+panel_noid).show()
        $('#save-data-'+panel_noid).hide()
        $('#cancel-data-'+panel_noid).hide()
        $('#tab-table-'+panel_noid+'_wrapper').show()
        $('#tab-appendInfoDatatable-'+panel_noid).show()
        $('#ts-'+panel_noid).show()
        $('#space_filter_button-'+panel_noid).show();
        $('#tool_refresh-'+panel_noid).show();
        $('#tool_filter-'+panel_noid).show()
        $('#tool_refresh_up-'+panel_noid).show();
    }

    function addDataTab(panel_noid) {
        console.log(nextnoidtab)
        resetDataTab(panel_noid)
        statusaddtab[panel_noid] = true;

        $('#form-detail_').show();
        $('#tab-form-'+panel_noid).show()
        $('#add-data-'+panel_noid).hide()
        $('#save-data-'+panel_noid).show()
        $('#cancel-data-'+panel_noid).show()
        $('#tab-table-'+panel_noid+'_wrapper').hide()
        $('#tab-appendInfoDatatable-'+panel_noid).hide()
        $('#ts-'+panel_noid).hide()
        $('#space_filter_button-'+panel_noid).hide();
        $('#tool_refresh-'+panel_noid).hide();
        $('#tool_filter-'+panel_noid).hide()
        $('#tool_refresh_up-'+panel_noid).hide();
    }

    function addData() {
        status.crudmaster = 'create';
        status.cruddetail = 'read';
        conditionDetail();
        // resetForm();
        // resetData();
        // alert('developing');
        // return false
        localStorage.removeItem(main.tabledetail);
        getInfo();
        getLastSupplier();
        getDatatableDetail(localStorage.getItem(main.tabledetail))
        setTab('tab-general', 'Header')
        $('#update-modal-detail').show();
        statusadd = true;
        statusdetail = false;
        // alert(maincms['widget'][0]['noid'])
        // getNextNoid();
        // $.each(maincms['widget'], function(k, v) {
        //     getNextNoidTab(v.maintable, v.noid)
        // })
        // getForm(statusadd, maincms['widget'][0]['noid'], 'divform');
        // getCmsTab(statusadd);
        // getCmsTabDatatable();
        // getFormTab();
        // $('#another-card').show();
        // var x = document.getElementById("from-departement");
        // var x2 = document.getElementById("tabledepartemen");
        // var y = document.getElementById("tool-kedua");
        // var y2 = document.getElementById("tool-pertama");
        // var tool_refresh = document.getElementById("tool_refresh");
        // var tool_filter = document.getElementById("tool_filter");
        // var tool_refresh_up = document.getElementById("tool_refresh_up");
        // var space_filter_button = $('#space_filter_button');

        // $.each(roles.fieldname, function(k, v) {
        //     if (roles.newhidden[k] == 1) {
        //         $('#div-'+v).css('display', 'none')
        //     } else {
        //         $('#div-'+v).css('display', 'block')
        //     }
        // })

        // if (x.style.display === "none") {
        //     x.style.display = "block";
        //     x2.style.display = "none";
        //     y.style.display = "block";
        //     y2.style.display = "none";
        //     tool_refresh.style.display = "none";
        //     tool_filter.style.display = "none";
        //     tool_refresh_up.style.display = "none";
        //     space_filter_button.css('display', 'none');
        // } else {
        //     x.style.display = "none";
        //     x2.style.display = "block";
        //     space_filter_button.css('display', 'block');
        // }
        // $('#form-header #cost-control').show();

        // $('#save-standard').show();
        // $('#save-dropdown').show();
        $('#tools-dropdown').hide();
        // $('#form-detail').show();
        // $('#purchase-request-detail').show();
        $('#from-departement').show();
        $('#tabledepartemen').hide();
        $('#tool-kedua').show();
        $('#tool-pertama').hide();
        $('#space_filter_button').hide();
        $('#tool_refresh').hide();
        $('#tool_filter').hide()
        $('#tool_refresh_up').hide();
        $('#another-card').show();
        // $('#form-header #kodereff').unfocus();
        $('#form-header #kodereff').focus();
    }

    //DOCUMENT
    // $('#pagepanel').keyup(function(e) {
    //     if (e.keyCode === 13) {
    //         alert('asd');
    //         localStorage.setItem(main.table+'-fullscreen', 'false');
    //     }
    // });

    //FOCUS 
    $('#form-header #kode').on('keydown',function(e){if(e.keyCode==13){$('#form-header #kodereff').focus();}})
    $('#form-header #kodereff').on('keydown',function(e){if(e.keyCode==13){$('#form-header #tanggal').focus();}})
    $('#form-header #tanggal').on('change',function(e){
        changeTanggal('tanggal', 'tanggal-modal')
        $('#form-header #tanggaldue').focus()
    })
    $('#form-header #tanggaldue').on('change',function(e){
        changeTanggal('tanggaldue', 'tanggaldue-modal')
        thenTanggalDue(ready ? true : false)
    })
    $('#form-header #idtypecostcontrol').on('select2:close',function(e){
        $('#form-header #idcostcontrol').val('0');
        $('#form-header #kodecostcontrol').val('NA');
        $('#form-header #cost-control').focus();
        getGenerateCode();
    })
    // $('#form-header #kodecostcontrol').on('keydown',function(e){if(e.keyCode==13){$('#form-header #cost-control').focus();}})
    $('#form-header #idcardrequest').on('select2:close',function(e){$('#form-header #idgudang').select2('focus');})
    // $('#form-header #idcardsupplier').on('select2:close',function(e){$('#form-header #idcompany').select2('focus')})
    // $('#form-header #idcompany').on('select2:close',function(e){$('#form-header #iddepartment').select2('focus')})
    // $('#form-header #iddepartment').on('select2:close',function(e){$('#form-header #idgudang').select2('focus')})
    $('#form-header #idgudang').on('select2:close',function(e){$('#deliverypoint').focus()})
    $('#form-header #deliverypoint').on('keypress',function(e){if(e.keyCode==13){$('#searchprev').focus();}})

    $('#form-internal #idcashbankbayar').on('select2:close',function(e){$('#form-internal #idakunpersediaan').select2('focus')})
    $('#form-internal #idakunpersediaan').on('select2:close',function(e){$('#form-internal #idakunpurchase').select2('focus')})
    $('#form-internal #idakunpurchase').on('select2:close',function(e){$('#form-detail #idinventor').select2('focus')})

    function thenIDCompany(ready) {
        ready ? $('#form-header #idcompany').select2('focus') : $('#form-header #idcompany').select2('close');
    }

    function thenIDDepartment(ready) {
        ready ? $('#form-header #idgudang').select2('focus') : $('#form-header #idgudang').select2('close');
    }
    
    function thenIDCashbankBayar(ready) {
        ready ? $('#form-header #idakunpersediaan').select2('focus') : $('#form-header #idakunpersediaan').select2('close');
    }

    function thenTanggalDue(ready) {
        // ready ? showModalCostControl() : hideModalCostControl();
        // $('#form-header #kodecostcontrol').focus()
        $('#form-header #idtypecostcontrol').select2('focus');
    }

    $('#tab-internal').on('click',function(e){
        if (!$('#form-internal #idcashbankbayar').val()) {
            $('#form-internal #idcashbankbayar').select2('focus')
        } else {
            if (!$('#form-internal #idakunpersediaan').val()) {
                $('#form-internal #idakunpersediaan').select2('focus')
            } else {
                if (!$('#form-internal #idakunpurchase').val()) {
                    $('#form-internal #idakunpurchase').select2('focus')
                }
            }
        }
    })

    $('#searchprev').on('keydown',function(e){if(e.keyCode==13){
        showModalDetailPrev();
    }})

    $('#form-detail #idinventor').on("select2:close", function(e) {
        changeKodeInventor();
        $('#form-detail #namainventor').focus();
    });
    $('#form-detail #namainventor').on('keydown',function(e){if(e.keyCode==13){$('#form-detail #unitprice').focus();}})
    $('#form-detail #unitprice').on('keydown',function(e){if(e.keyCode==13){$('#form-detail #unitqty').focus();}})
    $('#form-detail #unitqty').on('keydown',function(e){if(e.keyCode==13){
        $('#form-detail #idtypepackage').select2('focus')
    }})
    $('#form-detail #idtypepackage').on('select2:close',function(e){$('#form-detail #idgroupinv').select2('focus')})
    $('#form-detail #idgroupinv').on('select2:close',function(e){$('#form-detail #keterangandetail').focus()})
    $('#form-detail #keterangandetail').on('keypress',function(e){if(e.keyCode==13){
        if ($('#form-detail #unitprice').val() || $('#form-detail #unitqty').val()) {
            $('#form-detail #save-detail').focus();
            saveDetail();
            $('#form-detail #idinventor').select2('focus');
        } else {
            $.notify({
                message: 'Complete data, please!'
            },{
                type: 'danger'
            });
        }
    }})
    $('#form-detail #save-detail').on('click',function(e){if(e.keyCode==13){$('#form-detail #idinventor').select2('focus');}})
    $('#form-modal-detail #idsatuan-modal').on('change',function(e){
        let konversi = $('#form-modal-detail #idsatuan-modal :selected').attr('data-invmsatuan-konversi')
        $('#form-modal-detail #konvsatuan-modal').val(konversi)
    })
    $('#form-modal-detail #idtax-modal').on('change',function(e){
        let prosentax = $('#form-modal-detail #idtax-modal :selected').attr('data-accmpajak-prosentax')
        sumTotal();
    })

    //MODAL DETAIL
    let midinventor = $('#form-modal-detail #idinventor-modal');
    let mnamainventor = $('#form-modal-detail #namainventor-modal');
    let mketerangan = $('#form-modal-detail #keterangan-modal');
    let midcompany = $('#form-modal-detail #idcompany-modal');
    let middepartment = $('#form-modal-detail #iddepartment-modal');
    let midgudang = $('#form-modal-detail #idgudang-modal');
    let midsatuan = $('#form-modal-detail #idsatuan-modal');
    let mkonvsatuan = $('#form-modal-detail #konvsatuan-modal');
    let munitqty = $('#form-modal-detail #unitqty-modal');
    let munitqtysisa = $('#form-modal-detail #unitqtysisa-modal');
    let munitprice = $('#form-modal-detail #unitprice-modal');
    let msubtotal = $('#form-modal-detail #subtotal-modal');
    let mdiscountvar = $('#form-modal-detail #discountvar-modal');
    let mdiscount = $('#form-modal-detail #discount-modal');
    let mnilaidisc = $('#form-modal-detail #nilaidisc-modal');
    let midtypetax = $('#form-modal-detail #idtypetax-modal');
    let midtax = $('#form-modal-detail #idtax-modal');
    let mprosentax = $('#form-modal-detail #prosentax-modal');
    let mnilaitax = $('#form-modal-detail #nilaitax-modal');
    let mhargatotal = $('#form-modal-detail #hargatotal-modal');
    let mupdatedetail = $('#form-modal-detail #update-detail');


    midinventor.on('select2:close', function(e){
        sumTotal();
        midsatuan.val($('#form-modal-detail #idinventor-modal :selected').attr('data-invminventory-idsatuan')).trigger('change');
        munitprice.val($('#form-modal-detail #idinventor-modal :selected').attr('data-invminventory-purchaseprice'))
        mnamainventor.focus();
    });

    mnamainventor.on('keypress', function(e){
        if (e.keyCode == 13) {
            mketerangan.focus();
        }
    });

    midcompany.on('select2:close', function(e){
        middepartment.select2('focus');
    });

    middepartment.on('select2:close', function(e){
        midgudang.select2('focus');
    });

    midgudang.on('select2:close', function(e){
        midsatuan.select2('focus');
    });

    midsatuan.on('select2:close', function(e){
        sumTotal();
        munitqty.focus();
    });

    // mkonvsatuan.on('keyup', function(e){
    //     sumTotal();
    //     if (e.keyCode == 13) {
    //         munitqty.focus();
    //     }
    // });

    munitqty.on('keyup', function(e){
        sumTotal();
        if (e.keyCode == 13) {
            munitprice.focus();
        }
    });
    
    // munitqtysisa.on('keyup', function(e){
    //     sumTotal();
    //     if (e.keyCode == 13) {
    //         munitprice.focus();
    //     }
    // });

    munitprice.on('keyup', function(e){
        sumTotal();
        if (e.keyCode == 13) {
            mdiscount.focus();
        }
    });

    // mdiscountvar.on('keyup', function(e){
    //     sumTotal();
    //     if (e.keyCode == 13) {
    //         mdiscount.focus();
    //     }
    // });
    
    mdiscount.on('keyup', function(e){
        sumTotal();
        if (e.keyCode == 13) {
            midtax.select2('focus');
        }
    });

    // mnilaidisc.on('keyup', function(e){
    //     sumTotal();
    //     if (e.keyCode == 13) {
    //         midtypetax.focus();
    //     }
    // });

    // midtypetax.on('keyup', function(e){
    //     sumTotal();
    //     if (e.keyCode == 13) {
    //         midtax.focus();
    //     }
    // });

    midtax.on('select2:close', function(e){
        sumTotal();
        mupdatedetail.focus();
    });

    // mprosentax.on('keyup', function(e){
    //     sumTotal();
    //     if (e.keyCode == 13) {
    //         mnilaitax.focus();
    //     }
    // });

    // mnilaitax.on('keyup', function(e){
    //     sumTotal();
    //     if (e.keyCode == 13) {
    //         mupdatedetail.focus();
    //     }
    // });

    function sumTotal() {
        if ($('[name=searchtypeprev]:checked').val() == 1) {
            let unitqty = $('#form-modal-detail #unitqty-modal');
            let unitprice = $('#form-modal-detail #unitprice-modal');
            let subtotal = $('#form-modal-detail #subtotal-modal');
            let discount = $('#form-modal-detail #discount-modal');
            let nilaidisc = $('#form-modal-detail #nilaidisc-modal');
            let prosentax = $('#form-modal-detail #prosentax-modal');
            let _prosentax = $('#form-modal-detail #idtax-modal :selected').attr('data-accmpajak-prosentax');
            let nilaitax = $('#form-modal-detail #nilaitax-modal');
            let hargatotal = $('#form-modal-detail #hargatotal-modal');
            let resultsubtotal = parseInt(unitqty.val().replace(/[^0-9]/gi, ''))*parseInt(unitprice.val().replace(/[^0-9]/gi, ''));
            let resultnilaidisc = parseInt(resultsubtotal)*parseInt(discount.val().replace(/[^0-9]/gi, ''))/100;
            let resultprosentax = parseInt(_prosentax);
            let resultnilaitax = parseInt(resultsubtotal)*parseInt(_prosentax.replace(/[^0-9]/gi, ''))/100;
            let resulthargatotal = resultsubtotal-resultnilaidisc+resultnilaitax;

            subtotal.val(resultsubtotal);
            nilaidisc.val(resultnilaidisc);
            prosentax.val(resultprosentax);
            nilaitax.val(resultnilaitax);
            hargatotal.val(resulthargatotal);
        }
    }

    function getGenerateCode() {
        let noid = $('#form-header #noid').val();
        let formheader = {};
        let forminternal = {};
        let formfooter = {};
        let formmodaltanggal = {};
        let formmodalsupplier = {};

        $.each($('#form-header').serializeArray(), function(k, v){
            formheader[v.name] = v.value;
        });
        $.each($('#form-internal').serializeArray(), function(k, v){
            forminternal[v.name] = v.value;
        });
        $.each($('#form-footer').serializeArray(), function(k, v){
            formfooter[v.name] = v.value;
        });
        $.each($('#form-modal-tanggal').serializeArray(), function(k, v){
            formmodaltanggal[v.name] = v.value;
        });
        $.each($('#form-modal-supplier').serializeArray(), function(k, v){
            formmodalsupplier[v.name] = v.value;
        });

        $.ajax({
            type: 'POST',
            header:{
                'X-CSRF-TOKEN': $('#token').val()
            },
            url: '/generate_code'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('#token').val(),
                'idcostcontrol': $('#form-header #idcostcontrol').val(),
                'kodecostcontrol': $('#form-header #kodecostcontrol').val(),
                'noid': noid,
                'formheader': formheader,
                'forminternal': forminternal,
                'formfooter': formfooter,
                'formmodaltanggal': formmodaltanggal,
                'formmodalsupplier': formmodalsupplier
            },
            success: function(response) {
                $('#form-header #nourut').val(response.data.nourut);
                $('#form-header #kode').val(response.data.kode);
                hideLoading();
            }
        });
    }

    function getDepartmentByCC(params) {
        $.ajax({
            type: 'POST',
            header:{
                'X-CSRF-TOKEN': $('#token').val()
            },
            url: '/get_department_by_cc'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('#token').val(),
                'kodecostcontrol': params.kodecostcontrol,
            },
            success: function(response) {
                let html = '<option value="" id="iddepartment-empty">[Choose Department]</option>';

                $.each(response.data, (k,v) => {
                    html += `<option 
                                value="${v.noid}" id="iddepartment-${v.noid}"
                            >${v.kode} - ${v.nama}</option>`;
                })

                $('#iddepartment').html(html);

                $('.select2').select2();
                $('.select2').css('width', '100%');
                $('.select2 .selection .select2-selection').css('height', '35px');
                $('.select2 .selection .select2-selection .select2-selection__rendered').css('margin-top', '-6px');
                $('.select2 .selection .select2-selection .select2-selection__arrow').css('margin-top', '4px');
            }
        });
    }

    function getInfo() {
        $.when(
            $.ajax({
                type: 'POST',
                header:{
                    'X-CSRF-TOKEN': $('#token').val()
                },
                url: '/get_info'+main.urlcode,
                dataType: 'json',
                data: {
                    '_token': $('#token').val(),
                },
                success: function(response) {
                    $('#idstatustranc').val(response.data.mtypetranctypestatus.noid);
                    $('#idstatustranc-info').css('background-color', response.data.mtypetranctypestatus.classcolor);
                    $('.idstatustranc-info').text(response.data.mtypetranctypestatus.nama);
                    $('#idstatuscard').val(response.data.mcarduser.noid);
                    $('#idstatuscard-info').text(response.data.mcarduser.nama);
                    $('#dostatus').val(response.data.dostatus);
                    $('#dostatus-info').text(response.data.dostatus);
                    $('#form-header #idtypecostcontrol').val(response.data.idtypecostcontrol).trigger('change');
                    $('#form-header #idcostcontrol').val(response.data.idcostcontrol);
                    $('#form-header #kodecostcontrol').val(response.data.kodecostcontrol);
                    $('#form-header #idcompany').val(response.data.idcompany).trigger('change');
                    // $('#form-header #iddepartment').val(response.data.iddepartment).trigger('change');
                    $('#form-header #idgudang').val(response.data.idgudang).trigger('change');
                    $('#form-header #idcardrequest').val(response.data.idcardrequest).trigger('change');
                    $('#form-internal #idcashbankbayar').val('').trigger('change');
                    $('#form-internal #idakunpersediaan').val('').trigger('change');
                    $('#form-internal #idakunpurchase').val('').trigger('change');

                    let pr = response.data.pr;
                    let mttts = response.data.mttts;

                    $('#btn-saveto').html(getButtonSaveTo(pr,mttts,false));
                }
            })
        ).then(function(){
            getGenerateCode();
        })
    }

    function getLastSupplier() {
        // $.ajax({
        //     type: 'POST',
        //     header:{
        //         'X-CSRF-TOKEN': $('#token').val()
        //     },
        //     url: '/find_last_supplier'+main.urlcode,
        //     dataType: 'json',
        //     data: {
        //         '_token': $('#token').val(),
        //     },
        //     success: function(response) {
        //         $('#form-header #idcardsupplier').val(response.data.idcardsupplier).trigger('change');
        //         $('#form-modal-supplier #idakunsupplier-modal').val(response.data.idakunsupplier).trigger('change');
        //         $('#form-modal-supplier #suppliernama-modal').val(response.data.suppliernama);
        //         $('#form-modal-supplier #supplieraddress1-modal').val(response.data.supplieraddress1);
        //         $('#form-modal-supplier #supplieraddress2-modal').val(response.data.supplieraddress2);
        //         $('#form-modal-supplier #supplieraddressrt-modal').val(response.data.supplieraddressrt);
        //         $('#form-modal-supplier #supplieraddressrw-modal').val(response.data.supplieraddressrw);
        //         $('#form-modal-supplier #supplierlokasikel-modal').val(response.data.supplierlokasikel);
        //         $('#form-modal-supplier #supplierlokasikec-modal').val(response.data.supplierlokasikec);
        //         $('#form-modal-supplier #supplierlokasikab-modal').val(response.data.supplierlokasikab);
        //         $('#form-modal-supplier #supplierlokasiprop-modal').val(response.data.supplierlokasiprop);
        //         $('#form-modal-supplier #supplierlokasineg-modal').val(response.data.supplierlokasineg);
        //     }
        // });

        $('#form-header #idcardsupplier').val('').trigger('change');
        $('#form-modal-supplier #idakunsupplier-modal').val(0).trigger('change');
        $('#form-modal-supplier #suppliernama-modal').val('');
        $('#form-modal-supplier #supplieraddress1-modal').val('');
        $('#form-modal-supplier #supplieraddress2-modal').val('');
        $('#form-modal-supplier #supplieraddressrt-modal').val('');
        $('#form-modal-supplier #supplieraddressrw-modal').val('');
        $('#form-modal-supplier #supplierlokasikel-modal').val('');
        $('#form-modal-supplier #supplierlokasikec-modal').val('');
        $('#form-modal-supplier #supplierlokasikab-modal').val('');
        $('#form-modal-supplier #supplierlokasiprop-modal').val('');
        $('#form-modal-supplier #supplierlokasineg-modal').val('');
    }

    function resetForm() {
        $('#divform').html('')
    }

    function resetData() {
        $.each(maincms['panel'], function(k, v) {
            if (k == 0) {
                localStorage.removeItem('input-data');
            } else {
                localStorage.removeItem('input-data-'+v.noid);
            }
        })
    }

    function resetDataTab(panel_noid) {
        var maincms_widgetgridfield = new Array();
        $.each(maincms['widgetgridfield'], function(k, v) {
            $.each(v, function(k2, v2) {
                if (k2 == 0) {
                    maincms_widgetgridfield[panel_noid] = new Array();    
                }
                maincms_widgetgridfield[panel_noid][v2.fieldname] = v2;
            })
        })
        console.log('default')
        console.log(maincms_widgetgridfield)
        
        $.each(JSON.parse(localStorage.getItem('input-data-'+panel_noid)), function(k, v) {
            $.each(fieldnametab[panel_noid], function(k2, v2) {
                if (!v2.search('_lookup')) {
                    $("#tab-form-div-"+panel_noid+" div div #"+v2).attr('disabled', false);
                    $("#tab-form-div-"+panel_noid+" div div #"+v2).val(maincms_widgetgridfield[panel_noid][v2].coldefault);
                }
            })
        })
    }

    function showLoading() {
        $('#loading').show();
    }

    function hideLoading() {
        $('#loading').hide();
    }
    
    function cancelData() {
        $('#another-card').hide();
        divform = '';
        another_selecteds = [];
        destroyTabDatatable()

        $('#form-detail div').hide();

        localStorage.removeItem(main.tabledetail);
        undisableForm();
        unsetFormHeader();
        unsetFormInternal();
        unsetFormModalTanggal();
        // unsetFormModalSupplier();
        unsetFormDetail();
        unsetFormModalDetail();
        unsetFormFooter();
        $('.btn-view').hide();
        $('#update-modal-detail').show();
        // $('#save-standard').hide();
        // $('#save-dropdown').hide();

        $('#btn-saveto').html('');

        $('#tools-dropdown').show();
        $('#update-data').hide();
        $('#btn-save-data').removeAttr('disabled');
        // $('#purchase-request-detail').hide();
        $('#from-departement').hide();
        $('#tabledepartemen').show();
        $('#tool-kedua').hide();
        $('#tool-pertama').show();
        $('#space_filter_button').show();
        $('#tool_refresh').show();
        // if (!maincms['widgetgrid'][0]['panelfilter']) {
        //     $('#tool_filter').hide()
        // } else {
        //     $('#tool_filter').show();
        // }
        $('#tool_refresh_up').show();

        document.getElementById("formdepartement").reset();
        resetForm()
        resetData()
    }

    function undisableForm() {
        $('#form-header #kode').removeAttr('disabled');
        $('#form-header #generatekode').removeAttr('disabled');
        $('#form-header #kodereff').removeAttr('disabled');
        $('#form-header #tanggal').removeAttr('disabled');
        $('#form-header #tanggaldue').removeAttr('disabled');
        $('#form-header #idtypecostcontrol').removeAttr('disabled');
        // $('#form-modal-tanggal #tanggal-modal').removeAttr('disabled');
        $('#form-modal-tanggal #tanggaltax-modal').removeAttr('disabled');
        // $('#form-modal-tanggal #tanggaldue-modal').removeAttr('disabled');
        $('#form-header #idcardsupplier').removeAttr('disabled');
        $('#form-modal-supplier #idakunsupplier-modal').removeAttr('disabled');
        $('#form-modal-supplier #suppliernama-modal').removeAttr('disabled');
        $('#form-modal-supplier #supplieraddress1-modal').removeAttr('disabled');
        $('#form-modal-supplier #supplieraddress2-modal').removeAttr('disabled');
        $('#form-modal-supplier #supplieraddressrt-modal').removeAttr('disabled');
        $('#form-modal-supplier #supplieraddressrw-modal').removeAttr('disabled');
        $('#form-modal-supplier #supplierlokasikel-modal').removeAttr('disabled');
        $('#form-modal-supplier #supplierlokasikec-modal').removeAttr('disabled');
        $('#form-modal-supplier #supplierlokasikab-modal').removeAttr('disabled');
        $('#form-modal-supplier #supplierlokasikab-modal').removeAttr('disabled');
        $('#form-modal-supplier #supplierlokasiprop-modal').removeAttr('disabled');
        $('#form-modal-supplier #supplierlokasineg-modal').removeAttr('disabled');
        $('#form-detail #idinventor').removeAttr('disabled');
        $('#form-detail #namainventor').removeAttr('disabled');
        $('#form-detail #unitqty').removeAttr('disabled');
        $('#form-detail #update-detail').hide();
        $('#form-detail #save-detail').show();
        // $('#form-modal-detail #namainventor-modal').removeAttr('disabled');
        $('#form-modal-detail #idinventor-modal').removeAttr('disabled');
        $('#form-modal-detail #namainventor-modal').removeAttr('disabled');
        $('#form-modal-detail #idtypepackage-modal').removeAttr('disabled');
        $('#form-modal-detail #idgroupinv-modal').removeAttr('disabled');
        $('#form-modal-detail #keterangan-modal').removeAttr('disabled');
        $('#form-modal-detail #idgudang-modal').removeAttr('disabled');
        $('#form-modal-detail #idcompany-modal').removeAttr('disabled');
        $('#form-modal-detail #iddepartment-modal').removeAttr('disabled');
        $('#form-modal-detail #idsatuan-modal').removeAttr('disabled');
        $('#form-modal-detail #konvsatuan-modal').removeAttr('disabled');
        $('#form-modal-detail #unitqty-modal').removeAttr('disabled');
        $('#form-modal-detail #unitqtysisa-modal').removeAttr('disabled');
        $('#form-modal-detail #unitprice-modal').removeAttr('disabled');
        $('#form-modal-detail #subtotal-modal').removeAttr('disabled');
        $('#form-modal-detail #discountvar-modal').removeAttr('disabled');
        $('#form-modal-detail #discount-modal').removeAttr('disabled');
        $('#form-modal-detail #nilaidisc-modal').removeAttr('disabled');
        $('#form-modal-detail #idtypetax-modal').removeAttr('disabled');
        $('#form-modal-detail #idtax-modal').removeAttr('disabled');
        $('#form-modal-detail #prosentax-modal').removeAttr('disabled');
        $('#form-modal-detail #nilaitax-modal').removeAttr('disabled');
        $('#form-modal-detail #hargatotal-modal').removeAttr('disabled');
        $('#form-header #idcompany').removeAttr('disabled');
        $('#form-header #iddepartment').removeAttr('disabled');
        $('#form-header #idcardrequest').removeAttr('disabled');
        $('#form-header #idgudang').removeAttr('disabled');
        $('#form-header #deliverypoint').removeAttr('disabled');
        $('#form-footer #keterangan').removeAttr('disabled');
        $('#form-internal #idcashbankbayar').removeAttr('disabled');
        $('#form-internal #idakunpersediaan').removeAttr('disabled');
        $('#form-internal #idakunpurchase').removeAttr('disabled');
    }

    function unsetFormHeader() {
        $('#form-header #kode').val('');
        $('#form-header #kodereff').val('');
        $('#form-header #idstatustranc').val(1).trigger('change');
        $('#form-header #idstatustranc').removeAttr('style');
        $('#form-header #idstatustranc-info').css('background-color', '<?= $color[$mtypetranctypestatus_classcolor] ?>');
        $('#form-header #idstatustranc-info b').text('<?= $mtypetranctypestatus_nama ?>');
        if (!$('#form-header #idstatuscard').val() == '') {
            $('#form-header #idstatuscard').val('').trigger('change');
        }
        $('#form-header #dostatus').val('');
        $('#form-header #tanggal').val('<?= $defaulttanggal ?>');
        $('#form-header #tanggaldue').val('<?= $defaulttanggaldue ?>');
        // if (!$('#form-header #idcardsupplier').val() == '') {
        //     $('#form-header #idcardsupplier').val('').trigger('change');
        // }
        if (!$('#form-header #idcompany').val() == '') {
            $('#form-header #idcompany #idcompany-empty').attr('selected', 'selected');
            $('#select2-idcompany-container').text($('#form-header #idcompany #idcompany-empty').text());
        }
        if (!$('#form-header #iddepartment').val() == '') {
            $('#form-header #iddepartment #iddepartment-empty').attr('selected', 'selected');
            $('#select2-iddepartment-container').text($('#form-header #iddepartment #iddepartment-empty').text());
        }
        if (!$('#form-header #idgudang').val() == '') {
            $('#form-header #idgudang #idgudang-empty').attr('selected', 'selected');
            $('#select2-idgudang-container').text($('#form-header #idgudang #idgudang-empty').text());
        }
        $('#form-header #deliverypoint').val('');
        $('#form-header #idtypecostcontrol').val('').trigger('change');
        $('#form-header #idcostcontrol').val('');
        $('#form-header #kodecostcontrol').val('');
    }

    function unsetFormInternal() {
        if (!$('#form-internal #idcashbankbayar').val() == '') {
            $('#form-header #idcashbankbayar #idcashbankbayar-empty').attr('selected', 'selected');
            $('#select2-idcashbankbayar-container').text($('#form-header #idcashbankbayar #idcashbankbayar-empty').text());
        }
        if (!$('#form-internal #idakunpersediaan').val() == '') {
            $('#form-header #idakunpersediaan #idakunpersediaan-empty').attr('selected', 'selected');
            $('#select2-idakunpersediaan-container').text($('#form-header #idakunpersediaan #idakunpersediaan-empty').text());
        }
        if (!$('#form-internal #idakunpurchase').val() == '') {
            $('#form-header #idakunpurchase #idakunpurchase-empty').attr('selected', 'selected');
            $('#select2-idakunpurchase-container').text($('#form-header #idakunpurchase #idakunpurchase-empty').text());
        }
    }

    function unsetFormModalTanggal() {
        $('#form-modal-tanggal #tanggal-modal').val('<?= $defaulttanggal ?>');
        $('#form-modal-tanggal #tanggaltax-modal').val('');
        $('#form-modal-tanggal #tanggaldue-modal').val('<?= $defaulttanggal ?>');
    }

    function unsetFormModalSupplier() {
        $('#form-modal-supplier #idcardsupplier-modal').val('');
        if (!$('#form-modal-supplier #idakunsupplier').val() == '') {
            $('#form-modal-supplier #idakunsupplier').val('').trigger('change');
        }
        $('#form-modal-supplier #suppliernama-modal').val('');
        $('#form-modal-supplier #supplieraddress1-modal').val('');
        $('#form-modal-supplier #supplieraddress2-modal').val('');
        $('#form-modal-supplier #supplieraddressrt-modal').val('');
        $('#form-modal-supplier #supplieraddressrw-modal').val('');
        $('#form-modal-supplier #supplierlokasikel-modal').val('');
        $('#form-modal-supplier #supplierlokasikec-modal').val('');
        $('#form-modal-supplier #supplierlokasikab-modal').val('');
        $('#form-modal-supplier #supplierlokasiprop-modal').val('');
        $('#form-modal-supplier #supplierlokasineg-modal').val('');
    }
    
    function unsetFormFooter() {
        $('#form-footer #keterangan').val('');
        $('#form-footer #subtotal').val(0);
        $('#form-footer #total').val(0);
    }

    function view(id) {
        $.when(
            $.ajax({
                url: '/find_tmd',
                type: "POST",
                dataType: 'json',
                header:{
                    'X-CSRF-TOKEN':$('#token').val()
                },
                data: {
                    "id" : id,
                    "namatable" : $("#namatable").val(),
                    "_token": $('#token').val(),
                    maincms: JSON.stringify(maincms)
                },
                success: function (response) {
                    return response;
                }
            })
        ).then(function(responsefind) {
            statusadd = false;
            statusdetail = false;
            nownoid = id;
            $.each(maincms['widget'], function(k, v) {
                getNextNoidTab(v.maintable, v.noid)
            })
            resetForm();
            resetData();
            getCmsTab(statusadd);
            getForm(statusadd, maincms['widget'][0]['noid'], 'divform', responsefind);
            // alert(response)
            // getCmsTabDatatable();


            editid = id;

            // var x = document.getElementById("from-departement");
            // var x2 = document.getElementById("tabledepartemen");
            // var y = document.getElementById("tool-kedua");
            // var y2 = document.getElementById("tool-pertama");
            // var space_filter_button = $('#space_filter_button');

            // if (x.style.display === "none") {
            //     x.style.display = "block";
            //     x2.style.display = "none";
            //     y.style.display = "block";
            //     y2.style.display = "none";
            //     space_filter_button.css('display', 'none');
            // } else {
            //     x.style.display = "none";
            //     x2.style.display = "block";
            //     space_filter_button.css('display', 'block');
            // }

            $('#from-departement').show();
            $('#tabledepartemen').hide();
            $('#tool-kedua').show();
            $('#tool-pertama').hide();
            $('#space_filter_button').hide();
            $('#tool_refresh').hide();
            $('#tool_filter').hide()
            $('#tool_refresh_up').hide();

            // $.ajax({
            //     url: '/find_tmd',
            //     type: "POST",
            //     dataType: 'json',
            //     header:{
            //         'X-CSRF-TOKEN':$('#token').val()
            //     },
            //     data: {
            //         "id" : id,
            //         "namatable" : $("#namatable").val(),
            //         "_token": $('#token').val(),
            //         maincms: JSON.stringify(maincms)
            //     },
            //     success: function (response) {
                    nownoid = responsefind.nownoid;

                    var maincms_panel = new Array();
                    $.each(maincms['panel'], function(k, v) {
                        $.each(maincms['widget'], function(k2, v2) {
                            if (v.idwidget == v2.noid) {
                                maincms_panel[v.noid] = v2;
                            }
                        })
                    })

                    // console.log(response.result_detail)
                    var key = 0;
                    $.each(responsefind.result_detail, function(k, v) {
                        if (key == 0) {
                            nownoidtab = maincms_panel[k].noid
                        }
                        // alert(k)
                        // console.log(v)
                        // localStorage.removeItem('input-data-'+k)
                        localStorage.setItem('input-data-'+k, JSON.stringify(v))
                        localStorage.setItem('tablename-'+k, maincms_panel[k].maintable)
                        // console.log('aku cek localstoragenya')
                        // console.log(JSON.parse(localStorage.getItem('input-data-'+k)))
                        // console.log('input-data-'+k)
                        key++;
                    })
                    
                    // destroyTabDatatable('fincashbankdetail')
                    getCmsTabDatatable()
                    // alert(response['noid'])
                    // $('#noid').val(id)
                    // $.each(response, function(k, v) {
                    //     $("#"+v.field).val(v.value);
                    //     if (v.disabled) {
                    //         $("#"+v.field).attr('disabled', 'disabled');
                    //     }
                    //     if (v.edithidden) {
                    //         $("#div-"+v.field).css('display', 'none');
                    //     }
                    // });
                    // console.log(response)
                // }
            // });
        });
    }

    function viewTab(another_table, another_id, id, panel_noid) {
        resetDataTab(panel_noid)
        statusaddtab[panel_noid] = false;
        
        var maincms_panel = new Array();
        $.each(maincms['panel'], function(k, v) {
            $.each(maincms['widget'], function(k2, v2) {
                if (v.idwidget == v2.noid) {
                    maincms_panel[v.noid] = v2;
                }
            })
        })
        
        $.each(JSON.parse(localStorage.getItem('input-data-'+panel_noid)), function(k, v) {
            if (v.noid == another_id) {
                nownoidtab = v['noid'];
                $.each(fieldnametab[panel_noid], function(k2, v2) {
                    $("#tab-form-div-"+panel_noid+" div div #"+v2).attr('disabled', true);
                    $("#tab-form-div-"+panel_noid+" div div #"+v2).val(v[v2]);
                })
            }
        })

        $('#tab-form-'+panel_noid).show()
        $('#add-data-'+panel_noid).hide()
        $('#save-data-'+panel_noid).show()
        $('#cancel-data-'+panel_noid).show()
        $('#tab-table-'+panel_noid+'_wrapper').hide()
        $('#tab-appendInfoDatatable-'+panel_noid).hide()
        $('#ts-'+panel_noid).hide()
        $('#tool_refresh-'+panel_noid).hide();
        $('#tool_filter-'+panel_noid).hide()
        $('#tool_refresh_up-'+panel_noid).hide();
    }

    function editTab(another_table, another_id, id, panel_noid) {
        resetDataTab(panel_noid)
        statusaddtab[panel_noid] = false;
        // console.log(statusaddtab)
        // alert(another_table+' '+another_id+' '+id)
        var maincms_panel = new Array();
        $.each(maincms['panel'], function(k, v) {
            $.each(maincms['widget'], function(k2, v2) {
                if (v.idwidget == v2.noid) {
                    maincms_panel[v.noid] = v2;
                }
            })
        })
        // console.log(maincms_panel)
        // console.log(panel_noid)
        // console.log(maincms_panel[panel_noid].noid)

        // console.log(JSON.parse(localStorage.getItem('input-data-'+panel_noid)))
        // console.log(fieldnametab)
        $.each(JSON.parse(localStorage.getItem('input-data-'+panel_noid)), function(k, v) {
            if (v.noid == another_id) {
                nownoidtab = v['noid'];
                $.each(fieldnametab[panel_noid], function(k2, v2) {
                    var fieldsplit = v2.split('_lookup');

                    if (fieldsplit.length == 2) {
                        $("#tab-form-div-"+panel_noid+" div div div #"+localStorage.getItem('tablename-'+panel_noid)+'-'+fieldsplit[0]).val('disabled', false);
                        $("#tab-form-div-"+panel_noid+" div div div #"+localStorage.getItem('tablename-'+panel_noid)+'-'+fieldsplit[0]).val(v[fieldsplit[0]]).trigger('change');
                    } else {
                        $("#tab-form-div-"+panel_noid+" div div #"+localStorage.getItem('tablename-'+panel_noid)+'-'+v2).attr('disabled', false);
                        $("#tab-form-div-"+panel_noid+" div div #"+localStorage.getItem('tablename-'+panel_noid)+'-'+v2).val(v[v2]);
                    }

                    // console.log("#"+v2+'     '+v[v2])
                    // if (v.disabled) {
                    //     $("#"+v.field).attr('disabled', 'disabled');
                    // }
                    // if (v.edithidden) {
                    //     $("#div-"+v.field).css('display', 'none');
                    // }
                })
            }
        })
        // getForm(statusadd, maincms_panel[panel_noid].noid, 'tab-form-div-'+panel_noid);

        $('#tab-form-'+panel_noid).show()
        $('#add-data-'+panel_noid).hide()
        $('#save-data-'+panel_noid).show()
        $('#cancel-data-'+panel_noid).show()
        $('#tab-table-'+panel_noid+'_wrapper').hide()
        $('#tab-appendInfoDatatable-'+panel_noid).hide()
        $('#ts-'+panel_noid).hide()
        $('#space_filter_button-'+panel_noid).hide();
        $('#tool_refresh-'+panel_noid).hide();
        $('#tool_filter-'+panel_noid).hide()
        $('#tool_refresh_up-'+panel_noid).hide();
        console.log('masok jos')
    }

    function edit(id) {
        $.when(
            $.ajax({
                url: '/find_tmd',
                type: "POST",
                dataType: 'json',
                header:{
                    'X-CSRF-TOKEN':$('#token').val()
                },
                data: {
                    "id" : id,
                    "namatable" : $("#namatable").val(),
                    "_token": $('#token').val(),
                    maincms: JSON.stringify(maincms)
                },
                success: function (response) {
                    return response;
                }
            })
        ).then(function(responsefind) {
            statusadd = false;
            statusdetail = false;
            nownoid = id;
            $.each(maincms['widget'], function(k, v) {
                getNextNoidTab(v.maintable, v.noid)
            })
            resetForm();
            resetData();
            getCmsTab(statusadd);
            getForm(statusadd, maincms['widget'][0]['noid'], 'divform', responsefind);
            // alert(response)
            // getCmsTabDatatable();


            editid = id;

            // var x = document.getElementById("from-departement");
            // var x2 = document.getElementById("tabledepartemen");
            // var y = document.getElementById("tool-kedua");
            // var y2 = document.getElementById("tool-pertama");
            // var space_filter_button = $('#space_filter_button');

            // if (x.style.display === "none") {
            //     x.style.display = "block";
            //     x2.style.display = "none";
            //     y.style.display = "block";
            //     y2.style.display = "none";
            //     space_filter_button.css('display', 'none');
            // } else {
            //     x.style.display = "none";
            //     x2.style.display = "block";
            //     space_filter_button.css('display', 'block');
            // }

            $('#from-departement').show();
            $('#tabledepartemen').hide();
            $('#tool-kedua').show();
            $('#tool-pertama').hide();
            $('#space_filter_button').hide();
            $('#tool_refresh').hide();
            $('#tool_filter').hide()
            $('#tool_refresh_up').hide();

            // $.ajax({
            //     url: '/find_tmd',
            //     type: "POST",
            //     dataType: 'json',
            //     header:{
            //         'X-CSRF-TOKEN':$('#token').val()
            //     },
            //     data: {
            //         "id" : id,
            //         "namatable" : $("#namatable").val(),
            //         "_token": $('#token').val(),
            //         maincms: JSON.stringify(maincms)
            //     },
            //     success: function (response) {
                    nownoid = responsefind.nownoid;

                    var maincms_panel = new Array();
                    $.each(maincms['panel'], function(k, v) {
                        $.each(maincms['widget'], function(k2, v2) {
                            if (v.idwidget == v2.noid) {
                                maincms_panel[v.noid] = v2;
                            }
                        })
                    })

                    // console.log(response.result_detail)
                    var key = 0;
                    $.each(responsefind.result_detail, function(k, v) {
                        if (key == 0) {
                            nownoidtab = maincms_panel[k].noid
                        }
                        // alert(k)
                        // console.log(v)
                        // localStorage.removeItem('input-data-'+k)
                        localStorage.setItem('input-data-'+k, JSON.stringify(v))
                        localStorage.setItem('tablename-'+k, maincms_panel[k].maintable)
                        select_all_tab[k] = new Array();
                        // console.log('aku cek localstoragenya')
                        // console.log(JSON.parse(localStorage.getItem('input-data-'+k)))
                        // console.log('input-data-'+k)
                        key++;
                    })
                    
                    // destroyTabDatatable('fincashbankdetail')
                    getCmsTabDatatable()
                    // alert(response['noid'])
                    // $('#noid').val(id)
                    // $.each(response, function(k, v) {
                    //     $("#"+v.field).val(v.value);
                    //     if (v.disabled) {
                    //         $("#"+v.field).attr('disabled', 'disabled');
                    //     }
                    //     if (v.edithidden) {
                    //         $("#div-"+v.field).css('display', 'none');
                    //     }
                    // });
                    // console.log(response)
                // }
            // });
        });
    }

    function removeTab(another_table, another_id, id, panel_noid) {
        if (confirm('Are you sure you want to delete this?')) {
            nownoidtab = another_id;
            current_idd = localStorage.getItem('input-data-'+panel_noid);

            var replace_current_idd = new Array();
            var key = 0;
            $.each(JSON.parse(current_idd), function(k, v) {
                if (v.noid != nownoidtab) {
                    replace_current_idd[key] = v;
                    key++;
                }
            })

            refreshSumTab(id, panel_noid)

            localStorage.removeItem('input-data-'+panel_noid);
            localStorage.setItem('input-data-'+panel_noid, JSON.stringify(replace_current_idd))

            $.notify({
                message: 'Delete data has been succesfullly.' 
            },{
                type: 'success'
            });
            refreshTab();
        }

        console.log(localStorage.getItem('input-data-'+panel_noid))
        return false





        // console.log(id);return false;
        if (confirm('Are you sure you want to delete this?')) {
            $.ajax({
                url: '/delete_tab_tmd',
                type: "POST",
                header:{
                    'X-CSRF-TOKEN':$('#token').val()
                },
                data: {
                    "id" : id,
                    "another_id" : another_id,
                    "another_table" : another_table,
                    "_token": $('#token').val(),
                },
                success: function () {
                    // $("#notiftext").text('Data  Berhasil di hapus');
                    $.notify({
                        message: 'Delete data has been succesfullly.' 
                    },{
                        type: 'success'
                    });
                    refreshTab();
                    // $('#divsucces').show(0).delay(5000).hide(0);
                    // var x = document.getElementById("from-departement");
                    // var x2 = document.getElementById("tabledepartemen");
                    // var y = document.getElementById("tool-kedua");
                    // var y2 = document.getElementById("tool-pertama");
                    // x.style.display = "none";
                    // x2.style.display = "block";
                    // y.style.display = "none";
                    // y2.style.display = "block";
                }
            });
        }
    }

    function remove(id) {
        // console.log(id);return false;
        if (confirm('Are you sure you want to delete this?')) {
            $.ajax({
                url: '/delete_tmd',
                type: "POST",
                header:{
                    'X-CSRF-TOKEN':$('#token').val()
                },
                data: {
                    "id" : id,
                    "table" : maincms['widget'][0]['maintable'],
                    "_token": $('#token').val(),
                    maincms: JSON.stringify(maincms),
                },
                success: function () {
                    // $("#notiftext").text('Data  Berhasil di hapus');
                    $.notify({
                        message: 'Delete data has been succesfullly.' 
                    },{
                        type: 'success'
                    });
                    refresh();
                    // $('#divsucces').show(0).delay(5000).hide(0);
                    var x = document.getElementById("from-departement");
                    var x2 = document.getElementById("tabledepartemen");
                    var y = document.getElementById("tool-kedua");
                    var y2 = document.getElementById("tool-pertama");
                    x.style.display = "none";
                    x2.style.display = "block";
                    y.style.display = "none";
                    y2.style.display = "block";
                }
            });
        }
    }
    
    function print(noid, urlprint) {
        window.open('http://ux.lambada.id/'+noid+'/'+urlprint, '_blank');
        return false;

        $.ajax({
                url: panelnoid+'/print/'+noid,
                type: "GET",
                header:{
                    'X-CSRF-TOKEN':$('#token').val()
                },
                success: function (response) {
                    return response
                }
        })
    }

    (function ($) {
    // function infoDatatable() {
        // console.log('tes gaes')
        
        function calcDisableClasses(oSettings) {
            var start = oSettings._iDisplayStart;
            var length = oSettings._iDisplayLength;
            var visibleRecords = oSettings.fnRecordsDisplay();
            var all = length === -1;

            // Gordey Doronin: Re-used this code from main jQuery.dataTables source code. To be consistent.
            var page = all ? 0 : Math.ceil(start / length);
            var pages = all ? 1 : Math.ceil(visibleRecords / length);

            var disableFirstPrevClass = (page > 0 ? '' : oSettings.oClasses.sPageButtonDisabled);
            var disableNextLastClass = (page < pages - 1 ? '' : oSettings.oClasses.sPageButtonDisabled);

            return {
                'first': disableFirstPrevClass,
                'previous': disableFirstPrevClass,
                'next': disableNextLastClass,
                'last': disableNextLastClass
            };
        }

        function calcCurrentPage(oSettings) {
            return Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength) + 1;
        }

        function calcPages(oSettings) {
            return Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength);
        }

        var firstClassName = 'first';
        var previousClassName = 'previous';
        var nextClassName = 'next';
        var lastClassName = 'last';

        var paginateClassName = 'paginate';
        var paginatePageClassName = 'paginate_page';
        var paginateInputClassName = 'paginate_input';
        var paginateTotalClassName = 'paginate_total';

        $.fn.dataTableExt.oPagination.input = {
            'fnInit': function (oSettings, nPaging, fnCallbackDraw) {
                var nFirst = document.createElement('span');
                var nPrevious = document.createElement('span');
                var nNext = document.createElement('span');
                var nLast = document.createElement('span');
                var nInput = document.createElement('input');
                var nTotal = document.createElement('span');
                var nInfo = document.createElement('span');

                var language = oSettings.oLanguage.oPaginate;
                var classes = oSettings.oClasses;
                var info = language.info || `&nbsp;&nbsp;
                                                <label style="padding-top: 8px">Page</label>&nbsp;
                                                _INPUT_&nbsp;
                                                <label style="padding-top: 8px">of&nbsp;_TOTAL_</label>&nbsp;`;

                nFirst.innerHTML = language.sFirst;
                nPrevious.innerHTML = language.sPrevious;
                nNext.innerHTML = language.sNext;
                nLast.innerHTML = language.sLast;

                nFirst.className = firstClassName + ' ' + classes.sPageButton;
                nPrevious.className = previousClassName + ' ' + classes.sPageButton;
                nNext.className = nextClassName + ' ' + classes.sPageButton;
                nLast.className = lastClassName + ' ' + classes.sPageButton;

                nInput.className = paginateInputClassName;
                nTotal.className = paginateTotalClassName;

                if (oSettings.sTableId !== '') {
                    nPaging.setAttribute('id', oSettings.sTableId + '_' + paginateClassName);
                    nFirst.setAttribute('id', oSettings.sTableId + '_' + firstClassName);
                    nPrevious.setAttribute('id', oSettings.sTableId + '_' + previousClassName);
                    nNext.setAttribute('id', oSettings.sTableId + '_' + nextClassName);
                    nLast.setAttribute('id', oSettings.sTableId + '_' + lastClassName);
                }

                nInput.type = 'text';

                info = info.replace(/_INPUT_/g, '</span>' + nInput.outerHTML + '<span>');
                info = info.replace(/_TOTAL_/g, '</span>' + nTotal.outerHTML + '<span>');
                nInfo.innerHTML = '<span>' + info + '</span>';

                nPaging.appendChild(nFirst);
                nPaging.appendChild(nPrevious);
                $(nInfo).children().each(function (i, n) {
                    nPaging.appendChild(n);
                });
                nPaging.appendChild(nNext);
                nPaging.appendChild(nLast);

                $(nFirst).click(function() {
                    var iCurrentPage = calcCurrentPage(oSettings);
                    if (iCurrentPage !== 1) {
                        oSettings.oApi._fnPageChange(oSettings, 'first');
                        fnCallbackDraw(oSettings);
                    }
                });

                $(nPrevious).click(function() {
                    var iCurrentPage = calcCurrentPage(oSettings);
                    if (iCurrentPage !== 1) {
                        oSettings.oApi._fnPageChange(oSettings, 'previous');
                        fnCallbackDraw(oSettings);
                    }
                });

                $(nNext).click(function() {
                    var iCurrentPage = calcCurrentPage(oSettings);
                    if (iCurrentPage !== calcPages(oSettings)) {
                        oSettings.oApi._fnPageChange(oSettings, 'next');
                        fnCallbackDraw(oSettings);
                    }
                });

                $(nLast).click(function() {
                    var iCurrentPage = calcCurrentPage(oSettings);
                    if (iCurrentPage !== calcPages(oSettings)) {
                        oSettings.oApi._fnPageChange(oSettings, 'last');
                        fnCallbackDraw(oSettings);
                    }
                });

                $(nPaging).find('.' + paginateInputClassName).keyup(function (e) {
                    // 38 = up arrow, 39 = right arrow
                    if (e.which === 38 || e.which === 39) {
                        this.value++;
                    }
                    // 37 = left arrow, 40 = down arrow
                    else if ((e.which === 37 || e.which === 40) && this.value > 1) {
                        this.value--;
                    }

                    if (this.value === '' || this.value.match(/[^0-9]/)) {
                        /* Nothing entered or non-numeric character */
                        this.value = this.value.replace(/[^\d]/g, ''); // don't even allow anything but digits
                        return;
                    }

                    var iNewStart = oSettings._iDisplayLength * (this.value - 1);
                    if (iNewStart < 0) {
                        iNewStart = 0;
                    }
                    if (iNewStart >= oSettings.fnRecordsDisplay()) {
                        iNewStart = (Math.ceil((oSettings.fnRecordsDisplay()) / oSettings._iDisplayLength) - 1) * oSettings._iDisplayLength;
                    }

                    oSettings._iDisplayStart = iNewStart;
                    oSettings.oInstance.trigger("page.dt", oSettings);
                    fnCallbackDraw(oSettings);
                });

                // Take the brutal approach to cancelling text selection.
                $('span', nPaging).bind('mousedown', function () { return false; });
                $('span', nPaging).bind('selectstart', function() { return false; });

                // If we can't page anyway, might as well not show it.
                var iPages = calcPages(oSettings);
                if (iPages <= 1) {
                    $(nPaging).hide();
                }
            },

            'fnUpdate': function (oSettings) {
                if (!oSettings.aanFeatures.p) {
                    return;
                }

                var iPages = calcPages(oSettings);
                var iCurrentPage = calcCurrentPage(oSettings);

                var an = oSettings.aanFeatures.p;
                if (iPages <= 1) // hide paging when we can't page
                {
                    $(an).hide();
                    return;
                }

                var disableClasses = calcDisableClasses(oSettings);

                $(an).show();

                // Enable/Disable `first` button.
                $(an).children('.' + firstClassName)
                    .removeClass(oSettings.oClasses.sPageButtonDisabled)
                    .addClass(disableClasses[firstClassName]);

                // Enable/Disable `prev` button.
                $(an).children('.' + previousClassName)
                    .removeClass(oSettings.oClasses.sPageButtonDisabled)
                    .addClass(disableClasses[previousClassName]);

                // Enable/Disable `next` button.
                $(an).children('.' + nextClassName)
                    .removeClass(oSettings.oClasses.sPageButtonDisabled)
                    .addClass(disableClasses[nextClassName]);

                // Enable/Disable `last` button.
                $(an).children('.' + lastClassName)
                    .removeClass(oSettings.oClasses.sPageButtonDisabled)
                    .addClass(disableClasses[lastClassName]);

                // Paginate of N pages text
                $(an).find('.' + paginateTotalClassName).html(iPages);

                // Current page number input value
                $(an).find('.' + paginateInputClassName).val(iCurrentPage);
            }
        };
    // }
    })(jQuery);
</script>
@endsection
