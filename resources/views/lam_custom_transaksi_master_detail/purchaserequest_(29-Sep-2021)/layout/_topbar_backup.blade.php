@php
    if(Session::get('login')) {
        $amtn = DB::connection('mysql2')->table('appmtypenotification')->get();
        $resultan = DB::connection('mysql2')->table('appnotification')->get();

        $an = [];
        foreach ($resultan as $k => $v) {
            if (@$an[$v->idtypenotification] == null) {
                $an[$v->idtypenotification] = [];
            }
            array_push($an[$v->idtypenotification], $v);
        }
    }

    $color = [
        'white' => '#FFFFFF',
        'default' => '#E1E5EC',
        'dark' => '#2F353B',
        'blue' => '#3598DC',
        'blue-madison' => '#578EBE',
        'blue-chambray' => '#2C3E50',
        'blue-ebonyclay' => '#22313F',
        'blue-hoki' => '#67809F',
        'blue-steel' => '#4B77BE',
        'blue-soft' => '#4C87B9',
        'blue-dark' => '#5E738B',
        'blue-sharp' => '#5C9BD1',
        'blue-oleo' => '#94A0B2',
        'green' => '#32C5D2',
        'green-meadow' => '#1BBC9B',
        'green-seagreen' => '#1BA39C',
        'green-torquoise' => '#36D7B7',
        'green-haze' => '#44B6AE',
        'green-jungle' => '#26C281',
        'green-soft' => '#3FABA4',
        'green-dark' => '#4DB3A2',
        'green-sharp' => '#2AB4C0',
        'green-steel' => '#29B4B6',
        'grey' => '#E5E5E5',
        'grey-steel' => '#E9EDEF',
        'grey-cararra' => '#FAFAFA',
        'grey-gallery' => '#555555',
        'grey-cascade' => '#95A5A6',
        'grey-silver' => '#BFBFBF',
        'grey-salsa' => '#ACB5C3',
        'grey-salt' => '#BFCAD1',
        'grey-mint' => '#525E64',
        'red' => '#E7505A',
        'red-pink' => '#E08283',
        'red-sunglo' => '#E26A6A',
        'red-intense' => '#E35B5A',
        'red-thunderbird' => '#D91E18',
        'red-flamingo' => '#EF4836',
        'red-soft' => '#D05454',
        'red-haze' => '#F36A5A',
        'red-mint' => '#E43A45',
        'yellow' => '#C49F47',
        'yellow-gold' => '#E87E04',
        'yellow-casablanca' => '#F2784B',
        'yellow-crusta' => '#F3C200',
        'yellow-lemon' => '#F7CA18',
        'yellow-saffron' => '#F4D03F',
        'yellow-soft' => '#C8D046',
        'yellow-haze' => '#C5BF66',
        'yellow-mint' => '#C5B96B',
        'purple' => '#8E44AD',
        'purple-plum' => '#8775A7',
        'purple-medium' => '#BF55EC',
        'purple-studio' => '#8E44AD',
        'purple-wisteria' => '#9B59B6',
        'purple-seance' => '#9A12B3',
        'purple-intense' => '#8775A7',
        'purple-sharp' => '#796799',
        'purple-soft' => '#8877A9',
    ];
@endphp

<!-- Topbar -->
<div class="topbar">
    @if(Session::get('login'))
        @foreach($amtn as $k => $v)
            @if($v->noid != 0)
                <div class="dropdown" style="margin-right: 5px">
                    <!-- Toggle -->
                    <div class="topbar-item text-hover-primary" data-toggle="dropdown" data-offset="10px,0px">
                        <i class="{{$v->classicon}}" style="margin: 5px;"></i>
                        <span class="badge" style="background-color: {{$color[$v->classcolor]}}; color: white; margin: -15px 0px 0px -5px">{{@count($an[$v->noid])}}</span>
                        <span class="pulse-ring"></span>
                    </div>
                    <!-- Dropdown -->
                    <div class="dropdown-menu mt-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg" style="padding: 0px">
                        <form>
                            <div class="d-flex flex-column pt-2 bgi-size-cover bgi-no-repeat" style="background-color: {{$color[$v->classcolor]}};">
                                {{-- Title --}}
                                <h4 class="d-flex flex-center">
                                    <span class="text-white">{{$v->nama}}</span>
                                </h4>
                            </div>

                            <div class="tab-content">
                                <div class="tab-pane active show p-2" id="topbar_notifications_notifications" role="tabpanel">
                                    <div class="scroll pr-7 mr-n7" data-scroll="true" data-height="300" data-mobile-height="200">
                                        
                                        @if(@$an[$v->noid] != null)
                                            @foreach($an[$v->noid] as $k2 => $v2)
                                            <div class="navi navi-icon-circle navi-spacer-x-0">
                                                <!--begin::Item-->
                                                <a href="{{$v2->urlreff}}" class="navi-item">
                                                    <div class="navi-link rounded p-2">
                                                        <div class="symbol symbol-50 mr-3">
                                                            <div class="symbol-label">
                                                                <i class="{{$v->classicon}} icon-lg" style="color: {{$color[$v->classcolor]}}"></i>
                                                            </div>
                                                        </div>
                                                        <div class="navi-text">
                                                            <div class="font-weight-bold font-size-lg">Notification {{$v2->noid}}</div>
                                                            <!-- <div class="text-muted">Reports based on sales</div> -->
                                                        </div>
                                                    </div>
                                                </a>
                                                <!--end::Item-->
                                            </div>
                                            @endforeach
                                        @endif

                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- @if(@$an[$v->noid] != null)
                            @foreach($an[$v->noid] as $k2 => $v2)
                                
                                <p>{{$v2->noid}}</p>
                            @endforeach
                        @endif -->
                    </div>
                </div>
            @endif
        @endforeach
    @endif

    <!-- User -->
    @if (config('layout.extras.user.display'))
        @if (config('layout.extras.user.layout') == 'offcanvas')
            <div class="topbar-item">
                <div class="btn btn-icon w-auto btn-clean d-flex align-items-center btn-lg px-2" id="kt_quick_user_toggle">
                    @if (@$noid == 'null')
                        <!-- <a href="#" class="btn btn-success font-weight-bold mr-1">
                                <i class="la la-sign-in">Login</i>
                            </a> -->
                        <span class="text-muted font-weight-bold font-size-base d-none d-md-inline mr-1"></span>
                        <span class="text-dark-50 font-weight-bolder font-size-base d-none d-md-inline mr-3">Login</span>
                        <span class="symbol symbol-35 symbol-light-success">
                            <span class="symbol-label font-size-h5 font-weight-bold"><i class="la la-sign-in"></i></span>
                        </span>
                    @else
                        <span class="text-muted font-weight-bold font-size-base d-none d-md-inline mr-1">Hi,</span>
                        <span class="text-dark-50 font-weight-bolder font-size-base d-none d-md-inline mr-3">{{ $sessionname }}</span>
                        <span class="symbol symbol-35 symbol-light-success">
                            <span class="symbol-label font-size-h5 font-weight-bold"><i class="icon-user"></i></span>
                        </span>
                    @endif
                </div>
            </div>
        @else
            <div class="dropdown">
                <!-- Toggle -->
                <div class="topbar-item" data-toggle="dropdown" data-offset="0px,0px">
                    @if ($noid == 'null')
                        <div class="btn btn-icon w-auto btn-clean d-flex align-items-center btn-lg px-2">
                            <span class="text-muted font-weight-bold font-size-base d-none d-md-inline mr-1">Hioooo,</span>
                            <span class="text-dark-50 font-weight-bolder font-size-base d-none d-md-inline mr-3">Seankkk</span>
                            <span class="symbol symbol-35 symbol-light-success">
                                <span class="symbol-label font-size-h5 font-weight-bold">S</span>
                            </span>
                        </div>
                    @else
                        <div class="btn btn-icon w-auto btn-clean d-flex align-items-center btn-lg px-2">
                            <span class="text-muted font-weight-bold font-size-base d-none d-md-inline mr-1">Hi,</span>
                            <span class="text-dark-50 font-weight-bolder font-size-base d-none d-md-inline mr-3">Seen</span>
                            <span class="symbol symbol-35 symbol-light-success">
                                <span class="symbol-label font-size-h5 font-weight-bold">S</span>
                            </span>
                        </div>
                    @endif
                </div>

                <!-- Dropdown -->
                <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg p-0">
                    @include('layout.partials.extras.dropdown._user')
                </div>
            </div>
        @endif
    @endif
</div>