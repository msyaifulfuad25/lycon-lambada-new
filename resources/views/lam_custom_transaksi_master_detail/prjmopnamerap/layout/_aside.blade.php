<!-- Aside -->
@php
    $kt_logo_image = 'logo-light.png';
    $idhomepagelink = $idtypepage;
    $idhomepage = $idtypepage;

    $day = [
        'Minggu',
        'Senin',
        'Selasa',
        'Rabu',
        'Kamis',
        'Jum\'at',
        'Sabtu',
    ];
@endphp
@if (config('layout.brand.self.theme') === 'light')
    @php $kt_logo_image = 'logo-dark.png' @endphp
@elseif (config('layout.brand.self.theme') === 'dark')
    @php $kt_logo_image = 'lambada.png' @endphp
@endif

<div class="aside aside-left {{ Metronic::printClasses('aside', false) }} d-flex flex-column flex-row-auto" id="kt_aside">
    <!-- Brand -->
    <div class="brand_ flex-column-auto {{ Metronic::printClasses('brand', false) }}" id="kt_brand" style="height: 46px: width: 265px !important">
    </div>
    
    <div style="font-family: 'Orbitron', sans-serif;
        /* color: #66ff99; */
        color: white;
        padding-top: 50px;
        text-align: center;
        background-color: #424266;">
        <span>
            {{$day[date('w')].', '.date('d-M-Y')}}
        </span>
    </div>
    <div id="clock" style="font-family: 'Orbitron', sans-serif;
        /* color: #66ff99; */
        color: white;
        font-size: 30px;
        font-weight: bold;
        text-align: center;
        background-color: #424266;"></div>

    <!-- Aside menu -->
    <div class="aside-menu-wrapper flex-column-fluid" id="kt_aside_menu_wrapper">
        @if (config('layout.aside.self.display') === false)
            <div class="header-logo">
                <a href="{{ url('/') }}">
                    <img alt="{{ config('app.name') }}" src="{{ asset('media/logos/'.$kt_logo_image) }}"/>
                </a>
            </div>
        @endif
        <div id="kt_aside_menu" class="aside-menu {{ Metronic::printClasses('aside_menu', false) }}" data-menu-vertical="1" {{ Metronic::printAttrs('aside_menu') }}>
            <ul class="menu-nav {{ Metronic::printClasses('aside_menu_nav', false) }}" style="padding-top: 0px">
                @php
                    if(!Session::get('login')){
                        $data_sidebar = App\Menu::getSidebarLoginBaru($idhomepagelink);
                    }else{
                        $data_sidebar = App\Menu::getSidebarLogin($idhomepagelink);
                    }
                @endphp
    
                @foreach($data_sidebar as $key => $side)
                    @php if($side->groupmenu == ''){$submenunya = 'Dashboard';}else{$submenunya = $side->groupmenu;} @endphp
                        @if ($side->menulevel ==3)
                            <li class="menu-section " style="margin-top: 0px">
                                <h4 class="menu-text">{{ $submenunya }}</h4>
                                <i class="menu-icon ki ki-bold-more-hor icon-md"></i>
                            </li>
                        @if(!Session::get('login'))
                            @php
                                $subb4 =  \App\Menu::where('menulevel',3)->where('isactive',1)->where('ispublic',1)->where('groupmenu',$side->groupmenu)->orderBy('nourut')->get();
                            @endphp
                        @else
                        @php
                            $subb4 =  \App\Menu::where('menulevel',3)->where('isactive',1)->where('groupmenu',$side->groupmenu)->orderBy('nourut')->get();
                        @endphp
                    @endif

                    @foreach($subb4 as $value4)
                        @php
                            $dataurl =Request::path();
                            if($dataurl == '/'){
                                $urlbaru  = $idhomepage;
                            }else{
                                $urlbaru = Request::path();
                            }
                            
                            if(!Session::get('login')){
                                $carurl = \App\Menu::where('linkidpage',$urlbaru)->where('ispublic',1)->orderBy('nourut')->first();
                            }else{
                                $carurl = \App\Menu::where('linkidpage',$urlbaru)->orderBy('nourut')->first();
                            }

                            if(isset($carurl)){
                                $carurl2 = \App\Menu::where('noid',$carurl->idparent)->first();
                                if($value4->noid == $carurl->linkidpage ||  str_contains($carurl->linkidpage, $value4->noid)  ){
                        @endphp
                                    <li id="10" class="menu-item menu-item-submenu menu-item-open" aria-haspopup="true" data-menu-toggle="hover">
                        @php
                                }else{
                        @endphp
                                    <li id="11" class="menu-item  menu-item-submenu " aria-haspopup="true" data-menu-toggle="hover">
                        @php
                                }
                            }else{
                        @endphp
                                    <li id="11" class="menu-item  menu-item-submenu " aria-haspopup="true" data-menu-toggle="hover">
                        @php
                            }
                        @endphp

                        <a href="javascript:;" class="menu-link menu-toggle">
                            <span class="menu-text"><i class="fa fa-file"></i>&nbsp;&nbsp;{{ $value4->menucaption }}</span>
                            <i class="menu-arrow"></i>
                        </a>

                        @php
                            if($value4->groupmenu == ''){
                                if(!Session::get('login')  ){
                                    $men =  \App\Menu::where('menulevel', 4)->where('isactive',1)->where('ispublic',1)->where('idparent',$value4->noid)->orderBy('nourut')->get();
                                }else{
                                    $men =  \App\Menu::where('menulevel', 4)->where('isactive',1)->where('idparent',$value4->noid)->orderBy('nourut')->get();
                                }
                        @endphp
                        
                        @foreach($men as $value)
                            <div class="menu-submenu ">
                                <i class="menu-arrow"></i>
                                <ul class="menu-subnav">
                                @if(Request::path() == $value->linkidpage )
                                    <li class="menu-item menu-item-active" aria-haspopup="true" title="{{ $value->menuhint }}">
                                        <a href="{{ url('/'.$value->noid) }}" class="menu-link ">
                                @else
                                    <li class="menu-item " aria-haspopup="true" title="{{ $value->menuhint }}">
                                        <a href="{{ url('/'.$value->noid) }}" class="menu-link ">
                                @endif
                                <i class="menu-bullet menu-bullet-line">
                                    <span></span>
                                </i>
                                    <span class="menu-text">{{ $value->menucaption}}</span>
                                    </a>
                                    </li>
                                </ul>
                            </div>
                        @endforeach

                        @php
                            }else{
                                if(!Session::get('login') ||  str_contains($value4->noid, $idhomepage) ){
                                    $men =  \App\Menu::where('menulevel', 4)->where('isactive',1)->where('ispublic',1)->where('idparent',$value4->noid)->orderBy('nourut')->get();
                                }else{
                                    $men =  \App\Menu::where('menulevel', 4)->where('isactive',1)->where('idparent',$value4->noid)->orderBy('nourut')->get();
                                }
                        @endphp
                                @foreach($men as $value)
                                    <div class="menu-submenu ">
                                        <i class="menu-arrow"></i>
                                        <ul class="menu-subnav">
                                        @if(Request::path() == $value->linkidpage )
                                            <li class="menu-item menu-item-active" aria-haspopup="true" title="{{ $value->menuhint }}">
                                            <a href="{{ url('/'.$value->noid) }}" class="menu-link">
                                        @else
                                            <li class="menu-item " aria-haspopup="true" title="{{ $value->menuhint }}">
                                            <a href="{{ url('/'.$value->noid) }}" class="menu-link ">
                                        @endif
                                            <i class="menu-bullet menu-bullet-line">
                                                <span></span>
                                            </i>
                                                <span class="menu-text">{{ $value->menucaption}}</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                @endforeach
                        @php
                            }
                        @endphp
                        </li>
                    @endforeach
                    @endif
                @endforeach
            </ul>
        </div>
    </div>
</div>
