{{-- Header --}}
<?php
    $color = [
        'white' => [
            'primary' => '#FFFFFF',
            'btn' => '#2196F3',
            'info' => '#90CAF9',
            'thead' => '#BBDEFB',
            'td' => '#E3F2FD'
        ],
        'default' => [
            'primary' => '#E1E5EC',
            'btn' => '#2196F3',
            'info' => '#90CAF9',
            'thead' => '#BBDEFB',
            'td' => '#E3F2FD'
        ],
        'dark' => [
            'primary' => '#2F353B',
            'btn' => '#2196F3',
            'info' => '#90CAF9',
            'thead' => '#BBDEFB',
            'td' => '#E3F2FD'
        ],
        'blue' => [
            'primary' => '#3598DC',
            'btn' => '#2196F3',
            'info' => '#90CAF9',
            'thead' => '#BBDEFB',
            'td' => '#E3F2FD'
        ],
        'blue-madison' => [
            'primary' => '#578EBE',
            'btn' => '#2196F3',
            'info' => '#90CAF9',
            'thead' => '#BBDEFB',
            'td' => '#E3F2FD'
        ],
        'blue-chambray' => [
            'primary' => '#2C3E50',
            'btn' => '#2196F3',
            'info' => '#90CAF9',
            'thead' => '#BBDEFB',
            'td' => '#E3F2FD'
        ],
        'blue-ebonyclay' => [
            'primary' => '#22313F',
            'btn' => '#2196F3',
            'info' => '#90CAF9',
            'thead' => '#BBDEFB',
            'td' => '#E3F2FD'
        ],
        'blue-hoki' => [
            'primary' => '#67809F',
            'btn' => '#2196F3',
            'info' => '#90CAF9',
            'thead' => '#BBDEFB',
            'td' => '#E3F2FD'
        ],
        'blue-steel' => [
            'primary' => '#4B77BE',
            'btn' => '#2196F3',
            'info' => '#90CAF9',
            'thead' => '#BBDEFB',
            'td' => '#E3F2FD'
        ],
        'blue-soft' => [
            'primary' => '#4C87B9',
            'btn' => '#2196F3',
            'info' => '#90CAF9',
            'thead' => '#BBDEFB',
            'td' => '#E3F2FD'
        ],
        'blue-dark' => [
            'primary' => '#5E738B',
            'btn' => '#2196F3',
            'info' => '#90CAF9',
            'thead' => '#BBDEFB',
            'td' => '#E3F2FD'
        ],
        'blue-sharp' => [
            'primary' => '#5C9BD1',
            'btn' => '#2196F3',
            'info' => '#90CAF9',
            'thead' => '#BBDEFB',
            'td' => '#E3F2FD'
        ],
        'blue-oleo' => [
            'primary' => '#94A0B2',
            'btn' => '#2196F3',
            'info' => '#90CAF9',
            'thead' => '#BBDEFB',
            'td' => '#E3F2FD'
        ],
        'green' => [
            'primary' => '#26a69a',
            'btn' => '#4CAF50',
            'info' => '#A5D6A7',
            'thead' => '#C8E6C9',
            'td' => '#E8F5E9'
        ],
        'green-meadow' => [
            'primary' => '#1BBC9B',
            'btn' => '#4CAF50',
            'info' => '#A5D6A7',
            'thead' => '#C8E6C9',
            'td' => '#E8F5E9'
        ],
        'green-seagreen' => [
            'primary' => '#1BA39C',
            'btn' => '#4CAF50',
            'info' => '#A5D6A7',
            'thead' => '#C8E6C9',
            'td' => '#E8F5E9'
        ],
        'green-torquoise' => [
            'primary' => '#36D7B7',
            'btn' => '#4CAF50',
            'info' => '#A5D6A7',
            'thead' => '#C8E6C9',
            'td' => '#E8F5E9'
        ],
        'green-haze' => [
            'primary' => '#44B6AE',
            'btn' => '#4CAF50',
            'info' => '#A5D6A7',
            'thead' => '#C8E6C9',
            'td' => '#E8F5E9'
        ],
        'green-jungle' => [
            'primary' => '#26C281',
            'btn' => '#4CAF50',
            'info' => '#A5D6A7',
            'thead' => '#C8E6C9',
            'td' => '#E8F5E9'
        ],
        'green-soft' => [
            'primary' => '#3FABA4',
            'btn' => '#4CAF50',
            'info' => '#A5D6A7',
            'thead' => '#C8E6C9',
            'td' => '#E8F5E9'
        ],
        'green-dark' => [
            'primary' => '#4DB3A2',
            'btn' => '#4CAF50',
            'info' => '#A5D6A7',
            'thead' => '#C8E6C9',
            'td' => '#E8F5E9'
        ],
        'green-sharp' => [
            'primary' => '#2AB4C0',
            'btn' => '#4CAF50',
            'info' => '#A5D6A7',
            'thead' => '#C8E6C9',
            'td' => '#E8F5E9'
        ],
        'green-steel' => [
            'primary' => '#29B4B6',
            'btn' => '#4CAF50',
            'info' => '#A5D6A7',
            'thead' => '#C8E6C9',
            'td' => '#E8F5E9'
        ],
        'grey' => [
            'primary' => '#E5E5E5',
            'btn' => '#607D8B',
            'info' => '#B0BEC5',
            'thead' => '#CFD8DC',
            'td' => '#ECEFF1'
        ],
        'grey-steel' => [
            'primary' => '#E9EDEF',
            'btn' => '#607D8B',
            'info' => '#B0BEC5',
            'thead' => '#CFD8DC',
            'td' => '#ECEFF1'
        ],
        'grey-cararra' => [
            'primary' => '#FAFAFA',
            'btn' => '#607D8B',
            'info' => '#B0BEC5',
            'thead' => '#CFD8DC',
            'td' => '#ECEFF1'
        ],
        'grey-gallery' => [
            'primary' => '#555555',
            'btn' => '#607D8B',
            'info' => '#B0BEC5',
            'thead' => '#CFD8DC',
            'td' => '#ECEFF1'
        ],
        'grey-cascade' => [
            'primary' => '#95A5A6',
            'btn' => '#607D8B',
            'info' => '#B0BEC5',
            'thead' => '#CFD8DC',
            'td' => '#ECEFF1'
        ],
        'grey-silver' => [
            'primary' => '#BFBFBF',
            'btn' => '#607D8B',
            'info' => '#B0BEC5',
            'thead' => '#CFD8DC',
            'td' => '#ECEFF1'
        ],
        'grey-salsa' => [
            'primary' => '#ACB5C3',
            'btn' => '#607D8B',
            'info' => '#B0BEC5',
            'thead' => '#CFD8DC',
            'td' => '#ECEFF1'
        ],
        'grey-salt' => [
            'primary' => '#BFCAD1',
            'btn' => '#607D8B',
            'info' => '#B0BEC5',
            'thead' => '#CFD8DC',
            'td' => '#ECEFF1'
        ],
        'grey-mint' => [
            'primary' => '#525E64',
            'btn' => '#607D8B',
            'info' => '#B0BEC5',
            'thead' => '#CFD8DC',
            'td' => '#ECEFF1'
        ],
        'red' => [
            'primary' => '#cb5a5e',
            'btn' => '#F44336',
            'info' => '#EF9A9A',
            'thead' => '#FFCDD2',
            'td' => '#FFEBEE'
        ],
        'red-pink' => [
            'primary' => '#E08283',
            'btn' => '#F44336',
            'info' => '#EF9A9A',
            'thead' => '#FFCDD2',
            'td' => '#FFEBEE'
        ],
        'red-sunglo' => [
            'primary' => '#E26A6A',
            'btn' => '#F44336',
            'info' => '#EF9A9A',
            'thead' => '#FFCDD2',
            'td' => '#FFEBEE'
        ],
        'red-intense' => [
            'primary' => '#E35B5A',
            'btn' => '#F44336',
            'info' => '#EF9A9A',
            'thead' => '#FFCDD2',
            'td' => '#FFEBEE'
        ],
        'red-thunderbird' => [
            'primary' => '#D91E18',
            'btn' => '#F44336',
            'info' => '#EF9A9A',
            'thead' => '#FFCDD2',
            'td' => '#FFEBEE'
        ],
        'red-flamingo' => [
            'primary' => '#EF4836',
            'btn' => '#F44336',
            'info' => '#EF9A9A',
            'thead' => '#FFCDD2',
            'td' => '#FFEBEE'
        ],
        'red-soft' => [
            'primary' => '#D05454',
            'btn' => '#F44336',
            'info' => '#EF9A9A',
            'thead' => '#FFCDD2',
            'td' => '#FFEBEE'
        ],
        'red-haze' => [
            'primary' => '#F36A5A',
            'btn' => '#F44336',
            'info' => '#EF9A9A',
            'thead' => '#FFCDD2',
            'td' => '#FFEBEE'
        ],
        'red-mint' => [
            'primary' => '#E43A45',
            'btn' => '#F44336',
            'info' => '#EF9A9A',
            'thead' => '#FFCDD2',
            'td' => '#FFEBEE'
        ],
        'yellow' => [
            'primary' => '#C49F47',
            'btn' => '#FF9800',
            'info' => '#FFCC80',
            'thead' => '#FFE0B2',
            'td' => '#FFF3E0'
        ],
        'yellow-gold' => [
            'primary' => '#E87E04',
            'btn' => '#FF9800',
            'info' => '#FFCC80',
            'thead' => '#FFE0B2',
            'td' => '#FFF3E0'
        ],
        'yellow-casablanca' => [
            'primary' => '#F2784B',
            'btn' => '#FF9800',
            'info' => '#FFCC80',
            'thead' => '#FFE0B2',
            'td' => '#FFF3E0'
        ],
        'yellow-crusta' => [
            'primary' => '#F3C200',
            'btn' => '#FF9800',
            'info' => '#FFCC80',
            'thead' => '#FFE0B2',
            'td' => '#FFF3E0'
        ],
        'yellow-lemon' => [
            'primary' => '#F7CA18',
            'btn' => '#FF9800',
            'info' => '#FFCC80',
            'thead' => '#FFE0B2',
            'td' => '#FFF3E0'
        ],
        'yellow-saffron' => [
            'primary' => '#F4D03F',
            'btn' => '#FF9800',
            'info' => '#FFCC80',
            'thead' => '#FFE0B2',
            'td' => '#FFF3E0'
        ],
        'yellow-soft' => [
            'primary' => '#C8D046',
            'btn' => '#FF9800',
            'info' => '#FFCC80',
            'thead' => '#FFE0B2',
            'td' => '#FFF3E0'
        ],
        'yellow-haze' => [
            'primary' => '#C5BF66',
            'btn' => '#FF9800',
            'info' => '#FFCC80',
            'thead' => '#FFE0B2',
            'td' => '#FFF3E0'
        ],
        'yellow-mint' => [
            'primary' => '#C5B96B',
            'btn' => '#FF9800',
            'info' => '#FFCC80',
            'thead' => '#FFE0B2',
            'td' => '#FFF3E0'
        ],
        'purple' => [
            'primary' => '#8e5fa2',
            'btn' => '#9C27B0',
            'info' => '#CE93D8',
            'thead' => '#E1BEE7',
            'td' => '#F3E5F5'
        ],
        'purple-plum' => [
            'primary' => '#8775A7',
            'btn' => '#9C27B0',
            'info' => '#CE93D8',
            'thead' => '#E1BEE7',
            'td' => '#F3E5F5'
        ],
        'purple-medium' => [
            'primary' => '#BF55EC',
            'btn' => '#9C27B0',
            'info' => '#CE93D8',
            'thead' => '#E1BEE7',
            'td' => '#F3E5F5'
        ],
        'purple-studio' => [
            'primary' => '#8E44AD',
            'btn' => '#9C27B0',
            'info' => '#CE93D8',
            'thead' => '#E1BEE7',
            'td' => '#F3E5F5'
        ],
        'purple-wisteria' => [
            'primary' => '#9B59B6',
            'btn' => '#9C27B0',
            'info' => '#CE93D8',
            'thead' => '#E1BEE7',
            'td' => '#F3E5F5'
        ],
        'purple-seance' => [
            'primary' => '#9A12B3',
            'btn' => '#9C27B0',
            'info' => '#CE93D8',
            'thead' => '#E1BEE7',
            'td' => '#F3E5F5'
        ],
        'purple-intense' => [
            'primary' => '#8775A7',
            'btn' => '#9C27B0',
            'info' => '#CE93D8',
            'thead' => '#E1BEE7',
            'td' => '#F3E5F5'
        ],
        'purple-sharp' => [
            'primary' => '#796799',
            'btn' => '#9C27B0',
            'info' => '#CE93D8',
            'thead' => '#E1BEE7',
            'td' => '#F3E5F5'
        ],
        'purple-soft' => [
            'primary' => '#8877A9',
            'btn' => '#9C27B0',
            'info' => '#CE93D8',
            'thead' => '#E1BEE7',
            'td' => '#F3E5F5'
        ],
    ];
?>
<div id="kt_header" class="header header-fixed {{ Metronic::printClasses('header', false) }}" {{ Metronic::printAttrs('header') }} style="height: 46px">
    {{-- Container --}}
    <div class="container-fluid d-flex align-items-center justify-content-between" style="padding-left: 0px;">
        

        @if (config('layout.header.self.display'))
            @php
                $kt_logo_image = 'logo-light.png';
            @endphp
            @if (config('layout.header.self.theme') === 'light')
                @php $kt_logo_image = 'logo-dark.png' @endphp
            @elseif (config('layout.header.self.theme') === 'dark')
                @php $kt_logo_image = 'logo-light.png' @endphp
            @endif
            {{-- Header Menu --}}
            <div class="">
            <div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
            <div class="brand" id="kt_brand" style="height: 46px; padding: 10px; width: 265px; background-color: black">
            <div style="height: 100%; width: 200px; left: 0; background: transparent">
                <a href="#">
                    <img alt="{{ config('app.name') }}" src="{{ asset('media/logos/lambada.png') }}" style="height: 30px"/>
                </a>
            </div>
            @if (config('layout.aside.self.minimize.toggle'))
                <button class="brand-toggle btn btn-sm px-0" id="kt_aside_toggle">
                    {{ Metronic::getSVG("media/svg/icons/Navigation/Angle-double-left.svg", "svg-icon-xl") }}
                </button>
            @endif
        </div>
                @if(config('layout.aside.self.display') == false)
                    <div class="header-logo">
                        <a href="{{ url('/') }}">
                            <img alt="Logo" src="{{ asset('media/logos/'.$kt_logo_image) }}"/>
                        </a>
                    </div>
                @endif
                <div id="kt_header_menu" class="header-menu header-menu-mobile {{ Metronic::printClasses('header_menu', false) }}" {{ Metronic::printAttrs('header_menu') }} style="margin-top: 10px">
                    <ul class="menu-nav {{ Metronic::printClasses('header_menu_nav', false) }}">
                        @php
                            if(!Session::get('login')){
                                $data = App\Menu::getAll();
                            }else{
                                $data = App\Menu::getAllAfter();
                            }
                        @endphp

                        @foreach($data as $key => $dat)
                            @if($dat->menulevel == 1)
                                @if($dat->groupmenu == "")
                                    <li>
                                        <a  href="{{ url('/') }}" class="menu-link">
                                            <span class="menu-text">
                                        {{ $dat->menucaption }}
                                            </span>
                                            <i class="menu-arrow"></i>
                                        </a>
                                    </li>
                                @else

                                    @php
                                        ${"'q'.$key"} = json_decode($dat->submenu);
                                        if($dat->linkidpage != 0 ){
                                    @endphp
                                        <li class="menu-item" >
                                            <a  href="{{ url('/'.$dat->linkidpage)}}" class="menu-link">
                                                <span class="menu-text">
                                                {{ $dat->menucaption }}
                                                </span>
                                                <i class="menu-arrow"></i>
                                            </a>
                                    @php
                                        }else{
                                    @endphp
                                        <li class="menu-item menu-item-submenu menu-item-rel" data-menu-toggle="click" aria-haspopup="true" >
                                            <a  href="javascript:;" class="menu-link menu-toggle">
                                                <span class="menu-text">
                                                {{ $dat->menucaption }}
                                                </span>
                                                <i class="menu-arrow"></i>
                                            </a>
                                    @php
                                        }
                                    @endphp

                                    @foreach($dat->submenu as $submenu)
                                        @if($submenu->groupmenu !== '' )
                                            @php
                                                $subb1 =  \App\Menu::where('menulevel',2)->where('isactive',1)->where('idparent',$dat->noid)->groupBy('groupmenu')->orderBy('noid')->get();
                                                $wordCount = count($subb1);
                                                if($wordCount == 2){
                                                    $panjang = '500px';
                                                }else{
                                                    $panjang = '680px';
                                                }
                                            @endphp
                                            <div class="menu-submenu menu-submenu-fixed" style="width:{{$panjang}}" data-hor-direction="menu-submenu-left">
                                                <div class="menu-subnav">
                                                    <ul class="menu-content">
                                                        @foreach($subb1 as $sub)
                                                            <li class="menu-item">
                                                                <h3 class="menu-heading menu-toggle">
                                                                    <i class="menu-bullet menu-bullet-dot">
                                                                    <span></span>
                                                                    </i>
                                                                    <span class="menu-text">{{ $sub->groupmenu}}</span>
                                                                    <i class="menu-arrow"></i>
                                                                </h3>
                                                                <ul class="menu-inner">
                                                                    @php
                                                                        $subb2 =  \App\Menu::where('menulevel',2)->where('isactive',1)->where('groupmenu',$sub->groupmenu)->orderBy('noid')->get();
                                                                    @endphp
                                                                    @foreach($subb2 as $sob)
                                                                        <li class="menu-item" aria-haspopup="true">
                                                                            <a href="{{ $sob->linkidpage }}" class="menu-link">
                                                                                <!-- <i class="{{ $sub->iconclass }}"></i> -->
                                                                                <span style="color: {{$color[$sob->classcolor]['primary']}}"><i class="fa fa-file" style="color: {{$color[$sob->classcolor]['primary']}}"></i>&nbsp;&nbsp;{{ $sob->menucaption }}</span>
                                                                            </a>
                                                                        </li>
                                                                    @endforeach
                                                                </ul>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        @else
                                            @php
                                                $subb =  \App\Menu::where('idparent',$dat->noid)->get();
                                            @endphp
                                            <div class="menu-submenu menu-submenu-classic menu-submenu-left" data-hor-direction="menu-submenu-left">
                                                <ul class="menu-subnav">
                                                    @foreach($subb as $sub)
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ $sub->linkidpage }}" class="menu-link">
                                                                <!-- <i class="{{ $sub->iconclass }}"></i> -->
                                                                <span style="color: {{$color[$sub->classcolor]['primary']}}"><i class="fa fa-file" style="color: {{$color[$sub->classcolor]['primary']}}"></i>&nbsp;&nbsp;{{ $sub->menucaption}}</span>
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                    @endforeach
                                @endif
                                        </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
            </div>
        @else
                <div>
            </div>
        @endif
        @include('layout_lambada.partials._topbar')
    </div>
</div>
