<!-- Footer -->
<div class="footer d-flex flex-lg-column {{ Metronic::printClasses('footer', false) }}" id="kt_footer" style="position: fixed; bottom: 0; right: 0; left: 0; z-index: 97; background-color: black; height: 33px; padding: 0px 20px 0px 20px;">
    <!-- Container -->
    <div class="{{ Metronic::printClasses('footer-container', false) }} d-flex flex-column flex-md-row align-items-center justify-content-between">
        <!-- Copyright -->
        <div class="text-dark order-2 order-md-1">
            <span class="text-muted font-weight-bold mr-2" style="color: #a3a3a3 !important;">{{ date("Y") }} &copy;</span>
            <a href="https://lambada.id" target="_blank" class="text-dark-75 text-hover-primary" style="color: #a3a3a3 !important;">Lambada</a>
        </div>
        <!-- Nav -->
        <div class="nav nav-dark order-1 order-md-2">
            <!-- <a href="https://keenthemes.com/metronic" target="_blank" class="nav-link pr-3 pl-0" style="color: #a3a3a3 !important;">About</a>
            <a href="https://keenthemes.com/metronic" target="_blank" class="nav-link px-3" style="color: #a3a3a3 !important;">Team</a>
            <a href="https://keenthemes.com/metronic" target="_blank" class="nav-link pl-3 pr-0" style="color: #a3a3a3 !important;">Contact</a> -->
            <span class="nav-link" style="color: #a3a3a3 !important;">Implemented by Lambada</span>
        </div>
    </div>
</div>

<script>
    function ambil(dat) {
        if (dat == 0) {
            $(this).next("ul").toggle();
        } else {
            location.href = 'http://ux.lambada.id/'+dat;
        }
    }
</script>
