@php
	$direction = config('layout.extras.user.offcanvas.direction', 'right');
@endphp

 {{-- User Panel --}}
<div id="kt_quick_user" class="offcanvas offcanvas-{{ $direction }} p-10">
	<!-- Header -->
        @if (@$noid == 'null')
            <div class="offcanvas-header d-flex align-items-center justify-content-between pb-5">
                <h3 class="font-weight-bold m-0">
                    Login to your account
                    <!-- <small class="text-muted font-size-sm ml-2">12 messages</small> -->
                </h3>
                <a href="#" class="btn btn-xs btn-icon btn-light btn-hover-danger" id="kt_quick_user_close">
                    <i class="ki ki-close icon-xs text-muted"></i>
                </a>
            </div>
    <!-- End Header -->

            <form method="POST" action="{{ url('/login/loginPost') }}">
                @csrf
                <div class="form-group row">
                    <input placeholder="Username" id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="username" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group row">
                    <input placeholder="Password" id="password" type="password" class="form-control form-control-solid h-auto @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group row">
                    <!-- <div class="col-md-12"> -->
                        <!-- <button type="submit" id="kt_login_signin_submit" class="btn btn-primary font-weight-bold px-9 py-4 my-3">Login</button> -->
                        <button type="submit" class="btn btn-primary font-weight-bold"><i class="la la-sign-in"></i>
                                {{ __('Login') }}
                        </button>
                    <!-- </div> -->
                </div>
            </form>
		@else
            <div class="offcanvas-header d-flex align-items-center justify-content-between">
                <h3 class="font-weight-bold m-0">
                    User Profile
                    <small class="text-muted font-size-sm ml-2">Lambada</small>
                </h3>
                <a href="#" class="btn btn-xs btn-icon btn-light btn-hover-danger" id="kt_quick_user_close">
                    <i class="ki ki-close icon-xs text-muted"></i>
                </a>
            </div>

		    <!-- Content -->
			<div class="offcanvas-content pr-5 mr-n5">
                <!-- Header -->
                <div class="row pt-5 pr-3 pb-5 pl-3">
                    <div class="col-md-5">
                        <div class="symbol symbol-100 mr-5">
                            <div class="symbol-label img-rounded" style="background-image:url('{{ asset('media/users/'.(@$userdetail ? $userdetail->mcard_profilepicture : 'default.jpg')) }}')">
                            </div>
                            <i class="symbol-badge bg-success"></i>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <a href="{{ url('/99020203') }}" class="font-weight-bold font-size-h7 text-dark-75 text-hover-primary">
                            {{ Session::get('nama') }}
                        </a>
                        <div class="text-muted mt-1">
                            <?php
                                if (@Session::get('position')) {
                                    foreach (Session::get('position') as $k => $v) {
                                        echo '* '.$v->nama.'<br>';
                                    }
                                }
                            ?>
                        </div>
                        <div class="navi mt-2">
                            <!-- <a href="mailto:{{ Session::get('email') }}" class="navi-item"> -->
                                <span class="navi-link p-0 pb-2">
                                    <span class="navi-icon mr-1">
                                        <i class="fa fa-envelope"></i>
                                    </span>
                                    <span class="navi-text text-muted text-hover-primary_">{{ Session::get('email') }}</span>
                                    <a href="{{ url('/login/logout') }}" class="btn btn-sm btn-light-danger font-weight-bolder py-2 px-5"><i class="la la-sign-out"></i>Sign Out</a>
                                </span>
                            <!-- </a> -->
                        </div>
                    </div>
                </div>
            </div>
		@endif
</div>