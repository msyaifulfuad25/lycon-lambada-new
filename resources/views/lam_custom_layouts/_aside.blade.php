<!-- Aside -->
<?php
    $kt_logo_image = 'logo-light.png';
    $idhomepagelink = @$idtypepage;
    $idhomepage = @$idtypepage;

    $day = [
        'Minggu',
        'Senin',
        'Selasa',
        'Rabu',
        'Kamis',
        'Jum\'at',
        'Sabtu',
    ];

    $kt_logo_image = config('layout.brand.self.theme') === 'light'
        ? 'logo-dark.png'
        : 'lambada.png';
?>

<div class="aside aside-left {{ Metronic::printClasses('aside', false) }} d-flex flex-column flex-row-auto" id="kt_aside">
    <!-- Brand -->
    <div class="brand_ flex-column-auto {{ Metronic::printClasses('brand', false) }}" id="kt_brand" style="height: 46px: width: 265px !important">
    </div>
    
    <div style="font-family: 'Orbitron', sans-serif;
        /* color: #66ff99; */
        color: white;
        padding-top: 50px;
        text-align: center;
        background-color: #424266;">
        <span>
            {{$day[date('w')].', '.date('d-M-Y')}}
        </span>
    </div>
    <div id="clock" style="font-family: 'Orbitron', sans-serif;
        /* color: #66ff99; */
        color: white;
        font-size: 30px;
        font-weight: bold;
        text-align: center;
        background-color: #424266;"></div>

    <!-- Aside menu -->
    <div class="aside-menu-wrapper flex-column-fluid" id="kt_aside_menu_wrapper">
        @if (config('layout.aside.self.display') === false)
            <div class="header-logo">
                <a href="{{ url('/') }}">
                    <img alt="{{ config('app.name') }}" src="{{ asset('media/logos/'.$kt_logo_image) }}"/>
                </a>
            </div>
        @endif
        <div id="kt_aside_menu" class="aside-menu {{ Metronic::printClasses('aside_menu', false) }}" data-menu-vertical="1" {{ Metronic::printAttrs('aside_menu') }}>
            <ul class="menu-nav {{ Metronic::printClasses('aside_menu_nav', false) }}" style="padding-top: 0px">
                <?php
                    $sidebar = !Session::get('login')
                        ? App\Menu::getSidebarLoginBaru($idhomepagelink)
                        : App\Menu::getSidebarLogin($idhomepagelink);
                
                    foreach ($sidebar as $k => $v) {
                        $groupmenu = ($v->groupmenu == '')
                            ? 'Dashboard'
                            : $v->groupmenu;

                        if ($v->menulevel == 3) { ?>
                            <li class="menu-section" style="margin-top: 0px">
                                <h4 class="menu-text">{{ $groupmenu }}</h4>
                                <i class="menu-icon ki ki-bold-more-hor icon-md"></i>
                            </li> <?php

                            $subsidebar = !Session::get('login')
                                ? \App\Menu::where('menulevel',3)->where('isactive',1)->where('ispublic',1)->where('groupmenu',$v->groupmenu)->orderBy('nourut')->get()
                                : \App\Menu::getMenuLevel(['menulevel'=>3,'idparent'=>$v->idparent, 'groupmenu'=>$v->groupmenu]);
                            
                            foreach ($subsidebar as $v2) {
                                $url = Request::path();
                                $fixurl = $url == '/'
                                    ? $idhomepage
                                    : Request::path();
                                
                                $findurl = !Session::get('login')
                                    ? \App\Menu::where('linkidpage',$fixurl)->where('ispublic',1)->orderBy('nourut')->first()
                                    : \App\Menu::where('linkidpage',$fixurl)->orderBy('nourut')->first();

                                if (isset($findurl)){
                                    if ($v2->noid == $findurl->linkidpage ||  str_contains($findurl->linkidpage, $v2->noid) ) { ?>
                                        <li id="10" class="menu-item menu-item-submenu menu-item-open" aria-haspopup="true" data-menu-toggle="hover"> <?php
                                    } else { ?>
                                        <li id="11" class="menu-item  menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover"> <?php
                                    }
                                } else { ?>
                                    <li id="11" class="menu-item  menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover"> <?php
                                } ?>

                                <a href="javascript:;" class="menu-link menu-toggle">
                                    <span class="menu-text"><i class="fa fa-file"></i>&nbsp;&nbsp;{{ $v2->menucaption }}</span>
                                    <i class="menu-arrow"></i>
                                </a> <?php

                                if ($v2->groupmenu == '') {
                                    $findsubsidebar = !Session::get('login')
                                        ? \App\Menu::where('menulevel', 4)->where('isactive',1)->where('ispublic',1)->where('idparent',$v2->noid)->orderBy('nourut')->get()
                                        : \App\Menu::where('menulevel', 4)->where('isactive',1)->where('idparent',$v2->noid)->orderBy('nourut')->get();

                                    foreach ($findsubsidebar as $v3) { ?>
                                        <div class="menu-submenu">
                                            <i class="menu-arrow"></i>
                                            <ul class="menu-subnav">
                                                <li class="menu-item {{ Request::path() == $v3->linkidpage ? 'menu-item-active' : '' }}" aria-haspopup="true" title="{{ $v3->menuhint }}">
                                                    <a href="{{ url('/'.$v3->noid) }}" class="menu-link">
                                                        <i class="menu-bullet menu-bullet-line">
                                                            <span></span>
                                                        </i>
                                                        <span class="menu-text">{{ $v3->menucaption}}</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div> <?php
                                    }
                                } else {
                                    $findsubsidebar = !Session::get('login') || str_contains($v2->noid, $idhomepage)
                                        ? \App\Menu::where('menulevel', 4)->where('isactive',1)->where('ispublic',1)->where('idparent',$v2->noid)->orderBy('nourut')->get()
                                        : \App\Menu::getMenuLevel(['menulevel'=>4,'idparent'=>$v2->noid]);
                                        
                                    foreach ($findsubsidebar as $v3) {
                                        if ($v3->linkidpage != @$privilege['idmenu'] || $privilege['isenable']) { ?>
                                            <div class="menu-submenu">
                                                <i class="menu-arrow"></i>
                                                <ul class="menu-subnav">
                                                    <li class="menu-item {{ Request::path() == $v3->linkidpage ? 'menu-item-active' : '' }}" aria-haspopup="true" title="{{ $v3->menuhint }}">
                                                        <a href="{{ url('/'.$v3->noid) }}" class="menu-link">
                                                            <i class="menu-bullet menu-bullet-line">
                                                                <span></span>
                                                            </i>
                                                            <span class="menu-text">
                                                                {{ $v3->menucaption}}
                                                            </span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div> <?php
                                        }
                                    }
                                } ?> </li> <?php
                            }
                        }
                    }
                ?>
            </ul>
        </div>
    </div>
</div>
