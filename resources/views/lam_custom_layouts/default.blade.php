<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" {{ Metronic::printAttrs('html') }} {{ Metronic::printClasses('html') }}>
    <head>
        <meta charset="utf-8"/>

        <!-- Title Section -->
        <title>{{ config('app.name') }}{{@$tabletitle ? ' | '.@$tabletitle : '' }}</title>
        
        <!-- Meta Data -->
        <meta name="description" content="@yield('page_description', $page_description ?? '')"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
        <link href="{{ asset('css/component.css') }}" rel="stylesheet"/>
        <link href="{{ asset('css/layout.min.css') }}" rel="stylesheet"/>

        <!-- Favicon -->
        <link rel="shortcut icon" href="{{ asset('media/logos/lambada.png') }}" />

        <!-- Fonts -->
        <link href="{{ asset('css/font/font-poppins.css') }}" rel="stylesheet"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"/>
        
        <!-- Global Theme Styles (used by all pages) -->
        @foreach(config('layout.resources.css') as $style)
            <link href="{{ config('layout.self.rtl') ? asset(Metronic::rtlCssPath($style)) : asset($style) }}" rel="stylesheet"/>
        @endforeach
        <link href="template/metronic/assets/plugins/global/plugins.bundle.css?v=7.0.6" rel="stylesheet"/>
        <link href="template/metronic/assets/plugins/custom/prismjs/prismjs.bundle.css?v=7.0.6" rel="stylesheet"/>
        <link href="template/metronic/assets/css/style.bundle.css?v=7.0.6" rel="stylesheet"/>
        
        <!-- Layout Themes (used by all pages) -->
        @foreach (Metronic::initThemes() as $theme)
            <link href="{{ config('layout.self.rtl') ? asset(Metronic::rtlCssPath($theme)) : asset($theme) }}" rel="stylesheet"/>
        @endforeach
        <link href="template/metronic/assets/css/themes/layout/header/base/light.css?v=7.0.6" rel="stylesheet"/>
        <link href="template/metronic/assets/css/themes/layout/header/menu/light.css?v=7.0.6" rel="stylesheet"/>
        <link href="template/metronic/assets/css/themes/layout/brand/dark.css?v=7.0.6" rel="stylesheet"/>
        <link href="template/metronic/assets/css/themes/layout/aside/dark.css?v=7.0.6" rel="stylesheet"/>

        <!-- Bootstrap -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet"></link>
        @yield('styles')
        <meta name="csrf-token" content="{{ csrf_token() }}" />

    </head>
    <body>
        <div id="loading" style="position: fixed;
            z-index: 999;
            height: 100%;
            width: 100%;
            top: 0;
            left: 0;
            background-color: Black;
            filter: alpha(opacity=60);
            opacity: 0.6;
            -moz-opacity: 0.8;
            display: none">
            <div style="z-index: 1000;
                margin: 300px auto;
                padding: 10px;
                width: 130px;
                background-color: White;
                border-radius: 10px;
                filter: alpha(opacity=100);
                opacity: 1;
                -moz-opacity: 1;">
                <img alt="" src="assets/loading.gif" width="110" height="110"/>
            </div>
        </div>
    
            @include('lam_custom_layouts._layout')
                
        <script>
            function currentTime() {
                var date = new Date(); /* creating object of Date class */
                var hour = date.getHours();
                var min = date.getMinutes();
                var sec = date.getSeconds();
                hour = updateTime(hour);
                min = updateTime(min);
                sec = updateTime(sec);
                document.getElementById("clock").innerText = hour + " : " + min + " : " + sec; /* adding time to the div */
                var t = setTimeout(function(){ currentTime() }, 1000); /* setting timer */
            }

            function updateTime(k) {
                if (k < 10) {
                    return "0" + k;
                }
                else {
                    return k;
                }
            }

            currentTime(); /* calling currentTime() function to initiate the process */




            /////////
            var HOST_URL = "{{ route('quick-search') }}";
            var KTAppSettings = {!! json_encode(config('layout.js'), JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES) !!};
        </script>

        <!-- Global Theme JS Bundle (used by all pages) -->
        @foreach(config('layout.resources.js') as $script)
            <script src="{{ asset($script) }}" type="text/javascript"></script>
        @endforeach

        <!-- Includable JS -->
        @yield('scripts')
    </body>
</html>
