{{-- Extends layout --}}

<!-- 10: Stat Box

23: Chart

71: Google Maps

151: News Slider

1011: DataTables -->

@extends('layout_lambada.default')

@section('content')

<div class="card card-custom gutter-b example example-compact">
			<div class="card-header">
				<div class="card-title">
					<h3 class="card-label">
						Site Map
					</h3>
				</div>
				<div class="card-toolbar">
					<div class="example-tools justify-content-center">
						<!-- <span class="example-toggle" data-toggle="tooltip" title="" data-original-title="View code"></span> -->
					</div>
				</div>
			</div>
			<div class="card-body">

        <body>
          <!-- google maps -->
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3957.264429900039!2d112.73631051404371!3d-7.324167274057504!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7fb6b30e84ae9%3A0x719cef8633c69031!2sPT.%20LYCON%20ASIA%20MANDIRI!5e0!3m2!1sid!2sid!4v1600829984462!5m2!1sid!2sid" width="900" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </body>


				<!--end::Code-->
			</div>
		</div>

@endsection

