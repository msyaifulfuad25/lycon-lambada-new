{{-- Extends layout --}}
<!-- 10: Stat Box
23: Chart
71: Google Maps
151: News Slider
1011: DataTables -->
@extends('layout_lambada.default')
@section('content')
<!--begin::Hero-->
<div class="d-flex flex-row-fluid bgi-size-cover bgi-position-center" style="background-image: url({{asset('media/bg/bg-9.jpg') }} ">
  <div class=" container ">
      <!--begin::Topbar-->
      <div class="d-flex justify-content-between align-items-center border-bottom border-white py-7">
          <h3 class="h4 text-dark mb-0">
              About Application
          </h3>
          <div class="d-flex">
              <!-- <a href="#" class="font-size-h6 font-weight-bold">Community</a>
              <a href="#" class="font-size-h6 font-weight-bold ml-8">Visit Blog</a> -->
          </div>
      </div>
      <!--end::Topbar-->
      <div class="d-flex align-items-stretch text-center flex-column py-40">
          <!--begin::Heading-->
          <h1 class="text-dark font-weight-bolder mb-12">
              How can we help?
          </h1>
          <!--end::Heading-->

          <!--begin::Form-->
          <form class="d-flex position-relative w-75 px-lg-40 m-auto">
              <div class="input-group">
                  <!--begin::Icon-->
                  <div class="input-group-prepend">
                      <span class="input-group-text bg-white border-0 py-7 px-8" >
                          <span class="svg-icon svg-icon-xl"><!--begin::Svg Icon | path:assets/media/svg/icons/General/Search.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
      <rect x="0" y="0" width="24" height="24"/>
      <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
      <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero"/>
  </g>
</svg><!--end::Svg Icon--></span>                        </span>
                  </div>
                  <!--end::Icon-->

                  <!--begin::Input-->
                  <input type="text" class="form-control h-auto border-0 py-7 px-1 font-size-h6" placeholder="Ask a question"  />
                  <!--end::Input-->
              </div>
          </form>
          <!--end::Form-->
      </div>
  </div>
</div>
<!--end::Hero-->


<!--begin::Section-->
<div class=" container  mt-n15 gutter-b">
	<div class="card card-custom">
		<div class="card-body py-12">
			<div class="row">
				<div class="col-lg-3">
					<!--begin::Navigation-->
					<ul class="navi navi-link-rounded navi-accent navi-success navi-hover mb-8 mb-md-0" role="tablist">
						<!--begin::Nav Item-->
						<li class="navi-item mb-2">
							<a class="navi-link" data-toggle="tab" href="#kt_profile_tab_personal_information">
								<span class="navi-text font-size-h5 font-weight-bold mb-0">Standard</span>
							</a>
						</li>
						<!--end::Nav Item-->

						<!--begin::Nav Item-->
						<li class="navi-item mb-2">
							<a class="navi-link active" data-toggle="tab" href="#kt_profile_tab_personal_information">
								<span class="navi-text font-size-h5 font-weight-bold mb-0">Multiple</span>
							</a>
						</li>
						<!--end::Nav Item-->

						<!--begin::Nav Item-->
						<li class="navi-item mb-2">
							<a class="navi-link" data-toggle="tab" href="#kt_profile_tab_personal_information">
								<span class="navi-text font-size-h5 font-weight-bold mb-0">Extended</span>
							</a>
						</li>
						<!--end::Nav Item-->

						<!--begin::Nav Item-->
						<li class="navi-item mb-2">
							<a class="navi-link" data-toggle="tab" href="#kt_profile_tab_personal_information">
								<span class="navi-text font-size-h5 font-weight-bold mb-0">Custom Licenses</span>
							</a>
						</li>
						<!--end::Nav Item-->
					</ul>
					<!--end::Navigation-->
				</div>
				<div class="col-lg-7">
					<h3 class="font-weight-bold mb-10 text-dark">The point here is that anyone can ramble?</h3>
					<div class="font-weight-nromal font-size-lg mb-6">
						<p>The point here is that anyone can ramble on and on or even write brief statement about something that really <a href="javascript:;">Download Sample</a>.
							If you want people to continue to follow you and your blog Your blog post fonts are automatically styled according to your site's theme. However,
							you can change the text font and format by selecting the text and clicking the options.</p>
					</div>
					<div class="mb-5">
						<!--begin::Table-->
						<div class="table-responsive">
							<table class="table table-light table-light-success">
								<thead>
									<tr>
										<th></th>
										<th class="table-center">Standard</th>
										<th class="table-center">Multiple</th>
										<th class="table-center">Extended</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="font-weight-bold table-row-title">Installations</td>
										<td class="table-center">1</td>
										<td class="table-center">10</td>
										<td class="table-center">Unlimited</td>
									</tr>
									<tr class="bg-gray-100">
										<td class="font-weight-bold table-row-title">End Product Usage</td>
										<td class="table-center"><span class="svg-icon svg-icon-danger"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Close.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
            <rect x="0" y="7" width="16" height="2" rx="1"></rect>
            <rect opacity="0.3" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000) " x="0" y="7" width="16" height="2" rx="1"></rect>
        </g>
    </g>
</svg><!--end::Svg Icon--></span></td>
										<td class="table-center"><span class="svg-icon svg-icon-success"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Double-check.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"></polygon>
        <path d="M9.26193932,16.6476484 C8.90425297,17.0684559 8.27315905,17.1196257 7.85235158,16.7619393 C7.43154411,16.404253 7.38037434,15.773159 7.73806068,15.3523516 L16.2380607,5.35235158 C16.6013618,4.92493855 17.2451015,4.87991302 17.6643638,5.25259068 L22.1643638,9.25259068 C22.5771466,9.6195087 22.6143273,10.2515811 22.2474093,10.6643638 C21.8804913,11.0771466 21.2484189,11.1143273 20.8356362,10.7474093 L17.0997854,7.42665306 L9.26193932,16.6476484 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(14.999995, 11.000002) rotate(-180.000000) translate(-14.999995, -11.000002) "></path>
        <path d="M4.26193932,17.6476484 C3.90425297,18.0684559 3.27315905,18.1196257 2.85235158,17.7619393 C2.43154411,17.404253 2.38037434,16.773159 2.73806068,16.3523516 L11.2380607,6.35235158 C11.6013618,5.92493855 12.2451015,5.87991302 12.6643638,6.25259068 L17.1643638,10.2525907 C17.5771466,10.6195087 17.6143273,11.2515811 17.2474093,11.6643638 C16.8804913,12.0771466 16.2484189,12.1143273 15.8356362,11.7474093 L12.0997854,8.42665306 L4.26193932,17.6476484 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.999995, 12.000002) rotate(-180.000000) translate(-9.999995, -12.000002) "></path>
    </g>
</svg><!--end::Svg Icon--></span></td>
										<td class="table-center"><span class="svg-icon svg-icon-success"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Double-check.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"></polygon>
        <path d="M9.26193932,16.6476484 C8.90425297,17.0684559 8.27315905,17.1196257 7.85235158,16.7619393 C7.43154411,16.404253 7.38037434,15.773159 7.73806068,15.3523516 L16.2380607,5.35235158 C16.6013618,4.92493855 17.2451015,4.87991302 17.6643638,5.25259068 L22.1643638,9.25259068 C22.5771466,9.6195087 22.6143273,10.2515811 22.2474093,10.6643638 C21.8804913,11.0771466 21.2484189,11.1143273 20.8356362,10.7474093 L17.0997854,7.42665306 L9.26193932,16.6476484 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(14.999995, 11.000002) rotate(-180.000000) translate(-14.999995, -11.000002) "></path>
        <path d="M4.26193932,17.6476484 C3.90425297,18.0684559 3.27315905,18.1196257 2.85235158,17.7619393 C2.43154411,17.404253 2.38037434,16.773159 2.73806068,16.3523516 L11.2380607,6.35235158 C11.6013618,5.92493855 12.2451015,5.87991302 12.6643638,6.25259068 L17.1643638,10.2525907 C17.5771466,10.6195087 17.6143273,11.2515811 17.2474093,11.6643638 C16.8804913,12.0771466 16.2484189,12.1143273 15.8356362,11.7474093 L12.0997854,8.42665306 L4.26193932,17.6476484 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.999995, 12.000002) rotate(-180.000000) translate(-9.999995, -12.000002) "></path>
    </g>
</svg><!--end::Svg Icon--></span></td>
									</tr>
									<tr>
										<td class="font-weight-bold table-row-title">Unlimited Users</td>
										<td class="table-center">Fixed</td>
										<td class="table-center"><span class="svg-icon svg-icon-success"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Double-check.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"></polygon>
        <path d="M9.26193932,16.6476484 C8.90425297,17.0684559 8.27315905,17.1196257 7.85235158,16.7619393 C7.43154411,16.404253 7.38037434,15.773159 7.73806068,15.3523516 L16.2380607,5.35235158 C16.6013618,4.92493855 17.2451015,4.87991302 17.6643638,5.25259068 L22.1643638,9.25259068 C22.5771466,9.6195087 22.6143273,10.2515811 22.2474093,10.6643638 C21.8804913,11.0771466 21.2484189,11.1143273 20.8356362,10.7474093 L17.0997854,7.42665306 L9.26193932,16.6476484 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(14.999995, 11.000002) rotate(-180.000000) translate(-14.999995, -11.000002) "></path>
        <path d="M4.26193932,17.6476484 C3.90425297,18.0684559 3.27315905,18.1196257 2.85235158,17.7619393 C2.43154411,17.404253 2.38037434,16.773159 2.73806068,16.3523516 L11.2380607,6.35235158 C11.6013618,5.92493855 12.2451015,5.87991302 12.6643638,6.25259068 L17.1643638,10.2525907 C17.5771466,10.6195087 17.6143273,11.2515811 17.2474093,11.6643638 C16.8804913,12.0771466 16.2484189,12.1143273 15.8356362,11.7474093 L12.0997854,8.42665306 L4.26193932,17.6476484 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.999995, 12.000002) rotate(-180.000000) translate(-9.999995, -12.000002) "></path>
    </g>
</svg><!--end::Svg Icon--></span></td>
										<td class="table-center"><span class="svg-icon svg-icon-success"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Double-check.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"></polygon>
        <path d="M9.26193932,16.6476484 C8.90425297,17.0684559 8.27315905,17.1196257 7.85235158,16.7619393 C7.43154411,16.404253 7.38037434,15.773159 7.73806068,15.3523516 L16.2380607,5.35235158 C16.6013618,4.92493855 17.2451015,4.87991302 17.6643638,5.25259068 L22.1643638,9.25259068 C22.5771466,9.6195087 22.6143273,10.2515811 22.2474093,10.6643638 C21.8804913,11.0771466 21.2484189,11.1143273 20.8356362,10.7474093 L17.0997854,7.42665306 L9.26193932,16.6476484 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(14.999995, 11.000002) rotate(-180.000000) translate(-14.999995, -11.000002) "></path>
        <path d="M4.26193932,17.6476484 C3.90425297,18.0684559 3.27315905,18.1196257 2.85235158,17.7619393 C2.43154411,17.404253 2.38037434,16.773159 2.73806068,16.3523516 L11.2380607,6.35235158 C11.6013618,5.92493855 12.2451015,5.87991302 12.6643638,6.25259068 L17.1643638,10.2525907 C17.5771466,10.6195087 17.6143273,11.2515811 17.2474093,11.6643638 C16.8804913,12.0771466 16.2484189,12.1143273 15.8356362,11.7474093 L12.0997854,8.42665306 L4.26193932,17.6476484 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.999995, 12.000002) rotate(-180.000000) translate(-9.999995, -12.000002) "></path>
    </g>
</svg><!--end::Svg Icon--></span></td>
									</tr>
									<tr class="bg-gray-100">
										<td class="font-weight-bold table-row-title">Unlimited Domains</td>
										<td class="table-center">Fixed</td>
										<td class="table-center"><span class="svg-icon svg-icon-danger"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Close.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
            <rect x="0" y="7" width="16" height="2" rx="1"></rect>
            <rect opacity="0.3" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000) " x="0" y="7" width="16" height="2" rx="1"></rect>
        </g>
    </g>
</svg><!--end::Svg Icon--></span></td>
										<td class="table-center"><span class="svg-icon svg-icon-success"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Double-check.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"></polygon>
        <path d="M9.26193932,16.6476484 C8.90425297,17.0684559 8.27315905,17.1196257 7.85235158,16.7619393 C7.43154411,16.404253 7.38037434,15.773159 7.73806068,15.3523516 L16.2380607,5.35235158 C16.6013618,4.92493855 17.2451015,4.87991302 17.6643638,5.25259068 L22.1643638,9.25259068 C22.5771466,9.6195087 22.6143273,10.2515811 22.2474093,10.6643638 C21.8804913,11.0771466 21.2484189,11.1143273 20.8356362,10.7474093 L17.0997854,7.42665306 L9.26193932,16.6476484 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(14.999995, 11.000002) rotate(-180.000000) translate(-14.999995, -11.000002) "></path>
        <path d="M4.26193932,17.6476484 C3.90425297,18.0684559 3.27315905,18.1196257 2.85235158,17.7619393 C2.43154411,17.404253 2.38037434,16.773159 2.73806068,16.3523516 L11.2380607,6.35235158 C11.6013618,5.92493855 12.2451015,5.87991302 12.6643638,6.25259068 L17.1643638,10.2525907 C17.5771466,10.6195087 17.6143273,11.2515811 17.2474093,11.6643638 C16.8804913,12.0771466 16.2484189,12.1143273 15.8356362,11.7474093 L12.0997854,8.42665306 L4.26193932,17.6476484 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.999995, 12.000002) rotate(-180.000000) translate(-9.999995, -12.000002) "></path>
    </g>
</svg><!--end::Svg Icon--></span></td>
									</tr>
								</tbody>
							</table>
						</div>
						<!--end::Table-->
					</div>

					<div class="font-weight-nromal font-size-lg mb-6">
						<p>If you have prepared your text in a Word document or another external source, you can paste it into your
							 Wix Blog post in plain text format. In order to do so, when pasting, use <span class="font-weight-bold">Ctrl + Shift + V</span> (Cmd + Shift + V on a Mac).
							 This will remove all formatting and prevent any formatting issues.
						 </p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--end::Section-->


@endsection
