{{-- Extends layout --}}

<!-- 10: Stat Box

23: Chart

71: Google Maps

151: News Slider

1011: DataTables -->

@extends('layout_lambada.default')

@section('content')


<div class="d-flex flex-row-fluid flex-column bgi-size-cover bgi-position-center bgi-no-repeat p-10 p-sm-30" style="background-image: url({{asset('media/error/bg1.jpg') }}">

	<!--begin::Content-->

	<h1 class="font-weight-boldest text-dark-75 mt-15" style="font-size: 5rem">Lock Screen</h1>

	<p class="font-size-h3 text-muted font-weight-normal">

		OOPS! Something went wrong here

	</p>

	<!--end::Content-->
</div>


@endsection

