{{-- Extends layout --}}

@extends('layout_lambada.default')

@section('styles')
    <!-- <link href="{{ asset('css/component.css') }}" rel="stylesheet" type="text/css"/> -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet"></link>
    <link href="//www.amcharts.com/lib/3/plugins/export/export.css?v=7.0.6" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.0.2/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.0.2/css/bootstrap-datetimepicker.css">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/tabel.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet" type="text/css"/>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <style media="screen">
        .container {
            display: none
        }

        .dataTable thead .sorting_asc,
        .dataTable thead .sorting_desc,
        .dataTable thead .sorting_disabled,
        .dataTable thead .sorting {
            padding: 3px 3px 3px 3px !important;
            /* color: black !important; */
        }
        
        .dataTable tbody td,
        .dataTable tbody td:hover,
        .dataTable tbody tr:hover {
            padding: 3px 3px 3px 3px !important;
        }

        .my_row {
            height: <?= $heightwgf ?>px;
        }

        .my_row:hover {
            /* display: none; */
            padding: 0px 0px 0px 0px !important;
        }

        .table-bordered th,
        .table-bordered td,
        .table-bordered tfoot th {
            border: 1px solid #95c9ed
        }

        .dataTables_wrapper {
            margin-top: -1px !important
        }
        
        #table_pagepanel_9921050101 {
            width:2500px !important;
            border: 0;
        }

        /* tbody tr td {
            padding: 0px 5px 0px 5px !important;f
            width: 100px !important;
        } */

        /* .dataTable thead th:nth-child(10) {
            width: 140px !important;
        } */

        /* .dataTable tbody tr td:nth-child(10) {
            width: 140px !important;
        } */


        /* tbody:nth-child(odd) tr td:nth-child(odd) {
            width: 100px !important;
        } */
    </style>
@endsection


{{-- Content --}}
@section('content')
    <div class="container">
    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
    <div id="pagepanel" class="card card-custom flat">
        <div class="card-header card-header_ bg-main-1 d-flex flat" >
            <div class="card-title">
                <span class="card-icon">
                    <i id="widgeticon" class="text-white"></i>
                </span>
                <h3 id="widgetcaption" class="card-label text-white">
                </h3>
            </div>
            <div class="card-toolbar" >
                <div id="tool-pertama">
                    <a  onclick="addData()" href="javascript:;" id="btn-add-data" style="color: #FFFFFF;margin-right: 5px;" class="btn btn-sm flat" onmouseover="mouseoverAddData('btn-add-data')" onmouseout="mouseoutAddData('btn-add-data')"><i style="color:#FFFFFF" class="fa fa fa-save"></i> Tambah Data</a>
                </div>
                <div id="tool-kedua" style="display: none">
                    <a id="savedata" class="btn btn-sm btn-success flat" style="margin-right: 5px;">
                        <i class="fa fa-save"></i> Save
                    </a>
                    <a onclick="cancelData()" class="btn btn-sm btn-default flat" style="margin-right: 5px;">
                        <i class="fa fa-reply"></i> Cancel
                    </a>
                </div>
                <div id="tool-ketiga" style="display: none">
                    <a id="back" class="btn btn-sm btn-success flat" onclick="cancelData()"  style="margin-right: 5px;">
                        <i class="fa fa-reply"></i> Back
                    </a>
                </div>
                <div class="dropdown" style="margin-right: 5px;">
                    <a style="background-color: transparent !important; border: 1px solid #95c9ed; color: #aad4f0;" class="btn btn-default btn-sm flat" href="javascript:;" data-toggle="dropdown">
                        <i style="color:#FFFFFF" class="fa fa-cogs"></i>
                            Tools
                        <i style="color:#FFFFFF" class="fa fa-angle-down"></i>
                    </a>
                    <ul style="" id="drop_export" class="dropdown-menu dropdown-menu-right">
                        <li id="drop_export2"></li>
                    </ul>
                </div>
                <a style="background-image: none !important; margin-right: 5px; /* height: 27px; */ width: 27px !important; text-align: center; padding: 2px !important; border-radius: 50% !important; background-color: #2196F3; color: #FFF;" id="tool_refresh" href="javascript:;" onclick="refresh()" class="bg-main-1"><i style="color:#FFFFFF" class="fa fa-sync-alt"></i></a>
                <a style="background-image: none !important; margin-right: 5px; /* height: 27px; */ width: 27px !important; text-align: center; padding: 2px !important; border-radius: 50% !important; background-color: #2196F3; color: #FFF;" id="tool_filter" href="javascript:;" onclick="showModalFilter()" class="bg-main-1"><i style="color:#FFFFFF" class="fa fa-filter"></i></a>
                <a style="background-image: none !important; /* height: 27px; */ width: 27px !important; text-align: center; padding: 2px !important; border-radius: 50% !important; background-color: #2196F3; color: #FFF;" id="tool_refresh_up" onClick="toggleButtonCollapse()" href="javascript:;" class="bg-main-1"><i style="color:#FFFFFF" class="fa fa-sort"></i></a>
                </div>
            </div>
            <div class="card-body card-body-custom flat">
                <div class="row">
                    <!-- <div class="col"> -->
                        <div id="space_filter_button" style="margin-bottom: 10px">
                        </div>
                    <!-- </div> -->
                </div>
                <input type="hidden" id="judul_excel">
                <input type="hidden"  id="tabel1" value="{{$code}}">
                <input type="hidden"  id="namatable">
                <input type="hidden"  id="url_code">
                
                <div class="row">
                    <div class="btn-toolbar" role="toolbar" aria-label="...">
                        <div id="filterlain">
                        </div>
                    </div>
                </div>

                <div class="cursor"></div>
                <!-- <input type="text" id="datetime" readonly> -->
                <div id="from-departement">
                    <form data-toggle="validator" class="form" id="formdepartement">
                        <div class="row" id="divform" style="margin-top: 20px">
                        </div>
                    </form>
                </div>
                <div id="tabledepartemen" class="table table-bordered " style="width: 100%; border: 0px; margin: -10px 0px -10px 0px !important">
                    <!-- <div id="divsucces" class="alert alert-custom alert-light-success fade show mb-5" role="alert">
                        <p id="notiftext"></p>
                    </div> -->
                <!-- <table id="tabletest" class="nowrap" style="width 100px; table-layout: auto">
                    <thead>
                        <tr>
                            <th style="width: 10% !important">Halaman 1</th>
                            <th style="width: 10% !important">Halaman 2</th>
                            <th style="width: 10% !important">Halaman 3</th>
                            <th style="width: 10% !important">Halaman 4</th>
                        </tr>
                    </thead>
                </table> -->
                <!-- <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12 col-xs-12 dtfiler">
                            <div class="col-md-4" id="table_pagepanel_9921050101_paginate">
                                <div class="pagination-panel row">
                                    Page
                                    <a href="#" class="btn btn-sm default prev disabled" title="<i class='fa fa-angle-left'></i>">
                                        <i class="fa fa-angle-left"></i>
                                    </a>
                                    <input type="text" class="pagination-panel-input form-control input-mini input-inline input-sm" maxlenght="5" style="text-align:center; margin: 0 5px;">
                                    <a href="#" class="btn btn-sm default next disabled" title="<i class='fa fa-angle-right'> </i>">
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                     of 
                                    <span class="pagination-panel-total">1</span>
                                </div>
                            </div>
                            <div class="col-md-4" id="table_pagepanel_9921050101_length">
                                <label class="">
                                    <span class="seperator">|</span>
                                    Rec/Page 
                                    <select name="table_pagepanel_9921050101_length" aria-controls="table_pagepanel_9921050101" class="form-control input-inline input-xsmall input-sm recofdt">
                                        <option value="10">10</option>
                                        <option value="50">50</option>
                                        <option value="150">150</option>
                                        <option value="1000">All</option>
                                    </select>
                                </label>
                            </div>
                            <div class="col-md-4" id="table_pagepanel_9921050101_info" role="status" aria-live="polite">
                                <span class="seperator">|</span>Total 6 records 
                            </div>
                        </div>
                        <!-- <div class="dtfilter_search">
                            <div id="table_pagepanel_9921050101_filter" class="dataTables_filter">
                                <label>
                                    <input type="search" class="form-control input-inline input-sm" placeholder="" aria-controls="table_pagepanel_9921050101">
                                </label>
                            </div>
                            <div class="btn fa fa-search dtfilter_search_btn">
                            </div>
                        </div>
                    </div>
                </div> -->
                <div id="appendInfoDatatable" style="border: 1px solid #3598dc; margin-top: 10px"></div>
                <div id="appendButtonDatatable"></div>
                <div class="table-scrollable" style="border: 1px solid #3598dc;">
                    <table bg-color="blue" class="nowrap table table-striped table-bordered table-hover  dataTable no-footer" id="table_pagepanel_9921050101"  aria-describedby="table_pagepanel_9921050101_info" role="grid">
                        <thead id="thead_pagepanel_9921050101">
                            <tr id="thead" style="background: #90CAF9"></tr>
                            <tr id="thead-columns-d" style="background: #BBDEFC"></tr>
                        </thead>
                        <tfoot>
                            <tr id="tfoot-columns-d" style="background: #BBDEFC">
                            </tr>
                        </tfoot>
                    </table>
                </div>

                <!-- <div class="table-scrollable" style="border: 1px solid #3598dc;">
                    <table id="example_baru" class="table table-striped table-bordered table-hover nowrap dataTable no-footer" style="background-color: #BBDEFB; border-collapse: collapse;">
                        <thead>
                            <tr id="thead" class="bg-main"></tr>
                            <tr id="thead-columns"></tr>
                        </thead>
                        <tfoot>
                            <tr id="tfoot-columns">
                            </tr>
                        </tfoot>
                    </table>
                </div> -->
            </div>
        </div>
    </div>

    <br>

    <div class="card card-custom flat" id="another-card" style="display: none">
        <div class="card-header card-header_ bg-main-1 d-flex flat" style="height: 40px">
            <div class="card-title">
                <span class="card-icon">
                    <i class="fa fa-list text-white"></i>
                </span>
            </div>
        </div>
        <div class="card-body card-body-tab-custom flat">
            <div class="right-tabs-widget">
                <ul class="nav nav-tabs" id="tab-widget" role="tablist">
                </ul>
                <div class="tab-content" id="tab-widget-content">
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="card card-custom flat">
        <div class="card-body flat">
            <div class="row" id="another-form">
            </div>
        </div>
    </div> -->

    <div id="modal-filter" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-filter modal-dialog">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="closeModalFilter()"></a>
                    <h4 class="modal-title">Config Datatable</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="formfilter">
                        <div class="row" id="filtercontent">
                            <div class="col-md-12">
                                <div class="portlet box blue-steel" style="margin: 8px;">
                                    <div class="portlet-title">
                                        <div class="caption" style="font-weight: bold">
                                            <i class="fa fa-filter"></i>
                                            Datatable Filter
                                        </div>
                                        <div class="actions">
                                            <a onclick="saveFilter()" href="javascript:;" class="btn green btn-sm">
                                                <i class="fa fa-save text-white"></i> Save
                                            </a>
                                            <a id="addgroup" href="javascript:;" class="btn yellow btn-sm">
                                                <i class="fa fa-indent text-white"></i> Add Group
                                            </a>
                                            <a id="delgroup" href="javascript:;" class="btn red btn-sm">
                                                <i class="fa fa-trash text-white"></i> Delete Group
                                            </a>
                                            <a onclick="addRow()" href="javascript:;" class="btn purple btn-sm">
                                                <i class="fa fa-list text-white"></i> Add Row
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row" style="margin:0;">
                                            <div class="col-md-12" style="padding: 0 0 15px 0;">
                                                <div id="filtercontentgroup">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin:0;">
                                            <div class="col-md-12">
                                                <div id="filtercontentbody">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal CKEditor -->
    <div id="modal-ckeditor" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-filter modal-dialog">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="closeModalFilter()"></a>
                    <h4 class="modal-title">[0] PHPSOURCE</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <textarea class="form-control" id="summary-ckeditor" name="summary-ckeditor"></textarea>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
@section('scripts')
<!-- <script src="//www.amcharts.com/lib/3/amcharts.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/serial.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/radar.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/pie.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/plugins/animate/animate.min.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/plugins/export/export.min.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/themes/light.js?v=7.0.6"></script> -->
<!-- <script src="https://cdn.amcharts.com/lib/4/core.js"></script> -->
<!-- <script src="https://cdn.amcharts.com/lib/4/charts.js"></script> -->
<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
<script src="{{ asset('js/pages/crud/forms/widgets/bootstrap-datepicker.js?v=7.0.6') }}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.3.1/js/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.21/pagination/input.js"></script>
<script  src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.0.2/js/bootstrap-datetimepicker.min.js"></script>
<!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script> -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-loading-overlay/2.1.7/loadingoverlay.min.js"></script>
<script src="{{ asset('js/jquery-currency.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script>
    $("#datetime").datetimepicker({
        // format: 'yyyy-mm-dd hh:ii'
    });

    CKEDITOR.replace('summary-ckeditor');
</script>
<script type="text/javascript">

    var maincms = {};
    var statusadd = true;
    var statusdetail = false;
    var statusaddtab = [];
    var editid = '';
    var nextnoid = '';
    var nextnoidtab = [];
    var nownoid = '';
    var nownoidtab = '';
    var nowmaintabletab = '';
    var fieldnametab = new Array();
    var tablelookuptab = new Array();
    var input_data = new Array();
    var mainonchange = new Array();
    var maintable = new Array();
    var values = $("input[name='panel[]']").map(function(){return $(this).val();}).get();
    var values2 = $("#slider").val();
    var values3 = $("#tabel1").val();
    var select_all = false;
    var select_all_tab = new Array();
    var cursor_top = 0;
    var selecteds = new Array();
    var another_selecteds = new Array();
    // var divform = '';
    var my_table = '';
    var mytable = '';
    var mytabtable = new Array();
    var groupbtn = '';
    var selected_filter = {
        operand: [],
        fieldname: [],
        operator: [],
        value: [],
    };
    var filter_tabs = new Array();
    var selected_filter_tabs = 0;
    var filtercontenttabs = {
        filtercontentgroup: new Array(),
        filtercontentbody: new Array(),
        filtercontentrow: new Array(),
    }
    var colsorted = [0,1];
    var roles = {
        fieldname: new Array(),
        newhidden: new Array(),
        edithidden: new Array(),
    };
    var cmsfilterbutton = new Array()
    var tab_datatable = new Array()

    var functionfooter = new Array();
    var functionfootertab = new Array();
    var searchtablewidth = 30;
    var tab_searchtablewidth = new Array();
    var statussearch = '';
    var tab_statussearch = new Array();
    var valuesearch = '';
    var tab_valuesearch = new Array();

    var maincms_widget = new Array();
    var maincms_widgetgrid = new Array();

    var noids = new Array();
    var noids_tab = new Array();

    function selectAllCheckbox() {
        var checkboxes = $('[name="checkbox[]"]');
        checkboxes.prop('checked', $('#select_all').is(':checked'));

        if (select_all == false) {
            select_all = true;
            $('.odd').css('background-color', '#90CAF9');
            $('.even').css('background-color', '#90CAF9');
            $('.checkbox').val('TRUE');
            selecteds = noids;
        } else {
            select_all = false;
            $('.odd').css('background-color', '#ffffff');
            $('.even').css('background-color', '#BBDEFB');
            $('.checkbox').val('FALSE');
            selecteds = new Array();
        }
    }

    function selectAllCheckboxTab(panel_noid) {
        var checkboxes = $('[name="checkbox-'+panel_noid+'[]"]');
        checkboxes.prop('checked', $('#select_all_'+panel_noid).is(':checked'));

        if (select_all_tab[panel_noid] == false) {
            select_all_tab[panel_noid] = true;
            $('#tab-table-'+panel_noid+' .odd').css('background-color', '#90CAF9');
            $('#tab-table-'+panel_noid+' .even').css('background-color', '#90CAF9');
            $('#tab-table-'+panel_noid+' .checkbox-'+panel_noid).val('TRUE');
            another_selecteds[panel_noid] = noids_tab[panel_noid];
        } else {
            select_all_tab[panel_noid] = false;
            $('#tab-table-'+panel_noid+' .odd').css('background-color', '#ffffff');
            $('#tab-table-'+panel_noid+' .even').css('background-color', '#BBDEFB');
            $('#tab-table-'+panel_noid+' .checkbox-'+panel_noid).val('FALSE');
            another_selecteds[panel_noid] = new Array();
        }
        console.log('another_selecteds')
        console.log(another_selecteds)
    }
    
    function changeCheckbox(index, id) {
        var checkbox = $('.checkbox'+index);
        if (checkbox.val() == 'FALSE') {
            $('.row'+index).css('background-color', '#90CAF9');
            $('.checkbox'+index).val('TRUE');
            selecteds.push(id);
        } else {
            if (index % 2 == 0) {
                $('.row'+index).css('background-color', '#ffffff');
            } else {
                $('.row'+index).css('background-color', '#BBDEFB');
            }
            $('.checkbox'+index).val('FALSE')
            var replace_selecteds = new Array();
            $.each(selecteds, function(k, v) {
                if (v != id) {
                    replace_selecteds.push(v)
                }
            })
            selecteds = replace_selecteds
        }
        select_all = false;
        $('#select_all').prop('checked', false);
        // console.log(selecteds)
    }

    function changeCheckboxTab(panel_noid, index, id) {
        var checkbox = $('.checkbox-'+panel_noid+'-'+index);
        // alert(checkbox.val())
        if (checkbox.val() == 'FALSE') {
            $('#tab-table-'+panel_noid+' .row'+index).css('background-color', '#90CAF9');
            $('#tab-table-'+panel_noid+' .checkbox-'+panel_noid+'-'+index).val('TRUE');
            another_selecteds[panel_noid].push(id);
        } else {
            if (index % 2 == 0) {
                $('#tab-table-'+panel_noid+' .row'+index).css('background-color', '#ffffff');
            } else {
                $('#tab-table-'+panel_noid+' .row'+index).css('background-color', '#BBDEFB');
            }
            $('#tab-table-'+panel_noid+' .checkbox-'+panel_noid+'-'+index).val('FALSE')
            // another_selecteds.pop(id);
            // delete another_selecteds[index]
            var replace_another_selecteds = new Array();
            $.each(another_selecteds[panel_noid], function(k, v) {
                if (v != id) {
                    replace_another_selecteds.push(v)
                }
            })
            another_selecteds[panel_noid] = replace_another_selecteds
        }
        select_all_tab[panel_noid] = false;
        $('#select_all_'+panel_noid).prop('checked', false);
        console.log('another_selecteds')
        console.log(another_selecteds)
    }

    function showButtonAction(element, index) {
        var rect = element.getBoundingClientRect();

        this.resetCss()

        // $('.dataTable tbody td').css('height', '100px !important')
        // $('.dataTable tbody td:nth-child('+index+')').css('background', 'black')
        $('.my_row').css('height', <?= $heightwgf ?>)
        $('.button-action-'+index).css('position', 'fixed')
        $('.button-action-'+index).css('top', parseInt(rect.y+(5)))
        $('.button-action-'+index).css('right', '68px')
        // $('.button-action-'+index).css('overflow', 'hidden')

        // console.log(parseInt(rect.y+8))
        // console.log('Element is ' + offset + ' vertical pixels from <body>');
    }

    function showButtonActionTab(element, index) {
        var rect = element.getBoundingClientRect();

        this.resetCssTab()

        $('.button-action-tab-'+index).css('position', 'fixed')
        $('.button-action-tab-'+index).css('top', parseInt(rect.y+5))
        $('.button-action-tab-'+index).css('right', '68px')
        // $('.button-action-'+index).css('overflow', 'hidden')

        // console.log(parseInt(rect.y+8))
        // console.log('Element is ' + offset + ' vertical pixels from <body>');
    }

    function resetCss() {
        $('.button-action').removeAttr('style')
    }

    function resetCssTab() {
        $('.button-action-tab').removeAttr('style')
    }

    function mouseoverAddData(element_id) {
        $('#'+element_id).css('background', '#337AB7')
    }

    function mouseoutAddData(element_id) {
        $('#'+element_id).css('background', '#3598dc')
    }
    
    $(document).ready(function() {
        getData()
        $.each(maincms['widget'], function(k, v) {
            maincms_widget[v.noid] = v;
        })
        $.each(maincms['widgetgrid'], function(k, v) {
            maincms_widgetgrid[v.idcmswidget] = v;
        })
        getCmsThead()
        getCmsColumns()
        $('#example_baru').attr('data-page-length', maincms['widgetgrid'][0]['recperpage']);
        $('#widgeticon').addClass(maincms['widget'][0]['widgeticon']);
        $('#widgetcaption').html(maincms['widget'][0]['widgetcaption']);
        $('#judul_excel').attr('value', maincms['widget'][0]['widgetcaption']);
        $('#tabel1').attr('value', maincms['menu']['noid']);
        $('#namatable').attr('value', maincms['menu']['noid']);
        $('#url_code').attr('value', maincms['menu']['noid']);
        if (!maincms['widgetgrid'][0]['panelfilter']) {
            $('#space_filter_button').hide()
            $('#tool_filter').hide()
        }
        // console.log(maincms['widgetgrid']['widgeticon'])
        $('#another-card').hide();
        $("#from-departement").hide();
        $("#tool-kedua").hide();
        $("#tool-ketiga").hide();
        $("#divsucces").hide();
        $('.datepicker').datepicker({
            format: 'd-m-yyyy'
            // startDate: '-3d'
        });
        $('.datepicker2').datepicker({
            format: 'd-m-yyyy'
            // startDate: '-3d'
        });
        // $(".first.paginate_button, .last.paginate_button").hide();

        getChart(values);
        // getSlider(values2);
        // tabel(values3);
        // getDT(values3);
        // getForm()
        // getDatatable();
        getFilterButton(values3)

        function getChart(id) {
            $.each(id, function (key, val) {
                $.ajax({
                    type: "POST",
                    header:{
                        'X-CSRF-TOKEN':$('#token').val()
                    },
                    url: "/ambilchart",
                    dataType: "json",
                    data:{
                        "_token":$('#token').val(),
                        "id_cart": val
                    },
                    success: function (response) {
                        if (response.defaultchart == 'column') {
                            chartBatang(response,val);
                        } else if (response.defaultchart == 'pie') {
                            chartPie(response,val);
                        } else {
                            chartBatang(response,val);
                        }
                    },
                });
            });
        }

        function getCmsColumns() {
            var theadcolumns = '';
            var colspanfooter = 2;
            var index = 1;
            if (maincms['widgetgrid'][0]['colcheckbox']) {             
                colspanfooter = 3;
                index = 2;
                // theadcolumns += '<th></th>'
                theadcolumns += `<th style="width: 30px"><input type="checkbox" id="select_all" onchange="selectAllCheckbox()">&nbsp;&nbsp;&nbsp;&nbsp;</th>`;
            }
            theadcolumns += `<th style="width: 30px">No&nbsp;&nbsp;&nbsp;</th>`;
            var tfootcolumns = ``
            // console.log(maincms['widgetgridfield'])
            $.each(maincms['widgetgridfield'][0], function(k, v) {
                if (v.colwidth != 0) {
                    var colwidth = v.colwidth+'px';
                } else {
                    var colwidth = 'auto';
                }
                if (v.colshow == 1) {
                    theadcolumns += `<th class="text-`+v.colheaderalign.toLowerCase()+`" style="width: `+colwidth+`; height: `+v.colheight+`px;">`+v.fieldcaption+`</th>`
                    // theadcolumns += `<th>`+v.fieldcaption+`</th>`
                    // tfootcolumns += `<th></th>`
                    // var dataconfig = JSON.stringify(v.colsummaryfield)['dataconfig'];

                    if (v.colsummaryfield) {
                        var dataconfig = JSON.parse(v.colsummaryfield).dataconfig[0].summarytype+'('+index+')'
                        functionfooter.push({
                            "index": index,
                            "function": dataconfig,
                        })
                    } else {
                        var dataconfig = '';
                    }

                    if (v.colsummaryfield) {
                        tfootcolumns += `<th class="text-right" colspan="`+colspanfooter+`" data-function="`+dataconfig+`" style="border: 1px solid #95c9ed"></th>`
                        // console.log('collll '+colspanfooter)
                        colspanfooter = 1
                    } else {
                        colspanfooter++
                    }
                    index++
                }
                if (maincms['widgetgridfield'][0].length == k+1) {
                    // console.log('terakhir '+colspanfooter)
                    tfootcolumns += `<th class="text-right" colspan="`+(colspanfooter)+`" style="border: 1px solid #95c9ed"></th>`
                }
            })
            theadcolumns += `<th>Action</th>`
            console.log('tfootcolumns')
            console.log(tfootcolumns)
            $('#thead-columns').html(theadcolumns)
            $('#thead-columns-d').html(theadcolumns)
            $('#tfoot-columns').html(tfootcolumns)
            $('#tfoot-columns-d').html(tfootcolumns)
        }

        

        // console.log(my_table.context[0].jqXHR.responseJSON);
        $('#example_baru_filter input').unbind();

        // console.log(my_table.search($('#example_baru_filter input').val()).draw());
        // my_table.search('tes').draw();
        $('#example_baru_filter input').bind('keyup', function (e) {
            if (e.keyCode == 13) {
                my_table.search($('#example_baru_filter input').val()).draw();
            }
        });

        $('#formdepartement').on('keyup keypress', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });

        // infoDatatable()
        $(".container").css('display', 'block');

        $.each(maincms['widget'], function(k, v) {
            maintable[v.noid] = v.maintable;
        })
    });

    function getCmsFilterButton(noid) {
        var buttonfilter = new Array();

        if (noid) {
            var each = JSON.parse(maincms_widget[noid]['configwidget'])['WidgetFilter'];
            var configwidget = JSON.parse(maincms_widget[noid]['configwidget']);
            var vnoid = noid;
        } else {
            var each = JSON.parse(maincms['widget'][0]['configwidget'])['WidgetFilter'];
            var configwidget = JSON.parse(maincms['widget'][0]['configwidget']);
            var vnoid = maincms['widget'][0]['noid'];
        }
        
        $.each(each, function(k, v) {
            // buttonfilter[k] = new Array();
            buttonfilter[k] = {
                'groupcaption' : v.groupcaption,
                'groupcaptionshow' : v.groupcaptionshow,
                'groupdropdown' : v.groupdropdown,
                'data' : new Array()
            }
            
            $.each(v.button, function(k2, v2) {
                var icon = '';
                if (v2.classicon != '') {
                    icon = '<i class="fa '+v2.classicon+'"></i> ';
                }
                var pecah = v2.condition[0].querywhere[0].fieldname.split('.');
                var buttoncaption = v2.buttoncaption.split(' ');
                var uniqeidbutton = '';
                if (buttoncaption.length > 1) {
                    $.each(buttoncaption, function(k3, v3) {
                        uniqeidbutton += v3
                    })
                } else {
                    uniqeidbutton = v2.buttoncaption
                }
                var oncl = 'onclick="filterData(\'btn-filter-'+uniqeidbutton+'-'+vnoid+'\', \'btn-filter-'+k+'-'+vnoid+'\', '+k+', \''+v2.buttoncaption+'\', \''+v2.classcolor+'\', '+v.groupdropdown+', false, \''+maincms['widget'][0]['maintable']+'\', \''+v2.condition[0].querywhere[0].operand+'\', \''+pecah[1]+'\', \''+v2.condition[0].querywhere[0].operator+'\', \''+v2.condition[0].querywhere[0].value+'\')" ';
                var button = '<button class="btn btn-outline-secondary btn-sm flat btn-filter-'+k+'-'+vnoid+'" id="btn-filter-'+uniqeidbutton+'-'+vnoid+'" style="background-color:#E3F2FD; color: '+v2.classcolor+'"'+oncl+'>'+icon+v2.buttoncaption+' </button>';
                if (v.groupdropdown == 1) {
                    button = '<button class="flat dropdown-item" id="btn-filter-'+uniqeidbutton+'-'+vnoid+'" '+oncl+'>'+icon+v2.buttoncaption+' </button>';
                }
                buttonfilter[k].data[k2] = {
                    'nama' : v2.buttoncaption,
                    'buttonactive' : v2.buttonactive,
                    'taga' : button,
                    'operand' : v2.condition[0].querywhere[0].operand,
                    'fieldname' : pecah[1],
                    'operator' : v2.condition[0].querywhere[0].operator,
                    'value' : v2.condition[0].querywhere[0].value,
                };
            })
        })

        
        var json_data = {
            "filterButton" : buttonfilter,
            "configWidget" : configwidget,
        };
        
        // console.log('test')
        $.each(json_data.filterButton, function(k, v) {
            $.each(v.data, function(k2, v2) {
                if (v2 != undefined) {
                    // console.log(v2.buttonactive)  
                }
            })
        })

        cmsfilterbutton = json_data;
        // return json_data;
    }

    function getFilterButton(values3, noid) {
        // $.ajax({
        //     type: "POST",
        //     header:{
        //         'X-CSRF-TOKEN':$('#token').val()
        //     },
        //     url: "/get_filter_button_tmd",
        //     dataType: "json",
        //     data:{
        //         "_token":$('#token').val(),
        //         table: maincms['menu']['noid'],
        //         maincms: JSON.stringify(maincms)
        //     },
        //     success: function(response) {
                getCmsFilterButton(noid)
                console.log('cmsfilterbutton')
                console.log(cmsfilterbutton)
                var response = cmsfilterbutton;

                if (noid) {
                    var vnoid = noid;
                } else {
                    var vnoid = maincms['widget'][0]['noid'];
                }
                // if (response == response2) {
                //     console.log('COCOKKKKK')
                // }
                // console.log(response)
                // console.log(response2)
                // return false

                var buttonactive = new Array();
                var filter_button = '';
                var groupdropdown = new Array();
                $.each(response.filterButton, function(k, v) {
                    // console.log(v.groupdropdown)
                    buttonactive.push('all-'+k+'-'+vnoid);
                    if (v.groupdropdown == 1) {
                        // alert(1)
                        filter_button += `<button class="btn btn-outline-secondary btn-sm flat btn-filter-all btn-filter-`+k+`-`+vnoid+`" id="btn-filter-all-`+k+`-`+vnoid+`" style="background-color:#90CAF9; font-weight: bold; color: black;">`+v.groupcaption+`</button>`;
                        filter_button += `<div class="dropdown btn-group" style="padding-right: 5px">`
                    } else {
                        // alert(0)
                        filter_button += `<button class="btn btn-outline-secondary btn-sm flat btn-filter-all btn-filter-`+k+`-`+vnoid+`" id="btn-filter-all-`+k+`-`+vnoid+`" style="background-color:#E3F2FD;" onclick="filterData('btn-filter-all-`+k+`-`+vnoid+`', 'btn-filter-`+k+`-`+vnoid+`', `+k+`, 'ALL', 'grey', 0, true)">ALL</button>`;
                    }
                    selected_filter.operand.push('')
                    selected_filter.fieldname.push('')
                    selected_filter.operator.push('')
                    selected_filter.value.push('')

                    $.each(v.data, function(k2, v2) {
                        if (v2 != undefined) {
                            if (v.groupdropdown == 1) {
                                if (k2 == 0) {
                                    if (v2.buttonactive) {
                                        buttonactive[k] = v2.nama;
                                        selected_filter.operand[k] = v2.operand;
                                        selected_filter.fieldname[k] = v2.fieldname;
                                        selected_filter.operator[k] = v2.operator;
                                        selected_filter.value[k] = v2.value;
                                        filter_button += `<button class="btn btn-secondary btn-sm flat dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <span id="filter-dropdown-active-`+k+`-`+vnoid+`">`+v2.nama+`</span>
                                                        </button>`;
                                    } else {
                                        filter_button += `<button class="btn btn-secondary btn-sm flat dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <span id="filter-dropdown-active-`+k+`-`+vnoid+`">ALL</span>
                                                        </button>`;
                                    }
                                    filter_button += `<div class="dropdown-menu flat" aria-labelledby="dropdownMenuButton">
                                                        <button class="dropdown-item btn-filter-all btn-filter-`+k+`-`+vnoid+`" id="btn-filter-all-`+k+`-`+vnoid+`"  onclick="filterData('btn-filter-all-`+k+`-`+vnoid+`', 'btn-filter-`+k+`-`+vnoid+`', `+k+`, 'ALL', 'grey', 1, true)">ALL</button>`;
                                }
                                groupdropdown.push(true);
                            } else {
                                if (v2.buttonactive) {
                                    buttonactive[k] = v2.nama;
                                    selected_filter.operand[k] = v2.operand;
                                    selected_filter.fieldname[k] = v2.fieldname;
                                    selected_filter.operator[k] = v2.operator;
                                    selected_filter.value[k] = v2.value;
                                }
                                groupdropdown.push(false);
                            }
                            filter_button += v2.taga;
                        }
                    })
                    
                    if (v.groupdropdown == 1) {
                        filter_button += `</div>
                                        </div>`;
                    }
                    filter_button += `<div class="btn" style="padding: 0px; width: 5px"></div>`
                })
                
                if (filter_button.length > 0) {
                    if (noid) {
                        $('#space_filter_button-'+noid).html(filter_button)
                    } else {
                        $('#space_filter_button').html(filter_button)
                    }
                    // $('#space_filter_button').css('margin-bottom', '20px')
                }
                
                console.log('buttonactive')
                console.log(buttonactive)
                console.log(groupdropdown)
                $.each(buttonactive, function(k, v) {
                    if (groupdropdown[k] == false) {
                        // alert('#btn-filter-'+buttonactive[k]+'-'+vnoid)
                        $('#btn-filter-'+buttonactive[k]).css('background', '#90CAF9');
                        $('#btn-filter-'+buttonactive[k]).css('color', 'black');
                        $('#btn-filter-'+buttonactive[k]).css('font-weight', 'bold');
                    }
                    // console.log(groupdropdown[k])
                    // console.log(buttonactive[k])
                })

                if (buttonactive.length > 0) {
                    destroyDatatable()
                    getDatatable()
                }
                
                
                //Datatable filter
                $.each(response.configWidget.WidgetFilter, function(k, v) {
                    var filtercontentgroup = '';
                    var filtercontentbody = '';

                    var classcolor = 'default';
                    var textcolor = 'text-dark';
                    var display = 'none';
                    if (k == 0) {
                        classcolor = 'purple';
                        textcolor = 'text-white'
                        display = 'block';
                        filter_tabs.push(true)
                    } else {
                        filter_tabs.push(false)
                    }
                    filtercontentgroup += `<input type="hidden" name="tabs[]" value="`+k+`">
                                            <div style="margin:5px 5px 0 0;" id="filter-content-group-`+k+`" onclick="changeTabFilter(`+k+`)" class="filter-content-group formbtn btn default `+classcolor+`">
                                                <span class="`+textcolor+`">
                                                    <i class="fa fa-indent `+textcolor+`"></i> `+k+`
                                                </span>
                                            </div>`;
                    filtercontenttabs.filtercontentgroup.push(filtercontentgroup)

                    var groupcaptionshow = `<option value="1">True</option>
                                            <option value="0" selected>False</option>`;
                    if (v.groupcaptionshow) {
                        groupcaptionshow = `<option value="1" selected>True</option>
                                            <option value="0">False</option>`
                    }

                    var groupdropdown = '';
                    if (v.groupdropdown == -1) {
                        groupdropdown = `<option value="-1" selected>Auto</option>
                                            <option value="0">False</option>
                                            <option value="1">True</option>`
                    } else if (v.groupdropdown == 0) {
                        groupdropdown = `<option value="-1">Auto</option>
                                            <option value="0" selected>False</option>
                                            <option value="1">True</option>`
                    } else if (v.groupdropdown == 1) {
                        groupdropdown = `<option value="-1">Auto</option>
                                            <option value="0">False</option>
                                            <option value="1" selected>True</option>`
                    }
                    filtercontentbody += `<div style="display: `+display+`" id="filter-content-body-`+k+`" class="filter-content-body formgroup col-md-12">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Group Caption</label>
                                                        <input name="groupcaption[]" id="groupcaption-`+k+`" class="form-control input-sm" value="`+v.groupcaption+`" placeholder="Group Caption">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Group Caption Showed</label>
                                                        <select name="groupcaptionshow[]" id="groupcaptionshow-`+k+`" class="form-control input-sm">
                                                            `+groupcaptionshow+`
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Group Dropdown Showed</label>
                                                        <select name="groupdropdown[]" id="groupdropdown-`+k+`" class="form-control input-sm">
                                                            `+groupdropdown+`
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="filter-content-row-`+k+`">
                                            </div>
                                            </div>`
                    filtercontenttabs.filtercontentbody.push(filtercontentbody)

                    filtercontenttabs.filtercontentrow[k] = new Array()
                    var _k2 = 0;
                    $.each(v.button, function(k2, v2) {
                        var filtercontentrow = '';

                        var condition = '';
                        $.each(v2.condition, function(k3, v3) {
                            condition += v3.groupoperand+'#'
                            $.each(v3.querywhere, function(k4, v4) {
                                condition += v4.fieldname+';'
                                condition += v4.operator+';'
                                condition += v4.value+';'
                                condition += v4.operand
                            })
                        })

                        var buttonactive = `<option value="1">True</option>
                                            <option value="0" selected>False</option>`
                        if (v2.buttonactive) {
                            buttonactive += `<option value="1" selected>True</option>
                                                <option value="0">False</option>`
                        }

                        filtercontentrow += `<input type="hidden" name="tabsrow[]" value="`+k+`">
                                            <div class="row filter-content-row-`+k+`">
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Caption</label>
                                                        <input class="form-control input-sm" name="buttoncaption[]" id="buttoncaption-`+k+`-`+_k2+`" value="`+v2.buttoncaption+`" placeholder="Caption">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Condition</label>
                                                        <input class="form-control input-sm" name="condition[]" id="condition-`+k+`-`+_k2+`" value="`+condition+`" placeholder="groupoperand#fieldname;operator;value;operand">
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Active</label>
                                                        <select class="form-control input-sm" name="buttonactive[]" id="buttonactive-`+k+`-`+_k2+`" value="1">
                                                            `+buttonactive+`
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Icon</label>
                                                        <input class="form-control input-sm" name="classicon[]" id="classicon-`+k+`-`+_k2+`" value="`+v2.classicon+`" placeholder="Icon">
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Color</label>
                                                        <input class="form-control input-sm" name="classcolor[]" id="classcolor-`+k+`-`+_k2+`" value="`+v2.classcolor+`" placeholder="Color">
                                                    </div>
                                                </div>
                                                <div class="col-md-1" style="text-align:center;padding:21px 0 0 0">
                                                    <div class="form-group">
                                                        <button id="del" onclick="delRow(`+_k2+`)" type="button" class="btn btn-sm red" style="height: 27px; width: 38px">
                                                            <i class="fa fa-minus-circle text-white"></i>
                                                        </button>
                                                        <button id="add" onclick="addRow()" type="button" class="btn btn-sm green" style="height: 27px; width: 38px">
                                                            <i class="fa fa-plus text-white"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>`
                        filtercontenttabs.filtercontentrow[k].push(filtercontentrow)
                        _k2++;
                    })
                    // filtercontentbody += `</div>
                    //                     </div>`
                })

                //Set to HTML
                var html_filtercontentgroup = '';
                var html_filtercontentbody = '';
                $.each(filtercontenttabs.filtercontentgroup, function(k, v) {
                    html_filtercontentgroup += v
                    html_filtercontentbody += filtercontenttabs.filtercontentbody[k]
                })
                $('#filtercontentgroup').html(html_filtercontentgroup);
                $('#filtercontentbody').html(html_filtercontentbody);

                $.each(filtercontenttabs.filtercontentrow, function(k, v) {
                    $('#filter-content-row-'+k).html(v);
                })
                //End Set to HTML

                // console.log(html_filtercontentrow)
                // console.log(filtercontenttabs)
            // },
        // });
    }

    // function addCommas(nStr) {
    //     nStr += '';
    //     var x = nStr.split('.');
    //     var x1 = x[0];
    //     var x2 = x.length > 1 ? '.' + x[1] : '';
    //     var rgx = /(\d+)(\d{3})/;
    //     while (rgx.test(x1)) {
    //         x1 = x1.replace(rgx, '$1' + ',' + '$2');
    //     }
    //     return x1 + x2;
    // }

    function getCmsTabColumns() {
        $.each(maincms['widget'], function(k, v) {
            if (k > 0) {
                var theadcolumns = `<th><input type="checkbox" id="select_all" onchange="selectAllCheckboxTab()"></th>
                                <th>No</th>`;
                var tfootcolumns = `<th></th>
                                    <th></th>`
                // console.log(maincms['widgetgridfield'])
                $.each(maincms['widgetgridfield'][k], function(k2, v2) {
                    if (v2.colshow == 1) {
                        theadcolumns += `<th class="text-`+v2.colheaderalign+`" style="width: `+v2.colwidth+`px; height: `+v2.colheight+`px;">`+v2.fieldcaption+`</th>`
                        tfootcolumns += `<th></th>`
                    }
                })
                theadcolumns += `<th>Action</th>`
                tfootcolumns += `<th></th>`
                $('#tab-thead-columns-'+v.maintable).html(theadcolumns)
                $('#tab-tfoot-columns'+v.maintable).html(tfootcolumns)
            }
        })
    }

    function getForm(isadd, widget_noid, element_id, responsefind=null) {
        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            url: "/get_form_tmd",
            dataType: "json",
            data:{
                "_token":$('#token').val(),
                table: maincms['menu']['noid'],
                maincms: JSON.stringify(maincms),
                isadd: isadd,
                key: 0,
                widget_noid: widget_noid,
            },
            beforeSend: function () {
                $('#'+element_id).LoadingOverlay("show");
            },
            success: function (response) {
                // console.log(response);
                var divform = ''
                divform += '<input type="hidden" id="noid" name="noid" value="">';
                // var colgroupname = [
                //     0: 'Administrasi'
                // ];
                // var colgroupname_data = [
                //     0: [
                //         //,
                //         //,
                //         //
                //     ]
                // ];
                // var colgroupname = new Array();
                // var colgroupname_data = new Array();
                $.each(response.field, function(key, value) {
                    // roles.fieldname.push(value.fieldname);
                    // roles.newhidden.push(value.newhidden);
                    // roles.edithidden.push(value.edithidden);

                    // if (value.colgroupname) {
                    //     if (colgroupname.indexOf(value.colgroupname) === -1) {
                    //         colgroupname.push(value.colgroupname);
                    //     }
                    //     colgroupname_data.push(value.field);
                    // } else {
                    //     // alert(key)
                        if (value.fieldname != 'idmaster') {
                            divform += value.field;
                        }
                    // }

                    // if (value.colsorted == 0) {
                    //     colsorted.push(key+1);
                    // }
                    
                });
                // colsorted[colsorted.length] = roles.fieldname.length+2;
                // colsorted.push(roles.fieldname.length+2);
                
                var divgroup = '';
                $.each(response.formgroupname, function(k, v) {
                    var formgroupname = k.split('-')

                    divgroup += `<div class="col-md-12">
                                    <div class="sub-card-custom">
                                        <div class="card-header card-header_ bg-main-1 d-flex">
                                            <div class="text-white" style="font-size: 14px">`+formgroupname[1].split(';')[1]+`</div>
                                        </div>
                                        <div class="card-body card-body-custom card-body-custom-group flat">
                                            <div class="right-tabs-group">`;
                                           
                    var key2 = 0;
                    var tab_widget = `<ul class="nav nav-tabs" id="tab-widget-`+k+`" role="tablist">`;
                    var tab_widget_content = `<div class="tab-content" id="tab-widget-content-`+k+`">`;
                    $.each(v, function(k2, v2) {
                        var active = '';
                        if (key2 == 0) {
                            active = 'active';
                        }

                        tab_widget += `<li class="nav-item">
                                        <a class="nav-link nav-link-custom flat `+active+`" id="tab-`+k2+`" data-toggle="tab" href="#`+k2+`" role="tab" aria-controls="`+k2+`"
                                        aria-selected="true" onclick="setTabActive(`+k2+`, '#tab-widget-`+k+` li a', '#tab-widget-`+k+` li #tab-`+k2+`', '#tab-widget-content-`+k+` .tab-widget-content-`+k+`', '#tab-widget-content-`+k+` #`+k2+`')">`+k2+`</a>
                                    </li>`;

                        tab_widget_content += `<div class="tab-pane tab-widget-content-`+k+` fade show `+active+`" id="`+k2+`" role="tabpanel" aria-labelledby="`+k2+`-tab">
                                                    <div class="row">`;
                        $.each(v2, function(k3, v3) {
                            tab_widget_content += v3;
                        })
                        tab_widget_content += `     </div>
                                                </div>`;
                        key2++;
                    });
                    tab_widget += `</ul>`;
                    tab_widget_content += `</div>`;
                    divgroup += tab_widget;
                    divgroup += tab_widget_content;

                    divgroup +=             `</div>
                                        </div>
                                    </div>
                                </div>`;
                });
                divform += divgroup;
                // console.log(colgroupname);
                // console.log(colgroupname_data);

                $('#'+element_id).html(divform);

                if (statusadd) {
                    $('#'+element_id).LoadingOverlay("hide");
                    // $('#another-card').show();
                } else {
                    if (element_id == 'divform') {
                        // console.log(widget_noid)
                        // console.log(divform)
                        
                        // $.ajax({
                        //     url: '/find_tmd',
                        //     type: "POST",
                        //     dataType: 'json',
                        //     header:{
                        //         'X-CSRF-TOKEN':$('#token').val()
                        //     },
                        //     data: {
                        //         "id" : nownoid,
                        //         "namatable" : $("#namatable").val(),
                        //         "_token": $('#token').val(),
                        //         maincms: JSON.stringify(maincms)
                        //     },
                        //     success: function (response) {
                                // alert(response.length);return false;
                                if (responsefind != null) {
                                    nownoid = responsefind.nownoid;

                                    var maincms_panel = new Array();
                                    $.each(maincms['panel'], function(k, v) {
                                        $.each(maincms['widget'], function(k2, v2) {
                                            if (v.idwidget == v2.noid) {
                                                maincms_panel[v.noid] = v2;
                                            }
                                        })
                                    })

                                    // console.log(responsefind.result_detail)
                                    var key = 0;
                                    $.each(responsefind.result_detail, function(k, v) {
                                        if (key == 0) {
                                            nownoidtab = maincms_panel[k].noid
                                        }
                                        // alert(k)
                                        // console.log(v)
                                        // localStorage.removeItem('input-data-'+k)
                                        localStorage.setItem('input-data-'+k, JSON.stringify(v))
                                        localStorage.setItem('tablename-'+k, maincms_panel[k].maintable)
                                        // console.log(v)
                                        key++;
                                    })
                                    // $('#noid').val(id)
                                    // console.log('responsefind')
                                    // console.log(responsefind)
                                    $.each(responsefind, function(k, v) {
                                        if (v.formtypefield == 11) {
                                            $("#"+v.tablename+'-'+v.field+' input').val(v.value);
                                        } else if (v.formtypefield == 31){
                                            if (v.value == '1') {
                                                $("#"+v.tablename+'-'+v.field).attr('checked', true);
                                            }
                                        } else {
                                            $("#"+v.tablename+'-'+v.field).val(v.value);
                                        }

                                        if (v.disabled) {
                                            $("#"+v.tablename+'-'+v.field).attr('disabled', 'disabled');
                                        }
                                        if (statusdetail) {
                                            $("#"+v.tablename+'-'+v.field).attr('disabled', 'disabled');
                                        }
                                        if (v.edithidden) {
                                            $("#div-"+v.field).css('display', 'none');
                                        }
                                    });
                                    $('#'+element_id).LoadingOverlay("hide");
                                    console.log(divform)
                                }
                            // }
                        // });
                    }
                }

                $.each(response.field, function(k, v) {
                    if (v.newhidden == 1) {
                        $('#div-'+v.fieldname).css('display', 'none');
                    }
                })    

                $.each(response.allfield, function(k, v) {
                    // if (v.tablename == 'fincashbankdetail' && v.fieldname == 'nominal') {
                    //     console.log('cekkkkkk')
                    //     console.log(JSON.parse(v.onchange))
                    // }
                    if (v.onchange) {
                        var onchange = JSON.parse(v.onchange);
                        $.each(onchange, function(k2, v2) {
                            if (v2.TriggerOn == 'OnChange') {
                                var variables = new Array();
                                $.each(v2.sourcefield, function(k3, v3) {
                                    variables.push(v3.alias)
                                })

                                $.each(v2.sourcefield, function(k3, v3) {
                                    localStorage.removeItem(v.tablename+'-'+v2.targetfield+'-$'+v3.alias);
                                    // localStorage.setItem('$'+v3.alias, 0);
                                    // alert(v3.fieldname)
                                    // if (v3.fieldname == 'nominal') {
                                    //     console.log('////////////////')
                                    //     console.log(v2.targetfield)
                                    // }
                                    if (v3.sourcetype == 'EditBox') {
                                        var value_onkeyup = document.querySelector('#'+v.tablename+'-'+v3.fieldname).getAttribute('onKeyUp');
                                        if (value_onkeyup == null) {
                                            value_onkeyup = ''
                                        }
                                        $('#'+v.tablename+'-'+v3.fieldname).attr('onKeyUp', value_onkeyup+`triggerOn(this, 'EditBox', '`+v.tablename+`', '`+JSON.stringify(variables)+`', '`+v2.throwtype+`', '`+v2.targettable+`', '`+v2.targetfield+`', '`+v3.alias+`', '`+v3.fieldname+`', '`+v3.fieldvalue+`', '`+v2.formula+`');`);
                                    } else {
                                        var value_onchange = document.querySelector('#'+v.tablename+'-'+v3.fieldname).getAttribute('onChange');
                                        if (value_onchange == null) {
                                            value_onchange = ''
                                        }
                                        $('#'+v.tablename+'-'+v3.fieldname).attr(v2.TriggerOn, value_onchange+`triggerOn(this, 'NotEditBox', '`+v.tablename+`', '`+JSON.stringify(variables)+`', '`+v2.throwtype+`', '`+v2.targettable+`', '`+v2.targetfield+`', '`+v3.alias+`', '`+v3.fieldname+`', '`+v3.fieldvalue+`', '`+v2.formula+`');`);
                                        $('#'+v.tablename+'-'+v3.fieldname).trigger('change')
                                        // console.log(v.tablename+'-'+v3.fieldname);
                                    }
                                })
                            } else if (v2.TriggerOn == 'OnSave') {
                                mainonchange.push(v2)
                            }
                        })
                    }

                    // if (v.newreadonly) {
                    //     $('#'+v.tablename+'-'+v.fieldname).
                    // }
                })    
                
                // console.log(mainonchange)

                // $.each(response.addonchange, function(k, v) {
                //     $('#'+v).attr('onChange', 'alert("add_onchange")')
                // })

                $('.datepicker').datepicker({
                    format: 'd-M-yyyy'
                    // startDate: '-3d'
                });

                //select2
                $('.select2').select2();
                $('.select2').css('width', '100%');
                $('.select2 .selection .select2-selection').css('height', '35px');
                $('.select2 .selection .select2-selection .select2-selection__rendered').css('margin-top', '-6px');
                $('.select2 .selection .select2-selection .select2-selection__arrow').css('margin-top', '4px');

                $.each(response.allfield, function(k, v) {
                    if (v.newreadonly) {
                        // alert('#select2-'+v.tablename+'-'+v.fieldname+'-container');
                        $('#div-'+v.tablename+'-'+v.fieldname+' span span .select2-selection--single').css('background-color', '#E5E5E5');
                    }
                })
                //select2readonly
                // $('.select2readonly').select2();
                // $('.select2readonly').css('width', '100%');
                // $('.currency').currency({region:'IDR'});
                // console.log(roles)  
                if (element_id != 'divform') {
                    $('#'+element_id).LoadingOverlay("hide");
                }
            },
        });
    }

    function triggerOn(context, sourcetype, tablename, variables, throwtype, targettable, targetfield, alias, fieldname, fieldvalue, formula) {
        // alert($('#idcurrency').val())
        // alert('#'+tablename+'-'+fieldname)
        var tt = tablename+'-'+targetfield;
        var tf = tablename+'-'+fieldname;
        
        if (sourcetype == 'EditBox') {
            localStorage.setItem(tt+'-$'+alias, $('#'+tf).val());
            // console.log(formula)
            // localStorage.removeItem('$'+alias);

            var replaceformula = formula;
            var f = formula.split('*');
            $.each(JSON.parse(variables), function(k, v) {
                var ls = localStorage.getItem(tt+'-$'+v);
                console.log('ls-'+tt+'-$'+v+' = '+localStorage.getItem(tt+'-$'+v))
                if (ls) {
                    // for (var i=0; i<10; i++) {
                        replaceformula = replaceformula.replace('$'+v, ls);
                        // console.log('ceking = '+ls)
                    // }
                } else {
                    replaceformula = replaceformula.replace('$'+v, 0);
                }
            })
            
            if (formula.split(' ')[0] != 'if') {
                $('#'+tt).val(eval(replaceformula))
            }
            // console.log('#'+tt)
            // console.log(replaceformula)
            // console.log(eval(replaceformula))
            // console.log(fieldname)
            // console.log(fieldvalue)
            // console.log(formula)
        } else {
            localStorage.setItem(tt+'-$'+alias, $('#'+tf+' :selected').val());
            // console.log('isi = '+$('#'+tf+' :selected').val())

            var getattribute = 'data-'+tf+'-'+$('#'+tf+' :selected').val();
            // console.log('testing = '+context.getAttribute(getattribute));
            // console.log('#'+tt)
            // alert(1)
            if (throwtype == 'FILTER') {
                $('#'+tt).attr('data-'+tf, $('#'+tf+' :selected').val())
                
                // console.log(context)
                // console.log('data-'+tt+'-querylookup')
                // console.log(context.getAttribute(getattribute))
                // console.log(document.querySelector('#'+tt))

                var frml = formula.split(';');
                var frml_where = '';
                $.each(frml, function(k, v) {
                    var _v = v.split(':');
                    if (_v[0] == 'WHERE') {
                        __v = _v[1].split(',');
                        frml_where += __v[0]+' '+__v[1]+' '+__v[2]+' '+__v[3]+' ';
                    }  
                })

                var querylookup = document.querySelector('#'+tt).getAttribute('data-'+tt+'-querylookup').split(';');
                var select = querylookup[0].split(':')[1];
                var from = querylookup[1].split(':')[1];
                var where = querylookup[2].split(':')[1].split('AND');
                var order = querylookup[3].split(':')[1].split(',');
        
                var query = 'SELECT '+select+' FROM '+from+' WHERE ';
                var add_frml = false;
                $.each(where, function(k, v) {
                    if (v) {
                        if (k > 0) {
                            query += 'AND ';
                            add_frml = true;
                        }
                        var _v = v.split(',');
                        query += _v[0]+' '+_v[1]+' '+_v[2]+' ';
                    }
                })
                if (add_frml) {
                    frml_where = frml_where.substr(4);
                }
                query += frml_where;
                
                query += ' ORDER BY '+order[0]+' '+order[1];
                query = query.replace('$'+alias, document.querySelector('#'+tt).getAttribute('data-'+tf))
                // console.log(frml_where.substr(4))

                getSelectChild(query, '#'+tt)

                // console.log('query = '+query)
                // console.log('filter = '+document.querySelector('#'+tt).getAttribute('data-'+tf))

            } else {
                $('#'+tt).val(context.getAttribute(getattribute))
            }
        }


        if (formula.split(' ')[0] == 'if') {
            var replaceformula = formula;
            $.each(JSON.parse(variables), function(k, v) {
                var ls = localStorage.getItem(tt+'-$'+v);
                console.log('ls-'+tt+'-$'+v+' = '+localStorage.getItem(tt+'-$'+v))
                if (ls) {
                    // for (var i=0; i<10; i++) {
                        replaceformula = replaceformula.replace('$'+v, ls);
                        // console.log('ceking = '+ls)
                    // }
                } else {
                    replaceformula = replaceformula.replace('$'+v, 0);
                }
            })
            eval("var fn = function(){ "+replaceformula+" }")
            // alert(replaceformula)
            // alert(fn())
            $('#'+tt).val(fn())
        }

        $('#'+tt).trigger('change')
        $('#'+tt).trigger('keyup')

        // console.log(targetfield)
        // console.log(context.getAttribute(getattribute))
        // console.log(context.getAttribute(getattribute))
        // if (targettable == 'self') {
        //     targettable = 'accmcurrency';
        //     $.ajax({
        //         type: "POST",
        //         header:{
        //             'X-CSRF-TOKEN':$('#token').val()
        //         },
        //         url: "/get_trigger_on_tmd",
        //         dataType: "json",
        //         data:{
        //             "_token":$('#token').val(),
        //             query: `SELECT `+fieldvalue+` AS `+alias+` FROM `+targettable+` WHERE noid='`+context.getAttribute(getattribute)+`' LIMIT 1`,
        //             alias: alias,
        //             formula: formula,
        //         },
        //         success: function(response) {
        //             $('#'+targetfield).val(response)
        //             console.log(response)
        //         }
        //     })
        // }
    }

    function getSelectChild(query, element_target) {
        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            url: "/get_select_child_tmd",
            data:{
                "_token":$('#token').val(),
                query: query,
            },
            success: function (response) {
                $(element_target).html(response)
            }
        })
    }

    function getFormTab() {
        var tab_form = '';
        var tab_form_content = '';
        $.each(maincms['widget'], function(k0, v0) {
            if (k0 > 0) {
                var active = '';
                if (k0 == 1) {
                    active = 'active';
                }

                $.ajax({
                    type: "POST",
                    header:{
                        'X-CSRF-TOKEN':$('#token').val()
                    },
                    url: "/get_form_tmd",
                    dataType: "json",
                    data:{
                        "_token":$('#token').val(),
                        table: maincms['menu']['noid'],
                        maincms: JSON.stringify(maincms),
                        isadd: true,
                        key: 1,
                    },
                    success: function (response) {
                        console.log('DDDDD');
                        console.log(response);
                        tab_form = `<li class="nav-item">
                                        <a class="nav-link flat `+active+`" id="tab-`+v0.maintable+`" data-toggle="tab" href="#`+v0.maintable+`" role="tab" aria-controls="`+v0.maintable+`"
                                        aria-selected="true" onclick="setTabActive(`+v0.maintable+`, '#tab-widget-`+v0.maintable+` li a', '#tab-widget-`+v0.maintable+` li #tab-`+v0.maintable+`', '#tab-widget-content-`+v0.maintable+` div', '#tab-widget-content-`+v0.maintable+` #`+v0.maintable+`')">`+k0+`</a>
                                    </li>`;
                        tab_form_content += `<form id="form-`+v0.maintable+`">
                                        <div class="row">`;
                        $.each(response.field, function(key, value) {
                            if (value.tablemaster != maincms['widget'][0]['maintable']) {
                                tab_form_content += value.field;
                            }
                        });
                        tab_form_content += `   </div>
                                    </form>`;

                        var tab_action = `<li class="nav-item" style="padding-top: 5px">
                                                <button class="btn btn-sm btn-primary"><i class="fa fa-plus"></i></button>
                                                <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                          </li>`;

                        $('#tab-form-'+v0.maintable).append(tab_form+tab_action);
                        $('#tab-form-content-'+v0.maintable).append(tab_form_content);

                        $('.datepicker').datepicker({
                            format: 'd-M-yyyy'
                        });
                    },
                });
            }
        });
    }

    function setTabActive(panel_noid, element_class, element_id, element_content_class, element_content_id) {
        // alert('setTabActive('+element_class+', '+element_id+', '+element_content_class+', '+element_content_id+')')
        nownoidtab = panel_noid
        another_selecteds = [];
        $(element_class).removeClass('active')
        $(element_id).addClass('active')
        $(element_content_class).removeClass('active')
        $(element_content_id).addClass('active')
        // alert(element_content_id)
    }

    function getCmsTab(statusadd) {
        var tab_widget = '';
        var tab_widget_content = '';
        // var maincms_widget = new Array();
        // $.each(maincms['widget'], function(k, v) {
        //     maincms_widget[v.noid] = v;
        // })
        var maincms_widgetgridfield = new Array();
        $.each(maincms['widgetgridfield'], function(k, v) {
            $.each(v, function(k2, v2) {
                maincms_widgetgridfield[v2.idcmswidgetgrid] = v;
            })
        })
        // console.log(maincms_widgetgridfield)
        $.each(maincms['panel'], function(k, v) {
        // console.log(maincms_widgetgridfield[v.idwidget].colshow)
            var v_widget = maincms_widget[v.idwidget];
            if (k > 0) {
                statusaddtab[v.noid] = true;

                var active = '';
                if (k == 1) {
                    active = 'active';
                }

                var tabcaption = v.tabcaption.split(';');

                tab_widget += `<li class="nav-item">
                                    <a class="nav-link nav-link-custom flat `+active+`" id="btn-tab-`+v.noid+`" data-toggle="tab" href="#`+v.noid+`" role="tab" aria-controls="`+v.noid+`"
                                    aria-selected="true" onclick="setTabActive(`+v.noid+`, '#tab-widget li a', '#tab-widget-`+v.noid+` li #btn-tab-`+v.noid+`', '#tab-widget-content .tab-widget-content', '#tab-widget-content-`+v.noid+`')">`+tabcaption[1]+`</a>
                                </li>`;

                tab_widget_content += `<div class="tab-pane tab-widget-content fade show `+active+`" id="tab-widget-content-`+v.noid+`" role="tabpanel" aria-labelledby="`+v.noid+`-tab">
                                            <div class="card card-custom flat">
                                                <div class="card-header card-header_ bg-main-1 d-flex flat" >
                                                    <div class="card-title">
                                                        <span class="card-icon">
                                                            <i class="`+v_widget.widgeticon+` text-white"></i>
                                                        </span>
                                                        <h3 class="card-label text-white">
                                                            `+v_widget.widgetcaption+`
                                                        </h3>
                                                    </div>`;

                // if (!statusadd) {
                    tab_widget_content += `         <div class="card-toolbar" >
                                                        <div id="add-data-`+v.noid+`">
                                                            <a onclick="addDataTab('`+v.noid+`')" href="javascript:;" id="btn-add-data-`+v.noid+`" style="color: #FFFFFF;margin-right: 5px;" class="btn btn-sm flat" onmouseover="mouseoverAddData('btn-add-data-`+v.noid+`')" onmouseout="mouseoutAddData('btn-add-data-`+v.noid+`')"><i style="color:#FFFFFF" class="fa fa fa-save"></i> Tambah Data</a>
                                                        </div>
                                                        <div id="save-data-`+v.noid+`" style="display: none">
                                                            <a onclick="saveDataTab(`+v.noid+`)" id="btn-save-data-`+v.noid+`" class="btn btn-sm btn-success flat" style="margin-right: 5px;">
                                                                <i class="fa fa-save"></i> Save
                                                            </a>
                                                        </div>
                                                        <div id="cancel-data-`+v.noid+`" style="display: none">
                                                            <a onclick="cancelDataTab('`+v.noid+`')" class="btn btn-sm btn-default flat" style="margin-right: 5px;">
                                                                <i class="fa fa-reply"></i> Cancel
                                                            </a>
                                                        </div>
                                                        <div id="back-data-`+v.noid+`" style="display: none">
                                                            <a id="btn-back-`+v.noid+`" class="btn btn-sm btn-success flat" onclick="cancelDataTab()"  style="margin-right: 5px;">
                                                                <i class="fa fa-reply"></i> Back
                                                            </a>
                                                        </div>
                                                        <div class="dropdown" style="margin-right: 5px;">
                                                            <a style="background-color: transparent !important; border: 1px solid #95c9ed; color: #aad4f0;" class="btn btn-default btn-sm flat" href="javascript:;" data-toggle="dropdown">
                                                                <i style="color:#FFFFFF" class="fa fa-cogs"></i>
                                                                    Tools
                                                                <i style="color:#FFFFFF" class="fa fa-angle-down"></i>
                                                            </a>
                                                            <ul style="" id="another-drop_export" class="dropdown-menu dropdown-menu-right">
                                                                <li id="another-space-button-export-`+v.noid+`"></li>
                                                            </ul>
                                                        </div>
                                                        <a style="background-image: none !important; margin-right: 5px; /* height: 27px; */ width: 27px !important; text-align: center; padding: 2px !important; border-radius: 50% !important; background-color: #2196F3; color: #FFF;" id="tool_refresh-`+v.noid+`" href="javascript:;" onclick="refresh(`+v.noid+`)" class="bg-main-1"><i style="color:#FFFFFF" class="fa fa-sync-alt"></i></a>
                                                        <a style="background-image: none !important; margin-right: 5px; /* height: 27px; */ width: 27px !important; text-align: center; padding: 2px !important; border-radius: 50% !important; background-color: #2196F3; color: #FFF;" id="tool_filter-`+v.noid+`" href="javascript:;" onclick="showModalFilter(`+v.noid+`)" class="bg-main-1"><i style="color:#FFFFFF" class="fa fa-filter"></i></a>
                                                        <a style="background-image: none !important; /* height: 27px; */ width: 27px !important; text-align: center; padding: 2px !important; border-radius: 50% !important; background-color: #2196F3; color: #FFF;" id="tool_refresh_up-`+v.noid+`" onClick="toggleButtonCollapse(`+v.noid+`)" href="javascript:;" class="bg-main-1"><i style="color:#FFFFFF" class="fa fa-sort"></i></a>
                                                    </div>`;
                // }

                         tab_widget_content += `</div>
                                                <div class="card-body card-body-tab-custom">
                                                    <div class="row" style="display: block">
                                                        <div id="space_filter_button-`+v.noid+`" style="margin-bottom: 10px">
                                                        </div>
                                                    </div>`
                             tab_widget_content += `<form data-toggle="validator" class="form" id="tab-form-`+v.noid+`" style="display: none">
                                                        <div class="row" id="tab-form-div-`+v.noid+`">
                                                        </div>
                                                    </form>`

                            // comment
                            //  tab_widget_content += `<table id="tab-table-`+v.noid+`" class="table table-striped table-bordered table-condensed nowrap" cellspacing="0" width="100%" style="background-color:#BBDEFB !important; border-collapse: collapse !important;">
                            //                             <thead>
                            //                                 <tr id="tab-thead-`+v.noid+`" class="bg-main">`

                            //             var colspan = 0;
                            //             $.each(maincms_widgetgridfield[v.idwidget], function(k2, v2) {
                            //                 if (v2.colshow == 1) {
                            //                     colspan++
                            //                 }
                            //             })
                            //             tab_widget_content += `<th style="text-align:center" colspan="2">#</th>`;
                            //             tab_widget_content += `<th style="text-align:center" colspan="`+colspan+`"></th>`;
                            //             tab_widget_content += `<th colspan="1" style="text-align:center">---</th>`;

                            //          tab_widget_content += `</tr>
                            //                                 <tr id="tab-thead-columns-`+v.noid+`">`

                                    
                            //             tab_widget_content += `<th><input type="checkbox" id="select_all_`+v.noid+`" onchange="selectAllCheckboxTab(`+v.noid+`)" class="checkbox-`+v.noid+` checkbox-`+v.noid+`-0" name="checkbox-`+v.noid+`[]" value="FALSE"></th>
                            //                                    <th>No</th>`;
                            //      $.each(maincms_widgetgridfield[v.idwidget], function(k2, v2) {
                            //          if (v2.colshow == 1) {
                            //              tab_widget_content += `<th class="text-`+v2.colheaderalign+`" style="width: `+v2.colwidth+`px; height: `+v2.colheight+`px;">`+v2.fieldcaption+`</th>`
                            //          }
                            //      })

                            //             tab_widget_content += `<th>Action</th>`

                            //          tab_widget_content += `</tr>
                            //                             </thead>
                            //                         </table>`
                            // endcomment


                            tab_widget_content += `<div id="tab-appendInfoDatatable-`+v.noid+`" style="border: 1px solid #3598dc;"></div>
                                                    <div id="tab-appendButtonDatatable-`+v.noid+`"></div>
                                                    <div id="ts-`+v.noid+`" class="table-scrollable" style="border: 1px solid #3598dc;">`
                             tab_widget_content += `<table bg-color="blue" class="nowrap table table-striped table-bordered table-hover  dataTable no-footer" id="tab-table-`+v.noid+`" style="background-color:#BBDEFB !important;"  aria-describedby="table_pagepanel_9921050101_info" role="grid">
                                                        <thead>
                                                            <tr id="tab-thead-`+v.noid+`" class="bg-main">`

                                        var colspan = 0;
                                        var colspanfooter = 0;
                                        var tfootcolumns = `<tfoot>
                                                                <tr>`
                                        var index = 2;
                                        var data_fft = new Array();
                                        $.each(maincms_widgetgridfield[v.idwidget], function(k2, v2) {
                                            if (v2.colshow == 1) {
                                                colspan++
                                                if (v2.colsummaryfield || v2.colsummaryfield != null) {
                                                    tfootcolumns += `<th class="text-right" colspan="`+colspanfooter+`" data-function="" style="border: 1px solid #95c9ed"></th>`
                                                    // console.log('collll '+colspanfooter)
                                                    colspanfooter = 1
                                                    var dataconfig = JSON.parse(v2.colsummaryfield).dataconfig[0].summarytype+'('+index+')'
                                                    data_fft.push({
                                                        "index": index,
                                                        "function": dataconfig,
                                                    })
                                                } else {
                                                    colspanfooter++
                                                }
                                                index++;
                                            }
                                            if (maincms_widgetgridfield[v.idwidget].length == k2+1) {
                                                // console.log('terakhir '+colspanfooter)
                                                tfootcolumns += `<th class="text-right" colspan="`+(colspanfooter)+`" style="border: 1px solid #95c9ed"></th>`
                                            }
                                        })
                                        
                                        functionfootertab[v.idwidget] = data_fft;
                                        
                                        tfootcolumns += `</tr>
                                                        </tfoot>`


                                        tab_widget_content += `<th style="text-align:center" colspan="2">#</th>`;
                                        tab_widget_content += `<th style="text-align:center" colspan="`+colspan+`"></th>`;
                                        tab_widget_content += `<th colspan="1" style="text-align:center">---</th>`;

                                     tab_widget_content += `</tr>
                                                            <tr id="tab-thead-columns-d-`+v.noid+`">`

                                    
                                        tab_widget_content += `<th style="width: 30px;"><input type="checkbox" id="select_all_`+v.noid+`" onchange="selectAllCheckboxTab(`+v.noid+`)" class="checkbox-`+v.noid+` checkbox-`+v.noid+`-0" name="checkbox-`+v.noid+`[]" value="FALSE"></th>
                                                               <th style="width: 30px;">No</th>`;
                                 $.each(maincms_widgetgridfield[v.idwidget], function(k2, v2) {
                                     if (v2.colshow == 1) {
                                         tab_widget_content += `<th class="text-`+v2.colheaderalign+`" style="width: `+v2.colwidth+`px; height: `+v2.colheight+`px;">`+v2.fieldcaption+`</th>`
                                     }
                                 })

                                        tab_widget_content += `<th style="width: auto;">Action</th>`

                                     tab_widget_content += `</tr>
                                                        </thead>`
                                     tab_widget_content += tfootcolumns
                                     tab_widget_content += `</table>`
                                                        
                         tab_widget_content += `</div>
                                            </div>
                                        </div>
                                    </div>`
            }
        })
        $('#tab-widget').html(tab_widget);
        $('#tab-widget-content').html(tab_widget_content);
        console.log('kopi ini')
        console.log(tab_widget)
    }

    function saveDataTab(panel_noid) {
        // localStorage.removeItem('input-data-'+panel_noid);

        // alert('nextnoid='+nextnoid);
        // alert('nownoid='+nownoid);
        // return false;
        var onrequired = false;
        var errormessage = '';
        if (statusaddtab[panel_noid] == true) {
            var idd = {};
            var idd_fix = new Array();
            $.each($('#tab-form-'+panel_noid).serializeArray(), function(k, v) {
                if (k == 0) {
                    idd['idmaster'] = statusadd ? nextnoid : nownoid;
                    idd['noid'] = nextnoidtab[panel_noid];
                    nextnoidtab[panel_noid]++;
                } else {
                    idd[v.name] = v.value
                    var tf = maintable[panel_noid]+'-'+v.name;
                    var coltypefield = document.querySelector('#'+tf).getAttribute('data-'+tf+'-coltypefield');
                    if (coltypefield == 96) {
                        idd[v.name+'_lookup'] = $('#'+tf+' :selected').text();
                        // alert(idd[v.name+'_lookup'])
                    }

                    var formcaption = $('#'+maintable[panel_noid]+'-'+v.name).attr('data-'+maintable[panel_noid]+'-formcaption');
                    if ($('#'+tf).attr('data-'+maintable[panel_noid]+'-newreq') == 1) {
                        if (v.value == '') {
                            onrequired = true;
                            errormessage += formcaption+' is required!<br>'
                        }
                    }
                }
            })

            if (onrequired) {
                $.notify({
                    message: errormessage
                },{
                    type: 'danger'
                });
                return false;
            }

            idd_fix[0] = idd;
            current_idd = localStorage.getItem('input-data-'+panel_noid);

            if (current_idd == null) {
                localStorage.setItem('input-data-'+panel_noid, JSON.stringify(idd_fix))
            } else {
                merge_idd = JSON.parse(current_idd).concat(idd_fix)
                localStorage.removeItem('input-data-'+panel_noid);
                localStorage.setItem('input-data-'+panel_noid, JSON.stringify(merge_idd))
            }   
            // console.log('SAVEDATATAB')
            // console.log(JSON.parse(localStorage.getItem('input-data-'+panel_noid)))
            // console.log(idd)
            // return false
            // refreshSumTab(0, panel_noid)
        } else {
            var idd = {};
            var idd_fix = new Array();
            $.each($('#tab-form-'+panel_noid).serializeArray(), function(k, v) {
                if (k == 0) {
                    idd['idmaster'] = nextnoid
                    idd['noid'] = nextnoidtab[panel_noid];
                    nextnoidtab[panel_noid]++;
                } else {
                    idd[v.name] = v.value
                    var tf = maintable[panel_noid]+'-'+v.name;

                    var formcaption = $('#'+maintable[panel_noid]+'-'+v.name).attr('data-'+maintable[panel_noid]+'-formcaption');
                    if ($('#'+tf).attr('data-'+maintable[panel_noid]+'-editreq') == 1) {
                        if (v.value == '') {
                            onrequired = true;
                            errormessage += formcaption+' is required!<br>'
                        }
                    }
                }
            })

            if (onrequired) {
                $.notify({
                    message: errormessage
                },{
                    type: 'danger'
                });
                return false;
            }

            idd_fix[0] = idd;
            current_idd = localStorage.getItem('input-data-'+panel_noid);

            var replace_current_idd = new Array();
            $.each(JSON.parse(current_idd), function(k, v) {
                if (v.noid == nownoidtab) {
                    replace_current_idd[k] = idd;
                    replace_current_idd[k]['noid'] = nownoidtab;
                    replace_current_idd[k]['idmaster'] = nownoid;
                } else {
                    replace_current_idd[k] = v;
                }
            })
            localStorage.removeItem('input-data-'+panel_noid);
            localStorage.setItem('input-data-'+panel_noid, JSON.stringify(replace_current_idd))
            // console.log(replace_current_idd)
            // console.log(idd)
        }

        $.each(mainonchange, function(k, v) {
            var current_idd_mo = JSON.parse(localStorage.getItem('input-data-'+panel_noid))
            // console.log(current_idd_mo)
            $.each(v.sourcefield, function(k2, v2) {
                if (v2.sourcetype == 'Summary') {
                    var summary = 0;
                    $.each(current_idd_mo, function(k3, v3) {
                        // console.log(v3)
                        summary = parseInt(summary)+parseInt(v3[v2.fieldname]);
                    })
                    $('#'+maintable[v.targettable]+'-'+v.targetfield).val(summary)
                    $('#'+maintable[v.targettable]+'-'+v.targetfield).trigger('change')
                    $('#'+maintable[v.targettable]+'-'+v.targetfield).trigger('keyup')
                    console.log('#'+maintable[v.targettable]+'-'+v.targetfield+' = '+summary)
                }
            })
        })


        refreshTab()
        cancelDataTab(panel_noid)
        // localStorage.removeItem('input-data-'+panel_noid);
        // console.log(JSON.parse(localStorage.getItem('input-data-'+panel_noid)))
        
        // console.log('aku cek ini')
        // console.log(JSON.parse(localStorage.getItem('input-data-'+panel_noid)))
        // console.log(panel_noid)
        // return false
    }

    function refreshSumTab(id, panel_noid) {

///////////////////

        // var maincms_widgetgrid = new Array();
        // $.each(maincms['widgetgrid'], function(k, v) {
        //     maincms_widgetgrid[v.idcmswidget] = v;
        // })

        if (maincms_widgetgrid[panel_noid]['gridconfig']) {
            // var wgb_onsave = maincms_widgetgrid[panel_noid]['gridconfig']['buttonevent']['OnSave'];
            var wgb_ondelete = JSON.parse(maincms_widgetgrid[panel_noid]['gridconfig'])['buttonevent']['OnDelete'];
            if (wgb_ondelete['status']) {
                $.each(wgb_ondelete['Parameter'], function(k, v) {
                console.log(v.Source['sourcetype']);
                // return false;
                    if (v.typeaction == 'EXEC_QUERY') {
                        // return false;
                        if (v['Source']['sourcetype'] == 'sum') {
                            var current_idd_mo = JSON.parse(localStorage.getItem('input-data-'+panel_noid))
                            var summary = 0;
                            var idtypetranc = '';
                            var idmaster = '';
                            var noid = '';
                            var globaluser = 59;
                            $.each(current_idd_mo, function(k2, v2) {
                                summary = parseInt(summary)+parseInt(v2[v['Source']['fieldname']]);
                                if (k2 == id) {
                                    idtypetranc = v2['idtypetranc'];
                                    idmaster = v2['idmaster'];
                                    noid = parseInt(nownoidtab)+parseInt(k2)+1;
                                }
                            })
                            if (v['Target']['targettype'] == 'EXEC_QUERY') {
                                var query = v['Query']['query'].replace('$idtypetranc', idtypetranc).replace('$idmaster', idmaster).replace('$noid', noid).replace('$GLOBAL_USER', globaluser)
                                // console.log(query)
                                // return false;
                                
                                $.ajax({
                                    type: "POST",
                                    header:{
                                        'X-CSRF-TOKEN':$('#token').val()
                                    },
                                    url: "/save_log_tmd",
                                    data:{
                                        "_token": $('#token').val(),
                                        "idconnection": v['Query']['idconnection'],
                                        "query": query,
                                    },
                                    success: function(response) {
                                    }
                                })
                            }
                        }
                    }
                })
            }
        }
    }
    
    function removeSelected(selecteds) {
        var _maintable = [];
        $.each(maincms['widget'], function(k, v){
            _maintable.push(v['maintable']);
        })

        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            url: "/delete_selected_tmd",
            dataType: "json",
            data:{
                "_token": $('#token').val(),
                "table" : JSON.stringify(_maintable),
                "selecteds": selecteds
            },
            success: function(response) {
                $.notify({
                    message: 'Delete data has been succesfullly.' 
                },{
                    type: 'success'
                });
                refresh()
                // var filter_button = '';
                // for (var i=0; i<response.filterButton[0].length; i++) {
                //     filter_button += response.filterButton[0][i].taga;
                // }
                // $('#space_filter_button').html(filter_button)
                // console.log(filter_button);
            },
        });
    }

    function removeSelectedTab(selecteds) {
        var maincms_panel = new Array();
        $.each(maincms['panel'], function(k, v) {
            $.each(maincms['widget'], function(k2, v2) {
                if (v.idwidget == v2.noid) {
                    maincms_panel[v.noid] = v2;
                }
            })
        })

        // $.ajax({
        //     type: "POST",
        //     header:{
        //         'X-CSRF-TOKEN':$('#token').val()
        //     },
        //     url: "/delete_selected_tmd",
        //     dataType: "json",
        //     data:{
        //         "_token": $('#token').val(),
        //         "table" : maincms_panel[nownoidtab].maintable,
        //         "selecteds": selecteds,
        //     },
        //     success: function(response) {
                // var replace_input_data = localStorage.getItem('input-data-'+nownoidtab);
                var replaceinputdata = new Array();
                $.each(JSON.parse(localStorage.getItem('input-data-'+nownoidtab)), function(k, v) {
                    var notinput = false;
                    
                    $.each(selecteds, function(k2, v2) {
                        if (v.noid == v2) {
                            notinput = true
                        }
                    })

                    if (!notinput) {
                        replaceinputdata.push(v);
                    }
                })
                // console.log('data tidak terhapus')
                // console.log(replaceinputdata)
                // return false
                localStorage.setItem('input-data-'+nownoidtab, JSON.stringify(replaceinputdata))

                // console.log(replaceinputdata);
                // return false;

                $.notify({
                    message: 'Delete data has been succesfullly.' 
                },{
                    type: 'success'
                });
                refreshTab()
                // var filter_button = '';
                // for (var i=0; i<response.filterButton[0].length; i++) {
                //     filter_button += response.filterButton[0][i].taga;
                // }
                // $('#space_filter_button').html(filter_button)
                // console.log(filter_button);
        //     },
        // });
    }

    function getCmsThead() {
        var count_wgf = 0;
        var colgroupname = new Array();
        $.each(maincms['widgetgridfield'][0], function(k, v) {
            if (v.colshow == 1) {
                if (v.colgroupname) {
                    colgroupname.push(v.colgroupname);
                }
                count_wgf++;
            }
        })
        const unique = (value, index, self) => {
            return self.indexOf(value) === index
        }
        colgroupname = colgroupname.filter(unique)
        console.log(colgroupname)

        if (maincms['widgetgrid'][0]['colcheckbox']) {
            var thead = `<th style="text-align:center" colspan="2">#</th>`;
        } else {
            var thead = `<th style="text-align:center" colspan="1">#</th>`;
        }
        // $.each(maincms['widgetgridfield'], function(k, v) {
            thead += `<th style="text-align:center" colspan="`+count_wgf+`"></th>`;
        // })
        thead += `<th colspan="1" style="text-align:center">---</th>`;
        $('#thead').html(thead);
    }

    function getCmsTabThead(panel_idwidget) {
        $.each(maincms['widgetgridfield'], function(k, v) {
            if (k > 0) {
                if (v.idcmswidgetgrid == panel_idwidget)
                    if (v.colshow == 1) {
                        var thead = `<th style="text-align:center" colspan="2">#</th>`;
                        // $.each(maincms['widgetgridfield'], function(k, v) {
                            thead += `<th style="text-align:center" colspan="`+v.length+`"></th>`;
                        // })
                        thead += `<th colspan="1" style="text-align:center">---</th>`;
                        return thead;
                        // $('#tab-thead-'+maincms['widget'][k].maintable).html(thead);
                        // console.log('#tab-thead-'+maincms['widget'][k].maintable)
                    }
            }
        })
    }

    function saveFilter() {
        // $('#modal-notif').css('display', 'none')
        // var config_widget = {
        //     WidgetFilter: '',
        //     WidgetConfig: '',
        // }
        var formdata = $('#formfilter').serialize();

        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            url: "/save_filter_tmd",
            dataType: "json",
            data:{
                "_token":$('#token').val(),
                "data" :{
                    'data': formdata,
                    'url_code': $('#url_code').val()
                }
            },
            success:function(response){
                // console.log(response)
                if (response.result) {
                    // alert('Success')
                    closeModalFilter()
                    $.notify({
                        message: 'Save data has been succesfullly.' 
                    },{
                        type: 'success'
                    });
                    setInterval(function(){
                        location.reload()
                    }, 3000)
                    // location.reload()
                } else {
                    // $('#modal-notif').css('display', 'block')
                    // setInterval(function(){
                    //     $('#modal-notif').css('display', 'none')
                    // }, 3000)
                    
                    $.notify({
                        message: 'Save data has been error.' 
                    },{
                        type: 'danger'
                    });
                }
            }
        });
    }

    function delRow(key) {
        filtercontenttabs.filtercontentrow[selected_filter_tabs][key] = '';
        getFilterContentTabs()
    }

    function addRow() {
        filtercontenttabs.filtercontentrow[selected_filter_tabs].push(`<input type="hidden" name="tabsrow[]" value="`+selected_filter_tabs+`">
                                                                        <div class="row filter-content-row-`+selected_filter_tabs+`">
                                                                            <div class="col-md-2">
                                                                                <div class="form-group">
                                                                                    <label class="col-md-12 control-label">Caption</label>
                                                                                    <input class="form-control input-sm" name="buttoncaption[]" id="buttoncaption-`+selected_filter_tabs+`-`+filtercontenttabs.filtercontentrow[selected_filter_tabs].length+`" value="" placeholder="Caption">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label class="col-md-12 control-label">Condition</label>
                                                                                    <input class="form-control input-sm" name="condition[]" id="condition-`+selected_filter_tabs+`-`+filtercontenttabs.filtercontentrow[selected_filter_tabs].length+`" value="" placeholder="groupoperand#fieldname;operator;value;operand">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1">
                                                                                <div class="form-group">
                                                                                    <label class="col-md-12 control-label">Active</label>
                                                                                    <select class="form-control input-sm" name="buttonactive[]" id="buttonactive-`+selected_filter_tabs+`-`+filtercontenttabs.filtercontentrow[selected_filter_tabs].length+`" value="1">
                                                                                        <option value="1">True</option>
                                                                                        <option value="0" selected>False</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1">
                                                                                <div class="form-group">
                                                                                    <label class="col-md-12 control-label">Icon</label>
                                                                                    <input class="form-control input-sm" name="classicon[]" id="classicon-`+selected_filter_tabs+`-`+filtercontenttabs.filtercontentrow[selected_filter_tabs].length+`" value="" placeholder="Icon">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1">
                                                                                <div class="form-group">
                                                                                    <label class="col-md-12 control-label">Color</label>
                                                                                    <input class="form-control input-sm" name="classcolor[]" id="classcolor-`+selected_filter_tabs+`-`+filtercontenttabs.filtercontentrow[selected_filter_tabs].length+`" value="" placeholder="Color">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1" style="text-align:center;padding:21px 0 0 0">
                                                                                <div class="form-group">
                                                                                    <button id="del" onclick="delRow(`+filtercontenttabs.filtercontentrow[selected_filter_tabs].length+`)" type="button" class="btn btn-sm red" style="height: 27px; width: 38px">
                                                                                        <i class="fa fa-minus-circle text-white"></i>
                                                                                    </button>
                                                                                    <button id="add" onclick="addRow()" type="button" class="btn btn-sm green" style="height: 27px; width: 38px">
                                                                                        <i class="fa fa-plus text-white"></i>
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </div>`)
        getFilterContentTabs()
        // console.log(filtercontenttabs)
    }

    $('#delgroup').on('click', function() {
        filter_tabs.pop()
        filtercontenttabs.filtercontentgroup.pop()
        filtercontenttabs.filtercontentbody.pop()
        filtercontenttabs.filtercontentrow.pop()
        getFilterContentTabs()
    })

    $('#addgroup').on('click', function() {
        filter_tabs.push(false)

        filtercontenttabs.filtercontentgroup.push(`<input type="hidden" name="tabs[]" value="`+filtercontenttabs.filtercontentgroup.length+`">
                                                    <div style="margin:5px 5px 0 0;" id="filter-content-group-`+filtercontenttabs.filtercontentgroup.length+`" onclick="changeTabFilter(`+filtercontenttabs.filtercontentgroup.length+`)" class="filter-content-group formbtn btn default default">
                                                        <span class="text-dark">
                                                            <i class="fa fa-indent text-dark"></i> `+filtercontenttabs.filtercontentgroup.length+`
                                                        </span>
                                                    </div>`)

        filtercontenttabs.filtercontentbody.push(`<div style="display: none" id="filter-content-body-`+filtercontenttabs.filtercontentbody.length+`" class="filter-content-body formgroup col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="col-md-12 control-label">Group Caption</label>
                                                                <input name="groupcaption[]" id="groupcaption-`+filtercontenttabs.filtercontentbody.length+`" class="form-control input-sm" value="" placeholder="Group Caption">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="col-md-12 control-label">Group Caption Showed</label>
                                                                <select name="groupcaptionshow[]" id="groupcaptionshow-`+filtercontenttabs.filtercontentbody.length+`" class="form-control input-sm">
                                                                    <option value="1">True</option>
                                                                    <option value="0" selected>False</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="col-md-12 control-label">Group Dropdown Showed</label>
                                                                <select name="groupdropdown[]" id="groupdropdown-`+filtercontenttabs.filtercontentbody.length+`" class="form-control input-sm">
                                                                    <option value="-1" selected>Auto</option>
                                                                    <option value="0" selected>False</option>
                                                                    <option value="1">True</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="filter-content-row-`+filtercontenttabs.filtercontentbody.length+`">
                                                    </div>
                                                </div>`)
        filtercontenttabs.filtercontentrow.push(new Array())
        getFilterContentTabs()
    })

    function getFilterContentTabs() {
        //Set to HTML
        var html_filtercontentgroup = '';
        var html_filtercontentbody = '';
        var value = {
            groupcaption: new Array(),
            groupcaptionshow: new Array(),
            groupdropdown: new Array(),
            buttoncaption: new Array(),
            condition: new Array(),
            buttonactive: new Array(),
            classicon: new Array(),
        };

        //SET
        $.each(filtercontenttabs.filtercontentgroup, function(k, v) {
            value.buttoncaption[k] = new Array();
            value.condition[k] = new Array();
            value.buttonactive[k] = new Array();
            value.classicon[k] = new Array();

            if ($('#groupcaption-'+k).val() == undefined) { value.groupcaption.push('') } else { value.groupcaption.push($('#groupcaption-'+k).val()) }
            if ($('#groupcaptionshow-'+k).val() == undefined) { value.groupcaptionshow.push(0) } else { value.groupcaptionshow.push($('#groupcaptionshow-'+k).val()) }
            if ($('#groupdropdown-'+k).val() == undefined) { value.groupdropdown.push(-1) } else { value.groupdropdown.push($('#groupdropdown-'+k).val()) }
            
            $.each(filtercontenttabs.filtercontentrow[k], function(k2, v2) {
                if ($('#buttoncaption-'+k+'-'+k2).val() == undefined) { value.buttoncaption[k][k2] = '' } else { value.buttoncaption[k][k2] = $('#buttoncaption-'+k+'-'+k2).val() }
                if ($('#condition-'+k+'-'+k2).val() == undefined) { value.condition[k][k2] = '' } else { value.condition[k][k2] = $('#condition-'+k+'-'+k2).val() }
                if ($('#buttonactive-'+k+'-'+k2).val() == undefined) { value.buttonactive[k][k2] = 0 } else { value.buttonactive[k][k2] = $('#buttonactive-'+k+'-'+k2).val() }
                if ($('#classicon-'+k+'-'+k2).val() == undefined) { value.classicon[k][k2] = '' } else { value.classicon[k][k2] = $('#classicon-'+k+'-'+k2).val() }
            })

            html_filtercontentgroup += v
            html_filtercontentbody += filtercontenttabs.filtercontentbody[k]
        })

        $('#filtercontentgroup').html(html_filtercontentgroup);
        $('#filtercontentbody').html(html_filtercontentbody);

        $.each(filtercontenttabs.filtercontentbody, function(k, v) {
            if (filter_tabs[k] == true) {
                changeTabFilter(k)
            }
            $('#groupcaption-'+k).val(value.groupcaption[k])
            $('#groupcaptionshow-'+k).val(value.groupcaptionshow[k])
            $('#groupdropdown-'+k).val(value.groupdropdown[k])
        })

        $.each(filtercontenttabs.filtercontentrow, function(k, v) {
            $('#filter-content-row-'+k).html(v);

            $.each(v, function(k2, v2) {
                $('#buttoncaption-'+k+'-'+k2).val(value.buttoncaption[k][k2])
                $('#condition-'+k+'-'+k2).val(value.condition[k][k2])
                $('#buttonactive-'+k+'-'+k2).val(value.buttonactive[k][k2])
                $('#classicon-'+k+'-'+k2).val(value.classicon[k][k2])
            })
        })
        //End Set to HTML
        // console.log(value)
    }
    
    function changeTabFilter(key) {
        $('.filter-content-group').removeClass('purple').addClass('default')
        $('.filter-content-group span').removeClass('text-white').addClass('text-dark')
        $('.filter-content-group span i').removeClass('text-white').addClass('text-dark')
        $('#filter-content-group-'+key).removeClass('default').addClass('purple')
        $('#filter-content-group-'+key+' span').removeClass('text-dark').addClass('text-white')
        $('#filter-content-group-'+key+' span i').removeClass('text-dark').addClass('text-white')
        $('.filter-content-body').css('display', 'none')
        $('#filter-content-body-'+key).css('display', 'block')
        selected_filter_tabs = key;
        
        $.each(filter_tabs, function(k, v) {
            if (k == key) {
                filter_tabs[k] = true
            } else {
                filter_tabs[k] = false
            }
        })
    }

    function closeModalFilter() {
        $('#modal-filter').modal('hide')
    }

    function showModalFilter() {
        $('#modal-filter').modal('show')
    }

    function showModalCKEditor() {
        $('#modal-ckeditor').modal('show')
    }
    
    function getData() {
        // if (!JSON.parse(localStorage.getItem('maincms-'+values3))) {
            // localStorage.removeItem('maincms');
            $.ajax({
                type: "POST",
                header:{
                    'X-CSRF-TOKEN':$('#token').val()
                },
                url: "/get_data_tmd",
                dataType: "JSON",
                data:{
                    "_token":$('#token').val(),
                    "table": values3
                },
                success: function(response) {
                    localStorage.setItem('maincms-'+values3, JSON.stringify(response))
                }
            })
        // }
        maincms = JSON.parse(localStorage.getItem('maincms-'+values3))
        if (maincms == null) {
            location.reload()
        }
    }

    function getDatatable() {
        var columns = new Array();
        var columndefs = new Array();
        var index = 1;
        var noncolsorted = new Array();
        var noncolsearch = new Array();
        if (maincms['widgetgrid'][0]['colcheckbox']) {
            // columndefs.push({"targets": [0,1,21],"orderable": false})
            noncolsorted.push(0)
            noncolsorted.push(1)
            noncolsearch.push(0)
            noncolsearch.push(1)
            columns.push({"data": 0,"class": "text-left"})
            columns.push({"data": 1,"class": "text-left"})
            index = 2;
        } else {
            noncolsorted.push(0)
            noncolsearch.push(0)
            columndefs.push({"targets": [0,20],"orderable": false})
            columns.push({"data": 0,"class": "text-left"})
        }
        $.each(maincms['widgetgridfield'][0], function(k, v) {
            if (k == 0) {
                if (maincms['widgetgrid'][0]['colcheckbox']) {
                    columndefs.push({
                        "height": '60px',
                        "targets": index
                    })
                }
                columndefs.push({
                    "height": '60px',
                    "targets": index
                })
            }
            if (v.colshow) {
                if (v.colwidth != 0) {
                    var colwidth = v.colwidth+'px';
                } else {
                    var colwidth = '100px';
                }
                if (!v.colsorted) {
                    noncolsorted.push(index)
                }
                if (!v.colsearch) {
                    noncolsearch.push(index)
                }
                columns.push({
                    "data": index,
                    "width": colwidth,
                    "class": "text-"+v.colalign.toLowerCase()
                })
                columndefs.push({
                    "height": '60px',
                    "targets": index
                })
                index++
            }
        })
        noncolsorted.push(index)
        noncolsearch.push(index)
        columndefs.push({"targets": noncolsorted,"orderable": false})
        columndefs.push({"targets": noncolsearch,"searchable": false})
        columns.push({"data": index,"class": "text-left"})
        columndefs.push({"height": '60px',"targets": index})
        console.log('columndefs')
        console.log(columns)
        // console.log(maincms['widgetgrid'][0].recperpage)
        // console.log('lihattttt')
        // console.log(columndefs)
        // return false
        mytable = $('#table_pagepanel_9921050101').DataTable({
            "autoWidth": false,
            "pageLength": maincms['widgetgrid'][0].recperpage,
            "searching": true,
            "processing": true,
            "serverSide": true,
            "bInfo": true,
            "bFilter": false,
            "bLengthChange": false,
            "columns": columns,
            "columnDefs": columndefs,
            // "dom": '<"top dtfiler"<"">>',
            // "dom": '<"info1 top dtfiler"pli<""f><"clear">>rt ',
            // "dom": '<lf<t>ip>',
            "infoCallback": function( settings, start, end, max, total, pre ) {
                var options = '';
                $.each(settings.aLengthMenu, function(k, v) {
                    options += '<option value="'+v+'" id="alm'+v+'" class="alm">'+v+'</option>'
                })
                if ((end-start) == settings._iDisplayLength-1) {
                    var valuepage = end/settings._iDisplayLength;
                } else {
                    var valuepage = ((start-1)/settings._iDisplayLength)+1;
                }
                if (start == 1) {
                    var prevdisabled = 'disabled'
                } else {
                    var prevdisabled = '';
                }
                if (end == total) {
                    var nextdisabled = 'disabled'
                } else {
                    var nextdisabled = '';
                }
                // if (settings.oPreviousSearch.sSearch == $('#searchform').val()) {
                //     alert($('#searchform').val())
                //     var valuesearch = $('#searchform').val()
                // } else {
                //     var valuesearch = $('#searchform').val()
                //     alert('else'+$('#searchform').val())
                // }
                if (settings.oPreviousSearch.sSearch != '') {
                    statussearch = true
                    searchtablewidth = 200
                    valuesearch = settings.oPreviousSearch.sSearch
                } else {
                    statussearch = false
                    searchtablewidth = 30
                    valuesearch = ''
                }
                $('#appendInfoDatatable').html(`
                <div class="row" style="background: #90CAF9; padding-left: 15px; padding-right: 15px;">
                    <div class="col-md-12">
                        Page
                        <button id="prevpage" onclick="prevpage()" class="btn btn-sm default prev `+prevdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-left">
                            </i>
                        </button>
                        <input type="text" id="valuepage" onkeyup="valuepage()" class="input-mini" value="`+valuepage+`" style="text-align: center; border: 0px; margin: 5px 5px 5px 5px">
                        <button id="nextpage" onclick="nextpage()" class="btn btn-sm default next `+nextdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-right">
                            </i>
                        </button> of <span class="pagination-panel-total">`+(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)+`</span>
                        <span class="seperator">|</span>
                        Rec/Page 
                        <select id="alengthmenu" onchange="alengthmenu()" name="table_pagepanel_9921050101_length" aria-controls="table_pagepanel_9921050101" class="input-inline input-xsmall input-sm recofdt" style="border: 0px; margin: 5px 5px 5px 5px">
                            `+options+`
                        </select>
                        <span class="seperator">|</span>Total `+settings._iRecordsTotal+` records
                        <input id="searchform" onkeyup="searchform()" value="`+valuesearch+`" type="search" class="form-control input-inline input-sm clicked" placeholder="" aria-controls="table_pagepanel_9921050101" style="width: `+searchtablewidth+`px; position: absolute; right: 0px; top: 4px; height: 80%; border: 0;">
                        <div onclick="searchtable()" class="btn fa fa-search text-right" style="position: absolute; right: 0px; top: 4px; height: 80%; background: transparent; margin: 0px 0px 0px 0px; padding: 5px 0px 5px 0px">
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                </div>`)
                console.log('settings')
                console.log(settings)
                console.log(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)
                $('.alm').removeAttr('selected');
                $('#alm'+settings._iDisplayLength).attr('selected', 'selected');
                // console.log(valuepage)
                $('.dataTables_info').css('display', 'none')
                $('.dataTables_paginate').css('display', 'none')
                // return '<"top dtfiler"<"">>';
            },
            "ajax": {
                "url": "/get_tmd",
                "type": "POST",
                "dataType":'json',
                "data": {
                    "_token":$('#token').val(),
                    key: 0,
                    table: values3,
                    selected_filter: selected_filter,
                    maincms: JSON.stringify(maincms)
                },
                beforeSend: function () {
                    $(".dataTables_scroll").LoadingOverlay("show");
                },
                complete: function (response) {
                    $(".dataTables_scroll").LoadingOverlay("hide");

                    if (response.responseJSON.noids.length > 0) {
                        noids = response.responseJSON.noids;
                    }
                    // alert('asd')
                    // $.each(maincms['widgetgridfield'][0], function(k, v) {
                    //     $('#example_baru th:nth-child('+k+')').attr('style', 'padding: 0px 5px 0px 5px; width: '+v.colwidth+'px;')
                    //     $('#example_baru td:nth-child('+k+')').attr('style', 'padding: 0px 5px 0px 5px; width: '+v.colwidth+'px;')
                    //     alert(document.querySelector('.dataTables_scrollHeadInner #thead-columns th:nth-child('+k+')').getAttribute('style'))
                    // })
                    // $('#example_baru .sorting_1').attr('style', 'width: 103px')
                },
            },
            "pagingType": "input",
            "createdRow": function(row, data, dataIndex) {
                // console.log(data)
                $(row).addClass('my_row');
                $(row).addClass('row'+dataIndex);
                // $(row).css('height', '5px');
                $(row).attr('onmouseover', 'showButtonAction(this, '+dataIndex+')');
                $(row).attr('onmouseout', 'resetCss()');
                // $(row).attr('onmouseover', 'getPositionXY(this)');
            },
            "oLanguage":{
                "loadingRecords": "Please wait - loading...",
                "sProcessing": "<div id='loadernya'></div>",
                "sInfo": "&nbsp;|&nbsp;Total&nbsp;_TOTAL_ records",
                "sLengthMenu": "&nbsp;|&nbsp;Rec/Page&nbsp;_MENU_",
                "sInfoFiltered": "",
                "sInfoEmpty": "| No records found to show",
                "sEmptyTable": "No data available in table",
                "sZeroRecords": "No matching records found",
                "sSearch": "<button onclick='toggleButtonSearch()' class='btn flat dtfilter_search_btn' style='background-color: white'><i class='fa fa-search'></i></button>",
                // "oPaginate": {
                //   "previous": "<i class='fa fa-angle-left'></i>",
                //   "next": "<i class='fa fa-angle-right'></i>",
                //   "page": "Page",
                //   "pageOf": "of"
                // },
                "oPaginate": {
                    "sPrevious": "<i class='fa fa-angle-left'></i>",
                    "sNext": "<i class='fa fa-angle-right'></i> ",
                // "sPage": "<i id='page_prev'>Page</i>",
                // "sPageOf": "<span id='page_next'> of </span>"
                }
            },
            footerCallback: function ( row, data, start, end, display ) {
                var api = this.api(), data;
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
                // var _split = function ( i ) {
                //     return typeof i === 'string' ?
                //         parseInt(i.replace('.', '')).toString() :
                //         typeof i === 'number' ?
                //             i : 0;
                // };
                var count = function ( i ) {
                    return api.column( 5 )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(end);
                            }, 0 )
                }
                var sum = function ( i ) {
                    var value = 0;
                    $.each(api.column( i ).data(), function(k, v) {
                        var _split = v.split('.');
                        var value2 = '';
                        $.each(_split, function(k2, v2) {
                            value2 += v2
                        })
                        value += Number(value2)
                    })
                    return value
                }
                var addcommas = function addCommas(nStr) {
                    nStr += '';
                    var x = nStr.split('.');
                    var x1 = x[0];
                    var x2 = x.length > 1 ? '.' + x[1] : '';
                    var rgx = /(\d+)(\d{3})/;
                    while (rgx.test(x1)) {
                        x1 = x1.replace(rgx, '$1' + '.' + '$2');
                    }
                    return x1 + x2;
                }

                $.each(functionfooter, function(k, v) {
                    $( api.column(v.index).footer() ).html(api.column(v.index).data().reduce( function (a, b) { return addcommas(eval(functionfooter[k].function)) }, 0 ));
                })
                // console.log('try')
                // console.log(row)
                // console.log(functionfooter)
            }
        });
        // console.log('xxx')
        // console.log(columns)

        my_table = $('#example_baru').DataTable({
            // "scrollX": true,
            // "scrollCollapse": true,
            "processing": true,
            "serverSide": true,
            "info": true,
            // "order": [[ 0, "asc" ]],
            // "columnDefs": [ {
            //     "targets": [0,1],
            //     "width": "20px"
            // },
            // {
            //     "targets": 2,
            //     "width": "50px"
            // },
            // {
            //     "targets": [3,4,5,6,7],
            //     "width": "auto"
            // }],
            "autoWidth": false, //step 1
            "columnDefs": [
                { "width": "100px", "targets": 0 }
            ],
            "columns": columns,
            // "columns": [
            //     { "data": 0 },
            //     { "data": 1 },
            //     { "data": 2 },
            //     { "data": 3 },
            //     { "data": 4 },
            //     { "data": 5 },
            //     { "data": 6 },
            //     { "data": 7 },
            //     { "data": 8 },
            //     { "data": 9, "class": "text-right"},
            //     { "data": 10 },
            //     { "data": 11 },
            //     { "data": 12 },
            //     { "data": 13 },
            //     { "data": 14 },
            //     { "data": 15 },
            //     { "data": 16 },
            //     { "data": 17 },
            //     { "data": 18 },
            //     { "data": 19 },
            //     { "data": 20 },
            //     { "data": 21 },
            // ],
            // "columnDefs": [
            //     { "width": "200px", "targets": 0 }
            // ],
            // "fixedColumns": true,
            "pagingType": "input",
            "searching": true,
            // "paging":   false,
            // "ordering": false,
            // "info":     false,
            "ajax": {
                "url": "/get_tmd",
                "type": "POST",
                "dataType":'json',
                "data": {
                    "_token":$('#token').val(),
                    key: 0,
                    table: values3,
                    selected_filter: selected_filter,
                    maincms: JSON.stringify(maincms)
                },
                beforeSend: function () {
                    $(".dataTables_scroll").LoadingOverlay("show");
                },
                complete: function () {
                    $(".dataTables_scroll").LoadingOverlay("hide");
                    // alert('asd')
                    // $.each(maincms['widgetgridfield'][0], function(k, v) {
                    //     $('#example_baru th:nth-child('+k+')').attr('style', 'padding: 0px 5px 0px 5px; width: '+v.colwidth+'px;')
                    //     $('#example_baru td:nth-child('+k+')').attr('style', 'padding: 0px 5px 0px 5px; width: '+v.colwidth+'px;')
                    //     alert(document.querySelector('.dataTables_scrollHeadInner #thead-columns th:nth-child('+k+')').getAttribute('style'))
                    // })
                    // $('#example_baru .sorting_1').attr('style', 'width: 103px')
                },
            },
            // "sPaginationType":"input",
            "dom": '<"top dtfiler"pli<""f><"clear">>rt ',
            "oLanguage":{
                "loadingRecords": "Please wait - loading...",
                "sProcessing": "<div id='loadernya'></div>",
                "sInfo": "&nbsp;|&nbsp;Total&nbsp;_TOTAL_ records",
                "sLengthMenu": "&nbsp;|&nbsp;Rec/Page&nbsp;_MENU_",
                "sInfoFiltered": "",
                "sInfoEmpty": "| No records found to show",
                "sEmptyTable": "No data available in table",
                "sZeroRecords": "No matching records found",
                "sSearch": "<button onclick='toggleButtonSearch()' class='btn flat dtfilter_search_btn' style='background-color: white'><i class='fa fa-search'></i></button>",
                // "oPaginate": {
                //   "previous": "<i class='fa fa-angle-left'></i>",
                //   "next": "<i class='fa fa-angle-right'></i>",
                //   "page": "Page",
                //   "pageOf": "of"
                // },
                "oPaginate": {
                    "sPrevious": "<i class='fa fa-angle-left'></i>",
                    "sNext": "<i class='fa fa-angle-right'></i> ",
                // "sPage": "<i id='page_prev'>Page</i>",
                // "sPageOf": "<span id='page_next'> of </span>"
                }
            },
            "createdRow": function(row, data, dataIndex) {
                // console.log(data)
                $(row).addClass('my_row');
                $(row).addClass('row'+dataIndex);
                // $(row).css('height', '5px');
                $(row).attr('onmouseover', 'showButtonAction(this, '+dataIndex+')');
                $(row).attr('onmouseout', 'resetCss()');
                // $(row).attr('onmouseover', 'getPositionXY(this)');
            },
            deferRender: true,
            footerCallback: function ( row, data, start, end, display ) {
                var api = this.api(), data;
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
                var monTotal = api.column( 1 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                $( api.column( 3 ).footer() ).html(monTotal);
            }
        });
        
        new $.fn.dataTable.Buttons( mytable, {
            buttons: [
                {
                    autoFilter: true,
                    text:      '<i class="fa fa-trash"></i> Delete Selected',
                    titleAttr: 'Delete Selected',
                    className: ' dropdown-item flat',
                    action:  function(e, dt, button, config) {
                        if (selecteds.length == 0) {
                            alert("There's no selected")
                        } else {
                            if (confirm('Are you sure you want to delete this?')) {
                                removeSelected(selecteds)
                            }
                        }
                    },
                },{
                    autoFilter: true,
                    text:      '<i class="fa fa-trash"></i> Delete All',
                    titleAttr: 'Delete All',
                    className: ' dropdown-item flat',
                    action:  function(e, dt, button, config) {
                        if (noids.length == 0) {
                            alert("There's no data")
                        } else {
                            if (confirm('Are you sure you want to delete all data?')) {
                                removeSelected(noids)
                                noids = new Array();
                            }
                        }
                    },
                },{
                    extend:    'excel',
                    autoFilter: true,
                    text:      '<i class="fa fa-file-excel"></i> Export to Excel',
                    titleAttr: 'Export to Excel',
                    title: $("#judul_excel").val(),
                    className: ' dropdown-item flat',
                    exportOptions: {
                        modifier: {
                            search: 'applied',
                            order: 'applied'
                        },
                        columns: [ 1,2,3, ':visible' ]
                        //                  <a style="font-size: 14px;min-width: 175px;
                        // text-align: left;" href="javascript:;"  class="dropdown-item">
                        // <i style="margin-top:10px;margin-right: 5px;" class="fa fa-file-excel"></i> Export to Excel </a>
                                        // columns: [ 0, ':visible' ]
                    },
                    action:  function(e, dt, button, config) {
                    // $('.loading').fadeIn();
                        var that = this;
                        setTimeout(function () {
                            $.fn.dataTable.ext.buttons.excelHtml5.action.call(that,e, dt, button, config);
                            // console.log(that);
                            // console.log(e);
                            // console.log(dt);
                            // console.log(button);
                            // console.log(config);
                            $('.loading').fadeOut();
                        },500);
                    },
                }
            ]
            // infoDatatable()
        });
        // infoDatatable()

        mytable.buttons().container().appendTo('#drop_export2');
        // $('#appendInfoDatatable').html($('.info1').html())
        // console.log('info1')
        // console.log($('.info1').html())

        $('#pagepanel').addClass(maincms['panel'][0]['nospan'])
        $('#pagepanel').addClass(maincms['panel'][0]['nospanxs'])
        if (!maincms['widgetgrid'][0]['actnew']) {
            $('#btn-add-data').hide()
        }
        if (!maincms['widgetgrid'][0]['actexportxls']) {
            // $('#drop_export2 button:nth-child(3)').hide()
        }
    }
    
    function searchform(noid) {
        if (noid) {
            $("#searchform-"+noid).keyup(function(e) {
                if(e.keyCode == 13) {
                    mytabtable[noid].search($("#searchform-"+noid).val()).draw('page');
                }
            })
        } else {
            $("#searchform").keyup(function(e) {
                if(e.keyCode == 13) {
                    mytable.search($("#searchform").val()).draw('page');
                }
            })
        }
    }

    function searchtable(noid) {
        if (noid) {
            if (tab_searchtablewidth[noid] == 30) {
                tab_searchtablewidth[noid] = 200
            } else {
                tab_searchtablewidth[noid] = 30
            }
            $("#searchform-"+noid).animate({ 
                width: tab_searchtablewidth[noid]+'px' 
            }, 100); 
        } else {
            if (searchtablewidth == 30) {
                searchtablewidth = 200
            } else {
                searchtablewidth = 30
            }
            $("#searchform").animate({ 
                width: searchtablewidth+'px' 
            }, 100); 
        }
    }

    // function valuepage() {
    //     $("#valuepage").keyup(function(e) {
    //         if(e.keyCode == 13) {
    //             alert('masuk')
    //             mytable.page(3).draw('page');
    //         }
    //     })
    // }

    function prevpage(noid) {
        if (noid) {
            mytabtable[noid].page('previous').draw('page');
        } else {
            mytable.page('previous').draw('page');
        }
    }

    function nextpage(noid) {
        if (noid) {
            mytabtable[noid].page('next').draw('page');
        } else {
            mytable.page('next').draw('page');
        }
    }

    function alengthmenu(noid) {
        if (noid) {
            mytabtable[noid].page.len($('#alengthmenu-'+noid+' :selected').val()).draw();
        } else {
            mytable.page.len($('#alengthmenu :selected').val()).draw();
        }
    }

    function getCmsTabDatatable() {
        var maincms_widgetgridfield = new Array();
        
        $.each(maincms['widgetgridfield'], function(k, v) {
            $.each(v, function(k2, v2) {
                maincms_widgetgridfield[v2.idcmswidgetgrid] = v;
            })
        })

        $.each(maincms['panel'], function(k, v) {
            if (k > 0) {
                // console.log('aku cek ini')
                // console.log(JSON.parse(localStorage.getItem('input-data-'+v.noid)))
                // return false
                // console.log('maincms_widgetgrid')
                // console.log(maincms_widgetgrid[v.noid])
                if (!maincms_widgetgrid[v.noid]['panelfilter']) {
                    $('#space_filter_button-'+v.noid).hide()
                    $('#tool_filter'+v.noid).hide()
                } else {
                    getFilterButton(values3, v.noid)
                }

                // console.log('idd_fix')
                // console.log(JSON.parse(localStorage.getItem('input-data-'+v.noid)))
                // return false
                console.log(localStorage.getItem('input-data-'+v.noid))
                $.each(maincms_widgetgridfield[v.idwidget], function(k2, v2) {
                    if (k2 == 0) {
                        fieldnametab[v.noid] = new Array();
                        tablelookuptab[v.noid] = new Array();
                    }
                        // alert(v3.idcmswidgetgrid+' '+v.idwidget)
                    if (v2.colshow == 1) {
                        if (v2.coltypefield == 96) {
                            fieldnametab[v.noid].push(v2.fieldname+'_lookup');
                        } else {
                            fieldnametab[v.noid].push(v2.fieldname);
                        }
                        tablelookuptab[v.noid].push(v2.tablelookup);
                    }
                })
                // console.log('cekkkk')
                // console.log(fieldnametab[v.noid])
                // tab_datatable.push(v.maintable);


                var columns = new Array();
                var columndefs = new Array();
                var index = 1;
                var noncolsorted = new Array();
                var noncolsearch = new Array();
                if (maincms['widgetgrid'][k]['colcheckbox']) {
                    // columndefs.push({"targets": [0,1,21],"orderable": false})
                    noncolsorted.push(0)
                    noncolsorted.push(1)
                    noncolsearch.push(0)
                    noncolsearch.push(1)
                    columns.push({"data": 0,"class": "text-left"})
                    columns.push({"data": 1,"class": "text-left"})
                    index = 2;
                } else {
                    noncolsorted.push(0)
                    noncolsearch.push(0)
                    columndefs.push({"targets": [0,20],"orderable": false})
                    columns.push({"data": 0,"class": "text-left"})
                }
                $.each(maincms['widgetgridfield'][k], function(k, v) {
                    if (k == 0) {
                        if (maincms['widgetgrid'][k]['colcheckbox']) {
                            columndefs.push({
                                "name": v.fieldsource,
                                "height": '60px',
                                "targets": index
                            })
                        }
                        columndefs.push({
                            "name": v.fieldsource,
                            "height": '60px',
                            "targets": index
                        })
                    }
                    if (v.colshow) {
                        if (v.colwidth != 0) {
                            var colwidth = v.colwidth+'px';
                        } else {
                            var colwidth = '100px';
                        }
                        if (!v.colsorted) {
                            noncolsorted.push(index)
                        }
                        if (!v.colsearch) {
                            noncolsearch.push(index)
                        }
                        columns.push({
                            "data": index,
                            "width": colwidth,
                            "class": "text-"+v.colalign.toLowerCase()
                        })
                        columndefs.push({
                            "name": v.fieldsource,
                            "height": '60px',
                            "targets": index
                        })
                        index++
                    }
                })

                noncolsorted.push(index)
                noncolsearch.push(index)
                columndefs.push({"targets": noncolsorted,"orderable": false})
                columndefs.push({"targets": noncolsearch,"searchable": false})
                columns.push({"data": index,"class": "text-left"})
                columndefs.push({"height": '60px',"targets": index})
                console.log('columndefstabs')
                console.log(columns)
                // console.log(maincms['widgetgrid'][0].recperpage)
                // console.log('columndefs')
                // console.log(columndefs)
                // return false

                // alert('#tab-table-'+v.noid)
                my_tab_table = $('#tab-table-'+v.noid).DataTable({
                    "autoWidth": false,
                    "pageLength": maincms['widgetgrid'][k].recperpage,
                    "searching": true,
                    "processing": true,
                    "serverSide": true,
                    "bInfo": true,
                    "bFilter": false,
                    "bLengthChange": false,
                    "columns": columns,
                    "columnDefs": columndefs,
                    // "dom": '<"top dtfiler"<"">>',
                    // "dom": '<"info1 top dtfiler"pli<""f><"clear">>rt ',
                    // "dom": '<lf<t>ip>',
                    "infoCallback": function( settings, start, end, max, total, pre ) {
                        var options = '';
                        $.each(settings.aLengthMenu, function(k, v) {
                            options += '<option value="'+v+'" id="alm'+v+'" class="alm">'+v+'</option>'
                        })
                        if ((end-start) == settings._iDisplayLength-1) {
                            var valuepage = end/settings._iDisplayLength;
                        } else {
                            var valuepage = ((start-1)/settings._iDisplayLength)+1;
                        }
                        if (start == 1) {
                            var prevdisabled = 'disabled'
                        } else {
                            var prevdisabled = '';
                        }
                        if (end == total) {
                            var nextdisabled = 'disabled'
                        } else {
                            var nextdisabled = '';
                        }
                        // if (settings.oPreviousSearch.sSearch == $('#searchform').val()) {
                        //     alert($('#searchform').val())
                        //     var valuesearch = $('#searchform').val()
                        // } else {
                        //     var valuesearch = $('#searchform').val()
                        //     alert('else'+$('#searchform').val())
                        // }
                        if (settings.oPreviousSearch.sSearch != '') {
                            tab_statussearch[v.noid] = true
                            tab_searchtablewidth[v.noid] = 200
                            tab_valuesearch[v.noid] = settings.oPreviousSearch.sSearch
                        } else {
                            tab_statussearch[v.noid] = false
                            tab_searchtablewidth[v.noid] = 30
                            tab_valuesearch[v.noid] = ''
                        }
                        $('#tab-appendInfoDatatable-'+v.noid).html(`
                        <div class="row" style="background: #90CAF9; padding-left: 15px; padding-right: 15px;">
                            <div class="col-md-12">
                                Page
                                <button id="prevpage" onclick="prevpage(`+v.noid+`)" class="btn btn-sm default prev `+prevdisabled+`" style="background-color: #2196F3;">
                                    <i class="fa fa-angle-left">
                                    </i>
                                </button>
                                <input type="text" id="valuepage" onkeyup="valuepage()" class="input-mini" value="`+valuepage+`" style="text-align: center; border: 0px; margin: 5px 5px 5px 5px">
                                <button id="nextpage" onclick="nextpage(`+v.noid+`)" class="btn btn-sm default next `+nextdisabled+`" style="background-color: #2196F3;">
                                    <i class="fa fa-angle-right">
                                    </i>
                                </button> of <span class="pagination-panel-total">`+(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)+`</span>
                                <span class="seperator">|</span>
                                Rec/Page 
                                <select id="alengthmenu-`+v.noid+`" onchange="alengthmenu(`+v.noid+`)" name="table_pagepanel_9921050101_length" aria-controls="table_pagepanel_9921050101" class="input-inline input-xsmall input-sm recofdt" style="border: 0px; margin: 5px 5px 5px 5px">
                                    `+options+`
                                </select>
                                <span class="seperator">|</span>Total `+settings._iRecordsTotal+` records
                                <input id="searchform-`+v.noid+`" onkeyup="searchform(`+v.noid+`)" value="`+tab_valuesearch[v.noid]+`" type="search" class="form-control input-inline input-sm clicked" placeholder="" aria-controls="table_pagepanel_9921050101" style="width: `+tab_searchtablewidth[v.noid]+`px; position: absolute; right: 0px; top: 4px; height: 80%; border: 0;">
                                <div onclick="searchtable(`+v.noid+`)" class="btn fa fa-search text-right" style="position: absolute; right: 0px; top: 4px; height: 80%; background: transparent; margin: 0px 0px 0px 0px; padding: 5px 0px 5px 0px">
                                    <i class="fa fa-search"></i>
                                </div>
                            </div>
                        </div>`)
                        console.log('settings '+v.noid)
                        console.log(settings)
                        console.log(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)
                        $('.alm').removeAttr('selected');
                        $('#alm'+settings._iDisplayLength).attr('selected', 'selected');
                        // console.log(valuepage)
                        $('.dataTables_info').css('display', 'none')
                        $('.dataTables_paginate').css('display', 'none')
                        // return '<"top dtfiler"<"">>';
                    },
                    "ajax": {
                        "url": "/get_tab_data_tmd",
                        "type": "POST",
                        "dataType":'json',
                        "data": {
                            "_token":$('#token').val(),
                            key: k-1,
                            id: v.idwidget,
                            table: maintable[v.idwidget],
                            selected_filter: selected_filter,
                            maincms: JSON.stringify(maincms),
                            data: localStorage.getItem('input-data-'+v.noid),
                            tablename: localStorage.getItem('tablename-'+v.noid),
                            panelnoid: v.noid,
                            masternoid: nownoid,
                            fieldnametab: fieldnametab[v.noid],
                            tablelookuptab: tablelookuptab[v.noid],
                            statusadd: statusadd,
                        },
                        beforeSend: function () {
                            $(".dataTables_scroll").LoadingOverlay("show");
                        },
                        complete: function (response) {
                            $(".dataTables_scroll").LoadingOverlay("hide");
                            
                            if (response.responseJSON.noids_tab[v.noid].length > 0) {
                                noids_tab[v.noid] = response.responseJSON.noids_tab[v.noid];
                                select_all_tab[v.noid] = response.responseJSON.select_all_tab[v.noid];
                            }
                            // alert('asd')
                            // $.each(maincms['widgetgridfield'][0], function(k, v) {
                            //     $('#example_baru th:nth-child('+k+')').attr('style', 'padding: 0px 5px 0px 5px; width: '+v.colwidth+'px;')
                            //     $('#example_baru td:nth-child('+k+')').attr('style', 'padding: 0px 5px 0px 5px; width: '+v.colwidth+'px;')
                            //     alert(document.querySelector('.dataTables_scrollHeadInner #thead-columns th:nth-child('+k+')').getAttribute('style'))
                            // })
                            // $('#example_baru .sorting_1').attr('style', 'width: 103px')
                        },
                    },
                    "pagingType": "input",
                    "createdRow": function(row, data, dataIndex) {
                        // console.log(data)
                        $(row).addClass('my_row');
                        $(row).addClass('row'+dataIndex);
                        
                        console.log('hwgf')
                        // console.log(<?= $hwgf ?>[v.noid])
                        $(row).css('height', <?= $hwgf ?>[v.noid]+'px');
                        $(row).attr('onmouseover', 'showButtonActionTab(this, '+dataIndex+')');
                        // alert(dataindex)
                        $(row).attr('onmouseout', 'resetCssTab()');
                        // $(row).attr('onmouseover', 'getPositionXY(this)');
                    },
                    "oLanguage":{
                        "loadingRecords": "Please wait - loading...",
                        "sProcessing": "<div id='loadernya'></div>",
                        "sInfo": "&nbsp;|&nbsp;Total&nbsp;_TOTAL_ records",
                        "sLengthMenu": "&nbsp;|&nbsp;Rec/Page&nbsp;_MENU_",
                        "sInfoFiltered": "",
                        "sInfoEmpty": "| No records found to show",
                        "sEmptyTable": "No data available in table",
                        "sZeroRecords": "No matching records found",
                        "sSearch": "<button onclick='toggleButtonSearch()' class='btn flat dtfilter_search_btn' style='background-color: white'><i class='fa fa-search'></i></button>",
                        // "oPaginate": {
                        //   "previous": "<i class='fa fa-angle-left'></i>",
                        //   "next": "<i class='fa fa-angle-right'></i>",
                        //   "page": "Page",
                        //   "pageOf": "of"
                        // },
                        "oPaginate": {
                            "sPrevious": "<i class='fa fa-angle-left'></i>",
                            "sNext": "<i class='fa fa-angle-right'></i> ",
                        // "sPage": "<i id='page_prev'>Page</i>",
                        // "sPageOf": "<span id='page_next'> of </span>"
                        }
                    },
                    footerCallback: function ( row, data, start, end, display ) {
                        var api = this.api(), data;
                        var intVal = function ( i ) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '')*1 :
                                typeof i === 'number' ?
                                    i : 0;
                        };
                        // var _split = function ( i ) {
                        //     return typeof i === 'string' ?
                        //         parseInt(i.replace('.', '')).toString() :
                        //         typeof i === 'number' ?
                        //             i : 0;
                        // };
                        var count = function ( i ) {
                            return api.column( 5 )
                                    .data()
                                    .reduce( function (a, b) {
                                        return intVal(end);
                                    }, 0 )
                        }
                        var sum = function ( i ) {
                            return i
                            var value = 0;
                            $.each(api.column( i ).data(), function(k, v) {
                                var _split = v.split('.');
                                var value2 = '';
                                $.each(_split, function(k2, v2) {
                                    value2 += v2
                                })
                                value += Number(value2)
                            })
                            return value
                        }
                        var addcommas = function addCommas(nStr) {
                            nStr += '';
                            var x = nStr.split('.');
                            var x1 = x[0];
                            var x2 = x.length > 1 ? '.' + x[1] : '';
                            var rgx = /(\d+)(\d{3})/;
                            while (rgx.test(x1)) {
                                x1 = x1.replace(rgx, '$1' + '.' + '$2');
                            }
                            return x1 + x2;
                        }

                        $.each(functionfootertab[v.noid], function(k, v) {
                            $( api.column(v.index).footer() ).html(api.column(v.index).data().reduce( function (a, b) { return addcommas(eval(v.function)) }, 0 ));
                        })
                        // console.log('try')
                        // console.log(row)
                        console.log('functionfootertab')
                        console.log(functionfootertab)
                    }
                });

                mytabtable[v.noid] = my_tab_table

                // my_tab_table = $('#tab-table-'+v.noid).DataTable({
                //     "scrollX": true,
                //     "processing": true,
                //     "serverSide": true,
                //     "info": false,
                //     // "order": [[ 0, "asc" ]],
                //     "columnDefs": [ 
                //         { 
                //             orderable: false,
                //             targets: colsorted
                //             // targets: [0,1,30]
                //         }
                //     ],
                //     "pagingType": "input",
                //     "searching": false,
                //     "ajax": {
                //         "url": "/get_tab_data_tmd",
                //         "type": "POST",
                //         "dataType":'json',
                //         "data": {
                //             "_token":$('#token').val(),
                //             key: k-1,
                //             id: v.idwidget,
                //             table: 'fincashbankdetail',
                //             selected_filter: selected_filter,
                //             maincms: JSON.stringify(maincms),
                //             data: localStorage.getItem('input-data-'+v.noid),
                //             tablename: localStorage.getItem('tablename-'+v.noid),
                //             panelnoid: v.noid,
                //             masternoid: nownoid,
                //             fieldnametab: fieldnametab[v.noid],
                //             statusadd: statusadd,
                //         },
                //         beforeSend: function () {
                //             $(".dataTables_scroll").LoadingOverlay("show");
                //         },
                //         complete: function () {
                //             $(".dataTables_scroll").LoadingOverlay("hide");
                //         },
                //     },
                //     // "sPaginationType":"input",
                //     "dom": '<"top dtfiler"pli<""f><"clear">>rt ',
                //     "oLanguage":{
                //         "loadingRecords": "Please wait - loading...",
                //         "sProcessing": "<div id='loadernya'></div>",
                //         "sInfo": "&nbsp;|&nbsp;Total&nbsp;_TOTAL_ records",
                //         "sLengthMenu": "&nbsp;|&nbsp;Rec/Page&nbsp;_MENU_",
                //         "sInfoFiltered": "",
                //         "sInfoEmpty": "| No records found to show",
                //         "sEmptyTable": "No data available in table",
                //         "sZeroRecords": "No matching records found",
                //         "sSearch": `<button onclick="toggleButtonSearchTab('`+v.noid+`')" class="btn flat dtfilter_search_btn" style="background-color: white"><i class="fa fa-search"></i></button>`,
                //         // "oPaginate": {
                //         //   "previous": "<i class='fa fa-angle-left'></i>",
                //         //   "next": "<i class='fa fa-angle-right'></i>",
                //         //   "page": "Page",
                //         //   "pageOf": "of"
                //         // },
                //         "oPaginate": {
                //             "sPrevious": "<i class='fa fa-angle-left'></i>",
                //             "sNext": "<i class='fa fa-angle-right'></i> ",
                //         // "sPage": "<i id='page_prev'>Page</i>",
                //         // "sPageOf": "<span id='page_next'> of </span>"
                //         }
                //     },
                //     "createdRow": function(row, data, dataIndex) {
                //         // console.log(data)
                //         $(row).addClass('my_row');
                //         $(row).addClass('row'+dataIndex);
                //         // $(row).css('height', '5px');
                //         $(row).attr('onmouseover', 'showButtonActionTab(this, '+dataIndex+')');
                //         $(row).attr('onmouseout', 'resetCssTab()');
                //         // $(row).attr('onmouseover', 'getPositionXY(this)');
                //     },
                //     deferRender: true,
                //     // footerCallback: function ( row, data, start, end, display ) {
                //     //     var api = this.api(), data;
                //     //     var intVal = function ( i ) {
                //     //         return typeof i === 'string' ?
                //     //             i.replace(/[\$,]/g, '')*1 :
                //     //             typeof i === 'number' ?
                //     //                 i : 0;
                //     //     };
                //     //     var monTotal = api.column( 1 )
                //     //         .data()
                //     //         .reduce( function (a, b) {
                //     //             return intVal(a) + intVal(b);
                //     //         }, 0 );

                //     //     $( api.column( 3 ).footer() ).html(monTotal);
                //     // }
                // });
                
                new $.fn.dataTable.Buttons( my_tab_table, {
                    buttons: [
                        {
                            autoFilter: true,
                            text:      '<i class="fa fa-trash"></i> Delete Selected',
                            titleAttr: 'Delete Selected',
                            className: ' dropdown-item flat',
                            action:  function(e, dt, button, config) {
                                if (another_selecteds[v.noid].length == 0) {
                                    alert("There's no selected")
                                    // console.log(another_selecteds)
                                } else {
                                        // alert(another_selecteds)
                                        // alert(v.maintable)
                                    if (confirm('Are you sure you want to delete this?')) {
                                        removeSelectedTab(another_selecteds[v.noid])
                                    }
                                }
                            },
                        },{
                            autoFilter: true,
                            text:      '<i class="fa fa-trash"></i> Delete All',
                            titleAttr: 'Delete All',
                            className: ' dropdown-item flat',
                            action:  function(e, dt, button, config) {
                                if (noids_tab[v.noid].length == 0) {
                                    alert("There's no selected")
                                } else {
                                    if (confirm('Are you sure you want to delete this?')) {
                                        removeSelectedTab(noids_tab[v.noid])
                                    }
                                }
                            },
                        },{
                            extend:    'excel',
                            autoFilter: true,
                            text:      '<i class="fa fa-file-excel"></i> Export to Excel',
                            titleAttr: 'Export to Excel',
                            title: v.widgetcaption,
                            className: ' dropdown-item flat',
                            exportOptions: {
                                modifier: {
                                    search: 'applied',
                                    order: 'applied'
                                },
                                columns: [ 1,2,3, ':visible' ]
                                //                  <a style="font-size: 14px;min-width: 175px;
                                // text-align: left;" href="javascript:;"  class="dropdown-item">
                                // <i style="margin-top:10px;margin-right: 5px;" class="fa fa-file-excel"></i> Export to Excel </a>
                                                // columns: [ 0, ':visible' ]
                            },
                            action:  function(e, dt, button, config) {
                            // $('.loading').fadeIn();
                                var that = this;
                                setTimeout(function () {
                                    $.fn.dataTable.ext.buttons.excelHtml5.action.call(that,e, dt, button, config);
                                    // console.log(that);
                                    // console.log(e);
                                    // console.log(dt);
                                    // console.log(button);
                                    // console.log(config);
                                    $('.loading').fadeOut();
                                },500);
                            },
                        }
                    ]
                    // infoDatatable()
                });
                // infoDatatable()
                $('#tab-table-'+v.noid+'_first').hide();
                $('#tab-table-'+v.noid+'_last').hide();
                
                my_tab_table.buttons().container().appendTo('#another-space-button-export-'+v.noid);

                getForm(statusadd, v.idwidget, 'tab-form-div-'+v.noid);

                // $('#'+element_id).LoadingOverlay("hide");
                $('#another-card').show();
            }
        })

        if (!statusadd) {
            // $('#another-card').show();
        }
    }

    function destroyDatatable() {
        // $('#example_baru').DataTable().destroy();
        $('#table_pagepanel_9921050101').DataTable().destroy();
    }

    function destroyTabDatatable() {
        $.each(maincms['panel'], function(k, v) {
            if (k > 0) {
                $('#tab-table-'+v.noid).DataTable().destroy();
            }
        })
    }
// 

    function toggleButtonCollapse(noid) {
        if (noid) {
            $('#tab-widget-content-'+noid+' .card .card-body').toggleClass('collapse');
        } else {
            $('#kt_content .container .card .card-body').toggleClass('collapse');
        }
        // $('#kt_content .container .card .card-body').toggleClass('collapse').delay('slow').fadeOut();
    }

    function toggleButtonSearch() {
        $('#example_baru_filter label input').addClass('form-control flat')
        $('#example_baru_filter label input').toggleClass('clicked');
    }

    function toggleButtonSearchTab(element_id) {
        $('#tab-table-'+element_id+'_filter label input').addClass('form-control flat')
        $('#tab-table-'+element_id+'_filter label input').toggleClass('clicked');
    }

    function filterData(element_id, element_class, key, buttoncaption, classcolor, groupdropdown, is_all, table='', operand='', fieldname='', operator='', value='') {
        selected_filter.operand[key] = operand;
        selected_filter.fieldname[key] = fieldname;
        selected_filter.operator[key] = operator;
        selected_filter.value[key] = value;

        if (groupdropdown == 1) {
            $('#filter-dropdown-active-'+key).text(buttoncaption)
            // alert(buttoncaption)
        } else {
            $('.'+element_class).css('background', '#E3F2FD');
            $('.'+element_class).css('color', classcolor);
            $('.'+element_class).css('font-weight', 'normal');
            $('#'+element_id).css('background', '#90CAF9');
            $('#'+element_id).css('color', 'black');
            $('#'+element_id).css('font-weight', 'bold');
        }
        // console.log(groupdropdown)
        
        destroyDatatable()
        getDatatable()
    }

    function getSlider(id) {
        var owl = $('.owl-carousel').owlCarousel({
            loop:true,
            smartSpeed: 100,
            autoplay: true,
            autoplaySpeed: 100,
            mouseDrag: false,
            margin:10,
            animateIn: 'slideInUp',
            animateOut: 'fadeOut',
            nav:false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });

        $.ajax({
            type: "POST",
            header:{
            'X-CSRF-TOKEN':$('#token').val()
            },
            url: "/get_slide",
            dataType: "json",
            data:{
                "_token":$('#token').val(),
                "id_slider": id
            },
            success: function (response) {
                // console.log(response.hasil);
                var base_url = '{{ url('/') }}';
                if(isNaN(base_url)) {
                    base_url = '';
                }
                var name1 = '';
                var name2 = '';

                $.each(response.hasil, function(i, item) {
                    // console.log(item.mainimage);
                    name1 = 'public/'+item.mainimage;

                    // console.log(name1);
                    var random_num =  Math.floor(Math.random()*60);
                    owl.trigger('add.owl.carousel', [jQuery('<div class="notification-message"> <img src="'+base_url+name1+'" class="user-image" alt=""> <div class="user-name">'+ item.newstitle+' <span class="lighter">from '+item.idnews+'</span></div> <div class="bought-details">Bought This <br>'+random_num+' minutes ago</div> </div>')]);
                });
                owl.trigger('refresh.owl.carousel');
            },
        });
    }

    function chartBatang(response,id_cart) {
        var dataatas = response.hasil;
        var dataProvideratas = [];// this variable you have to pass in dataProvider inside chart
        for(var key in dataatas) {
            dataProvideratas.push({
                category: dataatas[key]['categories'],
                visits: dataatas[key]['jumlahdata'],
            });
        }
        var chart = AmCharts.makeChart("kt_amcharts_"+id_cart, {
            "rtl": KTUtil.isRTL(),
            "type": "serial",
            "hideCredits":true,
            "theme": "light",
            "dataProvider": dataProvideratas,
            "valueAxes": [{
                "gridColor": "#FFFFFF",
                "gridAlpha": 0.2,
                "dashLength": 0
            }],
            "series": [{
                "type": "LineSeries",
                "stroke": "#ff0000",
                "strokeWidth": 3
            }],
            "valueAxes": [ {
                "position": "left",
                "title": "Jumlah Data"
            }, {
                "position": "bottom",
                "title": response.widget.widget_widgetcaption
            } ],
            "legend": {
                "useGraphSettings": true
            },
            "titles": [
                {
                    "size": 15,
                    "text":response.widget.widget_widgetcaption
                },
                {
                    "text": response.widget.widget_widgetsubcaption,
                    "bold": false
                }
            ],
            "gridAboveGraphs": true,
            "startDuration": 1,
            "graphs": [{
                "balloonText": "[[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "visits",
                "title": response.widget.widget_widgetcaption
            }],
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "category",
            "categoryAxis": {
                "gridPosition": "start",
            },
            "export": {
                "enabled": true
            }
        });
    }

    function chartPie(response,id_cart) {
        var data = response.hasil;
        var dataProvider = [];// this variable you have to pass in dataProvider inside chart
        for(var key in data) {
            dataProvider.push({
            country: data[key]['messagestatus'],
            litres: data[key]['jumlahdata'],
            });
        }

        var chart = AmCharts.makeChart("kt_amcharts_"+id_cart,{
            "type"    : "pie",
            "hideCredits"    : true,
            "theme"    : "light",
            "addClassNames": true,
            "titles": [
                {
                    "size": 15,
                    "text": response.widget.widget_widgetcaption
                },
                {
                    "text": response.widget.widget_widgetsubcaption,
                    "bold": false
                }
            ],
            // "pullOutDuration": 0,
            // "pullOutRadius": 0,

            "labelsEnabled": false,
            "titleField"  : "country",
            "valueField"  : "litres",
            "dataProvider"  : dataProvider,
            "numberFormatter": {
                "precision": -1,
                "decimalSeparator": ",",
                "thousandsSeparator": ""
            },
            "legend": {
                "align": "center",
                "position": "bottom",
                "marginRight": 0,
                "labelText": "[[title]]",
                "valueText": "",
                "valueWidth": 50,
                "textClickEnabled": true
            },
            "export": {
                "enabled": true
            }

        });
    }

    function setCheckbox(element_id) {
        if ($('#'+element_id).attr('checked')) {
            $('#'+element_id).val(0);
        } else {
            $('#'+element_id).val(1);
        }
    }

    $("#savedata").click(function(e){
        e.preventDefault();
        // alert(nextnoid)
        // return false
        // localStorage.setItem('input-data', response)
        // console.log($("#formdepartement").serializeArray())
        var input_data_master = {};
        var mstrtbl = maincms['widget'][0].maintable;
        var onrequired = false;
        var errormessage = '';
        $.each($("#formdepartement").serializeArray(), function(k, v) {
            var formcaption = $('#'+mstrtbl+'-'+v.name).attr('data-'+mstrtbl+'-formcaption');
            if (v.name == 'noid') {
                input_data_master[v.name] = nextnoid
            } else {
                input_data_master[v.name] = v.value

                if (statusadd) {
                    if ($('#'+mstrtbl+'-'+v.name).attr('data-'+mstrtbl+'-newreq') == 1) {
                        if (v.value == '') {
                            onrequired = true;
                            errormessage += formcaption+' is required!<br>'
                        }
                    }
                } else {
                    if ($('#'+mstrtbl+'-'+v.name).attr('data-'+mstrtbl+'-editreq') == 1) {
                        if (v.value == '') {
                            onrequired = true;
                            errormessage += formcaption+' is required!<br>'
                        }
                    }
                }
            }
        })
        
        if (onrequired) {
            $.notify({
                message: errormessage
            },{
                type: 'danger'
            });
            return false;
        }


        if (!statusadd) {
            input_data_master['noid'] = nownoid;
        }
        
        localStorage.setItem('input-data', JSON.stringify(input_data_master))

        // var maincms_widget = new Array();
        // $.each(maincms['widget'], function(k, v) {
        //     maincms_widget[v.noid] = v;
        // })

        var input_data_detail = {};
        $.each(maincms['panel'], function(k, v) {
            if (k > 0) {
                input_data_detail[v.noid] = {
                    'data': JSON.parse(localStorage.getItem('input-data-'+v.noid)),
                    'table': maincms_widget[v.idwidget].maintable
                }
            }
        })
        // console.log(JSON.stringify(maincms))
        // console.log(input_data_detail)
        // return false
        
        /////////////////////////////////

        var kode = $("#kode").val();
        var nama = $("#nama").val();
        var namatable =$('#namatable').val();
        var formdata = $("#formdepartement").serialize();
        var formdatatab = [];
        
        if (statusadd) {
            $.each(maincms['widget'], function(k, v) {
                if (k > 0) {
                    formdatatab.push($('#form-'+v.maintable).serialize())
                }
            })
        }
        // console.log('input_data_detail')
        // console.log(input_data_detail)
        // return false

        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            url: "/save_tmd",
            dataType: "json",
            data:{
                "_token":$('#token').val(),
                "data" :{
                    'data' : formdata,
                    'datatab' : formdatatab,
                    'tabel': namatable,
                },
                "table": namatable,
                "editid": editid,
                "statusadd": statusadd,
                "masternoid": nownoid,
                maincms: JSON.stringify(maincms),
                "input_data": {
                    "master": input_data_master,
                    "detail": JSON.stringify(input_data_detail),
                }
            },
            success:function(result){
                // return false
                if (result.status == 'add') {
                    // $("#notiftext").text('Data  Berhasil di Tambah');
                    $.notify({
                        message: 'Add data has been succesfullly.' 
                    },{
                        type: 'success'
                    });
                } else {
                    // $("#notiftext").text('Data  Berhasil di Edit');
                    $.notify({
                        message: 'Edit data has been succesfullly.' 
                    },{
                        type: 'success'
                    });
                }

                //
                $('#another-card').hide();
                divform = '';
                another_selecteds = [];
                destroyTabDatatable()

                document.getElementById("formdepartement").reset();
                resetData()
                refresh();
                // $('#divsucces').show(0).delay(5000).hide(0);
                var x = document.getElementById("from-departement");
                var x2 = document.getElementById("tabledepartemen");
                var y = document.getElementById("tool-kedua");
                var y2 = document.getElementById("tool-pertama");
                x.style.display = "none";
                x2.style.display = "block";
                y.style.display = "none";
                y2.style.display = "block";
            }
        });
    });

    function refresh(noid) {
        if (noid) {
            $('#tab-table-'+noid).DataTable().ajax.reload();
        } else {
            $('#table_pagepanel_9921050101').DataTable().ajax.reload();
        }
    }

    function refreshTab() {
        destroyTabDatatable()
        getCmsTabDatatable()
    }

    function changeInput() {
        // console.log($('#formdepartement').serialize())
    }

    function getNextNoid() {
        $.ajax({
            url: '/get_next_noid_tmd',
            type: "POST",
            dataType: 'json',
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            data: {
                "table" : maincms['widget'][0]['maintable'],
                "_token": $('#token').val(),
                maincms: JSON.stringify(maincms)
            },
            success: function (response) {
                nextnoid = response
                localStorage.setItem('input-data-noid', response)
                // console.log(localStorage.getItem('input-data-noid'))
                // alert(nextnoid)
            }
        })
    }

    function getNextNoidTab(tablename, panel_noid) {
        $.ajax({
            url: '/get_next_noid_tmd',
            type: "POST",
            dataType: 'json',
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            data: {
                "table" : tablename,
                "_token": $('#token').val(),
                maincms: JSON.stringify(maincms)
            },
            success: function (response) {
                nextnoidtab[panel_noid] = response
                // console.log(nextnoidtab[panel_noid])
            }
        })
    }

    function cancelDataTab(panel_noid) {
        $('#tab-form-'+panel_noid).hide()
        $('#add-data-'+panel_noid).show()
        $('#save-data-'+panel_noid).hide()
        $('#cancel-data-'+panel_noid).hide()
        $('#tab-table-'+panel_noid+'_wrapper').show()
        $('#tab-appendInfoDatatable-'+panel_noid).show()
        $('#ts-'+panel_noid).show()
        $('#space_filter_button-'+panel_noid).show();
        $('#tool_refresh-'+panel_noid).show();
        $('#tool_filter-'+panel_noid).show()
        $('#tool_refresh_up-'+panel_noid).show();
    }

    function addDataTab(panel_noid) {
        console.log(nextnoidtab)
        resetDataTab(panel_noid)
        statusaddtab[panel_noid] = true;
        $('#tab-form-'+panel_noid).show()
        $('#add-data-'+panel_noid).hide()
        $('#save-data-'+panel_noid).show()
        $('#cancel-data-'+panel_noid).show()
        $('#tab-table-'+panel_noid+'_wrapper').hide()
        $('#tab-appendInfoDatatable-'+panel_noid).hide()
        $('#ts-'+panel_noid).hide()
        $('#space_filter_button-'+panel_noid).hide();
        $('#tool_refresh-'+panel_noid).hide();
        $('#tool_filter-'+panel_noid).hide()
        $('#tool_refresh_up-'+panel_noid).hide();
    }

    function addData() {
        resetForm();
        resetData();
        // alert('developing');
        // return false
        statusadd = true;
        statusdetail = false;
        // alert(maincms['widget'][0]['noid'])
        getNextNoid();
        $.each(maincms['widget'], function(k, v) {
            getNextNoidTab(v.maintable, v.noid)
        })
        getForm(statusadd, maincms['widget'][0]['noid'], 'divform');
        getCmsTab(statusadd);
        getCmsTabDatatable();
        // getFormTab();
        // $('#another-card').show();
        // var x = document.getElementById("from-departement");
        // var x2 = document.getElementById("tabledepartemen");
        // var y = document.getElementById("tool-kedua");
        // var y2 = document.getElementById("tool-pertama");
        // var tool_refresh = document.getElementById("tool_refresh");
        // var tool_filter = document.getElementById("tool_filter");
        // var tool_refresh_up = document.getElementById("tool_refresh_up");
        // var space_filter_button = $('#space_filter_button');

        $.each(roles.fieldname, function(k, v) {
            if (roles.newhidden[k] == 1) {
                $('#div-'+v).css('display', 'none')
            } else {
                $('#div-'+v).css('display', 'block')
            }
        })

        // if (x.style.display === "none") {
        //     x.style.display = "block";
        //     x2.style.display = "none";
        //     y.style.display = "block";
        //     y2.style.display = "none";
        //     tool_refresh.style.display = "none";
        //     tool_filter.style.display = "none";
        //     tool_refresh_up.style.display = "none";
        //     space_filter_button.css('display', 'none');
        // } else {
        //     x.style.display = "none";
        //     x2.style.display = "block";
        //     space_filter_button.css('display', 'block');
        // }
        $('#from-departement').show();
        $('#tabledepartemen').hide();
        $('#tool-kedua').show();
        $('#tool-pertama').hide();
        $('#space_filter_button').hide();
        $('#tool_refresh').hide();
        $('#tool_filter').hide()
        $('#tool_refresh_up').hide();
    }

    function resetForm() {
        $('#divform').html('')
    }

    function resetData() {
        $.each(maincms['panel'], function(k, v) {
            if (k == 0) {
                localStorage.removeItem('input-data');
            } else {
                localStorage.removeItem('input-data-'+v.noid);
            }
        })
    }

    function resetDataTab(panel_noid) {
        var maincms_widgetgridfield = new Array();
        $.each(maincms['widgetgridfield'], function(k, v) {
            $.each(v, function(k2, v2) {
                if (k2 == 0) {
                    maincms_widgetgridfield[panel_noid] = new Array();    
                }
                maincms_widgetgridfield[panel_noid][v2.fieldname] = v2;
            })
        })
        console.log('default')
        console.log(maincms_widgetgridfield)
        
        $.each(JSON.parse(localStorage.getItem('input-data-'+panel_noid)), function(k, v) {
            $.each(fieldnametab[panel_noid], function(k2, v2) {
                if (!v2.search('_lookup')) {
                    $("#tab-form-div-"+panel_noid+" div div #"+v2).attr('disabled', false);
                    $("#tab-form-div-"+panel_noid+" div div #"+v2).val(maincms_widgetgridfield[panel_noid][v2].coldefault);
                }
            })
        })
    }

    function cancelData() {
        //
        $('#another-card').hide();
        divform = '';
        another_selecteds = [];
        destroyTabDatatable()
        // var x = document.getElementById("from-departement");
        // var x2 = document.getElementById("tabledepartemen");
        // var y = document.getElementById("tool-kedua");
        // var y2 = document.getElementById("tool-pertama");
        // var y3 = document.getElementById("tool-ketiga");
        // var tool_refresh = document.getElementById("tool_refresh");
        // var tool_filter = document.getElementById("tool_filter");
        // var tool_refresh_up = document.getElementById("tool_refresh_up");
        // var space_filter_button = $('#space_filter_button');

        // tool_refresh.style.display = "block";
        // tool_filter.style.display = "block";
        // tool_refresh_up.style.display = "block";
        // x.style.display = "none";
        // x2.style.display = "block";
        // y.style.display = "none";
        // y2.style.display = "block";
        // y3.style.display = "none";
        // space_filter_button.css('display', 'block');

        $('#from-departement').hide();
        $('#tabledepartemen').show();
        $('#tool-kedua').hide();
        $('#tool-pertama').show();
        $('#space_filter_button').show();
        $('#tool_refresh').show();
        if (!maincms['widgetgrid'][0]['panelfilter']) {
            $('#tool_filter').hide()
        } else {
            $('#tool_filter').show();
        }
        $('#tool_refresh_up').show();

        document.getElementById("formdepartement").reset();
        resetForm()
        resetData()
    }

    function view(id) {
        $.when(
            $.ajax({
                url: '/find_tmd',
                type: "POST",
                dataType: 'json',
                header:{
                    'X-CSRF-TOKEN':$('#token').val()
                },
                data: {
                    "id" : id,
                    "namatable" : $("#namatable").val(),
                    "_token": $('#token').val(),
                    maincms: JSON.stringify(maincms)
                },
                success: function (response) {
                    return response;
                }
            })
        ).then(function(responsefind) {
            statusadd = false;
            statusdetail = false;
            nownoid = id;
            $.each(maincms['widget'], function(k, v) {
                getNextNoidTab(v.maintable, v.noid)
            })
            resetForm();
            resetData();
            getCmsTab(statusadd);
            getForm(statusadd, maincms['widget'][0]['noid'], 'divform', responsefind);
            // alert(response)
            // getCmsTabDatatable();


            editid = id;

            // var x = document.getElementById("from-departement");
            // var x2 = document.getElementById("tabledepartemen");
            // var y = document.getElementById("tool-kedua");
            // var y2 = document.getElementById("tool-pertama");
            // var space_filter_button = $('#space_filter_button');

            // if (x.style.display === "none") {
            //     x.style.display = "block";
            //     x2.style.display = "none";
            //     y.style.display = "block";
            //     y2.style.display = "none";
            //     space_filter_button.css('display', 'none');
            // } else {
            //     x.style.display = "none";
            //     x2.style.display = "block";
            //     space_filter_button.css('display', 'block');
            // }

            $('#from-departement').show();
            $('#tabledepartemen').hide();
            $('#tool-kedua').show();
            $('#tool-pertama').hide();
            $('#space_filter_button').hide();
            $('#tool_refresh').hide();
            $('#tool_filter').hide()
            $('#tool_refresh_up').hide();

            // $.ajax({
            //     url: '/find_tmd',
            //     type: "POST",
            //     dataType: 'json',
            //     header:{
            //         'X-CSRF-TOKEN':$('#token').val()
            //     },
            //     data: {
            //         "id" : id,
            //         "namatable" : $("#namatable").val(),
            //         "_token": $('#token').val(),
            //         maincms: JSON.stringify(maincms)
            //     },
            //     success: function (response) {
                    nownoid = responsefind.nownoid;

                    var maincms_panel = new Array();
                    $.each(maincms['panel'], function(k, v) {
                        $.each(maincms['widget'], function(k2, v2) {
                            if (v.idwidget == v2.noid) {
                                maincms_panel[v.noid] = v2;
                            }
                        })
                    })

                    // console.log(response.result_detail)
                    var key = 0;
                    $.each(responsefind.result_detail, function(k, v) {
                        if (key == 0) {
                            nownoidtab = maincms_panel[k].noid
                        }
                        // alert(k)
                        // console.log(v)
                        // localStorage.removeItem('input-data-'+k)
                        localStorage.setItem('input-data-'+k, JSON.stringify(v))
                        localStorage.setItem('tablename-'+k, maincms_panel[k].maintable)
                        // console.log('aku cek localstoragenya')
                        // console.log(JSON.parse(localStorage.getItem('input-data-'+k)))
                        // console.log('input-data-'+k)
                        key++;
                    })
                    
                    // destroyTabDatatable('fincashbankdetail')
                    getCmsTabDatatable()
                    // alert(response['noid'])
                    // $('#noid').val(id)
                    // $.each(response, function(k, v) {
                    //     $("#"+v.field).val(v.value);
                    //     if (v.disabled) {
                    //         $("#"+v.field).attr('disabled', 'disabled');
                    //     }
                    //     if (v.edithidden) {
                    //         $("#div-"+v.field).css('display', 'none');
                    //     }
                    // });
                    // console.log(response)
                // }
            // });
        });
    }

    function viewTab(another_table, another_id, id, panel_noid) {
        resetDataTab(panel_noid)
        statusaddtab[panel_noid] = false;
        
        var maincms_panel = new Array();
        $.each(maincms['panel'], function(k, v) {
            $.each(maincms['widget'], function(k2, v2) {
                if (v.idwidget == v2.noid) {
                    maincms_panel[v.noid] = v2;
                }
            })
        })
        
        $.each(JSON.parse(localStorage.getItem('input-data-'+panel_noid)), function(k, v) {
            if (v.noid == another_id) {
                nownoidtab = v['noid'];
                $.each(fieldnametab[panel_noid], function(k2, v2) {
                    $("#tab-form-div-"+panel_noid+" div div #"+v2).attr('disabled', true);
                    $("#tab-form-div-"+panel_noid+" div div #"+v2).val(v[v2]);
                })
            }
        })

        $('#tab-form-'+panel_noid).show()
        $('#add-data-'+panel_noid).hide()
        $('#save-data-'+panel_noid).show()
        $('#cancel-data-'+panel_noid).show()
        $('#tab-table-'+panel_noid+'_wrapper').hide()
        $('#tab-appendInfoDatatable-'+panel_noid).hide()
        $('#ts-'+panel_noid).hide()
        $('#tool_refresh-'+panel_noid).hide();
        $('#tool_filter-'+panel_noid).hide()
        $('#tool_refresh_up-'+panel_noid).hide();
    }

    function editTab(another_table, another_id, id, panel_noid) {
        resetDataTab(panel_noid)
        statusaddtab[panel_noid] = false;
        // console.log(statusaddtab)
        // alert(another_table+' '+another_id+' '+id)
        var maincms_panel = new Array();
        $.each(maincms['panel'], function(k, v) {
            $.each(maincms['widget'], function(k2, v2) {
                if (v.idwidget == v2.noid) {
                    maincms_panel[v.noid] = v2;
                }
            })
        })
        // console.log(maincms_panel)
        // console.log(panel_noid)
        // console.log(maincms_panel[panel_noid].noid)

        // console.log(JSON.parse(localStorage.getItem('input-data-'+panel_noid)))
        // console.log(fieldnametab)
        $.each(JSON.parse(localStorage.getItem('input-data-'+panel_noid)), function(k, v) {
            if (v.noid == another_id) {
                nownoidtab = v['noid'];
                $.each(fieldnametab[panel_noid], function(k2, v2) {
                    var fieldsplit = v2.split('_lookup');

                    if (fieldsplit.length == 2) {
                        $("#tab-form-div-"+panel_noid+" div div div #"+localStorage.getItem('tablename-'+panel_noid)+'-'+fieldsplit[0]).val('disabled', false);
                        $("#tab-form-div-"+panel_noid+" div div div #"+localStorage.getItem('tablename-'+panel_noid)+'-'+fieldsplit[0]).val(v[fieldsplit[0]]).trigger('change');
                    } else {
                        $("#tab-form-div-"+panel_noid+" div div #"+localStorage.getItem('tablename-'+panel_noid)+'-'+v2).attr('disabled', false);
                        $("#tab-form-div-"+panel_noid+" div div #"+localStorage.getItem('tablename-'+panel_noid)+'-'+v2).val(v[v2]);
                    }

                    // console.log("#"+v2+'     '+v[v2])
                    // if (v.disabled) {
                    //     $("#"+v.field).attr('disabled', 'disabled');
                    // }
                    // if (v.edithidden) {
                    //     $("#div-"+v.field).css('display', 'none');
                    // }
                })
            }
        })
        // getForm(statusadd, maincms_panel[panel_noid].noid, 'tab-form-div-'+panel_noid);

        $('#tab-form-'+panel_noid).show()
        $('#add-data-'+panel_noid).hide()
        $('#save-data-'+panel_noid).show()
        $('#cancel-data-'+panel_noid).show()
        $('#tab-table-'+panel_noid+'_wrapper').hide()
        $('#tab-appendInfoDatatable-'+panel_noid).hide()
        $('#ts-'+panel_noid).hide()
        $('#space_filter_button-'+panel_noid).hide();
        $('#tool_refresh-'+panel_noid).hide();
        $('#tool_filter-'+panel_noid).hide()
        $('#tool_refresh_up-'+panel_noid).hide();
        console.log('masok jos')
    }

    function edit(id) {
        $.when(
            $.ajax({
                url: '/find_tmd',
                type: "POST",
                dataType: 'json',
                header:{
                    'X-CSRF-TOKEN':$('#token').val()
                },
                data: {
                    "id" : id,
                    "namatable" : $("#namatable").val(),
                    "_token": $('#token').val(),
                    maincms: JSON.stringify(maincms)
                },
                success: function (response) {
                    return response;
                }
            })
        ).then(function(responsefind) {
            statusadd = false;
            statusdetail = false;
            nownoid = id;
            $.each(maincms['widget'], function(k, v) {
                getNextNoidTab(v.maintable, v.noid)
            })
            resetForm();
            resetData();
            getCmsTab(statusadd);
            getForm(statusadd, maincms['widget'][0]['noid'], 'divform', responsefind);
            // alert(response)
            // getCmsTabDatatable();


            editid = id;

            // var x = document.getElementById("from-departement");
            // var x2 = document.getElementById("tabledepartemen");
            // var y = document.getElementById("tool-kedua");
            // var y2 = document.getElementById("tool-pertama");
            // var space_filter_button = $('#space_filter_button');

            // if (x.style.display === "none") {
            //     x.style.display = "block";
            //     x2.style.display = "none";
            //     y.style.display = "block";
            //     y2.style.display = "none";
            //     space_filter_button.css('display', 'none');
            // } else {
            //     x.style.display = "none";
            //     x2.style.display = "block";
            //     space_filter_button.css('display', 'block');
            // }

            $('#from-departement').show();
            $('#tabledepartemen').hide();
            $('#tool-kedua').show();
            $('#tool-pertama').hide();
            $('#space_filter_button').hide();
            $('#tool_refresh').hide();
            $('#tool_filter').hide()
            $('#tool_refresh_up').hide();

            // $.ajax({
            //     url: '/find_tmd',
            //     type: "POST",
            //     dataType: 'json',
            //     header:{
            //         'X-CSRF-TOKEN':$('#token').val()
            //     },
            //     data: {
            //         "id" : id,
            //         "namatable" : $("#namatable").val(),
            //         "_token": $('#token').val(),
            //         maincms: JSON.stringify(maincms)
            //     },
            //     success: function (response) {
                    nownoid = responsefind.nownoid;

                    var maincms_panel = new Array();
                    $.each(maincms['panel'], function(k, v) {
                        $.each(maincms['widget'], function(k2, v2) {
                            if (v.idwidget == v2.noid) {
                                maincms_panel[v.noid] = v2;
                            }
                        })
                    })

                    // console.log(response.result_detail)
                    var key = 0;
                    $.each(responsefind.result_detail, function(k, v) {
                        if (key == 0) {
                            nownoidtab = maincms_panel[k].noid
                        }
                        // alert(k)
                        // console.log(v)
                        // localStorage.removeItem('input-data-'+k)
                        localStorage.setItem('input-data-'+k, JSON.stringify(v))
                        localStorage.setItem('tablename-'+k, maincms_panel[k].maintable)
                        select_all_tab[k] = new Array();
                        // console.log('aku cek localstoragenya')
                        // console.log(JSON.parse(localStorage.getItem('input-data-'+k)))
                        // console.log('input-data-'+k)
                        key++;
                    })
                    
                    // destroyTabDatatable('fincashbankdetail')
                    getCmsTabDatatable()
                    // alert(response['noid'])
                    // $('#noid').val(id)
                    // $.each(response, function(k, v) {
                    //     $("#"+v.field).val(v.value);
                    //     if (v.disabled) {
                    //         $("#"+v.field).attr('disabled', 'disabled');
                    //     }
                    //     if (v.edithidden) {
                    //         $("#div-"+v.field).css('display', 'none');
                    //     }
                    // });
                    // console.log(response)
                // }
            // });
        });
    }

    function removeTab(another_table, another_id, id, panel_noid) {
        if (confirm('Are you sure you want to delete this?')) {
            nownoidtab = another_id;
            current_idd = localStorage.getItem('input-data-'+panel_noid);

            var replace_current_idd = new Array();
            var key = 0;
            $.each(JSON.parse(current_idd), function(k, v) {
                if (v.noid != nownoidtab) {
                    replace_current_idd[key] = v;
                    key++;
                }
            })

            refreshSumTab(id, panel_noid)

            localStorage.removeItem('input-data-'+panel_noid);
            localStorage.setItem('input-data-'+panel_noid, JSON.stringify(replace_current_idd))

            $.notify({
                message: 'Delete data has been succesfullly.' 
            },{
                type: 'success'
            });
            refreshTab();
        }

        console.log(localStorage.getItem('input-data-'+panel_noid))
        return false





        // console.log(id);return false;
        if (confirm('Are you sure you want to delete this?')) {
            $.ajax({
                url: '/delete_tab_tmd',
                type: "POST",
                header:{
                    'X-CSRF-TOKEN':$('#token').val()
                },
                data: {
                    "id" : id,
                    "another_id" : another_id,
                    "another_table" : another_table,
                    "_token": $('#token').val(),
                },
                success: function () {
                    // $("#notiftext").text('Data  Berhasil di hapus');
                    $.notify({
                        message: 'Delete data has been succesfullly.' 
                    },{
                        type: 'success'
                    });
                    refreshTab();
                    // $('#divsucces').show(0).delay(5000).hide(0);
                    // var x = document.getElementById("from-departement");
                    // var x2 = document.getElementById("tabledepartemen");
                    // var y = document.getElementById("tool-kedua");
                    // var y2 = document.getElementById("tool-pertama");
                    // x.style.display = "none";
                    // x2.style.display = "block";
                    // y.style.display = "none";
                    // y2.style.display = "block";
                }
            });
        }
    }

    function remove(id) {
        // console.log(id);return false;
        if (confirm('Are you sure you want to delete this?')) {
            $.ajax({
                url: '/delete_tmd',
                type: "POST",
                header:{
                    'X-CSRF-TOKEN':$('#token').val()
                },
                data: {
                    "id" : id,
                    "table" : maincms['widget'][0]['maintable'],
                    "_token": $('#token').val(),
                    maincms: JSON.stringify(maincms),
                },
                success: function () {
                    // $("#notiftext").text('Data  Berhasil di hapus');
                    $.notify({
                        message: 'Delete data has been succesfullly.' 
                    },{
                        type: 'success'
                    });
                    refresh();
                    // $('#divsucces').show(0).delay(5000).hide(0);
                    var x = document.getElementById("from-departement");
                    var x2 = document.getElementById("tabledepartemen");
                    var y = document.getElementById("tool-kedua");
                    var y2 = document.getElementById("tool-pertama");
                    x.style.display = "none";
                    x2.style.display = "block";
                    y.style.display = "none";
                    y2.style.display = "block";
                }
            });
        }
    }
    
    function print(noid, urlprint) {
        window.open('http://ux.lambada.my.id/'+noid+'/'+urlprint, '_blank');
        return false;

        $.ajax({
                url: panelnoid+'/print/'+noid,
                type: "GET",
                header:{
                    'X-CSRF-TOKEN':$('#token').val()
                },
                success: function (response) {
                    return response
                }
        })
    }

    (function ($) {
    // function infoDatatable() {
        // console.log('tes gaes')
        
        function calcDisableClasses(oSettings) {
            var start = oSettings._iDisplayStart;
            var length = oSettings._iDisplayLength;
            var visibleRecords = oSettings.fnRecordsDisplay();
            var all = length === -1;

            // Gordey Doronin: Re-used this code from main jQuery.dataTables source code. To be consistent.
            var page = all ? 0 : Math.ceil(start / length);
            var pages = all ? 1 : Math.ceil(visibleRecords / length);

            var disableFirstPrevClass = (page > 0 ? '' : oSettings.oClasses.sPageButtonDisabled);
            var disableNextLastClass = (page < pages - 1 ? '' : oSettings.oClasses.sPageButtonDisabled);

            return {
                'first': disableFirstPrevClass,
                'previous': disableFirstPrevClass,
                'next': disableNextLastClass,
                'last': disableNextLastClass
            };
        }

        function calcCurrentPage(oSettings) {
            return Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength) + 1;
        }

        function calcPages(oSettings) {
            return Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength);
        }

        var firstClassName = 'first';
        var previousClassName = 'previous';
        var nextClassName = 'next';
        var lastClassName = 'last';

        var paginateClassName = 'paginate';
        var paginatePageClassName = 'paginate_page';
        var paginateInputClassName = 'paginate_input';
        var paginateTotalClassName = 'paginate_total';

        $.fn.dataTableExt.oPagination.input = {
            'fnInit': function (oSettings, nPaging, fnCallbackDraw) {
                var nFirst = document.createElement('span');
                var nPrevious = document.createElement('span');
                var nNext = document.createElement('span');
                var nLast = document.createElement('span');
                var nInput = document.createElement('input');
                var nTotal = document.createElement('span');
                var nInfo = document.createElement('span');

                var language = oSettings.oLanguage.oPaginate;
                var classes = oSettings.oClasses;
                var info = language.info || `&nbsp;&nbsp;
                                                <label style="padding-top: 8px">Page</label>&nbsp;
                                                _INPUT_&nbsp;
                                                <label style="padding-top: 8px">of&nbsp;_TOTAL_</label>&nbsp;`;

                nFirst.innerHTML = language.sFirst;
                nPrevious.innerHTML = language.sPrevious;
                nNext.innerHTML = language.sNext;
                nLast.innerHTML = language.sLast;

                nFirst.className = firstClassName + ' ' + classes.sPageButton;
                nPrevious.className = previousClassName + ' ' + classes.sPageButton;
                nNext.className = nextClassName + ' ' + classes.sPageButton;
                nLast.className = lastClassName + ' ' + classes.sPageButton;

                nInput.className = paginateInputClassName;
                nTotal.className = paginateTotalClassName;

                if (oSettings.sTableId !== '') {
                    nPaging.setAttribute('id', oSettings.sTableId + '_' + paginateClassName);
                    nFirst.setAttribute('id', oSettings.sTableId + '_' + firstClassName);
                    nPrevious.setAttribute('id', oSettings.sTableId + '_' + previousClassName);
                    nNext.setAttribute('id', oSettings.sTableId + '_' + nextClassName);
                    nLast.setAttribute('id', oSettings.sTableId + '_' + lastClassName);
                }

                nInput.type = 'text';

                info = info.replace(/_INPUT_/g, '</span>' + nInput.outerHTML + '<span>');
                info = info.replace(/_TOTAL_/g, '</span>' + nTotal.outerHTML + '<span>');
                nInfo.innerHTML = '<span>' + info + '</span>';

                nPaging.appendChild(nFirst);
                nPaging.appendChild(nPrevious);
                $(nInfo).children().each(function (i, n) {
                    nPaging.appendChild(n);
                });
                nPaging.appendChild(nNext);
                nPaging.appendChild(nLast);

                $(nFirst).click(function() {
                    var iCurrentPage = calcCurrentPage(oSettings);
                    if (iCurrentPage !== 1) {
                        oSettings.oApi._fnPageChange(oSettings, 'first');
                        fnCallbackDraw(oSettings);
                    }
                });

                $(nPrevious).click(function() {
                    var iCurrentPage = calcCurrentPage(oSettings);
                    if (iCurrentPage !== 1) {
                        oSettings.oApi._fnPageChange(oSettings, 'previous');
                        fnCallbackDraw(oSettings);
                    }
                });

                $(nNext).click(function() {
                    var iCurrentPage = calcCurrentPage(oSettings);
                    if (iCurrentPage !== calcPages(oSettings)) {
                        oSettings.oApi._fnPageChange(oSettings, 'next');
                        fnCallbackDraw(oSettings);
                    }
                });

                $(nLast).click(function() {
                    var iCurrentPage = calcCurrentPage(oSettings);
                    if (iCurrentPage !== calcPages(oSettings)) {
                        oSettings.oApi._fnPageChange(oSettings, 'last');
                        fnCallbackDraw(oSettings);
                    }
                });

                $(nPaging).find('.' + paginateInputClassName).keyup(function (e) {
                    // 38 = up arrow, 39 = right arrow
                    if (e.which === 38 || e.which === 39) {
                        this.value++;
                    }
                    // 37 = left arrow, 40 = down arrow
                    else if ((e.which === 37 || e.which === 40) && this.value > 1) {
                        this.value--;
                    }

                    if (this.value === '' || this.value.match(/[^0-9]/)) {
                        /* Nothing entered or non-numeric character */
                        this.value = this.value.replace(/[^\d]/g, ''); // don't even allow anything but digits
                        return;
                    }

                    var iNewStart = oSettings._iDisplayLength * (this.value - 1);
                    if (iNewStart < 0) {
                        iNewStart = 0;
                    }
                    if (iNewStart >= oSettings.fnRecordsDisplay()) {
                        iNewStart = (Math.ceil((oSettings.fnRecordsDisplay()) / oSettings._iDisplayLength) - 1) * oSettings._iDisplayLength;
                    }

                    oSettings._iDisplayStart = iNewStart;
                    oSettings.oInstance.trigger("page.dt", oSettings);
                    fnCallbackDraw(oSettings);
                });

                // Take the brutal approach to cancelling text selection.
                $('span', nPaging).bind('mousedown', function () { return false; });
                $('span', nPaging).bind('selectstart', function() { return false; });

                // If we can't page anyway, might as well not show it.
                var iPages = calcPages(oSettings);
                if (iPages <= 1) {
                    $(nPaging).hide();
                }
            },

            'fnUpdate': function (oSettings) {
                if (!oSettings.aanFeatures.p) {
                    return;
                }

                var iPages = calcPages(oSettings);
                var iCurrentPage = calcCurrentPage(oSettings);

                var an = oSettings.aanFeatures.p;
                if (iPages <= 1) // hide paging when we can't page
                {
                    $(an).hide();
                    return;
                }

                var disableClasses = calcDisableClasses(oSettings);

                $(an).show();

                // Enable/Disable `first` button.
                $(an).children('.' + firstClassName)
                    .removeClass(oSettings.oClasses.sPageButtonDisabled)
                    .addClass(disableClasses[firstClassName]);

                // Enable/Disable `prev` button.
                $(an).children('.' + previousClassName)
                    .removeClass(oSettings.oClasses.sPageButtonDisabled)
                    .addClass(disableClasses[previousClassName]);

                // Enable/Disable `next` button.
                $(an).children('.' + nextClassName)
                    .removeClass(oSettings.oClasses.sPageButtonDisabled)
                    .addClass(disableClasses[nextClassName]);

                // Enable/Disable `last` button.
                $(an).children('.' + lastClassName)
                    .removeClass(oSettings.oClasses.sPageButtonDisabled)
                    .addClass(disableClasses[lastClassName]);

                // Paginate of N pages text
                $(an).find('.' + paginateTotalClassName).html(iPages);

                // Current page number input value
                $(an).find('.' + paginateInputClassName).val(iCurrentPage);
            }
        };
    // }
    })(jQuery);
</script>
@endsection
