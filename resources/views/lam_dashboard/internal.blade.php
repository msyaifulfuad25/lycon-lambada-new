{{-- Extends layout --}}
@extends('layout_lambada.default')

@section('styles')
    <link href="//www.amcharts.com/lib/3/plugins/export/export.css?v=7.0.6" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css"/>
@endsection


{{-- Content --}}
@section('content')
@if($widget != '')
          <div class="row" style="margin-top:10px">
                  @if($widget[0]->widget_idtypewidget == 10)
                  @php
                  $key2 = 10;
                  ${"'q'.$key2"}  =  json_decode($widget[0]->widget_configwidget);
                  ${"'hasil'.$key2"} = ${"'q'.$key2"}->Widget_Stat->datasource->dataquery;
                  $nilai =\App\Menu::ambilQuery2(${"'hasil'.$key2"});
                  $warna =\App\Menu::cariWarna($widget[0]->widget_panelcolor);
                  @endphp
                  <div class="col-lg-{{$widget[0]->panel_nospan}}">
                    <div class="card  card-stretch card-stretch-half gutter-b " style="background-color:{{ $warna }};height: 100px;">
                      <div class="card-body p-0" style="position: relative;">
                       <div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
                                          <div class="visual"><i class="{{ $widget[0]->widget_widgeticon }} icon-7x" style="color: #FFFFFF;opacity: 0.1;margin-left: -55px;font-size: 0px;line-height: 0px;"></i></div>
                                              <div class="details"><div class="number">{{ $nilai }}</div><div class="desc">{{ $widget[0]->widget_widgetcaption }}</div></div>

                       </div>
                        <a class="morekuning" href="javascript:;"> View more <i class="m-icon-swapright m-icon-white"></i></a>
                    </div>
                    </div>
                    </div>
                  @endif
                  @if($widget[1]->widget_idtypewidget == 10)
                  @php
                  $key2 = 10;
                  $data2 =  json_decode($widget[1]->widget_configwidget);
                  $pan2 =$data2->Widget_Stat->datasource->dataquery;
                  $nilai =\App\Menu::ambilQuery2($pan2);
                  $warna =\App\Menu::cariWarna($widget[1]->widget_panelcolor);
                  @endphp
                  <div class="col-lg-{{$widget[1]->panel_nospan}}">
                    <div class="card  card-stretch card-stretch-half gutter-b " style="background-color:{{ $warna }};height: 100px;">
                      <div class="card-body p-0" style="position: relative;">
                       <div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
                                          <div class="visual"><i class="{{ $widget[0]->widget_widgeticon }} icon-7x" style="color: #FFFFFF;opacity: 0.1;margin-left: -55px;font-size: 0px;line-height: 0px;"></i></div>
                                            <div class="details"><div class="number">{{ $nilai }}</div><div class="desc">{{ $widget[1]->widget_widgetcaption }}</div></div>

                       </div>
                        <a class="morehijau" href="javascript:;"> View more <i class="m-icon-swapright m-icon-white"></i></a>
                    </div>
                    </div>
                    </div>
                  @endif
                  @if($widget[2]->widget_idtypewidget == 10)
                  @php
                  $key2 = 10;
                  $data2 =  json_decode($widget[2]->widget_configwidget);
                  $pan2 =$data2->Widget_Stat->datasource->dataquery;
                  $nilai =\App\Menu::ambilQuery2($pan2);
                  $warna =\App\Menu::cariWarna($widget[2]->widget_panelcolor);
                  @endphp
                  <div class="col-lg-{{$widget[1]->panel_nospan}}">
                    <div class="card  card-stretch card-stretch-half gutter-b " style="background-color:{{ $warna }};height: 100px;">
                      <div class="card-body p-0" style="position: relative;">
                       <div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
                                          <div class="visual"><i class="{{ $widget[0]->widget_widgeticon }} icon-7x" style="color: #FFFFFF;opacity: 0.1;margin-left: -55px;font-size: 0px;line-height: 0px;"></i></div>
                                            <div class="details"><div class="number">{{ $nilai }}</div><div class="desc">{{ $widget[2]->widget_widgetcaption }}</div></div>

                       </div>
                        <a class="morekuningkus" href="javascript:;"> View more <i class="m-icon-swapright m-icon-white"></i></a>
                    </div>
                    </div>
                    </div>
                  @endif
                  @if($widget[3]->widget_idtypewidget == 10)
                  @php
                  $key2 = 10;
                  $data2 =  json_decode($widget[3]->widget_configwidget);
                  $pan2 =$data2->Widget_Stat->datasource->dataquery;
                  $nilai =\App\Menu::ambilQuery2($pan2);
                  $warna =\App\Menu::cariWarna($widget[3]->widget_panelcolor);
                  @endphp
                  <div class="col-lg-{{$widget[1]->panel_nospan}}">
                    <div class="card  card-stretch card-stretch-half gutter-b " style="margin-bottom: 50px;background-color:{{ $warna }};height: 100px;">
                      <div class="card-body p-0" style="position: relative;">
                       <div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
                                          <div class="visual"><i class="{{ $widget[0]->widget_widgeticon }} icon-7x" style="color: #FFFFFF;opacity: 0.1;margin-left: -55px;font-size: 0px;line-height: 0px;"></i></div>
                                            <div class="details"><div class="number">{{ $nilai }}</div><div class="desc">{{ $widget[3]->widget_widgetcaption }}</div></div>

                       </div>
                        <a class="moreredintense" href="javascript:;"> View more <i class="m-icon-swapright m-icon-white"></i></a>
                    </div>
                    </div>
                    </div>
                  @endif

                  @if($widget[4]->widget_idtypewidget = 23)
                    @php
                    $datachart2 = json_decode($widget[4]->widget_dataquery);
                    $hasi =\App\Menu::ambilQuery($datachart2[0]->query);
                    @endphp
                    <div class="col-lg-{{$widget[4]->panel_nospan}}">
                    <div class="card card-custom gutter-b">
                      <div class="card-header">
                        <div class="card-title">
                          <h3 class="card-label">
                            {{ $widget[4]->widget_widgetcaption }}
                          </h3>
                        </div>
                      </div>
                      <div class="card-body">
                        <div id="kt_amcharts_pertama" style="height: 500px; overflow: visible; text-align: left;"</div>
                      </div>
                    </div>
                  </div>
                  </div>
                  @endif
                  @if($widget[5]->widget_idtypewidget = 23)
                    @php
                    $datachart3 = json_decode($widget[5]->widget_dataquery);
                    $hasi2 =\App\Menu::ambilQuery($datachart3[0]->query);
                    @endphp
                    <div class="col-lg-{{$widget[4]->panel_nospan}}">
                    <div class="card card-custom gutter-b">
                      <div class="card-header">
                        <div class="card-title">
                          <h3 class="card-label">
                            {{ $widget[5]->widget_widgetcaption }}
                          </h3>
                        </div>
                      </div>
                      <div class="card-body">
                        <div id="kt_amcharts_kedua" style="height: 500px; overflow: visible; text-align: left;"</div>
                      </div>
                    </div>
                  </div>
                  </div>
                  @endif

                  @if($widget[6]->widget_idtypewidget = 23)
                    @php
                    $datachart4 = json_decode($widget[6]->widget_dataquery);
                    $hasi3 =\App\Menu::ambilQuery($datachart4[0]->query);
                    @endphp
                    <div class="col-lg-{{$widget[6]->panel_nospan}}">
                    <div class="card card-custom gutter-b">
                      <div class="card-header">
                        <div class="card-title">
                          <h3 class="card-label">
                            {{ $widget[6]->widget_widgetcaption }}
                          </h3>
                        </div>
                      </div>
                      <div class="card-body">
                        <div id="kt_amcharts_ketiga" style="height: 500px; overflow: visible; text-align: left;"</div>
                      </div>
                    </div>
                  </div>
                  </div>
                  @endif




            </div>
@endif
@endsection
@section('scripts')
<script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
<script src="//www.amcharts.com/lib/3/amcharts.js?v=7.0.6"></script>
<script src="//www.amcharts.com/lib/3/serial.js?v=7.0.6"></script>
<script src="//www.amcharts.com/lib/3/radar.js?v=7.0.6"></script>
<script src="//www.amcharts.com/lib/3/pie.js?v=7.0.6"></script>
<script src="//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.js?v=7.0.6"></script>
<script src="//www.amcharts.com/lib/3/plugins/animate/animate.min.js?v=7.0.6"></script>
<script src="//www.amcharts.com/lib/3/plugins/export/export.min.js?v=7.0.6"></script>
<script src="//www.amcharts.com/lib/3/themes/light.js?v=7.0.6"></script>
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script type="text/javascript">
var dataatas = {!! json_encode($hasi) !!};
    var dataProvideratas = [];// this variable you have to pass in dataProvider inside chart
    for(var key in dataatas) {
      dataProvideratas.push({
        category: dataatas[key]['categories'],
        visits: dataatas[key]['jumlahdata'],
      });
    }

    var chart = AmCharts.makeChart("kt_amcharts_pertama", {
      "rtl": KTUtil.isRTL(),
      "type": "serial",
       "hideCredits":true,
      "theme": "light",
      "dataProvider": dataProvideratas,
      "valueAxes": [{
        "gridColor": "#FFFFFF",
        "gridAlpha": 0.2,
        "dashLength": 0
      }],
      "series": [{
          "type": "LineSeries",
          "stroke": "#ff0000",
          "strokeWidth": 3
        }],
"valueAxes": [ {
"position": "left",
"title": "Jumlah Data"
}, {
"position": "bottom",
"title": @json($widget[4]->widget_widgetcaption)
} ],
"legend": {
"useGraphSettings": true
},
"titles": [
{
 "size": 15,
 "text": @json($widget[4]->widget_widgetcaption)
},
{
    "text": @json($widget[4]->widget_widgetsubcaption),
    "bold": false
}
],
      "gridAboveGraphs": true,
      "startDuration": 1,
      "graphs": [{
        "balloonText": "[[category]]: <b>[[value]]</b>",
        "fillAlphas": 0.8,
        "lineAlpha": 0.2,
        "type": "column",
        "valueField": "visits",
        "title": @json($widget[4]->widget_widgetcaption)
      }],
      "chartCursor": {
        "categoryBalloonEnabled": false,
        "cursorAlpha": 0,
        "zoomable": false
      },
      "categoryField": "category",
      "categoryAxis": {
        "gridPosition": "start",
        // "gridAlpha": 0,
        // "tickPosition": "start",
        // "tickLength": 20
      },
      "export": {
        "enabled": true
      }
    });
    var chart = AmCharts.makeChart("kt_amcharts_12",{
"type"    : "pie",
"hideCredits"    : true,
"theme"    : "light",
"addClassNames": true,
"titles": [
{
  "size": 15,
  "text": @json($widget[6]->widget_widgetcaption)
},
{
     "text": @json($widget[6]->widget_widgetsubcaption),
     "bold": false
 }
],
// "pullOutDuration": 0,
// "pullOutRadius": 0,

"labelsEnabled": false,
"titleField"  : "country",
"valueField"  : "litres",
"dataProvider"  : dataProvider,
"numberFormatter": {
        "precision": -1,
        "decimalSeparator": ",",
        "thousandsSeparator": ""
    },
"legend": {
           "align": "center",
           "position": "bottom",
           "marginRight": 0,
           "labelText": "[[title]]",
           "valueText": "",
           "valueWidth": 50,
           "textClickEnabled": true
       },
"export": {
"enabled": true
}

});

    // var data = {!! json_encode($hasi2) !!};
    //
    //      // console.log(data);
    //     var dataProvider = [];// this variable you have to pass in dataProvider inside chart
    //     for(var key in data) {
    //       dataProvider.push({
    //         country: data[key]['messagestatus'],
    //         litres: data[key]['jumlahdata'],
    //       });
    //     }
    //
    // var chart2 = am4core.create("kt_amcharts_kedua", am4charts.PieChart);
    //
    // chart2.data = dataProvider;
    // var title = chart2.titles.create();
    // title.text = @json($widget[6]->widget_widgetsubcaption);
    // title.fontSize = 10;
    // title.bold = true;
    // var title2 = chart2.titles.create();
    // title2.text = @json($widget[6]->widget_widgetcaption);
    // title2.fontSize = 15;
    // title2.bold = false;
    // chart2.numberFormatter.numberFormat = "#.";
    // var pieSeries = chart2.series.push(new am4charts.PieSeries());
    // pieSeries.dataFields.value = "litres";
    // pieSeries.dataFields.category = "country";
    // pieSeries.labels.template.disabled = true;
    // pieSeries.ticks.template.disabled = true;
    // pieSeries.calculatePercent = true;
    // pieSeries.labels.template.text = "{category}: {value.percent.formatNumber('###.00')}%";
    // pieSeries.slices.template.tooltipText = "{category}: {value.percent.formatNumber('###.00')}% ({value.value})";
    // var valuesAxis = new AmCharts.ValueAxis();
    //
    // valuesAxis.integersOnly = true;
    //
    // chart2.legend = new am4charts.Legend();
    // chart2.legend.itemContainers.template.events.on("hit", function(ev) {
    // var slice = ev.target.dataItem.dataContext.slice;
    // pieSeries.slices.each(function(item) {
    // if (item != slice) {
    // item.isActive = false;
    // }
    // else {
    // slice.isActive = !slice.isActive;
    // }
    // });
    // });

    var databawah1 = {!! json_encode($hasi3) !!};

    var idhomepage = {!! json_encode($idhomepage) !!};

    if (idhomepage == 10000002) {
      var dataProviderBawah2 = [];// this variable you have to pass in dataProvider inside chart
      for(var key in databawah1) {
        dataProviderBawah2.push({
          category: databawah1[key]["namabulan"],
          visits: databawah1[key]['jumlahdata'],
        });
      }
    }else{
      var dataProviderBawah2 = [];// this variable you have to pass in dataProvider inside chart
      for(var key in databawah1) {
        dataProviderBawah2.push({
          category: databawah1[key]["namabulan"],
          visits: databawah1[key]['jumlahdata'],
        });
      }
    }

         // console.log(data);



            var chart_bawah = AmCharts.makeChart("kt_amcharts_ketiga", {
              "rtl": KTUtil.isRTL(),
              "type": "serial",
               "hideCredits":true,
              "theme": "light",
              "dataProvider": dataProviderBawah2,
              "valueAxes": [{
                "gridColor": "#FFFFFF",
                "gridAlpha": 0.2,
                "dashLength": 0
              }],
              "series": [{
                  "type": "LineSeries",
                  "stroke": "#ff0000",
                  "strokeWidth": 3
                }],
    "valueAxes": [ {
        "position": "left",
        "title": "Jumlah Data"
      }, {
        "position": "bottom",
        "title": @json($widget[6]->widget_widgetcaption)
      } ],
      "legend": {
       "useGraphSettings": true
     },
     "titles": [
       {
         "size": 15,
         "text": @json($widget[6]->widget_widgetcaption)
       },
       {
            "text": @json($widget[6]->widget_widgetsubcaption),
            "bold": false
        }
     ],
              "gridAboveGraphs": true,
              "startDuration": 1,
              "graphs": [{
                "balloonText": "[[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "visits",
                "title": @json($widget[6]->widget_widgetcaption)
              }],
              "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
              },
              "categoryField": "category",
              "categoryAxis": {
                "gridPosition": "start",
                // "gridAlpha": 0,
                // "tickPosition": "start",
                // "tickLength": 20
              },
              "export": {
                "enabled": true
              }
            });
            var data = {!! json_encode($hasi2) !!};
                 // console.log(data);
                var dataProvider = [];// this variable you have to pass in dataProvider inside chart
                for(var key in data) {
                  dataProvider.push({
                    country: data[key]['messagestatus'],
                    litres: data[key]['jumlahdata'],
                  });
                }

            var chart = AmCharts.makeChart("kt_amcharts_kedua",{
      "type"    : "pie",
      "hideCredits"    : true,
      "theme"    : "light",
      "addClassNames": true,
      "titles": [
        {
          "size": 15,
          "text": @json($widget[6]->widget_widgetcaption)
        },
        {
             "text": @json($widget[6]->widget_widgetsubcaption),
             "bold": false
         }
      ],
      // "pullOutDuration": 0,
      // "pullOutRadius": 0,

      "labelsEnabled": false,
      "titleField"  : "country",
      "valueField"  : "litres",
      "dataProvider"  : dataProvider,
      "numberFormatter": {
                "precision": -1,
                "decimalSeparator": ",",
                "thousandsSeparator": ""
            },
      "legend": {
                   "align": "center",
                   "position": "bottom",
                   "marginRight": 0,
                   "labelText": "[[title]]",
                   "valueText": "",
                   "valueWidth": 50,
                   "textClickEnabled": true
               },
      "export": {
        "enabled": true
      }

    });
</script>
@endsection
