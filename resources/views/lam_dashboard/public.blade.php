{{-- Extends layout --}}
@extends('layout_lambada.default')
                            <link href="//www.amcharts.com/lib/3/plugins/export/export.css?v=7.0.6" rel="stylesheet" type="text/css"/>
{{-- Content --}}
@section('content')


    @php
    $datachart = json_decode($widget[5]->widget_dataquery);
    $hasi =\App\Menu::ambilQuery($datachart[0]->query);
    @endphp
    @if($widget != '')
              <div class="row" style="margin-top:10px">
    @if($dashboard == 0)
        @if($widget[0]->widget_idtypewidget == 151)
        @php
        $key = 151;
      ${"'q'.$key"}  =  json_decode($widget[0]->widget_configwidget);
        ${"'hasil'.$key"} = ${"'q'.$key"}->Widget_Slider->datasource->dataquery;
    ${"'data1'.$key"} =\App\Menu::ambilQuery(${"'hasil'.$key"});
      @endphp
      <div class="col-sm-{{ $widget[0]->panel_nospan }}">
        <div class="card card-custom card-stretch gutter-b">
          <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="height:{{$widget[0]->panel_panelheigth}}px;margin-bottom:10px;">
            <ol class="carousel-indicators">
              @for($i = 0; $i <= count(${"'data1'.$key"}); $i++)
              <li data-target="#carouselExampleIndicators" data-slide-to="{{ $i }}" class="{{ $i == 0 ? ' active' : '' }}"></li>
              @endfor
          </ol>
          @foreach(${"'data1'.$key"}  as $key => $slider)
              <div class="carousel-item {{$key == 0 ? 'active' : '' }}">
                    <img src="http://placehold.it/630x530" alt="Slide 1">
                  <div class="carousel-caption">
                    {{ $slider->newstitle }}
                  </div>
              </div>
              @endforeach
              <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
        </div>
      </div>
      @endif
      <style media="screen">
       .more {
  color: #FFFFFF;
  background-color: #258fd7;
}

 .more {
    clear: both;
    display: block;
    margin-top: -10px;
    padding: 6px 10px 6px 10px;
    position: relative;
    text-transform: uppercase;
    font-weight: 300;
    font-size: 11px;
    opacity: 0.7;
    filter: alpha(opacity=70);
}
.more2 {
color: #FFFFFF;
background-color: #26a69a;
}
 .more2 {
   clear: both;
     display: block;
       margin-top: -10px;
     padding: 6px 10px 6px 10px;
     position: relative;
     text-transform: uppercase;
     font-weight: 300;
     font-size: 11px;
     opacity: 0.7;
     filter: alpha(opacity=70);
}
.more3 {
color: #FFFFFF;
background-color: #cb5a5e;
}

 .more3 {
   clear: both;
     display: block;
       margin-top: -10px;
     padding: 6px 10px 6px 10px;
     position: relative;
     text-transform: uppercase;
     font-weight: 300;
     font-size: 11px;
     opacity: 0.7;
     filter: alpha(opacity=70);
}
.more4 {
color: #FFFFFF;
background-color: #8775a7;
}
.more4 {
  clear: both;
    display: block;
      margin-top: -10px;
    padding: 6px 10px 6px 10px;
    position: relative;
    text-transform: uppercase;
    font-weight: 300;
    font-size: 11px;
    opacity: 0.7;
    filter: alpha(opacity=70);
}
.visual {
    display: block;
    float: left;

}
      </style>
      @if($widget[1]->widget_idtypewidget == 10)
      @php
      $key2 = 10;
      ${"'q'.$key2"}  =  json_decode($widget[1]->widget_configwidget);
      ${"'hasil'.$key2"} = ${"'q'.$key2"}->Widget_Stat->datasource->dataquery;
      $nilai =\App\Menu::ambilQuery2(${"'hasil'.$key2"});
      if($widget[1]->widget_panelcolor == 'blue'){
        $warna = '#3598dc';
      }
      @endphp
      <div class="col-lg-{{$widget[1]->panel_nospan}}">

        <div class="card  card-stretch card-stretch-half gutter-b " style="margin-bottom: 35px;background-color:{{ $warna }};height: 100px;">
          <div class="card-body p-0" style="position: relative;">
           <div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
                              <div class="visual"><i class="fa fa-bed icon-7x" style="color: #FFFFFF;opacity: 0.1;margin-left: -55px;font-size: 0px;line-height: 0px;"></i></div>
               <div class="d-flex flex-column text-right">
                   <span class="text-dark-75 font-weight-bolder font-size-h3">{{ $nilai }}</span>
                   <span class="text-muted font-weight-bold mt-2">{{ $widget[1]->widget_widgetcaption }}</span>
               </div>
           </div>
            <a class="morebiru" href="javascript:;"> View more <i class="m-icon-swapright m-icon-white"></i></a>
        </div>
        </div>
            @php
            $key3 = 111;
            ${"'q'.$key3"}  =  json_decode($widget[2]->widget_configwidget);
            ${"'hasil'.$key3"} = ${"'q'.$key3"}->Widget_Stat->datasource->dataquery;
            $nilai =\App\Menu::ambilQuery2(${"'hasil'.$key3"});
            if($widget[2]->widget_panelcolor == 'green'){
              $warna = '#26a69a';
            }
            @endphp
                <div class="card  card-stretch card-stretch-half gutter-b " style="margin-bottom: 35px;background-color:{{ $warna }};height: 100px;">
                  <div class="card-body p-0" style="position: relative;">
                    <div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
                                       <div class="visual"><i class="fa fa-bed icon-7x" style="color: #FFFFFF;opacity: 0.1;margin-left: -55px;font-size: 0px;line-height: 0px;"></i></div>
                        <div class="d-flex flex-column text-right">
                            <span class="text-dark-75 font-weight-bolder font-size-h3">{{ $nilai }}</span>
                            <span class="text-muted font-weight-bold mt-2">{{ $widget[2]->widget_widgetcaption }}</span>
                        </div>
                    </div>
                    <a class="morehijauawal" href="javascript:;"> View more <i class="m-icon-swapright m-icon-white"></i></a>
                </div>
                </div>
                @php
                $key4 = 111;
                ${"'q'.$key4"}  =  json_decode($widget[3]->widget_configwidget);
                ${"'hasil'.$key4"} = ${"'q'.$key4"}->Widget_Stat->datasource->dataquery;
                $nilai =\App\Menu::ambilQuery2(${"'hasil'.$key4"});
                if($widget[3]->widget_panelcolor == 'red'){
                  $warna = '#cb5a5e';
                }
                @endphp
                <div class="card  card-stretch card-stretch-half gutter-b " style="margin-bottom: 35px;background-color:{{ $warna }};height: 100px;">
                  <div class="card-body p-0" style="position: relative;">
                   <div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
                                      <div class="visual"><i class="fa fa-bed icon-7x" style="color: #FFFFFF;opacity: 0.1;margin-left: -55px;font-size: 0px;line-height: 0px;"></i></div>
                       <div class="d-flex flex-column text-right">
                           <span class="text-dark-75 font-weight-bolder font-size-h3">{{ $nilai }}</span>
                           <span class="text-muted font-weight-bold mt-2">{{ $widget[3]->widget_widgetcaption }}</span>
                       </div>
                   </div>
                    <a class="moremerah" href="javascript:;"> View more <i class="m-icon-swapright m-icon-white"></i></a>
                </div>
                </div>
                    @php
                    $key5 = 111;
                    //purple-plum
                    ${"'q'.$key5"}  =  json_decode($widget[4]->widget_configwidget);
                    ${"'hasil'.$key5"} = ${"'q'.$key5"}->Widget_Stat->datasource->dataquery;
                      $nilai =\App\Menu::ambilQuery2(${"'hasil'.$key5"});
                    if($widget[4]->widget_panelcolor == 'purple-plum'){
                      $warna = '#8775a7';
                    }
                    @endphp
                    <div class="card  card-stretch card-stretch-half gutter-b " style="background-color:{{ $warna }};height: 100px;">
                      <div class="card-body p-0" style="position: relative;">
                           <div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
                                <div class="visual"><i class="fa fa-bed icon-7x" style="color: #FFFFFF;opacity: 0.1;margin-left: -55px;font-size: 0px;line-height: 0px;"></i></div>
                               <div class="d-flex flex-column text-right">
                                   <span class="text-dark-75 font-weight-bolder font-size-h3">{{ $nilai }}</span>
                                   <span class="text-muted font-weight-bold mt-2">{{ $widget[4]->widget_widgetcaption }}</span>
                               </div>
                           </div>
                             <a class="moreungu" href="javascript:;"> View more <i class="m-icon-swapright m-icon-white"></i></a>
                        </div>
                        </div>

            </div>
              @endif
              @if($widget[5]->widget_idtypewidget = 23)
              @php
              $datachart = json_decode($widget[5]->widget_dataquery);
              $hasi =\App\Menu::ambilQuery($datachart[0]->query);
              @endphp
              <div class="col-lg-{{$widget[5]->panel_nospan}}">
                <div class="card card-custom gutter-b">
                	<div class="card-header">
                		<div class="card-title">
                			<h3 class="card-label">
                				{{ $widget[5]->widget_widgetcaption }}
                			</h3>
                		</div>
                	</div>
                	<div class="card-body">
                		<div id="kt_amcharts_1" style="width: 100%; height: 500px;"></div>
                	</div>
                </div>
              </div>
              @endif
                            @if($widget[6]->widget_idtypewidget = 23)
                @php
                $datachart2 = json_decode($widget[6]->widget_dataquery);
                $hasi2 =\App\Menu::ambilQuery($datachart2[0]->query);
                @endphp
                <div class="col-lg-{{$widget[6]->panel_nospan}}">
                <div class="card card-custom gutter-b">
                	<div class="card-header">
                		<div class="card-title">
                			<h3 class="card-label">
                				{{ $widget[6]->widget_widgetcaption }}
                			</h3>
                		</div>
                	</div>
                	<div class="card-body">
                		<div id="kt_amcharts_12" style="height: 500px; overflow: visible; text-align: left;"></div>
                	</div>
                </div>
              </div>
              </div>
              @endif
                            @if($widget[7]->widget_idtypewidget = 23)
                  @php
                $datachart3 = json_decode($widget[7]->widget_dataquery);
                $hasi3 =\App\Menu::ambilQuery($datachart3[0]->query);
                @endphp
                <div class="col-lg-{{$widget[7]->panel_nospan}}">
                <div class="card card-custom gutter-b">
                	<div class="card-header">
                		<div class="card-title">
                			<h3 class="card-label">
                				{{ $widget[7]->widget_widgetcaption }}
                			</h3>
                		</div>
                	</div>
                	<div class="card-body">
                    <div id="kt_amcharts_bawah" style="width: 100%; height: 500px;"></div>
                		</div>
                </div>
              </div>
              </div>
              @endif

@endif
</div>
</div>
@endif
@endsection
{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/amcharts.js?v=7.0.6"></script>
    <script src="//www.amcharts.com/lib/3/serial.js?v=7.0.6"></script>
    <script src="//www.amcharts.com/lib/3/radar.js?v=7.0.6"></script>
    <script src="//www.amcharts.com/lib/3/pie.js?v=7.0.6"></script>
    <script src="//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.js?v=7.0.6"></script>
    <script src="//www.amcharts.com/lib/3/plugins/animate/animate.min.js?v=7.0.6"></script>
    <script src="//www.amcharts.com/lib/3/plugins/export/export.min.js?v=7.0.6"></script>
    <script src="//www.amcharts.com/lib/3/themes/light.js?v=7.0.6"></script>
    <script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
  <script type="text/javascript">
    var dataatas = {!! json_encode($hasi) !!};
         // console.log(data);
        var dataProvideratas = [];// this variable you have to pass in dataProvider inside chart
        for(var key in dataatas) {
          dataProvideratas.push({
            category: dataatas[key]['categories'],
            visits: dataatas[key]['jumlahdata'],
          });
        }

        var chart = AmCharts.makeChart("kt_amcharts_1", {
          "rtl": KTUtil.isRTL(),
          "type": "serial",
           "hideCredits":true,
          "theme": "light",
          "dataProvider": dataProvideratas,
          "valueAxes": [{
            "gridColor": "#FFFFFF",
            "gridAlpha": 0.2,
            "dashLength": 0
          }],
          "series": [{
              "type": "LineSeries",
              "stroke": "#ff0000",
              "strokeWidth": 3
            }],
"valueAxes": [ {
    "position": "left",
    "title": "Jumlah Data"
  }, {
    "position": "bottom",
    "title": @json($widget[5]->widget_widgetcaption)
  } ],
  "legend": {
   "useGraphSettings": true
 },
 "titles": [
   {
     "size": 15,
     "text": @json($widget[5]->widget_widgetcaption)
   },
   {
        "text": @json($widget[5]->widget_widgetsubcaption),
        "bold": false
    }
 ],
          "gridAboveGraphs": true,
          "startDuration": 1,
          "graphs": [{
            "balloonText": "[[category]]: <b>[[value]]</b>",
            "fillAlphas": 0.8,
            "lineAlpha": 0.2,
            "type": "column",
            "valueField": "visits",
            "title": @json($widget[5]->widget_widgetcaption)
          }],
          "chartCursor": {
            "categoryBalloonEnabled": false,
            "cursorAlpha": 0,
            "zoomable": false
          },
          "categoryField": "category",
          "categoryAxis": {
            "gridPosition": "start",
            // "gridAlpha": 0,
            // "tickPosition": "start",
            // "tickLength": 20
          },
          "export": {
            "enabled": true
          }
        });

        var databawah = {!! json_encode($hasi3) !!};

             console.log(data);
            var dataProviderBawah = [];// this variable you have to pass in dataProvider inside chart
            for(var key in databawah) {
              dataProviderBawah.push({
                category: databawah[key]['namabulan'],
                visits: databawah[key]['jumlahdata'],
              });
            }


        var chart_bawah = AmCharts.makeChart("kt_amcharts_bawah", {
          "rtl": KTUtil.isRTL(),
          "type": "serial",
           "hideCredits":true,
          "theme": "light",
          "dataProvider": dataProviderBawah,
          "valueAxes": [{
            "gridColor": "#FFFFFF",
            "gridAlpha": 0.2,
            "dashLength": 0
          }],
          "series": [{
              "type": "LineSeries",
              "stroke": "#ff0000",
              "strokeWidth": 3
            }],
"valueAxes": [ {
    "position": "left",
    "title": "Jumlah Data"
  }, {
    "position": "bottom",
    "title": @json($widget[7]->widget_widgetcaption)
  } ],
  "legend": {
   "useGraphSettings": true
 },
 "titles": [
   {
     "size": 15,
     "text": @json($widget[7]->widget_widgetcaption)
   },
   {
        "text": @json($widget[7]->widget_widgetsubcaption),
        "bold": false
    }
 ],
          "gridAboveGraphs": true,
          "startDuration": 1,
          "graphs": [{
            "balloonText": "[[category]]: <b>[[value]]</b>",
            "fillAlphas": 0.8,
            "lineAlpha": 0.2,
            "type": "column",
            "valueField": "visits",
            "title": @json($widget[7]->widget_widgetcaption)
          }],
          "chartCursor": {
            "categoryBalloonEnabled": false,
            "cursorAlpha": 0,
            "zoomable": false
          },
          "categoryField": "category",
          "categoryAxis": {
            "gridPosition": "start",
            // "gridAlpha": 0,
            // "tickPosition": "start",
            // "tickLength": 20
          },
          "export": {
            "enabled": true
          }
        });
        var data = {!! json_encode($hasi2) !!};
             // console.log(data);
            var dataProvider = [];// this variable you have to pass in dataProvider inside chart
            for(var key in data) {
              dataProvider.push({
                country: data[key]['messagestatus'],
                litres: data[key]['jumlahdata'],
              });
            }

        var chart = AmCharts.makeChart("kt_amcharts_12",{
  "type"    : "pie",
  "hideCredits"    : true,
  "theme"    : "light",
  "addClassNames": true,
  "titles": [
    {
      "size": 15,
      "text": @json($widget[6]->widget_widgetcaption)
    },
    {
         "text": @json($widget[6]->widget_widgetsubcaption),
         "bold": false
     }
  ],
  // "pullOutDuration": 0,
  // "pullOutRadius": 0,

  "labelsEnabled": false,
  "titleField"  : "country",
  "valueField"  : "litres",
  "dataProvider"  : dataProvider,
  "numberFormatter": {
            "precision": -1,
            "decimalSeparator": ",",
            "thousandsSeparator": ""
        },
  "legend": {
               "align": "center",
               "position": "bottom",
               "marginRight": 0,
               "labelText": "[[title]]",
               "valueText": "",
               "valueWidth": 50,
               "textClickEnabled": true
           },
  "export": {
    "enabled": true
  }

});

//         var data = {!! json_encode($hasi2) !!};
//              // console.log(data);
//             var dataProvider = [];// this variable you have to pass in dataProvider inside chart
//             for(var key in data) {
//               dataProvider.push({
//                 country: data[key]['messagestatus'],
//                 litres: data[key]['jumlahdata'],
//               });
//             }
//
//         var chart2 = am4core.create("kt_amcharts_12", am4charts.PieChart);
//
//         chart2.data = dataProvider;
// var title = chart2.titles.create();
// title.text = @json($widget[6]->widget_widgetsubcaption);
// title.fontSize = 10;
// title.bold = true;
// var title2 = chart2.titles.create();
// title2.text = @json($widget[6]->widget_widgetcaption);
// title2.fontSize = 15;
// title2.bold = false;
// chart2.numberFormatter.numberFormat = "#.";
// var pieSeries = chart2.series.push(new am4charts.PieSeries());
// pieSeries.dataFields.value = "litres";
// pieSeries.dataFields.category = "country";
// pieSeries.labels.template.disabled = true;
// pieSeries.ticks.template.disabled = true;
// pieSeries.calculatePercent = true;
// pieSeries.labels.template.text = "{category}: {value.percent.formatNumber('###.00')}%";
// pieSeries.slices.template.tooltipText = "{category}: {value.percent.formatNumber('###.00')}% ({value.value})";
// var valuesAxis = new AmCharts.ValueAxis();
// valuesAxis.integersOnly = true;
// chart2.exporting.menu = new am4core.ExportMenu();
// chart2.legend = new am4charts.Legend();
// chart2.legend.itemContainers.template.events.on("hit", function(ev) {
//   var slice = ev.target.dataItem.dataContext.slice;
//   pieSeries.slices.each(function(item) {
//     if (item != slice) {
//       item.isActive = false;
//     }
//     else {
//       slice.isActive = !slice.isActive;
//     }
//   });
// });
</script>
@endsection
