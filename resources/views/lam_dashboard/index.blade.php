
@extends('layout_lambada.default')
@section('styles')
    <link href="//www.amcharts.com/lib/3/plugins/export/export.css?v=7.0.6" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css"/>
@endsection
@section('content')
  <div class="row" style="margin-top:10px">
  @foreach($panel as $pan)
  @php
  $widget = App\Widget::where('noid',$pan->idwidget)->first();
  if($widget->idtypepage == 151 && $pan->panelheigth < 517 ){
    $heighasli = 517;
  }else{
    $heighasli = $pan->panelheigth;
  }

  if($widget->idtypepage == 1011 && $pan->panelheigth < 517 ){
    $heighasli = 517;
  }else{
      $heighasli = $pan->panelheigth;
  }


  if($widget->idtypepage == 10  && $pan->panelheigth < 100 ){
    $heighasli = 100;
  }else{
      $heighasli = $pan->panelheigth;
  }
  if($widget->idtypepage == 23 && $pan->panelheigth < 450 ){
    $heighasli = 450;
  }else{
    $heighasli = $pan->panelheigth;
  }
  do{
        if($pan->nocol != 1){
          @endphp
          <div class="row-fluid col-lg-{{ $pan->nocol }} col-md-{{ $pan->nospan }} col-xs-{{ $pan->nospanxs }}" >
            <div class="card card-custom card-stretch gutter-b" style="height:{{ $heighasli }}px;margin-bottom:10px;">
              <div class="card-header h-auto border-0">
                <h3 class="card-label"></h3>
              </div>
              <div class="card-body" style="position: relative;">
                @if($widget->idtypepage == 151)
                <input type="hidden"  id="slider" value="'.$widget->noid.'">
                  <div class="owl-carousel">

                  @elseif($widget->idtypepage == 23)

                  <input type="hidden"  id="cart" value="{{ $widget->noid }}">
                  <input type="hidden" name="panel[]"  id="cart_{{ $widget->noid }}" value="{{ $widget->noid }}">
                  <div  id="kt_amcharts_{{ $widget->noid }}" style="height: 500px; overflow: visible; text-align: left;"</div>
                @else
                <p>lain</p>
                @endif
              </div>
            </div>
          </div>
          @php
        }else{
          $ambildata = App\Panel::filterPanel($page->noid,$pan->noid);
          @endphp
          {!! $ambildata !!}
          @php
        }
    }while(false);
  @endphp


  @endforeach
</div>
<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
@endsection
@section('scripts')
<script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
<script src="//www.amcharts.com/lib/3/amcharts.js?v=7.0.6"></script>
<script src="//www.amcharts.com/lib/3/serial.js?v=7.0.6"></script>
<script src="//www.amcharts.com/lib/3/radar.js?v=7.0.6"></script>
<script src="//www.amcharts.com/lib/3/pie.js?v=7.0.6"></script>
<script src="//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.js?v=7.0.6"></script>
<script src="//www.amcharts.com/lib/3/plugins/animate/animate.min.js?v=7.0.6"></script>
<script src="//www.amcharts.com/lib/3/plugins/export/export.min.js?v=7.0.6"></script>
<script src="//www.amcharts.com/lib/3/themes/light.js?v=7.0.6"></script>
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="{{ asset('js/custom_dashboard.js') }}"></script>
<!-- <script src="{{ asset('js/custom_dashboard_slider.js') }}"></script> -->
@endsection
