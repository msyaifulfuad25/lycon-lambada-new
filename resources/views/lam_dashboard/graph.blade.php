{{-- Extends layout --}}
@extends('layout_lambada.default')

@section('styles')
    <link href="//www.amcharts.com/lib/3/plugins/export/export.css?v=7.0.6" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css"/>
@endsection

{{-- Content --}}
@section('content')
@if($widget != '')
          <div class="row" style="margin-top:10px">


                  @if($widget[0]->widget_idtypewidget = 23)
                    @php
                    $datachart2 = json_decode($widget[0]->widget_dataquery);
                    $hasi =\App\Menu::ambilQuery($datachart2[0]->query);
                    @endphp
                    <div class="col-lg-{{$widget[0]->panel_nospan}}">
                    <div class="card card-custom gutter-b">
                      <div class="card-header">
                        <div class="card-title">
                          <h3 class="card-label">
                            {{ $widget[0]->widget_widgetcaption }}
                          </h3>
                        </div>
                      </div>
                      <div class="card-body">
                        <div id="kt_amcharts_pertama" style="height: 500px; overflow: visible; text-align: left;"</div>
                      </div>
                    </div>
                  </div>
                  </div>
                  @endif





            </div>
@endif
@endsection
@section('scripts')
<script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
<script src="//www.amcharts.com/lib/3/amcharts.js?v=7.0.6"></script>
<script src="//www.amcharts.com/lib/3/serial.js?v=7.0.6"></script>
<script src="//www.amcharts.com/lib/3/radar.js?v=7.0.6"></script>
<script src="//www.amcharts.com/lib/3/pie.js?v=7.0.6"></script>
<script src="//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.js?v=7.0.6"></script>
<script src="//www.amcharts.com/lib/3/plugins/animate/animate.min.js?v=7.0.6"></script>
<script src="//www.amcharts.com/lib/3/plugins/export/export.min.js?v=7.0.6"></script>
<script src="//www.amcharts.com/lib/3/themes/light.js?v=7.0.6"></script>
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script type="text/javascript">
var dataatas = {!! json_encode($hasi) !!};
    var dataProvideratas = [];// this variable you have to pass in dataProvider inside chart
    for(var key in dataatas) {
      dataProvideratas.push({
        category: dataatas[key]['categories'],
        visits: dataatas[key]['jumlahdata'],
      });
    }

    var chart = AmCharts.makeChart("kt_amcharts_pertama", {
      "rtl": KTUtil.isRTL(),
      "type": "serial",
       "hideCredits":true,
      "theme": "light",
      "dataProvider": dataProvideratas,
      "valueAxes": [{
        "gridColor": "#FFFFFF",
        "gridAlpha": 0.2,
        "dashLength": 0
      }],
      "series": [{
          "type": "LineSeries",
          "stroke": "#ff0000",
          "strokeWidth": 3
        }],
"valueAxes": [ {
"position": "left",
"title": "Jumlah Data"
}, {
"position": "bottom",
"title": @json($widget[0]->widget_widgetcaption)
} ],
"legend": {
"useGraphSettings": true
},
"titles": [
{
 "size": 15,
 "text": @json($widget[0]->widget_widgetcaption)
},
{
    "text": @json($widget[0]->widget_widgetsubcaption),
    "bold": false
}
],
      "gridAboveGraphs": true,
      "startDuration": 1,
      "graphs": [{
        "balloonText": "[[category]]: <b>[[value]]</b>",
        "fillAlphas": 0.8,
        "lineAlpha": 0.2,
        "type": "column",
        "valueField": "visits",
        "title": @json($widget[0]->widget_widgetcaption)
      }],
      "chartCursor": {
        "categoryBalloonEnabled": false,
        "cursorAlpha": 0,
        "zoomable": false
      },
      "categoryField": "category",
      "categoryAxis": {
        "gridPosition": "start",
        // "gridAlpha": 0,
        // "tickPosition": "start",
        // "tickLength": 20
      },
      "export": {
        "enabled": true
      }
    });

</script>
@endsection
