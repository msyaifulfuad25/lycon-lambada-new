{{-- Extends layout --}}
@extends('lam_custom_layouts.default')
    @section('styles')
    <link href="//www.amcharts.com/lib/3/plugins/export/export.css?v=7.0.6" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css">

    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/tabel.css') }}" rel="stylesheet" type="text/css"/>
    <style media="screen">
        .dt-button.dropdown-item {
            font-size: 14px;min-width: 175px;
            text-align: left;
            background-color:
            color: black;
            outline: none;
            background: transparent;
            border: 0;
        }

        .dt-button:hover{
            background-color: blue;
        }
        
        #spinner {
            position: absolute;
            top: 0;
            width: 100%;
            overflow: hidden;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        #loadernya {
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #3498db;
            width: 120px;
            height: 120px;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
            margin-left:250px;
            margin-top:250px;
        }

        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }

        /* .dropdown-item{
        font-size: 14px;min-width: 175px;
        text-align: left;

        } */

        #example_baru_previous i span{
            display: none;
        }

        /* .table thead tr th{
            min-height: 10px;
            width: auto;
        }

        .table tbody tr td{
            min-height: 10px;
            width: auto;
        } */

        .table tbody tr:nth-of-type(odd) {
            background-color: #ffffff;
        }

        .table tbody tr:nth-of-type(even) {
            background-color: #BBDEFB !important;
        }

        .table tbody tr:hover {
            color: #ffffff;
            background-color:#90CAF9 !important;
        }


        .dataTables_wrapper .dataTables_filter  input{
            width: 43px;
            transition: all 0.5s ease;
            -o-transition: all 0.5s ease;
            -ms-transition: all 0.5s ease;
            -moz-transition: all 0.5s ease;
            -webkit-transition: all 0.5s ease;
            /* margin-top: -30px; */
                right: 9px;
        }

        .dataTables_wrapper .dataTables_filter  .clicked{
            width: calc(20vw - 10px);
            transition: all 0.5s ease;
            -o-transition: all 0.5s ease;
            -ms-transition: all 0.5s ease;
            -moz-transition: all 0.5s ease;
            -webkit-transition: all 0.5s ease;
        }

        #example_baru_last{
            display: none;
        }

        #example_baru_first{
            display: none;
        }

        #example_baru_previous span {
            margin: inherit;
        }

        #example_baru>tbody>td {
            height: 10px;
        }

        .atas{
            background-color: #90CAF9 !important;
            height: 10px;
        }

        .paginate_input{
            width: 50px;
            text-align: center;
        }

        .dtfilter_search_btn{
            position: absolute;
            right: -3px;
            margin: 4px;
            top: auto;
        }

/* //////////////////////// */

        .pagepanel-10 {
            /* padding: 0.5% !important; */
            margin-bottom: -2.5% !important;
        }
        
        .pagepanel-23 {
            /* padding: 0.5% !important; */
            margin-bottom: -2.5% !important;
        }

        .modal-config {
            max-width: 70%;
        }


        .owl-carousel .owl-stage {
            display: flex;
        }

        .owl-carousel .owl-item img {
            width: 100%;
            height: 585px;
        }
    </style>
@endsection

{{-- Content --}}
@section('content')
    <div class="container">
    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
    @if($widget != '')
        <div>
            <!-- <div class="row" style="margin: 0% -2.6% 0% -2.4% !important"> -->

                @php
                    $panel_norow = 0;
                    $panel_nocol = 0;
                    $nocol = false;
                    $qtycol = 0;
                @endphp


                <?php
                    // dd($widget);
                    // dd($widget[2]->widget_widgetconfig);
                    // die;
                ?>

                @foreach($widget as $key => $wid)
                    @php
                        $cariwidget  = json_decode($wid->widget_configwidget);
                        if ($cariwidget) {
                            $hasil = @$cariwidget->Widget_Stat->datasource->dataquery;
                            $score = @\App\Menu::ambilQuery2($hasil);
                            $warna = @\App\Menu::cariWarna($wid->widget_panelcolor);
                            $classwarna = @\App\Menu::cariClassWarna($wid->widget_panelcolor);
                            if($wid->widget_widgeticon == 'fa fa-heart-o'){
                                $icon = 'far fa-heart';
                            }else{
                                $icon = $wid->widget_widgeticon;
                            }
                        } else {
                            $dataquery = json_decode($wid->widget_dataquery)[0]->query;
                        }

                    @endphp
                    
                    <!-- SETTING NOSPAN -->
                    @if($key == 0)
                        <div class="row">
                    @else
                        @if($wid->panel_norow == $widget[$key-1]->panel_norow)

                        @else
                            <div class="row">
                        @endif
                    @endif

                    @if($key == 0)
                        <div class="col-md-{{$wid->panel_nospan}} col-xs-{{$wid->panel_nospanxs}}">
                        @php
                            $qtycol += $wid->panel_nospan;
                        @endphp

                        @if($wid->panel_nocol)

                        @else

                        @endif
                    @else
                        @php
                            $qtycol += $wid->panel_nospan;
                        @endphp

                        @if($qtycol <= 12)
                            <div class="col-md-{{$wid->panel_nospan}} col-xs-{{$wid->panel_nospanxs}}">
                        @else
                            @if($wid->panel_norow != $widget[$key-1]->panel_norow)
                                <div class="col-md-{{$wid->panel_nospan}} col-xs-{{$wid->panel_nospanxs}}">
                            @endif
                        @endif

                        @if($wid->panel_nocol)
                            <div class="row">
                                <div style="width: 100%">
                        @endif
                    @endif
                    <!-- END SETTING NOSPAN -->


                    @php

                        if ($wid->widget_idtypewidget == 151 && $wid->panel_panelheigth < 517 ) {
                            $heighasli = 517;
                        } else {
                            $heighasli = $wid->panel_panelheigth;
                        }

                        if ( $wid->widget_idtypewidget == 1011 && $wid->panel_panelheigth < 517 ) {
                            $heighasli = 517;
                        } else {
                            $heighasli = $wid->panel_panelheigth;
                        }

                        if ($wid->widget_idtypewidget == 10  && $wid->panel_panelheigth < 100 ) {
                            $heighasli = 100;
                        } else {
                            $heighasli = $wid->panel_panelheigth;
                        }

                        if ($wid->widget_idtypewidget == 23 && $wid->panel_panelheigth < 450 ) {
                            $heighasli = 450;
                        } else {
                            $heighasli = $wid->panel_panelheigth;
                        }
                    @endphp

                    @if($wid->panel_nocol == 1)
                        @if($wid->widget_idtypewidget == 151)
                            <!-- <div class="col-md-{{ $wid->panel_nospan }} col-xs-{{ $wid->panel_nospanxs }}"> -->
                                <div class="card card-custom card-stretch gutter-b" style="height:{{ $heighasli }}px;margin-bottom:10px;">
                                    <div class="card-header">
                                        <div class="card-title">
                                            <h3 class="card-label">
                                                {{ $wid->widget_widgetcaption }}
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <input type="hidden"  id="slider" value="{{ $wid->panel_idwidget }}" style="width: 100%">
                                        <div class="owl-carousel">
                                        </div>
                                    </div>
                                </div>
                            <!-- </div> -->
                        @elseif($wid->widget_idtypewidget == 10 && $page->noid == 20090102)
                            @php
                                ${"'cari'.$wid->widget_idtypewidget"}  =  json_decode($wid->widget_configwidget);
                                ${"'hasil'.$wid->widget_idtypewidget"} = ${"'cari'.$wid->widget_idtypewidget"}->Widget_Stat->datasource->dataquery;
                                $score =\App\Menu::ambilQuery2(${"'hasil'.$wid->widget_idtypewidget"});
                                ${"'warna'.$wid->widget_idtypewidget"} =\App\Menu::cariWarna($wid->widget_panelcolor);
                                ${"'claswarna'.$wid->widget_idtypewidget"} =\App\Menu::cariClassWarna($wid->widget_panelcolor);
                            @endphp
                            <!-- <div class="col-md-{{ $wid->panel_nospan }} col-xs-{{ $wid->panel_nospanxs }}" > -->
                                <div class="card  card-stretch card-stretch-half gutter-b " style="height:{{ $heighasli }}px;background-color:{{ ${"'warna'.$wid->widget_idtypewidget"} }};height: 100px;margin-bottom:50px;">
                                    <div class="card-body p-0" style="position: relative;">
                                        <div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
                                            <div class="visual"><i class="{{ $wid->widget_widgeticon }} icon-7x" style="color: #FFFFFF;opacity: 0.1;margin-left: -55px;font-size: 0px;line-height: 0px;"></i></div>
                                            <div class="details"><div class="number">{{ $score }} {{ $wid->panel_nocol }} </div><div class="desc">{{ $wid->widget_widgetcaption }}</div></div>
                                    </div>
                                        <a class="{{   ${"'claswarna'.$wid->widget_idtypewidget"} }}" href="{{$wid->widget_moreidpage}}"> View more <i class="m-icon-swapright m-icon-white"></i></a>
                                    </div>
                                </div>
                            <!-- </div> -->
                        @elseif($wid->widget_idtypewidget == 10)
                            @php
                                ${"'cari'.$wid->widget_idtypewidget"}  =  json_decode($wid->widget_configwidget);
                                ${"'hasil'.$wid->widget_idtypewidget"} = ${"'cari'.$wid->widget_idtypewidget"}->Widget_Stat->datasource->dataquery;
                                $score =\App\Menu::ambilQuery2(${"'hasil'.$wid->widget_idtypewidget"});
                                ${"'warna'.$wid->widget_idtypewidget"} =\App\Menu::cariWarna($wid->widget_panelcolor);
                                ${"'claswarna'.$wid->widget_idtypewidget"} =\App\Menu::cariClassWarna($wid->widget_panelcolor);
                            @endphp
                            <!-- <div class="col-md-{{ $wid->panel_nospan }} col-xs-{{ $wid->panel_nospanxs }}"> -->
                                <!-- <div class="card  card-stretch card-stretch-half gutter-b " style="height:{{ $heighasli }}px;background-color:{{ ${"'warna'.$wid->widget_idtypewidget"} }};height: 100px;margin-bottom:50px;">
                                    <div class="card-body p-0" style="position: relative;">
                                        <div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
                                            <div class="visual"><i class="{{ $wid->widget_widgeticon }} icon-7x" style="color: #FFFFFF;opacity: 0.1;margin-left: -55px;font-size: 0px;line-height: 0px;"></i></div>
                                            <div class="details"><div class="number">{{ $score }}</div><div class="desc">{{ $wid->widget_widgetcaption }}</div></div>
                                        </div>
                                        <a class="{{   ${"'claswarna'.$wid->widget_idtypewidget"} }}" href="javascript:;"> View more <i class="m-icon-swapright m-icon-white"></i></a>
                                    </div>
                                </div> -->
                            <!-- </div> -->

                            <div id="pagepanel_9921010101" class="pagepanel-10">
                                <div class="pagepanel" id="statbox_9921010101">
                                    <div class="dashboard-stat" style="background-color: {{$warna}}; margin-bottom: 20px !important;">
                                        <div class="visual">
                                            <i class="{{ $icon }} icon-7x" style="color: #FFFFFF;opacity: 0.1;"></i>
                                        </div>
                                        <div class="details">
                                            <div class="number">
                                                {{ $score }}
                                            </div>
                                            <div class="desc">
                                                {{ $wid->widget_widgetcaption }}
                                            </div>
                                        </div>
                                        <a class="more {{  $classwarna  }}" href="{{$wid->widget_dataurl}}">
                                            View more <i class="m-icon-swapright m-icon-white"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="card  card-stretch card-stretch-half gutter-b " style="background-color:{{ $warna }};height: 100px;margin-bottom:50px;">
                                <div class="card-body p-0" style="position: relative;">
                                    <div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
                                        <div class="visual">
                                            <i class="{{ $icon }} icon-7x" style="color: #FFFFFF;opacity: 0.1;margin-left: -55px;font-size: 0px;line-height: 0px;"></i>
                                        </div>
                                        <div class="details">
                                            <div class="number">{{ $score }} </div>
                                            <div class="desc">{{ $wid->widget_widgetcaption }}</div>
                                            </div>
                                        </div>
                                        <a class="{{  $classwarna  }}" href="javascript:;"> View more <i class="m-icon-swapright m-icon-white"></i></a>
                                    </div>
                                </div>
                            </div> -->
                        @elseif($wid->widget_idtypewidget == 23)
                            <!-- <div class="col-md-{{ $wid->panel_nospan }} col-xs-{{ $wid->panel_nospanxs }}"> -->
                                <div class="card card-custom gutter-b">
                                    <div class="card-header">
                                        <div class="card-title">
                                            <h3 class="card-label">
                                                {{ $wid->widget_widgetcaption }}
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="card-body"  onload="ambilchart1({{ $wid->panel_idwidget }})">
                                        <input type="hidden"  id="cart" value="{{ $wid->panel_idwidget }}">
                                        <input type="hidden" name="panel[]"  id="cart_{{ $wid->panel_idwidget }}" value="{{ $wid->panel_idwidget }}">
                                        <div  id="kt_amcharts_{{ $wid->panel_idwidget }}" style="height: 500px; overflow: visible; text-align: left;"</div>
                                        </div>
                                    </div>
                                </div>
                            <!-- </div> -->
                        @elseif($wid->widget_idtypewidget == 1011)
                            @php
                                $warna =App\Menu::cariWarna($wid->widget_panelcolor);
                            @endphp
                            <!-- <div class="col-md-{{ $wid->panel_nospan }} col-xs-{{ $wid->panel_nospanxs }}"  > -->
                                <div class="card card-custom">
                                    <div class="card-header bg-primary d-flex" style="background-color:{{ $warna }}">
                                        <div class="card-title">
                                            <span class="card-icon">
                                                <i class="{{ $wid->widget_widgeticon }} text-white"></i>
                                            </span>
                                            <h3 class="card-label text-white">
                                                {{ $wid->widget_widgetcaption }}
                                            </h3>
                                        </div>
                                        <div class="card-toolbar" >
                                            <div id="tool-pertama">
                                                <a  onclick="AddData()" href="javascript:;" style="color: #FFFFFF;margin-right: 5px;" class="btn btn-sm"><i style="color:#FFFFFF" class="fa fa fa-save"></i> Tambah Data</a>
                                                <!-- <a class="btn btn-sm btn-success">
                                                    <i class="fa fa-save"></i> Tambah Data
                                                </a> -->
                                            </div>
                                            <div id="tool-kedua" >
                                                <a id="savedata" class="btn btn-sm btn-success" style="margin-right: 5px;">
                                                    <i class="fa fa-save"></i> Save
                                                </a>
                                                <a onclick="CancelData()" class="btn btn-sm btn-default" style="margin-right: 5px;">
                                                    <i class="fa fa-reply"></i> Cancel
                                                </a>
                                            </div>
                                        <div id="tool-ketiga">
                                            <a id="back" class="btn btn-sm btn-success" onclick="CancelData()"  style="margin-right: 5px;">
                                                <i class="fa fa-reply"></i> Back
                                            </a>
                                        </div>
                                        <div class="dropdown" style="margin-right: 5px;">
                                            <a style="background-color: transparent !important;border: 1px solid #95c9ed;color: #aad4f0;" class="btn btn-default btn-sm " href="javascript:;" data-toggle="dropdown">
                                                <i style="color:#FFFFFF" class="fa fa-cogs"></i>
                                                    Tools
                                                <i style="color:#FFFFFF" class="fa fa-angle-down"></i>
                                            </a>
                                            <ul style="" id="drop_export" class="dropdown-menu dropdown-menu-right">
                                                <li id="drop_export2"></li>
                                            </ul>
                                        </div>
                                        <!-- <div id="portlettools_pagepanel_2009150101" class="tools" > -->
                                        <a style="background-image: none !important;margin-right: 5px;/* height: 27px; */width: 27px !important;text-align: center;padding: 2px !important;border-radius: 50% !important;background-color: #2196F3;color: #FFF;" id="tool_refresh" href="javascript:;" onclick="refresh()"><i style="color:#FFFFFF" class="fa fa-sync-alt"></i></a>
                                        <a style="background-image: none !important;/* height: 27px; */width: 27px !important;text-align: center;padding: 2px !important;border-radius: 50% !important;background-color: #2196F3;color: #FFF;" id="tool_refresh_up" data-toggle="collapse" data-target=".card-body" href="javascript:;" ><i style="color:#FFFFFF" class="fa fa-sort"></i></a>
                                        <!-- </div> -->
                                    </div>
                                </div>
                                <div class="card-body">
                                    <input type="hidden" id="judul_excel" value="{{ $wid->widget_widgetcaption }}">
                                    <input type="hidden"  id="tabel1" value="{{ $namatable }}">
                                    <div class="row">
                                        <div class="btn-toolbar" role="toolbar" aria-label="...">
                                            <div id="filterlain">
                                            </div>
                                        </div>
                                    </div>
                                    <br>

                                    <div id="from-departement">
                                        <form class="form" id="formdepartement" >
                                        </form>
                                    </div>

                                    <div id="tabledepartemen" class="table table-bordered " style="width: 100%;">
                                        <div id="divsucces" class="alert alert-custom alert-light-success fade show mb-5" role="alert">
                                            <p id="notiftext"></p>
                                        </div>
                                        <table id="example_baru" class="table table-striped table-bordered nowrap" data-page-length="{{ $widgetgrid2->recperpage }}" cellspacing="0" width="100%" style="background-color:#BBDEFB !important">
                                        <!-- <table id="example_baru"  class="display table table-bordered " style="width:100%  !important;background-color:#BBDEFB "> -->
                                            <thead>
                                                {!! $header_atas !!}
                                                <tr>
                                                    <th class="no-sorter" ><input type="checkbox" id="vehicle1" name="no" value="" ></th>
                                                    <th class="no-sorter"  >No</th>
                                                    @foreach($columns as $col)
                                                        {!! $col !!}
                                                    @endforeach
                                                    <th class="no-sorter"  >
                                                        Action
                                                    </th>
                                                </tr>
                                            </thead>
                                        <!-- <tfoot id="footernya">
                                            <tr>
                                                <th colspan="3"></th>
                                            </tr>
                                            </tfoot> -->
                                        </table>
                                    </div>
                                </div>
                            <!-- </div> -->
                        @else
                        @endif
                    @else
                        @if($wid->widget_idtypewidget == 10)
                            @php
                                ${"'cari'.$wid->widget_idtypewidget"}  =  json_decode($wid->widget_configwidget);
                                ${"'hasil'.$wid->widget_idtypewidget"} = ${"'cari'.$wid->widget_idtypewidget"}->Widget_Stat->datasource->dataquery;
                                $score = \App\Menu::getResultQuery(2, json_decode($wid->widget_widgetconfig)->widgetstat->datasource->dataquery);
                                $widget_panelcolor = \App\Menu::getColor($wid->widget_panelcolor);
                                ${"'claswarna'.$wid->widget_idtypewidget"} = \App\Menu::cariClassWarna($wid->widget_panelcolor);
                            @endphp
                            <?php //print_r($score) ?>
                            @if(!$wid->panel_norow)
                                <!-- <div class="col-md-12 col-xs-12"> -->
                                    <!-- <div class="row"> -->
                            @endif
                                        <!-- <div id="pagepanel_{{$wid->panel_noid}}" class="col-md-{{$wid->panel_nospan}} col-xs-{{$wid->panel_nospanxs}} pagepanel-10"> -->
                                            <div class="pagepanel" id="statbox_{{$wid->panel_noid}}">
                                                <div class="dashboard-stat {{$wid->widget_panelcolor}}" style="margin-bottom: 10px !important;">
                                                    <div class="visual">
                                                        <i class="{{$wid->widget_widgeticon}}"></i>
                                                    </div>
                                                    <div class="details">
                                                        <div class="number">
                                                            {{ $score }}
                                                        </div>
                                                        <div class="desc">
                                                            {{ $wid->widget_widgetcaption }}
                                                        </div>
                                                    </div>
                                                    <a class="more" href="{{$wid->widget_moreidpage}}">
                                                        View more <i class="m-icon-swapright m-icon-white"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        <!-- </div>   -->
                            @if(!$wid->panel_norow)
                                    <!-- </div> -->
                                <!-- </div> -->
                            @endif
                        @elseif($wid->widget_idtypewidget == 10 && $page->noid == 10000003)
                            @if($key == 1 )
                                <!-- <div class="col-lg-6"> -->
                                    <!-- <div class="row"> -->
                            @endif
                                <!-- <div class="col-md-{{ $wid->panel_nospan*2 }} col-xs-{{ $wid->panel_nospanxs }}" > -->
                                    <div class="card  card-stretch card-stretch-half gutter-b " style="background-color:{{ $warna }};height: 100px;margin-bottom:50px;">
                                        <div class="card-body p-0" style="position: relative;">
                                            <div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
                                                <div class="visual">
                                                    <i class="{{ $icon }} icon-7x" style="color: #FFFFFF;opacity: 0.1;"></i>
                                                </div>
                                                <div class="details">
                                                    <div class="number">{{ $score }} </div>
                                                    <div class="desc">{{ $wid->widget_widgetcaption }}</div>
                                                    </div>
                                                </div>
                                                <a class="{{  $classwarna  }}" href="{{$wid->widget_moreidpage}}"> View more <i class="m-icon-swapright m-icon-white"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                        @if($key == 6)
                                        <!-- </div> -->
                                    <!-- </div> -->
                                <!-- </div> -->
                            @endif
                        @elseif($wid->widget_idtypewidget == 23)
                            <!-- <div id="pagepanel_{{$wid->panel_noid}}" class="col-md-{{$wid->panel_nospan}} col-xs-{{$wid->panel_nospanxs}} pagepanel-23"> -->
                            <div id="pagepanel_{{$wid->panel_noid}}" class="pagepanel-23">
                                <div class="pagepanel" id="linechartPortlet_{{$wid->panel_noid}}">
                                    <div class="portlet box {{$wid->widget_panelcolor}}" id="portletbody_{{$wid->panel_noid}}">
                                        <div id="title_{{$wid->panel_noid}}" class="portlet-title">
                                            <div id="caption_{{$wid->panel_noid}}" class="caption">
                                                <i class="fa fa-briefcase"></i>
                                                <span></span>
                                            </div>

                                            <div id="tool_{{$wid->panel_noid}}" class="tools">
                                                <a href="javascript:;" class="update" style="background-image: url(assets/refresh.png);width: 12px;"></a>
                                                <a href="javascript:;" class="config" style="background-image: url(assets/config.png);width: 12px;" onclick="showModalConfig({{$wid->panel_noid}})"></a>
                                            </div>
                                            <div id="action_{{$wid->panel_noid}}" class="actions"></div>
                                        </div>
                                        <div id="portletinnerbody_{{$wid->panel_noid}}" class="portlet-body" style="">
                                            <div class="card-body"  onload="ambilchart1({{ $wid->panel_idwidget }})">
                                                <input type="hidden"  id="cart" value="{{ $wid->panel_idwidget }}">
                                                <input type="hidden" name="panel[]"  id="cart_{{ $wid->panel_idwidget }}" value="{{ $wid->panel_idwidget }}">
                                                <div id="kt_amcharts_{{ $wid->panel_idwidget }}" style="height: 500px; overflow: visible; text-align: left;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @elseif($wid->widget_idtypewidget == 151)
                            <!-- <div class="col-md-{{ $wid->panel_nospan }} col-xs-{{ $wid->panel_nospanxs }}"  > -->
                                <!-- <div class="card card-custom card-stretch gutter-b" style="height:{{ $heighasli }}px;margin-bottom:10px;"> -->
                                    <!-- <div class="card-header">
                                        <div class="card-title">
                                            <h3 class="card-label">
                                                {{ $wid->widget_widgetcaption }}
                                            </h3>
                                        </div>
                                    </div> -->
                                    <!-- <div class="card-body" style="height: 100%">
                                        <input type="hidden"  id="slider" value="{{ $wid->panel_idwidget }}" style="height: 100%">
                                        <div class="owl-carousel">
                                        </div>
                                    </div>
                                </div> -->
                            <!-- </div> -->

                            <!-- <div id="pagepanel_1000000000" class="col-md-8 col-xs-12"> -->
                                <div class="pagepanel" style="height:{{ $heighasli }}px;margin-bottom:10px;">
                                    <div id="carousel_1000000000" class="carousel image-carousel slide" style="height:100%;">
                                        <div class="carousel-inner" style="height:100%;">
                                            <input type="hidden" id="slider" value="{{ $wid->panel_idwidget }}" style="height: 100%">
                                            <div class="owl-carousel">
                                            </div>
                                        </div>
                                        <!-- <a class="carousel-control left" href="#carousel_1000000000" data-slide="prev" style="margin-top:0px;">
                                            <i class="m-icon-big-swapleft m-icon-white">
                                            </i>
                                        </a>
                                        <a class="carousel-control right" href="#carousel_1000000000" data-slide="next" style="margin-top:0px;">
                                            <i class="m-icon-big-swapright m-icon-white">
                                            </i>
                                        </a>
                                        <ol class="carousel-indicators">
                                            <li data-target="#carousel_1000000000" data-slide-to="0" class="">
                                            </li>
                                            <li data-target="#carousel_1000000000" data-slide-to="1" class="">
                                            </li>
                                            <li data-target="#carousel_1000000000" data-slide-to="2" class="">
                                            </li>
                                            <li data-target="#carousel_1000000000" data-slide-to="3" class="">
                                            </li>
                                            <li data-target="#carousel_1000000000" data-slide-to="4" class="">
                                            </li>
                                            <li data-target="#carousel_1000000000" data-slide-to="5" class="">
                                            </li>
                                            <li data-target="#carousel_1000000000" data-slide-to="6" class="active">
                                            </li>
                                            <li data-target="#carousel_1000000000" data-slide-to="7" class="">
                                            </li>
                                        </ol> -->
                                    </div>
                                </div>
                            <!-- </div> -->

                        @elseif($wid->widget_idtypewidget == 1011)
                            @php
                                $warna =App\Menu::cariWarna($wid->widget_panelcolor);
                            @endphp
                            <!-- <div class="col-md-{{ $wid->panel_nospan }} col-xs-{{ $wid->panel_nospanxs }}"  > -->
                                <div class="card card-custom">
                                    <div class="card-header bg-primary d-flex" style="background-color:{{ $warna }}">
                                        <div class="card-title">
                                            <span class="card-icon">
                                                <i class="{{ $wid->widget_widgeticon }} text-white"></i>
                                            </span>
                                            <h3 class="card-label text-white">
                                                {{ $wid->widget_widgetcaption }}
                                            </h3>
                                        </div>
                                        <div class="card-toolbar" >
                                            <div id="tool-pertama">
                                                <a onclick="AddData()" class="btn btn-sm btn-success">
                                                    <i class="fa fa-save"></i> Tambah Data
                                                </a>
                                            </div>
                                            <div id="tool-kedua">
                                                <a id="savedata" class="btn btn-sm btn-info">
                                                    <i class="fa fa-save"></i> Save
                                                </a>
                                                <a onclick="CancelData()" class="btn btn-sm btn-warning">
                                                    <i class="fa fa-reply"></i> Cancel
                                                </a>
                                            </div>
                                            <div id="tool-ketiga">
                                                <a id="back" class="btn btn-sm btn-warning" onclick="CancelData()" >
                                                    <i class="fa fa-reply"></i> Back
                                                </a>
                                            </div>
                                    </div>
                                    </div>
                                    <div class="card-body">
                                        <input type="hidden"  id="tabel1" value="{{ $namatable }}">
                                        <div class="row">
                                            <div class="btn-toolbar" role="toolbar" aria-label="...">
                                                <div id="filterlain">
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div id="from-departement">
                                            <form class="form" id="formdepartement" >
                                            </form>
                                        </div>
                                        <div id="tabledepartemen" class="table-responsive" style="width: 100%;">
                                            <div id="divsucces" class="alert alert-custom alert-light-success fade show mb-5" role="alert">
                                                <p id="notiftext"></p>
                                            </div>
                                            <table id="example_baru" class="table table-striped table-bordered nowrap" data-page-length="{{ $widgetgrid2->recperpage }}" cellspacing="0" width="100%" style="background-color:#BBDEFB !important">
                                                <thead id="headnya">
                                                    {!! $header_atas !!}
                                                    <tr>
                                                        <th class="no-sorter" ><input type="checkbox" id="vehicle1" name="no" value="" ></th>
                                                        <th class="no-sorter" >No</th>
                                                        @foreach($columns as $col)
                                                            {!! $col !!}
                                                        @endforeach
                                                        <th class="no-sorter" >
                                                            Action
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <!-- <tfoot id="footernya">
                                                    <tr>
                                                    <th colspan="3"></th>
                                                    </tr>
                                                </tfoot> -->
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            <!-- </div> -->
                        @else
                        @endif
                    @endif


                    <!-- SETTING NOSPAN -->
                    @if($key == 0)
                        @if($wid->panel_nocol)

                        @else

                        @endif

                        </div>
                    @else
                        @if($wid->panel_nocol)
                                </div>
                            </div>
                        @else
                            </div>
                        @endif
                    @endif

                    @if($key == 0)
                        @if($wid->panel_norow == @$widget[$key+1]->panel_norow)

                        @else
                            </div>
                        @endif
                    @else
                        @if($wid->panel_norow == @$widget[$key+1]->panel_norow)

                        @else
                            @if($qtycol <= 12)
                                </div>
                            @else
                                    </div>
                                </div>
                            @endif

                            @php
                                $qtycol = 0;
                            @endphp
                        @endif
                    @endif
                    <!-- END SETTING NOSPAN -->
                @endforeach
            <!-- </div> -->
        </div>
    @endif

    <div id="modal-config" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-config modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="align-items: center; padding: 0.6%; background-color: rgb(53, 152, 220) !important; margin: 0.6%; height: 38px;">
                    <div class="fancybox-item fancybox-close" data-dismiss="modal" aria-hidden="true"></div>
                    <h4 class="modal-title" style="text-transform: uppercase; color: white;">
                        <b>Config</b>
                    </h4>
                </div>
                <div class="modal-body" style="padding: 1.5%">
                    <div class="row">
                        <div class="col-md-6" style="text-align:left;">
                            <div class="form-group">
                                <label>Type Chart</label>
                                <div class="form">
                                    <select class="form-control combobox" id="configtypechart">
                                        <option value="line">Line Chart</option>
                                        <option value="column">Column Chart</option>
                                        <option value="area">Area Chart</option>
                                        <option value="scatter">Scatter Chart</option>
                                        <option value="pie">Pie Chart</option>
                                        <option value="spiderweb_circle">Spider Circle Chart</option>
                                        <option value="spiderweb_angel">Spider Angel Chart</option>
                                        <option value="bar_negativestack">Bar Negative Chart</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" style="text-align:left;">
                            <div class="form-group">
                                <label>Type Stacked</label>
                                <div class="form">
                                    <select class="form-control combobox" id="configtypestacked">
                                        <option value="1">True</option>
                                        <option value="0">False</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="padding: 0">
                    <button type="submit" id="save" style="float:right;" class="btn green" onclick="applyConfig()">Apply</button>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('scripts')
    <script src="//www.amcharts.com/lib/3/amcharts.js?v=7.0.6"></script>
    <script src="//www.amcharts.com/lib/3/serial.js?v=7.0.6"></script>
    <script src="//www.amcharts.com/lib/3/radar.js?v=7.0.6"></script>
    <script src="//www.amcharts.com/lib/3/pie.js?v=7.0.6"></script>
    <script src="//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.js?v=7.0.6"></script>
    <script src="//www.amcharts.com/lib/3/plugins/animate/animate.min.js?v=7.0.6"></script>
    <script src="//www.amcharts.com/lib/3/plugins/export/export.min.js?v=7.0.6"></script>
    <script src="//www.amcharts.com/lib/3/themes/light.js?v=7.0.6"></script>
    <script src="https://cdn.amcharts.com/lib/4/core.js"></script>
    <script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
    <script src="{{ asset('js/pages/crud/forms/widgets/bootstrap-datepicker.js?v=7.0.6') }}"></script>
    <script type="text/javascript" src="  https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.3.1/js/dataTables.fixedColumns.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.21/pagination/input.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-loading-overlay/2.1.7/loadingoverlay.min.js"></script>
    <script  src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"> </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"> </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"> </script>
    <!-- <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.print.min.js "></script> -->

<script type="text/javascript">

    var confignoid = '';

    var owl = '';

    function getAllChart(id) {
        $.each(id, function (k, v) {
            // console.log('GET CHART')
            // console.log('#pagepanel_'+v+' .pagepanel .portletbody_'+v+' .title_'+v+' .tool_'+v+' .update')
            $.ajax({
                type: "POST",
                header:{
                    // 'X-CSRF-TOKEN':$('#token').val()
                },
                url: "/get_chart",
                dataType: "json",
                data:{
                    "_token":$('#token').val(),
                    "noid": v
                },
                success: function (response) {
                    console.log('RESPONSE')
                    console.log(response)
                    var element_update = '#pagepanel_'+v+' .pagepanel #portletbody_'+v+' #title_'+v+' #tool_'+v+' .update';
                    var target = '#pagepanel_'+v+' .pagepanel #portletbody_'+v+' #portletinnerbody_'+v+' .card-body';

                    if (response.defaultchart == 'column') {
                        chartBar(response, v)
                    } else if (response.defaultchart == 'pie') {
                        chartPie(response, v)
                    } else {
                        chartBar(response, v)
                    }


                    $(element_update).attr('onClick', `getChart('`+target+`', `+v+`)`)

                    var caption = '';
                    if (response.hasil) {
                        // caption = response.hasil[0].dataseries
                        caption = response.widget.widget_widgetcaption
                    }
                    $('#caption_'+response.widget.panel_noid+' span').text(caption)
                },
            });
        });
    }
    
    function getChart(target, noid) {
        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            url: "/get_chart",
            dataType: "json",
            data:{
                "_token":$('#token').val(),
                "noid": noid
            },
            success: function(response) {
                
                if (response.defaultchart == 'column') {
                    charttype = 'serial'; 
                    chartBar(response, noid)
                } else if (response.defaultchart == 'pie') {
                    charttype = 'pie';
                    chartPie(response, noid)
                } else {
                    charttype = 'serial';
                    chartBar(response, noid)
                }

            }
        })
    }

    function closeModalConfig() {
        $('#modal-config').modal('hide')
    }

    function showModalConfig(panelnoid) {
        $('#modal-config').modal('show')
        confignoid = panelnoid;
        // alert(panelnoid)
    }

    function applyConfig() {
        var configtypechart = $('#configtypechart').val();
        var configtypestacked = $('#configtypestacked').val();

        alert(configtypechart)
        alert(configtypestacked)
    }

    var loading = $('#loadernya').hide();
    $(document).ready(function() {
        // console.log(ambilchart1(1000000211))
        var values = $("input[name='panel[]']").map(function(){return $(this).val();}).get();
        getAllChart(values);
        var values2 = $("#slider").val();
        slider(values2);
        var values3 = $("#tabel1").val();
        cariBtnFlt(values3);
        var tablenya = $('#example_baru').DataTable({
            proccessing: true,
            serverSide: true,
            "scrollX": true,
            "searching": true,
            "ajax": {
                "url": "/ambildata_tabel",
                "type": "POST",
                cache: false,
                "dataType":'json',
                "data": {
                    "_token":$('#token').val(),
                    table: values3
                },
                beforeSend: function () {
                    $("#example_baru tbody").LoadingOverlay("show");
                },
                complete: function () {
                    $("#example_baru tbody").LoadingOverlay("hide");
                },
            },
            columnDefs: [{
                targets: [0,'no-sorter'],
                orderable: false
            }],
            "sPaginationType":"input",
            "dom": '<"top dtfiler"pli<"dtfilter2_search"f><"clear">>rt ',
            "oLanguage":{
            "loadingRecords": "Please wait - loading...",
                // "sProcessing": "<div id='loadernya'></div>",
            "sInfo": "<span class='seperator'>|</span> Total _TOTAL_ records",
            "sLengthMenu": "<span class='seperator'>|</span> Rec/Page _MENU_",
            "sInfoFiltered": "",
            "sInfoEmpty": "| No records found to show",
            "sEmptyTable": "No data available in table",
            "sZeroRecords": "No matching records found",
            "sSearch": "<a onclick='ambilhide()'  class='fa fa-search dtfilter_search_btn' ></a>",
            "oPaginate": {
                "sPrevious": "<i  class='fa fa-angle-left'></i>",
                "sNext": "<i  class='fa fa-angle-right'></i> ",
            }
        },
    });


    new $.fn.dataTable.Buttons( tablenya, {
        buttons: [
            {
                extend: 'excel',
                autoFilter: true,
                text: '<i class="fa fa-file-excel"></i> Export to Excel',
                titleAttr: 'Export to Excel',
                title: $("#judul_excel").val(),
                className: ' dropdown-item ',
                exportOptions: {
                modifier: {
                    page: 'current'
                }
            },
            "action": newexportaction,
            },
        ]
    });

    // filterTabel

    tablenya.buttons().container().appendTo('#drop_export2');

    function newexportaction(e, dt, button, config) {
        var self = this;
        var oldStart = dt.settings()[0]._iDisplayStart;
        dt.one('preXhr', function (e, s, data) {
        data.start = 0;
        data.length = 2147483647;
        dt.one('preDraw', function (e, settings) {
            // Call the original action function
            if (button[0].className.indexOf('buttons-copy') >= 0) {
                $.fn.dataTable.ext.buttons.copyHtml5.action.call(self, e, dt, button, config);
            } else if (button[0].className.indexOf('buttons-excel') >= 0) {
                $.fn.dataTable.ext.buttons.excelHtml5.available(dt, config) ?
                    $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config) :
                    $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
            } else if (button[0].className.indexOf('buttons-csv') >= 0) {
                $.fn.dataTable.ext.buttons.csvHtml5.available(dt, config) ?
                    $.fn.dataTable.ext.buttons.csvHtml5.action.call(self, e, dt, button, config) :
                    $.fn.dataTable.ext.buttons.csvFlash.action.call(self, e, dt, button, config);
            } else if (button[0].className.indexOf('buttons-pdf') >= 0) {
                $.fn.dataTable.ext.buttons.pdfHtml5.available(dt, config) ?
                    $.fn.dataTable.ext.buttons.pdfHtml5.action.call(self, e, dt, button, config) :
                    $.fn.dataTable.ext.buttons.pdfFlash.action.call(self, e, dt, button, config);
            } else if (button[0].className.indexOf('buttons-print') >= 0) {
                $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
            }
            dt.one('preXhr', function (e, s, data) {
                settings._iDisplayStart = oldStart;
                data.start = oldStart;
            });
            // Reload the grid with the original page. Otherwise, API functions like table.cell(this) don't work properly.
            setTimeout(dt.ajax.reload, 0);
                // Prevent rendering of the full data to the DOM
                return false;
            });
        });
        // Requery the server with the new one-time export settings
        dt.ajax.reload();
    };


    $('#example_baru_filter input').unbind();

    $('#example_baru_filter input').bind('keyup', function (e) {
        if (e.keyCode == 13) {
            tablenya.search(this.value).draw();
        }
    });

    var html = '';
    $('#formdepartement').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

    var divform = '<div class="form-group">';
    var groupbtn = '';
    $.ajax({
        type: "POST",
        header:{
            'X-CSRF-TOKEN':$('#token').val()
            },
        url: "/ambildata_from",
            dataType: "json",
        data:{
            "_token":$('#token').val(),
            "table": values3
        },
        success: function (response) {
            // console.log(response);
            divform += '<input type="hidden" id="noid" name="noid" value="">';
            $.each(response.field, function(key, value) {
                divform += "<label>"+value.label+"</label>";
                divform += value.field;
            });
            divform += '</div>';
            $('#formdepartement').html(divform);
        },
    });

    $("#from-departement").hide();
    $("#tool-kedua").hide();
    $("#tool-ketiga").hide();
    $("#divsucces").hide();
    $('.datepicker').datepicker({
        format: 'd-m-yyyy'
    });
    $('.datepicker2').datepicker({
        format: 'd-m-yyyy'
    });

    $(".first.paginate_button, .last.paginate_button").hide();
});

    function cariBtnFlt(values3) {
        var groupbtn = '';
        $.ajax({
            type : "POST",
            url: "/buttonFilt2",
            dataType: "json",
            data:{
                "_token":$('#token').val(),
                "table": values3
            },
            success: function (response) {
                $.each(response, function(key, value) {
                    groupbtn += '<div class="btn-group mr-1" role="group" aria-label="All">';
                    // groupbtn += '<a href="#"  class="filternya btn btn-outline-secondary btn-sm"   onclick="caridata(\'All\',0)"   style="background-color:#BBDEFB !important" >ALL </a>';
                    groupbtn += '<button class="filterTabel btn btn-outline-secondary btn-sm filterTabel"    style="background-color:#BBDEFB !important" >ALL </button>';
                    groupbtn += '</div>';
                    $.each(value, function(key2, value2) {
                        groupbtn += '<div class="btn-group mr-1" role="group" aria-label="'+value2.fieldname+'">';
                        // groupbtn += '<a href="#"  class="filternya btn btn-outline-secondary btn-sm"   style="background-color:#E3F2FD;" onclick="caridata(\''+value2.fieldname+'\' ) "    style="background-color:#BBDEFB !important" >'+value2.caption+' </a>';
                        groupbtn += '<button href="#"  class="filterTabel btn btn-outline-secondary btn-sm"   style="background-color:#E3F2FD;"    style="background-color:#BBDEFB !important" >'+value2.caption+' </button>';
                        // groupbtn += value2.taga;
                        groupbtn += '</div>';
                    });
                });
                $('#filterlain').append(groupbtn);
            }
        });

        $(".filterTabel").click(function () {
        $('#example_baru').dataTable().fnFilter('ok');
        // var rows = $("#stud_body").find("tr").hide();
        // rows.filter(":contains('OK')").show();
    });
}


    function ambilhide() {
        $('#example_baru_filter label input').toggleClass('clicked');
    }

    function caridata(id,value) {
        $('#example_baru').dataTable().fnFilter(id);
    }

    function slider(id) {
        owl = $('.owl-carousel').owlCarousel({
            loop:true,
            smartSpeed: 100,
            autoplay: true,
            autoplaySpeed: 100,
            mouseDrag: false,
            margin:10,
            animateIn: 'slideInUp',
            animateOut: 'fadeOut',
            nav:false,
            autoHeight: true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });

        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            url: "/ambilSlide",
            dataType: "json",
            data:{
                "_token":$('#token').val(),
                "id_slider": id
            },
            success: function (response) {
                // console.log(response.hasil);
                var base_url = '{{ url('/') }}';
                if(isNaN(base_url)) {
                    base_url = '';
                }
                var name1 = '';
                var name2 = '';

                var licarousel = new Array();

                $.each(response.hasil, function(k, v) {
                    var lc = '';
                    $.each(response.hasil, function(k2, v2) {
                        if (k == k2) {
                            lc += `<li class="active"></li>`;
                        } else {
                            lc += `<li onclick="btnGoToSlide(`+k2+`)"></li>`;
                        }
                    })
                    licarousel.push(lc)
                })

                $.each(response.hasil, function(i, item) {
                    // console.log('ddd');
                    // console.log(item);
                    // name1 = 'public/'+item.mainimage;
                    name1 = item.mainimage;

                    var random_num =  Math.floor(Math.random()*60);
                    owl.trigger('add.owl.carousel', [jQuery(`
                        <div class="notification-message">
                            <div class="carousel-caption">
                                <h4>
                                    <a href="`+base_url+item.linkidpage+`" target="_blank">`+item.newstitle+`</a>
                                </h4>
                                <p>`+item.newssubtitle+`</p>
                                <ol class="carousel-indicators" style="position: relative">
                                    `+licarousel[i]+`
                                </ol>
                            </div>
                            <img src="`+base_url+name1+`" class="user-image" alt="">
                            <a class="carousel-control left btn" onclick="btnPrev(); return false;" style="margin-top:0px;">
                                <i class="fa fa-arrow-left" style="color: grey; font-size: 30px"></i>
                            </a>
                            <a class="carousel-control right btn" onclick="btnNext(); return false;" style="margin-top:0px;">
                                <i class="fa fa-arrow-right" style="color: grey; font-size: 30px"></i>
                            </a>
                        </div>`)]);
                });
                owl.trigger('refresh.owl.carousel');
            },
        });
    }
    
    function btnPrev() {
        owl.trigger('prev.owl.carousel');
    }

    function btnNext() {
        owl.trigger('next.owl.carousel');
    }
    
    function btnGoToSlide(i) {
        owl.trigger('to.owl.carousel', i);
    }

    function chartBatang(response,id_cart) {
        var dataatas = response.hasil;
        var dataProvideratas = [];// this variable you have to pass in dataProvider inside chart
        for(var key in dataatas) {
            dataProvideratas.push({
            category: dataatas[key]['categories'],
            visits: dataatas[key]['jumlahdata'],
            });
        }

        var chart = AmCharts.makeChart("kt_amcharts_"+id_cart, {
            "rtl": KTUtil.isRTL(),
            "type": "serial",
            "hideCredits":true,
            "theme": "light",
            "dataProvider": dataProvideratas,
            "valueAxes": [{
                "gridColor": "#FFFFFF",
                "gridAlpha": 0.2,
                "dashLength": 0
            }],
            "series": [{
                "type": "LineSeries",
                "stroke": "#ff0000",
                "strokeWidth": 3
            }],
            "valueAxes": [
                {
                    "position": "left",
                    "title": "Jumlah Data"
                },{
                    "position": "bottom",
                    "title": response.widget.widget_widgetcaption
                }
            ],
            "legend": {
                "useGraphSettings": true
            },
            "titles": [
                {
                    "size": 15,
                    "text":response.widget.widget_widgetcaption
                },
                {
                    "text": response.widget.widget_widgetsubcaption,
                    "bold": false
                }
            ],
            "gridAboveGraphs": true,
            "startDuration": 1,
            "graphs": [{
                "balloonText": "[[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "visits",
                "title": response.widget.widget_widgetcaption
            }],
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "category",
            "categoryAxis": {
                "gridPosition": "start",
            },
            "export": {
                "enabled": true
            }
        });
    }
    
    function chartBar(response, noid) {
        var dataatas = response.hasil;
        var dataProvideratas = [];// this variable you have to pass in dataProvider inside chart
        for(var key in dataatas) {
            dataProvideratas.push({
            category: dataatas[key]['categories'] == undefined ? dataatas[key]['namaprop'] : dataatas[key]['categories'],
            visits: dataatas[key]['jumlahdata'],
            });
        }

        var chart = AmCharts.makeChart("kt_amcharts_"+noid, {
            "rtl": KTUtil.isRTL(),
            "type": 'serial',
            "hideCredits":true,
            "theme": "light",
            "dataProvider": dataProvideratas,
            "valueAxes": [{
                "gridColor": "#FFFFFF",
                "gridAlpha": 0.2,
                "dashLength": 0
            }],
            "series": [{
                "type": "LineSeries",
                "stroke": "#ff0000",
                "strokeWidth": 3
            }],
            "valueAxes": [
                {
                    "position": "left",
                    "title": "Jumlah Data"
                },{
                    "position": "bottom",
                    "title": response.widget.widget_widgetcaption
                }
            ],
            "legend": {
                "useGraphSettings": true
            },
            "titles": [
                {
                    "size": 15,
                    "text":response.widget.widget_widgetcaption
                },
                {
                    "text": response.widget.widget_widgetsubcaption,
                    "bold": false
                }
            ],
            "wrap": true,
            "gridAboveGraphs": true,
            "startDuration": 1,
            "graphs": [{
                "balloonText": "[[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "visits",
                "title": response.widget.widget_widgetcaption
            }],
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "category",
            "categoryAxis": {
                "gridPosition": "start",
                "autoWrap": true
            },
            "export": {
                "enabled": true
            }
        });
    }

    function chartPie(response,id_cart) {
        var data = response.hasil;
        var dataProvider = [];// this variable you have to pass in dataProvider inside chart
        
        for(var key in data) {
            dataProvider.push({
                country: data[key]['messagestatus'],
                litres: data[key]['jumlahdata'],
            });
        }

        var chart = AmCharts.makeChart("kt_amcharts_"+id_cart,{
            "type"    : "pie",
            "hideCredits"    : true,
            "theme"    : "light",
            "addClassNames": true,
            "titles": [
                {
                    "size": 15,
                    "text": response.widget.widget_widgetcaption
                },
                {
                    "text": response.widget.widget_widgetsubcaption,
                    "bold": false
                }
            ],
            // "pullOutDuration": 0,
            // "pullOutRadius": 0,
            "labelsEnabled": false,
            "titleField"  : "country",
            "valueField"  : "litres",
            "dataProvider"  : dataProvider,
            "numberFormatter": {
                "precision": -1,
                "decimalSeparator": ",",
                "thousandsSeparator": ""
            },
            "legend": {
                "align": "center",
                "position": "bottom",
                "marginRight": 0,
                "labelText": "[[title]]",
                "valueText": "",
                "valueWidth": 50,
                "textClickEnabled": true
            },
            "export": {
                "enabled": true
            }
        });
    }

    $("#savedata").click(function(e){
        e.preventDefault();
        // var kode = $("#kode").val();
        // var nama = $("#nama").val();
        // var namatable =$('#namatable').val();
        var formdata = $("#formdepartement").serialize();
        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            url: "/savedata",
            dataType: "json",
            data:{
                "_token":$('#token').val(),
                "data" :{
                    'data' : formdata,
                    'tabel': namatable
                }
            },
            success:function(result){
                if (result.status == 'add') {
                    $("#notiftext").text('Data  Berhasil di Tambah');
                } else {
                    $("#notiftext").text('Data  Berhasil di Edit');
                }

                document.getElementById("formdepartement").reset();
                refresh();
                $('#divsucces').show(0).delay(5000).hide(0);
                var x = document.getElementById("from-departement");
                var x2 = document.getElementById("tabledepartemen");
                var y = document.getElementById("tool-kedua");
                var y2 = document.getElementById("tool-pertama");
                x.style.display = "none";
                x2.style.display = "block";
                y.style.display = "none";
                y2.style.display = "block";
            }
        });
    });

    function refresh() {
        $('#example_baru').DataTable().ajax.reload();
    }

    function AddData() {
        var x = document.getElementById("from-departement");
        var x2 = document.getElementById("tabledepartemen");
        var y = document.getElementById("tool-kedua");
        var y2 = document.getElementById("tool-pertama");
        var tool_refresh = document.getElementById("tool_refresh");
        var tool_refresh_up = document.getElementById("tool_refresh_up");
    
        if (x.style.display === "none") {
            x.style.display = "block";
            x2.style.display = "none";
            y.style.display = "block";
            y2.style.display = "none";
            tool_refresh.style.display = "none";
            tool_refresh_up.style.display = "none";
        } else {
            x.style.display = "none";
            x2.style.display = "block";
        }
    }

    function CancelData() {
        var x = document.getElementById("from-departement");
        var x2 = document.getElementById("tabledepartemen");
        var y = document.getElementById("tool-kedua");
        var y2 = document.getElementById("tool-pertama");
        var y3 = document.getElementById("tool-ketiga");
        var tool_refresh = document.getElementById("tool_refresh");
        var tool_refresh_up = document.getElementById("tool_refresh_up");
        tool_refresh.style.display = "block";
        tool_refresh_up.style.display = "block";
        x.style.display = "none";
        x2.style.display = "block";
        y.style.display = "none";
        y2.style.display = "block";
        y3.style.display = "none";
        document.getElementById("formdepartement").reset();
    }

    function view(id) {
        var x = document.getElementById("from-departement");
        var x2 = document.getElementById("tabledepartemen");
        var y = document.getElementById("tool-kedua");
        var y2 = document.getElementById("tool-pertama");
        var y3 = document.getElementById("tool-ketiga");
        
        if (x.style.display === "none") {
            x.style.display = "block";
            x2.style.display = "none";
            y.style.display = "none";
            y2.style.display = "none";
            y3.style.display = "block";
        } else {
            x.style.display = "none";
            x2.style.display = "block";
        }

        $.ajax({
            url: '/edit',
            type: "POST",
            dataType: 'json',
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            data: {
                "id" : id,
                "namatable" : $("#namatable").val(),
                "_token": $('#token').val(),
            },
            success: function (data) {
                $.each(data, function(key, value) {
                    $.each(value, function(key2, value2) {
                        // console.log(key2);
                        $("#"+key2).val(value2);
                    });
                });
            },
        });
    }

    function edit(id) {
        var x = document.getElementById("from-departement");
        var x2 = document.getElementById("tabledepartemen");
        var y = document.getElementById("tool-kedua");
        var y2 = document.getElementById("tool-pertama");
        
        if (x.style.display === "none") {
            x.style.display = "block";
            x2.style.display = "none";
            y.style.display = "block";
            y2.style.display = "none";
        } else {
            x.style.display = "none";
            x2.style.display = "block";
        }

        $.ajax({
            url: '/edit',
            type: "POST",
            dataType: 'json',
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            data: {
                "id" : id,
                "namatable" : $("#namatable").val(),
                "_token": $('#token').val()
            },
            success: function (response) {
                $.each(response, function(key, value) {
                    $.each(value, function(key2, value2) {
                        $("#"+key2).val(value2);
                    });
                });
            }
        });
    }

    function hapus(id) {
        // console.log(id);return false;
        if (confirm('Are you sure you want to delete this?')) {
            $.ajax({
                url: '/hapus',
                type: "POST",
                header:{
                    'X-CSRF-TOKEN':$('#token').val()
                },
                data: {
                    "id" : id,
                    "namatable" :  $("#namatable").val(),
                    "_token": $('#token').val()
                },
                success: function () {
                    $("#notiftext").text('Data  Berhasil di hapus');
                    refresh();
                    $('#divsucces').show(0).delay(5000).hide(0);
                    var x = document.getElementById("from-departement");
                    var x2 = document.getElementById("tabledepartemen");
                    var y = document.getElementById("tool-kedua");
                    var y2 = document.getElementById("tool-pertama");
                    x.style.display = "none";
                    x2.style.display = "block";
                    y.style.display = "none";
                    y2.style.display = "block";
                }
            });
        }
    }


    (function ($) {
        function calcDisableClasses(oSettings) {
            var start = oSettings._iDisplayStart;
            var length = oSettings._iDisplayLength;
            var visibleRecords = oSettings.fnRecordsDisplay();
            var all = length === -1;

            // Gordey Doronin: Re-used this code from main jQuery.dataTables source code. To be consistent.
            var page = all ? 0 : Math.ceil(start / length);
            var pages = all ? 1 : Math.ceil(visibleRecords / length);

            var disableFirstPrevClass = (page > 0 ? '' : oSettings.oClasses.sPageButtonDisabled);
            var disableNextLastClass = (page < pages - 1 ? '' : oSettings.oClasses.sPageButtonDisabled);

            return {
                'first': disableFirstPrevClass,
                'previous': disableFirstPrevClass,
                'next': disableNextLastClass,
                'last': disableNextLastClass
            };
        }

        function calcCurrentPage(oSettings) {
            return Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength) + 1;
        }

        function calcPages(oSettings) {
            return Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength);
        }

        var firstClassName = 'first';
        var previousClassName = 'previous';
        var nextClassName = 'next';
        var lastClassName = 'last';

        var paginateClassName = 'paginate';
        var paginatePageClassName = 'paginate_page';
        var paginateInputClassName = 'paginate_input';
        var paginateTotalClassName = 'paginate_total';

        $.fn.dataTableExt.oPagination.input = {
            'fnInit': function (oSettings, nPaging, fnCallbackDraw) {
                var nFirst = document.createElement('span');
                var nPrevious = document.createElement('span');
                var nNext = document.createElement('span');
                var nLast = document.createElement('span');
                var nInput = document.createElement('input');
                var nTotal = document.createElement('span');
                var nInfo = document.createElement('span');

                var language = oSettings.oLanguage.oPaginate;
                var classes = oSettings.oClasses;
                var info = language.info || 'Page _INPUT_ of _TOTAL_';

                nFirst.innerHTML = language.sFirst;
                nPrevious.innerHTML = language.sPrevious;
                nNext.innerHTML = language.sNext;
                nLast.innerHTML = language.sLast;

                nFirst.className = firstClassName + ' ' + classes.sPageButton;
                nPrevious.className = previousClassName + ' ' + classes.sPageButton;
                nNext.className = nextClassName + ' ' + classes.sPageButton;
                nLast.className = lastClassName + ' ' + classes.sPageButton;

                nInput.className = paginateInputClassName;
                nTotal.className = paginateTotalClassName;

                if (oSettings.sTableId !== '') {
                    nPaging.setAttribute('id', oSettings.sTableId + '_' + paginateClassName);
                    nFirst.setAttribute('id', oSettings.sTableId + '_' + firstClassName);
                    nPrevious.setAttribute('id', oSettings.sTableId + '_' + previousClassName);
                    nNext.setAttribute('id', oSettings.sTableId + '_' + nextClassName);
                    nLast.setAttribute('id', oSettings.sTableId + '_' + lastClassName);
                }

                nInput.type = 'text';

                info = info.replace(/_INPUT_/g, '</span>' + nInput.outerHTML + '<span>');
                info = info.replace(/_TOTAL_/g, '</span>' + nTotal.outerHTML + '<span>');
                nInfo.innerHTML = '<span>' + info + '</span>';

                nPaging.appendChild(nFirst);
                nPaging.appendChild(nPrevious);
                $(nInfo).children().each(function (i, n) {
                    nPaging.appendChild(n);
                });
                nPaging.appendChild(nNext);
                nPaging.appendChild(nLast);

                $(nFirst).click(function() {
                    var iCurrentPage = calcCurrentPage(oSettings);
                    if (iCurrentPage !== 1) {
                        oSettings.oApi._fnPageChange(oSettings, 'first');
                        fnCallbackDraw(oSettings);
                    }
                });

                $(nPrevious).click(function() {
                    var iCurrentPage = calcCurrentPage(oSettings);
                    if (iCurrentPage !== 1) {
                        oSettings.oApi._fnPageChange(oSettings, 'previous');
                        fnCallbackDraw(oSettings);
                    }
                });

                $(nNext).click(function() {
                    var iCurrentPage = calcCurrentPage(oSettings);
                    if (iCurrentPage !== calcPages(oSettings)) {
                        oSettings.oApi._fnPageChange(oSettings, 'next');
                        fnCallbackDraw(oSettings);
                    }
                });

                $(nLast).click(function() {
                    var iCurrentPage = calcCurrentPage(oSettings);
                    if (iCurrentPage !== calcPages(oSettings)) {
                        oSettings.oApi._fnPageChange(oSettings, 'last');
                        fnCallbackDraw(oSettings);
                    }
                });

                $(nPaging).find('.' + paginateInputClassName).keyup(function (e) {
                    // 38 = up arrow, 39 = right arrow
                    if (e.which === 38 || e.which === 39) {
                        this.value++;
                    }
                    // 37 = left arrow, 40 = down arrow
                    else if ((e.which === 37 || e.which === 40) && this.value > 1) {
                        this.value--;
                    }

                    if (this.value === '' || this.value.match(/[^0-9]/)) {
                        /* Nothing entered or non-numeric character */
                        this.value = this.value.replace(/[^\d]/g, ''); // don't even allow anything but digits
                        return;
                    }

                    var iNewStart = oSettings._iDisplayLength * (this.value - 1);
                    if (iNewStart < 0) {
                        iNewStart = 0;
                    }
                    if (iNewStart >= oSettings.fnRecordsDisplay()) {
                        iNewStart = (Math.ceil((oSettings.fnRecordsDisplay()) / oSettings._iDisplayLength) - 1) * oSettings._iDisplayLength;
                    }

                    oSettings._iDisplayStart = iNewStart;
                    oSettings.oInstance.trigger("page.dt", oSettings);
                    fnCallbackDraw(oSettings);
                });

                // Take the brutal approach to cancelling text selection.
                $('span', nPaging).bind('mousedown', function () { return false; });
                $('span', nPaging).bind('selectstart', function() { return false; });

                // If we can't page anyway, might as well not show it.
                var iPages = calcPages(oSettings);
                if (iPages <= 1) {
                    $(nPaging).hide();
                }
            },

            'fnUpdate': function (oSettings) {
                if (!oSettings.aanFeatures.p) {
                    return;
                }

                var iPages = calcPages(oSettings);
                var iCurrentPage = calcCurrentPage(oSettings);

                var an = oSettings.aanFeatures.p;
                if (iPages <= 1) // hide paging when we can't page
                {
                    $(an).hide();
                    return;
                }

                var disableClasses = calcDisableClasses(oSettings);

                $(an).show();

                // Enable/Disable `first` button.
                $(an).children('.' + firstClassName)
                    .removeClass(oSettings.oClasses.sPageButtonDisabled)
                    .addClass(disableClasses[firstClassName]);

                // Enable/Disable `prev` button.
                $(an).children('.' + previousClassName)
                    .removeClass(oSettings.oClasses.sPageButtonDisabled)
                    .addClass(disableClasses[previousClassName]);

                // Enable/Disable `next` button.
                $(an).children('.' + nextClassName)
                    .removeClass(oSettings.oClasses.sPageButtonDisabled)
                    .addClass(disableClasses[nextClassName]);

                // Enable/Disable `last` button.
                $(an).children('.' + lastClassName)
                    .removeClass(oSettings.oClasses.sPageButtonDisabled)
                    .addClass(disableClasses[lastClassName]);

                // Paginate of N pages text
                $(an).find('.' + paginateTotalClassName).html(iPages);

                // Current page number input value
                $(an).find('.' + paginateInputClassName).val(iCurrentPage);
            }
        };
    })(jQuery);
</script>
@endsection
