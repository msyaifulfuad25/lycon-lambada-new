<div class="col-md-{{ $wid->panel_nospan }} col-xs-{{ $wid->panel_nospanxs }}"  >
  <div class="card card-custom">
            <div class="card-header bg-primary d-flex" style="background-color:{{ $warna }}">
              <div class="card-title">
                      <span class="card-icon">
                          <i class="{{ $wid->widget_widgeticon }} text-white"></i>
                      </span>
                <h3 class="card-label text-white">
                  {{ $wid->widget_widgetcaption }}
                </h3>
              </div>
                  <div class="card-toolbar" >
                    <div id="tool-pertama">
                      <a onclick="AddData()" class="btn btn-sm btn-success">
                          <i class="fa fa-save"></i> Tambah Data
                      </a>
                    </div>
                    <div id="tool-kedua">
                      <a id="savedata" class="btn btn-sm btn-info">
                          <i class="fa fa-save"></i> Save
                      </a>
                      <a onclick="CancelData()" class="btn btn-sm btn-warning">
                          <i class="fa fa-reply"></i> Cancel
                      </a>
                    </div>
                    <div id="tool-ketiga">
                      <a id="back" class="btn btn-sm btn-warning" onclick="CancelData()" >
                          <i class="fa fa-reply"></i> Back
                      </a>
                    </div>
                  </div>
            </div>
            <div class="card-body">
              <input type="text"  id="tabel1" value="{{ $namatable }}">
              <div class="row">
                <div class="btn-toolbar" role="toolbar" aria-label="...">
                  <div id="filterlain">
                  </div>
                </div>
              </div>
              <br>



              <div id="from-departement">
                <form class="form" id="formdepartement" >
                </form>
              </div>
              <div id="tabledepartemen" class="table-responsive" style="width: 100%;">
                <div id="divsucces" class="alert alert-custom alert-light-success fade show mb-5" role="alert">
                  <p id="notiftext"></p>
                </div>
                <div id="datad">
                </div>
              </div>
              </div>
          </div>
</div>
</div>
