{{-- Extends layout --}}
@extends('layout_lambada.default')
@section('styles')
    <link href="//www.amcharts.com/lib/3/plugins/export/export.css?v=7.0.6" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/3.3.1/css/fixedColumns.dataTables.min.css">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/tabel.css') }}" rel="stylesheet" type="text/css"/>
@endsection
{{-- Content --}}
@section('content')
@if($widget != '')



<div class="row" style="margin-bottom:10px">
            @foreach($widget as $key=> $wid)
            {{-- dd($wid) --}}
            @php
            if($wid->widget_idtypewidget == 151 && $wid->panel_panelheigth < 517 ){
              $heighasli = 517;
            }else{
              $heighasli = $wid->panel_panelheigth;
            }

            if($wid->widget_idtypewidget == 1011 && $wid->panel_panelheigth < 517 ){
              $heighasli = 517;
            }else{
                $heighasli = $wid->panel_panelheigth;
            }

            if($wid->widget_idtypewidget == 10  && $wid->panel_panelheigth < 100 ){
              $heighasli = 100;
            }else{
                $heighasli = $wid->panel_panelheigth;
            }
            if($wid->widget_idtypewidget == 23 && $wid->panel_panelheigth < 450 ){
              $heighasli = 450;
            }else{
              $heighasli = $wid->panel_panelheigth;
            }
            @endphp
            @if($wid->panel_nocol == 1)
            @if($wid->widget_idtypewidget == 151)
            <div class="col-md-{{ $wid->panel_nospan }} col-xs-{{ $wid->panel_nospanxs }}"  >
              <div class="card card-custom card-stretch gutter-b" style="height:{{ $heighasli }}px;margin-bottom:10px;">
                <div class="card-header">
              <div class="card-title">
                <h3 class="card-label">
                  {{ $wid->widget_widgetcaption }} {{ $wid->panel_nocol }}
                </h3>
              </div>
            </div>
            <div class="card-body">
              <input type="hidden"  id="slider" value="{{ $wid->panel_idwidget }}">
              <div class="owl-carousel">
              </div>
            </div>
            </div>
            </div>
            @elseif($wid->widget_idtypewidget == 10)
            @php

            ${"'cari'.$wid->widget_idtypewidget"}  =  json_decode($wid->widget_configwidget);
            ${"'hasil'.$wid->widget_idtypewidget"} = ${"'cari'.$wid->widget_idtypewidget"}->Widget_Stat->datasource->dataquery;
            ${"'nilai'.$wid->widget_idtypewidget"} =\App\Menu::ambilQuery2(${"'hasil'.$wid->widget_idtypewidget"});
            ${"'warna'.$wid->widget_idtypewidget"} =\App\Menu::cariWarna($wid->widget_panelcolor);
            ${"'claswarna'.$wid->widget_idtypewidget"} =\App\Menu::cariClassWarna($wid->widget_panelcolor);
            @endphp
            @if($key == 1)
            <div class="col-4">
            @endif
            <div class="col-md-{{ $wid->panel_nospan }} col-xs-{{ $wid->panel_nospanxs }}" >
              <div class="card  card-stretch card-stretch-half gutter-b " style="height:{{ $heighasli }}px;background-color:{{ ${"'warna'.$wid->widget_idtypewidget"} }};height: 100px;margin-bottom:50px;">
                <div class="card-body p-0" style="position: relative;">
                 <div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
                                    <div class="visual"><i class="{{ $wid->widget_widgeticon }} icon-7x" style="color: #FFFFFF;opacity: 0.1;margin-left: -55px;font-size: 0px;line-height: 0px;"></i></div>
                                        <div class="details"><div class="number">{{   ${"'nilai'.$wid->widget_idtypewidget"} }} {{ $wid->panel_nocol }} </div><div class="desc">{{ $wid->widget_widgetcaption }}</div></div>
                 </div>
                  <a class="{{   ${"'claswarna'.$wid->widget_idtypewidget"} }}" href="javascript:;"> View more <i class="m-icon-swapright m-icon-white"></i></a>
              </div>
              </div>
              </div>
              @if($key == 4)
                          </div>
              @endif
          @elseif($wid->widget_idtypewidget == 23)
          <div class="col-md-{{ $wid->panel_nospan }} col-xs-{{ $wid->panel_nospanxs }}">
          <div class="card card-custom gutter-b">
            <div class="card-header">
              <div class="card-title">
                <h3 class="card-label">
                  {{ $wid->widget_widgetcaption }} {{ $wid->panel_nocol }}
                </h3>
              </div>
            </div>
            <div class="card-body"  onload="ambilchart1({{ $wid->panel_idwidget }})">
              <input type="hidden"  id="cart" value="{{ $wid->panel_idwidget }}">
              <input type="hidden" name="panel[]"  id="cart_{{ $wid->panel_idwidget }}" value="{{ $wid->panel_idwidget }}">
              <div  id="kt_amcharts_{{ $wid->panel_idwidget }}" style="height: 500px; overflow: visible; text-align: left;"</div>
            </div>
          </div>
        </div>
        </div>
        @elseif($wid->widget_idtypewidget == 1011)
        @php
        $warna =App\Menu::cariWarna($wid->widget_panelcolor);
        @endphp
        <div class="col-md-{{ $wid->panel_nospan }} col-xs-{{ $wid->panel_nospanxs }}"  >
          <div class="card card-custom">
                    <div class="card-header bg-primary d-flex" style="background-color:{{ $warna }}">
                      <div class="card-title">
                              <span class="card-icon">
                                  <i class="{{ $wid->widget_widgeticon }} text-white"></i>
                              </span>
                        <h3 class="card-label text-white">
                          {{ $wid->widget_widgetcaption }}
                        </h3>
                      </div>
                          <div class="card-toolbar" >
                            <div id="tool-pertama">
                              <a onclick="AddData()" class="btn btn-sm btn-success">
                                  <i class="fa fa-save"></i> Tambah Data
                              </a>
                            </div>
                            <div id="tool-kedua">
                              <a id="savedata" class="btn btn-sm btn-info">
                                  <i class="fa fa-save"></i> Save
                              </a>
                              <a onclick="CancelData()" class="btn btn-sm btn-warning">
                                  <i class="fa fa-reply"></i> Cancel
                              </a>
                            </div>
                            <div id="tool-ketiga">
                              <a id="back" class="btn btn-sm btn-warning" onclick="CancelData()" >
                                  <i class="fa fa-reply"></i> Back
                              </a>
                            </div>
                          </div>
                    </div>
                    <div class="card-body">
                      <input type="hidden"  id="tabel1" value="{{ $wid->widget_maintable }}">
                      <div id="from-departement">
                        <form class="form" id="formdepartement" >
                        </form>
                      </div>
                      <div id="tabledepartemen" class="table-responsive" style="width: 100%;">
                        <div id="divsucces" class="alert alert-custom alert-light-success fade show mb-5" role="alert">
                          <p id="notiftext"></p>
                        </div>
                        <div id="datad">
                        </div>
                      </div>
                      </div>
                  </div>
        </div>
            @else

            @endif

            @else

            @if($wid->widget_idtypewidget == 151)
            <div class="col-md-{{ $wid->panel_nospan }} col-xs-{{ $wid->panel_nospanxs }}"  >
              <div class="card card-custom card-stretch gutter-b" style="height:{{ $heighasli }}px;margin-bottom:10px;">
                <div class="card-header">
              <div class="card-title">
                <h3 class="card-label">
                  {{ $wid->widget_widgetcaption }} {{ $wid->panel_nocol }}
                </h3>
              </div>
            </div>
            <div class="card-body">
              <input type="hidden"  id="slider" value="{{ $wid->panel_idwidget }}">
              <div class="owl-carousel">
              </div>
            </div>
            </div>
            </div>
            @elseif($wid->widget_idtypewidget == 10)
            @php
            ${"'cari'.$wid->widget_idtypewidget"}  =  json_decode($wid->widget_configwidget);
            ${"'hasil'.$wid->widget_idtypewidget"} = ${"'cari'.$wid->widget_idtypewidget"}->Widget_Stat->datasource->dataquery;
            ${"'nilai'.$wid->widget_idtypewidget"} =\App\Menu::ambilQuery2(${"'hasil'.$wid->widget_idtypewidget"});
            ${"'warna'.$wid->widget_idtypewidget"} =\App\Menu::cariWarna($wid->widget_panelcolor);
            ${"'claswarna'.$wid->widget_idtypewidget"} =\App\Menu::cariClassWarna($wid->widget_panelcolor);
            @endphp
            <div class="col-md-{{ $wid->panel_nospan }} col-xs-{{ $wid->panel_nospanxs }}" >
              <div class="card  card-stretch card-stretch-half gutter-b " style="height:{{ $heighasli }}px;background-color:{{ ${"'warna'.$wid->widget_idtypewidget"} }};height: 100px;margin-bottom:50px;">
                <div class="card-body p-0" style="position: relative;">
                 <div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
                                    <div class="visual"><i class="{{ $wid->widget_widgeticon }} icon-7x" style="color: #FFFFFF;opacity: 0.1;margin-left: -55px;font-size: 0px;line-height: 0px;"></i></div>
                                        <div class="details"><div class="number">{{   ${"'nilai'.$wid->widget_idtypewidget"} }} {{ $wid->panel_nocol }} </div><div class="desc">{{ $wid->widget_widgetcaption }}</div></div>
                 </div>
                  <a class="{{   ${"'claswarna'.$wid->widget_idtypewidget"} }}" href="javascript:;"> View more <i class="m-icon-swapright m-icon-white"></i></a>
              </div>
              </div>
              </div>

          @elseif($wid->widget_idtypewidget == 23)

          <div class="col-md-{{ $wid->panel_nospan }} col-xs-{{ $wid->panel_nospanxs }}">
          <div class="card card-custom gutter-b">
            <div class="card-header">
              <div class="card-title">
                <h3 class="card-label">
                  {{ $wid->widget_widgetcaption }} {{ $wid->panel_nocol }}
                </h3>
              </div>
            </div>
            <div class="card-body"  onload="ambilchart1({{ $wid->panel_idwidget }})">
              <input type="hidden"  id="cart" value="{{ $wid->panel_idwidget }}">
              <input type="hidden" name="panel[]"  id="cart_{{ $wid->panel_idwidget }}" value="{{ $wid->panel_idwidget }}">
              <div  id="kt_amcharts_{{ $wid->panel_idwidget }}" style="height: 500px; overflow: visible; text-align: left;"</div>
            </div>
          </div>
        </div>
        </div>
        @elseif($wid->widget_idtypewidget == 1011)
        @php
        $warna =App\Menu::cariWarna($wid->widget_panelcolor);
        @endphp
        <div class="col-md-{{ $wid->panel_nospan }} col-xs-{{ $wid->panel_nospanxs }}"  >
          <div class="card card-custom">
                    <div class="card-header bg-primary d-flex" style="background-color:{{ $warna }}">
                      <div class="card-title">
                              <span class="card-icon">
                                  <i class="{{ $wid->widget_widgeticon }} text-white"></i>
                              </span>
                        <h3 class="card-label text-white">
                          {{ $wid->widget_widgetcaption }}
                        </h3>
                      </div>
                          <div class="card-toolbar" >
                            <div id="tool-pertama">
                              <a onclick="AddData()" class="btn btn-sm btn-success">
                                  <i class="fa fa-save"></i> Tambah Data
                              </a>
                            </div>
                            <div id="tool-kedua">
                              <a id="savedata" class="btn btn-sm btn-info">
                                  <i class="fa fa-save"></i> Save
                              </a>
                              <a onclick="CancelData()" class="btn btn-sm btn-warning">
                                  <i class="fa fa-reply"></i> Cancel
                              </a>
                            </div>
                            <div id="tool-ketiga">
                              <a id="back" class="btn btn-sm btn-warning" onclick="CancelData()" >
                                  <i class="fa fa-reply"></i> Back
                              </a>
                            </div>
                          </div>
                    </div>
                    <div class="card-body">
                      <input type="hidden"  id="tabel1" value="{{ $wid->widget_maintable }}">
                      <div id="from-departement">
                        <form class="form" id="formdepartement" >
                        </form>
                      </div>
                      <div id="tabledepartemen" class="table-responsive" style="width: 100%;">
                        <div id="divsucces" class="alert alert-custom alert-light-success fade show mb-5" role="alert">
                          <p id="notiftext"></p>
                        </div>
                        <div id="datad">
                        </div>
                      </div>
                      </div>
                  </div>
        </div>
          @else
            @endif
            @endif

            @endforeach

@endif
  <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
@endsection
@section('scripts')
<script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
<script src="//www.amcharts.com/lib/3/amcharts.js?v=7.0.6"></script>
<script src="//www.amcharts.com/lib/3/serial.js?v=7.0.6"></script>
<script src="//www.amcharts.com/lib/3/radar.js?v=7.0.6"></script>
<script src="//www.amcharts.com/lib/3/pie.js?v=7.0.6"></script>
<script src="//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.js?v=7.0.6"></script>
<script src="//www.amcharts.com/lib/3/plugins/animate/animate.min.js?v=7.0.6"></script>
<script src="//www.amcharts.com/lib/3/plugins/export/export.min.js?v=7.0.6"></script>
<script src="//www.amcharts.com/lib/3/themes/light.js?v=7.0.6"></script>
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="{{ asset('js/pages/crud/forms/widgets/bootstrap-datepicker.js?v=7.0.6') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/@tensorflow/tfjs/dist/tf.min.js"> </script>
<script type="text/javascript" src="  https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.3.1/js/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.21/api/sum().js"></script>
<script src="{{ asset('js/custom_dashboard.js') }}"></script>
<!-- <script src="{{ asset('js/custom_dashboard_slider.js') }}"></script> -->
@endsection
