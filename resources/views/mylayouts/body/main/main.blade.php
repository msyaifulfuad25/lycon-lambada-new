@include('mylayouts.body.main.header_mobile.header_mobile')




<!--begin::Main-->
<div class="d-flex flex-column flex-root">

    <!--begin::Page-->
    <div class="d-flex flex-row flex-column-fluid page">

    @include('mylayouts.body.main.aside.aside')

        <!--begin::Wrapper-->
        <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
    
            @include('mylayouts.body.main.header.header')

            <!--begin::Content-->
            <div class="content d-flex flex-column flex-column-fluid" id="kt_content" style="background-color: {{$backgroundcolor}}">

                @yield('content')

            </div>
            <!--end::Content-->

            @include('mylayouts.body.main.footer.footer')

        </div>
        <!--end::Wrapper-->

    </div>
    <!--end::Page-->
    
</div>
<!--end::Main-->