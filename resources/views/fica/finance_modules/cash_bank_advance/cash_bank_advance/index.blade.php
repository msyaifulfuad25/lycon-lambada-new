@extends('lam_custom_layouts.default')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row mb-2">
                <div class="col-md-6">
                    <h5><b>Cash Bank Advance</b></h5>
                </div>
                <div class="col-md-6 text-right">
                    <button class="btn btn-primary" onclick="addData()"><i class="fa fa-plus"></i>Add</button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h6><b>Data</b></h6>
                            <table id="table_most_item_requested" class="table compact">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal</th>
                                        <th>Keterangan</th>
                                        <th>Request By</th>
                                        <th>Total</th>
                                        <th>Cost Control</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modal_data" tabindex="-1" role="dialog" aria-labelledby="modal_dataLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_dataLabel"><b id="modal_data_name"></b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" id="form_data">
                    <div class="form-group row">
                        <input type="hidden" name="noid" id="noid" value="">
                        <!-- Date -->
                        <label for="date" class="col-md-4 col-form-label">Tanggal <span class="text-danger">*</span></label>
                        <div class="col-md-8">
                            <input type="text" name="date" id="date" class="form-control datepicker" placeholder="Tanggal">
                        </div>
                    </div>
                    <div class="form-group row">
                        <!-- Description -->
                        <label for="description" class="col-md-4 col-form-label">Keterangan</label>
                        <div class="col-md-8">
                            <textarea name="description" id="description" class="form-control" cols="30" rows="10"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <!-- Total -->
                        <label for="total" class="col-md-4 col-form-label">Total <span class="text-danger">*</span></label>
                        <div class="col-md-8">
                            <input type="text" name="total" id="total" class="form-control input-mask" placeholder="Total" value="0">
                        </div>
                    </div>
                    <div class="form-group row">
                        <!-- Request By -->
                        <label for="request_by" class="col-md-4 col-form-label">Request By <span class="text-danger">*</span></label>
                        <div class="col-md-8">
                            <select name="request_by" id="request_by" class="form-control select2">
                                <option value="">Pilih Request By</option>
                                <?php foreach ($employees as $k => $v) { ?>
                                    <option value="{{ $v->noid }}">{{ $v->code }} - {{ $v->name }}</option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <!-- Cost Control -->
                        <label for="id_cost_control" class="col-md-4 col-form-label">Cost Control</label>
                        <div class="col-md-8">
                            <select name="id_cost_control" id="id_cost_control" class="form-control select2">
                                <option value="">Pilih Cost Control</option>
                                <?php foreach ($cost_controls as $k => $v) { ?>
                                    <option value="{{ $v->noid }}">{{ $v->kode }} - {{ $v->projectname }}</option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn_close_data" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="button" id="btn_save_data" class="btn btn-success" onclick="saveData()"><i class="fa fa-save"></i> Save</button>
                <button type="button" id="btn_update_data" class="btn btn-success" onclick="updateData()"><i class="fa fa-save"></i> Update</button>
            </div>
            </div>
        </div>
    </div>
@endsection

@section('styles')
    <!-- Datatable -->
    <link href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <!-- Select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection
    
@section('scripts')
    <!-- Datatable -->
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <!-- Select2 -->
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    
    <!-- Custom -->
    <script type="text/javascript">
        var main = {
            urlcode: 20211001
        };

        $(document).ready(function() {
            getSelect2();
            getData();
        })

        const getData = () => {
            $('#table_most_item_requested').DataTable().destroy();
            $('#table_most_item_requested').DataTable({
                serverSide: true,
                order: [[1, 'desc']],
                pageLength: 10,
                processing: true,
                columns: [
                    { mData:'no', width: "1%", orderable: false, class: 'text-right' },
                    { mData: 'date', width: "9%" },
                    { mData: 'description', width: "20%" },
                    { mData: 'name_request_by', width: "25%" },
                    { 
                        mData: 'total', 
                        width: "15%", 
                        class:'text-right',
                        render:$.fn.dataTable.render.number(',','.',0,'Rp.') 
                    },
                    { mData: 'cost_control', width: "15%" },
                    { mData:'action', width: "15%", orderable:false, class:'text-right' },
                ],
                ajax: {
                    url: `/getData_${main.urlcode}`,
                    type: 'POST',
                    dataType:'json',
                    data: {
                        _token:$('meta[name="csrf-token"]').attr('content'),
                    },
                },
            });
            $('<button class="btn btn-secondary ml-2" onclick="getData()"><i class="icon-refresh"></i></button>').appendTo('#table_most_item_requested_filter');
        }

        const resetFormData = () => {
            $('#form_data #date').val(null);
            $('#form_data #description').val(null);
            $('#form_data #total').val(null);
            $('#form_data #request_by').val(null).trigger('change');
            $('#form_data #id_cost_control').val(null).trigger('change');
        }

        const addData = () => {
            resetFormData();
            $('#btn_save_data').show();
            $('#btn_update_data').hide();
            showModal({name: 'Add Data', element_id: '#modal_data'});
        }

        const saveData = () => {
            let form = $('#form_data').serializeArray();
            $.ajax({
                type: 'POST',
                url: `/saveData_${main.urlcode}`,
                dataType: 'json',
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'form': form,
                },
                success: function(response) {
                    if (!response.success) {
                        $.notify({ message: response.message },{ type: 'danger', z_index: 2000 });
                    } else {
                        resetFormData();
                        hideModal({element_id: '#modal_data'});
                        getData();
                        $.notify({ message: response.message },{ type: 'success', z_index: 2000 });
                    }
                }
            })
        }

        const editData = (params) => {
            $('#form_data #noid').val(params.noid);
            $('#form_data #date').val(params.date);
            $('#form_data #description').val(params.description);
            $('#form_data #total').val(params.total);
            $('#form_data #request_by').val(params.request_by).trigger('change');
            $('#form_data #id_cost_control').val(params.id_cost_control).trigger('change');
            $('#btn_update_data').show();
            $('#btn_save_data').hide();
            showModal({name: 'Edit Data', element_id: '#modal_data'});
        }

        const updateData = () => {
            let form = $('#form_data').serializeArray();
            $.ajax({
                type: 'POST',
                url: `/updateData_${main.urlcode}`,
                dataType: 'json',
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'form': form,
                },
                success: function(response) {
                    if (!response.success) {
                        $.notify({ message: response.message },{ type: 'danger', z_index: 2000 });
                    } else {
                        resetFormData();
                        hideModal({element_id: '#modal_data'});
                        getData();
                        $.notify({ message: response.message },{ type: 'success', z_index: 2000 });
                    }
                }
            })
        }

        const deleteData = (params) => {
            Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: 'POST',
                        url: `/deleteData_${main.urlcode}`,
                        dataType: 'json',
                        data: {
                            '_token': $('meta[name="csrf-token"]').attr('content'),
                            'noid': params.noid,
                        },
                        success: function(response) {
                            if (!response.success) {
                                $.notify({ message: response.message },{ type: 'danger', z_index: 2000 });
                            } else {
                                getData();
                                $.notify({ message: response.message },{ type: 'success', z_index: 2000 });
                            }
                        }
                    });
                }
            })
        }

        const showModal = (params) => {
            $(`${params.element_id}_name`).text(params.name);
            $(params.element_id).modal('show');
        }

        const hideModal = (params) => {
            $(params.element_id).modal('hide');
        }

        const getSelect2 = () => {
            $('.select2').select2();
            $('.select2').css('width', '100%');
            $('.select2 .selection .select2-selection').css('height', '31px');
            $('.select2 .selection .select2-selection .select2-selection__rendered').css('margin-top', '-6px');
            $('.select2 .selection .select2-selection .select2-selection__arrow').css('margin-top', '4px');
        }

        ////////////////////////////////

        $('#form_data #date').bind('change', () => {
            $('#form_data #description').focus();
        });
        $('#form_data #description').bind('keyup', (e) => {
            if (e.keyCode == 13) $('#form_data #total').focus();
        });
        $('#form_data #total').bind('keyup', (e) => {
            if (e.keyCode == 13) $('#form_data #request_by').select2('focus');
        });
        $('#form_data #request_by').bind('select2:close', () => {
            $('#form_data #id_cost_control').select2('focus');
        });
        $('#form_data #id_cost_control').bind('select2:close', () => {
            $('#btn_save_data').focus();
        });

        $(".datepicker").datepicker( {
            format: "d-M-yyyy",
            autoclose: true
        });

        $('.input-mask').inputmask('decimal', {
            'alias': 'numeric',
            'groupSeparator': ',',
            'autoGroup': true,
            'allowMinus': false,
            'prefix': 'Rp.',
            'placeholder': '0'
        });
    </script>
@endsection