{{-- Extends layout --}}

@extends('layout_lambada.default')

@section('styles')
    <!-- <link href="{{ asset('css/component.css') }}" rel="stylesheet" type="text/css"/> -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet"></link>
    <link href="//www.amcharts.com/lib/3/plugins/export/export.css?v=7.0.6" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.0.2/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.0.2/css/bootstrap-datetimepicker.css">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/tabel.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet" type="text/css"/>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <style media="screen">
        .container {
            display: none
        }

        .dataTable thead .sorting_asc,
        .dataTable thead .sorting_desc,
        .dataTable thead .sorting_disabled,
        .dataTable thead .sorting {
            padding: 3px 3px 3px 3px !important;
            /* color: black !important; */
        }
        
        .dataTable tbody td,
        .dataTable tbody td:hover,
        .dataTable tbody tr:hover {
            padding: 3px 3px 3px 3px !important;
        }

        .my_row {
            height: <?= $heightwgf ?>px;
        }

        .my_row:hover {
            /* display: none; */
            padding: 0px 0px 0px 0px !important;
        }

        .table-bordered th,
        .table-bordered td,
        .table-bordered tfoot th {
            border: 1px solid #95c9ed
        }

        .dataTables_wrapper {
            margin-top: -1px !important
        }
        
        #table_pagepanel_9921050101 {
            width:2500px !important;
            border: 0;
        }

        /* tbody tr td {
            padding: 0px 5px 0px 5px !important;f
            width: 100px !important;
        } */

        /* .dataTable thead th:nth-child(10) {
            width: 140px !important;
        } */

        /* .dataTable tbody tr td:nth-child(10) {
            width: 140px !important;
        } */


        /* tbody:nth-child(odd) tr td:nth-child(odd) {
            width: 100px !important;
        } */
    </style>
@endsection


{{-- Content --}}
@section('content')
    <div class="container">
    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
    <div id="pagepanel" class="card card-custom flat">
        <div class="card-header card-header_ bg-main-1 d-flex flat" >
            <div class="card-title">
                <span class="card-icon">
                    <i id="widgeticon" class="text-white"></i>
                </span>
                <h3 id="widgetcaption" class="card-label text-white">
                </h3>
            </div>
            <div class="card-toolbar" >
                <div id="tool-pertama">
                    <a  onclick="addData()" href="javascript:;" id="btn-add-data" style="color: #FFFFFF;margin-right: 5px;" class="btn btn-sm flat" onmouseover="mouseoverAddData('btn-add-data')" onmouseout="mouseoutAddData('btn-add-data')"><i style="color:#FFFFFF" class="fa fa fa-save"></i> Tambah Data</a>
                </div>
                <div id="tool-kedua" style="display: none">
                    <a id="savedata" class="btn btn-sm btn-success flat" style="margin-right: 5px;">
                        <i class="fa fa-save"></i> Save
                    </a>
                    <a onclick="cancelData()" class="btn btn-sm btn-default flat" style="margin-right: 5px;">
                        <i class="fa fa-reply"></i> Cancel
                    </a>
                </div>
                <div id="tool-ketiga" style="display: none">
                    <a id="back" class="btn btn-sm btn-success flat" onclick="cancelData()"  style="margin-right: 5px;">
                        <i class="fa fa-reply"></i> Back
                    </a>
                </div>
                <div class="dropdown" style="margin-right: 5px;">
                    <a style="background-color: transparent !important; border: 1px solid #95c9ed; color: #aad4f0;" class="btn btn-default btn-sm flat" href="javascript:;" data-toggle="dropdown">
                        <i style="color:#FFFFFF" class="fa fa-cogs"></i>
                            Tools
                        <i style="color:#FFFFFF" class="fa fa-angle-down"></i>
                    </a>
                    <ul style="" id="drop_export" class="dropdown-menu dropdown-menu-right">
                        <li id="drop_export2"></li>
                    </ul>
                </div>
                <a style="background-image: none !important; margin-right: 5px; /* height: 27px; */ width: 27px !important; text-align: center; padding: 2px !important; border-radius: 50% !important; background-color: #2196F3; color: #FFF;" id="tool_refresh" href="javascript:;" onclick="refresh()" class="bg-main-1"><i style="color:#FFFFFF" class="fa fa-sync-alt"></i></a>
                <a style="background-image: none !important; margin-right: 5px; /* height: 27px; */ width: 27px !important; text-align: center; padding: 2px !important; border-radius: 50% !important; background-color: #2196F3; color: #FFF;" id="tool_filter" href="javascript:;" onclick="showModalFilter()" class="bg-main-1"><i style="color:#FFFFFF" class="fa fa-filter"></i></a>
                <a style="background-image: none !important; /* height: 27px; */ width: 27px !important; text-align: center; padding: 2px !important; border-radius: 50% !important; background-color: #2196F3; color: #FFF;" id="tool_refresh_up" onClick="toggleButtonCollapse()" href="javascript:;" class="bg-main-1"><i style="color:#FFFFFF" class="fa fa-sort"></i></a>
                </div>
            </div>
            <div class="card-body card-body-custom flat">
                <div class="row">
                    <!-- <div class="col"> -->
                        <div id="space_filter_button" style="margin-bottom: 10px">
                        </div>
                    <!-- </div> -->
                </div>
                <input type="hidden" id="judul_excel">
                <input type="hidden"  id="tabel1" value="{{$code}}">
                <input type="hidden"  id="namatable">
                <input type="hidden"  id="url_code">
                
                <div class="row">
                    <div class="btn-toolbar" role="toolbar" aria-label="...">
                        <div id="filterlain">
                        </div>
                    </div>
                </div>

                <div class="cursor"></div>
                <!-- <input type="text" id="datetime" readonly> -->
                <div id="from-departement">
                    <form data-toggle="validator" class="form" id="formdepartement">
                        <div class="row" id="divform" style="margin-top: 20px">
                        </div>
                    </form>
                </div>
                <div id="tabledepartemen" class="table table-bordered " style="width: 100%; border: 0px; margin: -10px 0px -10px 0px !important">
                    <!-- <div id="divsucces" class="alert alert-custom alert-light-success fade show mb-5" role="alert">
                        <p id="notiftext"></p>
                    </div> -->

                    <div id="appendInfoDatatable" style="border: 1px solid #3598dc; margin-top: 10px"></div>
                    <div id="appendButtonDatatable"></div>
                    <div class="table-scrollable" style="border: 1px solid #3598dc;">
                        <table bg-color="blue" class="nowrap table table-striped table-bordered table-hover  dataTable no-footer" id="table_pagepanel_9921050101"  aria-describedby="table_pagepanel_9921050101_info" role="grid">
                            <thead id="thead_pagepanel_9921050101">
                                <tr id="thead" style="background: #90CAF9"></tr>
                                <tr id="thead-columns-d" style="background: #BBDEFC"></tr>
                            </thead>
                            <tfoot>
                                <tr id="tfoot-columns-d" style="background: #BBDEFC">
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                    <!-- <table id="example_baru" class="table table-striped table-bordered table-condensed nowrap" cellspacing="0" width="100%" style="background-color:#BBDEFB !important; border-collapse: collapse !important;">
                        <thead>
                            <tr id="thead" class="bg-main"></tr>
                            <tr id="thead-columns">
                            </tr>
                        </thead> -->
                        <!-- <tfoot>
                            <tr id="tfoot-columns">
                            </tr>
                        </tfoot> -->
                    <!-- </table> -->
            </div>
        </div>
    </div>

    <br>

    <div id="modal-filter" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-filter modal-dialog">
            <div data-notify="container" id="modal-notif" class="col-xs-11 col-sm-3 alert alert-danger" role="alert" style="z-index: 10000 !important; position: fixed; opacity: 0.95 !important; left: 73% !important; top: 5%; display: none">
                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                <span data-notify="icon"></span> 
                <span data-notify="title" style="opacity: 1 !important"><b style="font-size: 18px"><i class="fa fa-times" style="font-size: 18px; color: white;"></i> Error</b></span> 
                <span data-notify="message" style="opacity: 1 !important">Add data has been error.</span>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:;" class="fancybox-item fancybox-close" onclick="closeModalFilter()"></a>
                    <h4 class="modal-title">Config Datatable</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="formfilter">
                        <div class="row" id="filtercontent">
                            <div class="col-md-12">
                                <div class="portlet box blue-steel" style="margin: 8px;">
                                    <div class="portlet-title">
                                        <div class="caption" style="font-weight: bold">
                                            <i class="fa fa-filter"></i>
                                            Datatable Filter
                                        </div>
                                        <div class="actions">
                                            <a onclick="saveFilter()" href="javascript:;" class="btn green btn-sm">
                                                <i class="fa fa-save text-white"></i> Save
                                            </a>
                                            <a id="addgroup" href="javascript:;" class="btn yellow btn-sm">
                                                <i class="fa fa-indent text-white"></i> Add Group
                                            </a>
                                            <a id="delgroup" href="javascript:;" class="btn red btn-sm">
                                                <i class="fa fa-trash text-white"></i> Delete Group
                                            </a>
                                            <a onclick="addRow()" href="javascript:;" class="btn purple btn-sm">
                                                <i class="fa fa-list text-white"></i> Add Row
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row" style="margin:0;">
                                            <div class="col-md-12" style="padding: 0 0 15px 0;">
                                                <div id="filtercontentgroup">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin:0;">
                                            <div class="col-md-12">
                                                <div id="filtercontentbody">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- PREVIEW FILE -->
    

    <!-- TRY WITH PORTLET -->
    <!-- <br>
    <br>
    <br>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="pagepanel">
                <div class="portlet box portlet-dt yellow-crusta">
                    <div class="portlet-title">
                        <div id="portletcaption_pagepanel_1000020101" class="caption">
                            <i class="fa fa-briefcase"></i>
                            <span id="caption_pagepanel_1000020101">Customer Master List</span>
                        </div>
                        <div id="portlettools_pagepanel_1000020101" class="tools" style="display: block;">
                            <a id="toolreload_pagepanel_1000020101" href="javascript:;" class="update">
                                <i class="fa fa-refresh"></i>
                            </a>
                            <a id="toolconfigfilter_pagepanel_1000020101" href="javascript:;" class="config">
                                <i class="fa fa-filter"></i>
                            </a>
                            <a id="toolconfig_pagepanel_1000020101" href="javascript:;" class="config">
                                <i class="fa fa-tasks"></i>
                            </a>
                            <a id="toolclose_pagepanel_1000020101" href="javascript:;" class="remove">
                                <i class="fa fa-close"></i>
                            </a>
                            <a id="toolcollapse_pagepanel_1000020101" href="javascript:;" class="collapse">
                                <i class="fa fa-unsorted"></i>
                            </a>
                        </div>
                        <div id="portletactions_pagepanel_1000020101" class="actions">
                            <div class="btn-group" style="display: none;">
                                <a href="javascript:;" id="first" class="btn btn-sm green" style="">
                                    <i class="fa fa fa-save"></i> Save
                                </a>
                            </div>
                            <a id="add_actioncancel_pagepanel_1000020101" href="javascript:;" class="btn default" style="display: none;">
                                <i class="fa fa fa-reply"></i> Cancel
                            </a>
                            <a id="100002010111_actionadd_pagepanel_1000020101" href="javascript:;" class="btn yellow-casablanca" style="display: inline-block;">
                                <i class="fa fa-info-circle"></i> Broadcast Notification
                            </a>
                            <a id="new_actionadd_pagepanel_1000020101" href="javascript:;" class="btn blue" style="display: inline-block;">
                                <i class="fa fa fa-save"></i> Tambah Data
                            </a>
                            <div class="btn-group">
                                <a class="btn btn-default btn-sm" href="javascript:;" data-toggle="dropdown">
                                    <i class="fa fa-cogs"></i> Tools <i class="fa fa-angle-down"></i>
                                </a>
                                <ul id="actiontools_actiontools_pagepanel_1000020101" class="dropdown-menu pull-right">
                                    <li id="actiontools_actionexportexcel_pagepanel_1000020101">
                                        <a href="javascript:;">
                                            <i class="fa fa-file-excel-o"></i> Export to Excel 
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
@endsection
@section('scripts')
<!-- <script src="//www.amcharts.com/lib/3/amcharts.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/serial.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/radar.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/pie.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/plugins/animate/animate.min.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/plugins/export/export.min.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/themes/light.js?v=7.0.6"></script> -->
<!-- <script src="https://cdn.amcharts.com/lib/4/core.js"></script> -->
<!-- <script src="https://cdn.amcharts.com/lib/4/charts.js"></script> -->
<script src="{{ asset('js/pages/crud/forms/widgets/bootstrap-datepicker.js?v=7.0.6') }}"></script>
<script type="text/javascript" src="  https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.3.1/js/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.21/pagination/input.js"></script>
<script  src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.0.2/js/bootstrap-datetimepicker.min.js"></script>
<!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script> -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-loading-overlay/2.1.7/loadingoverlay.min.js"></script>
<script src="{{ asset('js/jquery-currency.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script>
    $("#datetime").datetimepicker({
        // format: 'yyyy-mm-dd hh:ii'
    });
</script>
<script type="text/javascript">

    var maincms = {};
    var statusadd = true;
    var values = $("input[name='panel[]']").map(function(){return $(this).val();}).get();
    var values2 = $("#slider").val();
    var values3 = $("#tabel1").val();
    var select_all = false;
    var cursor_top = 0;
    var selecteds = new Array();
    var my_table = '';
    var mytable = '';
    var groupbtn = '';
    var selected_filter = {
        operand: [],
        fieldname: [],
        operator: [],
        value: [],
    };
    var filter_tabs = new Array();
    var selected_filter_tabs = 0;
    var filtercontenttabs = {
        filtercontentgroup: new Array(),
        filtercontentbody: new Array(),
        filtercontentrow: new Array(),
    }
    var colsorted = [0,1];
    var roles = {
        fieldname: new Array(),
        newhidden: new Array(),
        edithidden: new Array(),
    };
    var cmsfilterbutton = new Array()

    var functionfooter = new Array()

    var noids = new Array();

    // $('#select_all').change(function() {
    function selectAllCheckbox() {
        var checkboxes = $('[name="checkbox[]"]');
        checkboxes.prop('checked', $('#select_all').is(':checked'));

        if (select_all == false) {
            select_all = true;
            $('.odd').css('background-color', '#90CAF9');
            $('.even').css('background-color', '#90CAF9');
            $('.checkbox').val('TRUE');
            selecteds = noids;
        } else {
            select_all = false;
            $('.odd').css('background-color', '#ffffff');
            $('.even').css('background-color', '#BBDEFB');
            $('.checkbox').val('FALSE');
            selecteds = new Array();
        }
    }
    // });
    
    function changeCheckbox(index, id) {
        var checkbox = $('.checkbox'+index);
        if (checkbox.val() == 'FALSE') {
            $('.row'+index).css('background-color', '#90CAF9');
            $('.checkbox'+index).val('TRUE');
            selecteds.push(id);
        } else {
            if (index % 2 == 0) {
                $('.row'+index).css('background-color', '#ffffff');
            } else {
                $('.row'+index).css('background-color', '#BBDEFB');
            }
            $('.checkbox'+index).val('FALSE')
            var replace_selecteds = new Array();
            $.each(selecteds, function(k, v) {
                if (v != id) {
                    replace_selecteds.push(v)
                }
            })
            selecteds = replace_selecteds
        }
        select_all = false;
        $('#select_all').prop('checked', false);
        // console.log(selecteds)
    }

    function showButtonAction(element, index) {
        var rect = element.getBoundingClientRect();

        this.resetCss()

        $('.my_row').css('height', '33px')
        $('.button-action-'+index).css('position', 'fixed')
        $('.button-action-'+index).css('top', parseInt(rect.y+5))
        $('.button-action-'+index).css('right', '68px')
        // $('.button-action-'+index).css('overflow', 'hidden')

        // console.log(parseInt(rect.y+8))
        // console.log('Element is ' + offset + ' vertical pixels from <body>');
    }

    function resetCss() {
        $('.button-action').removeAttr('style')
    }

    function mouseoverAddData(element_id) {
        $('#'+element_id).css('background', '#337AB7')
    }

    function mouseoutAddData(element_id) {
        $('#'+element_id).css('background', '#3598dc')
    }
    
    $(document).ready(function() {
        getData()
        getCmsThead()
        getCmsColumns()
        $('#example_baru').attr('data-page-length', maincms['widgetgrid']['recperpage']);
        $('#widgeticon').addClass(maincms['widget']['widgeticon']);
        $('#widgetcaption').html(maincms['widget']['widgetcaption']);
        $('#judul_excel').attr('value', maincms['widget']['widgetcaption']);
        $('#tabel1').attr('value', maincms['menu']['noid']);
        $('#namatable').attr('value', maincms['menu']['noid']);
        $('#url_code').attr('value', maincms['menu']['noid']);
        // console.log(maincms['widgetgrid']['widgeticon'])

        $("#from-departement").hide();
        $("#tool-kedua").hide();
        $("#tool-ketiga").hide();
        $("#divsucces").hide();
        $('.datepicker').datepicker({
            format: 'd-m-yyyy'
            // startDate: '-3d'
        });
        $('.datepicker2').datepicker({
            format: 'd-m-yyyy'
            // startDate: '-3d'
        });
        // $(".first.paginate_button, .last.paginate_button").hide();

        getChart(values);
        // getSlider(values2);
        // tabel(values3);
        // getDT(values3);
        // getForm()
        // getDatatable();
        getFilterButton(values3)

        function getChart(id) {
            $.each(id, function (key, val) {
                $.ajax({
                    type: "POST",
                    header:{
                        'X-CSRF-TOKEN':$('#token').val()
                    },
                    url: "/ambilchart",
                    dataType: "json",
                    data:{
                        "_token":$('#token').val(),
                        "id_cart": val
                    },
                    success: function (response) {
                        if (response.defaultchart == 'column') {
                            chartBatang(response,val);
                        } else if (response.defaultchart == 'pie') {
                            chartPie(response,val);
                        } else {
                            chartBatang(response,val);
                        }
                    },
                });
            });
        }

        function getCmsColumns() {
            var theadcolumns = '';
            var colspanfooter = 2;
            var index = 1;
            if (maincms['widgetgrid']['colcheckbox']) {             
                colspanfooter = 3;
                index = 2;
                theadcolumns += `<th style="width: 10px"><input type="checkbox" id="select_all" onchange="selectAllCheckbox()"></th>`;
            }
            theadcolumns += `<th style="width: 10px">No</th>`;
            var tfootcolumns = ``
            // console.log(maincms['widgetgridfield'])
            $.each(maincms['widgetgridfield'], function(k, v) {
                if (v.colwidth != 0) {
                    var colwidth = v.colwidth+'px';
                } else {
                    var colwidth = 'auto';
                }
                if (v.colshow == 1) {
                    theadcolumns += `<th class="text-`+v.colheaderalign.toLowerCase()+`" style="width: `+colwidth+`; height: `+v.colheight+`px;">`+v.fieldcaption+`</th>`
                    // theadcolumns += `<th>`+v.fieldcaption+`</th>`
                    // tfootcolumns += `<th></th>`
                    // var dataconfig = JSON.stringify(v.colsummaryfield)['dataconfig'];

                    if (v.colsummaryfield) {
                        var dataconfig = JSON.parse(v.colsummaryfield).dataconfig[0].summarytype+'('+index+')'
                        functionfooter.push({
                            "index": index,
                            "function": dataconfig,
                        })
                    } else {
                        var dataconfig = '';
                    }

                    if (v.colsummaryfield) {
                        tfootcolumns += `<th class="text-right" colspan="`+colspanfooter+`" data-function="`+dataconfig+`" style="border: 1px solid #95c9ed"></th>`
                        // console.log('collll '+colspanfooter)
                        colspanfooter = 1
                    } else {
                        colspanfooter++
                    }
                    index++
                }
                if (maincms['widgetgridfield'].length == k+1) {
                    // console.log('terakhir '+colspanfooter)
                    tfootcolumns += `<th class="text-right" colspan="`+(colspanfooter)+`" style="border: 1px solid #95c9ed"></th>`
                }
            })
            theadcolumns += `<th style="width: 70px">Action</th>`
            console.log('tfootcolumns')
            console.log(tfootcolumns)
            $('#thead-columns').html(theadcolumns)
            $('#thead-columns-d').html(theadcolumns)
            $('#tfoot-columns').html(tfootcolumns)
            $('#tfoot-columns-d').html(tfootcolumns)
        }

        // console.log(my_table.context[0].jqXHR.responseJSON);
        $('#example_baru_filter input').unbind();

        // console.log(my_table.search($('#example_baru_filter input').val()).draw());
        // my_table.search('tes').draw();
        $('#example_baru_filter input').bind('keyup', function (e) {
            if (e.keyCode == 13) {
                my_table.search($('#example_baru_filter input').val()).draw();
            }
        });

        $('#formdepartement').on('keyup keypress', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });

        // infoDatatable()
        $(".container").css('display', 'block');

    });

    function afterPickFiles(id) {
        var filename = $('#'+id).val().split('\\')[2];
        // console.log('afterpickfiles')
        // console.log($('#'+id).val())
        $("#content_upload_files").hide();
        $("#content_upload_files2").show();
        $("#content_upload_files2").html(`<div class="col-md-12" style="padding:20px;">
            <div class="row">
                <div class="col-md-9">
                    <div class="form-group">
                        <label class="control-label ">Filename</label>
                        <div class="">
                            <input name="" class="falignleft form-text form-control valid" value="`+filename+`" id="filename" type="text" placeholder="Filename">
                        </div>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <label class="control-label ">Public</label>
                        <div class="">
                            <div class="checker" id="uniform-ispublic">
                                <span>
                                    <input name="" class="form-checkbox" id="ispublic" type="checkbox" placeholder="Public">
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <div class="btn btn-sm purple" id="upload" onclick="uploadFile()" style="margin-top:30px;">
                            <i class="fa fa-reply text-light"></i> Upload
                        </div>
                        <div class="btn btn-sm red" id="clear" onclick="clearFile()" style="margin-top:30px;">
                            <i class="fa fa-trash text-light"></i> Cancel
                        </div>
                    </div>
                </div>
            </div>
        </div>`);
    }

    function uploadFile() {
        var formdata = new FormData();
        var files = $('#files')[0].files[0];
        var ispublic = $('#ispublic').prop('checked') ? 1 : 0;
        formdata.append('_token', $('#token').val());
        formdata.append('file', files);
        formdata.append('ispublic', ispublic);

        $.ajax({
            url: "/upload_file_master_table",
            type: "POST",
            method: "POST",
            data: formdata,
            processData: false,  // Important!
            contentType: false,
            cache: false,
            mimeType: 'multipart/form-data',
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            success: function(response){
                var response = JSON.parse(response);

                if(response['success']){ 
                    alert(response['message'])
                    $.notify({
                        message: response['message']
                    },{
                        type: 'success'
                    });
                } else {
                    alert(response['message'])
                    $.notify({
                        message: response['message']
                    },{
                        type: 'danger'
                    });
                }
            }, 
        });
    }

    function chooseFile(noid, _noid, fieldsource, urlfile, title) {
        $('#fm-btn-action-'+noid).html(`<div class="fm-btn-action">
            <div class="btn btn-sm green btn_hide" id="iddmslogo" onclick="showModalDocumentViewer(`+noid+`)">
                <span id="icon">
                    <i class="fa fa-edit text-light"></i>
                </span> 
                <span id="caption">Change</span>
            </div>
            <div class="btn btn-sm blue btn_hide" id="view_img" style="" onclick="showPreviewFile(`+noid+`, '`+urlfile+`', '`+title+`')">
                <i class="fa fa-search text-light"></i>
            </div>
            <div class="btn btn-sm red btn_hide" id="clear_img" style="" onclick="removeFile(`+noid+`, '`+fieldsource+`')">
                <i class="fa fa-trash text-light"></i>
            </div>
        </div>`);
        $('#content_image-'+noid+' .fm-image-content img').attr('src', urlfile);
        $('#title_img-'+noid).text(title);
        $('#modal-document-viewer-'+noid).modal('hide');
        $('#'+fieldsource+'-'+noid).val(_noid);
    }

    function editFile(noid, _noid) {
        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            url: "/edit_file_master_table",
            dataType: "JSON",
            data:{
                "_token":$('#token').val(),
                "table": values3,
                "noid": _noid,
                "filename": $('#attribute_nama-'+noid).val(),
                "filestatus": $('#attribute_status-'+noid).val(),
                "filetag": $('#attribute_tag-'+noid).val(),
            },
            success: function(response) {
                alert(response)
            }
        })
        // alert($('#attribute_tag-'+noid).val())
    }

    function removeFile(noid, fieldsource) {
        $('#fm-btn-action-'+noid).html(`<div class="btn btn-sm green btn_hide" id="iddmslogo" onclick="showModalDocumentViewer(`+noid+`)">
            <span id="icon">
                <i class="fa fa-plus text-light"></i>
            </span> 
            <span id="caption">File Manager</span>
        </div>`);
        $('#content_image-'+noid).html(`<div class="modal-image-content" style="width: 100%; height: inherit; text-align: -webkit-center">
            <div class="fm-image-content" style="vertical-align: middle; display: table-cell; width: 100%; height: inherit; text-align: -webkit-center">
                <img src="http://dashboard.dev.lambada.id/data_lycon/filemanager/noimage.png" style="max-width:100%;max-height:200px;">
            </div>
        </div>`);
        $('#title_img-'+noid).text('File Manager');
        $('#'+fieldsource+'-'+noid).val('');
    }

    function selectContentItem(noid, _noid, fieldsource, urlfile, nama, title, size, type, modified, created, ispublic, classicon, classcolor, filetag) {
        $(".select2tag").select2({
            tags: true,
            tokenSeparators: [',', ' ']
        })
        $('#fm_meta_filename-'+noid+' strong').text(title);
        $('#fm_meta_image-'+noid+' img').attr('src', urlfile);
        $('#fm_meta_size-'+noid).text(size != 'null' ? size+' KB' : '-');
        $('#fm_meta_type-'+noid).text(type != 'null' ? type : '-');
        $('#fm_meta_modified-'+noid).text(modified != 'null' ? modified : '-');
        $('#fm_meta_created-'+noid).text(created != 'null' ? created : '-');
        $('.fm_meta_status').hide();
        if (ispublic == '1') {
            $('#fm_meta_status_public-'+noid).show();
            $('#fm_meta_attributes_ispublic-'+noid).text('PUBLIC');
            $('#fm_meta_attributes_ispublic-'+noid).attr('class', 'btn btn-xs green tooltips');
            $('#attribute_status-'+noid).val(1);
        } else {
            $('#fm_meta_status_private-'+noid).show();
            $('#fm_meta_attributes_ispublic-'+noid).text('PRIVATE');
            $('#fm_meta_attributes_ispublic-'+noid).attr('class', 'btn btn-xs yellow tooltips');
            $('#attribute_status-'+noid).val(0);
        }

        $('#fm_meta_details_download-'+noid).attr('href', urlfile);
        $('#fm_meta_details_preview-'+noid).attr('onclick', `showPreviewFile(`+noid+`, '`+urlfile+`', '`+title+`')`);
        $('#fm_meta_details_choose-'+noid).attr('onclick', `chooseFile(`+noid+`, `+_noid+`, '`+fieldsource+`', '`+urlfile+`', '`+title+`')`);

        $('#fm_meta_attributes_fileextention-'+noid).html(`<i class="`+classicon+`"></i> `+type);
        $('#fm_meta_attributes_fileextention-'+noid).attr('class', 'btn btn-xs tooltips '+classcolor);
        $('#fm_meta_attributes_size-'+noid).text(size != 'null' ? size+' KB' : '-');
        $('#fm_meta_attributes_created-'+noid).text(created != 'null' ? created : '-');
        $('#fm_meta_attributes_updated-'+noid).text(modified != 'null' ? modified : '-');

        $('#editfile-'+noid).attr('onclick', `editFile(`+noid+`, `+_noid+`)`);
        $('#attribute_nama-'+noid).val(nama);
        
        if (filetag.split(',').length > 0) {
            $('#attribute_tag-'+noid).val(filetag.split(','));
        }

        $('.fm_contentitems').attr('style', 'height: 139px; overflow: hidden;  border: 1px solid grey');
        $('#fm_contentitems-'+noid+'-'+_noid).css('box-shadow', '0px 0px 5px 2px #9a12b3');
        $('#fm_meta-'+noid).show();
    }

    function showPreviewFile(noid, urlfile, title) {
        $('#modal-preview-file-'+noid+' div div div div .fm-image-content img').attr('src', urlfile);
        $('#modal-preview-file-'+noid+' div div div .modal-title').text(title);
        $('#modal-preview-file-'+noid).modal('show');
    }
    
    function hidePreviewFile() {
        $('#modal-preview-file').modal('hide');
    }

    function mouseoverGridItem(id1, id2) {
        $(id1).css('height', '95%');
        $(id2).css('left', '0%');
    }

    function mouseoutGridItem(id1, id2) {
        $(id1).css('height', '23px');
        $(id2).css('left', '-200%');
    }

    function clearFile() {
        $("#content_upload_files").show();
        $("#content_upload_files2").hide();
    }

    function toggleFmBtn(noid, fieldsource, type, where=null) {
        getFile(noid, fieldsource, where);
        $('.fm_panelcontent').hide();
        $('#fm_meta-'+noid).hide();

        var id = '';
        var id2 = '';
        if (type == 'upload') {
            id = '#content_upload-'+noid;
            id2 = '#upload-'+noid+' span';
        } else if (type == 'myfiles') {
            id = '#content_myfile-'+noid;
            id2 = '#myfiles-'+noid+' span';
        } else if (type == 'library') {
            id = '#content_library-'+noid;
            id2 = '#library-'+noid+' span';
        }

        $('.fm_btnlibrary a span').hide()
        $(id2).css('display', 'inline-block');
        $(id).show();
    }

    function getFile(noid, fieldsource, where) {
        var domain = 'http://'+location.host;

        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            url: "/get_file_master_table",
            dataType: "JSON",
            data:{
                "_token":$('#token').val(),
                "table": values3,
                "where": where
            },
            success: function(response) {
                console.log('asek');
                console.log(response);
                var html = '';
                for (var i = 0; i<response.length; i++) {
                    var thumbnail = response[i]['fileextention'] == 'PNG' || response[i]['fileextention'] == 'JPG' || response[i]['fileextention'] == 'JPEG' || response[i]['fileextention'] == 'GIF' ? domain+`/storage/filemanager/thumb/`+response[i]['filename'] : domain+`/filemanager/default/`+response[i]['fileextention'].toLowerCase()+'.png';
                    html += `<div class="col-md-2 col-xs-4 fm_content_grid_items" id="fm_content_grid_item" style="margin-bottom: 4px; padding: 4px; text-align: -webkit-center; height: 100%" onmouseover="mouseoverGridItem('#content_myfile-`+noid+` .row .fm_content .row #panelcontent .row .fm_content_grid_items #fm_contenttitle-`+response[i]['noid']+`', '#content_myfile-`+noid+` .row .fm_content .row #panelcontent .row .fm_content_grid_items #fm_contenttitle_action-`+response[i]['noid']+`')" onmouseout="mouseoutGridItem('#content_myfile-`+noid+` .row .fm_content .row #panelcontent .row .fm_content_grid_items #fm_contenttitle-`+response[i]['noid']+`', '#content_myfile-`+noid+` .row .fm_content .row #panelcontent .row .fm_content_grid_items #fm_contenttitle_action-`+response[i]['noid']+`')">
                                <div class="fm_contentitems" id="fm_contentitems-`+noid+`-`+response[i]['noid']+`" style="height: 139px; overflow: hidden;  border: 1px solid grey" onclick="selectContentItem(`+noid+`, `+response[i]['noid']+`, '`+fieldsource+`', '`+domain+`/storage/filemanager/thumb/`+response[i]['filename']+`', '`+response[i]['nama']+`', '`+response[i]['filename']+`', '`+response[i]['filesize']+`', '`+response[i]['fileextention']+`', '`+response[i]['lastupdate']+`', '`+response[i]['docreate']+`', '`+response[i]['ispublic']+`', '`+response[i]['classicon']+`', '`+response[i]['classcolor']+`', '`+response[i]['filetag']+`')">
                                    <span class="badge badge-null badge-roundless" style="display: none;">UNSPECIFIED</span>
                                    <img src="`+thumbnail+`" class="image_ori">
                                </div>
                                <div class="fm_contenttitle" id="fm_contenttitle-`+response[i]['noid']+`" onclick="selectContentItem(`+noid+`, `+response[i]['noid']+`, '`+fieldsource+`', '`+domain+`/storage/filemanager/thumb/`+response[i]['filename']+`', '`+response[i]['nama']+`', '`+response[i]['filename']+`', '`+response[i]['filesize']+`', '`+response[i]['fileextention']+`', '`+response[i]['lastupdate']+`', '`+response[i]['docreate']+`', '`+response[i]['ispublic']+`', '`+response[i]['classicon']+`', '`+response[i]['classcolor']+`', '`+response[i]['filetag']+`')" style="text-align: center; height: 23px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis; color: #fff; bottom: 4px; position: absolute; width: calc(100% - 8px); padding: 4px; background: rgba(0, 0, 0, .7); display: block; font-size: 80%; -webkit-transition: all 0.5s ease;">
                                    `+response[i]['nama']+`
                                    <div class="fm_contenttitle_action-`+response[i]['noid']+`" id="fm_contenttitle_action-`+response[i]['noid']+`" style="position: absolute; bottom: 4px; left: -200%; right: 0; transition: all 0.5s ease 0s;">
                                        <a type="button" class="btn btn-sm purple tooltips" data-container="body" data-placement="top" data-original-title="Preview" id="preview" onclick="showPreviewFile(`+noid+`, '`+domain+`/storage/filemanager/thumb/`+response[i]['filename']+`', '`+response[i]['filename']+`')">
                                            <i class="fa fa-desktop text-light"></i>
                                        </a>
                                        <span class="btn btn-sm yellow tooltips" data-container="body" data-placement="top" data-original-title="Attach" id="setdocument" onclick="chooseFile(`+noid+`, `+response[i]['noid']+`, '`+fieldsource+`', '`+domain+`/storage/filemanager/thumb/`+response[i]['filename']+`', '`+response[i]['filename']+`')">
                                            <i class="fa fa-upload text-light"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>`
                }
                $('#content_myfile-'+noid+' .row .fm_content .row #panelcontent .row').html(html)
                // alert(response)
                console.log('response')
                console.log(response)
            }
        })
    }

    function getCmsFilterButton() {
        var buttonfilter = new Array();
        
        if (maincms['widget']['configwidget'] != null) {
            $.each(JSON.parse(maincms['widget']['configwidget'])['WidgetFilter'], function(k, v) {
                // buttonfilter[k] = new Array();
                buttonfilter[k] = {
                    'groupcaption' : v.groupcaption,
                    'groupcaptionshow' : v.groupcaptionshow,
                    'groupdropdown' : v.groupdropdown,
                    'data' : new Array()
                }
                
                $.each(v.button, function(k2, v2) {
                    var icon = '';
                    if (v2.classicon != '') {
                        icon = '<i class="fa '+v2.classicon+'"></i> ';
                    }
                    var pecah = v2.condition[0].querywhere[0].fieldname.split('.');
                    var buttoncaption = v2.buttoncaption.split(' ');
                    var uniqeidbutton = '';
                    if (buttoncaption.length > 1) {
                        $.each(buttoncaption, function(k3, v3) {
                            uniqeidbutton += v3
                        })
                    } else {
                        uniqeidbutton = v2.buttoncaption
                    }
                    var oncl = 'onclick="filterData(\'btn-filter-'+uniqeidbutton+'\', \'btn-filter-'+k+'\', '+k+', \''+v2.buttoncaption+'\', \''+v2.classcolor+'\', '+v.groupdropdown+', false, \''+maincms['widget']['maintable']+'\', \''+v2.condition[0].querywhere[0].operand+'\', \''+pecah[1]+'\', \''+v2.condition[0].querywhere[0].operator+'\', \''+v2.condition[0].querywhere[0].value+'\')" ';
                    var button = '<button class="btn btn-outline-secondary btn-sm flat btn-filter-'+k+'" id="btn-filter-'+uniqeidbutton+'" style="background-color:#E3F2FD; color: '+v2.classcolor+'"'+oncl+'>'+icon+v2.buttoncaption+' </button>';
                    if (v.groupdropdown == 1) {
                        button = '<button class="flat dropdown-item" id="btn-filter-'+uniqeidbutton+'" '+oncl+'>'+icon+v2.buttoncaption+' </button>';
                    }
                    buttonfilter[k].data[k2] = {
                        'nama' : v2.buttoncaption,
                        'buttonactive' : v2.buttonactive,
                        'taga' : button,
                        'operand' : v2.condition[0].querywhere[0].operand,
                        'fieldname' : pecah[1],
                        'operator' : v2.condition[0].querywhere[0].operator,
                        'value' : v2.condition[0].querywhere[0].value,
                    };
                })
            })
            
            var json_data = {
                "filterButton" : buttonfilter,
                "configWidget" : JSON.parse(maincms['widget']['configwidget']),
            };
            
            // console.log('test')
            $.each(json_data.filterButton, function(k, v) {
                $.each(v.data, function(k2, v2) {
                    if (v2 != undefined) {
                        // console.log(v2.buttonactive)  
                    }
                })
            })

            cmsfilterbutton = json_data;
        }
        // return json_data;
    }

    function getFilterButton(values3) {
        // $.ajax({
        //     type: "POST",
        //     header:{
        //         'X-CSRF-TOKEN':$('#token').val()
        //     },
        //     url: "/get_filter_button_master_table",
        //     dataType: "json",
        //     data:{
        //         "_token":$('#token').val(),
        //         table: maincms['menu']['noid'],
        //         maincms: JSON.stringify(maincms)
        //     },
        //     success: function(response) {
                getCmsFilterButton()
                var response = cmsfilterbutton;
                // if (response == response2) {
                //     console.log('COCOKKKKK')
                // }
                // console.log(response)
                // console.log(response2)
                // return false

                var buttonactive = new Array();
                var filter_button = '';
                var groupdropdown = new Array();
                $.each(response.filterButton, function(k, v) {
                    // console.log(v.groupdropdown)
                    buttonactive.push('all-'+k);
                    if (v.groupdropdown == 1) {
                        filter_button += `<button class="btn btn-outline-secondary btn-sm flat btn-filter-all btn-filter-`+k+`" id="btn-filter-all-`+k+`" style="background-color:#90CAF9; font-weight: bold; color: black;">`+v.groupcaption+`</button>`;
                        filter_button += `<div class="dropdown btn-group" style="padding-right: 5px">`
                    } else {
                        filter_button += `<button class="btn btn-outline-secondary btn-sm flat btn-filter-all btn-filter-`+k+`" id="btn-filter-all-`+k+`" style="background-color:#E3F2FD;" onclick="filterData('btn-filter-all-`+k+`', 'btn-filter-`+k+`', `+k+`, 'ALL', 'grey', 0, true)">ALL</button>`;
                    }
                    selected_filter.operand.push('')
                    selected_filter.fieldname.push('')
                    selected_filter.operator.push('')
                    selected_filter.value.push('')

                    $.each(v.data, function(k2, v2) {
                        if (v2 != undefined) {
                            if (v.groupdropdown == 1) {
                                if (k2 == 0) {
                                    if (v2.buttonactive) {
                                        buttonactive[k] = v2.nama;
                                        selected_filter.operand[k] = v2.operand;
                                        selected_filter.fieldname[k] = v2.fieldname;
                                        selected_filter.operator[k] = v2.operator;
                                        selected_filter.value[k] = v2.value;
                                        filter_button += `<button class="btn btn-secondary btn-sm flat dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <span id="filter-dropdown-active-`+k+`">`+v2.nama+`</span>
                                                        </button>`;
                                    } else {
                                        filter_button += `<button class="btn btn-secondary btn-sm flat dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <span id="filter-dropdown-active-`+k+`">ALL</span>
                                                        </button>`;
                                    }
                                    filter_button += `<div class="dropdown-menu flat" aria-labelledby="dropdownMenuButton">
                                                        <button class="dropdown-item btn-filter-all btn-filter-`+k+`" id="btn-filter-all-`+k+`"  onclick="filterData('btn-filter-all-`+k+`', 'btn-filter-`+k+`', `+k+`, 'ALL', 'grey', 1, true)">ALL</button>`;
                                }
                                groupdropdown.push(true);
                            } else {
                                if (v2.buttonactive) {
                                    buttonactive[k] = v2.nama;
                                    selected_filter.operand[k] = v2.operand;
                                    selected_filter.fieldname[k] = v2.fieldname;
                                    selected_filter.operator[k] = v2.operator;
                                    selected_filter.value[k] = v2.value;
                                }
                                groupdropdown.push(false);
                            }
                            filter_button += v2.taga;
                        }
                    })
                    
                    if (v.groupdropdown == 1) {
                        filter_button += `</div>
                                        </div>`;
                    }
                    filter_button += `<div class="btn" style="padding: 0px; width: 5px"></div>`
                })
                
                if (filter_button.length > 0) {
                    $('#space_filter_button').html(filter_button)
                    $('#space_filter_button').css('margin-bottom', '10px')
                }
                
                // console.log(buttonactive)
                $.each(buttonactive, function(k, v) {
                    if (groupdropdown[k] == false) {
                        $('#btn-filter-'+buttonactive[k]).css('background', '#90CAF9');
                        $('#btn-filter-'+buttonactive[k]).css('color', 'black');
                        $('#btn-filter-'+buttonactive[k]).css('font-weight', 'bold');
                    }
                    // console.log(groupdropdown[k])
                    // console.log(buttonactive[k])
                })

                if (buttonactive.length > 0) {
                    destroyDatatable()
                    getDatatable()
                } else {
                    getDatatable()
                }
                
                
                //Datatable filter
                $.each(response.configWidget.WidgetFilter, function(k, v) {
                    var filtercontentgroup = '';
                    var filtercontentbody = '';

                    var classcolor = 'default';
                    var textcolor = 'text-dark';
                    var display = 'none';
                    if (k == 0) {
                        classcolor = 'purple';
                        textcolor = 'text-white'
                        display = 'block';
                        filter_tabs.push(true)
                    } else {
                        filter_tabs.push(false)
                    }
                    filtercontentgroup += `<input type="hidden" name="tabs[]" value="`+k+`">
                                            <div style="margin:5px 5px 0 0;" id="filter-content-group-`+k+`" onclick="changeTabFilter(`+k+`)" class="filter-content-group formbtn btn default `+classcolor+`">
                                                <span class="`+textcolor+`">
                                                    <i class="fa fa-indent `+textcolor+`"></i> `+k+`
                                                </span>
                                            </div>`;
                    filtercontenttabs.filtercontentgroup.push(filtercontentgroup)

                    var groupcaptionshow = `<option value="1">True</option>
                                            <option value="0" selected>False</option>`;
                    if (v.groupcaptionshow) {
                        groupcaptionshow = `<option value="1" selected>True</option>
                                            <option value="0">False</option>`
                    }

                    var groupdropdown = '';
                    if (v.groupdropdown == -1) {
                        groupdropdown = `<option value="-1" selected>Auto</option>
                                            <option value="0">False</option>
                                            <option value="1">True</option>`
                    } else if (v.groupdropdown == 0) {
                        groupdropdown = `<option value="-1">Auto</option>
                                            <option value="0" selected>False</option>
                                            <option value="1">True</option>`
                    } else if (v.groupdropdown == 1) {
                        groupdropdown = `<option value="-1">Auto</option>
                                            <option value="0">False</option>
                                            <option value="1" selected>True</option>`
                    }
                    filtercontentbody += `<div style="display: `+display+`" id="filter-content-body-`+k+`" class="filter-content-body formgroup col-md-12">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Group Caption</label>
                                                        <input name="groupcaption[]" id="groupcaption-`+k+`" class="form-control input-sm" value="`+v.groupcaption+`" placeholder="Group Caption">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Group Caption Showed</label>
                                                        <select name="groupcaptionshow[]" id="groupcaptionshow-`+k+`" class="form-control input-sm">
                                                            `+groupcaptionshow+`
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Group Dropdown Showed</label>
                                                        <select name="groupdropdown[]" id="groupdropdown-`+k+`" class="form-control input-sm">
                                                            `+groupdropdown+`
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="filter-content-row-`+k+`">
                                            </div>
                                            </div>`
                    filtercontenttabs.filtercontentbody.push(filtercontentbody)

                    filtercontenttabs.filtercontentrow[k] = new Array()
                    var _k2 = 0;
                    $.each(v.button, function(k2, v2) {
                        var filtercontentrow = '';

                        var condition = '';
                        $.each(v2.condition, function(k3, v3) {
                            condition += v3.groupoperand+'#'
                            $.each(v3.querywhere, function(k4, v4) {
                                condition += v4.fieldname+';'
                                condition += v4.operator+';'
                                condition += v4.value+';'
                                condition += v4.operand
                            })
                        })

                        var buttonactive = `<option value="1">True</option>
                                            <option value="0" selected>False</option>`
                        if (v2.buttonactive) {
                            buttonactive += `<option value="1" selected>True</option>
                                                <option value="0">False</option>`
                        }

                        filtercontentrow += `<input type="hidden" name="tabsrow[]" value="`+k+`">
                                            <div class="row filter-content-row-`+k+`">
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Caption</label>
                                                        <input class="form-control input-sm" name="buttoncaption[]" id="buttoncaption-`+k+`-`+_k2+`" value="`+v2.buttoncaption+`" placeholder="Caption">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Condition</label>
                                                        <input class="form-control input-sm" name="condition[]" id="condition-`+k+`-`+_k2+`" value="`+condition+`" placeholder="groupoperand#fieldname;operator;value;operand">
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Active</label>
                                                        <select class="form-control input-sm" name="buttonactive[]" id="buttonactive-`+k+`-`+_k2+`" value="1">
                                                            `+buttonactive+`
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Icon</label>
                                                        <input class="form-control input-sm" name="classicon[]" id="classicon-`+k+`-`+_k2+`" value="`+v2.classicon+`" placeholder="Icon">
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Color</label>
                                                        <input class="form-control input-sm" name="classcolor[]" id="classcolor-`+k+`-`+_k2+`" value="`+v2.classcolor+`" placeholder="Color">
                                                    </div>
                                                </div>
                                                <div class="col-md-1" style="text-align:center;padding:21px 0 0 0">
                                                    <div class="form-group">
                                                        <button id="del" onclick="delRow(`+_k2+`)" type="button" class="btn btn-sm red" style="height: 27px; width: 38px">
                                                            <i class="fa fa-minus-circle text-white"></i>
                                                        </button>
                                                        <button id="add" onclick="addRow()" type="button" class="btn btn-sm green" style="height: 27px; width: 38px">
                                                            <i class="fa fa-plus text-white"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>`
                        filtercontenttabs.filtercontentrow[k].push(filtercontentrow)
                        _k2++;
                    })
                    // filtercontentbody += `</div>
                    //                     </div>`
                })

                //Set to HTML
                var html_filtercontentgroup = '';
                var html_filtercontentbody = '';
                $.each(filtercontenttabs.filtercontentgroup, function(k, v) {
                    html_filtercontentgroup += v
                    html_filtercontentbody += filtercontenttabs.filtercontentbody[k]
                })
                $('#filtercontentgroup').html(html_filtercontentgroup);
                $('#filtercontentbody').html(html_filtercontentbody);

                $.each(filtercontenttabs.filtercontentrow, function(k, v) {
                    $('#filter-content-row-'+k).html(v);
                })
                //End Set to HTML

                // console.log(html_filtercontentrow)
                // console.log(filtercontenttabs)
            // },
        // });
    }

    function getForm(isadd, widget_noid, element_id) {
        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            url: "/get_form_master_table",
            dataType: "json",
            data:{
                "_token":$('#token').val(),
                table: maincms['menu']['noid'],
                maincms: JSON.stringify(maincms),
                isadd: isadd,
                key: 0,
                widget_noid: widget_noid,
            },
            beforeSend: function () {
                $('#'+element_id).LoadingOverlay("show");
            },
            success: function (response) {
                // console.log(response);
                var divform = '';
                divform += '<input type="hidden" id="noid" name="noid" value="">';
                // var colgroupname = [
                //     0: 'Administrasi'
                // ];
                // var colgroupname_data = [
                //     0: [
                //         //,
                //         //,
                //         //
                //     ]
                // ];
                // var colgroupname = new Array();
                // var colgroupname_data = new Array();
                $.each(response.field, function(key, value) {
                    // roles.fieldname.push(value.fieldname);
                    // roles.newhidden.push(value.newhidden);
                    // roles.edithidden.push(value.edithidden);

                    // if (value.colgroupname) {
                    //     if (colgroupname.indexOf(value.colgroupname) === -1) {
                    //         colgroupname.push(value.colgroupname);
                    //     }
                    //     colgroupname_data.push(value.field);
                    // } else {
                    //     divform += value.field;
                    // }

                    // if (value.colsorted == 0) {
                    //     colsorted.push(key+1);
                    // }
                    if (value.fieldname != 'idmaster') {
                        divform += value.field;
                    }
                });
                // colsorted[colsorted.length] = roles.fieldname.length+2;
                // colsorted.push(roles.fieldname.length+2);
                
                var divgroup = '';
                $.each(response.formgroupname, function(k, v) {
                    var formgroupname = k.split('-')

                    divgroup += `<div class="col-md-12">
                                    <div class="sub-card-custom">
                                        <div class="card-header card-header_ bg-main-1 d-flex">
                                            <div class="text-white" style="font-size: 14px">`+formgroupname[1].split(';')[1]+`</div>
                                        </div>
                                        <div class="card-body card-body-custom card-body-custom-group flat">
                                            <div class="right-tabs-group">`;
                                           
                    var key2 = 0;
                    var tab_widget = `<ul class="nav nav-tabs" id="tab-widget-`+k+`" role="tablist">`;
                    var tab_widget_content = `<div class="tab-content" id="tab-widget-content-`+k+`">`;
                    $.each(v, function(k2, v2) {
                        var active = '';
                        if (key2 == 0) {
                            active = 'active';
                        }

                        tab_widget += `<li class="nav-item">
                                        <a class="nav-link nav-link-custom flat `+active+`" id="tab-`+k2+`" data-toggle="tab" href="#`+k2+`" role="tab" aria-controls="`+k2+`"
                                        aria-selected="true" onclick="setTabActive(`+k2+`, '#tab-widget-`+k+` li a', '#tab-widget-`+k+` li #tab-`+k2+`', '#tab-widget-content-`+k+` .tab-widget-content-`+k+`', '#tab-widget-content-`+k+` #`+k2+`')">`+k2+`</a>
                                    </li>`;

                        tab_widget_content += `<div class="tab-pane tab-widget-content-`+k+` fade show `+active+`" id="`+k2+`" role="tabpanel" aria-labelledby="`+k2+`-tab">
                                                    <div class="row">`;
                        $.each(v2, function(k3, v3) {
                            tab_widget_content += v3;
                        })
                        tab_widget_content += `     </div>
                                                </div>`;
                        key2++;
                    });
                    tab_widget += `</ul>`;
                    tab_widget_content += `</div>`;
                    divgroup += tab_widget;
                    divgroup += tab_widget_content;

                    divgroup +=             `</div>
                                        </div>
                                    </div>
                                </div>`;
                });
                divform += divgroup;
                // console.log('colgroupname');
                // console.log(colgroupname);

                $('#divform').html(divform);
                
                if (statusadd) {
                    $('#'+element_id).LoadingOverlay("hide");
                } else {
                    if (element_id == 'divform') {
                        // console.log(widget_noid)
                        // console.log(divform)
                        
                        $.ajax({
                            url: '/find_master_table',
                            type: "POST",
                            dataType: 'json',
                            header:{
                                'X-CSRF-TOKEN':$('#token').val()
                            },
                            data: {
                                "id" : nownoid,
                                "namatable" : $("#namatable").val(),
                                "_token": $('#token').val(),
                                maincms: JSON.stringify(maincms)
                            },
                            success: function (response) {
                                nownoid = response.nownoid;

                                var maincms_panel = new Array();
                                $.each(maincms['panel'], function(k, v) {
                                    $.each(maincms['widget'], function(k2, v2) {
                                        // if (v.idwidget == v2.noid) {
                                        //     maincms_panel[v.noid] = v2;
                                        // }
                                    })
                                })

                                // console.log(response.result_detail)
                                var key = 0;
                                $.each(response.result_detail, function(k, v) {
                                    if (key == 0) {
                                        nownoidtab = maincms_panel[k].noid
                                    }
                                    // alert(k)
                                    // console.log(v)
                                    // localStorage.removeItem('input-data-'+k)
                                    localStorage.setItem('input-data-'+k, JSON.stringify(v))
                                    localStorage.setItem('tablename-'+k, maincms_panel[k].maintable)
                                    // console.log(v)
                                    key++;
                                })
                                // $('#noid').val(id)
                                $.each(response, function(k, v) {
                                    if (v.coltypefield == 11) {
                                        $("#"+v.field+' input').val(v.value);
                                    } else {
                                        $("#"+v.field).val(v.value);
                                    }

                                    if (v.disabled) {
                                        $("#"+v.field).attr('disabled', 'disabled');
                                    }
                                    // if (statusdetail) {
                                    //     $("#"+v.field).attr('disabled', 'disabled');
                                    // }
                                    if (v.edithidden) {
                                        $("#div-"+v.field).css('display', 'none');
                                    }
                                });
                                $('#'+element_id).LoadingOverlay("hide");
                                console.log('element_id')
                                console.log(element_id)
                            }
                        });
                    }
                }

                $.each(response.field, function(k, v) {
                    if (v.newhidden == 1) {
                        $('#div-'+v.fieldname).css('display', 'none');
                    }
                })               
                // console.log(roles)  

                //SELECT2
                $('.select2').select2();
                $('.select2').css('width', '100%');
                $('.select2 .selection .select2-selection').css('height', '35px');
                $('.select2 .selection .select2-selection .select2-selection__rendered').css('margin-top', '-6px');
                $('.select2 .selection .select2-selection .select2-selection__arrow').css('margin-top', '4px');

                //SELECT2TAG
                $(".select2tag").select2({
                    tags: true,
                    tokenSeparators: [',', ' ']
                })
                $('.select2tag').css('width', '100%');
                $('.select2tag .selection .select2tag-selection').css('height', '35px');
                $('.select2tag .selection .select2tag-selection .select2tag-selection__rendered').css('margin-top', '-6px');
                $('.select2tag .selection .select2tag-selection .select2tag-selection__arrow').css('margin-top', '4px');
            },
        });
    }
    
    function removeSelected(selecteds) {
        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            url: "/delete_selected_master_table",
            dataType: "json",
            data:{
                "_token": $('#token').val(),
                "namatable" :  $("#namatable").val(),
                "selecteds": selecteds
            },
            success: function(response) {
                $.notify({
                    message: 'Delete data has been succesfullly.' 
                },{
                    type: 'success'
                });
                refresh()
                // var filter_button = '';
                // for (var i=0; i<response.filterButton[0].length; i++) {
                //     filter_button += response.filterButton[0][i].taga;
                // }
                // $('#space_filter_button').html(filter_button)
                // console.log(filter_button);
            },
        });
    }

    function showBtnFileManager(noid) {
        $('#fm-btn-action-'+noid).animate({top: '0px'});
    }
    
    function hideBtnFileManager(noid) {
        // $('#fm-btn-action-'+noid).animate({top: '-40px'});
    }

    function showModalDocumentViewer(noid) {
        $('#modal-document-viewer-'+noid).modal('show');
    }

    function getCmsThead() {
        var thead = `<th style="text-align:center" colspan="2">#</th>`;
        // $.each(maincms['widgetgridfield'], function(k, v) {
            thead += `<th style="text-align:center" colspan="`+maincms['widgetgridfield'].length+`"></th>`;
        // })
        thead += `<th colspan="1" style="text-align:center">---</th>`;
        $('#thead').html(thead);
    }

    function saveFilter() {
        // $('#modal-notif').css('display', 'none')
        // var config_widget = {
        //     WidgetFilter: '',
        //     WidgetConfig: '',
        // }
        var formdata = $('#formfilter').serialize();

        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            url: "/save_filter_master_table",
            dataType: "json",
            data:{
                "_token":$('#token').val(),
                "data" :{
                    'data': formdata,
                    'url_code': $('#url_code').val()
                }
            },
            success:function(response){
                // console.log(response)
                if (response.result) {
                    // alert('Success')
                    closeModalFilter()
                    $.notify({
                        message: 'Save data has been succesfullly.' 
                    },{
                        type: 'success'
                    });
                    setInterval(function(){
                        location.reload()
                    }, 3000)
                    // location.reload()
                } else {
                    // $('#modal-notif').css('display', 'block')
                    // setInterval(function(){
                    //     $('#modal-notif').css('display', 'none')
                    // }, 3000)
                    
                    $.notify({
                        message: 'Save data has been error.' 
                    },{
                        type: 'danger'
                    });
                }
            }
        });
    }

    function delRow(key) {
        filtercontenttabs.filtercontentrow[selected_filter_tabs][key] = '';
        getFilterContentTabs()
    }

    function addRow() {
        filtercontenttabs.filtercontentrow[selected_filter_tabs].push(`<input type="hidden" name="tabsrow[]" value="`+selected_filter_tabs+`">
                                                                        <div class="row filter-content-row-`+selected_filter_tabs+`">
                                                                            <div class="col-md-2">
                                                                                <div class="form-group">
                                                                                    <label class="col-md-12 control-label">Caption</label>
                                                                                    <input class="form-control input-sm" name="buttoncaption[]" id="buttoncaption-`+selected_filter_tabs+`-`+filtercontenttabs.filtercontentrow[selected_filter_tabs].length+`" value="" placeholder="Caption">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label class="col-md-12 control-label">Condition</label>
                                                                                    <input class="form-control input-sm" name="condition[]" id="condition-`+selected_filter_tabs+`-`+filtercontenttabs.filtercontentrow[selected_filter_tabs].length+`" value="" placeholder="groupoperand#fieldname;operator;value;operand">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1">
                                                                                <div class="form-group">
                                                                                    <label class="col-md-12 control-label">Active</label>
                                                                                    <select class="form-control input-sm" name="buttonactive[]" id="buttonactive-`+selected_filter_tabs+`-`+filtercontenttabs.filtercontentrow[selected_filter_tabs].length+`" value="1">
                                                                                        <option value="1">True</option>
                                                                                        <option value="0" selected>False</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1">
                                                                                <div class="form-group">
                                                                                    <label class="col-md-12 control-label">Icon</label>
                                                                                    <input class="form-control input-sm" name="classicon[]" id="classicon-`+selected_filter_tabs+`-`+filtercontenttabs.filtercontentrow[selected_filter_tabs].length+`" value="" placeholder="Icon">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1">
                                                                                <div class="form-group">
                                                                                    <label class="col-md-12 control-label">Color</label>
                                                                                    <input class="form-control input-sm" name="classcolor[]" id="classcolor-`+selected_filter_tabs+`-`+filtercontenttabs.filtercontentrow[selected_filter_tabs].length+`" value="" placeholder="Color">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1" style="text-align:center;padding:21px 0 0 0">
                                                                                <div class="form-group">
                                                                                    <button id="del" onclick="delRow(`+filtercontenttabs.filtercontentrow[selected_filter_tabs].length+`)" type="button" class="btn btn-sm red" style="height: 27px; width: 38px">
                                                                                        <i class="fa fa-minus-circle text-white"></i>
                                                                                    </button>
                                                                                    <button id="add" onclick="addRow()" type="button" class="btn btn-sm green" style="height: 27px; width: 38px">
                                                                                        <i class="fa fa-plus text-white"></i>
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </div>`)
        getFilterContentTabs()
        // console.log(filtercontenttabs)
    }

    $('#delgroup').on('click', function() {
        filter_tabs.pop()
        filtercontenttabs.filtercontentgroup.pop()
        filtercontenttabs.filtercontentbody.pop()
        filtercontenttabs.filtercontentrow.pop()
        getFilterContentTabs()
    })

    $('#addgroup').on('click', function() {
        filter_tabs.push(false)

        filtercontenttabs.filtercontentgroup.push(`<input type="hidden" name="tabs[]" value="`+filtercontenttabs.filtercontentgroup.length+`">
                                                    <div style="margin:5px 5px 0 0;" id="filter-content-group-`+filtercontenttabs.filtercontentgroup.length+`" onclick="changeTabFilter(`+filtercontenttabs.filtercontentgroup.length+`)" class="filter-content-group formbtn btn default default">
                                                        <span class="text-dark">
                                                            <i class="fa fa-indent text-dark"></i> `+filtercontenttabs.filtercontentgroup.length+`
                                                        </span>
                                                    </div>`)

        filtercontenttabs.filtercontentbody.push(`<div style="display: none" id="filter-content-body-`+filtercontenttabs.filtercontentbody.length+`" class="filter-content-body formgroup col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="col-md-12 control-label">Group Caption</label>
                                                                <input name="groupcaption[]" id="groupcaption-`+filtercontenttabs.filtercontentbody.length+`" class="form-control input-sm" value="" placeholder="Group Caption">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="col-md-12 control-label">Group Caption Showed</label>
                                                                <select name="groupcaptionshow[]" id="groupcaptionshow-`+filtercontenttabs.filtercontentbody.length+`" class="form-control input-sm">
                                                                    <option value="1">True</option>
                                                                    <option value="0" selected>False</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="col-md-12 control-label">Group Dropdown Showed</label>
                                                                <select name="groupdropdown[]" id="groupdropdown-`+filtercontenttabs.filtercontentbody.length+`" class="form-control input-sm">
                                                                    <option value="-1" selected>Auto</option>
                                                                    <option value="0" selected>False</option>
                                                                    <option value="1">True</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="filter-content-row-`+filtercontenttabs.filtercontentbody.length+`">
                                                    </div>
                                                </div>`)
        filtercontenttabs.filtercontentrow.push(new Array())
        getFilterContentTabs()
    })

    function getFilterContentTabs() {
        //Set to HTML
        var html_filtercontentgroup = '';
        var html_filtercontentbody = '';
        var value = {
            groupcaption: new Array(),
            groupcaptionshow: new Array(),
            groupdropdown: new Array(),
            buttoncaption: new Array(),
            condition: new Array(),
            buttonactive: new Array(),
            classicon: new Array(),
        };

        //SET
        $.each(filtercontenttabs.filtercontentgroup, function(k, v) {
            value.buttoncaption[k] = new Array();
            value.condition[k] = new Array();
            value.buttonactive[k] = new Array();
            value.classicon[k] = new Array();

            if ($('#groupcaption-'+k).val() == undefined) { value.groupcaption.push('') } else { value.groupcaption.push($('#groupcaption-'+k).val()) }
            if ($('#groupcaptionshow-'+k).val() == undefined) { value.groupcaptionshow.push(0) } else { value.groupcaptionshow.push($('#groupcaptionshow-'+k).val()) }
            if ($('#groupdropdown-'+k).val() == undefined) { value.groupdropdown.push(-1) } else { value.groupdropdown.push($('#groupdropdown-'+k).val()) }
            
            $.each(filtercontenttabs.filtercontentrow[k], function(k2, v2) {
                if ($('#buttoncaption-'+k+'-'+k2).val() == undefined) { value.buttoncaption[k][k2] = '' } else { value.buttoncaption[k][k2] = $('#buttoncaption-'+k+'-'+k2).val() }
                if ($('#condition-'+k+'-'+k2).val() == undefined) { value.condition[k][k2] = '' } else { value.condition[k][k2] = $('#condition-'+k+'-'+k2).val() }
                if ($('#buttonactive-'+k+'-'+k2).val() == undefined) { value.buttonactive[k][k2] = 0 } else { value.buttonactive[k][k2] = $('#buttonactive-'+k+'-'+k2).val() }
                if ($('#classicon-'+k+'-'+k2).val() == undefined) { value.classicon[k][k2] = '' } else { value.classicon[k][k2] = $('#classicon-'+k+'-'+k2).val() }
            })

            html_filtercontentgroup += v
            html_filtercontentbody += filtercontenttabs.filtercontentbody[k]
        })

        $('#filtercontentgroup').html(html_filtercontentgroup);
        $('#filtercontentbody').html(html_filtercontentbody);

        $.each(filtercontenttabs.filtercontentbody, function(k, v) {
            if (filter_tabs[k] == true) {
                changeTabFilter(k)
            }
            $('#groupcaption-'+k).val(value.groupcaption[k])
            $('#groupcaptionshow-'+k).val(value.groupcaptionshow[k])
            $('#groupdropdown-'+k).val(value.groupdropdown[k])
        })

        $.each(filtercontenttabs.filtercontentrow, function(k, v) {
            $('#filter-content-row-'+k).html(v);

            $.each(v, function(k2, v2) {
                $('#buttoncaption-'+k+'-'+k2).val(value.buttoncaption[k][k2])
                $('#condition-'+k+'-'+k2).val(value.condition[k][k2])
                $('#buttonactive-'+k+'-'+k2).val(value.buttonactive[k][k2])
                $('#classicon-'+k+'-'+k2).val(value.classicon[k][k2])
            })
        })
        //End Set to HTML
        // console.log(value)
    }
    
    function changeTabFilter(key) {
        $('.filter-content-group').removeClass('purple').addClass('default')
        $('.filter-content-group span').removeClass('text-white').addClass('text-dark')
        $('.filter-content-group span i').removeClass('text-white').addClass('text-dark')
        $('#filter-content-group-'+key).removeClass('default').addClass('purple')
        $('#filter-content-group-'+key+' span').removeClass('text-dark').addClass('text-white')
        $('#filter-content-group-'+key+' span i').removeClass('text-dark').addClass('text-white')
        $('.filter-content-body').css('display', 'none')
        $('#filter-content-body-'+key).css('display', 'block')
        selected_filter_tabs = key;
        
        $.each(filter_tabs, function(k, v) {
            if (k == key) {
                filter_tabs[k] = true
            } else {
                filter_tabs[k] = false
            }
        })
    }

    function closeModalFilter() {
        $('#modal-filter').modal('hide')
    }

    function showModalFilter() {
        $('#modal-filter').modal('show')
    }
    
    function getData() {
        // if (!JSON.parse(localStorage.getItem('maincms-'+values3))) {
            // localStorage.removeItem('maincms');
            $.ajax({
                type: "POST",
                header:{
                    'X-CSRF-TOKEN':$('#token').val()
                },
                url: "/get_data_master_table",
                dataType: "JSON",
                data:{
                    "_token":$('#token').val(),
                    "table": values3
                },
                success: function(response) {
                    localStorage.setItem('maincms-'+values3, JSON.stringify(response))
                }
            })
        // }
        maincms = JSON.parse(localStorage.getItem('maincms-'+values3))
        if (maincms == null) {
            location.reload()
        }
    }

    function getDatatable() {
        var columns = new Array();
        var columndefs = new Array();
        var index = 1;
        var noncolsorted = new Array();
        var noncolsearch = new Array();
        if (maincms['widgetgrid']['colcheckbox']) {
            // columndefs.push({"targets": [0,1,21],"orderable": false})
            noncolsorted.push(0)
            noncolsorted.push(1)
            noncolsearch.push(0)
            noncolsearch.push(1)
            columns.push({"data": 0,"class": "text-left"})
            columns.push({"data": 1,"class": "text-left"})
            index = 2;
        } else {
            noncolsorted.push(0)
            noncolsearch.push(0)
            columndefs.push({"targets": [0,20],"orderable": false})
            columns.push({"data": 0,"class": "text-left"})
        }
        $.each(maincms['widgetgridfield'], function(k, v) {
            if (k == 0) {
                if (maincms['widgetgrid']['colcheckbox']) {
                    columndefs.push({
                        "height": '60px',
                        "targets": index
                    })
                }
                columndefs.push({
                    "height": '60px',
                    "targets": index
                })
            }
            if (v.colshow) {
                if (v.colwidth != 0) {
                    var colwidth = v.colwidth+'px';
                } else {
                    var colwidth = '100px';
                }
                if (!v.colsorted) {
                    noncolsorted.push(index)
                }
                if (!v.colsearch) {
                    noncolsearch.push(index)
                }
                columns.push({
                    "data": index,
                    "width": colwidth,
                    "class": "text-"+v.colalign.toLowerCase()
                })
                columndefs.push({
                    "height": '60px',
                    "targets": index
                })
                index++
            }
        })
        noncolsorted.push(index)
        noncolsearch.push(index)
        columndefs.push({"targets": noncolsorted,"orderable": false})
        columndefs.push({"targets": noncolsearch,"searchable": false})
        columns.push({"data": index,"class": "text-left"})
        columndefs.push({"height": '60px',"targets": index})
        console.log('columndefs')
        console.log(columns)
        // console.log(maincms['widgetgrid'][0].recperpage)
        mytable = $('#table_pagepanel_9921050101').DataTable({
            "autoWidth": false,
            "pageLength": maincms['widgetgrid'].recperpage,
            "searching": true,
            "processing": true,
            "serverSide": true,
            "bInfo": true,
            "bFilter": false,
            "bLengthChange": false,
            "columns": columns,
            "columnDefs": columndefs,
            // "dom": '<"top dtfiler"<"">>',
            // "dom": '<"info1 top dtfiler"pli<""f><"clear">>rt ',
            // "dom": '<lf<t>ip>',
            "infoCallback": function( settings, start, end, max, total, pre ) {
                var options = '';
                $.each(settings.aLengthMenu, function(k, v) {
                    options += '<option value="'+v+'" id="alm'+v+'" class="alm">'+v+'</option>'
                })
                if ((end-start) == settings._iDisplayLength-1) {
                    var valuepage = end/settings._iDisplayLength;
                } else {
                    var valuepage = ((start-1)/settings._iDisplayLength)+1;
                }
                if (start == 1) {
                    var prevdisabled = 'disabled'
                } else {
                    var prevdisabled = '';
                }
                if (end == total) {
                    var nextdisabled = 'disabled'
                } else {
                    var nextdisabled = '';
                }
                // if (settings.oPreviousSearch.sSearch == $('#searchform').val()) {
                //     alert($('#searchform').val())
                //     var valuesearch = $('#searchform').val()
                // } else {
                //     var valuesearch = $('#searchform').val()
                //     alert('else'+$('#searchform').val())
                // }
                if (settings.oPreviousSearch.sSearch != '') {
                    statussearch = true
                    searchtablewidth = 200
                    valuesearch = settings.oPreviousSearch.sSearch
                } else {
                    statussearch = false
                    searchtablewidth = 30
                    valuesearch = ''
                }
                $('#appendInfoDatatable').html(`
                <div class="row" style="background: #90CAF9; padding-left: 15px; padding-right: 15px;">
                    <div class="col-md-12">
                        Page
                        <button id="prevpage" onclick="prevpage()" class="btn btn-sm default prev `+prevdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-left">
                            </i>
                        </button>
                        <input type="text" id="valuepage" onkeyup="valuepage()" class="input-mini" value="`+valuepage+`" style="text-align: center; border: 0px; margin: 5px 5px 5px 5px">
                        <button id="nextpage" onclick="nextpage()" class="btn btn-sm default next `+nextdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-right">
                            </i>
                        </button> of <span class="pagination-panel-total">`+(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)+`</span>
                        <span class="seperator">|</span>
                        Rec/Page 
                        <select id="alengthmenu" onchange="alengthmenu()" name="table_pagepanel_9921050101_length" aria-controls="table_pagepanel_9921050101" class="input-inline input-xsmall input-sm recofdt" style="border: 0px; margin: 5px 5px 5px 5px">
                            `+options+`
                        </select>
                        <span class="seperator">|</span>Total `+settings._iRecordsTotal+` records
                        <input id="searchform" onkeyup="searchform()" value="`+valuesearch+`" type="search" class="form-control input-inline input-sm clicked" placeholder="" aria-controls="table_pagepanel_9921050101" style="width: `+searchtablewidth+`px; position: absolute; right: 0px; top: 4px; height: 80%; border: 0;">
                        <div onclick="searchtable()" class="btn fa fa-search text-right" style="position: absolute; right: 0px; top: 4px; height: 80%; background: transparent; margin: 0px 0px 0px 0px; padding: 5px 0px 5px 0px">
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                </div>`)
                console.log('settings')
                console.log(settings)
                console.log(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)
                $('.alm').removeAttr('selected');
                $('#alm'+settings._iDisplayLength).attr('selected', 'selected');
                // console.log(valuepage)
                $('.dataTables_info').css('display', 'none')
                $('.dataTables_paginate').css('display', 'none')
                // return '<"top dtfiler"<"">>';
            },
            "ajax": {
                "url": "/get_master_table",
                "type": "POST",
                "dataType":'json',
                "data": {
                    "_token":$('#token').val(),
                    key: 0,
                    table: values3,
                    selected_filter: selected_filter,
                    maincms: JSON.stringify(maincms)
                },
                beforeSend: function () {
                    $(".dataTables_scroll").LoadingOverlay("show");
                },
                complete: function () {
                    $(".dataTables_scroll").LoadingOverlay("hide");
                    // alert('asd')
                    // $.each(maincms['widgetgridfield'][0], function(k, v) {
                    //     $('#example_baru th:nth-child('+k+')').attr('style', 'padding: 0px 5px 0px 5px; width: '+v.colwidth+'px;')
                    //     $('#example_baru td:nth-child('+k+')').attr('style', 'padding: 0px 5px 0px 5px; width: '+v.colwidth+'px;')
                    //     alert(document.querySelector('.dataTables_scrollHeadInner #thead-columns th:nth-child('+k+')').getAttribute('style'))
                    // })
                    // $('#example_baru .sorting_1').attr('style', 'width: 103px')
                },
            },
            "pagingType": "input",
            "createdRow": function(row, data, dataIndex) {
                // console.log(data)
                $(row).addClass('my_row');
                $(row).addClass('row'+dataIndex);
                // $(row).css('height', '5px');
                $(row).attr('onmouseover', 'showButtonAction(this, '+dataIndex+')');
                $(row).attr('onmouseout', 'resetCss()');
                // $(row).attr('onmouseover', 'getPositionXY(this)');
            },
            "oLanguage":{
                "loadingRecords": "Please wait - loading...",
                "sProcessing": "<div id='loadernya'></div>",
                "sInfo": "&nbsp;|&nbsp;Total&nbsp;_TOTAL_ records",
                "sLengthMenu": "&nbsp;|&nbsp;Rec/Page&nbsp;_MENU_",
                "sInfoFiltered": "",
                "sInfoEmpty": "| No records found to show",
                "sEmptyTable": "No data available in table",
                "sZeroRecords": "No matching records found",
                "sSearch": "<button onclick='toggleButtonSearch()' class='btn flat dtfilter_search_btn' style='background-color: white'><i class='fa fa-search'></i></button>",
                // "oPaginate": {
                //   "previous": "<i class='fa fa-angle-left'></i>",
                //   "next": "<i class='fa fa-angle-right'></i>",
                //   "page": "Page",
                //   "pageOf": "of"
                // },
                "oPaginate": {
                    "sPrevious": "<i class='fa fa-angle-left'></i>",
                    "sNext": "<i class='fa fa-angle-right'></i> ",
                // "sPage": "<i id='page_prev'>Page</i>",
                // "sPageOf": "<span id='page_next'> of </span>"
                }
            },
            footerCallback: function ( row, data, start, end, display ) {
                var api = this.api(), data;
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
                // var _split = function ( i ) {
                //     return typeof i === 'string' ?
                //         parseInt(i.replace('.', '')).toString() :
                //         typeof i === 'number' ?
                //             i : 0;
                // };
                var count = function ( i ) {
                    return api.column( 5 )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(end);
                            }, 0 )
                }
                var sum = function ( i ) {
                    var value = 0;
                    $.each(api.column( i ).data(), function(k, v) {
                        var _split = v.split('.');
                        var value2 = '';
                        $.each(_split, function(k2, v2) {
                            value2 += v2
                        })
                        value += Number(value2)
                    })
                    return value
                }
                var addcommas = function addCommas(nStr) {
                    nStr += '';
                    var x = nStr.split('.');
                    var x1 = x[0];
                    var x2 = x.length > 1 ? '.' + x[1] : '';
                    var rgx = /(\d+)(\d{3})/;
                    while (rgx.test(x1)) {
                        x1 = x1.replace(rgx, '$1' + '.' + '$2');
                    }
                    return x1 + x2;
                }

                $.each(functionfooter, function(k, v) {
                    $( api.column(v.index).footer() ).html(api.column(v.index).data().reduce( function (a, b) { return addcommas(eval(functionfooter[k].function)) }, 0 ));
                })
                // console.log('try')
                // console.log(row)
                // console.log(functionfooter)
            }
        });
        
        my_table = $('#example_baru').DataTable({
            "scrollX": true,
            "processing": true,
            "serverSide": true,
            "info": true,
            // "order": [[ 0, "asc" ]],
            "columnDefs": [ 
                { 
                    orderable: false,
                    targets: colsorted
                    // targets: [0,1,30]
                }
            ],
            "pagingType": "input",
            "searching": true,
            "ajax": {
                "url": "/get_master_table",
                "type": "POST",
                "dataType":'json',
                "data": {
                    "_token":$('#token').val(),
                    table: values3,
                    selected_filter: selected_filter,
                    maincms: JSON.stringify(maincms)
                },
                beforeSend: function () {
                    $(".dataTables_scroll").LoadingOverlay("show");
                },
                complete: function () {
                    $(".dataTables_scroll").LoadingOverlay("hide");
                },
            },
            // "sPaginationType":"input",
            "dom": '<"top dtfiler"pli<""f><"clear">>rt ',
            "oLanguage":{
                "loadingRecords": "Please wait - loading...",
                "sProcessing": "<div id='loadernya'></div>",
                "sInfo": "&nbsp;|&nbsp;Total&nbsp;_TOTAL_ records",
                "sLengthMenu": "&nbsp;|&nbsp;Rec/Page&nbsp;_MENU_",
                "sInfoFiltered": "",
                "sInfoEmpty": "| No records found to show",
                "sEmptyTable": "No data available in table",
                "sZeroRecords": "No matching records found",
                "sSearch": "<button onclick='toggleButtonSearch()' class='btn flat dtfilter_search_btn' style='background-color: white'><i class='fa fa-search'></i></button>",
                // "oPaginate": {
                //   "previous": "<i class='fa fa-angle-left'></i>",
                //   "next": "<i class='fa fa-angle-right'></i>",
                //   "page": "Page",
                //   "pageOf": "of"
                // },
                "oPaginate": {
                    "sPrevious": "<i class='fa fa-angle-left'></i>",
                    "sNext": "<i class='fa fa-angle-right'></i> ",
                // "sPage": "<i id='page_prev'>Page</i>",
                // "sPageOf": "<span id='page_next'> of </span>"
                }
            },
            "createdRow": function(row, data, dataIndex) {
                // console.log(data)
                $(row).addClass('my_row');
                $(row).addClass('row'+dataIndex);
                // $(row).css('height', '5px');
                $(row).attr('onmouseover', 'showButtonAction(this, '+dataIndex+')');
                $(row).attr('onmouseout', 'resetCss()');
                // $(row).attr('onmouseover', 'getPositionXY(this)');
            },
            deferRender: true,
            // footerCallback: function ( row, data, start, end, display ) {
            //     var api = this.api(), data;
            //     var intVal = function ( i ) {
            //         return typeof i === 'string' ?
            //             i.replace(/[\$,]/g, '')*1 :
            //             typeof i === 'number' ?
            //                 i : 0;
            //     };
            //     var monTotal = api.column( 1 )
            //         .data()
            //         .reduce( function (a, b) {
            //             return intVal(a) + intVal(b);
            //         }, 0 );

            //     $( api.column( 3 ).footer() ).html(monTotal);
            // }
        });
        
        new $.fn.dataTable.Buttons( mytable, {
            buttons: [
                {
                    autoFilter: true,
                    text:      '<i class="fa fa-trash"></i> Delete Selected',
                    titleAttr: 'Delete Selected',
                    className: ' dropdown-item flat',
                    action:  function(e, dt, button, config) {
                        if (selecteds.length == 0) {
                            alert("There's no selected")
                        } else {
                            if (confirm('Are you sure you want to delete this?')) {
                                removeSelected(selecteds)
                            }
                        }
                    },
                },{
                    autoFilter: true,
                    text:      '<i class="fa fa-trash"></i> Delete All',
                    titleAttr: 'Delete All',
                    className: ' dropdown-item flat',
                    action:  function(e, dt, button, config) {
                        if (selecteds.length == 0) {
                            alert("There's no selected")
                        } else {
                            if (confirm('Are you sure you want to delete this?')) {
                                removeSelected(selecteds)
                            }
                        }
                    },
                },{
                    extend:    'excel',
                    autoFilter: true,
                    text:      '<i class="fa fa-file-excel"></i> Export to Excel',
                    titleAttr: 'Export to Excel',
                    title: $("#judul_excel").val(),
                    className: ' dropdown-item flat',
                    exportOptions: {
                        modifier: {
                            search: 'applied',
                            order: 'applied'
                        },
                        columns: [ 1,2,3, ':visible' ]
                        //                  <a style="font-size: 14px;min-width: 175px;
                        // text-align: left;" href="javascript:;"  class="dropdown-item">
                        // <i style="margin-top:10px;margin-right: 5px;" class="fa fa-file-excel"></i> Export to Excel </a>
                                        // columns: [ 0, ':visible' ]
                    },
                    action:  function(e, dt, button, config) {
                    // $('.loading').fadeIn();
                        var that = this;
                        setTimeout(function () {
                            $.fn.dataTable.ext.buttons.excelHtml5.action.call(that,e, dt, button, config);
                            // console.log(that);
                            // console.log(e);
                            // console.log(dt);
                            // console.log(button);
                            // console.log(config);
                            $('.loading').fadeOut();
                        },500);
                    },
                }
            ]
            // infoDatatable()
        });
        // infoDatatable()

        mytable.buttons().container().appendTo('#drop_export2');
    }

    function searchform() {
        $("#searchform").keyup(function(e) {
            if(e.keyCode == 13) {
                mytable.search($("#searchform").val()).draw('page');
            }
        })
    }

    function searchtable(noid) {
        if (searchtablewidth == 30) {
            searchtablewidth = 200
        } else {
            searchtablewidth = 30
        }
        $("#searchform").animate({ 
            width: searchtablewidth+'px' 
        }, 100); 
    }

    function prevpage() {
        mytable.page('previous').draw('page');
    }

    function nextpage() {
        mytable.page('next').draw('page');
    }

    function alengthmenu() {
        mytable.page.len($('#alengthmenu :selected').val()).draw();
    }

    function destroyDatatable() {
        // $('#example_baru').DataTable().destroy();
        $('#table_pagepanel_9921050101').DataTable().destroy();
    }
// 
    function toggleButtonCollapse(noid) {
        if (noid) {
            $('#tab-widget-content-'+noid+' .card .card-body').toggleClass('collapse');
        } else {
            $('#kt_content .container .card .card-body').toggleClass('collapse');
        }
        // $('#kt_content .container .card .card-body').toggleClass('collapse').delay('slow').fadeOut();
    }

    function toggleButtonSearch() {
        $('#example_baru_filter label input').toggleClass('clicked');
    }

    function filterData(element_id, element_class, key, buttoncaption, classcolor, groupdropdown, is_all, table='', operand='', fieldname='', operator='', value='') {
        selected_filter.operand[key] = operand;
        selected_filter.fieldname[key] = fieldname;
        selected_filter.operator[key] = operator;
        selected_filter.value[key] = value;

        if (groupdropdown == 1) {
            $('#filter-dropdown-active-'+key).text(buttoncaption)
            // alert(buttoncaption)
        } else {
            $('.'+element_class).css('background', '#E3F2FD');
            $('.'+element_class).css('color', classcolor);
            $('.'+element_class).css('font-weight', 'normal');
            $('#'+element_id).css('background', '#90CAF9');
            $('#'+element_id).css('color', 'black');
            $('#'+element_id).css('font-weight', 'bold');
        }
        // console.log(groupdropdown)
        
        destroyDatatable()
        getDatatable()
    }

    function getSlider(id) {
        var owl = $('.owl-carousel').owlCarousel({
            loop:true,
            smartSpeed: 100,
            autoplay: true,
            autoplaySpeed: 100,
            mouseDrag: false,
            margin:10,
            animateIn: 'slideInUp',
            animateOut: 'fadeOut',
            nav:false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });

        $.ajax({
            type: "POST",
            header:{
            'X-CSRF-TOKEN':$('#token').val()
            },
            url: "/get_slide_master_table",
            dataType: "json",
            data:{
                "_token":$('#token').val(),
                "id_slider": id
            },
            success: function (response) {
                // console.log(response.hasil);
                var base_url = '{{ url('/') }}';
                if(isNaN(base_url)) {
                    base_url = '';
                }
                var name1 = '';
                var name2 = '';

                $.each(response.hasil, function(i, item) {
                    // console.log(item.mainimage);
                    name1 = 'public/'+item.mainimage;

                    // console.log(name1);
                    var random_num =  Math.floor(Math.random()*60);
                    owl.trigger('add.owl.carousel', [jQuery('<div class="notification-message"> <img src="'+base_url+name1+'" class="user-image" alt=""> <div class="user-name">'+ item.newstitle+' <span class="lighter">from '+item.idnews+'</span></div> <div class="bought-details">Bought This <br>'+random_num+' minutes ago</div> </div>')]);
                });
                owl.trigger('refresh.owl.carousel');
            },
        });
    }

    function chartBatang(response,id_cart) {
        var dataatas = response.hasil;
        var dataProvideratas = [];// this variable you have to pass in dataProvider inside chart
        for(var key in dataatas) {
            dataProvideratas.push({
                category: dataatas[key]['categories'],
                visits: dataatas[key]['jumlahdata'],
            });
        }
        var chart = AmCharts.makeChart("kt_amcharts_"+id_cart, {
            "rtl": KTUtil.isRTL(),
            "type": "serial",
            "hideCredits":true,
            "theme": "light",
            "dataProvider": dataProvideratas,
            "valueAxes": [{
                "gridColor": "#FFFFFF",
                "gridAlpha": 0.2,
                "dashLength": 0
            }],
            "series": [{
                "type": "LineSeries",
                "stroke": "#ff0000",
                "strokeWidth": 3
            }],
            "valueAxes": [ {
                "position": "left",
                "title": "Jumlah Data"
            }, {
                "position": "bottom",
                "title": response.widget.widget_widgetcaption
            } ],
            "legend": {
                "useGraphSettings": true
            },
            "titles": [
                {
                    "size": 15,
                    "text":response.widget.widget_widgetcaption
                },
                {
                    "text": response.widget.widget_widgetsubcaption,
                    "bold": false
                }
            ],
            "gridAboveGraphs": true,
            "startDuration": 1,
            "graphs": [{
                "balloonText": "[[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "visits",
                "title": response.widget.widget_widgetcaption
            }],
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "category",
            "categoryAxis": {
                "gridPosition": "start",
            },
            "export": {
                "enabled": true
            }
        });
    }

    function chartPie(response,id_cart) {
        var data = response.hasil;
        var dataProvider = [];// this variable you have to pass in dataProvider inside chart
        for(var key in data) {
            dataProvider.push({
            country: data[key]['messagestatus'],
            litres: data[key]['jumlahdata'],
            });
        }

        var chart = AmCharts.makeChart("kt_amcharts_"+id_cart,{
            "type"    : "pie",
            "hideCredits"    : true,
            "theme"    : "light",
            "addClassNames": true,
            "titles": [
                {
                    "size": 15,
                    "text": response.widget.widget_widgetcaption
                },
                {
                    "text": response.widget.widget_widgetsubcaption,
                    "bold": false
                }
            ],
            // "pullOutDuration": 0,
            // "pullOutRadius": 0,

            "labelsEnabled": false,
            "titleField"  : "country",
            "valueField"  : "litres",
            "dataProvider"  : dataProvider,
            "numberFormatter": {
                "precision": -1,
                "decimalSeparator": ",",
                "thousandsSeparator": ""
            },
            "legend": {
                "align": "center",
                "position": "bottom",
                "marginRight": 0,
                "labelText": "[[title]]",
                "valueText": "",
                "valueWidth": 50,
                "textClickEnabled": true
            },
            "export": {
                "enabled": true
            }

        });
    }

    $("#savedata").click(function(e){
        // console.log($("#formdepartement").serializeArray())
        // return false;
        e.preventDefault();

        var input_data_master = {};
        var mstrtbl = maincms['widget'].maintable;
        var onrequired = false;
        var errormessage = '';
        $.each($("#formdepartement").serializeArray(), function(k, v) {
            var formcaption = $('#'+mstrtbl+'-'+v.name).attr('data-'+mstrtbl+'-formcaption');
            if (v.name == 'noid') {
                // input_data_master[v.name] = nextnoid
            } else {
                input_data_master[v.name] = v.value

                if (statusadd) {
                    if ($('#'+mstrtbl+'-'+v.name).attr('data-'+mstrtbl+'-newreq') == 1) {
                        if (v.value == '') {
                            onrequired = true;
                            errormessage += formcaption+' is required!<br>'
                        }
                    }
                } else {
                    if ($('#'+mstrtbl+'-'+v.name).attr('data-'+mstrtbl+'-editreq') == 1) {
                        if (v.value == '') {
                            onrequired = true;
                            errormessage += formcaption+' is required!<br>'
                        }
                    }
                }
            }
        })
        
        if (onrequired) {
            $.notify({
                message: errormessage
            },{
                type: 'danger'
            });
            return false;
        }

        var kode = $("#kode").val();
        var nama = $("#nama").val();
        var namatable =$('#namatable').val();
        var formdata = $("#formdepartement").serialize();
        var formdata2 = '';
        var checkbox = $("#formdepartement").find("input[type=checkbox]");
        $.each(checkbox, function(k, v) {
            if (checkbox[k].checked) {
                var checked = 1;
            } else {
                var checked = 0;
            }
            if (k == 0) {
                if (formdata2 = '') {
                    formdata2 += checkbox[k].name+'='+checked;
                } else {
                    formdata2 += '&'+checkbox[k].name+'='+checked;
                }
            } else {
                formdata2 += '&'+checkbox[k].name+'='+checked;
            }
        });
        formdata += formdata2
        // console.log('formdata')
        // console.log(formdata)
        // console.log(checkbox[0].checked)
        // console.log(checkbox[0].name)
        // return false;
        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            url: "/save_master_table",
            dataType: "json",
            data:{
                "_token":$('#token').val(),
                "data" :{
                    'data' : formdata,
                    'tabel': namatable
                },
                "table": namatable,
                "statusadd": statusadd,
                maincms: JSON.stringify(maincms)
            },
            success:function(result){
                if (result.status == 'add') {
                    // $("#notiftext").text('Data  Berhasil di Tambah');
                    $.notify({
                        message: 'Add data has been succesfullly.' 
                    },{
                        type: 'success'
                    });
                } else {
                    // $("#notiftext").text('Data  Berhasil di Edit');
                    $.notify({
                        message: 'Edit data has been succesfullly.' 
                    },{
                        type: 'success'
                    });
                }

                document.getElementById("formdepartement").reset();
                refresh();
                // $('#divsucces').show(0).delay(5000).hide(0);
                var x = document.getElementById("from-departement");
                var x2 = document.getElementById("tabledepartemen");
                var y = document.getElementById("tool-kedua");
                var y2 = document.getElementById("tool-pertama");
                x.style.display = "none";
                x2.style.display = "block";
                y.style.display = "none";
                y2.style.display = "block";
            }
        });
    });

    function refresh() {
        $('#table_pagepanel_9921050101').DataTable().ajax.reload();
    }

    function getNextNoid() {
        $.ajax({
            url: '/get_next_noid_master_table',
            type: "POST",
            dataType: 'json',
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            data: {
                "table" : maincms['widget']['maintable'],
                "_token": $('#token').val(),
                maincms: JSON.stringify(maincms)
            },
            success: function (response) {
                nextnoid = response
                localStorage.setItem('input-data-noid', response)
                // console.log(localStorage.getItem('input-data-noid'))
                // alert(nextnoid)
            }
        })
    }

    function addData() {
        resetForm();
        resetData();
        statusadd = true;
        getNextNoid();
        getForm(statusadd, maincms['widget']['noid'], 'divform');
        // getForm();


        // var x = document.getElementById("from-departement");
        // var x2 = document.getElementById("tabledepartemen");
        // var y = document.getElementById("tool-kedua");
        // var y2 = document.getElementById("tool-pertama");
        // var tool_refresh = document.getElementById("tool_refresh");
        // var tool_refresh_up = document.getElementById("tool_refresh_up");
        // var space_filter_button = $('#space_filter_button');

        $.each(roles.fieldname, function(k, v) {
            if (roles.newhidden[k] == 1) {
                $('#div-'+v).css('display', 'none')
            } else {
                $('#div-'+v).css('display', 'block')
            }
        })

        // if (x.style.display === "none") {
        //     x.style.display = "block";
        //     x2.style.display = "none";
        //     y.style.display = "block";
        //     y2.style.display = "none";
        //     tool_refresh.style.display = "none";
        //     tool_refresh_up.style.display = "none";
        //     space_filter_button.css('display', 'none');
        // } else {
        //     x.style.display = "none";
        //     x2.style.display = "block";
        //     space_filter_button.css('display', 'block');
        // }

        $('#from-departement').show();
        $('#tabledepartemen').hide();
        $('#tool-kedua').show();
        $('#tool-pertama').hide();
        $('#space_filter_button').hide();
        $('#tool_refresh').hide();
        $('#tool_filter').hide();
        $('#tool_refresh_up').hide();
    }

    function resetForm() {
        $('#divform').html('')
    }

    function resetData() {
        localStorage.removeItem('input-data');
    }

    function cancelData() {
        // var x = document.getElementById("from-departement");
        // var x2 = document.getElementById("tabledepartemen");
        // var y = document.getElementById("tool-kedua");
        // var y2 = document.getElementById("tool-pertama");
        // var y3 = document.getElementById("tool-ketiga");
        // var tool_refresh = document.getElementById("tool_refresh");
        // var tool_refresh_up = document.getElementById("tool_refresh_up");
        // var space_filter_button = $('#space_filter_button');

        // tool_refresh.style.display = "block";
        // tool_refresh_up.style.display = "block";
        // x.style.display = "none";
        // x2.style.display = "block";
        // y.style.display = "none";
        // y2.style.display = "block";
        // y3.style.display = "none";
        // space_filter_button.css('display', 'block');

        $('#from-departement').hide();
        $('#tabledepartemen').show();
        $('#tool-kedua').hide();
        $('#tool-pertama').show();
        $('#space_filter_button').show();
        $('#tool_refresh').show();
        $('#tool_filter').show();
        $('#tool_refresh_up').show();

        document.getElementById("formdepartement").reset();
    }

    function view(id) {
        statusadd = false;
        nownoid = id;
        resetForm();
        resetData();
        getForm(statusadd, maincms['widget']['noid'], 'divform');


        // var x = document.getElementById("from-departement");
        // var x2 = document.getElementById("tabledepartemen");
        // var y = document.getElementById("tool-kedua");
        // var y2 = document.getElementById("tool-pertama");
        // var y3 = document.getElementById("tool-ketiga");
        // var space_filter_button = $('#space_filter_button');

        $.ajax({
            url: '/find_master_table',
            type: "POST",
            dataType: 'json',
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            data: {
                "id" : id,
                "namatable" : $("#namatable").val(),
                "_token": $('#token').val(),
                maincms: JSON.stringify(maincms)
            },
            success: function (response) {
                $.each(response, function(k, v) {
                    if (v.formtypefield == 31) {
                        if (v.value) {
                            $("#"+v.tablename+'-'+v.field).attr('checked', true);
                        }
                    } else {
                        $("#"+v.tablename+'-'+v.field).val(v.value);
                    }
                    $("#"+v.tablename+'-'+v.field).attr('disabled', 'disabled');
                });
                
                // if (x.style.display === "none") {
                //     x.style.display = "block";
                //     x2.style.display = "none";
                //     y.style.display = "none";
                //     y2.style.display = "none";
                //     y3.style.display = "block";
                //     space_filter_button.css('display', 'none');
                // } else {
                //     x.style.display = "none";
                //     x2.style.display = "block";
                //     space_filter_button.css('display', 'block');
                // }
                
                $('#from-departement').show();
                $('#tabledepartemen').hide();
                $('#tool-kedua').show();
                $('#tool-pertama').hide();
                $('#space_filter_button').hide();
                $('#tool_refresh').hide();
                $('#tool_filter').hide();
                $('#tool_refresh_up').hide();
            },
        });
        
    }

    function edit(id) {
        statusadd = false;
        statusdetail = false;
        nownoid = id;
        resetForm();
        resetData();
        getForm(statusadd, maincms['widget']['noid'], 'divform');
        // getCmsTabDatatable();


        editid = id;
        



        // var x = document.getElementById("from-departement");
        // var x2 = document.getElementById("tabledepartemen");
        // var y = document.getElementById("tool-kedua");
        // var y2 = document.getElementById("tool-pertama");
        // var space_filter_button = $('#space_filter_button');

        // if (x.style.display === "none") {
        //     x.style.display = "block";
        //     x2.style.display = "none";
        //     y.style.display = "block";
        //     y2.style.display = "none";
        //     space_filter_button.css('display', 'none');
        // } else {
        //     x.style.display = "none";
        //     x2.style.display = "block";
        //     space_filter_button.css('display', 'block');
        // }

        $('#from-departement').show();
        $('#tabledepartemen').hide();
        $('#tool-kedua').show();
        $('#tool-pertama').hide();
        $('#space_filter_button').hide();
        $('#tool_refresh').hide();
        $('#tool_filter').hide();
        $('#tool_refresh_up').hide();
        
        $.ajax({
            url: '/find_master_table',
            type: "POST",
            dataType: 'json',
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            data: {
                "id" : id,
                "namatable" : $("#namatable").val(),
                "_token": $('#token').val(),
                maincms: JSON.stringify(maincms)
            },
            success: function (response) {
                $('#noid').val(id)
                console.log('response')
                console.log(response)
                $.each(response, function(k, v) {
                    if (v.formtypefield == 31) {
                        if (v.value) {
                            $("#"+v.tablename+'-'+v.field).attr('checked', true);
                        }
                    } else if (v.formtypefield == 41) {
                        if (v.value) {
                            $("#"+v.tablename+'-'+v.field).attr('checked', true);
                        }
                    } else if (v.formtypefield == 51) {
                        if (v.value) {
                            $("#"+v.tablename+'-'+v.field).attr('checked', true);
                        }
                    } else if (v.formtypefield == 96) {
                        if (v.value) {
                            $("#"+v.tablename+'-'+v.field+'-select').val(v.value).trigger('change');
                        }
                    } else if (v.formtypefield == 97) {
                        if (v.value) {
                            $("#"+v.tablename+'-'+v.field+'-select').val(v.value).trigger('change');
                        }
                    } else if (v.formtypefield == 98) {
                        if (v.value) {
                            $("#"+v.tablename+'-'+v.field+'-select').val(v.value).trigger('change');
                        }
                    } else {
                        $("#"+v.tablename+'-'+v.field).val(v.value);
                    }
                    if (v.disabled) {
                        $("#"+v.tablename+'-'+v.field).attr('disabled', 'disabled');
                    }
                    if (v.edithidden) {
                        $("#div-"+v.tablename+'-'+v.field).css('display', 'none');
                    }
                });
                // console.log(response)
            }
        });

    }

    function remove(id) {
        // console.log(id);return false;
        if (confirm('Are you sure you want to delete this?')) {
            $.ajax({
                url: '/delete_master_table',
                type: "POST",
                header:{
                    'X-CSRF-TOKEN':$('#token').val()
                },
                data: {
                    "id" : id,
                    "namatable" :  $("#namatable").val(),
                    "_token": $('#token').val()
                },
                success: function () {
                    // $("#notiftext").text('Data  Berhasil di hapus');
                    $.notify({
                        message: 'Delete data has been succesfullly.' 
                    },{
                        type: 'success'
                    });
                    refresh();
                    // $('#divsucces').show(0).delay(5000).hide(0);
                    var x = document.getElementById("from-departement");
                    var x2 = document.getElementById("tabledepartemen");
                    var y = document.getElementById("tool-kedua");
                    var y2 = document.getElementById("tool-pertama");
                    x.style.display = "none";
                    x2.style.display = "block";
                    y.style.display = "none";
                    y2.style.display = "block";
                }
            });
        }
    }

    (function ($) {
    // function infoDatatable() {
        console.log('tes gaes')
        
        function calcDisableClasses(oSettings) {
            var start = oSettings._iDisplayStart;
            var length = oSettings._iDisplayLength;
            var visibleRecords = oSettings.fnRecordsDisplay();
            var all = length === -1;

            // Gordey Doronin: Re-used this code from main jQuery.dataTables source code. To be consistent.
            var page = all ? 0 : Math.ceil(start / length);
            var pages = all ? 1 : Math.ceil(visibleRecords / length);

            var disableFirstPrevClass = (page > 0 ? '' : oSettings.oClasses.sPageButtonDisabled);
            var disableNextLastClass = (page < pages - 1 ? '' : oSettings.oClasses.sPageButtonDisabled);

            return {
                'first': disableFirstPrevClass,
                'previous': disableFirstPrevClass,
                'next': disableNextLastClass,
                'last': disableNextLastClass
            };
        }

        function calcCurrentPage(oSettings) {
            return Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength) + 1;
        }

        function calcPages(oSettings) {
            return Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength);
        }

        var firstClassName = 'first';
        var previousClassName = 'previous';
        var nextClassName = 'next';
        var lastClassName = 'last';

        var paginateClassName = 'paginate';
        var paginatePageClassName = 'paginate_page';
        var paginateInputClassName = 'paginate_input';
        var paginateTotalClassName = 'paginate_total';

        $.fn.dataTableExt.oPagination.input = {
            'fnInit': function (oSettings, nPaging, fnCallbackDraw) {
                var nFirst = document.createElement('span');
                var nPrevious = document.createElement('span');
                var nNext = document.createElement('span');
                var nLast = document.createElement('span');
                var nInput = document.createElement('input');
                var nTotal = document.createElement('span');
                var nInfo = document.createElement('span');

                var language = oSettings.oLanguage.oPaginate;
                var classes = oSettings.oClasses;
                var info = language.info || `&nbsp;&nbsp;
                                                <label style="padding-top: 8px">Page</label>&nbsp;
                                                _INPUT_&nbsp;
                                                <label style="padding-top: 8px">of&nbsp;_TOTAL_</label>&nbsp;`;

                nFirst.innerHTML = language.sFirst;
                nPrevious.innerHTML = language.sPrevious;
                nNext.innerHTML = language.sNext;
                nLast.innerHTML = language.sLast;

                nFirst.className = firstClassName + ' ' + classes.sPageButton;
                nPrevious.className = previousClassName + ' ' + classes.sPageButton;
                nNext.className = nextClassName + ' ' + classes.sPageButton;
                nLast.className = lastClassName + ' ' + classes.sPageButton;

                nInput.className = paginateInputClassName;
                nTotal.className = paginateTotalClassName;

                if (oSettings.sTableId !== '') {
                    nPaging.setAttribute('id', oSettings.sTableId + '_' + paginateClassName);
                    nFirst.setAttribute('id', oSettings.sTableId + '_' + firstClassName);
                    nPrevious.setAttribute('id', oSettings.sTableId + '_' + previousClassName);
                    nNext.setAttribute('id', oSettings.sTableId + '_' + nextClassName);
                    nLast.setAttribute('id', oSettings.sTableId + '_' + lastClassName);
                }

                nInput.type = 'text';

                info = info.replace(/_INPUT_/g, '</span>' + nInput.outerHTML + '<span>');
                info = info.replace(/_TOTAL_/g, '</span>' + nTotal.outerHTML + '<span>');
                nInfo.innerHTML = '<span>' + info + '</span>';

                nPaging.appendChild(nFirst);
                nPaging.appendChild(nPrevious);
                $(nInfo).children().each(function (i, n) {
                    nPaging.appendChild(n);
                });
                nPaging.appendChild(nNext);
                nPaging.appendChild(nLast);

                $(nFirst).click(function() {
                    var iCurrentPage = calcCurrentPage(oSettings);
                    if (iCurrentPage !== 1) {
                        oSettings.oApi._fnPageChange(oSettings, 'first');
                        fnCallbackDraw(oSettings);
                    }
                });

                $(nPrevious).click(function() {
                    var iCurrentPage = calcCurrentPage(oSettings);
                    if (iCurrentPage !== 1) {
                        oSettings.oApi._fnPageChange(oSettings, 'previous');
                        fnCallbackDraw(oSettings);
                    }
                });

                $(nNext).click(function() {
                    var iCurrentPage = calcCurrentPage(oSettings);
                    if (iCurrentPage !== calcPages(oSettings)) {
                        oSettings.oApi._fnPageChange(oSettings, 'next');
                        fnCallbackDraw(oSettings);
                    }
                });

                $(nLast).click(function() {
                    var iCurrentPage = calcCurrentPage(oSettings);
                    if (iCurrentPage !== calcPages(oSettings)) {
                        oSettings.oApi._fnPageChange(oSettings, 'last');
                        fnCallbackDraw(oSettings);
                    }
                });

                $(nPaging).find('.' + paginateInputClassName).keyup(function (e) {
                    // 38 = up arrow, 39 = right arrow
                    if (e.which === 38 || e.which === 39) {
                        this.value++;
                    }
                    // 37 = left arrow, 40 = down arrow
                    else if ((e.which === 37 || e.which === 40) && this.value > 1) {
                        this.value--;
                    }

                    if (this.value === '' || this.value.match(/[^0-9]/)) {
                        /* Nothing entered or non-numeric character */
                        this.value = this.value.replace(/[^\d]/g, ''); // don't even allow anything but digits
                        return;
                    }

                    var iNewStart = oSettings._iDisplayLength * (this.value - 1);
                    if (iNewStart < 0) {
                        iNewStart = 0;
                    }
                    if (iNewStart >= oSettings.fnRecordsDisplay()) {
                        iNewStart = (Math.ceil((oSettings.fnRecordsDisplay()) / oSettings._iDisplayLength) - 1) * oSettings._iDisplayLength;
                    }

                    oSettings._iDisplayStart = iNewStart;
                    oSettings.oInstance.trigger("page.dt", oSettings);
                    fnCallbackDraw(oSettings);
                });

                // Take the brutal approach to cancelling text selection.
                $('span', nPaging).bind('mousedown', function () { return false; });
                $('span', nPaging).bind('selectstart', function() { return false; });

                // If we can't page anyway, might as well not show it.
                var iPages = calcPages(oSettings);
                if (iPages <= 1) {
                    $(nPaging).hide();
                }
            },

            'fnUpdate': function (oSettings) {
                if (!oSettings.aanFeatures.p) {
                    return;
                }

                var iPages = calcPages(oSettings);
                var iCurrentPage = calcCurrentPage(oSettings);

                var an = oSettings.aanFeatures.p;
                if (iPages <= 1) // hide paging when we can't page
                {
                    $(an).hide();
                    return;
                }

                var disableClasses = calcDisableClasses(oSettings);

                $(an).show();

                // Enable/Disable `first` button.
                $(an).children('.' + firstClassName)
                    .removeClass(oSettings.oClasses.sPageButtonDisabled)
                    .addClass(disableClasses[firstClassName]);

                // Enable/Disable `prev` button.
                $(an).children('.' + previousClassName)
                    .removeClass(oSettings.oClasses.sPageButtonDisabled)
                    .addClass(disableClasses[previousClassName]);

                // Enable/Disable `next` button.
                $(an).children('.' + nextClassName)
                    .removeClass(oSettings.oClasses.sPageButtonDisabled)
                    .addClass(disableClasses[nextClassName]);

                // Enable/Disable `last` button.
                $(an).children('.' + lastClassName)
                    .removeClass(oSettings.oClasses.sPageButtonDisabled)
                    .addClass(disableClasses[lastClassName]);

                // Paginate of N pages text
                $(an).find('.' + paginateTotalClassName).html(iPages);

                // Current page number input value
                $(an).find('.' + paginateInputClassName).val(iCurrentPage);
            }
        };
    // }
    })(jQuery);
</script>
@endsection
