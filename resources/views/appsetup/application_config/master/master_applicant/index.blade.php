@extends('lam_custom_layouts.default')

@section('content')
    <div class="card">
        <div class="card-body">
            <h5><b>Master Applicant</b></h5>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h6><b>Data</b></h6>
                            <table id="table_applicant" class="table compact">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Posisi</th>
                                        <th>Nama Depan</th>
                                        <th>Nama Belakang</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modal_detail" tabindex="-1" role="dialog" aria-labelledby="modal_detailLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_detailLabel">Detail <b id="modal_detail_name"></b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4">Nama Depan</div>
                            <div class="col-md-8" id="detail-firstname"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Nama Belakang</div>
                            <div class="col-md-8" id="detail-lastname"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Jenis Kelamin</div>
                            <div class="col-md-8" id="detail-gender"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Pendidikan Terakhir</div>
                            <div class="col-md-8" id="detail-lasteducation"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Tempat Lahir</div>
                            <div class="col-md-8" id="detail-birthplace"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Tanggal Lahir</div>
                            <div class="col-md-8" id="detail-birthdate"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Alamat KTP</div>
                            <div class="col-md-8" id="detail-ktpaddress"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Alamat Domisili</div>
                            <div class="col-md-8" id="detail-localaddress"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Nomor Telepon</div>
                            <div class="col-md-8" id="detail-phone"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Whatsapp</div>
                            <div class="col-md-8" id="detail-whatsapp"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Nomor KK</div>
                            <div class="col-md-8" id="detail-kknumber"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Nomor KTP</div>
                            <div class="col-md-8" id="detail-Nomor KTP"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Nomor NPWP</div>
                            <div class="col-md-8" id="detail-npwpnumber"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4">Golongan Darah</div>
                            <div class="col-md-8" id="detail-blood"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Tinggi Badan</div>
                            <div class="col-md-8" id="detail-height"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Status</div>
                            <div class="col-md-8" id="detail-status"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Agama</div>
                            <div class="col-md-8" id="detail-religion"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Kebangsaan</div>
                            <div class="col-md-8" id="detail-nationality"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Tempat Tinggal</div>
                            <div class="col-md-8" id="detail-residence"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Kendaraan</div>
                            <div class="col-md-8" id="detail-vehiclestatus"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Jenis Kendaraan</div>
                            <div class="col-md-8" id="detail-vehicletype"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Bahasa</div>
                            <div class="col-md-8" id="detail-language"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">SIM</div>
                            <div class="col-md-8" id="detail-sims"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Merk HP</div>
                            <div class="col-md-8" id="detail-phonebrand"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Tipe HP</div>
                            <div class="col-md-8" id="detail-phonetype"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Ukuran Baju</div>
                            <div class="col-md-8" id="detail-clothessize"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Ukuran Sepatu</div>
                            <div class="col-md-8" id="detail-shoessize"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">Email</div>
                            <div class="col-md-8" id="detail-email"></div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <h4># Orang yang dapat dihubungi dalam keadaan darurat (Tidak Seatap)</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table border="1">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Status Hubungan</th>
                                    <th>Nama Lengkap</th>
                                    <th>Alamat Lengkap</th>
                                    <th>Nomor Telepon</th>
                                    <th>Nomor Whatsapp</th>
                                </tr>
                            </thead>
                            <tbody id="tbody-emergency">
                            </tbody>
                        </table>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <h4># Susunan Keluarga (Wajib Dilengkapi)</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table border="1">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Status Hubungan</th>
                                    <th>Nama Lengkap</th>
                                    <th>Nomor Telepon</th>
                                    <th>Nomor Whatsapp</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Tempat Lahir</th>
                                    <th>Tanggal Lahir</th>
                                    <th>Pendidikan</th>
                                </tr>
                            </thead>
                            <tbody id="tbody-family">
                            </tbody>
                        </table>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <h4># Riwayat Pendidikan</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table border="1">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Masuk</th>
                                    <th>Lulus</th>
                                    <th>Sekolah/PT</th>
                                    <th>Jurusan</th>
                                    <th>IPK/NEM</th>
                                    <th>Kota</th>
                                    <th>Ijazah</th>
                                </tr>
                            </thead>
                            <tbody id="tbody-education">
                            </tbody>
                        </table>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <h4># Pernah Training</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table border="1">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Pelatihan/Kursus</th>
                                    <th>Kota</th>
                                    <th>Penyelenggara</th>
                                    <th>Tahun</th>
                                    <th>Sertifikat</th>
                                </tr>
                            </thead>
                            <tbody id="tbody-training">
                            </tbody>
                        </table>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <h4># Pernah Bekerja</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table border="1">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Masuk</th>
                                    <th>Keluar</th>
                                    <th>Nama Perusahaan</th>
                                    <th>Kota</th>
                                    <th>Jabatan</th>
                                    <th>Gaji</th>
                                    <th>Alasan Keluar</th>
                                </tr>
                            </thead>
                            <tbody id="tbody-worked">
                            </tbody>
                        </table>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <h4># Kelebihan</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table border="1">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kelebihan</th>
                                </tr>
                            </thead>
                            <tbody id="tbody-positive">
                            </tbody>
                        </table>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <h4># Kelemahan</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table border="1">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kelemahan</th>
                                </tr>
                            </thead>
                            <tbody id="tbody-negative">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            </div>
            </div>
        </div>
    </div>
@endsection

@section('styles')
    <!-- Datatable -->
    <link href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <!-- Select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection
    
@section('scripts')
    <!-- Datatable -->
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <!-- Select2 -->
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    
    <!-- Custom -->
    <script type="text/javascript">
        var main = {
            urlcode: 99020209
        };

        $(document).ready(function() {
            getSelect2();
            getApplicant();
        })

        const getApplicant = () => {
            $('#table_applicant').DataTable().destroy();
            $('#table_applicant').DataTable({
                serverSide: true,
                order: [[2, 'desc']],
                pageLength: 5,
                processing: true,
                lengthMenu: [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, 'All'],
                ],
                columns: [
                    { mData: 'no', orderable: false, class: 'text-right' },
                    { mData: 'jobappliedfor' },
                    { mData: 'firstname' },
                    { mData: 'lastname' },
                    { mData: 'action', orderable: false },
                ],
                ajax: {
                    url: `/getApplicant_${main.urlcode}`,
                    type: 'POST',
                    dataType:'json',
                    data: {
                        _token:$('meta[name="csrf-token"]').attr('content'),
                        month_and_year: $('#month_and_year').val(),
                        year_only: $('#year_only').val(),
                        id_cost_control: $('#id_cost_control :selected').val(),
                    },
                },
            });
            $('<button class="btn btn-secondary ml-2" onclick="getApplicant()"><i class="icon-refresh"></i></button>').appendTo('#table_applicant_filter');
        }

        const getApplicantDetail = (params) => {
            $.ajax({
                url: `/getApplicantDetail_${main.urlcode}`,
                type: 'POST',
                dataType: 'json',
                data: {
                    noid: params.noid
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(res) {
                    if (res.success) {
                        let tbody_emergency = '';
                        let tbody_family = '';
                        let tbody_education = '';
                        let tbody_training = '';
                        let tbody_worked = '';
                        let tbody_positive = '';
                        let tbody_negative = '';
                        $.each(res.data.emergency, function(k, v) {
                            tbody_emergency += `<tr>
                                <td>${k+1}</td>
                                <td>${v.status}</td>
                                <td>${v.name}</td>
                                <td>${v.address}</td>
                                <td>${v.phone}</td>
                                <td>${v.whatsapp}</td>
                            </tr>`;
                        });
                        $.each(res.data.family, function(k, v) {
                            tbody_family += `<tr>
                                <td>${k+1}</td>
                                <td>${v.status}</td>
                                <td>${v.name}</td>
                                <td>${v.phone}</td>
                                <td>${v.whatsapp}</td>
                                <td>${v.gender}</td>
                                <td>${v.birthplace}</td>
                                <td>${v.birthdate}</td>
                                <td>${v.education}</td>
                            </tr>`;
                        });
                        $.each(res.data.education, function(k, v) {
                            tbody_education += `<tr>
                                <td>${k+1}</td>
                                <td>${v.in}</td>
                                <td>${v.out}</td>
                                <td>${v.school}</td>
                                <td>${v.major}</td>
                                <td>${v.score}</td>
                                <td>${v.city}</td>
                                <td>${v.certificate}</td>
                            </tr>`;
                        });
                        $.each(res.data.training, function(k, v) {
                            tbody_training += `<tr>
                                <td>${k+1}</td>
                                <td>${v.name}</td>
                                <td>${v.city}</td>
                                <td>${v.organizer}</td>
                                <td>${v.year}</td>
                                <td>${v.certificate}</td>
                            </tr>`;
                        });
                        $.each(res.data.worked, function(k, v) {
                            tbody_worked += `<tr>
                                <td>${k+1}</td>
                                <td>${v.in}</td>
                                <td>${v.out}</td>
                                <td>${v.company}</td>
                                <td>${v.city}</td>
                                <td>${v.position}</td>
                                <td>${v.salary}</td>
                                <td>${v.stopreason}</td>
                            </tr>`;
                        });
                        $.each(res.data.positive, function(k, v) {
                            tbody_positive += `<tr>
                                <td>${k+1}</td>
                                <td>${v.description}</td>
                            </tr>`;
                        });
                        $.each(res.data.negative, function(k, v) {
                            tbody_negative += `<tr>
                                <td>${k+1}</td>
                                <td>${v.description}</td>
                            </tr>`;
                        });
                        $('#tbody-emergency').html(tbody_emergency);
                        $('#tbody-family').html(tbody_family);
                        $('#tbody-education').html(tbody_education);
                        $('#tbody-training').html(tbody_training);
                        $('#tbody-worked').html(tbody_worked);
                        $('#tbody-positive').html(tbody_positive);
                        $('#tbody-negative').html(tbody_negative);
                    }
                    console.log(res)
                },
            });
        }

        const showModal = (params) => {
            getApplicantDetail({noid: params.noid});
            $.each(params, function(k, v) {
                $(`#detail-${k}`).html(`<b>${v}</b>`);
            })
            $(params.element_id).modal('show');
        }

        const hideModal = (params) => {
            $(params.element_id).modal('hide');
        }

        const reset = () => {
            $('#month_and_year').val(null);
            $('#year_only').val(null);
            $('#id_cost_control').val(null).trigger('change');
            getApplicant();
        }

        const getSelect2 = () => {
            $('.select2').select2();
            $('.select2').css('width', '100%');
            $('.select2 .selection .select2-selection').css('height', '31px');
            $('.select2 .selection .select2-selection .select2-selection__rendered').css('margin-top', '-6px');
            $('.select2 .selection .select2-selection .select2-selection__arrow').css('margin-top', '4px');
        }

        ////////////////////////////////


        $('#month_and_year').on('keyup', () => $('#year_only').val(null));
        $('#year_only').on('keyup', () => $('#month_and_year').val(null));

        $(".datepicker_month_and_year").datepicker( {
            format: "yyyy-mm",
            viewMode: "months", 
            minViewMode: "months",
            autoclose: true
        }).on('change', () => $('#year_only').val(null));
        
        $(".datepicker_year_only").datepicker( {
            format: "yyyy",
            viewMode: "years", 
            minViewMode: "years",
            autoclose: true
        }).on('change', () => $('#month_and_year').val(null));
    </script>
@endsection