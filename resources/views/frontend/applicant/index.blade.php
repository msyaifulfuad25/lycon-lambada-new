<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Lycon Form Applicant</title>
    <link rel="icon" href="{{ asset('assets/lycon.jpeg') }}" type="image/icon type">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
    <!-- Datepicker -->
    <link rel= "stylesheet" href= "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
    <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css"> -->
    <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css"> -->
    <style>
        label.error{
            width: 100%;
            color: red;
            font-style: italic;
            margin-bottom: 5px;
        }
        .datepicker {
            padding-left: 12px;
        }
    </style>
</head>
<body style="background-image:url({{url('media/bg/view.jpg')}})">
    <div class="container mt-5 mb-5 pt-2 pb-2" style="background-color: white">
        <form id="form" method="POST" enctype="multipart/form-data">
            <fieldset>
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <div class="row mt-3">
                    <h1 class="col-md-12 bg-success text-light text-center">LYCON FORM APPLICANT</h1>
                </div>
                <div class="row mt-2" id="div-jobappliedfor">
                    <label for="jobappliedfor" class="col-sm-2 col-form-label">Pilih Posisi</label>
                    <div class="col-sm-10">
                        <select name="jobappliedfor" id="jobappliedfor" class="form-control col-md-6 select2">
                            <option value="" selected="">Pilih Posisi</option>
                            <option value="ADMIN OPERASIONAL">ADMIN OPERASIONAL</option>
                            <option value="ASSISTANT MANAGER IT">ASSISTANT MANAGER IT</option>
                            <option value="ASSISTEN MANAGER MARKETING">ASSISTEN MANAGER MARKETING</option>
                            <option value="BARISTA">BARISTA</option>
                            <option value="BRANCH MANAGER">BRANCH MANAGER</option>
                            <option value="CLEANER TECH">CLEANER TECH</option>
                            <option value="CLEANING SERVICE">CLEANING SERVICE</option>
                            <option value="COLLECTOR">COLLECTOR</option>
                            <option value="CONTENT SPECIALIST MARKETING">CONTENT SPECIALIST MARKETING</option>
                            <option value="COOK AND KITCHEN HELPER">COOK AND KITCHEN HELPER</option>
                            <option value="DRIVER">DRIVER</option>
                        </select>
                        <br>
                        <small class="text-danger error-text" id="error-jobappliedfor"></small>
                    </div>
                </div>
                <div class="row mt-2" id="div-firstname">
                    <label for="firstname" class="col-sm-2 col-form-label">Nama Depan</label>
                    <div class="col-sm-10">
                        <input type="text" name="firstname" class="form-control" id="firstname" placeholder="Nama Depan" required>
                        <small class="text-danger error-text" id="error-firstname"></small>
                    </div>
                </div>
                <div class="row mt-2" id="div-lastname">
                    <label for="lastname" class="col-sm-2 col-form-label">Nama Belakang</label>
                    <div class="col-sm-10">
                        <input type="text" name="lastname" class="form-control" id="lastname" placeholder="Nama Belakang">
                        <small class="text-danger error-text" id="error-lastname"></small>
                    </div>
                </div>
                <div class="row mt-2" id="div-gender">
                    <label for="gender" class="col-sm-2 col-form-label">Jenis Kelamin</label>
                    <div class="col-sm-10">
                        <select name="gender" class="form-control col-md-6 select2" id="gender">
                            <option value="">Pilh Jenis Kelamin</option>
                            <option value="Pria">Pria</option>
                            <option value="Wanita">Wanita</option>
                        </select>
                        <small class="text-danger error-text" id="error-gender"></small>
                    </div>
                </div>
                <div class="row mt-2" id="div-lasteducation">
                    <label class="col-sm-2 col-form-label">Pendidikan Terakhir</label>
                    <div class="col-sm-10">
                        <input type="radio" name="lasteducation" value="SD" id="lesd"> <label for="lesd">SD</label>
                        <input type="radio" name="lasteducation" value="SMP" id="lesmp"> <label for="lesmp">SMP</label>
                        <input type="radio" name="lasteducation" value="SMA" id="lesma"> <label for="lesma">SMA</label><br>
                        <input type="radio" name="lasteducation" value="SMK" id="lesmk"> <label for="lesmk">SMK</label><br>
                        <input type="radio" name="lasteducation" value="D1" id="led1"> <label for="led1">D1</label>
                        <input type="radio" name="lasteducation" value="D2" id="led2"> <label for="led2">D2</label>
                        <input type="radio" name="lasteducation" value="D3" id="led3"> <label for="led3">D3</label><br>
                        <input type="radio" name="lasteducation" value="D4" id="led4"> <label for="led4">D4</label>
                        <input type="radio" name="lasteducation" value="S1" id="les1"> <label for="les1">S1</label>
                        <input type="radio" name="lasteducation" value="S2" id="les2"> <label for="les2">S2</label>
                        <div id="lasteducationdetail">
                            <input type="text" name="major" class="form-control" id="major" placeholder="Jurusan">
                            <small class="text-danger error-text" id="error-major"></small>
                            <input type="text" name="college" class="form-control mt-2" id="college" placeholder="Sekolah/Universitas">
                            <small class="text-danger error-text" id="error-college"></small>
                        </div>
                        <br>
                        <small class="text-danger error-text" id="error-lasteducation"></small>
                    </div>
                </div>
                <div class="row mt-2" id="div-birthplace">
                    <label for="birthplace" class="col-sm-2 col-form-label">Tempat Lahir</label>
                    <div class="col-sm-10">
                        <input type="text" name="birthplace" class="form-control" id="birthplace" placeholder="Tempat Lahir">
                        <small class="text-danger error-text" id="error-birthplace"></small>
                    </div>
                </div>
                <div class="row mt-2" id="div-birthdate">
                    <label for="birthdate" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                    <div class="col-sm-10">
                        <input type="text" name="birthdate" class="form-control datepicker" id="birthdate" placeholder="Tanggal Lahir" autocomplete="off">
                        <small class="text-danger error-text" id="error-birthdate"></small>
                    </div>
                </div>
    
                <div class="row mt-2">
                    <label for="ktpaddress" class="col-sm-2 col-form-label">Alamat KTP</label>
                    <div class="col-sm-5" id="div-ktpaddress">
                        <input type="text" name="ktpaddress" class="form-control" id="ktpaddress" placeholder="Alamat">
                        <small class="text-danger error-text" id="error-ktpaddress"></small>
                    </div>
                    <div class="col-sm-5" id="div-ktpvillage">
                        <input type="text" name="ktpvillage" class="form-control" id="ktpvillage" placeholder="Kelurahan">
                        <small class="text-danger error-text" id="error-ktpvillage"></small>
                    </div>
                </div>
                <div class="row mt-2">
                    <label for="ktprt" class="col-sm-2 col-form-label"></label>
                    <div class="col-sm-5" id="div-ktprt">
                        <input type="text" name="ktprt" class="form-control" id="ktprt" placeholder="RT">
                        <small class="text-danger error-text" id="error-ktprt"></small>
                    </div>
                    <div class="col-sm-5" id="div-ktpdistrict">
                        <input type="text" name="ktpdistrict" class="form-control" id="ktpdistrict" placeholder="Kecamatan">
                        <small class="text-danger error-text" id="error-ktpdistrict"></small>
                    </div>
                </div>
                <div class="row mt-2">
                    <label for="ktprw" class="col-sm-2 col-form-label"></label>
                    <div class="col-sm-5" id="div-ktprw">
                        <input type="text" name="ktprw" class="form-control" id="ktprw" placeholder="RW">
                        <small class="text-danger error-text" id="error-ktprw"></small>
                    </div>
                    <div class="col-sm-5" id="div-ktpcity">
                        <input type="text" name="ktpcity" class="form-control" id="ktpcity" placeholder="Kota">
                        <small class="text-danger error-text" id="error-ktpcity"></small>
                    </div>
                </div>
                <div class="row mt-2">
                    <label for="ktpposcode" class="col-sm-2 col-form-label"></label>
                    <div class="col-sm-5" id="div-ktpposcode">
                        <input type="text" name="ktpposcode" class="form-control" id="ktpposcode" placeholder="Kode POS">
                        <small class="text-danger error-text" id="error-ktpposcode"></small>
                    </div>
                    <div class="col-sm-5" id="div-ktpprovince">
                        <input type="text" name="ktpprovince" class="form-control" id="ktpprovince" placeholder="Provinsi">
                        <small class="text-danger error-text" id="error-ktpprovince"></small>
                    </div>
                </div>
    
                <div class="row mt-2">
                    <label for="ifdomicileisktp" class="col-sm-2 col-form-label">
                        Domisili = KTP
                        <input type="checkbox" id="ifdomicileisktp">
                    </label>
                    <div class="col-sm-5" id="div-localaddress">
                        <input type="text" name="localaddress" class="form-control" id="localaddress" placeholder="Alamat">
                        <small class="text-danger error-text" id="error-localaddress"></small>
                    </div>
                    <div class="col-sm-5" id="div-localvillage">
                        <input type="text" name="localvillage" class="form-control" id="localvillage" placeholder="Kelurahan">
                        <small class="text-danger error-text" id="error-localvillage"></small>
                    </div>
                </div>
                <div class="row mt-2">
                    <label for="localrt" class="col-sm-2 col-form-label"></label>
                    <div class="col-sm-5" id="div-localrt">
                        <input type="text" name="localrt" class="form-control" id="localrt" placeholder="RT">
                        <small class="text-danger error-text" id="error-localrt"></small>
                    </div>
                    <div class="col-sm-5" id="div-localdistrict">
                        <input type="text" name="localdistrict" class="form-control" id="localdistrict" placeholder="Kecamatan">
                        <small class="text-danger error-text" id="error-localdistrict"></small>
                    </div>
                </div>
                <div class="row mt-2">
                    <label for="localrw" class="col-sm-2 col-form-label"></label>
                    <div class="col-sm-5" id="div-localrw">
                        <input type="text" name="localrw" class="form-control" id="localrw" placeholder="RW">
                        <small class="text-danger error-text" id="error-localrw"></small>
                    </div>
                    <div class="col-sm-5" id="div-localcity">
                        <input type="text" name="localcity" class="form-control" id="localcity" placeholder="Kota">
                        <small class="text-danger error-text" id="error-localcity"></small>
                    </div>
                </div>
                <div class="row mt-2">
                    <label for="localposcode" class="col-sm-2 col-form-label"></label>
                    <div class="col-sm-5" id="div-localposcode">
                        <input type="text" name="localposcode" class="form-control" id="localposcode" placeholder="Kode POS">
                        <small class="text-danger error-text" id="error-localposcode"></small>
                    </div>
                    <div class="col-sm-5" id="div-localprovince">
                        <input type="text" name="localprovince" class="form-control" id="localprovince" placeholder="Provinsi">
                        <small class="text-danger error-text" id="error-localprovince"></small>
                    </div>
                </div>
    
                <div class="row mt-2" id="div-phone">
                    <label for="phone" class="col-sm-2 col-form-label">Nomor Telepon</label>
                    <div class="col-sm-10">
                        <input type="text" name="phone" class="form-control" id="phone" placeholder="Nomor Telepon">
                        <small class="text-danger error-text" id="error-phone"></small>
                    </div>
                </div>
                <div class="row mt-2" id="div-whatsapp">
                    <label for="whatsapp" class="col-sm-2 col-form-label">Nomor Whatsapp</label>
                    <div class="col-sm-10">
                        <input type="text" name="whatsapp" class="form-control" id="whatsapp" placeholder="Nomor Whatsapp">
                        <small class="text-danger error-text" id="error-whatsapp"></small>
                    </div>
                </div>
                <div class="row mt-2" id="div-kknumber">
                    <label for="kknumber" class="col-sm-2 col-form-label">Nomor KK</label>
                    <div class="col-sm-10">
                        <input type="text" name="kknumber" class="form-control" id="kknumber" placeholder="Nomor KK">
                        <small class="text-danger error-text" id="error-kknumber"></small>
                    </div>
                </div>
                <div class="row mt-2" id="div-ktpnumber">
                    <label for="ktpnumber" class="col-sm-2 col-form-label">Nomor KTP</label>
                    <div class="col-sm-10">
                        <input type="text" name="ktpnumber" class="form-control" id="ktpnumber" placeholder="Nomor KTP">
                        <small class="text-danger error-text" id="error-ktpnumber"></small>
                    </div>
                </div>
                <div class="row mt-2" id="div-npwpnumber">
                    <label for="npwpnumber" class="col-sm-2 col-form-label">Nomor NPWP</label>
                    <div class="col-sm-10">
                        <input type="text" name="npwpnumber" class="form-control" id="npwpnumber" placeholder="Nomor NPWP">
                    </div>
                </div>
                <div class="row mt-2" id="div-blood">
                    <label for="blood" class="col-sm-2 col-form-label">Golongan Darah</label>
                    <div class="col-sm-10">
                        <select name="blood" id="blood" class="form-control col-md-6 select2">
                            <option value="">Pilih Golongan Darah</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="AB">AB</option>
                            <option value="O">O</option>
                        </select>
                    </div>
                </div>
                <div class="row mt-2" id="div-height">
                    <label for="height" class="col-sm-2 col-form-label">Tinggi Badan</label>
                    <div class="col-sm-10">
                        <input type="text" name="height" class="form-control" id="height" placeholder="Tinggi Badan">
                        <small class="text-danger error-text" id="error-height"></small>
                    </div>
                </div>
                <div class="row mt-2" id="div-weight">
                    <label for="weight" class="col-sm-2 col-form-label">Berat Badan</label>
                    <div class="col-sm-10">
                        <input type="text" name="weight" class="form-control" id="weight" placeholder="Berat Badan">
                        <small class="text-danger error-text" id="error-weight"></small>
                    </div>
                </div>
                <div class="row mt-2" id="div-status">
                    <label for="status" class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-10">
                        <select name="status" id="status" class="form-control col-md-6 select2">
                            <option value="">Pilih Status</option>
                            <option value="Lajang">Lajang</option>
                            <option value="Menikah">Menikah</option>
                            <option value="Duda">Duda</option>
                            <option value="Janda">Janda</option>
                        </select>
                        <br>
                        <small class="text-danger error-text" id="error-status"></small>
                    </div>
                </div>
                <div class="row mt-2" id="div-religion">
                    <label for="religion" class="col-sm-2 col-form-label">Agama</label>
                    <div class="col-sm-10">
                        <select name="religion" id="religion" class="form-control col-md-6 select2">
                            <option value="">Pilih Agama</option>
                            <option value="Islam">Islam</option>
                            <option value="Protestan">Protestan</option>
                            <option value="Katholik">Katholik</option>
                            <option value="Hindu">Hindu</option>
                            <option value="Budha">Budha</option>
                            <option value="Konghucu">Konghucu</option>
                            <option value="Lainnya">Lainnya</option>
                        </select>
                        <br>
                        <small class="text-danger error-text" id="error-religion"></small>
                    </div>
                </div>
                <div class="row mt-2" id="div-nationality">
                    <label for="nationality" class="col-sm-2 col-form-label">Kebangsaan</label>
                    <div class="col-sm-10">
                        <select name="nationality" id="nationality" class="form-control col-md-6 select2">
                            <option value="">Pilih Kebangsaan</option>
                            <option value="WNI">WNI</option>
                            <option value="WNA">WNA</option>
                        </select>
                        <br>
                        <small class="text-danger error-text" id="error-nationality"></small>
                    </div>
                </div>
                <div class="row mt-2" id="div-residence">
                    <label for="residence" class="col-sm-2 col-form-label">Tempat Tinggal</label>
                    <div class="col-sm-10">
                        <select name="residence" id="residence" class="form-control col-md-6 select2">
                            <option value="">Pilih Tempat Tinggal</option>
                            <option value="Sendiri">Sendiri</option>
                            <option value="Orang Tua">Orang Tua</option>
                            <option value="Kerabat">Kerabat</option>
                            <option value="Kontrak">Kontrak</option>
                            <option value="Kost">Kost</option>
                        </select>
                        <br>
                        <small class="text-danger error-text" id="error-residence"></small>
                    </div>
                </div>
                <div class="row mt-2" id="div-vehiclestatus">
                    <label for="vehicle" class="col-sm-2 col-form-label">Kendaraan</label>
                    <div class="col-sm-10">
                        <select name="vehiclestatus" id="vehiclestatus" class="form-control col-md-6 select2">
                            <option value="">Pilih Kendaraan</option>
                            <option value="Sendiri">Sendiri</option>
                            <option value="Umum">Umum</option>
                            <option value="Kantor">Kantor</option>
                            <option value="Lainnya">Lainnya</option>
                        </select>
                        <br>
                        <small class="text-danger error-text" id="error-vehiclestatus"></small>
                    </div>
                </div>
                <div class="row mt-2" id="div-vehicletype">
                    <label for="vehicletype" class="col-sm-2 col-form-label">Jenis Kendaraan</label>
                    <div class="col-sm-10">
                        <select name="vehicletype" id="vehicletype" class="form-control col-md-6 select2">
                            <option value="">Pilih Jenis Kendaraan</option>
                            <option value="Roda Dua">Roda Dua</option>
                            <option value="Roda Empat">Roda Empat</option>
                            <option value="Tidak Punya">Tidak Punya</option>
                        </select>
                        <br>
                        <small class="text-danger error-text" id="error-vehicletype"></small>
                    </div>
                </div>
                <div class="row mt-2" id="div-language">
                    <label class="col-sm-2 col-form-label">Bahasa</label>
                    <div class="col-sm-10">
                        <input type="checkbox" name="language[0]" value="indonesian" id="language-indonesian"><label for="language-indonesian">&nbsp;Indonesia</label>
                        <input type="checkbox" name="language[1]" value="england" id="language-england"><label for="language-england">&nbsp;Inggris</label>
                        <input type="checkbox" name="language[2]" value="mandarin" id="language-mandarin"><label for="language-mandarin">&nbsp;Mandarin</label>
                        <input type="checkbox" name="language[3]" value="etc" id="language-etc"><label for="language-etc">&nbsp;Lainnya</label>
                        <br>
                        <small class="text-danger error-text" id="error-language"></small>
                    </div>
                </div>
                <div class="row mt-2" id="div-sims">
                    <label class="col-sm-2 col-form-label">SIM</label>
                    <div class="col-sm-10">
                        <input type="checkbox" name="sima" id="sima"><label for="sima">&nbsp;A</label>
                        <input type="checkbox" name="simb1" id="simb1"><label for="simb1">&nbsp;B1</label>
                        <input type="checkbox" name="simb2" id="simb2"><label for="simb2">&nbsp;B2</label>
                        <input type="checkbox" name="simc" id="simc"><label for="simc">&nbsp;C</label>
                        <input type="checkbox" name="simd" id="simd"><label for="simd">&nbsp;D</label>
                        <br>
                        <small class="text-danger error-text" id="error-sim"></small>
                    </div>
                </div>
                <div class="row mt-2" id="div-phonebrand">
                    <label for="phonebrand" class="col-sm-2 col-form-label">Merk HP</label>
                    <div class="col-sm-10">
                        <input type="text" name="phonebrand" class="form-control" id="phonebrand" placeholder="Contoh: Iphone, Samsung, dll">
                    </div>
                </div>
                <div class="row mt-2" id="div-phonetype">
                    <label for="phonetype" class="col-sm-2 col-form-label">Tipe HP</label>
                    <div class="col-sm-10">
                        <input type="text" name="phonetype" class="form-control" id="phonetype" placeholder="Contoh: Iphone X, Samsung Note 8, dll">
                    </div>
                </div>
                <div class="row mt-2" id="div-clothessize">
                    <label for="clothessize" class="col-sm-2 col-form-label">Ukuran Baju</label>
                    <div class="col-sm-10">
                        <select name="clothessize" id="clothessize" class="form-control col-md-6 select2">
                            <option value="">Pilih Ukuran Baju</option>
                            <option value="S">S</option>
                            <option value="M">M</option>
                            <option value="L">L</option>
                            <option value="XL">XL</option>
                            <option value="XXL">XXL</option>
                            <option value="3L">3L</option>
                            <option value="4L">4L</option>
                        </select>
                    </div>
                </div>
                <div class="row mt-2" id="div-shoessize">
                    <label for="shoessize" class="col-sm-2 col-form-label">Ukuran Sepatu</label>
                    <div class="col-sm-10">
                        <input type="text" name="shoessize" class="form-control" id="shoessize" placeholder="Ukuran Sepatu">
                    </div>
                </div>
                <div class="row mt-2" id="div-email">
                    <label for="email" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                        <input type="text" name="email" class="form-control" id="email" placeholder="Email">
                        <small class="text-danger error-text" id="error-email"></small>
                    </div>
                </div>
    
                <br><br><hr>
                <h3>Akun Sosial Media</h3>
                <div class="row mt-2" id="div-facebook">
                    <label for="facebook" class="col-sm-2 col-form-label">Facebook</label>
                    <div class="col-sm-10">
                        <input type="text" name="facebook" class="form-control" id="facebook" placeholder="Facebook">
                    </div>
                </div>
                <div class="row mt-2" id="div-instagram">
                    <label for="instagram" class="col-sm-2 col-form-label">Instagram</label>
                    <div class="col-sm-10">
                        <input type="text" name="instagram" class="form-control" id="instagram" placeholder="Instagram">
                    </div>
                </div>
    
                <br><br><hr>
                <div class="row mt-2" id="div-photo">
                    <label for="photo" class="col-sm-2 col-form-label">Foto</label>
                    <div class="col-sm-10">
                        <input type="file" name="photo" class="form-control" id="photo">
                        <small for="">Max. 2 MB</small><br>
                        <small class="text-danger error-text" id="error-photo"></small>
                    </div>
                </div>
                
                <br><br><hr>
                <div class="row" id="div-emergency">
                    <div class="col-sm-6">
                        <h3>Orang yang dapat dihubungi dalam keadaan darurat (Tidak Seatap)</h3>
                        <small>Min. 1</small>
                    </div>
                    <div class="col-sm-6 text-right">
                        <button type="button" class="btn btn-danger btn-sm hidden" id="emergency-delete" style="display: none"><i class="fa fa-remove"></i> Hapus</button>
                        <button type="button" class="btn btn-primary btn-sm" id="emergency-create"><i class="fa fa-plus"></i> Tambah</button>
                    </div>
                </div>
                <div class="row m-0 p-0">
                    <table class="table table-condensed table-bordered _datatable col-sm-12">
                        <thead>
                            <tr>
                                <th>Status Hubungan</th>
                                <th>Nama Lengkap</th>
                                <th>Alamat Lengkap</th>
                                <th>Nomor Telepon</th>
                                <th>Nomor Whatsapp</th>
                            </tr>
                        </thead>
                        <tbody id="emergency-tbody">
                            <tr id="emergency-row-0">
                                <td>
                                    <input type="text" name="emergency[status][0]" class="form-control" id="emergency-row-0-status" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-emergency-row-0-status"></small>
                                </td>
                                <td>
                                    <input type="text" name="emergency[name][0]" class="form-control" id="emergency-row-0-name" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-emergency-row-0-name"></small>
                                </td>
                                <td>
                                    <input type="text" name="emergency[address][0]" class="form-control" id="emergency-row-0-address" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-emergency-row-0-address"></small></td>
                                <td>
                                    <input type="text" name="emergency[phone][0]" class="form-control" id="emergency-row-0-phone" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-emergency-row-0-phone"></small></td>
                                <td>
                                    <input type="text" name="emergency[whatsapp][0]" class="form-control" id="emergency-row-0-whatsapp" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-emergency-row-0-whatsapp"></small></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
    
                <br><br><hr>
                <div class="row" id="div-family">
                    <div class="col-sm-6">
                        <h3>Susunan Keluarga (Wajib Dilengkapi)</h3>
                        <small>Min. 3</small>
                    </div>
                    <div class="col-sm-6 text-right">
                        <button type="button" class="btn btn-danger btn-sm hidden" id="family-delete" style="display: none"><i class="fa fa-remove"></i> Hapus</button>
                        <button type="button" class="btn btn-primary btn-sm" id="family-create"><i class="fa fa-plus"></i> Tambah</button>
                    </div>
                </div>
                <div class="row m-0 p-0">
                    <table class="table table-condensed table-bordered _datatable col-sm-12">
                        <thead>
                            <tr>
                                <th class="text-center">Hubungan Keluarga</th>
                                <th class="text-center">Nama Lengkap</th>
                                <th class="text-center">Nomor Telepon</th>
                                <th class="text-center">Nomor Whatsapp</th>
                                <th class="text-center">Jenis Kelamin</th>
                                <th class="text-center">Tempat Lahir</th>
                                <th class="text-center">Tanggal Lahir</th>
                                <th class="text-center">Pendidikan</th>
                            </tr>
                        </thead>
                        <tbody id="family-tbody">
                            <tr id="family-row-0">
                                <td>
                                    <input type="text" name="family[status][0]" class="form-control" id="family-row-0-status" placeholder="..." value="Ayah" readonly/>
                                    <small class="text-danger error-text" id="error-family-row-0-status"></small>
                                </td>
                                <td>
                                    <input type="text" name="family[name][0]" class="form-control" id="family-row-0-name" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-family-row-0-name"></small>
                                </td>
                                <td>
                                    <input type="text" name="family[phone][0]" class="form-control" id="family-row-0-phone" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-family-row-0-phone"></small>
                                </td>
                                <td>
                                    <input type="text" name="family[whatsapp][0]" class="form-control" id="family-row-0-whatsapp" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-family-row-0-whatsapp"></small>
                                </td>
                                <td>
                                    <select name="family[gender][0]" class="form-control" id="family-row-0-gender" readonly>
                                        <option value="Pria" selected>Pria</option>
                                    </select>
                                    <small class="text-danger error-text" id="error-family-row-0-gender"></small>
                                </td>
                                <td>
                                    <input type="text" name="family[birthplace][0]" class="form-control" id="family-row-0-birthplace" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-family-row-0-birthplace"></small>
                                </td>
                                <td>
                                    <input type="text" name="family[birthdate][0]" class="form-control datepicker" id="family-row-0-birthdate" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-family-row-0-birthdate"></small>
                                </td>
                                <td>
                                    <input type="text" name="family[education][0]" class="form-control" id="family-row-0-education" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-family-row-0-education"></small>
                                </td>
                            </tr>
                            <tr id="family-row-1">
                                <td>
                                    <input type="text" name="family[status][1]" class="form-control" id="family-row-1-status" placeholder="..." value="Ibu" readonly/>
                                
                                    <small class="text-danger error-text" id="error-family-row-1-status"></small>
                                </td>
                                <td>
                                    <input type="text" name="family[name][1]" class="form-control" id="family-row-1-name" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-family-row-1-name"></small>
                                </td>
                                <td>
                                    <input type="text" name="family[phone][1]" class="form-control" id="family-row-1-phone" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-family-row-1-phone"></small>
                                </td>
                                <td>
                                    <input type="text" name="family[whatsapp][1]" class="form-control" id="family-row-1-whatsapp" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-family-row-1-whatsapp"></small>
                                </td>
                                <td>
                                    <select name="family[gender][1]" class="form-control" id="family-row-1-gender" readonly>
                                        <option value="Wanita" selected>Wanita</option>
                                    </select>
                                    <small class="text-danger error-text" id="error-family-row-1-gender"></small>
                                </td>
                                <td>
                                    <input type="text" name="family[birthplace][1]" class="form-control" id="family-row-1-birthplace" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-family-row-1-birthplace"></small>
                                </td>
                                <td>
                                    <input type="text" name="family[birthdate][1]" class="form-control datepicker" id="family-row-1-birthdate" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-family-row-1-birthdate"></small>
                                </td>
                                <td>
                                    <input type="text" name="family[education][1]" class="form-control" id="family-row-1-education" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-family-row-1-education"></small>
                                </td>
                            </tr>
                            <tr id="family-row-2">
                                <td>
                                    <div class="input-group mb-3">
                                        <div class="input-group-append">
                                            <span class="input-group-text">Anak Ke</span>
                                        </div>
                                        <input type="text" name="family[status][2]" class="form-control" id="family-row-2-status" placeholder="..."/>
                                    </div>
                                
                                    <small class="text-danger error-text" id="error-family-row-2-status"></small>
                                </td>
                                <td>
                                    <input type="text" name="family[name][2]" class="form-control" id="family-row-2-name" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-family-row-2-name"></small>
                                </td>
                                <td>
                                    <input type="text" name="family[phone][2]" class="form-control" id="family-row-2-phone" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-family-row-2-phone"></small>
                                </td>
                                <td>
                                    <input type="text" name="family[whatsapp][2]" class="form-control" id="family-row-2-whatsapp" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-family-row-2-whatsapp"></small>
                                </td>
                                <td>
                                    <select name="family[gender][2]" class="form-control" id="family-row-2-gender">
                                        <option value="">Pilih Jenis Kelamin</option>
                                        <option value="Pria">Pria</option>
                                        <option value="Wanita">Wanita</option>
                                    </select>
                                    <small class="text-danger error-text" id="error-family-row-2-gender"></small>
                                </td>
                                <td>
                                    <input type="text" name="family[birthplace][2]" class="form-control" id="family-row-2-birthplace" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-family-row-2-birthplace"></small>
                                </td>
                                <td>
                                    <input type="text" name="family[birthdate][2]" class="form-control datepicker" id="family-row-2-birthdate" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-family-row-2-birthdate"></small>
                                </td>
                                <td>
                                    <input type="text" name="family[education][2]" class="form-control" id="family-row-2-education" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-family-row-2-education"></small>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
    
                <br><br><hr>
                <div class="row" id="div-education">
                    <div class="col-sm-6">
                        <h3>Riwayat Pendidikan</h3>
                    </div>
                    <div class="col-sm-6 text-right">
                        <button type="button" class="btn btn-danger btn-sm hidden" id="education-delete" style="display: none"><i class="fa fa-remove"></i> Hapus</button>
                        <button type="button" class="btn btn-primary btn-sm" id="education-create"><i class="fa fa-plus"></i> Tambah</button>
                    </div>
                </div>
                <div class="row m-0 p-0">
                    <table class="table table-condensed table-bordered _datatable col-sm-12">
                        <thead>
                            <tr>
                                <th colspan="2">Periode Kelulusan</th>
                                <th rowspan="2">Sekolah/PT</th>
                                <th rowspan="2">Jurusan</th>
                                <th rowspan="2">IPK/NEM</th>
                                <th rowspan="2">Kota</th>
                                <th rowspan="2">Ijazah</th>
                            </tr>
                            <tr>
                                <th>Masuk</th>
                                <th>Lulus</th>
                            </tr>
                        </thead>
                        <tbody id="education-tbody">
                            <tr id="education-row-0">
                                <td>
                                    <input type="text" name="education[in][0]" class="form-control" id="education-row-0-in" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-education-row-0-in"></small></td>
                                <td>
                                    <input type="text" name="education[out][0]" class="form-control" id="education-row-0-out" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-education-row-0-out"></small></td>
                                <td>
                                    <input type="text" name="education[school][0]" class="form-control" id="education-row-0-school" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-education-row-0-school"></small></td>
                                <td>
                                    <input type="text" name="education[major][0]" class="form-control" id="education-row-0-major" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-education-row-0-major"></small></td>
                                <td>
                                    <input type="text" name="education[score][0]" class="form-control" id="education-row-0-score" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-education-row-0-score"></small></td>
                                <td>
                                    <input type="text" name="education[city][0]" class="form-control" id="education-row-0-city" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-education-row-0-city"></small></td>
                                <td>
                                    <input type="text" name="education[certificate][0]" class="form-control" id="education-row-0-certificate" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-education-row-0-certificate"></small></td>
                            </tr>
                            <tr id="education-row-1">
                                <td>
                                    <input type="text" name="education[in][1]" class="form-control" id="education-row-1-in" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-education-row-1-in"></small></td>
                                <td>
                                    <input type="text" name="education[out][1]" class="form-control" id="education-row-1-out" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-education-row-1-out"></small></td>
                                <td>
                                    <input type="text" name="education[school][1]" class="form-control" id="education-row-1-school" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-education-row-1-school"></small></td>
                                <td>
                                    <input type="text" name="education[major][1]" class="form-control" id="education-row-1-major" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-education-row-1-major"></small></td>
                                <td>
                                    <input type="text" name="education[score][1]" class="form-control" id="education-row-1-score" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-education-row-1-score"></small></td>
                                <td>
                                    <input type="text" name="education[city][1]" class="form-control" id="education-row-1-city" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-education-row-1-city"></small></td>
                                <td>
                                    <input type="text" name="education[certificate][1]" class="form-control" id="education-row-1-certificate" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-education-row-1-certificate"></small></td>
                            </tr>
                            <tr id="education-row-2">
                                <td>
                                    <input type="text" name="education[in][2]" class="form-control" id="education-row-2-in" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-education-row-2-in"></small></td>
                                <td>
                                    <input type="text" name="education[out][2]" class="form-control" id="education-row-2-out" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-education-row-2-out"></small></td>
                                <td>
                                    <input type="text" name="education[school][2]" class="form-control" id="education-row-2-school" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-education-row-2-school"></small></td>
                                <td>
                                    <input type="text" name="education[major][2]" class="form-control" id="education-row-2-major" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-education-row-2-major"></small></td>
                                <td>
                                    <input type="text" name="education[score][2]" class="form-control" id="education-row-2-score" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-education-row-2-score"></small></td>
                                <td>
                                    <input type="text" name="education[city][2]" class="form-control" id="education-row-2-city" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-education-row-2-city"></small></td>
                                <td>
                                    <input type="text" name="education[certificate][2]" class="form-control" id="education-row-2-certificate" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-education-row-2-certificate"></small></td>
                            </tr>
                            <tr id="education-row-3">
                                <td>
                                    <input type="text" name="education[in][3]" class="form-control" id="education-row-3-in" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-education-row-3-in"></small></td>
                                <td>
                                    <input type="text" name="education[out][3]" class="form-control" id="education-row-3-out" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-education-row-3-out"></small></td>
                                <td>
                                    <input type="text" name="education[school][3]" class="form-control" id="education-row-3-school" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-education-row-3-school"></small></td>
                                <td>
                                    <input type="text" name="education[major][3]" class="form-control" id="education-row-3-major" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-education-row-3-major"></small></td>
                                <td>
                                    <input type="text" name="education[score][3]" class="form-control" id="education-row-3-score" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-education-row-3-score"></small></td>
                                <td>
                                    <input type="text" name="education[city][3]" class="form-control" id="education-row-3-city" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-education-row-3-city"></small></td>
                                <td>
                                    <input type="text" name="education[certificate][3]" class="form-control" id="education-row-3-certificate" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-education-row-3-certificate"></small></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
    
                <br><br><hr>
                <h3>Apakah Pernah Mengikuti Training Sebelumnya?</h3>
                <input type="radio" name="training" id="training-yes"><label for="training-yes">&nbsp;Pernah</label>
                <input type="radio" name="training" id="training-no"><label for="training-no">&nbsp;Belum</label>
                <div id="training-div" style="display: none">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6 text-right">
                            <button type="button" class="btn btn-danger btn-sm hidden" id="training-delete" style="display: none"><i class="fa fa-remove"></i> Hapus</button>
                            <button type="button" class="btn btn-primary btn-sm" id="training-create"><i class="fa fa-plus"></i> Tambah</button>
                        </div>
                    </div>
                    <div class="row m-0 p-0">
                        <table class="table table-condensed table-bordered _datatable col-sm-12">
                            <thead>
                                <tr>
                                    <th>Nama Pelatihan/Kursus</th>
                                    <th>Kota</th>
                                    <th>Penyelenggara</th>
                                    <th>Tahun</th>
                                    <th>Sertifikat</th>
                                </tr>
                            </thead>
                            <tbody id="training-tbody">
                            </tbody>
                        </table>
                    </div>
                </div>
    
                <br><br><hr>
                <h3>Apakah Pernah Bekerja Sebelumnya?</h3>
                <input type="radio" name="worked" id="worked-yes"><label for="worked-yes">&nbsp;Pernah</label>
                <input type="radio" name="worked" id="worked-no"><label for="worked-no">&nbsp;Belum</label>
                <div id="worked-div" style="display: none">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6 text-right">
                            <button type="button" class="btn btn-danger btn-sm hidden" id="worked-delete" style="display: none"><i class="fa fa-remove"></i> Hapus</button>
                            <button type="button" class="btn btn-primary btn-sm" id="worked-create"><i class="fa fa-plus"></i> Tambah</button>
                        </div>
                    </div>
                    <div class="row m-0 p-0" id="worked-table">
                        <table class="table table-condensed table-bordered _datatable col-sm-12">
                            <thead>
                                <tr>
                                    <th colspan="2">Periode</th>
                                    <th rowspan="2">Nama Perusahaan</th>
                                    <th rowspan="2">Kota</th>
                                    <th rowspan="2">Jabatan</th>
                                    <th rowspan="2">Gaji</th>
                                    <th rowspan="2">Alasan Keluar</th>
                                </tr>
                                <tr>
                                    <th>Masuk</th>
                                    <th>Keluar</th>
                                </tr>
                            </thead>
                            <tbody id="worked-tbody">
                            </tbody>
                        </table>
                    </div>
                </div>
    
                <br><br><hr>
                <div class="row" id="div-positive">
                    <div class="col-sm-6">
                        <h3>Sebutkan sifat positif dan kelebihan dalam bekerja? (Minimal 3)</h3>
                    </div>
                    <div class="col-sm-6 text-right">
                        <button type="button" class="btn btn-danger btn-sm hidden" id="positive-delete" style="display: none"><i class="fa fa-remove"></i> Hapus</button>
                        <button type="button" class="btn btn-primary btn-sm" id="positive-create"><i class="fa fa-plus"></i> Tambah</button>
                    </div>
                </div>
                <div class="row m-0 p-0">
                    <table class="table table-condensed table-bordered _datatable col-sm-12">
                        <thead>
                            <tr>
                                <th class="text-center">No</th>
                                <th class="text-center">Kelebihan Anda</th>
                            </tr>
                        </thead>
                        <tbody id="positive-tbody">
                            <tr id="positive-row-0">
                                <td>1</td>
                                <td>
                                    <input type="text" name="positive[description][0]" class="form-control" id="positive-row-0-description" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-positive-row-0-description"></small>
                                </td>
                            </tr>
                            <tr id="positive-row-1">
                                <td>2</td>
                                <td>
                                    <input type="text" name="positive[description][1]" class="form-control" id="positive-row-1-description" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-positive-row-1-description"></small>
                                </td>
                            </tr>
                            <tr id="positive-row-2">
                                <td>3</td>
                                <td>
                                    <input type="text" name="positive[description][2]" class="form-control" id="positive-row-2-description" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-positive-row-2-description"></small>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
    
                <br><br><hr>
                <div class="row" id="div-negative">
                    <div class="col-sm-6">
                        <h3>Sebutkan sifat negatif dan kelemahan dalam bekerja? (Minimal 3)</h3>
                    </div>
                    <div class="col-sm-6 text-right">
                        <button type="button" class="btn btn-danger btn-sm hidden" id="negative-delete" style="display: none"><i class="fa fa-remove"></i> Hapus</button>
                        <button type="button" class="btn btn-primary btn-sm" id="negative-create"><i class="fa fa-plus"></i> Tambah</button>
                    </div>
                </div>
                <div class="row m-0 p-0">
                    <table class="table table-condensed table-bordered _datatable col-sm-12">
                        <thead>
                            <tr>
                                <th class="text-center">No</th>
                                <th class="text-center">Kelemahan Anda</th>
                            </tr>
                        </thead>
                        <tbody id="negative-tbody">
                            <tr id="negative-row-0">
                                <td>1</td>
                                <td>
                                    <input type="text" name="negative[description][0]" class="form-control" id="negative-row-0-description" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-negative-row-0-description"></small>
                                </td>
                            </tr>
                            <tr id="negative-row-1">
                                <td>2</td>
                                <td>
                                    <input type="text" name="negative[description][1]" class="form-control" id="negative-row-1-description" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-negative-row-1-description"></small>
                                </td>
                            </tr>
                            <tr id="negative-row-2">
                                <td>3</td>
                                <td>
                                    <input type="text" name="negative[description][2]" class="form-control" id="negative-row-2-description" placeholder="..."/>
                                    <small class="text-danger error-text" id="error-negative-row-2-description"></small>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
    
    
    
                <br><br>
                <div id="div-hobby">
                    <h3>Sebutkan Hobby Anda?</h3>
                    <textarea name="hobby" class="form-control" id="hobby" rows="3" placeholder="..."></textarea>
                    <small class="text-danger error-text" id="error-hobby"></small>
                </div>

    
                <br><br>
                <div id="div-resignfactor">
                    <h3>Faktor apa yang nantinya bisa membuat anda resign dari tempat kerja anda?</h3>
                    <textarea name="resignfactor" class="form-control" id="resignfactor" rows="3" placeholder="..."></textarea>
                    <small class="text-danger error-text" id="error-resignfactor"></small>
                </div>

    
                <br><br>
                <div id="div-lokerinfo">
                    <h3>Darimana anda mengetahui adanya lowongan pekerjaan di perusahaan ini ?</h3>
                    <textarea name="lokerinfo" class="form-control" id="lokerinfo" rows="3" placeholder="..."></textarea>
                    <small class="text-danger error-text" id="error-lokerinfo"></small>
                </div>

    
                <br><br>
                <div id="div-responsibility">
                    <h3>Bila anda diterima, apakah tugas dan tanggung jawab pekerjaan anda ?</h3>
                    <textarea name="responsibility" class="form-control" id="responsibility" rows="3" placeholder="..."></textarea>
                    <small class="text-danger error-text" id="error-responsibility"></small>
                </div>

    
                <br><br>
                <div id="div-appliedbefore">
                    <h3>Apakah anda pernah melamar di group perusahaan ini ? Bila "Ya", kapan dan menjabat sebagai apa ?</h3>
                    <textarea name="appliedbefore" class="form-control" id="appliedbefore" rows="3" placeholder="..."></textarea>
                    <small class="text-danger error-text" id="error-appliedbefore"></small>
                </div>

    
                <br><br>
                <div id="div-workingrelative">
                    <h3>Apakah anda mempunyai teman/sanak saudara yang bekerja di group perusahaan ini? Bila "Ya" , siapa dan menjabat sebagai apa ?</h3>
                    <textarea name="workingrelative" class="form-control" id="workingrelative" rows="3" placeholder="..."></textarea>
                    <small class="text-danger error-text" id="error-workingrelative"></small>
                </div>

    
                <br><br>
                <div id="div-seriouslyill">
                    <h3>Apakah anda pernah menderita sakit keras/kecelakaan berat/operasi ? Bila "Ya", sebutkan kapan dan nama penyakit tersebut ?</h3>
                    <textarea name="seriouslyill" class="form-control" id="seriouslyill" rows="3" placeholder="..."></textarea>
                    <small class="text-danger error-text" id="error-seriouslyill"></small>
                </div>

    
                <br><br>
                <div id="div-policeproblem">
                    <h3>Apakah anda pernah/sedang berurusan dengan kepolisian ? Bila "Ya", sebutkan kapan dan perkara apa ?</h3>
                    <textarea name="policeproblem" class="form-control" id="policeproblem" rows="3" placeholder="..."></textarea>
                    <small class="text-danger error-text" id="error-policeproblem"></small>
                </div>

    
                <br><br>
                <div id="div-placedready">
                    <h3>Apakah anda bersedia ditempatkan di luar kota/pulau baik saat ini ataupun di masa yang akan datang ?</h3>
                    <textarea name="placedready" class="form-control" id="placedready" rows="3" placeholder="..."></textarea>
                    <small class="text-danger error-text" id="error-placedready"></small>
                </div>

    
                <br><br>
                <div id="div-officialtravel">
                    <h3>Apakah anda bersedia melakukan perjalanan dinas ke luar kota ?</h3>
                    <textarea name="officialtravel" class="form-control" id="officialtravel" rows="3" placeholder="..."></textarea>
                    <small class="text-danger error-text" id="error-officialtravel"></small>
                </div>

    
                <br><br>
                <div id="div-workingdate">
                    <h3>Bilamana diterima, kapan anda bisa mulai bekerja ?</h3>
                    <textarea name="workingdate" class="form-control" id="workingdate" rows="3" placeholder="..."></textarea>
                    <small class="text-danger error-text" id="error-workingdate"></small>
                </div>

    
                <br><br>
                <div id="div-salary">
                    <h3>Bilamana diterima, berapa nominal gaji yang anda inginkan ( Rp ) ? Hanya angka</h3>
                    <textarea name="salary" class="form-control" id="salary" rows="3" placeholder="..."></textarea>
                    <small class="text-danger error-text" id="error-salary"></small>
                </div>

    
                <div class="mt-2">
                    <button type="button" class="btn btn-success" id="submit" onclick="save()"><i class="fa fa-save"></i> Submit</button>
                </div>
            </fieldset>
        </form>
    </div>
    
    <!-- JS -->
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <!-- Datepicker -->
    <script type= "text/javascript" src= "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <!-- <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script> -->
    <!-- Datatable -->
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <!-- Jquery Validation Pluggin -->
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/additional-methods.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/additional-methods.min.js"></script>
    <!-- Sweetalert -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    
    <script>
        function setDatepicker() {
            $('.datepicker').datepicker({
                format: 'dd-M-yyyy',
                autoclose: true,
                locale: 'id'
            });
        }

        function resetForm() {
            $('#form')[0].reset();

            // Last Education
            $('#lesd').attr('checked', false);
            $('#lesmp').attr('checked', false);
            $('#lesma').attr('checked', false);
            $('#lesmk').attr('checked', false);
            $('#led1').attr('checked', false);
            $('#led2').attr('checked', false);
            $('#led3').attr('checked', false);
            $('#led4').attr('checked', false);
            $('#les1').attr('checked', false);
            $('#les2').attr('checked', false);

            // Language
            $('#language-indonesian').attr('checked', false);
            $('#language-england').attr('checked', false);
            $('#language-mandarin').attr('checked', false);
            $('#language-etc').attr('checked', false);
            
            // SIM
            $('#sima').attr('checked', false);
            $('#simb1').attr('checked', false);
            $('#simb2').attr('checked', false);
            $('#simc').attr('checked', false);
            $('#simd').attr('checked', false);

            // All
            $('#jobappliedfor').val('').trigger('change');
            $('#gender').val('').trigger('change');
            $('#blood').val('').trigger('change');
            $('#status').val('').trigger('change');
            $('#religion').val('').trigger('change');
            $('#nationality').val('').trigger('change');
            $('#residence').val('').trigger('change');
            $('#vehiclestatus').val('').trigger('change');
            $('#vehiclestype').val('').trigger('change');
            $('#clothessize').val('').trigger('change');
        }

        function save() {
            $('#submit').html('<i class="fa fa-spinner"></i> Processing');
            $('#submit').addClass('disabled');
            
            
            // alert($('#token').val())
            let form = new FormData();
            form.append('photo', $('#photo')[0].files[0]);
            
            $.each($('#form').serializeArray(), function(k, v) {
                form.append(v.name, v.value);
            });
            
            // console.log(form);
            // return false;
            
            // console.log(form);

            // console.log($('#form').serializeArray());
            $.ajax({
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/applicant/save',
                contentType: false,
                processData: false,
                data: form,
                // berforeSend: function() {
                // },
                success: function(res) {
                    scrolled = false;
                    $(document).find('small.error-text').text('');
                    if (!res.success) {
                        $.each(res.errors, function(k, v) {
                            $(`small#error-${k}`).html(`<i class="fa fa-warning"></i> ${v[0]}`);
                            // $(`#${k}`).addClass('is-invalid')
                            if (!scrolled) {
                                // $(window).scrollTop($(`#error-${k}`).position().top);
                                let idscrolltop = '';
                                if (k.search('emergency-row') == 0) {
                                    idscrolltop = '#div-emergency';
                                } else if (k.search('family-row') == 0) {
                                    idscrolltop = '#div-family';
                                } else if (k.search('education-row') == 0) {
                                    idscrolltop = '#div-education';
                                } else if (k.search('positive-row') == 0) {
                                    idscrolltop = '#div-positive';
                                } else if (k.search('negative-row') == 0) {
                                    idscrolltop = '#div-negative';
                                } else {
                                    idscrolltop = `#div-${k}`;
                                }
                                // alert(k.search('emergency-row'))
                                // alert(idscrolltop)

                                $('html, body').animate({scrollTop:$(idscrolltop).position().top}, 'slow');
                                scrolled = true;
                            }
                        });
                    } else {
                        swal("Success", res.message, "success");
                        resetForm();
                    }
                    $('#submit').html('<i class="fa fa-save"></i> Submit');
                    $('#submit').removeClass('disabled');
                    // $.notify({
                    //     message: response.message 
                    // },{
                    //     type: 'success'
                    // });
                    // // if (statusissued) {
                    // //     SNST(noid, 2, 'ISSUED')
                    // // }
                    // socket.emit('sendNotifToServer', true);
                    // getDatatable()
                    // hideModalSNST();
                    // cancelData()
                },
                // error: function(XMLHttpRequest, textStatus, errorThrown) { 
                //     alert('Unstable connection!');
                //     hideLoading();
                //     return;
                // }
            });
        }

        $(document).ready(function() {
            setDatepicker();
            $('.select2').select2({
                // theme: 'bootstrap',
            });
            $('.datatable').DataTable({
                "scrollX": true
            });

            // $('#form').validate({
            //     rules: {
            //         jobappliedfor: {
            //             required: true
            //         },
            //         firstname: {
            //             required: true
            //         },
            //         lastname: {
            //             required: true
            //         },
            //         lasteducation: {
            //             required: true
            //         },
            //         birthplace: {
            //             required: true
            //         },
            //         birthdate: {
            //             required: true
            //         },
            //         kknumber: {
            //             required: true
            //         },
            //         ktpnumber: {
            //             required: true
            //         },
            //         status: {
            //             required: true
            //         },
            //         religion: {
            //             required: true
            //         },
            //         nationality: {
            //             required: true
            //         },
            //         residence: {
            //             required: true
            //         },
            //         email: {
            //             required: true
            //         },
            //     },
            //     messages: {
            //         jobappliedfor: {
            //             required: 'Posisi harus diisi'
            //         },
            //         firstname: {
            //             required: 'Nama Depan harus diisi'
            //         },
            //         lastname: {
            //             required: 'Nama Belakang harus diisi.'
            //         },
            //         lasteducation: {
            //             required: 'Pendidikan Terakhir harus diisi.'
            //         },
            //         birthplace: {
            //             required: 'Tempat Lahir harus diisi.'
            //         },
            //         birthdate: {
            //             required: 'Tanggal Lahir harus diisi.'
            //         },
            //         kknumber: {
            //             required: 'Nomor KK harus diisi.'
            //         },
            //         ktpnumber: {
            //             required: 'Nomor KTP harus diisi.'
            //         },
            //         status: {
            //             required: 'Status harus diisi.'
            //         },
            //         religion: {
            //             required: 'Agama harus diisi.'
            //         },
            //         nationality: {
            //             required: 'Kebangsaan harus diisi.'
            //         },
            //         residence: {
            //             required: 'Tempat Tinggal harus diisi.'
            //         },
            //         email: {
            //             required: 'Nama harus diisi.'
            //         },
            //     },
            //     highlight:function(element){
            //         $(element).closest('.form-group');
            //         $(element).addClass('has-error');
            //     },
            //     unhighlight:function(element){
            //         $(element).closest('.form-group');
            //         $(element).removeClass('has-error');
            //     },
            //     submitHandler: save()
            // });

            // LAST EDUCATION
            $('#lasteducationdetail').hide();
            $('#lesd').on('change', function() { $('#lasteducationdetail').hide(); })
            $('#lesmp').on('change', function() { $('#lasteducationdetail').hide(); })
            $('#lesma').on('change', function() { $('#lasteducationdetail').hide(); })
            $('#lesmk').on('change', function() { $('#lasteducationdetail').show(); })
            $('#led1').on('change', function() { $('#lasteducationdetail').show(); })
            $('#led2').on('change', function() { $('#lasteducationdetail').show(); })
            $('#led3').on('change', function() { $('#lasteducationdetail').show(); })
            $('#led4').on('change', function() { $('#lasteducationdetail').show(); })
            $('#les1').on('change', function() { $('#lasteducationdetail').show(); })
            $('#les2').on('change', function() { $('#lasteducationdetail').show(); })

            // ADDRESS
            $('#ifdomicileisktp').on('change', function() {
                if ($('#ifdomicileisktp').is(':checked')) {
                    $('#localaddress').val($('#ktpaddress').val());
                    $('#localrt').val($('#ktprt').val());
                    $('#localrw').val($('#ktprw').val());
                    $('#localposcode').val($('#ktpposcode').val());
                    $('#localvillage').val($('#ktpvillage').val());
                    $('#localdistrict').val($('#ktpdistrict').val());
                    $('#localcity').val($('#ktpcity').val());
                    $('#localprovince').val($('#ktpprovince').val());
                } else {
                    $('#localaddress').val('');
                    $('#localrt').val('');
                    $('#localrw').val('');
                    $('#localposcode').val('');
                    $('#localvillage').val('');
                    $('#localdistrict').val('');
                    $('#localcity').val('');
                    $('#localprovince').val('');
                }
            })

            // EMERGENCY
            emergency_row = 1;
            $('#emergency-create').on('click', function() {
                emergency_tbody = `<tr id="emergency-row-${emergency_row}">
                        <td><input type="text" name="emergency[status][${emergency_row}]" class="form-control" id="emergency-row-${emergency_row}-status" placeholder="..."/></td>
                        <td><input type="text" name="emergency[name][${emergency_row}]" class="form-control" id="emergency-row-${emergency_row}-name" placeholder="..."/></td>
                        <td><input type="text" name="emergency[address][${emergency_row}]" class="form-control" id="emergency-row-${emergency_row}-address" placeholder="..."/></td>
                        <td><input type="text" name="emergency[phone][${emergency_row}]" class="form-control" id="emergency-row-${emergency_row}-phone" placeholder="..."/></td>
                        <td><input type="text" name="emergency[whatsapp][${emergency_row}]" class="form-control" id="emergency-row-${emergency_row}-whatsapp" placeholder="..."/></td>
                    </tr>
                `;
                $('#emergency-tbody').append(emergency_tbody);
                $('#emergency-delete').show();
                emergency_row += 1;
            });
            $('#emergency-delete').on('click', function() {
                $(`#emergency-row-${emergency_row-1}`).remove();
                if (emergency_row > 0) { emergency_row -= 1 }
                if (emergency_row <= 1) { $('#emergency-delete').hide() }
            });

            // FAMILY
            family_row = 3;
            $('#family-create').on('click', function() {
                family_tbody = `<tr id="family-row-${family_row}">
                    <td><input type="text" name="family[status][${family_row}]" class="form-control" id="family-row-${family_row}-status" placeholder="..."/></td>
                    <td><input type="text" name="family[name][${family_row}]" class="form-control" id="family-row-${family_row}-name" placeholder="..."/></td>
                    <td><input type="text" name="family[phone][${family_row}]" class="form-control" id="family-row-${family_row}-phone" placeholder="..."/></td>
                    <td><input type="text" name="family[whatsapp][${family_row}]" class="form-control" id="family-row-${family_row}-whatsapp" placeholder="..."/></td>
                    <td>
                        <select name="family[gender][${family_row}]" class="form-control" id="family-row-${family_row}-gender">
                            <option value="">Pilih Jenis Kelamin</option>
                            <option value="Pria">Pria</option>
                            <option value="Wanita">Wanita</option>
                        </select>
                    </td>
                    <td><input type="text" name="family[birthplace][${family_row}]" class="form-control" id="family-row-${family_row}-birthplace" placeholder="..."/></td>
                    <td><input type="text" name="family[birthdate][${family_row}]" class="form-control datepicker" id="family-row-${family_row}-birthdate" placeholder="..."/></td>
                    <td><input type="text" name="family[education][${family_row}]" class="form-control" id="family-row-${family_row}-education" placeholder="..."/></td>
                </tr>
                `;
                $('#family-tbody').append(family_tbody);
                setDatepicker();
                $('#family-delete').show();
                family_row += 1;
            });
            $('#family-delete').on('click', function() {
                $(`#family-row-${family_row-1}`).remove();
                if (family_row > 0) { family_row -= 1 }
                if (family_row <= 3) { $('#family-delete').hide() }
            });

            // EDUCATION
            education_row = 4;
            $('#education-create').on('click', function() {
                education_tbody = `<tr id="education-row-${education_row}">
                    <td><input type="text" name="education[in][${education_row}]" class="form-control" id="education-row-${education_row}-in" placeholder="..."/></td>
                    <td><input type="text" name="education[out][${education_row}]" class="form-control" id="education-row-${education_row}-out" placeholder="..."/></td>
                    <td><input type="text" name="education[school][${education_row}]" class="form-control" id="education-row-${education_row}-school" placeholder="..."/></td>
                    <td><input type="text" name="education[major][${education_row}]" class="form-control" id="education-row-${education_row}-major" placeholder="..."/></td>
                    <td><input type="text" name="education[score][${education_row}]" class="form-control" id="education-row-${education_row}-score" placeholder="..."/></td>
                    <td><input type="text" name="education[city][${education_row}]" class="form-control" id="education-row-${education_row}-city" placeholder="..."/></td>
                    <td><input type="text" name="education[certificate][${education_row}]" class="form-control" id="education-row-${education_row}-certificate" placeholder="..."/></td>
                </tr>
                `;
                $('#education-tbody').append(education_tbody);
                $('#education-delete').show();
                education_row += 1;
            });
            $('#education-delete').on('click', function() {
                $(`#education-row-${education_row-1}`).remove();
                if (education_row > 0) { education_row -= 1 }
                if (education_row <= 4) { $('#education-delete').hide() }
            });

            // TRAINING
            $('#training-yes').on('click', function() { $('#training-div').show() });
            $('#training-no').on('click', function() { $('#training-div').hide() });

                // TRAINING TABLE
                training_row = 0;
                $('#training-create').on('click', function() {
                    training_tbody = `<tr id="training-row-${training_row}">
                    <td><input type="text" name="training[name][${training_row}]" class="form-control" id="training-row-${training_row}-name" placeholder="..."/></td>
                    <td><input type="text" name="training[city][${training_row}]" class="form-control" id="training-row-${training_row}-city" placeholder="..."/></td>
                    <td><input type="text" name="training[organizer][${training_row}]" class="form-control" id="training-row-${training_row}-organizer" placeholder="..."/></td>
                    <td><input type="text" name="training[year][${training_row}]" class="form-control" id="training-row-${training_row}-year" placeholder="..."/></td>
                    <td><input type="text" name="training[certificate][${training_row}]" class="form-control" id="training-row-${training_row}-certificate" placeholder="..."/></td>
                    </tr>`;
                    $('#training-tbody').append(training_tbody);
                    $('#training-delete').show();
                    training_row += 1;
                });
                $('#training-delete').on('click', function() {
                    $(`#training-row-${training_row-1}`).remove();
                    if (training_row > 0) { training_row -= 1 }
                    if (training_row == 0) { $('#training-delete').hide() }
                });

            // WORKED
            $('#worked-yes').on('click', function() { $('#worked-div').show() });
            $('#worked-no').on('click', function() { $('#worked-div').hide() });

                // WORKED TABLE
                worked_row = 0;
                $('#worked-create').on('click', function() {
                    worked_tbody = `<tr id="worked_row_${worked_row}">
                        <td><input type="text" name="worked[in][${worked_row}]" class="form-control" id="worked-row-${worked_row}-in" placeholder="..."/></td>
                        <td><input type="text" name="worked[out][${worked_row}]" class="form-control" id="worked-row-${worked_row}-out" placeholder="..."/></td>
                        <td><input type="text" name="worked[company][${worked_row}]" class="form-control" id="worked-row-${worked_row}-company" placeholder="..."/></td>
                        <td><input type="text" name="worked[city][${worked_row}]" class="form-control" id="worked-row-${worked_row}-city" placeholder="..."/></td>
                        <td><input type="text" name="worked[position][${worked_row}]" class="form-control" id="worked-row-${worked_row}-position" placeholder="..."/></td>
                        <td><input type="text" name="worked[salary][${worked_row}]" class="form-control" id="worked-row-${worked_row}-salary" placeholder="..."/></td>
                        <td><input type="text" name="worked[stopreason][${worked_row}]" class="form-control" id="worked-row-${worked_row}-stopreason" placeholder="..."/></td>
                    </tr>`;
                    $('#worked-tbody').append(worked_tbody);
                    $('#worked-delete').show();
                    worked_row += 1;
                });
                $('#worked-delete').on('click', function() {
                    $(`#worked_row_${worked_row-1}`).remove();
                    if (worked_row > 0) { worked_row -= 1 }
                    if (worked_row == 0) { $('#worked-delete').hide() }
                });
            
            // POSITIF
            positive_row = 3;
            $('#positive-create').on('click', function() {
                positive_tbody = `<tr id="positive-row-${positive_row}">
                    <td>${positive_row+1}</td>
                    <td><input type="text" name="positive[description][${positive_row}]" class="form-control" id="positive-row-${positive_row}-description" placeholder="..."/></td>
                </tr>`;
                $('#positive-tbody').append(positive_tbody);
                $('#positive-delete').show();
                positive_row += 1;
            });
            $('#positive-delete').on('click', function() {
                $(`#positive-row-${positive_row-1}`).remove();
                if (positive_row > 0) { positive_row -= 1 }
                if (positive_row <= 3) { $('#positive-delete').hide() }
            });

            // NEGATIF
            negative_row = 3;
            $('#negative-create').on('click', function() {
                negative_tbody = `<tr id="negative-row-${negative_row}">
                    <td>${negative_row+1}</td>
                    <td><input type="text" name="negative[description][${negative_row}]" class="form-control" id="negative-row-${negative_row}-description" placeholder="..."/></td>
                </tr>`;
                $('#negative-tbody').append(negative_tbody);
                $('#negative-delete').show();
                negative_row += 1;
            });
            $('#negative-delete').on('click', function() {
                $(`#negative-row-${negative_row-1}`).remove();
                if (negative_row > 0) { negative_row -= 1 }
                if (negative_row <= 3) { $('#negative-delete').hide() }
            });


            // // DUMMY DATA
            // $('#jobappliedfor').val('ADMIN OPERASIONAL').trigger('change');
            // $('#firstname').val('Muhammad Syaiful');
            // $('#lastname').val('Fuad');
            // $('#gender').val('Pria').trigger('change');
            // $('#lesma').trigger('change');
            // $('#lesma').attr('checked', true);
            // $('#birthplace').val('Tulunggung');
            // $('#birthdate').val('25-03-1992');
            // $('#ktpaddress').val('Gondangsari');
            // $('#ktprt').val('4');
            // $('#ktprw').val('2');
            // $('#ktpposcode').val('66291');
            // $('#ktpvillage').val('Jabalsari');
            // $('#ktpdistrict').val('Sumbergempol');
            // $('#ktpcity').val('Tulungagung');
            // $('#ktpprovince').val('Jawa Timur');
            // $('#localaddress').val('Gondangsari');
            // $('#localrt').val('4');
            // $('#localrw').val('2');
            // $('#localposcode').val('66291');
            // $('#localvillage').val('Jabalsari');
            // $('#localdistrict').val('Sumbergempol');
            // $('#localcity').val('Tulungagung');
            // $('#localprovince').val('Jawa Timur');
            // $('#phone').val('085772567156');
            // $('#whatsapp').val('085772567156');
            // $('#kknumber').val('3504102503980002');
            // $('#ktpnumber').val('3504102503980001');
            // $('#npwpnumber').val('3504102503980003');
            // $('#blood').val('B').trigger('change');
            // $('#height').val('168');
            // $('#weight').val('78');
            // $('#status').val('Menikah').trigger('change');
            // $('#religion').val('Islam').trigger('change');
            // $('#nationality').val('WNI').trigger('change');
            // $('#residence').val('Kontrak').trigger('change');
            // $('#vehiclestatus').val('Sendiri').trigger('change');
            // $('#vehicletype').val('Roda Dua').trigger('change');
            // $('#language-indonesian').attr('checked', true);
            // $('#language-mandarin').attr('checked', true);
            // $('#sima').attr('checked', true);
            // $('#simb1').attr('checked', true);
            // $('#phonebrand').val('Xiaomi');
            // $('#phonetype').val('Xiaomi Note 8');
            // $('#clothessize').val('L').trigger('change');
            // $('#shoessize').val('34');
            // $('#email').val('msyaifulfuad25@gmail.com');
            // $('#facebook').val('Muhammad Syaiful Fuad');
            // $('#instagram').val('syaifulfu');
            // // $('#photo').val('syaifulfu.jpg');
            
            // // Emergency
            // $('#emergency-row-0-status').val('Ayah');
            // $('#emergency-row-0-name').val('Imam Kateni');
            // $('#emergency-row-0-address').val('Tulungagung');
            // $('#emergency-row-0-phone').val('081222333444');
            // $('#emergency-row-0-whatsapp').val('081222333444');
            // $('#emergency-row-1-status').val('Ibu');
            // $('#emergency-row-1-name').val('Tarwiyatin');
            // $('#emergency-row-1-address').val('Tulungagung');
            // $('#emergency-row-1-phone').val('081222333555');
            // $('#emergency-row-1-whatsapp').val('081222333555');

            // // Family
            // $('#family-row-0-status').val('Ayah');
            // $('#family-row-0-name').val('Imam Kateni');
            // $('#family-row-0-phone').val('081222333444');
            // $('#family-row-0-whatsapp').val('081222333444');
            // // $('#family-row-0-gender').val('L');
            // $('#family-row-0-birthplace').val('Tulungagung');
            // $('#family-row-0-birthdate').val('25/03/1955');
            // $('#family-row-0-education').val('MI');
            // $('#family-row-1-status').val('Ibu');
            // $('#family-row-1-name').val('Imam Tarwiyatin');
            // $('#family-row-1-phone').val('081222333555');
            // $('#family-row-1-whatsapp').val('081222333555');
            // // $('#family-row-1-gender').val('female');
            // $('#family-row-1-birthplace').val('Tulungagung');
            // $('#family-row-1-birthdate').val('25/03/1966');
            // $('#family-row-1-education').val('SD');
            // $('#family-row-2-status').val('2');
            // $('#family-row-2-name').val('Muhammad Syaiful Fuad');
            // $('#family-row-2-phone').val('085772567156');
            // $('#family-row-2-whatsapp').val('085772567156');
            // $('#family-row-2-gender').val('Pria');
            // $('#family-row-2-birthplace').val('Tulungagung');
            // $('#family-row-2-birthdate').val('25/03/1966');
            // $('#family-row-2-education').val('D1');

            // // Education
            // $('#education-row-0-in').val('2016');
            // $('#education-row-0-out').val('2019');
            // $('#education-row-0-school').val('Politeknik Kota Malang');
            // $('#education-row-0-major').val('Teknik Informatika');
            // $('#education-row-0-score').val('3.6');
            // $('#education-row-0-city').val('Malang');
            // $('#education-row-0-certificate').val('Ya');
            // $('#education-row-1-in').val('2016');
            // $('#education-row-1-out').val('2019');
            // $('#education-row-1-school').val('Politeknik Kota Malang');
            // $('#education-row-1-major').val('Teknik Informatika');
            // $('#education-row-1-score').val('3.6');
            // $('#education-row-1-city').val('Malang');
            // $('#education-row-1-certificate').val('Tidak');

            // // Training
            // $('#training-create').trigger('click');
            // $('#training-row-0-name').val('Pelatihan Flutter');
            // $('#training-row-0-city').val('Bekasi');
            // $('#training-row-0-organizer').val('BLK Bekasi');
            // $('#training-row-0-year').val('2020');
            // $('#training-row-0-certificate').val('Tidak');
            // $('#training-row-1-name').val('Pelatihan Flutter');
            // $('#training-row-1-city').val('Bekasi');
            // $('#training-row-1-organizer').val('BLK Bekasi');
            // $('#training-row-1-year').val('2020');
            // $('#training-row-1-certificate').val('Ya');

            // // Worked
            // $('#worked-yes').trigger('click');
            // $('#worked-create').trigger('click');
            // $('#worked-row-0-in').val('2019');
            // $('#worked-row-0-out').val('2020');
            // $('#worked-row-0-company').val('GMF');
            // $('#worked-row-0-city').val('Tangerang');
            // $('#worked-row-0-position').val('Programmer');
            // $('#worked-row-0-salary').val('8jt');
            // $('#worked-row-0-stopreason').val('Covid');
            // $('#worked-create').trigger('click');
            // $('#worked-row-1-in').val('2019');
            // $('#worked-row-1-out').val('2020');
            // $('#worked-row-1-company').val('GMF');
            // $('#worked-row-1-city').val('Tangerang');
            // $('#worked-row-1-position').val('Programmer');
            // $('#worked-row-1-salary').val('8jt');
            // $('#worked-row-1-stopreason').val('Corona');

            // // Positive
            // $('#positive-row-0-description').val('Memahami Web & Mobile Programming');
            // $('#positive-row-1-description').val('Memahami Web & Mobile Programming 2');
            // $('#positive-row-2-description').val('Memahami Web & Mobile Programming 3');

            // // Negative
            // $('#negative-row-0-description').val('Malas');
            // $('#negative-row-1-description').val('Malas 2');
            // $('#negative-row-2-description').val('Malas 3');

            // $('#hobby').val('Musik');
            // $('#resignfactor').val('Kenyamanan Kerja');
            // $('#lokerinfo').val('Instagram');
            // $('#responsibility').val('Mengerjakan sesuai jobdesk');
            // $('#appliedbefore').val('Belum');
            // $('#workingrelative').val('Tidak');
            // $('#seriouslyill').val('Tidak');
            // $('#policeproblem').val('Tidak');
            // $('#placedready').val('Ya');
            // $('#officialtravel').val('Ya');
            // $('#workingdate').val('1-12-2022');
            // $('#salary').val('10jt');


        });
    </script>
</body>
</html>