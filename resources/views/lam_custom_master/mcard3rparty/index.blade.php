{{-- Extends layout --}}

@extends('lam_custom_layouts.default')

@section('styles')
    <!-- <link href="{{ asset('css/component.css') }}" rel="stylesheet" type="text/css"/> -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet"></link>
    <link href="//www.amcharts.com/lib/3/plugins/export/export.css?v=7.0.6" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.0.2/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.0.2/css/bootstrap-datetimepicker.css">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/tabel.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/myDatatable.css') }}" rel="stylesheet" type="text/css"/>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
    <style media="screen">
        .container {
            display: none
        }

        .dataTable thead .sorting_asc,
        .dataTable thead .sorting_desc,
        .dataTable thead .sorting_disabled,
        .dataTable thead .sorting {
            padding: 3px 3px 3px 3px !important;
            /* color: black !important; */
        }

        .dataTable thead th {
            text-align: center;
        }
        
        .dataTable tbody td,
        .dataTable tbody td:hover,
        .dataTable tbody tr:hover {
            height: 26px;
            padding: 3px 3px 3px 3px !important;
        }

        .dataTable thead tr{
            height: 40px !important;
        }

        .dataTable tbody tr{
            height: 0px !important;
        }

        /* .my_row { */
            /* height: 35px; */
        /* } */

        /* .my_row:hover { */
            /* display: none; */
            /* padding: 0px 0px 0px 0px !important; */
        /* } */

        .table-bordered th,
        .table-bordered td,
        .table-bordered tfoot th {
            border: 1px solid #95c9ed
        }

        .dataTables_wrapper {
            margin-top: -1px !important;
            background: #3598dc !important;
        }
        
        #table_master {
            /* width:2500px !important; */
            border: 0;
        }

        /* tbody tr td {
            padding: 0px 5px 0px 5px !important;f
            width: 100px !important;
        } */

        /* .dataTable thead th:nth-child(10) {
            width: 140px !important;
        } */

        /* .dataTable tbody tr td:nth-child(10) {
            width: 140px !important;
        } */


        /* tbody:nth-child(odd) tr td:nth-child(odd) {
            width: 100px !important;
        } */

        #btn-dropdown-info button::after {
            color: white;
        }

        #pagepanel:fullscreen {
            overflow: scroll !important;
        }
        #pagepanel:-ms-fullscreen {
            overflow: scroll !important;
        }
        #pagepanel:-webkit-full-screen {
            overflow: scroll !important;
        }
        #pagepanel:-moz-full-screen {
            overflow: scroll !important;
        }

        .modal-detail {
            min-width: 950px;
        }

        table.dataTable tfoot th {
            padding: 10px 3px 10px 3px !important;
        }

        .tfoot-kode {
            text-align: right;
        }

        #table-detail #thead-qty,
        #table-detail #thead-price,
        #table-detail #thead-total {
            text-align: center !important;
        }
        
        #th-action {
            width: 60px !important;
        }

        #table-log {
            width: 1104px !important;
        }

        td.milestones-control {
            background: url('https://cdn.rawgit.com/DataTables/DataTables/6c7ada53ebc228ea9bc28b1b216e793b1825d188/examples/resources/details_open.png') no-repeat center center;
            cursor: pointer;
        }
        tr.shown td.milestones-control {
            background: url('https://cdn.rawgit.com/DataTables/DataTables/6c7ada53ebc228ea9bc28b1b216e793b1825d188/examples/resources/details_close.png') no-repeat center center;
        }

        td.headers-control {
            background: url('https://cdn.rawgit.com/DataTables/DataTables/6c7ada53ebc228ea9bc28b1b216e793b1825d188/examples/resources/details_open.png') no-repeat center center;
            cursor: pointer;
        }
        tr.shown td.headers-control {
            background: url('https://cdn.rawgit.com/DataTables/DataTables/6c7ada53ebc228ea9bc28b1b216e793b1825d188/examples/resources/details_close.png') no-repeat center center;
        }

        td.details-control {
            background: url('https://cdn.rawgit.com/DataTables/DataTables/6c7ada53ebc228ea9bc28b1b216e793b1825d188/examples/resources/details_open.png') no-repeat center center;
            cursor: pointer;
        }
        tr.shown td.details-control {
            background: url('https://cdn.rawgit.com/DataTables/DataTables/6c7ada53ebc228ea9bc28b1b216e793b1825d188/examples/resources/details_close.png') no-repeat center center;
        }

        .c-rounded {
            border-radius: 0.42rem !important
        }

        #pagepanel .datepicker .disabled {
            background: #E9ECEF
        }
    </style>
@endsection


{{-- Content --}}
@section('content')
    <div class="container">
    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

    <!-- <div id="treeViewDiv"></div> -->
    <div id="pagepanel" class="card card-custom flat table-scrollable pagepanel">
        <div id="loading-fullscreen" style="position: fixed;
            z-index: 999;
            height: 100%;
            width: 100%;
            top: 0;
            left: 0;
            background-color: Black;
            filter: alpha(opacity=60);
            opacity: 0.6;
            -moz-opacity: 0.8;
            display: none">
            <div style="z-index: 1000;
                margin: 300px auto;
                padding: 10px;
                width: 130px;
                background-color: White;
                border-radius: 10px;
                filter: alpha(opacity=100);
                opacity: 1;
                -moz-opacity: 1;">
                <img alt="" src="assets/loading.gif" width="110" height="110"/>
            </div>
        </div>
        <div class="card-header card-header_ bg-main-1 d-flex flat" >
            <div class="card-title">
                <span class="card-icon">
                    <i id="widgeticon" class="text-white icon-briefcase"></i>
                </span>
                <h3 id="widgetcaption" class="card-label text-white">
                    Master Card
                </h3>
            </div>
            <div class="card-toolbar" >
                <div id="tool-pertama">
                    <a  onclick="addData()" href="javascript:;" id="btn-add-data" style="color: #FFFFFF;margin-right: 5px;" class="btn btn-sm flat" onmouseover="mouseoverAddData('btn-add-data')" onmouseout="mouseoutAddData('btn-add-data')"><i style="color:#FFFFFF" class="icon-save"></i> Tambah Data</a>
                </div>
                <div id="tool-kedua" style="display: none">
                    <a class="btn btn-sm btn-success flat" style="margin-right: 5px;" id="btn-save-data" onclick="saveData()">
                        <i class="fa fa-save"></i> Save
                    </a>
                    <a onclick="cancelData()" class="btn btn-sm btn-default flat" style="margin-right: 5px;">
                        <i class="fa fa-reply"></i> Cancel
                    </a>
                </div>
                
                <div id="btn-saveto">
                </div>

                <div id="tool-ketiga" style="display: none">
                    <a id="back" class="btn btn-sm btn-success flat" onclick="cancelData()"  style="margin-right: 5px;">
                        <i class="fa fa-reply"></i> Back
                    </a>
                </div>
                <div class="dropdown" style="margin-right: 5px;" id="tools-dropdown">
                    <a style="background-color: transparent !important; border: 1px solid white; color: white;" class="btn btn-default btn-sm flat" href="javascript:;" data-toggle="dropdown">
                        <i style="color:#FFFFFF" class="icon-cogs"></i>
                            Tools
                        <i style="color:#FFFFFF" class="icon-angle-down"></i>
                    </a>
                    <!-- <ul style="" id="drop_export" class="dropdown-menu dropdown-menu-right">
                        <li id="drop_export2"></li>
                    </ul> -->

                    <div class="dropdown-menu dropdown-menu-right" style="min-width: 220px; padding: 5px">
                        <div id="drop_export2">
                        </div>
                    </div>

                    <!-- <div class="dropdown-menu dropdown-menu-right" style="min-width: 220px; padding: 5px">
                        <div class="row">
                            <button type="button" class="btn btn-danger col-md-12" onclick="deleteSelected()"><i class="fa fa-trash"></i> Delete Selected</button>
                        </div>
                        <div class="row mt-2">
                            <button type="button" class="btn btn-danger col-md-12" onclick="deleteAll()"><i class="fa fa-trash"></i> Delete All</button>
                        </div>
                        <div class="row mt-2">
                            <button type="button" class="btn btn-success col-md-12" onclick="exportExcel()"><i class="fa fa-file-excel"></i> Export Excel</button>
                        </div>
                    </div> -->
                </div>
                <a style="background-image: none !important; margin-right: 5px; /* height: 27px; */ width: 27px !important; text-align: center; padding: 2px 2px 0px 2px !important; border-radius: 50% !important; background-color: #2196F3; color: #FFF;" id="tool_refresh" href="javascript:;" onclick="refresh()" class="bg-main-1"><i style="color:#FFFFFF" class="icon-refresh"></i></a>
                <!-- <a style="background-image: none !important; margin-right: 5px; /* height: 27px; */ width: 27px !important; text-align: center; padding: 2px 2px 0px 2px !important; border-radius: 50% !important; background-color: #2196F3; color: #FFF;" id="tool_filter" href="javascript:;" onclick="showModalFilter()" class="bg-main-1"><i style="color:#FFFFFF" class="icon-filter"></i></a> -->
                <a style="background-image: none !important; margin-right: 5px; /* height: 27px; */ width: 27px !important; text-align: center; padding: 2px 2px 0px 2px !important; border-radius: 50% !important; background-color: #2196F3; color: #FFF;" id="tool_refresh_up" onClick="toggleButtonCollapse()" href="javascript:;" class="bg-main-1"><i style="color:#FFFFFF" class="icon-sort"></i></a>
                <a class="bg-main-1" id="tool_fullscreen" onclick="toggleFullscreen()" style="background-image: none !important; /* height: 27px; */ width: 27px !important; text-align: center; padding: 2px 2px 0px 2px !important; border-radius: 50% !important; background-color: #2196F3; color: #FFF;">
                    <i class="icon-fullscreen" style="color:#FFFFFF;"></i>   
                </a>
            </div>
            </div>
            <div class="card-body card-body-custom flat" id="pagepanel-body" style="padding: 10px !important">
                <input type="hidden" id="judul_excel">
                <input type="hidden"  id="tabel1" value="">
                <input type="hidden"  id="namatable">
                <input type="hidden"  id="url_code">
                
                <div class="row">
                    <div class="btn-toolbar" role="toolbar" aria-label="...">
                        <div id="filterlain">
                        </div>
                    </div>
                </div>

                <div class="cursor"></div>
                <div id="from-departement">


                        <!-- MCARD -->
                        <div class="row" style="margin-top: 20px" id="mcard">
                            <div class="col-md-12" style="padding: 0px !important">
                                <div class="card" style="margin: 0px !important">
                                    <div class="card-header" id="header-sticky-1" style="padding-top: 0px; padding-bottom: 0px;">
                                        <div class="row">
                                            <div class="col-md-7">
                                                <div class="form-group pull-left" style="padding-top: 8px; padding-left: 0px; padding-right: 8px">
                                                    <b style="font-size: 16px" class="tab-title">Card</b>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <ul class="nav nav-tabs mb-3 justify-content-end" id="tab-form-mcard" role="tablist" style="margin-bottom: 0px !important">
                                                    <li></li>
                                                    <li class="nav-item" role="presentation" id="li-tab-mcard">
                                                        <a class="nav-link show active" id="tab-mcard" data-mdb-toggle="tab" role="tab" aria-controls="tab-mcard" aria-selected="true" onclick="setTab({parent: 'mcard', element: 'tab-mcard', title: 'Card'})">Card</a>
                                                    </li>
                                                    <li class="nav-item" role="presentation" id="li-tab-mcardgeneral">
                                                        <a class="nav-link" id="tab-mcardgeneral" data-mdb-toggle="tab" role="tab" aria-controls="tab-mcardgeneral" aria-selected="true" onclick="setTab({parent: 'mcard', element: 'tab-mcardgeneral', title: 'General'})">General</a>
                                                    </li>
                                                    <li class="nav-item" role="presentation" id="li-tab-mcardaddress">
                                                        <a class="nav-link" id="tab-mcardaddress" data-mdb-toggle="tab" role="tab" aria-controls="tab-mcardaddress" aria-selected="true" onclick="setTab({parent: 'mcard', element: 'tab-mcardaddress', title: 'Address'})">Address</a>
                                                    </li>
                                                    <li class="nav-item" role="presentation" id="li-tab-mcardcontact">
                                                        <a class="nav-link" id="tab-mcardcontact" data-mdb-toggle="tab" role="tab" aria-controls="tab-mcardcontact" aria-selected="true" onclick="setTab({parent: 'mcard', element: 'tab-mcardcontact', title: 'Contact'})">Contact</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="tab-content" id="tab-content-mcard">
                                            <div class="tab-pane fade show active" id="tab-mcard-content" role="tabpanel" aria-labelledby="tab-mcard-content">
                                                <form id="form-mcard">
                                                    <div class="row" style="margin-top: 20px">
                                                        <div class="col-md-6" style="padding: 0px !important">
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Kode</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="hidden" id="noid">
                                                                        <input type="text" name="kode" id="kode" class="form-control" placeholder="Kode">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">First Name</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="firstname" id="firstname" class="form-control" placeholder="First Name">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Middle Name</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="middlename" id="middlename" class="form-control" placeholder="Middle Name">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Last Name</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="lastname" id="lastname" class="form-control" placeholder="Last Name">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Gelar</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="gelar" id="gelar" class="form-control" placeholder="Gelar">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Email</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="email" id="email" class="form-control" placeholder="Email">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Group User</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="isgroupuser" id="isgroupuser">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is User</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="isuser" id="isuser" onchange="addTab({tab: 'user'})">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Employee</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="isemployee" id="isemployee" onchange="addTab({tab: 'employee'})">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Customer</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="iscustomer" id="iscustomer" onchange="addTab({tab: 'customer'})">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Supplier</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="issupplier" id="issupplier" onchange="addTab({tab: 'supplier'})">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is 3rdparty</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="is3rdparty" id="is3rdparty">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Active</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="isactive" id="isactive">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="tab-pane fade" id="tab-mcardgeneral-content" role="tabpanel" aria-labelledby="tab-mcardgeneral-content">
                                                <form id="form-mcardgeneral">
                                                    <div class="row" style="margin-top: 20px">
                                                        <div class="col-md-6" style="padding: 0px !important">
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Gender</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mcardgeneral_idgender" id="mcardgeneral_idgender">
                                                                        <option value="">[Choose Gender]</option>
                                                                        @foreach($genmgender as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Religion</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mcardgeneral_idreligion" id="mcardgeneral_idreligion">
                                                                        <option value="">[Choose Religion]</option>
                                                                        @foreach($genmreligion as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Marital</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mcardgeneral_idmarital" id="mcardgeneral_idmarital">
                                                                        <option value="">[Choose Marital]</option>
                                                                        @foreach($genmmarital as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Nationality</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mcardgeneral_idnationality" id="mcardgeneral_idnationality">
                                                                        <option value="">[Choose Nationality]</option>
                                                                        @foreach($genmnationality as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Race</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mcardgeneral_idrace" id="mcardgeneral_idrace">
                                                                        <option value="">[Choose Race]</option>
                                                                        @foreach($genmrace as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Blood Type</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mcardgeneral_idbloodtype" id="mcardgeneral_idbloodtype">
                                                                        <option value="">[Choose Blood Type]</option>
                                                                        @foreach($genmbloodtype as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Language 1</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mcardgeneral_idlanguage1" id="mcardgeneral_idlanguage1">
                                                                        <option value="">[Choose Language 1]</option>
                                                                        @foreach($genmlanguage as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Language 2</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mcardgeneral_idlanguage2" id="mcardgeneral_idlanguage2">
                                                                        <option value="">[Choose Language 2]</option>
                                                                        @foreach($genmlanguage as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Lokasi Lahir</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mcardgeneral_idlokasilahir" id="mcardgeneral_idlokasilahir">
                                                                        <option value="">[Choose Lokasi Lahir]</option>
                                                                        @foreach($genmlokasi as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Tempat Lahir</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mcardgeneral_tempatlahir" id="mcardgeneral_tempatlahir" class="form-control" placeholder="Tempat Lahir">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Do Birth</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mcardgeneral_dobirth" id="mcardgeneral_dobirth" class="form-control datepicker" placeholder="Do Birth">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="tab-pane fade" id="tab-mcardaddress-content" role="tabpanel" aria-labelledby="tab-mcardaddress-content">
                                                <form id="form-mcardaddress">
                                                    <div class="row" style="margin-top: 20px">
                                                        <div class="col-md-6" style="padding: 0px !important">
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Main Contact</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardaddress_ismaincontact" id="mcardaddress_ismaincontact">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Type Address</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mcardaddress_idtypeaddress" id="mcardaddress_idtypeaddress">
                                                                        <option value="">[Choose Type Address]</option>
                                                                        @foreach($genmtypeaddress as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Alamat 1</label>
                                                                <div class="col-md-9">
                                                                    <textarea name="mcardaddress_alamat1" id="mcardaddress_alamat1" cols="3" rows="5" class="form-control" placeholder="Alamat 1"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Alamat 2</label>
                                                                <div class="col-md-9">
                                                                    <textarea name="mcardaddress_alamat2" id="mcardaddress_alamat2" cols="3" rows="5" class="form-control" placeholder="Alamat 2"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">RT</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="number" name="mcardaddress_alamatrt" id="mcardaddress_alamatrt" class="form-control" placeholder="RT">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">RW</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="number" name="mcardaddress_alamatrw" id="mcardaddress_alamatrw" class="form-control" placeholder="RW">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Propinsi</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mcardaddress_idlokasiprop" id="mcardaddress_idlokasiprop">
                                                                        <option value="">[Choose Propinsi]</option>
                                                                        @foreach($mlokasiprop as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Kota</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mcardaddress_idlokasikab" id="mcardaddress_idlokasikab">
                                                                        <option value="">[Choose Kota]</option>
                                                                        @foreach($mlokasikab as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Kecamatan</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mcardaddress_idlokasikec" id="mcardaddress_idlokasikec">
                                                                        <option value="">[Choose Kecamatan]</option>
                                                                        @foreach($mlokasikec as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Kelurahan</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mcardaddress_idlokasikel" id="mcardaddress_idlokasikel">
                                                                        <option value="">[Choose Kelurahan]</option>
                                                                        @foreach($mlokasikel as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="tab-pane fade" id="tab-mcardcontact-content" role="tabpanel" aria-labelledby="tab-mcardcontact-content">
                                                <form id="form-mcardcontact">
                                                    <div class="row" style="margin-top: 20px">
                                                        <div class="col-md-6" style="padding: 0px !important">
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Type Contact</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mcardcontact_idtypecontact" id="mcardcontact_idtypecontact">
                                                                        <option value="">[Choose Type Contact]</option>
                                                                        @foreach($genmtypecontact as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Contact Name</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mcardcontact_contactname" id="mcardcontact_contactname" class="form-control" placeholder="Contact Name">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Contact Var</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mcardcontact_contactvar" id="mcardcontact_contactvar" class="form-control" placeholder="Contact Var">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <!-- MCARD OTHERs -->
                        <div class="row" style="margin-top: 20px; display: none" id="mcardothers">
                            <div class="col-md-12" style="padding: 0px !important">
                                <div class="card" style="margin: 0px !important">
                                    <div class="card-header" id="header-sticky-1" style="padding-top: 0px; padding-bottom: 0px;">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group pull-left" style="padding-top: 8px; padding-left: 0px; padding-right: 8px">
                                                    <b style="font-size: 16px" class="tab-title">MCARD Other</b>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <ul class="nav nav-tabs mb-3 justify-content-end" id="tab-form" role="tablist" style="margin-bottom: 0px !important">
                                                    <li></li>
                                                    <li class="nav-item" role="presentation" id="li-tab-mcarduser" style="display: none">
                                                        <a class="nav-link" id="tab-mcarduser" data-mdb-toggle="tab" role="tab" aria-controls="tab-mcarduser" aria-selected="true" onclick="setTab({parent: 'mcardothers', element: 'tab-mcarduser', title: 'User'})">User</a>
                                                    </li>
                                                    <li class="nav-item" role="presentation" id="li-tab-mcardprivilidge" style="display: none">
                                                        <a class="nav-link" id="tab-mcardprivilidge" data-mdb-toggle="tab" role="tab" aria-controls="tab-mcardprivilidge" aria-selected="true" onclick="setTab({parent: 'mcardothers', element: 'tab-mcardprivilidge', title: 'Privilidge'})">Privilidge</a>
                                                    </li>
                                                    <li class="nav-item" role="presentation" id="li-tab-mcardemployee" style="display: none">
                                                        <a class="nav-link" id="tab-mcardemployee" data-mdb-toggle="tab" role="tab" aria-controls="tab-mcardemployee" aria-selected="true" onclick="setTab({parent: 'mcardothers', element: 'tab-mcardemployee', title: 'Employee'})">Employee</a>
                                                    </li>
                                                    <li class="nav-item" role="presentation" id="li-tab-mcardcustomer" style="display: none">
                                                        <a class="nav-link" id="tab-mcardcustomer" data-mdb-toggle="tab" role="tab" aria-controls="tab-mcardcustomer" aria-selected="true" onclick="setTab({parent: 'mcardothers', element: 'tab-mcardcustomer', title: 'Customer'})">Customer</a>
                                                    </li>
                                                    <li class="nav-item" role="presentation" id="li-tab-mcardsupplier" style="display: none">
                                                        <a class="nav-link" id="tab-mcardsupplier" data-mdb-toggle="tab" role="tab" aria-controls="tab-mcardsupplier" aria-selected="true" onclick="setTab({parent: 'mcardothers', element: 'tab-mcardsupplier', title: 'Supplier'})">Supplier</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="tab-content" id="tab-content">
                                            <div class="tab-pane fade" id="tab-mcarduser-content" role="tabpanel" aria-labelledby="tab-mcarduser-content">
                                                <form id="form-mcarduser">
                                                    <div class="row" style="margin-top: 20px">
                                                        <div class="col-md-6" style="padding: 0px !important">
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Username</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mcarduser_myusername" id="mcarduser_myusername" class="form-control" placeholder="Username">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Password</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="password" name="mcarduser_mypassword" id="mcarduser_mypassword" class="form-control" placeholder="Password">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Group User</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mcarduser_groupuser" id="mcarduser_groupuser" class="form-control" placeholder="Group User">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="tab-pane fade" id="tab-mcardprivilidge-content" role="tabpanel" aria-labelledby="tab-mcardprivilidge-content">
                                                <form id="form-mcardprivilidge">
                                                    <div class="row" style="margin-top: 20px">
                                                        <div class="col-md-6" style="padding: 0px !important">
                                                            <!-- <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Menu</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mcardprivilidge_idmenu" id="mcardprivilidge_idmenu" class="form-control" placeholder="Menu">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Menu Parent</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mcardprivilidge_menuparent" id="mcardprivilidge_menuparent" class="form-control" placeholder="Menu Parent">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Menu Group</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mcardprivilidge_menugroup" id="mcardprivilidge_menugroup" class="form-control" placeholder="Menu Group">
                                                                    </div>
                                                                </div>
                                                            </div> -->
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Visible</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_isvisible" id="mcardprivilidge_isvisible">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Enable</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_isenable" id="mcardprivilidge_isenable">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Add</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_isadd" id="mcardprivilidge_isadd">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Edit</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_isedit" id="mcardprivilidge_isedit">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Delete</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_isdelete" id="mcardprivilidge_isdelete">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Delete Other</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_isdeleteother" id="mcardprivilidge_isdeleteother">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is View</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_isview" id="mcardprivilidge_isview">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Report</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_isreport" id="mcardprivilidge_isreport">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Report Detail</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_isreportdetail" id="mcardprivilidge_isreportdetail">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Confirm</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_isconfirm" id="mcardprivilidge_isconfirm">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Unconfirm</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_isunconfirm" id="mcardprivilidge_isunconfirm">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Post</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_ispost" id="mcardprivilidge_ispost">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Unpost</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_isunpost" id="mcardprivilidge_isunpost">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Print Daftar</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_isprintdaftar" id="mcardprivilidge_isprintdaftar">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Print Detail</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_isprintdetail" id="mcardprivilidge_isprintdetail">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Show Audit</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_isshowaudit" id="mcardprivilidge_isshowaudit">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Show Log</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_isshowlog" id="mcardprivilidge_isshowlog">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Export To XCL</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_isexporttoxcl" id="mcardprivilidge_isexporttoxcl">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Export To CSV</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_isexporttocsv" id="mcardprivilidge_isexporttocsv">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Export To PDF</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_isexporttopdf" id="mcardprivilidge_isexporttopdf">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Export To HTML</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_isexporttohtml" id="mcardprivilidge_isexporttohtml">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Custom 1</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_iscustom1" id="mcardprivilidge_iscustom1">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Custom 2</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_iscustom2" id="mcardprivilidge_iscustom2">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Custom 3</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_iscustom3" id="mcardprivilidge_iscustom3">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Custom 4</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_iscustom4" id="mcardprivilidge_iscustom4">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Custom 5</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_iscustom5" id="mcardprivilidge_iscustom5">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Production 1</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_isproduction01" id="mcardprivilidge_isproduction01">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Production 2</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_isproduction02" id="mcardprivilidge_isproduction02">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Production 3</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_isproduction03" id="mcardprivilidge_isproduction03">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Production 4</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_isproduction04" id="mcardprivilidge_isproduction04">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Production 5</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_isproduction05" id="mcardprivilidge_isproduction05">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Production 6</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcardprivilidge_isproduction06" id="mcardprivilidge_isproduction06">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="tab-pane fade" id="tab-mcardemployee-content" role="tabpanel" aria-labelledby="tab-mcardemployee-content">
                                                <form id="form-mcardemployee">
                                                    <div class="row" style="margin-top: 20px">
                                                        <div class="col-md-6" style="padding: 0px !important">
                                                            <!-- <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Unit Kerja</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mce_idunitkerja" id="mce_idunitkerja">
                                                                        <option value="">[Choose Unit Kerja]</option>
                                                                        @foreach($mlokasiprop as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div> -->
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">NIP</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mce_nip" id="mce_nip" class="form-control" placeholder="NIP">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Company</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mce_idcompany" id="mce_idcompany">
                                                                        <option value="">[Choose Company]</option>
                                                                        @foreach($genmcompany as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Department</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mce_iddepartment" id="mce_iddepartment">
                                                                        <option value="">[Choose Department]</option>
                                                                        @foreach($genmdepartment as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Gudang</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mce_idgudang" id="mce_idgudang">
                                                                        <option value="">[Choose Gudang]</option>
                                                                        @foreach($genmgudang as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Report to</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mce_idreportto" id="mce_idreportto">
                                                                        <option value="">[Choose Report to]</option>
                                                                        @foreach($genmgudang as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div> -->
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Gelar Depan</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mce_gelardepan" id="mce_gelardepan" class="form-control" placeholder="Gelar Depan">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Gelar Belakang</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mce_gelarbelakang" id="mce_gelarbelakang" class="form-control" placeholder="Gelar Belakang">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Nama Lengkap</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mce_namalengkap" id="mce_namalengkap" class="form-control" placeholder="Nama Lengkap">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">ID Pangkat Gol.</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mce_idpangkatgol" id="mce_idpangkatgol">
                                                                        <option value="">[Choose Pangkat Gol.]</option>
                                                                        @foreach($genmgudang as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div> -->
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Pangkat Gol.</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mce_pangkatgol" id="mce_pangkatgol" class="form-control" placeholder="Pangkat Golongan">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Kode Pangkat Gol.</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mce_pangkatgolkode" id="mce_pangkatgolkode" class="form-control" placeholder="Kode Pangkat Golongan">
                                                                    </div>
                                                                </div>
                                                            </div> -->
                                                            <!-- <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Dot MT Pangkat</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mce_dotmtpangkat" id="mce_dotmtpangkat" class="form-control" placeholder="Dot MT Pangkat">
                                                                    </div>
                                                                </div>
                                                            </div> -->
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Nama Jabatan</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mce_jabatannama" id="mce_jabatannama" class="form-control" placeholder="Nama Jabatan">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">ID Strata</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="text" name="mce_idstrata" id="mce_idstrata" class="form-control" placeholder="ID Strata">
                                                                </div>
                                                            </div> -->
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Nama Pddikan.</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mce_pendidikannama" id="mce_pendidikannama" class="form-control" placeholder="Nama Pendidikan">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Lulus Pddikan.</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="number" name="mce_pendidikanlulus" id="mce_pendidikanlulus" class="form-control" placeholder="Lulus Pendidikan">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Tempat Lahir</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mce_tempatlahir" id="mce_tempatlahir" class="form-control" placeholder="Tempat Lahir">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">DOB</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mce_dob" id="mce_dob" class="form-control" placeholder="DOB">
                                                                    </div>
                                                                </div>
                                                            </div> -->
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Gender</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mce_idgender" id="mce_idgender">
                                                                        <option value="">[Choose Gender]</option>
                                                                        @foreach($genmgender as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Religion</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mce_idreligion" id="mce_idreligion">
                                                                        <option value="">[Choose Religion]</option>
                                                                        @foreach($genmreligion as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Marital</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mce_idmarital" id="mce_idmarital">
                                                                        <option value="">[Choose Marital]</option>
                                                                        @foreach($genmmarital as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6" style="padding: 0px !important">
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Alamat</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <textarea name="mce_alamat" id="mce_alamat" cols="3" rows="5" class="form-control" placeholder="Keterangan"></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Alamat Jln.</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <textarea type="text" name="mce_alamatjl" id="mce_alamatjl" class="form-control" placeholder="Alamat Jln.">
                                                                        </textarea>
                                                                    </div>
                                                                </div>
                                                            </div> -->
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Propinsi</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mce_idlokasiprop" id="mce_idlokasiprop">
                                                                        <option value="">[Choose Propinsi]</option>
                                                                        @foreach($mlokasiprop as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Kota</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mce_idlokasikota" id="mce_idlokasikota">
                                                                        <option value="">[Choose Kota]</option>
                                                                        @foreach($mlokasikab as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Kecamatan</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mce_idlokasikec" id="mce_idlokasikec">
                                                                        <option value="">[Choose Kecamatan]</option>
                                                                        @foreach($mlokasikec as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Kelurahan</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mce_idlokasikel" id="mce_idlokasikel">
                                                                        <option value="">[Choose Kelurahan]</option>
                                                                        @foreach($mlokasikec as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">No. Mobile</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mce_nomobile" id="mce_nomobile" class="form-control" placeholder="No. Mobile">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">No. Phone</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mce_nophone" id="mce_nophone" class="form-control" placeholder="No. Phone">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="tab-pane fade" id="tab-mcardcustomer-content" role="tabpanel" aria-labelledby="tab-mcardcustomer-content">
                                                <form id="form-mcardcustomer">
                                                    <div class="row" style="margin-top: 20px">
                                                        <div class="col-md-6" style="padding: 0px !important">
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Kode</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mcc_kode" id="mcc_kode" class="form-control" placeholder="Kode">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Nama</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mcc_nama" id="mcc_nama" class="form-control" placeholder="Nama">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Nama Alias</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mcc_namaalias" id="mcc_namaalias" class="form-control" placeholder="Nama Alias">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Keterangan</label>
                                                                <div class="col-md-9">
                                                                    <textarea name="mcc_keterangan" id="mcc_keterangan" cols="3" rows="5" class="form-control" placeholder="Keterangan"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Is Active</label>
                                                                <div class="col-md-9 pt-2">
                                                                    <input type="checkbox" name="mcc_isactive" id="mcc_isactive">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">NPWP</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mcc_npwp" id="mcc_npwp" class="form-control" placeholder="NPWP">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Alamat 1</label>
                                                                <div class="col-md-9">
                                                                    <textarea name="mcc_alamat1" id="mcc_alamat1" cols="3" rows="5" class="form-control" placeholder="Alamat 1"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Alamat 2</label>
                                                                <div class="col-md-9">
                                                                    <textarea name="mcc_alamat2" id="mcc_alamat2" cols="3" rows="5" class="form-control" placeholder="Alamat 2"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6" style="padding: 0px !important">
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">RT</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="number" name="mcc_alamatrt" id="mcc_alamatrt" class="form-control" placeholder="RT">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">RW</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="number" name="mcc_alamatrw" id="mcc_alamatrw" class="form-control" placeholder="RW">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Propinsi</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mcc_idlokasiprop" id="mcc_idlokasiprop">
                                                                        <option value="">[Choose Propinsi]</option>
                                                                        @foreach($mlokasiprop as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Kota</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mcc_idlokasikota" id="mcc_idlokasikota">
                                                                        <option value="">[Choose Kota]</option>
                                                                        @foreach($mlokasikab as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Kecamatan</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mcc_idlokasikec" id="mcc_idlokasikec">
                                                                        <option value="">[Choose Kecamatan]</option>
                                                                        @foreach($mlokasikec as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Kelurahan</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mcc_idlokasikel" id="mcc_idlokasikel">
                                                                        <option value="">[Choose Kelurahan]</option>
                                                                        @foreach($mlokasikel as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">No Phone</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mcc_nophone" id="mcc_nophone" class="form-control" placeholder="No Phone">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">No Fax</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mcc_nofax" id="mcc_nofax" class="form-control" placeholder="No Fax">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Email</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mcc_email" id="mcc_email" class="form-control" placeholder="Email">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="tab-pane fade" id="tab-mcardsupplier-content" role="tabpanel" aria-labelledby="tab-mcardsupplier-content">
                                                <form id="form-mcardsupplier">
                                                    <div class="row" style="margin-top: 20px">
                                                        <div class="col-md-6" style="padding: 0px !important">
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Nama</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mcs_nama" id="mcs_nama" class="form-control" placeholder="Nama">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Alamat</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <textarea name="mcs_alamat" id="mcs_alamat" cols="3" rows="5" class="form-control" placeholder="Alamat"></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Alamat Jln.</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <textarea name="mcs_alamatjl" id="mcs_alamatjl" cols="3" rows="5" class="form-control" placeholder="Alamat Jln"></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Propinsi</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mcs_idlokasiprop" id="mcs_idlokasiprop">
                                                                        <option value="">[Choose Propinsi]</option>
                                                                        @foreach($mlokasiprop as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Kota</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mcs_idlokasikota" id="mcs_idlokasikota">
                                                                        <option value="">[Choose Kota]</option>
                                                                        @foreach($mlokasikab as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Kecamatan</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mcs_idlokasikec" id="mcs_idlokasikec">
                                                                        <option value="">[Choose Kecamatan]</option>
                                                                        @foreach($mlokasikec as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Kelurahan</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control select2" name="mcs_idlokasikel" id="mcs_idlokasikel">
                                                                        <option value="">[Choose Kelurahan]</option>
                                                                        @foreach($mlokasikel as $v)
                                                                            <option value="{{$v->noid}}">{{$v->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <!-- <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Alamat Kecamatan</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <textarea type="text" name="mcs_alamatkec" id="mcs_alamatkec" class="form-control" placeholder="Alamat Kecamatan">
                                                                        </textarea>
                                                                    </div>
                                                                </div>
                                                            </div> -->
                                                        </div>
                                                        <div class="col-md">
                                                            <!-- <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Core Bussiness</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mcs_corebussiness" id="mcs_corebussiness" class="form-control" placeholder="Core Bussiness">
                                                                    </div>
                                                                </div>
                                                            </div> -->
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">NPWP</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mcs_npwp" id="mcs_npwp" class="form-control" placeholder="NPWP">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">No. Mobile</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mcs_nomobile" id="mcs_nomobile" class="form-control" placeholder="Nomor Mobile">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">No. Phone</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mcs_nophone" id="mcs_nophone" class="form-control" placeholder="No Phone">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">No Fax</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mcs_nofax" id="mcs_nofax" class="form-control" placeholder="No Fax">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Comp. Email</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mcs_companyemail" id="mcs_companyemail" class="form-control" placeholder="Company Email">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Comp. Web</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mcs_companyweb" id="mcs_companyweb" class="form-control" placeholder="Company Web">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Termijn Payment</label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mcs_termijnpayment" id="mcs_termijnpayment" class="form-control" placeholder="Alamat Termijn Payment">
                                                                    </div>
                                                                </div>
                                                            </div> -->
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <!-- <ul class="nav nav-tabs mb-3" id="tab-form_" role="tablist">
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link active" id="tab-transaksi" data-mdb-toggle="tab" role="tab" aria-controls="tab-transaksi" aria-selected="true" onclick="setTab('tab-transaksi')">Transaksi</a>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link" id="tab-supplier" data-mdb-toggle="tab" role="tab" aria-controls="tab-supplier" aria-selected="false" onclick="setTab('tab-supplier')">Supplier</a>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link" id="tab-delivery" data-mdb-toggle="tab" role="tab" aria-controls="tab-delivery" aria-selected="false" onclick="setTab('tab-delivery')">Delivery</a>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link" id="tab-akun" data-mdb-toggle="tab" role="tab" aria-controls="tab-akun" aria-selected="false" onclick="setTab('tab-akun')">Akun</a>
                                    </li>
                                </ul> -->

                                <div class="tab-content" id="tab-content_">
                                    <div class="tab-pane fade" id="tab-transaksi-content" role="tabpanel" aria-labelledby="tab-transaksi-content">
                                        <form id="form-transaksi">
                                            <div class="card">
                                                <!-- <div class="card-header" style="background-color: #8E44AD; color: white"> -->
                                                <div class="card-header">
                                                    <b style="font-size: 18px">Data Transaksi</b>
                                                </div>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Status Tranc</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Status Card</label>
                                                                <select class="form-control select2">
                                                                <option value="">[Choose Status Card]</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Kode</label>
                                                                <input type="text" name="kode" class="form-control" placeholder="Kode">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Kode Reff</label>
                                                                <input type="text" name="kodereff" class="form-control" placeholder="Kode Reff">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Tanggal</label>
                                                                <input type="text" class="form-control datepicker" placeholder="Tanggal" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Tanggal Tax</label>
                                                                <input type="text" name="tanggaltax" class="form-control datepicker" placeholder="Tanggal Tax" autocomplete="off">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Tanggal Due</label>
                                                                <input type="text" class="form-control datepicker" placeholder="Tanggal Due" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <!-- <div class="form-group">
                                                                <label>Tanggal Tax</label>
                                                                <input type="text" class="form-control datepicker" placeholder="Tanggal Tax">
                                                            </div> -->
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Keterangan</label>
                                                                <textarea name="keterangan" id="" cols="3" rows="5" class="form-control" placeholder="Keterangan"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>ID Currency</label>
                                                                <select class="form-control select2">
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Konv Curr</label>
                                                                <input type="text" name="konvcurr" class="form-control" placeholder="Konv Curr">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>ID Gudang</label>
                                                                <select class="form-control select2">
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>ID Company</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>ID Department</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>ID Type Tranc Prev</label>
                                                                <select class="form-control select2" name="idtypetrancprev">
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>ID Tranc Prev</label>
                                                                <input type="text" name="idtrancprev" class="form-control" placeholder="ID Tranc Prev">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>ID Type Tranc Order</label>
                                                                <select class="form-control select2" name="idtypetrancorder">
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>ID Tranc Order</label>
                                                                <input type="text" name="idtrancorder" class="form-control" placeholder="ID Tranc Order">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane fade" id="tab-supplier-content" role="tabpanel" aria-labelledby="tab-supplier-content">
                                        
                                    </div>
                                    <div class="tab-pane fade" id="tab-delivery-content" role="tabpanel" aria-labelledby="tab-delivery-content">
                                        <form id="form-delivery">
                                            <div class="card">
                                                <!-- <div class="card-header" style="background-color: #32C5D2; color: white"> -->
                                                <div class="card-header">
                                                    <b style="font-size: 18px">Data Delivery</b>
                                                </div>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label>Card Delivery</label>
                                                                <select class="form-control select2" name="idcarddelivery">
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Delivery Name</label>
                                                                <input type="text" name="deliveryname" class="form-control" placeholder="Delivery Name">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Delivery Tanggal</label>
                                                                <input type="text" name="deliverytanggal" class="form-control" placeholder="Delivery Tanggal">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Delivery Vehicle</label>
                                                                <input type="text" name="deliveryvehicle" class="form-control" placeholder="Delivery Vehicle">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Delivery Vehicle Plat</label>
                                                                <input type="text" name="deliveryvehicleplat" class="form-control" placeholder="Delivery Vehicle Plat">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane fade" id="tab-akun-content" role="tabpanel" aria-labelledby="tab-akun-content">
                                        <form id="form-akun">
                                            <div class="card">
                                                <!-- <div class="card-header" style="background-color: #C49F47; color: white"> -->
                                                <div class="card-header">
                                                    <b style="font-size: 18px">Data Akun</b>
                                                </div>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Akun Bayar</label>
                                                                <select class="form-control select2" name="idakunbayar">
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Akun Deposit</label>
                                                                <select class="form-control select2" name="idakundeposit">
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Akun Biaya Lain</label>
                                                                <select class="form-control select2" name="idakunbiayalain">
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Akun Hutang</label>
                                                                <select class="form-control select2" name="idakunhutang">
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Akun Purchase</label>
                                                                <select class="form-control select2" name="idakunpurchase">
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Akun Persediaan</label>
                                                                <select class="form-control select2" name="idakunpersediaan">
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Akun Sales Return</label>
                                                                <select class="form-control select2" name="idakunsalesreturn">
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Akun Discount</label>
                                                                <select class="form-control select2" name="idakundiscount">
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="row" style="margin-top: 20px">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body" style="display: none">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>ID Create</label>
                                                    <select class="form-control" disabled>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Do Create</label>
                                                    <input type="text" class="form-control" value="<?= date('d-M-Y H:i:s')?>" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>ID Update</label>
                                                    <select class="form-control" disabled>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Last Update</label>
                                                    <input type="text" class="form-control" value="<?= date('d-M-Y H:i:s')?>" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    <!-- </form> -->
                </div>
                <div id="tabledepartemen" class="table table-bordered " style="width: 100%; border: 0px; margin: -10px 0px -10px 0px !important">
                    <!-- <div id="divsucces" class="alert alert-custom alert-light-success fade show mb-5" role="alert">
                        <p id="notiftext"></p>
                    </div> -->
                <!-- <table id="tabletest" class="nowrap" style="width 100px; table-layout: auto">
                    <thead>
                        <tr>
                            <th style="width: 10% !important">Halaman 1</th>
                            <th style="width: 10% !important">Halaman 2</th>
                            <th style="width: 10% !important">Halaman 3</th>
                            <th style="width: 10% !important">Halaman 4</th>
                        </tr>
                    </thead>
                </table> -->
                <!-- <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12 col-xs-12 dtfiler">
                            <div class="col-md-4" id="table_master_paginate">
                                <div class="pagination-panel row">
                                    Page
                                    <a href="#" class="btn btn-sm default prev disabled" title="<i class='fa fa-angle-left'></i>">
                                        <i class="fa fa-angle-left"></i>
                                    </a>
                                    <input type="text" class="pagination-panel-input form-control input-mini input-inline input-sm" maxlenght="5" style="text-align:center; margin: 0 5px;">
                                    <a href="#" class="btn btn-sm default next disabled" title="<i class='fa fa-angle-right'> </i>">
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                     of 
                                    <span class="pagination-panel-total">1</span>
                                </div>
                            </div>
                            <div class="col-md-4" id="table_master_length">
                                <label class="">
                                    <span class="seperator">|</span>
                                    Rec/Page 
                                    <select name="table_master_length" aria-controls="table_master" class="form-control input-inline input-xsmall input-sm recofdt">
                                        <option value="10">10</option>
                                        <option value="50">50</option>
                                        <option value="150">150</option>
                                        <option value="1000">All</option>
                                    </select>
                                </label>
                            </div>
                            <div class="col-md-4" id="table_master_info" role="status" aria-live="polite">
                                <span class="seperator">|</span>Total 6 records 
                            </div>
                        </div>
                        <!-- <div class="dtfilter_search">
                            <div id="table_master_filter" class="dataTables_filter">
                                <label>
                                    <input type="search" class="form-control input-inline input-sm" placeholder="" aria-controls="table_master">
                                </label>
                            </div>
                            <div class="btn fa fa-search dtfilter_search_btn">
                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="row" style="margin-top: 10px; margin-bottom: 10px">
                    <!-- <div class="col"> -->
                        <div id="space_filter_button" style="width: 100%">
                            <div class="row">
                                <div class="btn-group">
                                    <button type="button" id="dropdown-date-filter" class="btn btn-sm dropdown-toggle text-right" style="height: 26px; background-color: #a1b1bc; color: white; margin-right: 0px;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="icon-calendar pull-left" style="margin-top: 2px; color: white"></i>
                                        <span>{{@$datefilter['tm']}}</span>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" style="width: 215px">
                                        <a id="did-a" data-date="{{@$datefilter['a']}}" class="dropdown-item dropdown-item-date" style="padding-left: 10px; padding-right: 10px;" onclick="filterDate('a')" onmouseover="hoverFilterDate('a')" onmouseout="hoverOutFilterDate('a')">
                                            <span style="background-color: {{ @$datefilterdefault == 'a' ? '#4b8df8' : '#f5f5f5' }}; width: 100%; padding: 2px 5px 2px 5px; color: {{ @$datefilterdefault == 'a' ? 'white' : '#08c' }}">All</span>
                                        </a>
                                        <a id="did-t"data-date="{{@$datefilter['t']}}" class="dropdown-item dropdown-item-date" style="padding-left: 10px; padding-right: 10px;" onclick="filterDate('t')" onmouseover="hoverFilterDate('t')" onmouseout="hoverOutFilterDate('t')">
                                            <span style="background-color: {{ @$datefilterdefault == 't' ? '#4b8df8' : '#f5f5f5' }}; width: 100%; padding: 2px 5px 2px 5px; color: {{ @$datefilterdefault == 't' ? 'white' : '#08c' }}">Today</span>
                                        </a>
                                        <a id="did-y" data-date="{{@$datefilter['y']}}" class="dropdown-item dropdown-item-date" style="padding-left: 10px; padding-right: 10px;" onclick="filterDate('y')" onmouseover="hoverFilterDate('y')" onmouseout="hoverOutFilterDate('y')">
                                            <span style="background-color: {{ @$datefilterdefault == 'y' ? '#4b8df8' : '#f5f5f5' }}; width: 100%; padding: 2px 5px 2px 5px; color: {{ @$datefilterdefault == 'y' ? 'white' : '#08c' }};">Yesterday</span>
                                        </a>
                                        <a id="did-l7d" data-date="{{@$datefilter['l7d']}}" class="dropdown-item dropdown-item-date" style="padding-left: 10px; padding-right: 10px;" onclick="filterDate('l7d')" onmouseover="hoverFilterDate('l7d')" onmouseout="hoverOutFilterDate('l7d')">
                                            <span style="background-color: {{ @$datefilterdefault == 'l7d' ? '#4b8df8' : '#f5f5f5' }}; width: 100%; padding: 2px 5px 2px 5px; color: {{ @$datefilterdefault == 'l7d' ? 'white' : '#08c' }};">Last 7 Days</span>
                                        </a>
                                        <a id="did-l30d" data-date="{{@$datefilter['l30d']}}" class="dropdown-item dropdown-item-date" style="padding-left: 10px; padding-right: 10px;" onclick="filterDate('l30d')" onmouseover="hoverFilterDate('l30d')" onmouseout="hoverOutFilterDate('30d')">
                                            <span style="background-color: {{ @$datefilterdefault == 'l30d' ? '#4b8df8' : '#f5f5f5' }}; width: 100%; padding: 2px 5px 2px 5px; color: {{ @$datefilterdefault == 'l30d' ? 'white' : '#08c' }};">Last 30 Days</span>
                                        </a>
                                        <a id="did-tm" data-date="{{@$datefilter['tm']}}" class="dropdown-item dropdown-item-date" style="padding-left: 10px; padding-right: 10px;" onclick="filterDate('tm')" onmouseover="hoverFilterDate('tm')" onmouseout="hoverOutFilterDate('tm')">
                                            <span style="background-color: {{ @$datefilterdefault == 'tm' ? '#4b8df8' : '#f5f5f5' }}; width: 100%; padding: 2px 5px 2px 5px; color: {{ @$datefilterdefault == 'tm' ? 'white' : '#08c' }};">This Month</span>
                                        </a>
                                        <a id="did-lm" data-date="{{@$datefilter['lm']}}" class="dropdown-item dropdown-item-date" style="padding-left: 10px; padding-right: 10px;" onclick="filterDate('lm')" onmouseover="hoverFilterDate('lm')" onmouseout="hoverOutFilterDate('lm')">
                                            <span style="background-color: {{ @$datefilterdefault == 'lm' ? '#4b8df8' : '#f5f5f5' }}; width: 100%; padding: 2px 5px 2px 5px; color: {{ @$datefilterdefault == 'lm' ? 'white' : '#08c' }};">Last Month</span>
                                        </a>
                                        <a id="did-cr" class="dropdown-item dropdown-item-date" style="padding-left: 10px; padding-right: 10px;" onclick="filterDate('cr')" onmouseover="hoverFilterDate('cr')" onmouseout="hoverOutFilterDate('cr')" disabled>
                                            <span style="background-color: {{ @$datefilterdefault == 'cr' ? '#4b8df8' : '#f5f5f5' }}; width: 100%; padding: 2px 5px 2px 5px; color: {{ @$datefilterdefault == 'cr' ? 'white' : '#08c' }};">Custom Range</span>
                                        </a>
                                        <div class="row" style="padding-left: 5px; padding-right: 5px;">
                                            <div class="col-md-6">
                                                <label>From</label>
                                                <input type="text" name="did-from" value="{{date('d-m-Y', mktime (0, 0, 0, date('m'), 1, date('Y')))}}" id="did-from" class="form-control datepicker2" onchange="changeCustomRange()" style="font-size: 12px" placeholder="From">
                                            </div>
                                            <div class="col-md-6">
                                                <label>To</label>
                                                <input type="text" name="did-to" value="{{date('d-m-Y', mktime (0, 0, 0, date('m'), date('t'), date('Y')))}}" id="did-to" class="form-control datepicker2" onchange="changeCustomRange()" style="font-size: 12px" placeholder="To">
                                            </div>
                                        </div>
                                        <div class="row" style="padding-left: 5px; padding-right: 5px; margin-top: 8px; margin-bottom: 2px">
                                            <div class="col-md-12">
                                                <button class="btn btn-success" onclick="filterDate('cr')">Apply</button>
                                                <div class="btn btn-default">Cancel</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="btn" style="padding: 0px; width: 5px"></div>
                                @foreach($filteris as $k => $v)
                                    @if($v->active)
                                        <button class="btn btn-outline-secondary btn-sm flat btn-filter-all btn-filter-is" id="btn-filter-is-{{$v->noid}}" style="background: rgb(144, 202, 249); color: black; font-weight: bold; margin-right: 0px; margin-bottom: 5px;" onclick="filterData({noid:'{{$v->noid}}',class:'btn-filter-is'})">{{$v->nama}}</button>
                                    @else
                                        <button class="btn btn-outline-secondary btn-sm flat btn-filter-is" id="btn-filter-is-{{$v->noid}}" style="background-color:#E3F2FD; color: grey; margin-right: 0px; margin-bottom: 5px;" onclick="filterData({noid:'{{$v->noid}}',class:'btn-filter-is'})">{{$v->nama}}</button>
                                    @endif
                                @endforeach

                                <div class="btn" style="padding: 0px; width: 5px"></div>

                                <div class="btn" style="padding: 0px; width: 5px"></div>
                            </div>
                        </div>
                            <!-- <button class="btn btn-outline-secondary btn-sm flat btn-filter-1-9921050101" id="btn-filter-NEEDAPPROVED-9921050101" style="background-color:#E3F2FD; color: grey; margin-right: 0px" onclick="filterData('btn-filter-NEEDAPPROVED-9921050101', 'btn-filter-1-9921050101', 1, 'NEED APPROVED', 'grey', -1, false, 'fincashbank', 'AND', 'idstatustranc', '=', '2')">NEED APPROVED </button><button class="btn btn-outline-secondary btn-sm flat btn-filter-1-9921050101" id="btn-filter-APPROVED-9921050101" style="background-color:#E3F2FD; color: grey; margin-right: 0px" onclick="filterData('btn-filter-APPROVED-9921050101', 'btn-filter-1-9921050101', 1, 'APPROVED', 'grey', -1, false, 'fincashbank', 'AND', 'idstatustranc', '=', '3')">APPROVED </button><div class="btn" style="padding: 0px; width: 5px"></div></div> -->
                    <!-- </div> -->
                </div>
                <div id="appendInfoDatatable" style="border-top: 2px solid #3598dc; border-right: 3px solid #3598dc; border-left: 2px solid #3598dc;">
                </div>
                <!-- <div id="appendButtonDatatable"></div> -->
                <div class="table-scrollable" style="border: 1px solid #3598dc; margin-top: 0px !important">
                    <table bg-color="blue" class="nowrap table table-striped table-bordered table-hover  dataTable no-footer" style="background-color:#BBDEFB !important; margin: 0 !important; border: 1px solid #3598dc" id="table_master"  aria-describedby="table_master_info" role="grid">
                        <thead id="thead_pagepanel_9921050101">
                            <tr id="thead-columns-d" style="background: #BBDEFC">
                                <th style="width: 30px;">
                                    <input type="checkbox" id="checkbox-m-all" onchange="selectCheckboxMAll()" class="checkbox-m" name="checkbox-m-all" value="FALSE">
                                </th>
                                <th>No</th>
                                <th>Kode</th>
                                <th>Nama</th>
                                <th>First Name</th>
                                <th>Middle Name</th>
                                <th>Last Name</th>
                                <th>Gelar</th>
                                <th>Email</th>
                                <th>Group User</th>
                                <th>User</th>
                                <th>Employee</th>
                                <th>Customer</th>
                                <th>Supplier</th>
                                <th>3rdparty</th>
                                <th>Active</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>

                <!-- <div class="table-scrollable" style="border: 1px solid #3598dc;">
                    <table id="example_baru" class="table table-striped table-bordered table-hover nowrap dataTable no-footer" style="background-color: #BBDEFB; border-collapse: collapse;">
                        <thead>
                            <tr id="thead" class="bg-main"></tr>
                            <tr id="thead-columns"></tr>
                        </thead>
                        <tfoot>
                            <tr id="tfoot-columns">
                            </tr>
                        </tfoot>
                    </table>
                </div> -->
            </div>
        </div>
    </div>

    <br>

@endsection
@section('scripts')
<!-- <script src="//www.amcharts.com/lib/3/amcharts.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/serial.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/radar.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/pie.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/plugins/animate/animate.min.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/plugins/export/export.min.js?v=7.0.6"></script> -->
<!-- <script src="//www.amcharts.com/lib/3/themes/light.js?v=7.0.6"></script> -->
<!-- <script src="https://cdn.amcharts.com/lib/4/core.js"></script> -->
<!-- <script src="https://cdn.amcharts.com/lib/4/charts.js"></script> -->
<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
<script src="{{ asset('js/pages/crud/forms/widgets/bootstrap-datepicker.js?v=7.0.6') }}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.3.1/js/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.21/pagination/input.js"></script>
<script  src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.0.2/js/bootstrap-datetimepicker.min.js"></script>
<!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script> -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-loading-overlay/2.1.7/loadingoverlay.min.js"></script>
<script src="{{ asset('js/jquery-currency.js') }}"></script>
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(".datepicker").datepicker({
        format: 'dd-M-yyyy',
        autoclose: true,
        container: '#pagepanel'
    });

    $(".datepicker2").datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true,
        container: '#pagepanel'
    });

    $('.select2').select2({
        dropdownParent: $('#pagepanel')
    });
    $('.select2').css('width', '100%');
    $('.select2 .selection .select2-selection').css('height', '31px');
    $('.select2 .selection .select2-selection .select2-selection__rendered').css('margin-top', '-6px');
    $('.select2 .selection .select2-selection .select2-selection__arrow').css('margin-top', '4px');

    $('.input-mask').inputmask('decimal', {
        'alias': 'numeric',
        'groupSeparator': ',',
        'autoGroup': true,
        // 'digits': 0,
        // 'radixPoint': ",",
        // 'digitsOptional': false,
        'allowMinus': false,
        'prefix': 'Rp.',
        'placeholder': '0'
    });

    CKEDITOR.replace('summary-ckeditor');
</script>

<!-- GOJS -->
<script src="plugins/custom/GoJS/release/go.js"></script>
<script src="plugins/custom/GoJS/extensions/Figures.js"></script>
<link rel='stylesheet' href='plugins/custom/GoJS/extensions/DataInspector.css' />
<script src="plugins/custom/GoJS/extensions/DataInspector.js"></script>
<script id="code">
    function init() {

      var $ = go.GraphObject.make;

      // initialize main Diagram
      myDiagram =
        $(go.Diagram, "myDiagramDiv",
          {
            "undoManager.isEnabled": true
          });

        let gomargin = new go.Margin(0, 0, 0, 0);
        let gofont = "10pt sans-serif";
        let gominSize = new go.Size(16, 16);
        let gomaxSize = new go.Size(120, NaN);
        let gotextAlign = "center";
        let goeditable = true;

        myDiagram.linkTemplate =
        $(go.Link,
            $(go.Shape),                           // this is the link shape (the line)
            $(go.Shape, { toArrow: "Standard" }),  // this is an arrowhead
            $(go.TextBlock,                        // this is a Link label
            new go.Binding("text", "text"))
        );

      myDiagram.nodeTemplate =
        $(go.Node, "Auto",
          { locationSpot: go.Spot.Center },
          new go.Binding("location", "location", go.Point.parse).makeTwoWay(go.Point.stringify),
          $(go.Shape, "Circle",
            {
              fill: "white", stroke: "gray", strokeWidth: 2,
              portId: "", fromLinkable: true, toLinkable: true,
              fromLinkableDuplicates: true, toLinkableDuplicates: true,
              fromLinkableSelfNode: true, toLinkableSelfNode: true
            },
            new go.Binding("stroke", "color"),
            new go.Binding("figure")),
        //   $(go.TextBlock,
        //     {
        //       margin: new go.Margin(5, 5, 3, 5), font: "10pt sans-serif",
        //       minSize: new go.Size(16, 16), maxSize: new go.Size(120, NaN),
        //       textAlign: "center", editable: true
        //     },
        //     new go.Binding("text").makeTwoWay()),

            
            $(go.Panel, "Vertical",
                $(go.Picture,
                    { width: 32, height: 32, margin: 4 },
                    new go.Binding("source", "icon")),
            ),

            $(go.Panel, "Table",
                { defaultAlignment: go.Spot.Left },
                $(go.TextBlock, {
                    row: 1,
                    column: 0,
                    margin: gomargin,
                    font: gofont,
                    minSize: gominSize,
                    maxSize: gomaxSize,
                    textAlign: gotextAlign,
                    editable: goeditable,
                }, "V:"),
                $(go.TextBlock, {
                    row: 1,
                    column: 1,
                    margin: gomargin,
                    font: gofont,
                    minSize: gominSize,
                    maxSize: gomaxSize,
                    textAlign: gotextAlign,
                    editable: goeditable,
                }, new go.Binding("text", "v")),
                $(go.TextBlock, {
                    row: 2,
                    column: 0,
                    margin: gomargin,
                    font: gofont,
                    minSize: gominSize,
                    maxSize: gomaxSize,
                    textAlign: gotextAlign,
                    editable: goeditable,
                }, "I:"),
                $(go.TextBlock, {
                    row: 2,
                    column: 1,
                    margin: gomargin,
                    font: gofont,
                    minSize: gominSize,
                    maxSize: gomaxSize,
                    textAlign: gotextAlign,
                    editable: goeditable,
                }, new go.Binding("text", "i")),
                $(go.TextBlock, {
                    row: 3,
                    column: 0,
                    margin: gomargin,
                    font: gofont,
                    minSize: gominSize,
                    maxSize: gomaxSize,
                    textAlign: gotextAlign,
                    editable: goeditable,
                }, "R:"),
                $(go.TextBlock, {
                    row: 3,
                    column: 1,
                    margin: gomargin,
                    font: gofont,
                    minSize: gominSize,
                    maxSize: gomaxSize,
                    textAlign: gotextAlign,
                    editable: goeditable,
                }, new go.Binding("text", "r")),
            )
        );

      // initialize Palette
      myPalette =
        $(go.Palette, "myPaletteDiv",
          {
            nodeTemplate: myDiagram.nodeTemplate,
            contentAlignment: go.Spot.Center,
            layout:
              $(go.GridLayout,
                { wrappingColumn: 1, cellSize: new go.Size(2, 2) }),
            "ModelChanged": function(e) {     // just for demonstration purposes,
              if (e.isTransactionFinished) {  // show the model data in the page's TextArea
                document.getElementById("mySavedPaletteModel").textContent = e.model.toJson();
              }
            }
          });

        

      // now add the initial contents of the Palette
      myPalette.model.nodeDataArray = [
        { text: "Circle", color: "blue", figure: "Circle", v: "10000", i: "1000", r: "1", icon: "filemanagerr/preview/cat.png" },
        // { text: "Square", color: "purple", figure: "Square", v: "20000", i: "2000", r: "2" },
        // { text: "Ellipse", color: "orange", figure: "Ellipse", v: "30000", i: "3000", r: "3" },
        // { text: "Rectangle", color: "red", figure: "Rectangle", v: "40000", i: "4000", r: "4" },
        // { text: "Rounded\nRectangle", color: "green", figure: "RoundedRectangle", v: "50000", i: "5000", r: "5" },
        // { text: "Triangle", color: "purple", figure: "Triangle", v: "60000", i: "6000", r: "6" },
      ];

      // initialize Overview
      myOverview =
        $(go.Overview, "myOverviewDiv",
          {
            observed: myDiagram,
            contentAlignment: go.Spot.Center
          });

      var inspector = new Inspector('myInspectorDiv', myDiagram,
        {
          // uncomment this line to only inspect the named properties below instead of all properties on each object:
          // includesOwnProperties: false,
          properties: {
            "text": {},
            // key would be automatically added for nodes, but we want to declare it read-only also:
            "key": { readOnly: true, show: Inspector.showIfPresent },
            // color would be automatically added for nodes, but we want to declare it a color also:
            "color": { type: 'color' },
            "figure": {}
          }
        });

      load();
    }

    // Show the diagram's model in JSON format
    function save() {
      document.getElementById("mySavedModel").value = myDiagram.model.toJson();
      myDiagram.isModified = false;
    }
    function load() {
      myDiagram.model = go.Model.fromJson(document.getElementById("mySavedModel").value);
    }

    function addToPalette() {
      var node = myDiagram.selection.filter(function(p) { return p instanceof go.Node; }).first();
      if (node !== null) {
        myPalette.startTransaction();
        var item = myPalette.model.copyNodeData(node.data);
        myPalette.model.addNodeData(item);
        myPalette.commitTransaction("added item to palette");
      }
    }

    // The user cannot delete selected nodes in the Palette with the Delete key or Control-X,
    // but they can if they do so programmatically.
    function removeFromPalette() {
      myPalette.commandHandler.deleteSelection();
    }
    window.addEventListener('DOMContentLoaded', init);
</script>

<script type="text/javascript">

    let main = {
        'table': 'mcard',
        'tablemilestone': 'prjmrabmilestone',
        'tableheader': 'prjmrabheader',
        'tabledetail': 'prjmrabdetail',
        'sendtonext': 'Send to Purchase Order',
        'urlcode': '_mcard_cm',
        'datefilterdefault': 'a',
        'condition': {
            'create': {
                'c': true,
                'r': true,
                'u': true,
                'd': true,
                'v': false,
                'formdetail': true,
            },
            'read': {
                'c': true,
                'r': true,
                'u': true,
                'd': true,
                'v': true,
                'formdetail': true,
            },
            'update': {
                'c': true,
                'r': true,
                'u': true,
                'd': true,
                'v': false,
                'formdetail': true,
            },
            'delete': {
                'c': false,
                'r': false,
                'u': false,
                'd': true,
                'v': false,
                'formdetail': true,
            },
            'view': {
                'c': false,
                'r': true,
                'u': false,
                'd': false,
                'v': true,
                'formdetail': false,
            },
        }
    };

    let status = {
        'crudmaster': 'read',
        'cruddetail': 'read',
    };

    function conditionDetail() {
        main.condition[status.crudmaster]['formdetail'] ? $('#form-detail').show() : $('#form-detail').hide();
    }

    var statusgetlog = false;
    var statusgetmilestone = false;
    var statusgetheader = false;
    var statusgetdetail = false;

    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        datefilter2.from = start.format('YYYY-MM-DD');
        datefilter2.to = end.format('YYYY-MM-DD');
        getDatatable();
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    var daterangepicker = $('#reportrange').daterangepicker({
        opened: true,
        // expanded: true,
        // standalone: true,
        // parentElement: document.getElementById('pagepanel'),
        // anchorElement: document.getElementById('pagepanel'),
        // container: '#pagepanel',
        startDate: start,
        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);




    // MASTER

    const setToggle = (params) => {
        $(`#${params.from}`).is(":checked")
            ? $(`#${params.to}`).show()
            : $(`#${params.to}`).hide()
    }

    const addTab = (params) => {
        let isuser = $('#isuser').is(':checked');
        let isemployee = $('#isemployee').is(':checked');
        let iscustomer = $('#iscustomer').is(':checked');
        let issupplier = $('#issupplier').is(':checked');

        if (isuser || isemployee || iscustomer || issupplier) {
            $('#mcardothers').show();
        } else {
            $('#mcardothers').hide();
        }

        $(`#mcardothers .nav-link`).removeClass('active');
        $(`#mcardothers #tab-mcard${params.tab}`).addClass('active');
        $(`#mcardothers .tab-pane`).removeClass('show active');
        $(`#tab-mcard${params.tab}-content`).addClass('show active');
        $(`#tab-title`).text(params.tab[0].toUpperCase()+params.tab.substring(1))


        if (isuser) {
            $('#li-tab-mcarduser').show();
            $('#li-tab-mcardprivilidge').show();
        } else {
            $('#li-tab-mcarduser').hide();
            $('#li-tab-mcardprivilidge').hide();
        }

        if (isemployee) {
            $('#li-tab-mcardemployee').show();
        } else {
            $('#li-tab-mcardemployee').hide();
        }

        if (iscustomer) {
            $('#li-tab-mcardcustomer').show();
        } else {
            $('#li-tab-mcardcustomer').hide();
        }

        if (issupplier) {
            $('#li-tab-mcardsupplier').show();
        } else {
            $('#li-tab-mcardsupplier').hide();
        }
    }


    // END MASTER




    console.log('daterangepicker')
    console.log(document.getElementById('pagepanel'))

    var qty_tr_detail = 0;

    function setTab(params) {
        if (params.title) {
            $(`#${params.parent} .tab-title`).text(params.title);
        }

        $(`#${params.parent} li a`).removeClass('active');
        $(`#${params.parent} li #${params.element}`).addClass('active');
        $(`#${params.parent} .tab-content div`).removeClass('show active');
        $(`#${params.parent} .tab-content #${params.element}-content`).addClass('show active');
    }

    function changeMasterTanggal() {
        getGenerateCode();
        getMilestoneAndHeader({data:JSON.parse(localStorage.getItem(main.tablemilestone))});
    }

    function changeMasterTanggalDue() {
        getMilestoneAndHeader({data:JSON.parse(localStorage.getItem(main.tablemilestone))});
    }

    function changeTanggal(elfrom, elto) {
        $('#'+elto).val($('#'+elfrom).val())
    }

    function changeCardSupplier(elfrom, elto) {
        let idcardsupplier = $('#'+elfrom+' :selected');
        let prop = $('#'+elfrom+' :selected').attr('data-mlokasiprop-nama');
        let kab = $('#'+elfrom+' :selected').attr('data-mlokasikab-nama');
        let kec = $('#'+elfrom+' :selected').attr('data-mlokasikec-nama');
        let kel = $('#'+elfrom+' :selected').attr('data-mlokasikel-nama');

        $('#idcardsupplier-modal').val(idcardsupplier.val() ? idcardsupplier.text() : '');
        $('#suppliernama-modal').val(idcardsupplier.val() ? idcardsupplier.text() : '');
        $('#supplierlokasiprop-modal').val(prop);
        $('#supplierlokasikab-modal').val(kab);
        $('#supplierlokasikec-modal').val(kec);
        $('#supplierlokasikel-modal').val(kel);
        $('#idcardsupplier-modal').val(idcardsupplier.text())
    }

    function changeKodeInventor() {
        let idinventor = $('#idinventor :selected');
        $('#form-detail #namainventor').val(idinventor.val() ? idinventor.attr('data-invminventory-nama') : '');
        $('#form-detail #unitprice').val(idinventor.val() ? idinventor.attr('data-invminventory-purchaseprice') : '');
        $('#form-modal-detail #idinventor-modal').val(idinventor.val() ? idinventor.text() : '');
        $('#form-modal-detail #namainventor-modal').val(idinventor.val() ? idinventor.attr('data-invminventory-nama') : '');
    }

    function changeKodeInventorModal() {
        let idinventor = $('#idinventor-modal :selected');
        $('#form-modal-detail #namainventorori-modal').val(idinventor.val() ? idinventor.attr('data-invminventory-nama') : '');
        $('#form-modal-detail #namainventor-modal').val(idinventor.val() ? idinventor.attr('data-invminventory-nama') : '');
    }

    function changeNamaInventor() {
        let namainventor = $('#form-detail #namainventor').val();
        $('#form-modal-detail #namainventor-modal').val(namainventor);
    }

    // $('#form-header #idcardsupplier').on('change', function() {
    //     $('#form-modal-supplier #idcardsupplier-modal').val($('#form-header #idcardsupplier :selected').val()).trigger('change')
    // });
    // $('#form-modal-supplier #idcardsupplier-modal').on('custom', function(idcardsupplier) {
    //     $('#form-modal-supplier #idcardsupplier-modal').val(idcardsupplier)
    // });

    $('#form-header #idcompany').on('change', function() {
        $('#form-modal-detail #idcompany-modal').val($('#form-header #idcompany :selected').val()).trigger('change')
    });
    // $('#form-modal-detail #idcompany-modal').on('change', function() {
    //     $('#form-header #idcompany').val($('#form-modal-detail #idcompany-modal :selected').val()).trigger('change')
    // });

    $('#form-header #iddepartment').on('change', function() {
        $('#form-modal-detail #iddepartment-modal').val($('#form-header #iddepartment :selected').val()).trigger('change')
    });
    // $('#form-modal-detail #iddepartment-modal').on('change', function() {
    //     $('#form-header #iddepartment').val($('#form-modal-detail #iddepartment-modal :selected').val()).trigger('change')
    // });
    
    function changeUnitQty(elfrom, elto) {
        let unitqty = $('#'+elfrom).val();
        $('#'+elto).val(unitqty);
        $('#form-modal-detail [name="unitprice"]').val(13000);
        $('#form-modal-detail [name="hargatotal"]').val(unitqty*13000);
    }

    const chooseDetailPrev = (params) => {
        let idelement = params.idelement;
        let data = params.data;
        console.log(data)
        let milestone = JSON.parse(localStorage.getItem(main.tablemilestone));

        getSelect({
            from: milestone,
            selected: false,
            title: 'Milestone',
            targetelement: '#form-detail #detail-idmilestone'
        });

        $('#detail-noiddetail-'+idelement).val(data.noid);
        $('#detail-idmaster-'+idelement).val(data.idmaster);
        $('#detail-kodeprev-'+idelement).val(data.kodeprev);
        $('#detail-idinventor-'+idelement).val(data.idinventor).trigger('change');
        $('#detail-kodeinventor-'+idelement).val(data.kodeinventor).trigger('change');
        $('#detail-namainventor-'+idelement).val(data.namainventor);
        $('#detail-namainventor2-'+idelement).val(data.namainventor2);
        $('#detail-unitqtyoriginal-'+idelement).val(data.unitqtysisa);
        $('#detail-unitqty-'+idelement).val($('[name=searchtypeprev]:checked').val() == 1 ? data.unitqtysisa : '');
        $('#form-modal-detail #keterangan-modal').val(data.keterangan);
        $('#form-modal-detail #idgudang-modal').val(data.idgudang).trigger('change');
        $('#form-modal-detail #idcompany-modal').val(data.idcompany).trigger('change');
        $('#form-modal-detail #iddepartment-modal').val(data.iddepartment).trigger('change');
        $('#form-modal-detail #idsatuan-modal').val(data.idsatuan).trigger('change');
        $('#form-modal-detail #unitprice-modal').val(data.unitprice);
        $('#form-modal-detail #subtotal-modal').val(data.subtotal);
        $('#form-modal-detail #discount-modal').val(data.discount);
        $('#form-modal-detail #nilaidisc-modal').val(data.nilaidisc);
        $('#form-modal-detail #idtax-modal').val(data.idtax).trigger('change');
        $('#form-modal-detail #prosentax-modal').val(data.prosentax);
        $('#form-modal-detail #nilaitax-modal').val(data.nilaitax);
        $('#form-modal-detail #hargatotal-modal').val(data.hargatotal);

        $('#div-formadddetail-'+idelement).slideDown();
        $('#detail-unitqty-'+idelement).focus();

        hideModalDetailPrev();
    }

    const saveDetail = (params) => {
        let noiddetail = $('#form-detail #noiddetail').val();
        var idmilestone = $('#detail-idmilestone-'+params.idelement).val();
        var idheader = $('#detail-idheader-'+params.idelement).val();
        let idmaster = $('#detail-idmaster-'+params.idelement).val();
        let kodeprev = $('#detail-kodeprev-'+params.idelement).val();
        let idinventor = $('#detail-idinventor-'+params.idelement).val();
        let kodeinventor = $('#detail-idinventor-'+params.idelement+' :selected').attr('data-invminventory-kode');
        let namainventor = $('#detail-idinventor-'+params.idelement+' :selected').attr('data-invminventory-nama');
        let namainventor2 = $('#detail-namainventor-'+params.idelement).val();
        let unitprice = parseInt($('#form-modal-detail #unitprice-modal').val().replace(/[^0-9]/gi, ''));
        let unitqtyoriginal = $('#detail-unitqtyoriginal-'+params.idelement).val();
        let unitqty = $('#detail-unitqty-'+params.idelement).val();
        let subtotal = parseInt(unitqty)*parseInt(unitprice);
            
        let idcompany = $('#form-header #idcompany').val();
        let iddepartment = $('#form-header #iddepartment').val();
        let idgudang = $('#form-header #idgudang').val();
        let idcurrency = $('#form-header #idcurrency :selected').val();
        let namacurrency = $('#form-header #idcurrency :selected').attr('data-accmcurrency-nama');

        let keterangan = $('#form-modal-detail #keterangan-modal').val();
        let idsatuan = $('#form-modal-detail #idsatuan-modal :selected').val();
        let namasatuan = $('#form-modal-detail #idsatuan-modal :selected').attr('data-invmsatuan-nama');
        let konvsatuan = $('#detail-idinventor-'+params.idelement+' :selected').attr('data-invminventory-konversi');
        let unitqtysisa = unitqty;
        let discountvar = 0;
        let discount = 0;
        let nilaidisc = 0;
        let idtypetax = 0;
        let idtax = $('#detail-idinventor-'+params.idelement+' :selected').attr('data-invminventory-idtax');
        let prosentax = $('#detail-idinventor-'+params.idelement+' :selected').attr('data-invminventory-taxprocent')
        let nilaitax = parseInt(subtotal)*parseInt(prosentax)/100;
        let hargatotal = parseInt(subtotal)-parseInt(nilaidisc)+parseInt(nilaitax);

        let dataprd = localStorage.getItem(main.tabledetail);
        let formdetail = new Array();
        let formheader = new Array();
        let others = new Array();

        formdetail.push({name:"noid",value:noiddetail});
        formdetail.push({name:"idmilestone",value:idmilestone});
        formdetail.push({name:"idheader",value:idheader});
        formdetail.push({name:"kodeprev",value:kodeprev});
        formdetail.push({name:"idinventor",value:idinventor});
        formdetail.push({name:"kodeinventor",value:kodeinventor});
        formdetail.push({name:"namainventor",value:namainventor});
        formdetail.push({name:"namainventor2",value:namainventor2});
        formheader.push({name:"idcompany",value:idcompany});
        formheader.push({name:"iddepartment",value:iddepartment});
        formheader.push({name:"idgudang",value:idgudang});
        formheader.push({name:"idcurrency",value:idcurrency});
        formheader.push({name:"namacurrency",value:namacurrency});
        others.push({name:"keterangan",value:keterangan});
        others.push({name:"idsatuan",value:idsatuan});
        others.push({name:"namasatuan",value:namasatuan});
        others.push({name:"konvsatuan",value:konvsatuan});
        others.push({name:"unitqty",value:unitqty});
        others.push({name:"unitqtysisa",value:unitqtysisa});
        others.push({name:"unitprice",value:currency(unitprice)});
        others.push({name:"subtotal",value:currency(subtotal)});
        others.push({name:"discountvar",value:discountvar});
        others.push({name:"discount",value:discount});
        others.push({name:"nilaidisc",value:nilaidisc});
        others.push({name:"idtypetax",value:idtypetax});
        others.push({name:"idtax",value:idtax});
        others.push({name:"prosentax",value:prosentax});
        others.push({name:"nilaitax",value:nilaitax});
        others.push({name:"hargatotal",value:currency(hargatotal)});
        
        let allprd = formdetail.concat(formheader, others);
        let condition = {
            status: '',
            message: {
                title: '',
                text: ''
            },
            type: '',
        };

        if (idmilestone == '') {
            condition.status = 'notif';
            condition.message.title = "'Milestone' is required!";
            condition.type = 'warning';
        } else if (idheader == '') {
            condition.status = 'notif';
            condition.message.title = "'Header' is required!";
            condition.type = 'warning';
        } else if (unitqty == '') {
            condition.status = 'notif';
            condition.message.title = "'Qty Updated' is required!";
            condition.type = 'warning';
        } else if (parseInt(unitqty) > parseInt(unitqtyoriginal)) {
            condition.status = 'confirm';
            condition.message.title = 'Qty Updated melebihi Qty Sisa!';
            condition.message.text = 'Apakah ingin tetap melanjutkan transaksi?';
            condition.type = 'warning';
        } else if (parseInt(unitqty) < 1) {
            condition.status = 'notif';
            condition.message.title = 'Qty Updated minimal 1!'
            condition.type = 'warning';
        } else if (isNaN(parseInt(unitqty))) {
            condition.status = 'notif';
            condition.message.title = 'Qty Updated harus angka!'
            condition.type = 'warning';
        } else {
            if (statusadd) {
                let _datareplace = [];
                $.each(JSON.parse(localStorage.getItem(main.tablemilestone)), function(k,v){
                    _datareplace[k] = v;
                    if (v.noid == idmilestone) {
                        $.each(v.header, function(k2,v2){
                            _datareplace[k].header[k2] = v2;
                            if (v2.noid == idheader) {
                                // alert(idmilestone+'   '+idheader)
                                // console.log(_datareplace.header)
                                // return false
                                let _detail = {
                                    noid: noiddetail,
                                    idmilestone: idmilestone,
                                    idheader: idheader,
                                    kodeprev: kodeprev,
                                    idinventor: idinventor,
                                    kodeinventor: kodeinventor,
                                    namainventor: namainventor,
                                    namainventor2: namainventor2,
                                    idcompany: idcompany,
                                    iddepartment: iddepartment,
                                    idgudang: idgudang,
                                    idcurrency: idcurrency,
                                    namacurrency: namacurrency,
                                    keterangan: keterangan,
                                    idsatuan: idsatuan,
                                    namasatuan: namasatuan,
                                    konvsatuan: konvsatuan,
                                    unitqty: unitqty,
                                    unitqtysisa: unitqtysisa,
                                    unitprice: currency(unitprice),
                                    subtotal: currency(subtotal),
                                    discountvar: discountvar,
                                    discount: discount,
                                    nilaidisc: nilaidisc,
                                    idtypetax: idtypetax,
                                    idtax: idtax,
                                    prosentax: prosentax,
                                    nilaitax: nilaitax,
                                    hargatotal: currency(hargatotal),
                                };
                                console.log(_detail);
                                _datareplace[k].header[k2].detail.push(_detail);
                            }
                        });
                    }
                });

                localStorage.setItem(main.tablemilestone, JSON.stringify(_datareplace));
                
                $('#searchprev').focus();
                $('#form-detail div').slideUp();

                collapseHeader({idelement: params.idelement, alwaysopen: true})
                getMilestoneAndHeader({data:JSON.parse(localStorage.getItem(main.tablemilestone))});  
                cancelDetail()

            } else {
                let _dataprev = JSON.parse(localStorage.getItem(main.tablemilestone)).find(i => i.noid == idmilestone).header.find(i => i.noid == idheader).detail;
                let _datanext = {};
                $.each(allprd, function(k,v) {
                    _datanext[v.name] = v.value;
                });
                let _datamerge = _dataprev.concat(_datanext);
                let _datareplace = [];
                $.each(JSON.parse(localStorage.getItem(main.tablemilestone)), function(k,v){
                    _datareplace[k] = v;
                    if (v.noid == idmilestone) {
                        $.each(v.header, function(k2,v2){
                            if (v2.noid == idheader) {
                                _datareplace[k].header[k2] = v2;
                                _datareplace[k].header[k2].detail = _datamerge;
                            }
                        });
                    }
                });

                localStorage.setItem(main.tablemilestone, JSON.stringify(_datareplace));
                
                $('#searchprev').focus();
                $('#form-detail div').slideUp();

                collapseHeader({idelement: params.idelement, alwaysopen: true})

                getMilestoneAndHeader({data:JSON.parse(localStorage.getItem(main.tablemilestone))});  
                cancelDetail()
            }
        }

        if (condition.status == 'confirm') {
            Swal.fire({
                title: condition.message.title,
                text: condition.message.text,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                target: '#pagepanel'
            }).then((result) => {
                if (result.isConfirmed) {        
                    let _dataprev = [];

                    if (dataprd) {
                        let datamerge = JSON.parse(dataprd);
                        datamerge.push(allprd);
                        // datamerge[noiddetail] = allprd;
                        localStorage.setItem(main.tabledetail, JSON.stringify(datamerge));
                    } else {
                        let firstdata = new Array();
                        firstdata.push(allprd);
                        // firstdata[noiddetail] = allprd;
                        localStorage.setItem(main.tabledetail, JSON.stringify(firstdata));
                    }

                    $('#searchprev').focus();
                    $('#form-detail div').slideUp();
                    getDatatableDetail(localStorage.getItem(main.tabledetail))
                    cancelDetail()
                    return false;
                }
            })
        } else if (condition.status == 'notif') {
            $.notify({
                message: condition.message.title
            },{
                type: condition.type
            });
            return false;
        }
    }

    function updateDetail(params) {
        let _idmilestone = params.idmilestone;
        let _idheader = params.idheader;
        let _keydetail = params.keydetail;
        let _idinventor = $('#idinventor-modal :selected').val();
        let _kodeinventor = $('#idinventor-modal :selected').attr('data-invminventory-kode');
        let _namainventor = $('#idinventor-modal :selected').attr('data-invminventory-nama');
        let _namainventor2 = $('#namainventor-modal').val();
        let _keterangan = $('#keterangan-modal').val();
        // let _idcompany = $('#idcompany-modal :selected').val();
        // let _iddepartment = $('#iddepartment-modal :selected').val();
        let _idgudang = $('#idgudang-modal :selected').val();
        let _idsatuan = $('#idsatuan-modal :selected').val();
        let _konvsatuan = $('#idsatuan-modal :selected').attr('data-invmsatuan-konversi');
        let _unitqty = $('#unitqty-modal').val();
        let _unitqtysisa = $('#unitqtysisa-modal').val();
        let _unitprice = $('#unitprice-modal').val();
        let _subtotal = $('#subtotal-modal').val();
        let _discount = $('#discount-modal').val();
        let _nilaidisc = $('#nilaidisc-modal').val();
        let _idtax = $('#idtax-modal :selected').val();
        let _prosentax = $('#prosentax-modal').val();
        let _nilaitax = $('#nilaitax-modal').val();
        let _hargatotal = $('#hargatotal-modal').val();

        let _datanext = {
            idmilestone: _idmilestone,
            idheader: _idheader,
            keydetail: _keydetail,
            idinventor: _idinventor,
            kodeinventor: _kodeinventor,
            namainventor: _namainventor,
            namainventor2: _namainventor2,
            keterangan: _keterangan,
            // idcompany: _idcompany,
            // iddepartment: _iddepartment,
            idgudang: _idgudang,
            idsatuan: _idsatuan,
            konvsatuan: _konvsatuan,
            unitqty: _unitqty,
            unitqtysisa: _unitqtysisa,
            unitprice: _unitprice,
            subtotal: _subtotal,
            discount: _discount,
            nilaidisc: _nilaidisc,
            idtax: _idtax,
            prosentax: _prosentax,
            nilaitax: _nilaitax,
            hargatotal: _hargatotal,
        };

        let _datareplace = [];
        $.each(JSON.parse(localStorage.getItem(main.tablemilestone)), function(k,v){
            _datareplace[k] = v;
            if (v.noid == _idmilestone) {
                $.each(v.header, function(k2,v2){
                    _datareplace[k].header[k2] = v2;
                    if (v2.noid == _idheader) {
                        let _detail = [];
                        $.each(v2.detail, function(k3,v3){
                            _datareplace[k].header[k2].detail[k3] = v3;
                            if (k3 == _keydetail) {
                                _datanext.discountvar = v3.discountvar
                                _datanext.idtypetax = v3.idtypetax
                                _datanext.kodesatuan = v3.kodesatuan
                                _datanext.namasatuan = v3.namasatuan
                                _datanext.noid = v3.noid
                                _detail.push(_datanext);
                            } else {
                                _detail.push(v3);
                            }

                        });
                        _datareplace[k].header[k2].detail = _detail;
                    }
                });
            }
        });

        localStorage.setItem(main.tablemilestone, JSON.stringify(_datareplace));
        // getDatatableMilestone(localStorage.getItem(main.tablemilestone))
        getMilestoneAndHeader({data:JSON.parse(localStorage.getItem(main.tablemilestone))});      
        cancelDetail()
    }

    function defaultOperation() {
        // let subtotal = 0;
        // let total = 0;

        // $.each(JSON.parse(localStorage.getItem(main.tabledetail)), function(k, v) {
        //     $.each(v, function(k2, v2) {
        //         if (v2.name == 'hargatotal') {
        //             subtotal += $.isNumeric(v2.value) ? v2.value : parseInt(v2.value.replace(/[^0-9]/gi, ''))
        //             total += $.isNumeric(v2.value) ? v2.value : parseInt(v2.value.replace(/[^0-9]/gi, ''))
        //         }
        //     })
        // });
        
        // $('#subtotal').val(subtotal);
        // $('#total').val(total);
    }

    function editMilestone(params) {
        let data = {};
        let keymilestone;
        let idel = params.idelement;
        let iscollapse = $("#"+idel).hasClass('show');

        $.each(JSON.parse(localStorage.getItem(main.tablemilestone)), function(k,v){
            if (params.keymilestone == k) {
                data = v;
                keymilestone = k;
            }
        });
        
        if (data) {
            $('#'+idel+'-noid').val(data.noid);
            $('#'+idel+'-nama').val(data.nama);
            $('#'+idel+'-keterangan').val(data.keterangan);
            $('#'+idel+'-idcardpj').val(data.idcardpj).trigger('change');
            $('#'+idel+'-idcardpjwakil').val(data.idcardpjwakil).trigger('change');
            $('#'+idel+'-nourut').val(data.nourut);
            $('#'+idel+'-tanggalstart').val(data.tanggalstart);
            $('#'+idel+'-tanggalstop').val(data.tanggalstop);
        }

        if (params.type == 'view') {
            $('#'+idel+'-idinventor').attr('disabled','disabled');
            $('#'+idel+'-keterangan').attr('style','background-color: #E9ECEF !important').attr('readonly','readonly');
            $('#'+idel+'-idcardpj').attr('disabled', 'disabled');
            $('#'+idel+'-idcardpjwakil').attr('disabled', 'disabled');
            $('#'+idel+'-nourut').attr('style','background-color: #E9ECEF !important').attr('readonly','readonly');
            $('#'+idel+'-tanggalstart').attr('style','background-color: #E9ECEF !important').attr('readonly','readonly');
            $('#'+idel+'-tanggalstop').attr('style','background-color: #E9ECEF !important').attr('readonly','readonly');
        } else {
            $('#'+idel+'-idinventor').removeAttr('disabled');
            $('#'+idel+'-keterangan').removeAttr('style').removeAttr('readonly');
            $('#'+idel+'-idcardpj').removeAttr('disabled');
            $('#'+idel+'-idcardpjwakil').removeAttr('disabled');
            $('#'+idel+'-nourut').removeAttr('style').removeAttr('readonly');
            $('#'+idel+'-tanggalstart').removeAttr('style').removeAttr('readonly');
            $('#'+idel+'-tanggalstop').removeAttr('style').removeAttr('readonly');
        }

        // $('#btn-cancel-'+idel).show();
        // params.type == 'view' 
        //     ? $('#btn-save-'+idel).hide()
        //     : $('#btn-save-'+idel).show();
        // $('#btn-view-'+idel).hide();
        // $('#btn-edit-'+idel).hide();
        // $('#btn-remove-'+idel).hide();
        // $('#btn-collapse-'+idel).hide();

        $('#btn-addheader-'+idel).hide();
        $('#btn-cancel-'+idel).show();
        if (params.type == 'view') {
            $('#btn-save-'+idel).hide()
        } else {
            $('#btn-save-'+idel).show();
            // $('#'+idel+'-idinventor').select2('focus');
            $('#'+idel+'-idinventor').on('select2:close',function(e){$('#'+idel+'-keterangan').focus()})
            $('#'+idel+'-keterangan').on('keydown',function(e){if(e.keyCode==13){$('#'+idel+'-idcardpj').select2('focus');}})
            $('#'+idel+'-idcardpj').on('select2:close',function(e){$('#'+idel+'-idcardpjwakil').select2('focus')})
            $('#'+idel+'-idcardpjwakil').on('select2:close',function(e){$('#'+idel+'-tanggalstart').focus()})
            $('#'+idel+'-tanggalstart').on('keydown',function(e){if(e.keyCode==13){$('#'+idel+'-tanggalstop').focus();}})
            $('#'+idel+'-tanggalstop').on('keydown',function(e){if(e.keyCode==13){$('#btn-save-'+idel).focus();}})
        }
        $('#btn-saveheader-'+idel).hide();
        $('#btn-view-'+idel).hide();
        $('#btn-edit-'+idel).hide();
        $('#btn-remove-'+idel).hide();
        $('#btn-collapse-'+idel).hide();
        $('#div-form-'+idel).slideDown();

        $('#btn-updatemilestone').attr('onclick',`saveMilestone({type:'update',keymilestone:`+keymilestone+`})`)
        // console.log(_areaedit)
        // showFormMilestone({status:'edit'});
    }

    function editHeader(params) {
        let data = {};
        let idmaster = '';
        let idheader = '';
        let idel = params.idelement;

        $.each(JSON.parse(localStorage.getItem(main.tablemilestone)), function(k,v){
            $.each(v.header, function(k2,v2){
                if (params.keyheader == k2 && params.idmilestone == v2.idmilestone) {
                    data = v2;
                    idmaster = v2.idmaster;
                    idheader = v2.noid;
                    // break;
                }
            });
        });

        console.table(data);
        if (data) {
            $('#'+idel+'-noid').val(data.noid);
            $('#'+idel+'-idmilestone').val(data.idmilestone).trigger('change');
            $('#'+idel+'-nama').val(data.nama);
            $('#'+idel+'-keterangan').val(data.keterangan);
            $('#'+idel+'-idcardpj').val(data.idcardpj).trigger('change');
            $('#'+idel+'-idcardpjwakil').val(data.idcardpjwakil).trigger('change');
            $('#'+idel+'-nourut').val(data.nourut);
            $('#'+idel+'-tanggalstart').val(data.tanggalstart);
            $('#'+idel+'-tanggalstop').val(data.tanggalstop);
            $('#'+idel+'-tanggalfinished').val(data.tanggalfinished);
        }

        if (params.type == 'view') {
            $('#'+idel+'-idmilestone').attr('disabled', 'disabled');
            $('#'+idel+'-nama').attr('style','background-color: #E9ECEF !important').attr('readonly','readonly');
            $('#'+idel+'-keterangan').attr('style','background-color: #E9ECEF !important').attr('readonly','readonly');
            $('#'+idel+'-idcardpj').attr('disabled', 'disabled');
            $('#'+idel+'-idcardpjwakil').attr('disabled', 'disabled');
            $('#'+idel+'-nourut').attr('style','background-color: #E9ECEF !important').attr('readonly','readonly');
            $('#'+idel+'-tanggalstart').attr('style','background-color: #E9ECEF !important').attr('readonly','readonly');
            $('#'+idel+'-tanggalstop').attr('style','background-color: #E9ECEF !important').attr('readonly','readonly');
            $('#'+idel+'-tanggalfinished').attr('style','background-color: #E9ECEF !important').attr('readonly','readonly');
        } else {
            $('#'+idel+'-idmilestone').removeAttr('disabled');
            $('#'+idel+'-nama').removeAttr('style').removeAttr('readonly');
            $('#'+idel+'-keterangan').removeAttr('style').removeAttr('readonly');
            $('#'+idel+'-idcardpj').removeAttr('disabled');
            $('#'+idel+'-idcardpjwakil').removeAttr('disabled');
            $('#'+idel+'-nourut').removeAttr('style').removeAttr('readonly');
            $('#'+idel+'-tanggalstart').removeAttr('style').removeAttr('readonly');
            $('#'+idel+'-tanggalstop').removeAttr('style').removeAttr('readonly');
            $('#'+idel+'-tanggalfinished').removeAttr('style').removeAttr('readonly');
        }

        // $('#btn-cancel-'+idel).show();
        // params.type == 'view' 
        //     ? $('#btn-save-'+idel).hide()
        //     : $('#btn-save-'+idel).show();
        // $('#btn-view-'+idel).hide();
        // $('#btn-edit-'+idel).hide();
        // $('#btn-remove-'+idel).hide();
        $('#btn-cancel-'+idel).show();
        if (params.type == 'view') {
            $('#btn-save-'+idel).hide()
        } else {
            $('#btn-save-'+idel).show();
        }
        $('#btn-view-'+idel).hide();
        $('#btn-edit-'+idel).hide();
        $('#btn-remove-'+idel).hide();
        $('#btn-collapse-'+idel).hide();
        $('#div-form-'+idel).slideDown();

        $('#btn-updateheader').attr('onclick',`saveHeader({type:'update',keyheader:`+params.keyheader+`,idmilestone:`+data.idmilestone+`})`)

        // getDatatableDetail({idelement:idel,idmaster:idmaster,idheader:idheader})
        // showFormHeader({status:'edit'});
    }

    const editDetail = (params) => {
        let lsmilestone = JSON.parse(localStorage.getItem(main.tablemilestone));
        let detail = lsmilestone.find(i => i.noid == params.idmilestone).header.find(i => i.noid == params.idheader).detail[params.keydetail];

        status.cruddetail = 'update';


        $('#idmilestone').val(params.idmilestone);
        $('#idheader').val(params.idheader);
        $('#keydetail').val(params.keydetail);
        $('#idinventor-modal').val(detail.idinventor).trigger('change');
        $('#keterangan-modal').val(detail.keterangan);
        $('#idcompany-modal').val(detail.idcompany).trigger('change');
        $('#iddepartment-modal').val(detail.iddepartment).trigger('change');
        $('#idgudang-modal').val(detail.idgudang).trigger('change');
        $('#idsatuan-modal').val(detail.idsatuan).trigger('change');
        $('#konvsatuan-modal').val(detail.konvsatuan);
        $('#unitqty-modal').val(detail.unitqty);
        $('#unitqtysisa-modal').val(detail.unitqtysisa);
        $('#unitprice-modal').val(detail.unitprice);
        $('#subtotal-modal').val(detail.subtotal);
        $('#discount-modal').val(detail.discount);
        $('#discountvar-modal').val(detail.discountvar);
        $('#konvsatuan-modal').val(detail.konvsatuan);
        $('#nilaidisc-modal').val(detail.nilaidisc);
        $('#idtax-modal').val(detail.idtax).trigger('change');
        $('#prosentax-modal').val(detail.prosentax);
        $('#nilaitax-modal').val(detail.nilaitax);
        $('#hargatotal-modal').val(detail.hargatotal);

        $('#update-detail').attr('onclick', `updateDetail({
            keydetail:${params.keydetail},
            idheader:${params.idheader},
            idmilestone:${params.idmilestone}
        })`)
        $('#update-detail').show()

        hideFormMilestone()
        showModalDetail()
    }

    function cancelDetail() {
        unsetFormDetail();
        unsetFormModalDetail();
        $('#noiddetail').val('');
        $('#cancel-detail').hide();
        if (statusdetail) {
            $('#update-detail').hide();
            $('#save-detail').hide();
        } else {
            if (status.cruddetail == 'read') {
                $('#update-detail').hide();
                $('#save-detail').hide();
            } else {
                $('#update-detail').show();
                $('#save-detail').hide();
            }
        }
        hideModalDetail()
    }

    function unsetFormMilestoneAndHeader() {
        localStorage.removeItem(main.tablemilestone);
        $('#milestoneandheader').html('No Detail Data');
    }

    function unsetFormDetail() {
        $('#searchprev').val('');
        if (!$('#form-detail #idinventor').val() == '') {
            $('#form-detail #idinventor').val('').trigger('change')
        }
        $('#form-detail #namainventor').val('');
        $('#form-detail #unitprice').val('');
        $('#form-detail #unitqty').val('');
    }

    function unsetFormModalDetail() {
        $('#form-modal-detail #namainventor-modal').val('');
        $('#form-modal-detail #keterangan-modal').val('');
        // if (!$('#form-modal-detail #idgudang-modal').val() == '') {
        //     $('#form-modal-detail #idgudang-modal').val('')
        // }
        // if (!$('#form-modal-detail #idcompany-modal').val() == '') {
        //     $('#form-modal-detail #idcompany-modal').val('')
        // }
        // if (!$('#form-modal-detail #iddepartment-modal').val() == '') {
        //     $('#form-modal-detail #iddepartment-modal').val('')
        // }
        $('#form-modal-detail #idsatuan-modal').val('');
        $('#form-modal-detail #konvsatuan-modal').val('');
        $('#form-modal-detail #unitqty-modal').val('');
        $('#form-modal-detail #unitqtysisa-modal').val('');
        $('#form-modal-detail #unitprice-modal').val('');
        $('#form-modal-detail #subtotal-modal').val('');
        $('#form-modal-detail #discountvar-modal').val('');
        $('#form-modal-detail #discount-modal').val('');
        $('#form-modal-detail #nilaidisc-modal').val('');
        $('#form-modal-detail #idtypetax-modal').val('');
        $('#form-modal-detail #idtax-modal').val('');
        $('#form-modal-detail #prosentax-modal').val('');
        $('#form-modal-detail #nilaitax-modal').val('');
        $('#form-modal-detail #hargatotal-modal').val('');
    }

    function setDatatableDetail() {
        $('#table-detail').DataTable();
    }

    const saveData = (next_idstatustranc=null, next_nama=null, next_keterangan=null) => {
        status.crudmaster = 'read';
        status.cruddetail = 'read';

        let formmcard = $('#form-mcard').serializeArray();
        let formmcardgeneral = $('#form-mcardgeneral').serializeArray();
        let formmcardaddress = $('#form-mcardaddress').serializeArray();
        let formmcardcontact = $('#form-mcardcontact').serializeArray();
        let formuser = $('#form-mcarduser').serializeArray();
        let formprivilidge = $('#form-mcardprivilidge').serializeArray();
        let formemployee = $('#form-mcardemployee').serializeArray();
        let formcustomer = $('#form-mcardcustomer').serializeArray();
        let formsupplier = $('#form-mcardsupplier').serializeArray();
        let data = {};
        let _validation = {
            kode: {
                required: {
                    status: true,
                    message: "'Kode' is required!"
                }
            },
            nama: {
                required: {
                    status: true,
                    message: "'Nama' is required!"
                }
            },
            firstname: {
                required: {
                    status: true,
                    message: "'First Name' is required!"
                }
            },
            middlename: {
                required: {
                    status: true,
                    message: "'Middle Name' is required!"
                }
            },
            lastname: {
                required: {
                    status: true,
                    message: "'Last Name' is required!"
                }
            },
            gelar: {
                required: {
                    status: true,
                    message: "'Gelar' is required!"
                }
            },
            email: {
                required: {
                    status: true,
                    message: "'Email' is required!"
                }
            },
        };
        let _showvalidation = {
            status: false,
            message: ''
        };

        $('#btn-save-data').attr('disabled','disabled');

        $.each($('#form').serializeArray(), function(k, v){
            if (v.name in _validation) {
                if (_validation[v.name].required.status) {
                    if (v.value == '') {
                        _showvalidation.status = true;
                        _showvalidation.message += _validation[v.name].required.message+'<br>'
                    }
                }
            }
            data[v.name] = v.value;
        });

        if (_showvalidation.status) {
            $.notify({ message: _showvalidation.message },{ type: 'danger' });
            return false;
        }

        $.ajax({
            type: 'POST',
            header:{
                'X-CSRF-TOKEN': $('#token').val()
            },
            url: '/save'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('#token').val(),
                'noid': $('#noid').val(),
                'statusadd': JSON.stringify(statusadd),
                'formmcard': formmcard,
                'formmcardgeneral': formmcardgeneral,
                'formmcardaddress': formmcardaddress,
                'formmcardcontact': formmcardcontact,
                'formuser': formuser,
                'formprivilidge': formprivilidge,
                'formemployee': formemployee,
                'formcustomer': formcustomer,
                'formsupplier': formsupplier,
            },
            beforeSend: function(){
                // showLoading();
            },
            success: function(response) {
                if (!response.success) {
                    $.notify({ message: response.message },{ type: 'danger' });
                    hideLoading();
                    return false;
                }

                $.notify({message: response.message},{type: 'success'});
                
                getDatatable()
                hideModalSNST();
                cancelData()
            }
        });
    }

    const viewData = (noid) => {
        status.crudmaster = 'view';
        status.cruddetail = 'read';
        statusadd = false;
        statusdetail = true;
        
        $('#btn-save-data').hide();

        $.ajax({
            type: 'POST',
            header:{
                'X-CSRF-TOKEN': $('#token').val()
            },
            url: '/find'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('#token').val(),
                'noid': noid
            },
            success: function(response) {
                let d = response.data.find;
                let dg = response.data.findgeneral;
                let da = response.data.findaddress;
                let dco = response.data.findcontact;
                let du = response.data.finduser;
                let dp = response.data.findprivilidge;
                let de = response.data.findemployee;
                let dc = response.data.findcustomer;
                let ds = response.data.findsupplier;

                // CARD
                $('#noid').val(d.noid);
                $('#kode').val(d.kode);
                $('#firstname').val(d.firstname);
                $('#middlename').val(d.middlename);
                $('#lastname').val(d.lastname);
                $('#gelar').val(d.gelar);
                $('#email').val(d.email);
                $('#isgroupuser').prop('checked', d.isgroupuser == 1 ? true : false);
                $('#isuser').prop('checked', d.isuser == 1 ? true : false);
                $('#isemployee').prop('checked', d.isemployee == 1 ? true : false);
                $('#iscustomer').prop('checked', d.iscustomer == 1 ? true : false);
                $('#issupplier').prop('checked', d.issupplier == 1 ? true : false);
                $('#is3rdparty').prop('checked', d.is3rdparty == 1 ? true : false);
                $('#isactive').prop('checked', d.isactive == 1 ? true : false);

                // GENERAL
                $('#mcardgeneral_idgender').val(dg.idgender).trigger('change');
                $('#mcardgeneral_idreligion').val(dg.idreligion).trigger('change');
                $('#mcardgeneral_idmarital').val(dg.idmarital).trigger('change');
                $('#mcardgeneral_idnationality').val(dg.idnationality).trigger('change');
                $('#mcardgeneral_idrace').val(dg.idrace).trigger('change');
                $('#mcardgeneral_idbloodtype').val(dg.idbloodtype).trigger('change');
                $('#mcardgeneral_idlanguage1').val(dg.idlanguage1).trigger('change');
                $('#mcardgeneral_idlanguage2').val(dg.idlanguage2).trigger('change');
                $('#mcardgeneral_idlokasilahir').val(dg.idlokasilahir).trigger('change');
                $('#mcardgeneral_tempatlahir').val(dg.tempatlahir);
                $('#mcardgeneral_dobirth').val(dg.dobirth);

                // ADDRESS
                $('#mcardaddress_ismaincontact').prop('checked', da.ismaincontact == 1 ? true : false);
                $('#mcardaddress_idtypeaddress').val(da.idtypeaddress).trigger('change');
                $('#mcardaddress_alamat1').val(da.alamat1);
                $('#mcardaddress_alamat2').val(da.alamat2);
                $('#mcardaddress_alamatrt').val(da.alamatrt);
                $('#mcardaddress_alamatrw').val(da.alamatrw);
                $('#mcardaddress_idlokasiprop').val(da.idlokasiprop).trigger('change');
                $('#mcardaddress_idlokasikab').val(da.idlokasikab).trigger('change');
                $('#mcardaddress_idlokasikec').val(da.idlokasikec).trigger('change');
                $('#mcardaddress_idlokasikel').val(da.idlokasikel).trigger('change');

                // CONTACT
                $('#mcardcontact_idtypecontact').val(dco.idtypecontact).trigger('change');
                $('#mcardcontact_contactname').val(dco.contactname);
                $('#mcardcontact_contactvar').val(dco.contactvar);

                if (du) {
                    addTab({tab: 'user'});

                    $('#mcarduser_myusername').val(du.myusername);
                    // $('#mcarduser_mypassword').val(du.mypassword);
                    $('#mcarduser_groupuser').val(du.groupuser);
                }


                if (dp) {
                    addTab({tab: 'privilidge'});

                    $('#mcardprivilidge_idmenu').val(dp.idmenu);
                    $('#mcardprivilidge_menuparent').val(dp.menuparent);
                    $('#mcardprivilidge_menugroup').val(dp.menugroup);
                    $('#mcardprivilidge_isvisible').prop('checked', dp.isvisible == 1 ? true : false);
                    $('#mcardprivilidge_isenable').prop('checked', dp.isenable == 1 ? true : false);
                    $('#mcardprivilidge_isadd').prop('checked', dp.isadd == 1 ? true : false);
                    $('#mcardprivilidge_isedit').prop('checked', dp.isedit == 1 ? true : false);
                    $('#mcardprivilidge_isdelete').prop('checked', dp.isdelete == 1 ? true : false);
                    $('#mcardprivilidge_isdeleteother').prop('checked', dp.isdeleteother == 1 ? true : false);
                    $('#mcardprivilidge_isview').prop('checked', dp.isview == 1 ? true : false);
                    $('#mcardprivilidge_isreport').prop('checked', dp.isreport == 1 ? true : false);
                    $('#mcardprivilidge_isreportdetail').prop('checked', dp.isreportdetail == 1 ? true : false);
                    $('#mcardprivilidge_isconfirm').prop('checked', dp.isconfirm == 1 ? true : false);
                    $('#mcardprivilidge_isunconfirm').prop('checked', dp.isunconfirm == 1 ? true : false);
                    $('#mcardprivilidge_ispost').prop('checked', dp.ispost == 1 ? true : false);
                    $('#mcardprivilidge_isunpost').prop('checked', dp.isunpost == 1 ? true : false);
                    $('#mcardprivilidge_isprintdaftar').prop('checked', dp.isprintdaftar == 1 ? true : false);
                    $('#mcardprivilidge_isprintdetail').prop('checked', dp.isprintdetail == 1 ? true : false);
                    $('#mcardprivilidge_isshowaudit').prop('checked', dp.isshowaudit == 1 ? true : false);
                    $('#mcardprivilidge_isshowlog').prop('checked', dp.isshowlog == 1 ? true : false);
                    $('#mcardprivilidge_isexporttoxcl').prop('checked', dp.isexporttoxcl == 1 ? true : false);
                    $('#mcardprivilidge_isexporttocsv').prop('checked', dp.isexporttocsv == 1 ? true : false);
                    $('#mcardprivilidge_isexporttopdf').prop('checked', dp.isexporttopdf == 1 ? true : false);
                    $('#mcardprivilidge_isexporttohtml').prop('checked', dp.isexporttohtml == 1 ? true : false);
                    $('#mcardprivilidge_iscustom1').prop('checked', dp.iscustom1 == 1 ? true : false);
                    $('#mcardprivilidge_iscustom2').prop('checked', dp.iscustom2 == 1 ? true : false);
                    $('#mcardprivilidge_iscustom3').prop('checked', dp.iscustom3 == 1 ? true : false);
                    $('#mcardprivilidge_iscustom4').prop('checked', dp.iscustom4 == 1 ? true : false);
                    $('#mcardprivilidge_iscustom5').prop('checked', dp.iscustom5 == 1 ? true : false);
                    $('#mcardprivilidge_isproduction01').prop('checked', dp.isproduction01 == 1 ? true : false);
                    $('#mcardprivilidge_isproduction02').prop('checked', dp.isproduction02 == 1 ? true : false);
                    $('#mcardprivilidge_isproduction03').prop('checked', dp.isproduction03 == 1 ? true : false);
                    $('#mcardprivilidge_isproduction04').prop('checked', dp.isproduction04 == 1 ? true : false);
                    $('#mcardprivilidge_isproduction05').prop('checked', dp.isproduction05 == 1 ? true : false);
                    $('#mcardprivilidge_isproduction06').prop('checked', dp.isproduction06 == 1 ? true : false);
                }


                if (de) {
                    addTab({tab: 'employee'});

                    $('#mce_unitkerja').val(de.unitkerja).trigger('change');
                    $('#mce_nip').val(de.nip);
                    $('#mce_idcompany').val(de.idcompany).trigger('change');
                    $('#mce_iddepartment').val(de.iddepartment).trigger('change');
                    $('#mce_idgudang').val(de.idgudang).trigger('change');
                    $('#mce_reportto').val(de.reportto).trigger('change');
                    $('#mce_gelardepan').val(de.gelardepan);
                    $('#mce_gelarbelakang').val(de.gelarbelakang);
                    $('#mce_namalengkap').val(de.namalengkap);
                    $('#mce_idpangkatgol').val(de.idpangkatgol).trigger('change');
                    $('#mce_pangkatgol').val(de.pangkatgol);
                    $('#mce_pangkatgolkode').val(de.pangkatgolkode);
                    $('#mce_dotmtpangkat').val(de.dotmtpangkat);
                    $('#mce_jabatannama').val(de.jabatannama);
                    $('#mce_idstrata').val(de.idstrata);
                    $('#mce_pendidikannama').val(de.pendidikannama);
                    $('#mce_pendidikanlulus').val(de.pendidikanlulus);
                    $('#mce_tempatlahir').val(de.tempatlahir);
                    $('#mce_dob').val(de.dob);
                    $('#mce_idgender').val(de.idgender).trigger('change');
                    $('#mce_idreligion').val(de.idreligion).trigger('change');
                    $('#mce_idmarital').val(de.idmarital).trigger('change');
                    $('#mce_alamat').val(de.alamat);
                    $('#mce_alamatjl').val(de.alamatjl);
                    $('#mce_idlokasiprop').val(de.idlokasiprop).trigger('change');
                    $('#mce_idlokasikota').val(de.idlokasikota).trigger('change');
                    $('#mce_idlokasikec').val(de.idlokasikec).trigger('change');
                    $('#mce_idlokasikel').val(de.idlokasikel).trigger('change');
                    $('#mce_nomobile').val(de.nomobile);
                    $('#mce_nophone').val(de.nophone);
                }


                if (dc) {
                    addTab({tab: 'customer'});

                    $('#mcc_kode').val(dc.kode);
                    $('#mcc_nama').val(dc.nama);
                    $('#mcc_namaalias').val(dc.namaalias);
                    $('#mcc_keterangan').val(dc.keterangan);
                    $('#mcc_isactive').val(dc.isactive);
                    $('#mcc_npwp').val(dc.npwp);
                    $('#mcc_alamat1').val(dc.alamat1);
                    $('#mcc_alamat2').val(dc.alamat2);
                    $('#mcc_alamatrt').val(dc.alamatrt);
                    $('#mcc_alamatrw').val(dc.alamatrw);
                    $('#mcc_idlokasiprop').val(dc.idlokasiprop).trigger('change');
                    $('#mcc_idlokasikota').val(dc.idlokasikota).trigger('change');
                    $('#mcc_idlokasikec').val(dc.idlokasikec).trigger('change');
                    $('#mcc_idlokasikel').val(dc.idlokasikel).trigger('change');
                    $('#mcc_nophone').val(dc.nophone);
                    $('#mcc_nofax').val(dc.nofax);
                    $('#mcc_email').val(dc.email);
                }


                if (ds) {
                    addTab({tab: 'supplier'});

                    $('#mcs_nama').val(ds.nama);
                    $('#mcs_alamat').val(ds.alamat);
                    $('#mcs_alamatjl').val(ds.alamatjl);
                    $('#mcs_idlokasiprop').val(ds.idlokasiprop).trigger('change');
                    $('#mcs_idlokasikota').val(ds.idlokasikota).trigger('change');
                    $('#mcs_idlokasikec').val(ds.idlokasikec).trigger('change');
                    $('#mcs_idlokasikel').val(ds.idlokasikel).trigger('change');
                    $('#mcs_alamatkec').val(ds.alamatkec);
                    $('#mcs_corebussiness').val(ds.corebussiness);
                    $('#mcs_npwp').val(ds.npwp);
                    $('#mcs_nomobile').val(ds.nomobile);
                    $('#mcs_nophone').val(ds.nophone);
                    $('#mcs_nofax').val(ds.nofax);
                    $('#mcs_companyemail').val(ds.companyemail);
                    $('#mcs_companyweb').val(ds.companyweb);
                    $('#mcs_termijnpayment').val(ds.termijnpayment);
                }

                disableForm();
                disableFormGeneral();
                disableFormAddress();
                disableFormContact();
                disableFormUser();
                disableFormPrivilidge();
                disableFormEmployee();
                disableFormCustomer();
                disableFormSupplier();

                // $('#form-detail').hide();
                $('#tools-dropdown').hide();
                $('#from-departement').show();
                $('#tabledepartemen').hide();
                $('#tool-kedua').show();
                $('#tool-pertama').hide();
                $('#space_filter_button').hide();
                $('#tool_refresh').hide();
                $('#tool_filter').hide()
                $('#tool_refresh_up').hide();
            }
        });
    }

    function getButtonSaveTo(pr,mttts,fromview) {
        let btnsaveto = '';
        if (fromview) {
            if (mttts.isinternal == 1 && mttts.isexternal == 0) {
                if (mttts.prev_idstatusbase != null && mttts.next_idstatusbase != null) {
                    if (mttts.next_idstatusbase == 4) {
                        btnsaveto = `<a class="btn btn-sm btn-success flat btn-view" style="background-color: `+mttts.next_classcolor+`; color: white; margin-right: -4px" onclick="showModalSendToOrder(`+pr.noid+`,`+pr.idtypetranc+`, true)">
                                        <i class="icon-save text-light"></i> Send to `+mttts.next_table+`
                                    </a>
                                    <div class="btn-group btn-view">
                                        <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-sm flat" style="background-color: `+mttts.next_classcolor+`; color: white">
                                            <i class="icon-angle-down pr-0 text-light"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right" style="min-width: 200px; padding: 5px">
                                            <div class="row">
                                                <button type="button" class="btn col-md-12" style="background-color: `+mttts.prev_classcolor+`; color: white" onclick="showModalSNST(`+pr.noid+`,`+pr.idtypetranc+`, `+mttts.prev_noid+`, '`+mttts.prev_nama+`', true)"><i class="icon-save text-light"></i> Save to `+mttts.prev_nama+`</button>
                                            </div>
                                        </div>
                                    </div>`;
                    } else {
                        btnsaveto = `<a class="btn btn-sm flat btn-view" style="background-color: `+mttts.next_classcolor+`; color: white; margin-right: -4px" onclick="showModalSNST(`+pr.noid+`,`+pr.idtypetranc+`, `+mttts.next_noid+`, '`+mttts.next_nama+`', true)">
                                        <i class="icon-save text-light"></i> Save to `+mttts.next_nama+`
                                    </a>
                                    <div class="btn-group btn-view">
                                        <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-sm flat" style="background-color: `+mttts.next_classcolor+`; color: white">
                                            <i class="icon-angle-down pr-0 text-light"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right" style="min-width: 200px; padding: 5px">
                                            <div class="row">
                                                <button type="button" class="btn col-md-12" style="background-color: `+mttts.prev_classcolor+`; color: white" onclick="showModalSNST(`+pr.noid+`,`+pr.idtypetranc+`, `+mttts.prev_noid+`, '`+mttts.prev_nama+`', true)"><i class="icon-save text-light"></i> Save to `+mttts.prev_nama+`</button>
                                            </div>
                                        </div>
                                    </div>`;
                    }
                } else if (mttts.prev_idstatusbase == null && mttts.next_idstatusbase != null) {
                    btnsaveto = `<a class="btn btn-sm flat btn-view" style="background-color: `+mttts.next_classcolor+`; color: white;" onclick="showModalSNST(`+pr.noid+`,`+pr.idtypetranc+`, `+mttts.next_noid+`, '`+mttts.next_nama+`', true)">
                                    <i class="icon-save text-light"></i> Save to `+mttts.next_nama+`
                                </a>`;
                }
            } else {
                btnsaveto += `<div class="btn-group">
                                    <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-success btn-sm flat">
                                        Set Status <i class="icon-angle-down pr-0"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" style="min-width: 200px; padding: 0px 5px 5px 5px">`;

                $.each(mttts.btnexternal, function(k,v) {
                    btnsaveto += `<div class="row" style="margin-top: 5px">
                                        <button type="button" class="btn col-md-12" style="background-color: `+v.classcolor+`; color: white;" onmouseover="mouseOver('save-to-issued', '#1f8e87')" onmouseout="mouseOut('save-to-issued', '`+v.classcolor+`')" id="save-to-issued" onclick="showModalSNST(`+pr.noid+`, 501, `+v.noid+`, '`+v.nama+`')"><i class="`+v.classicon+` text-light"></i> <span>Set to `+v.nama+`<span></button>
                                    </div>`;
                })

                btnsaveto += `</div>
                            </div>`;
            }
        } else {
            btnsaveto += `<a class="btn btn-sm btn-success flat" id="save-standard" style="margin-right: -4px" onclick="saveData()">
                        <i class="icon-save"></i> Save
                    </a>
                    <div class="btn-group btn-view">
                        <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-sm btn-success flat" style="color: white">
                            <i class="icon-angle-down pr-0 text-light"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" style="min-width: 200px; padding: 5px">
                            <div class="row">
                                <button type="button" class="btn col-md-12" style="background-color: `+mttts.next_classcolor+`; color: white" onclick="showModalSNST(`+pr.noid+`,`+pr.idtypetranc+`, `+mttts.next_noid+`, '`+mttts.next_nama+`', `+fromview+`)"><i class="icon-save text-light"></i> Save to `+mttts.next_nama+`</button>
                            </div>
                        </div>
                    </div>`;
        }
        return btnsaveto;
    }

    function viewLog(noid) {
        alert(noid)
    }

    function printData(noid, urlprint) {
        window.open('http://'+$(location).attr('host')+'/'+noid+'/'+urlprint, '_blank');
    }

    function sendToNext(noid) {
        let formmodalstn = $('#form-modal-stn').serializeArray();
        $.ajax({
            type: 'POST',
            header:{
                'X-CSRF-TOKEN': $('#token').val()
            },
            url: '/send_to_next'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('#token').val(),
                'noid': noid,
                'idcardsupplier': formmodalstn[0].value,
            },
            success: function(response) {
                if (response.success) {
                    $.notify({
                        message: response.message 
                    },{
                        type: 'success'
                    });
                } else{
                    $.notify({
                        message: response.message 
                    },{
                        type: 'danger'
                    });
                }
                getDatatable();
                hideModalSendToOrder();
            }
        });
    }

    function SNST(noid, idstatustranc, statustranc, fromview) {
        let idtypetranc = $('#form-modal-snst #idtypetranc-snst').val();
        let answer = $('#form-modal-snst #answer-snst').val();
        let keterangan = $('#form-modal-snst #keterangan-snst').val();

        if (idstatustranc == 6024) 
            if (!answer) { alert("'Answer' is required "); return false; }

        $.ajax({
            type: 'POST',
            header:{
                'X-CSRF-TOKEN': $('#token').val()
            },
            url: '/snst'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('#token').val(),
                'noid': noid,
                'idstatustranc': idstatustranc,
                'statustranc': statustranc,
                'idtypetranc': idtypetranc,
                'answer': answer,
                'keterangan': keterangan,
            },
            success: function(response) {
                getDatatable();
                if (response.success) {
                    $.notify({
                        message: response.message 
                    },{
                        type: 'success'
                    });
                } else{
                    $.notify({
                        message: response.message 
                    },{
                        type: 'danger'
                    });
                }
                hideModalSNST();
                if (fromview) {
                    cancelData();
                }
            }
        });
    }

    function DIV(noid, idstatustranc) {
        let ist = $('#form-modal-div #idstatustranc-div :selected').val();

        $.ajax({
            type: 'POST',
            header:{
                'X-CSRF-TOKEN': $('#token').val()
            },
            url: '/div'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('#token').val(),
                'noid': noid,
                'idtypetranc': $('#form-modal-div #idtypetranc-div').val(),
                'idstatustranc': ist,
                'statustranc': $('#form-modal-div #idstatustranc-div :selected').attr('data-mtypetranctypestatus-nama'),
                'keterangan': $('#form-modal-div #keterangan-div').val(),
            },
            success: function(response) {
                getDatatable();
                if (response.success) {
                    $.notify({
                        message: response.message 
                    },{
                        type: 'success'
                    });
                } else{
                    $.notify({
                        message: response.message 
                    },{
                        type: 'danger'
                    });
                }
                hideModalDIV();
            }
        });
    }

    function SAPCF(noid) {
        let idtypetranc = $('#modal-sapcf #idtypetranc-sapcf').val();
        let idstatustranc = $('#modal-sapcf #idstatustranc-sapcf :selected').val();
        let statustranc = $('#modal-sapcf #idstatustranc-sapcf :selected').attr('data-mtypetranctypestatus-nama');
        let keterangan = $('#modal-sapcf #keterangan-sapcf').val();

        if (idstatustranc == '') {
            alert('Status Tranc is required');
            $('#modal-sapcf #idstatustranc-sapcf').select2('focus');
            return false;
        }
        if (keterangan == '') {
            alert('Keterangan is required');
            $('#modal-sapcf #keterangan-sapcf').focus();
            return false;
        }

        $.ajax({
            type: 'POST',
            header:{
                'X-CSRF-TOKEN': $('#token').val()
            },
            url: '/sapcf'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('#token').val(),
                'noid': noid,
                'idtypetranc': idtypetranc,
                'idstatustranc': idstatustranc,
                'statustranc': statustranc,
                'keterangan': keterangan,
            },
            success: function(response) {
                if (!response.success) {
                    $.notify({message: response.message},{type: 'danger'});
                } else{
                    $.notify({message: response.message},{type: 'success'});
                }

                getDatatable();
                hideModalSAPCF();
            }
        });
    }

    function setPending(noid) {
        $.ajax({
            type: 'POST',
            header:{
                'X-CSRF-TOKEN': $('#token').val()
            },
            url: '/set_pending'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('#token').val(),
                'noid': noid,
                'idtypetranc': $('#form-modal-set-pending #idtypetranc-pending').val(),
                'keterangan': $('#form-modal-set-pending #keterangan-pending').val(),
            },
            success: function(response) {
                getDatatable();
                if (response.success) {
                    $.notify({
                        message: response.message 
                    },{
                        type: 'success'
                    });
                } else{
                    $.notify({
                        message: response.message 
                    },{
                        type: 'danger'
                    });
                }
                hideModalSetPending();
            }
        });
    }

    function setCanceled(noid) {
        $.ajax({
            type: 'POST',
            header:{
                'X-CSRF-TOKEN': $('#token').val()
            },
            url: '/set_canceled'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('#token').val(),
                'noid': noid,
                'idtypetranc': $('#form-modal-set-canceled #idtypetranc-canceled').val(),
                'keterangan': $('#form-modal-set-canceled #keterangan-canceled').val(),
            },
            success: function(response) {
                getDatatable();
                if (response.success) {
                    $.notify({
                        message: response.message 
                    },{
                        type: 'success'
                    });
                } else{
                    $.notify({
                        message: response.message 
                    },{
                        type: 'danger'
                    });
                }
                hideModalSetCanceled();
            }
        });
    }

    const editData = (noid) => {
        ready = false;
        status.crudmaster = 'update';
        status.cruddetail = 'read';
        statusadd = false;
        statusdetail = false;

        $('#btn-save-data').show();

        $.ajax({
            type: 'POST',
            header: {
                'X-CSRF-TOKEN': $('#token').val()
            },
            url: '/find'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('#token').val(),
                'noid': noid
            },
            success: function(response) {
                let d = response.data.find;
                let dg = response.data.findgeneral;
                let da = response.data.findaddress;
                let dco = response.data.findcontact;
                let du = response.data.finduser;
                let dp = response.data.findprivilidge;
                let de = response.data.findemployee;
                let dc = response.data.findcustomer;
                let ds = response.data.findsupplier;

                // CARD
                $('#noid').val(d.noid);
                $('#kode').val(d.kode);
                $('#firstname').val(d.firstname);
                $('#middlename').val(d.middlename);
                $('#lastname').val(d.lastname);
                $('#gelar').val(d.gelar);
                $('#email').val(d.email);
                $('#isgroupuser').prop('checked', d.isgroupuser == 1 ? true : false);
                $('#isuser').prop('checked', d.isuser == 1 ? true : false);
                $('#isemployee').prop('checked', d.isemployee == 1 ? true : false);
                $('#iscustomer').prop('checked', d.iscustomer == 1 ? true : false);
                $('#issupplier').prop('checked', d.issupplier == 1 ? true : false);
                $('#is3rdparty').prop('checked', d.is3rdparty == 1 ? true : false);
                $('#isactive').prop('checked', d.isactive == 1 ? true : false);

                // GENERAL
                $('#mcardgeneral_idgender').val(dg.idgender).trigger('change');
                $('#mcardgeneral_idreligion').val(dg.idreligion).trigger('change');
                $('#mcardgeneral_idmarital').val(dg.idmarital).trigger('change');
                $('#mcardgeneral_idnationality').val(dg.idnationality).trigger('change');
                $('#mcardgeneral_idrace').val(dg.idrace).trigger('change');
                $('#mcardgeneral_idbloodtype').val(dg.idbloodtype).trigger('change');
                $('#mcardgeneral_idlanguage1').val(dg.idlanguage1).trigger('change');
                $('#mcardgeneral_idlanguage2').val(dg.idlanguage2).trigger('change');
                $('#mcardgeneral_idlokasilahir').val(dg.idlokasilahir).trigger('change');
                $('#mcardgeneral_tempatlahir').val(dg.tempatlahir);
                $('#mcardgeneral_dobirth').val(dg.dobirth);

                // ADDRESS
                $('#mcardaddress_ismaincontact').prop('checked', da.ismaincontact == 1 ? true : false);
                $('#mcardaddress_idtypeaddress').val(da.idtypeaddress).trigger('change');
                $('#mcardaddress_alamat1').val(da.alamat1);
                $('#mcardaddress_alamat2').val(da.alamat2);
                $('#mcardaddress_alamatrt').val(da.alamatrt);
                $('#mcardaddress_alamatrw').val(da.alamatrw);
                $('#mcardaddress_idlokasiprop').val(da.idlokasiprop).trigger('change');
                $('#mcardaddress_idlokasikab').val(da.idlokasikab).trigger('change');
                $('#mcardaddress_idlokasikec').val(da.idlokasikec).trigger('change');
                $('#mcardaddress_idlokasikel').val(da.idlokasikel).trigger('change');

                // CONTACT
                $('#mcardcontact_idtypecontact').val(dco.idtypecontact).trigger('change');
                $('#mcardcontact_contactname').val(dco.contactname);
                $('#mcardcontact_contactvar').val(dco.contactvar);

                if (du) {
                    addTab({tab: 'user'});

                    $('#mcarduser_myusername').val(du.myusername);
                    // $('#mcarduser_mypassword').val(du.mypassword);
                    $('#mcarduser_groupuser').val(du.groupuser);
                }


                if (dp) {
                    addTab({tab: 'privilidge'});

                    $('#mcardprivilidge_idmenu').val(dp.idmenu);
                    $('#mcardprivilidge_menuparent').val(dp.menuparent);
                    $('#mcardprivilidge_menugroup').val(dp.menugroup);
                    $('#mcardprivilidge_isvisible').prop('checked', dp.isvisible == 1 ? true : false);
                    $('#mcardprivilidge_isenable').prop('checked', dp.isenable == 1 ? true : false);
                    $('#mcardprivilidge_isadd').prop('checked', dp.isadd == 1 ? true : false);
                    $('#mcardprivilidge_isedit').prop('checked', dp.isedit == 1 ? true : false);
                    $('#mcardprivilidge_isdelete').prop('checked', dp.isdelete == 1 ? true : false);
                    $('#mcardprivilidge_isdeleteother').prop('checked', dp.isdeleteother == 1 ? true : false);
                    $('#mcardprivilidge_isview').prop('checked', dp.isview == 1 ? true : false);
                    $('#mcardprivilidge_isreport').prop('checked', dp.isreport == 1 ? true : false);
                    $('#mcardprivilidge_isreportdetail').prop('checked', dp.isreportdetail == 1 ? true : false);
                    $('#mcardprivilidge_isconfirm').prop('checked', dp.isconfirm == 1 ? true : false);
                    $('#mcardprivilidge_isunconfirm').prop('checked', dp.isunconfirm == 1 ? true : false);
                    $('#mcardprivilidge_ispost').prop('checked', dp.ispost == 1 ? true : false);
                    $('#mcardprivilidge_isunpost').prop('checked', dp.isunpost == 1 ? true : false);
                    $('#mcardprivilidge_isprintdaftar').prop('checked', dp.isprintdaftar == 1 ? true : false);
                    $('#mcardprivilidge_isprintdetail').prop('checked', dp.isprintdetail == 1 ? true : false);
                    $('#mcardprivilidge_isshowaudit').prop('checked', dp.isshowaudit == 1 ? true : false);
                    $('#mcardprivilidge_isshowlog').prop('checked', dp.isshowlog == 1 ? true : false);
                    $('#mcardprivilidge_isexporttoxcl').prop('checked', dp.isexporttoxcl == 1 ? true : false);
                    $('#mcardprivilidge_isexporttocsv').prop('checked', dp.isexporttocsv == 1 ? true : false);
                    $('#mcardprivilidge_isexporttopdf').prop('checked', dp.isexporttopdf == 1 ? true : false);
                    $('#mcardprivilidge_isexporttohtml').prop('checked', dp.isexporttohtml == 1 ? true : false);
                    $('#mcardprivilidge_iscustom1').prop('checked', dp.iscustom1 == 1 ? true : false);
                    $('#mcardprivilidge_iscustom2').prop('checked', dp.iscustom2 == 1 ? true : false);
                    $('#mcardprivilidge_iscustom3').prop('checked', dp.iscustom3 == 1 ? true : false);
                    $('#mcardprivilidge_iscustom4').prop('checked', dp.iscustom4 == 1 ? true : false);
                    $('#mcardprivilidge_iscustom5').prop('checked', dp.iscustom5 == 1 ? true : false);
                    $('#mcardprivilidge_isproduction01').prop('checked', dp.isproduction01 == 1 ? true : false);
                    $('#mcardprivilidge_isproduction02').prop('checked', dp.isproduction02 == 1 ? true : false);
                    $('#mcardprivilidge_isproduction03').prop('checked', dp.isproduction03 == 1 ? true : false);
                    $('#mcardprivilidge_isproduction04').prop('checked', dp.isproduction04 == 1 ? true : false);
                    $('#mcardprivilidge_isproduction05').prop('checked', dp.isproduction05 == 1 ? true : false);
                    $('#mcardprivilidge_isproduction06').prop('checked', dp.isproduction06 == 1 ? true : false);
                }


                if (de) {
                    addTab({tab: 'employee'});

                    $('#mce_unitkerja').val(de.unitkerja).trigger('change');
                    $('#mce_nip').val(de.nip);
                    $('#mce_idcompany').val(de.idcompany).trigger('change');
                    $('#mce_iddepartment').val(de.iddepartment).trigger('change');
                    $('#mce_idgudang').val(de.idgudang).trigger('change');
                    $('#mce_reportto').val(de.reportto).trigger('change');
                    $('#mce_gelardepan').val(de.gelardepan);
                    $('#mce_gelarbelakang').val(de.gelarbelakang);
                    $('#mce_namalengkap').val(de.namalengkap);
                    $('#mce_idpangkatgol').val(de.idpangkatgol).trigger('change');
                    $('#mce_pangkatgol').val(de.pangkatgol);
                    $('#mce_pangkatgolkode').val(de.pangkatgolkode);
                    $('#mce_dotmtpangkat').val(de.dotmtpangkat);
                    $('#mce_jabatannama').val(de.jabatannama);
                    $('#mce_idstrata').val(de.idstrata);
                    $('#mce_pendidikannama').val(de.pendidikannama);
                    $('#mce_pendidikanlulus').val(de.pendidikanlulus);
                    $('#mce_tempatlahir').val(de.tempatlahir);
                    $('#mce_dob').val(de.dob);
                    $('#mce_idgender').val(de.idgender).trigger('change');
                    $('#mce_idreligion').val(de.idreligion).trigger('change');
                    $('#mce_idmarital').val(de.idmarital).trigger('change');
                    $('#mce_alamat').val(de.alamat);
                    $('#mce_alamatjl').val(de.alamatjl);
                    $('#mce_idlokasiprop').val(de.idlokasiprop).trigger('change');
                    $('#mce_idlokasikota').val(de.idlokasikota).trigger('change');
                    $('#mce_idlokasikec').val(de.idlokasikec).trigger('change');
                    $('#mce_idlokasikel').val(de.idlokasikel).trigger('change');
                    $('#mce_nomobile').val(de.nomobile);
                    $('#mce_nophone').val(de.nophone);
                }


                if (dc) {
                    addTab({tab: 'customer'});

                    $('#mcc_kode').val(dc.kode);
                    $('#mcc_nama').val(dc.nama);
                    $('#mcc_namaalias').val(dc.namaalias);
                    $('#mcc_keterangan').val(dc.keterangan);
                    $('#mcc_isactive').val(dc.isactive);
                    $('#mcc_npwp').val(dc.npwp);
                    $('#mcc_alamat1').val(dc.alamat1);
                    $('#mcc_alamat2').val(dc.alamat2);
                    $('#mcc_alamatrt').val(dc.alamatrt);
                    $('#mcc_alamatrw').val(dc.alamatrw);
                    $('#mcc_idlokasiprop').val(dc.idlokasiprop).trigger('change');
                    $('#mcc_idlokasikota').val(dc.idlokasikota).trigger('change');
                    $('#mcc_idlokasikec').val(dc.idlokasikec).trigger('change');
                    $('#mcc_idlokasikel').val(dc.idlokasikel).trigger('change');
                    $('#mcc_nophone').val(dc.nophone);
                    $('#mcc_nofax').val(dc.nofax);
                    $('#mcc_email').val(dc.email);
                }


                if (ds) {
                    addTab({tab: 'supplier'});

                    $('#mcs_nama').val(ds.nama);
                    $('#mcs_alamat').val(ds.alamat);
                    $('#mcs_alamatjl').val(ds.alamatjl);
                    $('#mcs_idlokasiprop').val(ds.idlokasiprop).trigger('change');
                    $('#mcs_idlokasikota').val(ds.idlokasikota).trigger('change');
                    $('#mcs_idlokasikec').val(ds.idlokasikec).trigger('change');
                    $('#mcs_idlokasikel').val(ds.idlokasikel).trigger('change');
                    $('#mcs_alamatkec').val(ds.alamatkec);
                    $('#mcs_corebussiness').val(ds.corebussiness);
                    $('#mcs_npwp').val(ds.npwp);
                    $('#mcs_nomobile').val(ds.nomobile);
                    $('#mcs_nophone').val(ds.nophone);
                    $('#mcs_nofax').val(ds.nofax);
                    $('#mcs_companyemail').val(ds.companyemail);
                    $('#mcs_companyweb').val(ds.companyweb);
                    $('#mcs_termijnpayment').val(ds.termijnpayment);
                }

                undisableForm();
                undisableFormGeneral();
                undisableFormAddress();
                undisableFormContact();
                undisableFormUser();
                undisableFormPrivilidge();
                undisableFormEmployee();
                undisableFormCustomer();
                undisableFormSupplier();


                $('#tools-dropdown').hide(); // Hide button tools
                $('#tabledepartemen').hide(); // Hide datatable
                $('#tool-pertama').hide(); // Show buton add data
                $('#space_filter_button').hide(); // Hide filter di datatable
                $('#tool_refresh').hide(); // Hide button refresh
                $('#from-departement').show(); // Show form
                $('#tool-kedua').show(); // Show button cancel
                
                ready = true;
            }
        });
    }

    function removeData(noid) {
        $.ajax({
            type: 'POST',
            header:{
                'X-CSRF-TOKEN': $('#token').val()
            },
            url: '/delete'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('#token').val(),
                'noid': noid
            },
            success: function(response) {
                $.notify({
                    message: response.message 
                },{
                    type: 'success'
                });
                getDatatable()
                hideModalDelete()
            }
        });
    }

    function removeMilestone(params) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You want to delete this milestone?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            target: '#pagepanel'
        }).then((result) => {
            if (result.isConfirmed) {
                let _data = [];
                $.each(JSON.parse(localStorage.getItem(main.tablemilestone)), function(k, v) {
                    if (params.keymilestone != k) {
                        _data.push(v);
                    }
                })
                localStorage.removeItem(main.tablemilestone)
                localStorage.setItem(main.tablemilestone, JSON.stringify(_data))
                // console.log(JSON.parse(localStorage.getItem(main.tablemilestone)));return false

                Swal.fire({
                    icon: 'success',
                    title: 'Deleted!',
                    text: 'Delete data successfully.',
                    target: '#pagepanel',
                });

                getMilestoneAndHeader({data:_data})
            }
        })
    }

    function removeHeader(params) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You want to delete this header?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            target: '#pagepanel'
        }).then((result) => {
            if (result.isConfirmed) {
                let _data = [];
                $.each(JSON.parse(localStorage.getItem(main.tablemilestone)), function(k, v) {
                    let _milestone = v;
                    let _header = [];
                    $.each(v.header, function(k2, v2) {
                        if (params.keyheader == k2 && params.idmilestone == v.noid) {
                        } else {
                            _header.push(v2);
                        }
                    })
                    _milestone.header = _header;
                    _data.push(_milestone);
                    // console.log(_milestone)
                })

                // console.log(_data);
                // localStorage.removeItem(main.tablemilestone)
                localStorage.setItem(main.tablemilestone, JSON.stringify(_data))

                Swal.fire({
                    icon: 'success',
                    title: 'Deleted!',
                    text: 'Delete data successfully.',
                    target: '#pagepanel',
                });

                getMilestoneAndHeader({data:_data})
            }
        })
    }

    function removeDetail(params) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You want to delete this detail?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            target: '#pagepanel'
        }).then((result) => {
            if (result.isConfirmed) {
                let _datareplace = [];
                $.each(JSON.parse(localStorage.getItem(main.tablemilestone)), function(k,v){
                    _datareplace[k] = v;
                    $.each(v.header, function(k2,v2){
                        _datareplace[k].header[k2] = v2;
                        if (v2.noid == params.idheader) {
                            let _detail = [];
                            $.each(v2.detail, function(k3,v3){
                                if (k3 != params.keydetail) {                                    
                                    _detail.push(v3);
                                } else {
                                    // console.log(v3);return false
                                }
                            });
                            _datareplace[k].header[k2].detail = _detail;
                        }
                    });
                });

                localStorage.setItem(main.tablemilestone, JSON.stringify(_datareplace))

                Swal.fire({
                    icon: 'success',
                    title: 'Deleted!',
                    text: 'Delete data successfully.',
                    target: '#pagepanel',
                });

                getMilestoneAndHeader({data:_datareplace})
            }
        })
    }

    function getDatatable() {
        $('#table_master').DataTable().destroy();
        mytable = $('#table_master').DataTable({
            "autoWidth": false,
            "pageLength": 10,
            "searching": true,
            "processing": true,
            "serverSide": true,
            "bInfo": true,
            "bFilter": false,
            "bLengthChange": false,
            "columns": [
                {mData:'checkbox',orderable:false,class:'text-center'},
                {mData:'no',orderable:false,class:'text-right'},
                {mData:'kode'},
                {mData:'nama'},
                {mData:'firstname'},
                {mData:'middlename'},
                {mData:'lastname'},
                {mData:'gelar'},
                {mData:'email'},
                {mData:'isgroupuser',class:'text-center'},
                {mData:'isuser',class:'text-center'},
                {mData:'isemployee',class:'text-center'},
                {mData:'iscustomer',class:'text-center'},
                {mData:'issupplier',class:'text-center'},
                {mData:'is3rdparty',class:'text-center'},
                {mData:'isactive',class:'text-center'},
                {mData:'action',orderable:false},
            ],
            "columnDefs": [
                {targets:0,width:"15px"},
                {targets:1,width:"15px"},
                {targets:2,width:"100px"},
                {targets:3,width:"350px"},
                {targets:4,width:"350px"},
                {targets:5,width:"100px"},
                {targets:6,width:"100px"},
                {targets:7,width:"100px"},
                {targets:8,width:"100px"},
                {targets:9,width:"100px"},
                {targets:10,width:"100px"},
                {targets:11,width:"100px"},
                {targets:12,width:"100px"},
                {targets:13,width:"100px"},
                {targets:14,width:"100px"},
                {targets:15,width:"100px"},
                {targets:16,width:"100px"},
            ],
            "infoCallback": function( settings, start, end, max, total, pre ) {
                var options = '';
                $.each(settings.aLengthMenu, function(k, v) {
                    options += '<option value="'+v+'" id="alm'+v+'" class="alm">'+v+'</option>'
                })
                if ((end-start) == settings._iDisplayLength-1) {
                    var valuepage = end/settings._iDisplayLength;
                } else {
                    var valuepage = ((start-1)/settings._iDisplayLength)+1;
                }
                if (start == 1) {
                    var prevdisabled = 'disabled'
                } else {
                    var prevdisabled = '';
                }
                if (end == total) {
                    var nextdisabled = 'disabled'
                } else {
                    var nextdisabled = '';
                }
                // if (settings.oPreviousSearch.sSearch == $('#searchform').val()) {
                //     alert($('#searchform').val())
                //     var valuesearch = $('#searchform').val()
                // } else {
                //     var valuesearch = $('#searchform').val()
                //     alert('else'+$('#searchform').val())
                // }
                if (settings.oPreviousSearch.sSearch != '') {
                    statussearch = true
                    searchtablewidth = 200
                    valuesearch = settings.oPreviousSearch.sSearch
                } else {
                    statussearch = false
                    searchtablewidth = 30
                    valuesearch = ''
                }
                $('#appendInfoDatatable').html(`
                <div class="row" style="background: #90CAF9; padding-left: 15px; padding-right: 15px;">
                    <div class="col-md-12">
                        Page
                        <button id="prevpage" onclick="prevpage()" class="btn btn-sm default prev `+prevdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-left">
                            </i>
                        </button>
                        <input type="text" id="valuepage" onkeyup="valuepage()" class="input-mini" value="`+valuepage+`" style="text-align: center; border: 0px; margin: 5px 5px 5px 5px">
                        <button id="nextpage" onclick="nextpage()" class="btn btn-sm default next `+nextdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-right">
                            </i>
                        </button> of <span class="pagination-panel-total">`+
                            (settings._iRecordsTotal == settings._iDisplayLength ? 1 : parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)
                        +`</span>
                        <span class="seperator">|</span>
                        Rec/Page 
                        <select id="alengthmenu" onchange="alengthmenu()" name="table_master_length" aria-controls="table_master" class="input-inline input-xsmall input-sm recofdt" style="border: 0px; margin: 5px 5px 5px 5px">
                            `+options+`
                        </select>
                        <span class="seperator">|</span>Total `+settings._iRecordsTotal+` records
                        <input id="searchform" onkeyup="searchform_()" value="`+valuesearch+`" type="search" class="form-control input-inline input-sm clicked" placeholder="" aria-controls="table_master" style="width: `+searchtablewidth+`px; position: absolute; right: 0px; top: 4px; height: 80%; border: 0;">
                        <div onclick="searchtable()" class="btn fa fa-search text-right" style="position: absolute; right: 0px; top: 4px; height: 80%; background: transparent; margin: 0px 0px 0px 0px; padding: 5px 0px 5px 0px">
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                </div>`)

                $("#searchform").keyup(e => {
                    if(e.keyCode == 13) {
                        mytable.search($("#searchform").val()).page(1).draw(true);
                    };
                })

                console.log('settings')
                console.log(settings)
                console.log(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)
                $('.alm').removeAttr('selected');
                $('#alm'+settings._iDisplayLength).attr('selected', 'selected');
                // console.log(valuepage)
                $('.dataTables_info').css('display', 'none')
                $('.dataTables_paginate').css('display', 'none')
            },
            "ajax": {
                "url": "/get"+main.urlcode,
                "type": "POST",
                "dataType":'json',
                "data": {
                    "_token":$('#token').val(),
                    "filter":selectedfilter,
                    "datefilter":datefilter,
                    "datefilter2":datefilter2,
                },
                "dataSrc": function(json) {
                    selectcheckboxman = json.allnoid;
                    localStorage.setItem('sc-m-an', JSON.stringify(json.allnoid))
                    if (json.datefilter.type == 'a') {
                        $('#dropdown-date-filter span').text('All')
                    } else {
                        $('#dropdown-date-filter span').text(json.datefilter.from+' - '+json.datefilter.to)
                    }
                    return json.data;
                }
            },
            "pagingType": "input",
            "createdRow": function(row, data, dataIndex) {
                // console.log('datarowwww')
                // console.log(data.noid)
                $(row).addClass('my_row');
                $(row).addClass('row-m-'+data.noid);
                // // $(row).css('height', '5px');
                $(row).attr('onmouseover', `showButtonAction(this, ${dataIndex})`);
                $(row).attr('onmouseout', 'resetCss()');
                // $(row).attr('onmouseover', 'getPositionXY(this)');

                let scm = JSON.parse(localStorage.getItem('sc-m'));
                if (scm.includes(data.noid)) {
                    console.log('rowwwww')
                    console.log($(row).children('td').children('input').val('TRUE'))
                    $(row).css('background-color', '#90CAF9');
                    $(row).children('td:first').html('<input type="checkbox" value="TRUE" class="checkbox-m" id="checkbox-m-'+data.noid+'" onchange="selectCheckboxM('+data.noid+')" checked>');
                }
            },
            "oLanguage":{
                "loadingRecords": "Please wait - loading...",
                "sProcessing": "<div id='loadernya'></div>",
                "sInfo": "&nbsp;|&nbsp;Total&nbsp;_TOTAL_ records",
                "sLengthMenu": "&nbsp;|&nbsp;Rec/Page&nbsp;_MENU_",
                "sInfoFiltered": "",
                "sInfoEmpty": "| No records found to show",
                "sEmptyTable": "No data available in table",
                "sZeroRecords": "No matching records found",
                "sSearch": "<button onclick='toggleButtonSearch()' class='btn flat dtfilter_search_btn' style='background-color: white'><i class='fa fa-search'></i></button>",
                "oPaginate": {
                    "sPrevious": "<i class='fa fa-angle-left'></i>",
                    "sNext": "<i class='fa fa-angle-right'></i> ",
                }
            },
            footerCallback: function ( row, data, start, end, display ) {
                var api = this.api(), data;
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
    
                var sum = function ( i ) {
                    var value = 0;
                    $.each(api.column( i ).data(), function(k, v) {
                        var _split = v.split('.');
                        var value2 = '';
                        $.each(_split, function(k2, v2) {
                            value2 += v2
                        })
                        value += Number(value2)
                    })
                    return value
                }
                var addcommas = function addCommas(nStr) {
                    nStr += '';
                    var x = nStr.split('.');
                    var x1 = x[0];
                    var x2 = x.length > 1 ? '.' + x[1] : '';
                    var rgx = /(\d+)(\d{3})/;
                    while (rgx.test(x1)) {
                        x1 = x1.replace(rgx, '$1' + '.' + '$2');
                    }
                    return x1 + x2;
                }

                pageTotal = api
                    .column( 9, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                // Update footer
                // console.log('akak')
                // console.log(eval(api.column(9).data()[0]))
                // $( api.column(4).footer() ).html(data.length);
                // $( api.column(8).footer() ).html(api.column(8).data().reduce( function (a, b) { return parseInt(a)+parseInt(b); }, 0 ));
                // var $th8 = $(row).find('th').eq(8);
                // $th8.text( $.fn.dataTable.render.number(',', '.', 0, 'Rp.').display( $th8.text() )) 
                // $( api.column(9).footer() ).html(api.column(9).data().reduce( function (a, b) { return parseInt(a)+parseInt(b); }, 0 ));
                // var $th9 = $(row).find('th').eq(9);
                // $th9.text( $.fn.dataTable.render.number(',', '.', 0, 'Rp.').display( $th9.text() )) 
                // $( api.column(6).footer() ).html(data.length);
            }
        });

        new $.fn.dataTable.Buttons( mytable, {
            buttons: [
                {
                    autoFilter: true,
                    text:      '<i class="fa fa-trash"></i> Delete Selected',
                    titleAttr: 'Delete Selected',
                    className: 'btn btn-danger col-md-12',
                    action:  function(e, dt, button, config) {
                        let scm = JSON.parse(localStorage.getItem('sc-m'));
                        if (scm.length == 0) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'There\'s no selected!',
                                target: '#pagepanel'
                            })
                        } else {
                            Swal.fire({
                                title: 'Are you sure?',
                                text: "You want to delete this selected?",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                target: '#pagepanel'
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    removeSelected(scm)
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'Deleted!',
                                        text: 'Delete data successfully.',
                                        target: '#pagepanel',
                                    });
                                }
                            })
                        }
                    },
                },{
                    autoFilter: true,
                    text:      '<i class="fa fa-trash"></i> Delete All',
                    titleAttr: 'Delete All',
                    className: 'btn btn-danger col-md-12',
                    action:  function(e, dt, button, config) {
                        let scma = JSON.parse(localStorage.getItem('sc-m-a'));
                        let scman = JSON.parse(localStorage.getItem('sc-m-an'));
                        let scm = JSON.parse(localStorage.getItem('sc-m'));
                        // if (scma) {
                            Swal.fire({
                                title: 'Are you sure?',
                                text: "You want to delete all data?",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes',
                                target: '#pagepanel'
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    removeSelected(scman)
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'Deleted!',
                                        text: 'Delete data successfully.',
                                        target: '#pagepanel',
                                    });
                                }
                            })
                        // } else {
                        //     Swal.fire({
                        //         icon: 'error',
                        //         title: 'Oops...',
                        //         text: 'Something went wrong!',
                        //         footer: '<a href>Why do I have this issue?</a>'
                        //     })
                        // }
                    },
                },{
                    extend:    'excel',
                    autoFilter: true,
                    text:      '<i class="fa fa-file-excel"></i> Export to Excel',
                    titleAttr: 'Export to Excel',
                    title: $("#judul_excel").val(),
                    className: 'btn btn-success col-md-12 mb-0',
                    exportOptions: {
                        modifier: {
                            search: 'applied',
                            order: 'applied'
                        },
                        columns: [ 1,2,3, ':visible' ]
                        //                  <a style="font-size: 14px;min-width: 175px;
                        // text-align: left;" href="javascript:;"  class="dropdown-item">
                        // <i style="margin-top:10px;margin-right: 5px;" class="fa fa-file-excel"></i> Export to Excel </a>
                                        // columns: [ 0, ':visible' ]
                    },
                    action:  function(e, dt, button, config) {
                    // $('.loading').fadeIn();
                        var that = this;
                        setTimeout(function () {
                            $.fn.dataTable.ext.buttons.excelHtml5.action.call(that,e, dt, button, config);
                            // console.log(that);
                            // console.log(e);
                            // console.log(dt);
                            // console.log(button);
                            // console.log(config);
                            $('.loading').fadeOut();
                        },500);
                    },
                }
            ]
            // infoDatatable()
        });
        // infoDatatable()

        mytable.buttons().container().appendTo('#drop_export2');

        $('#pagepanel-body').css('min-height', screen.height-(screen.height/3))
    }

    function deleteSelected() {
        let scm = JSON.parse(localStorage.getItem('sc-m'));
        if (scm.length == 0) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'There\'s no selected!',
                target: '#pagepanel'
            })
        } else {
            Swal.fire({
                title: 'Are you sure?',
                text: "You want to delete this selected?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                target: '#pagepanel'
            }).then((result) => {
                if (result.isConfirmed) {
                    removeSelected(scm)
                    Swal.fire({
                        icon: 'success',
                        title: 'Deleted!',
                        text: 'Delete data successfully.',
                        target: '#pagepanel',
                    });
                }
            })
        }
    }

    function deleteAll() {
        let scma = JSON.parse(localStorage.getItem('sc-m-a'));
        let scman = JSON.parse(localStorage.getItem('sc-m-an'));
        let scm = JSON.parse(localStorage.getItem('sc-m'));

        Swal.fire({
            title: 'Are you sure?',
            text: "You want to delete all data?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            target: '#pagepanel'
        }).then((result) => {
            if (result.isConfirmed) {
                removeSelected(scman)
                Swal.fire({
                    icon: 'success',
                    title: 'Deleted!',
                    text: 'Delete data successfully.',
                    target: '#pagepanel',
                });
            }
        })
    }

    function exportExcel() {

    }

    function chooseGenerateTemplate(params) {
        hideModalGenerateTemplate()
        setTimeout(function(){ 
            getMilestoneAndHeader({
                data: JSON.parse(localStorage.getItem('template-'+params.noid))
            })
        }, 500);
    }

    function format(d) {
        // `d` is the original data object for the row
        return '<b>Log Subject : </b><span>'+(d.logsubjectfull ? d.logsubjectfull : '')+'</span><br>'+
                '<b>Keterangan : </b><span>'+(d.keteranganfull ? d.keteranganfull : '')+'</span>';
    }

    function getDatatableLog(noid, kode, noidmore='', fieldmore='') {
        $('#table-log').DataTable().destroy();
        mytablelog = $('#table-log').DataTable({
            "pageLength": 10,
            "searching": true,
            "processing": true,
            "serverSide": true,
            "bInfo": true,
            "bFilter": false,
            "bLengthChange": false,
            "bAutoWidth": false,
            "order": [[ 6, "desc" ]],
            "columns": [
                {className:'details-control',orderable:false,data:null,defaultContent:''},
                {mData:'no',orderable:false,class:'text-right'},
                {mData:'idtypeaction'},
                {mData:'logsubject'},
                {mData:'keterangan'},
                {mData:'idcreate'},
                {mData:'docreate'},
            ],
            "columnDefs": [
                {targets:0,width:"15px"},
                {targets:1,width:"15px"},
                {targets:2,width:"100px"},
                {targets:3,width:"350px"},
                {targets:4,width:"350px"},
                {targets:5,width:"100px"},
                {targets:6,width:"100px"},
            ],
            // "fixedColumns": true,
            "infoCallback": function( settings, start, end, max, total, pre ) {
                var options = '';
                $.each(settings.aLengthMenu, function(k, v) {
                    options += '<option value="'+v+'" id="alm'+v+'" class="alm">'+v+'</option>'
                })
                if ((end-start) == settings._iDisplayLength-1) {
                    var valuepage = end/settings._iDisplayLength;
                } else {
                    var valuepage = ((start-1)/settings._iDisplayLength)+1;
                }
                if (start == 1) {
                    var prevdisabled = 'disabled'
                } else {
                    var prevdisabled = '';
                }
                if (end == total) {
                    var nextdisabled = 'disabled'
                } else {
                    var nextdisabled = '';
                }
                // if (settings.oPreviousSearch.sSearch == $('#searchform').val()) {
                //     alert($('#searchform').val())
                //     var valuesearch = $('#searchform').val()
                // } else {
                //     var valuesearch = $('#searchform').val()
                //     alert('else'+$('#searchform').val())
                // }
                if (settings.oPreviousSearch.sSearch != '') {
                    statussearch = true
                    searchtablewidth = 200
                    valuesearch = settings.oPreviousSearch.sSearch
                } else {
                    statussearch = false
                    searchtablewidth = 30
                    valuesearch = ''
                }
                $('#appendInfoDatatableLog').html(`
                <div class="row" style="background: #90CAF9; padding-left: 15px; padding-right: 15px;">
                    <div class="col-md-12">
                        Page
                        <button id="prevpagelog" onclick="prevpagelog()" class="btn btn-sm default prev `+prevdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-left">
                            </i>
                        </button>
                        <input type="text" id="valuepagelog" onkeyup="valuepagelog()" class="input-mini" value="`+valuepage+`" style="text-align: center; border: 0px; margin: 5px 5px 5px 5px">
                        <button id="nextpagelog" onclick="nextpagelog()" class="btn btn-sm default next `+nextdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-right">
                            </i>
                        </button> of <span class="pagination-panel-total">`+
                            (settings._iRecordsTotal == settings._iDisplayLength ? 1 : parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)
                        +`</span>
                        <span class="seperator">|</span>
                        Rec/Page 
                        <select id="alengthmenulog" onchange="alengthmenulog()" name="table_master_length" aria-controls="table_master" class="input-inline input-xsmall input-sm recofdt" style="border: 0px; margin: 5px 5px 5px 5px">
                            `+options+`
                        </select>
                        <span class="seperator">|</span>Total `+settings._iRecordsTotal+` records
                        <input id="searchformlog" onkeyup="searchformlog()" value="`+valuesearch+`" type="search" class="form-control input-inline input-sm clicked" placeholder="" aria-controls="table_master" style="width: `+searchtablewidth+`px; position: absolute; right: 0px; top: 4px; height: 80%; border: 0;">
                        <div onclick="searchtablelog()" class="btn fa fa-search text-right" style="position: absolute; right: 0px; top: 4px; height: 80%; background: transparent; margin: 0px 0px 0px 0px; padding: 5px 0px 5px 0px">
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                </div>`)
                console.log('settings')
                console.log(settings)
                console.log(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)
                $('.alm').removeAttr('selected');
                $('#alm'+settings._iDisplayLength).attr('selected', 'selected');
                // console.log(valuepage)
                $('.dataTables_info').css('display', 'none')
                $('.dataTables_paginate').css('display', 'none')
            },
            "ajax": {
                "url": "/get_log"+main.urlcode,
                "type": "POST",
                "dataType":'json',
                "data": {
                    "_token": $('#token').val(),
                    'statusadd': statusadd,
                    'noid': noid,
                    'kode': kode,
                    'noidmore': noidmore,
                    'fieldmore': fieldmore,
                },
            },
            "pagingType": "input",
            "createdRow": function(row, data, dataIndex) {
                $(row).attr('onmouseover', 'showButtonAction(this, '+dataIndex+', 100)');
                $(row).attr('onmouseout', 'resetCss()');
            },
            "oLanguage":{
                "loadingRecords": "Please wait - loading...",
                "sProcessing": "<div id='loadernya'></div>",
                "sInfo": "&nbsp;|&nbsp;Total&nbsp;_TOTAL_ records",
                "sLengthMenu": "&nbsp;|&nbsp;Rec/Page&nbsp;_MENU_",
                "sInfoFiltered": "",
                "sInfoEmpty": "| No records found to show",
                "sEmptyTable": "No data available in table",
                "sZeroRecords": "No matching records found",
                "sSearch": "<button onclick='toggleButtonSearch()' class='btn flat dtfilter_search_btn' style='background-color: white'><i class='fa fa-search'></i></button>",
                "oPaginate": {
                    "sPrevious": "<i class='fa fa-angle-left'></i>",
                    "sNext": "<i class='fa fa-angle-right'></i> ",
                }
            },
            footerCallback: function ( row, data, start, end, display ) {
            }
        });
        
        if (!statusgetlog) {
            $('#table-log tbody').on('click', 'td.details-control', function(){
                var tr = $(this).closest('tr');
                var row = mytablelog.row( tr );

                if(row.child.isShown()){
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                } else {
                    // Open this row
                    row.child(format(row.data())).show();
                    tr.addClass('shown');
                }
            });
            statusgetlog = true;
        }
    }

    function moreLog(noid, field) {
        $('#table-log #'+field+'-'+noid).show();
        $('#table-log #morelog-'+field+'-'+noid).attr('onclick', 'lessLog('+noid+', \''+field+'\')');
        $('#table-log #morelog-'+field+'-'+noid+' i').attr('class', 'icon-angle-left');
        // $('#table-log #morelog-'+field+'-'+noid+' span').text('Less');
    }

    function lessLog(noid, field) {
        $('#table-log #'+field+'-'+noid).hide();
        $('#table-log #morelog-'+field+'-'+noid).attr('onclick', 'moreLog('+noid+', \''+field+'\')');
        $('#table-log #morelog-'+field+'-'+noid+' i').attr('class', 'icon-angle-right');
        // $('#table-log #morelog-'+field+'-'+noid+' span').text('More');
    }

    function getDatatableGenerateTemplate() {
        $('#table-generatetemplate').DataTable().destroy();
        mytablegeneratetemplate = $('#table-generatetemplate').DataTable({
            "autoWidth": false,
            "pageLength": 10,
            "searching": true,
            "processing": true,
            "serverSide": true,
            "bInfo": true,
            "bFilter": false,
            "bLengthChange": false,
            "columns": [
                {mData:'checkbox',orderable:false,class:'text-center'},
                {mData:'no',orderable:false,class:'text-right'},
                {mData:'kode'},
                {mData:'kodereff'},
                {mData:'keterangan'},
                {mData:'amc_nama'},
                {mData:'action',orderable:false},
            ],
            "columnDefs": [
                {targets:0,width:'1%'},
                {targets:1,width:'4%'},
                {targets:2,width:'22%'},
                {targets:3,width:'22%'},
                {targets:4,width:'22%'},
                {targets:5,width:'22%'},
                {targets:6,width:'7%'},
            ],
            "infoCallback": function( settings, start, end, max, total, pre ) {
                var options = '';
                $.each(settings.aLengthMenu, function(k, v) {
                    options += '<option value="'+v+'" id="alm'+v+'" class="alm">'+v+'</option>'
                })
                if ((end-start) == settings._iDisplayLength-1) {
                    var valuepage = end/settings._iDisplayLength;
                } else {
                    var valuepage = ((start-1)/settings._iDisplayLength)+1;
                }
                if (start == 1) {
                    var prevdisabled = 'disabled'
                } else {
                    var prevdisabled = '';
                }
                if (end == total) {
                    var nextdisabled = 'disabled'
                } else {
                    var nextdisabled = '';
                }
                // if (settings.oPreviousSearch.sSearch == $('#searchform').val()) {
                //     alert($('#searchform').val())
                //     var valuesearch = $('#searchform').val()
                // } else {
                //     var valuesearch = $('#searchform').val()
                //     alert('else'+$('#searchform').val())
                // }
                if (settings.oPreviousSearch.sSearch != '') {
                    statussearch = true
                    searchtablewidth = 200
                    valuesearch = settings.oPreviousSearch.sSearch
                } else {
                    statussearch = false
                    searchtablewidth = 30
                    valuesearch = ''
                }
                $('#appendInfoDatatableCostControl').html(`
                <div class="row" style="background: #90CAF9; padding-left: 15px; padding-right: 15px;">
                    <div class="col-md-12">
                        Page
                        <button id="prevpagegeneratetemplate" onclick="prevpagegeneratetemplate()" class="btn btn-sm default prev `+prevdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-left">
                            </i>
                        </button>
                        <input type="text" id="valuepagegeneratetemplate" onkeyup="valuepagegeneratetemplate()" class="input-mini" value="`+valuepage+`" style="text-align: center; border: 0px; margin: 5px 5px 5px 5px">
                        <button id="nextpagegeneratetemplate" onclick="nextpagegeneratetemplate()" class="btn btn-sm default next `+nextdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-right">
                            </i>
                        </button> of <span class="pagination-panel-total">`+(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)+`</span>
                        <span class="seperator">|</span>
                        Rec/Page 
                        <select id="alengthmenugeneratetemplate" onchange="alengthmenugeneratetemplate()" name="table_master_length" aria-controls="table_master" class="input-inline input-xsmall input-sm recofdt" style="border: 0px; margin: 5px 5px 5px 5px">
                            `+options+`
                        </select>
                        <span class="seperator">|</span>Total `+settings._iRecordsTotal+` records
                        <input id="searchformgeneratetemplate" onkeyup="searchformgeneratetemplate_()" value="`+valuesearch+`" type="search" class="form-control input-inline input-sm clicked" placeholder="" aria-controls="table_master" style="width: `+searchtablewidth+`px; position: absolute; right: 0px; top: 4px; height: 80%; border: 0;">
                        <div onclick="searchtablegeneratetemplate()" class="btn fa fa-search text-right" style="position: absolute; right: 0px; top: 4px; height: 80%; background: transparent; margin: 0px 0px 0px 0px; padding: 5px 0px 5px 0px">
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                </div>`)

                $("#searchformgeneratetemplate").keyup(e => {
                    if(e.keyCode == 13) {
                        mytablegeneratetemplate.search($("#searchformgeneratetemplate").val()).page(1).draw(true);
                    };
                })

                console.log('settings')
                console.log(settings)
                console.log(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)
                $(`#alengthmenugeneratetemplate .alm`).removeAttr('selected');
                $(`#alengthmenugeneratetemplate #alm${settings._iDisplayLength}`).attr('selected', 'selected');
                // console.log(valuepage)
                $('.dataTables_info').css('display', 'none')
                $('.dataTables_paginate').css('display', 'none')
            },
            "ajax": {
                "url": "/get_generatetemplate"+main.urlcode,
                "type": "POST",
                "dataType":'json',
                "data": {
                    "_token": $('#token').val(),
                    'statusadd': statusadd,
                    'tanggal': $('#form-header #tanggal').val(),
                    'tanggaldue': $('#form-header #tanggaldue').val(),
                },
                "dataSrc": function(json) {
                    $.each(json.data, function(k,v){
                        localStorage.setItem('template-'+v.noid, JSON.stringify(v.milestone))
                        // console.log(localStorage.getItem('template-'+v.noid))
                    })
                    return json.data;
                }
            },
            "pagingType": "input",
            "createdRow": function(row, data, dataIndex) {
                $(row).attr('onmouseover', 'showButtonAction(this, '+dataIndex+', 100)');
                $(row).attr('onmouseout', 'resetCss()');
            },
            "oLanguage":{
                "loadingRecords": "Please wait - loading...",
                "sProcessing": "<div id='loadernya'></div>",
                "sInfo": "&nbsp;|&nbsp;Total&nbsp;_TOTAL_ records",
                "sLengthMenu": "&nbsp;|&nbsp;Rec/Page&nbsp;_MENU_",
                "sInfoFiltered": "",
                "sInfoEmpty": "| No records found to show",
                "sEmptyTable": "No data available in table",
                "sZeroRecords": "No matching records found",
                "sSearch": "<button onclick='toggleButtonSearch()' class='btn flat dtfilter_search_btn' style='background-color: white'><i class='fa fa-search'></i></button>",
                "oPaginate": {
                    "sPrevious": "<i class='fa fa-angle-left'></i>",
                    "sNext": "<i class='fa fa-angle-right'></i> ",
                }
            },
            footerCallback: function ( row, data, start, end, display ) {
            }
        });
    }

    function getDatatableDetailPrev(params) {
        let idcardsupplier = $('#form-header #idcardsupplier').val();
        let idcompany = $('#form-header #idcompany').val();
        let iddepartment = $('#form-header #iddepartment').val();
        let idgudang = $('#form-header #idgudang').val();
        let searchtypeprev = 2;
        let searchprev = params.frommodal ? $(`#searchformdetailprev-${params.idelement}`).val() : params.searchprev;
        let detailprev = new Array();

        defaultOperation()

        $('#table-detailprev').DataTable().destroy();
        mytabledetailprev = $('#table-detailprev').DataTable({
            "autoWidth": false,
            "pageLength": 10,
            "searching": true,
            "processing": true,
            "serverSide": true,
            "bInfo": true,
            "bFilter": false,
            "bLengthChange": false,
            "infoCallback": function( settings, start, end, max, total, pre ) {
                var options = '';
                $.each(settings.aLengthMenu, function(k, v) {
                    options += '<option value="'+v+'" id="alm'+v+'" class="alm">'+v+'</option>'
                })
                if ((end-start) == settings._iDisplayLength-1) {
                    var valuepage = end/settings._iDisplayLength;
                } else {
                    var valuepage = ((start-1)/settings._iDisplayLength)+1;
                }
                if (start == 1) {
                    var prevdisabled = 'disabled'
                } else {
                    var prevdisabled = '';
                }
                if (end == total) {
                    var nextdisabled = 'disabled'
                } else {
                    var nextdisabled = '';
                }
                // if (settings.oPreviousSearch.sSearch == $('#searchform').val()) {
                //     alert($('#searchform').val())
                //     var valuesearch = $('#searchform').val()
                // } else {
                //     var valuesearch = $('#searchform').val()
                //     alert('else'+$('#searchform').val())
                // }
                if (settings.oPreviousSearch.sSearch != '') {
                    statussearch = true
                    searchtablewidth = 200
                    valuesearch = settings.oPreviousSearch.sSearch
                } else {
                    statussearch = false
                    searchtablewidth = 30
                    valuesearch = ''
                }
                $('#appendInfoDatatableDetailPrev').html(`
                <div class="row" style="background: #90CAF9; padding-left: 15px; padding-right: 15px;">
                    <div class="col-md-12">
                        Page
                        <button type="button" onclick="prevpagedetailprev()" class="btn btn-sm default prev `+prevdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-left">
                            </i>
                        </button>
                        <input type="text" onkeyup="valuepagedetailprev()" class="input-mini" value="`+valuepage+`" style="text-align: center; border: 0px; margin: 5px 5px 5px 5px">
                        <button type="button" onclick="nextpagedetailprev()" class="btn btn-sm default next `+nextdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-right">
                            </i>
                        </button> of <span class="pagination-panel-total">`+(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)+`</span>
                        <span class="seperator">|</span>
                        Rec/Page 
                        <select id="alengthmenudetailprev" onchange="alengthmenudetailprev({idelement:'${params.idelement}'})" name="table_master_length" aria-controls="table_master" class="input-inline input-xsmall input-sm recofdt" style="border: 0px; margin: 5px 5px 5px 5px">
                            `+options+`
                        </select>
                        <span class="seperator">|</span>Total `+settings._iRecordsTotal+` records
                        <input id="searchformdetailprev-${params.idelement}" value="`+valuesearch+`" type="search" class="form-control input-inline input-sm clicked" placeholder="" aria-controls="table_master" style="width: `+searchtablewidth+`px; position: absolute; right: 0px; top: 4px; height: 80%; border: 0;">
                        <div onclick="searchtabledetailprev({idelement:'${params.idelement}'})" class="btn fa fa-search text-right" style="position: absolute; right: 0px; top: 4px; height: 80%; background: transparent; margin: 0px 0px 0px 0px; padding: 5px 0px 5px 0px">
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                </div>`)

                $(`#searchformdetailprev-${params.idelement}`).keyup(e => {
                    if(e.keyCode == 13) {
                        getDatatableDetailPrev({frommodal:true,idelement:params.idelement})
                    };
                })
                
                console.log('settings')
                console.log(settings)
                console.log(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)
                $(`#alengthmenudetailprev .alm`).removeAttr('selected');
                $(`#alengthmenudetailprev #alm${settings._iDisplayLength}`).attr('selected', 'selected');
                // console.log(valuepage)
                $('.dataTables_info').css('display', 'none')
                $('.dataTables_paginate').css('display', 'none')
            },
            "ajax": {
                "url": "/get_detailpi"+main.urlcode,
                "type": "POST",
                "dataType":'json',
                "data": {
                    "_token": $('#token').val(),
                    'statusdetail': statusdetail,
                    'idcardsupplier': idcardsupplier,
                    'idcompany': idcompany,
                    'iddepartment': iddepartment,
                    'idgudang': idgudang,
                    'detailprev': JSON.stringify(detailprev),
                    'searchtypeprev': searchtypeprev,
                    'searchprev': searchprev,
                    'idelement': params.idelement
                },
                "dataSrc": function(json) {
                    if (json.data.length == 1) {
                        chooseDetailPrev({idelement: json.idelement, data: json.data[0]});
                    } else {
                        $('#modal-detailprev').modal('show')
                        return json.data;
                    }
                }
            },
            "pagingType": "input",
            "createdRow": function(row, data, dataIndex) {
                let right = statusdetail ? 128 : 128;
                $(row).attr('onmouseover', 'showButtonActionTabPrev(this, '+dataIndex+', '+right+')');
                $(row).attr('onmouseout', 'resetCssTabPrev()');
            },
            "oLanguage":{
                "loadingRecords": "Please wait - loading...",
                "sProcessing": "<div id='loadernya'></div>",
                "sInfo": "&nbsp;|&nbsp;Total&nbsp;_TOTAL_ records",
                "sLengthMenu": "&nbsp;|&nbsp;Rec/Page&nbsp;_MENU_",
                "sInfoFiltered": "",
                "sInfoEmpty": "| No records found to show",
                "sEmptyTable": "No data available in table",
                "sZeroRecords": "No matching records found",
                "sSearch": "<button onclick='toggleButtonSearch()' class='btn flat dtfilter_search_btn' style='background-color: white'><i class='fa fa-search'></i></button>",
                "oPaginate": {
                    "sPrevious": "<i class='fa fa-angle-left'></i>",
                    "sNext": "<i class='fa fa-angle-right'></i> ",
                }
            },
            footerCallback: function ( row, data, start, end, display ) {
                var api = this.api(), data;
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
    
                var sum = function ( i ) {
                    var value = 0;
                    $.each(api.column( i ).data(), function(k, v) {
                        var _split = v.split('.');
                        var value2 = '';
                        $.each(_split, function(k2, v2) {
                            value2 += v2
                        })
                        value += Number(value2)
                    })
                    return value
                }
                var addcommas = function addCommas(nStr) {
                    nStr += '';
                    var x = nStr.split('.');
                    var x1 = x[0];
                    var x2 = x.length > 1 ? '.' + x[1] : '';
                    var rgx = /(\d+)(\d{3})/;
                    while (rgx.test(x1)) {
                        x1 = x1.replace(rgx, '$1' + '.' + '$2');
                    }
                    return x1 + x2;
                }

                pageTotal = api
                    .column( 11, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                // Update footer
                // console.log('akak')
                // console.log(eval(api.column(9).data()[0]))
                $( api.column(2).footer() ).html(data.length);
                $( api.column(6).footer() ).html(api.column(6).data().reduce( function (a, b) { return parseInt(a)+parseInt(b); }, 0 ));
                $( api.column(7).footer() ).html(api.column(7).data().reduce( function (a, b) { return parseInt(a)+parseInt(b); }, 0 ));
                $( api.column(11).footer() ).html(api.column(11).data().reduce( function (a, b) { return pageTotal; }, 0 ));
                var $th = $(row).find('th').eq(11);
                $th.text( $.fn.dataTable.render.number(',', '.', 0, 'Rp.').display( $th.text() )) 
                // $( api.column(6).footer() ).html(data.length);
            }
        });
    }

    function formatDetail(d) {
        // console.log(d)
        let _tbody = '<tbody>';
        let _no = 1;
        $.each(d.detail, function(k,v){
            _tbody += `<tr>
                <td class="details-control"></td>
                <td style="width: 30px;">
                    <input type="checkbox" id="select_all_9921050102" onchange="selectAllCheckboxTab(9921050102)" class="checkbox-9921050102 checkbox-9921050102-0" name="checkbox-9921050102[]" value="FALSE">
                </td>
                <td>`+_no+`</td>
                <td>`+v.kodeprev+`</td>
                <td>`+v.kodeinv+`</td>
                <td>`+v.nama+`</td>
                <td>`+v.inventor+`</td>
                <td>`+v.keterangan+`</td>
                <td>`+v.qty+`</td>
                <td>`+v.qtysisa+`</td>
                <td>`+v.unit+`</td>
                <td>`+v.price+`</td>
                <td>`+v.total+`</td>
                <td>`+v.currency+`</td>
                <td>Action</td>
            </tr>`;
        })
        _tbody += '</tbody';
        _html = `<table bg-color="blue" class="nowrap table table-striped table-bordered table-hover  dataTable no-footer" id="table-detail-`+d.noid+`" style="background-color:#BBDEFB !important; margin: 0 !important; border: 1px solid #3598dc"  aria-describedby="table_master_info" role="grid">
            <thead>
                <tr style="height: 40px">
                    <th>[D]</th>
                    <th style="width: 30px;">
                        <input type="checkbox" id="select_all_9921050102" onchange="selectAllCheckboxTab(9921050102)" class="checkbox-9921050102 checkbox-9921050102-0" name="checkbox-9921050102[]" value="FALSE">
                    </th>
                    <th>No</th>
                    <th>Kode Prev.</th>
                    <th>Kode Inv.</th>
                    <th>Nama</th>
                    <th>Inventor</th>
                    <th>Keterangan</th>
                    <th>Qty.</th>
                    <th>Qty. Sisa</th>
                    <th>Unit</th>
                    <th>Price</th>
                    <th>Total</th>
                    <th>Currency</th>
                    <th id="th-action">Action</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </tfoot>
        </table>`;

        return {
            idheader: d.noid,
            idmaster: d.idmaster,
            html: _html,
        };
    }
    
    function getPreDatatableDetail(params) {
        var tr = $('#table-header-'+params.idheader+' tbody td:first-child').closest('tr');
        var row = mytableheader.row( tr );

        if(row.child.isShown()){
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            let _formatdata = formatDetail(row.data());
            row.child(_formatdata.html).show();
            tr.addClass('shown');
            
            getDatatableDetail({
                idheader: _formatdata.idheader,
                idmaster: _formatdata.idmaster,
            })
        }
    }

    function getDatatableDetail(params) {
        // defaultOperation()
        // alert('#table-'+params.idelement)

        $('#table-'+params.idelement).DataTable().destroy();
        mytabtable[params.idelement] = $('#table-'+params.idelement).DataTable({
            "autoWidth": false,
            "pageLength": 10,
            "searching": true,
            "processing": true,
            "serverSide": true,
            "bInfo": true,
            "bFilter": false,
            "bLengthChange": false,
            "columns": [
                // {className:'details-control',orderable:false,data:null,defaultContent:''},
                // {mData:'collapse',orderable:false,class:'text-center'},
                // {mData:'collapse',orderable:false,class:'text-center'},
                {mData:'checkbox',orderable:false,class:'text-center'},
                {mData:'no',orderable:false,class:'text-right'},
                {mData:'kodeprev'},
                {mData:'kodeinventor'},
                {mData:'namainventor'},
                {mData:'namainventor2'},
                {mData:'keterangan'},
                {mData:'unitqty',class:'text-right'},
                {mData:'unitqtysisa',class:'text-right'},
                {mData:'namasatuan'},
                {mData:'unitprice',class:'text-right',render:$.fn.dataTable.render.number(',','.',0,'Rp.')},
                {mData:'hargatotal',class:'text-right',render:$.fn.dataTable.render.number(',','.',0,'Rp.')},
                {mData:'namacurrency'},
                {mData:'action',orderable:false},
            ],
            "columnDefs": [
                {targets:0,width:'1%'},
                {targets:1,width:'5%'},
                {targets:2,width:'5%'},
                {targets:3,width:'15%'},
                {targets:4,width:'15%'},
                {targets:5,width:'15%'},
                {targets:6,width:'15%'},
                {targets:7,width:'15%'},
                {targets:8,width:'5%'},
                {targets:9,width:'5%'},
                {targets:10,width:'5%'},
                {targets:10,width:'5%'}
            ],
            "infoCallback": function( settings, start, end, max, total, pre ) {
                var options = '';
                $.each(settings.aLengthMenu, function(k, v) {
                    options += '<option value="'+v+'" id="alm'+v+'" class="alm">'+v+'</option>'
                })
                if ((end-start) == settings._iDisplayLength-1) {
                    var valuepage = end/settings._iDisplayLength;
                } else {
                    var valuepage = ((start-1)/settings._iDisplayLength)+1;
                }
                if (start == 1) {
                    var prevdisabled = 'disabled'
                } else {
                    var prevdisabled = '';
                }
                if (end == total) {
                    var nextdisabled = 'disabled'
                } else {
                    var nextdisabled = '';
                }
                // if (settings.oPreviousSearch.sSearch == $('#searchform').val()) {
                //     alert($('#searchform').val())
                //     var valuesearch = $('#searchform').val()
                // } else {
                //     var valuesearch = $('#searchform').val()
                //     alert('else'+$('#searchform').val())
                // }
                if (settings.oPreviousSearch.sSearch != '') {
                    statussearch = true
                    searchtablewidth = 200
                    valuesearch = settings.oPreviousSearch.sSearch
                } else {
                    statussearch = false
                    searchtablewidth = 30
                    valuesearch = ''
                }
                $('#appendInfoDatatableDetail-'+params.idelement).html(`
                <div class="row" style="background: #90CAF9; padding-left: 15px; padding-right: 15px;">
                    <div class="col-md-12">
                        Page
                        <button id="prevpagedetail-${params.idelement}" onclick="prevpagedetail({idelement:'${params.idelement}'})" class="btn btn-sm default prev `+prevdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-left">
                            </i>
                        </button>
                        <input type="text" id="valuepagedetail-${params.idelement}" onkeyup="valuepagedetail({idelement:'${params.idelement}'})" class="input-mini" value="`+valuepage+`" style="text-align: center; border: 0px; margin: 5px 5px 5px 5px">
                        <button id="nextpagedetail-${params.idelement}" onclick="nextpagedetail({idelement:'${params.idelement}'})" class="btn btn-sm default next `+nextdisabled+`" style="background-color: #2196F3;">
                            <i class="fa fa-angle-right">
                            </i>
                        </button> of <span class="pagination-panel-total">`+(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)+`</span>
                        <span class="seperator">|</span>
                        Rec/Page 
                        <select id="alengthmenudetail-${params.idelement}" onchange="alengthmenudetail({idelement:'${params.idelement}'})" name="table_master_length" aria-controls="table_master" class="input-inline input-xsmall input-sm recofdt" style="border: 0px; margin: 5px 5px 5px 5px">
                            `+options+`
                        </select>
                        <span class="seperator">|</span>Total `+settings._iRecordsTotal+` records
                        <input id="searchformdetail-${params.idelement}" onkeyup="searchformdetail({idelement:'${params.idelement}'})" value="`+valuesearch+`" type="search" class="form-control input-inline input-sm clicked" placeholder="" aria-controls="table_master" style="width: `+searchtablewidth+`px; position: absolute; right: 0px; top: 4px; height: 80%; border: 0;">
                        <div onclick="searchtabledetail({idelement:'${params.idelement}'})" class="btn fa fa-search text-right" style="position: absolute; right: 0px; top: 4px; height: 80%; background: transparent; margin: 0px 0px 0px 0px; padding: 5px 0px 5px 0px">
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                </div>`)
                console.log('settings')
                console.log(settings)
                console.log(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)
                $(`#searchformdetail-${params.idelement} .alm`).removeAttr('selected');
                $(`searchformdetail-${params.idelement} #alm${settings._iDisplayLength}`).attr('selected', 'selected');
                // console.log(valuepage)
                $('.dataTables_info').css('display', 'none')
                $('.dataTables_paginate').css('display', 'none')
            },
            "ajax": {
                "url": "/get_detail"+main.urlcode,
                "type": "POST",
                "dataType":'json',
                "data": {
                    "_token": $('#token').val(),
                    // 'data': data ? data : '[]',
                    // 'statusdetail': statusdetail,
                    'condition': JSON.stringify(main.condition[status.crudmaster]),
                    
                    'idelement': params.idelement,
                    'idheader': params.idheader,
                    'idmaster': params.idmaster,
                    'milestone': localStorage.getItem(main.tablemilestone)
                },
            },
            "pagingType": "input",
            "createdRow": function(row, data, dataIndex) {
                // console.log(params.idelement)
                let right = statusdetail ? 45 : 45;
                $(row).attr('onmouseover', 'showButtonActionTab(this, '+dataIndex+', '+right+', "'+params.idelement+'")');
                $(row).attr('onmouseout', `resetCssTab({dataIndex:${dataIndex},idelement:'${params.idelement}'})`);
            },
            "oLanguage":{
                "loadingRecords": "Please wait - loading...",
                "sProcessing": "<div id='loadernya'></div>",
                "sInfo": "&nbsp;|&nbsp;Total&nbsp;_TOTAL_ records",
                "sLengthMenu": "&nbsp;|&nbsp;Rec/Page&nbsp;_MENU_",
                "sInfoFiltered": "",
                "sInfoEmpty": "| No records found to show",
                "sEmptyTable": "No data available in table",
                "sZeroRecords": "No matching records found",
                "sSearch": "<button onclick='toggleButtonSearch()' class='btn flat dtfilter_search_btn' style='background-color: white'><i class='fa fa-search'></i></button>",
                "oPaginate": {
                    "sPrevious": "<i class='fa fa-angle-left'></i>",
                    "sNext": "<i class='fa fa-angle-right'></i> ",
                }
            },
            footerCallback: function ( row, data, start, end, display ) {
                var api = this.api(), data;
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
    
                var sum = function ( i ) {
                    var value = 0;
                    $.each(api.column( i ).data(), function(k, v) {
                        var _split = v.split('.');
                        var value2 = '';
                        $.each(_split, function(k2, v2) {
                            value2 += v2
                        })
                        value += Number(value2)
                    })
                    return value
                }
                var addcommas = function addCommas(nStr) {
                    nStr += '';
                    var x = nStr.split('.');
                    var x1 = x[0];
                    var x2 = x.length > 1 ? '.' + x[1] : '';
                    var rgx = /(\d+)(\d{3})/;
                    while (rgx.test(x1)) {
                        x1 = x1.replace(rgx, '$1' + '.' + '$2');
                    }
                    return x1 + x2;
                }

                pageTotal = api
                    .column( 9, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                // Update footer
                // console.log('akak')
                // console.log(eval(api.column(9).data()[0]))
                $( api.column(2).footer() ).html(data.length);
                $( api.column(7).footer() ).html(api.column(7).data().reduce( function (a, b) { return parseInt(a)+parseInt(b); }, 0 ));
                $( api.column(8).footer() ).html(api.column(8).data().reduce( function (a, b) { return parseInt(a)+parseInt(b); }, 0 ));
                $( api.column(11).footer() ).html(api.column(11).data().reduce( function (a, b) { return pageTotal; }, 0 ));
                var $th = $(row).find('th').eq(11);
                $th.text( $.fn.dataTable.render.number(',', '.', 0, 'Rp.').display( $th.text() )) 
                // $( api.column(6).footer() ).html(data.length);
            }
        });

        if (!statusgetdetail) {
            $('#table-detail-'+params.idheader+' tbody').on('click', 'td.details-control', function(){
                var tr = $(this).closest('tr');
                var row = mytabledetail.row( tr );

                if(row.child.isShown()){
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                } else {
                    // Open this row
                    let _formatdata = formatMilestone(row.data());
                    row.child(_formatdata.html).show();
                    tr.addClass('shown');
                    
                    alert('okebosss')
                    getDatatableDetail({
                        idheader: _formatdata.idheader,
                        idmaster: _formatdata.idmaster,
                    })
                }
            });
            statusgetdetail = true;
        }
    }

    function formatMilestone(d) {
        // console.log(d)
        let _tbody = '<tbody>';
        let _no = 1;
        $.each(d.header, function(k,v){
            _tbody += `<tr>
                <td class="milestones-control"></td>
                <td style="width: 30px;">
                    <input type="checkbox" id="select_all_9921050102" onchange="selectAllCheckboxTab(9921050102)" class="checkbox-9921050102 checkbox-9921050102-0" name="checkbox-9921050102[]" value="FALSE">
                </td>
                <td>`+_no+`</td>
                <td>`+v.keterangan+`</td>
                <td>`+v.tanggalstart+`</td>
                <td>`+v.tanggalstop+`</td>
                <td>`+v.tanggalfinished+`</td>
                <td>Action</td>
            </tr>`;
        })
        _tbody += '</tbody';
        _html = `<div style="margin-left: 20px">
            <div id="ts-9921050102" class="table-scrollable" style="border: 1px solid #3598dc; background-color: #3598dc; margin-top: 0px !important; margin-bottom: 0px !important">
                <table bg-color="blue" class="nowrap table table-striped table-bordered table-hover  dataTable no-footer" id="table-header-`+d.noid+`" style="background-color:#BBDEFB !important; margin: 0 !important; border: 1px solid #3598dc"  aria-describedby="table_master_info" role="grid">
                    <thead style="display: none">
                        <tr style="height: 40px">
                            <th>[H]</th>
                            <th>Keterangan</th>
                            <th>Keterangan</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>`;

        return {
            idmilestone: d.noid,
            idmaster: d.idmaster,
            html: _html,
        };
    }

    function getDatatableMilestone(data) {
        // defaultOperation()

        $('#table-milestone').DataTable().destroy();
        mytablemilestone = $('#table-milestone').DataTable({
            "autoWidth": false,
            "pageLength": 10,
            "searching": true,
            "processing": true,
            "serverSide": true,
            "bInfo": true,
            "bFilter": false,
            "bLengthChange": false,
            "columns": [
                {className:'milestones-control',orderable:false,data:null,defaultContent:''},
                {mData:'keterangan'},
                {mData:'action',orderable:false},
            ],
            "columnDefs": [
                {'targets':0,'width':'5px'},
                // {'targets':1,'width':'5px'},
                // {'targets':1,'width':'5%'},
                // {'targets':2,'width':'5%'},
                // {'targets':3,'width':'15%'},
                // {'targets':4,'width':'15%'},
                // {'targets':5,'width':'15%'},
                // {'targets':6,'width':'15%'},
                // {'targets':7,'width':'15%'},
            ],
            "infoCallback": function( settings, start, end, max, total, pre ) {
                var options = '';
                $.each(settings.aLengthMenu, function(k, v) {
                    options += '<option value="'+v+'" id="alm'+v+'" class="alm">'+v+'</option>'
                })
                if ((end-start) == settings._iDisplayLength-1) {
                    var valuepage = end/settings._iDisplayLength;
                } else {
                    var valuepage = ((start-1)/settings._iDisplayLength)+1;
                }
                if (start == 1) {
                    var prevdisabled = 'disabled'
                } else {
                    var prevdisabled = '';
                }
                if (end == total) {
                    var nextdisabled = 'disabled'
                } else {
                    var nextdisabled = '';
                }
                // if (settings.oPreviousSearch.sSearch == $('#searchform').val()) {
                //     alert($('#searchform').val())
                //     var valuesearch = $('#searchform').val()
                // } else {
                //     var valuesearch = $('#searchform').val()
                //     alert('else'+$('#searchform').val())
                // }
                if (settings.oPreviousSearch.sSearch != '') {
                    statussearch = true
                    searchtablewidth = 200
                    valuesearch = settings.oPreviousSearch.sSearch
                } else {
                    statussearch = false
                    searchtablewidth = 30
                    valuesearch = ''
                }
                // console.log('settings')
                // console.log(settings)
                // console.log(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)
                $('.alm').removeAttr('selected');
                $('#alm'+settings._iDisplayLength).attr('selected', 'selected');
                // console.log(valuepage)
                $('.dataTables_info').css('display', 'none')
                $('.dataTables_paginate').css('display', 'none')
            },
            "ajax": {
                "url": "/get_milestone"+main.urlcode,
                "type": "POST",
                "dataType":'json',
                "data": {
                    "_token": $('#token').val(),
                    'data': data ? data : '[]',
                    'statusdetail': statusdetail,
                    'condition': JSON.stringify(main.condition[status.crudmaster])
                },
            },
            "pagingType": "input",
            // "createdRow": function(row, data, dataIndex) {
            //     let right = statusdetail ? 60 : 38;
            //     $(row).attr('onmouseover', 'showButtonActionTab(this, '+dataIndex+', '+right+')');
            //     $(row).attr('onmouseout', 'resetCssTab()');
            // },
            "oLanguage":{
                "loadingRecords": "Please wait - loading...",
                "sProcessing": "<div id='loadernya'></div>",
                "sInfo": "&nbsp;|&nbsp;Total&nbsp;_TOTAL_ records",
                "sLengthMenu": "&nbsp;|&nbsp;Rec/Page&nbsp;_MENU_",
                "sInfoFiltered": "",
                "sInfoEmpty": "| No records found to show",
                "sEmptyTable": "No data available in table",
                "sZeroRecords": "No matching records found",
                "sSearch": "<button onclick='toggleButtonSearch()' class='btn flat dtfilter_search_btn' style='background-color: white'><i class='fa fa-search'></i></button>",
                "oPaginate": {
                    "sPrevious": "<i class='fa fa-angle-left'></i>",
                    "sNext": "<i class='fa fa-angle-right'></i> ",
                }
            },
            // footerCallback: function ( row, data, start, end, display ) {
            //     var api = this.api(), data;
            //     var intVal = function ( i ) {
            //         return typeof i === 'string' ?
            //             i.replace(/[\$,]/g, '')*1 :
            //             typeof i === 'number' ?
            //                 i : 0;
            //     };
    
            //     var sum = function ( i ) {
            //         var value = 0;
            //         $.each(api.column( i ).data(), function(k, v) {
            //             var _split = v.split('.');
            //             var value2 = '';
            //             $.each(_split, function(k2, v2) {
            //                 value2 += v2
            //             })
            //             value += Number(value2)
            //         })
            //         return value
            //     }
            //     var addcommas = function addCommas(nStr) {
            //         nStr += '';
            //         var x = nStr.split('.');
            //         var x1 = x[0];
            //         var x2 = x.length > 1 ? '.' + x[1] : '';
            //         var rgx = /(\d+)(\d{3})/;
            //         while (rgx.test(x1)) {
            //             x1 = x1.replace(rgx, '$1' + '.' + '$2');
            //         }
            //         return x1 + x2;
            //     }

            //     pageTotal = api
            //         .column( 9, { page: 'current'} )
            //         .data()
            //         .reduce( function (a, b) {
            //             return intVal(a) + intVal(b);
            //         }, 0 );
            //     // Update footer
            //     // console.log('akak')
            //     // console.log(eval(api.column(9).data()[0]))
            //     $( api.column(2).footer() ).html(data.length);
            //     $( api.column(7).footer() ).html(api.column(7).data().reduce( function (a, b) { return parseInt(a)+parseInt(b); }, 0 ));
            //     $( api.column(8).footer() ).html(api.column(8).data().reduce( function (a, b) { return parseInt(a)+parseInt(b); }, 0 ));
            //     $( api.column(11).footer() ).html(api.column(11).data().reduce( function (a, b) { return pageTotal; }, 0 ));
            //     var $th = $(row).find('th').eq(11);
            //     $th.text( $.fn.dataTable.render.number(',', '.', 0, 'Rp.').display( $th.text() )) 
            //     // $( api.column(6).footer() ).html(data.length);
            // }
        });

        if (!statusgetmilestone) {
            $('#table-milestone tbody').on('click', 'td.milestones-control', function(){
                var tr = $(this).closest('tr');
                var row = mytablemilestone.row( tr );

                if(row.child.isShown()){
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                } else {
                    // Open this row
                    let _formatdata = formatMilestone(row.data());
                    row.child(_formatdata.html).show();
                    tr.addClass('shown');
                    // console.log('asolole')
                    // console.log(tr)
                    
                    getDatatableHeader({
                        idmilestone: _formatdata.idmilestone,
                        idmaster: _formatdata.idmaster,
                    })
                }
            });
            statusgetmilestone = true;
        }
    }

    function formatHeader(d) {
        // console.log(d)
        let _tbody = '<tbody>';
        let _no = 1;
        $.each(d.detail, function(k,v){
            _tbody += `<tr>
                <td class="details-control"></td>
                <td style="width: 30px;">
                    <input type="checkbox" id="select_all_9921050102" onchange="selectAllCheckboxTab(9921050102)" class="checkbox-9921050102 checkbox-9921050102-0" name="checkbox-9921050102[]" value="FALSE">
                </td>
                <td>`+_no+`</td>
                <td>`+v.kodeprev+`</td>
                <td>`+v.kodeinv+`</td>
                <td>`+v.nama+`</td>
                <td>`+v.inventor+`</td>
                <td>`+v.keterangan+`</td>
                <td>`+v.qty+`</td>
                <td>`+v.qtysisa+`</td>
                <td>`+v.unit+`</td>
                <td>`+v.price+`</td>
                <td>`+v.total+`</td>
                <td>`+v.currency+`</td>
                <td>Action</td>
            </tr>`;
        })
        _tbody += '</tbody';
        _html = `<div style="margin-left: 20px">
            <div id="appendInfoDatatableDetail-`+d.noid+`" style="border-top: 2px solid #3598dc; border-right: 3px solid #3598dc; border-left: 2px solid #3598dc;">
            </div>
            <div id="ts-9921050102" class="table-scrollable" style="border: 1px solid #3598dc; background-color: #3598dc; margin-top: 0px !important; margin-bottom: 0px !important">
                <table bg-color="blue" class="nowrap table table-striped table-bordered table-hover  dataTable no-footer" id="table-detail-`+d.noid+`" style="background-color:#BBDEFB !important; margin: 0 !important; border: 1px solid #3598dc"  aria-describedby="table_master_info" role="grid">
                    <thead>
                        <tr style="height: 40px !important; background: #BBDEFC">
                            <th style="width: 30px;">
                                <input type="checkbox" id="select_all_9921050102" onchange="selectAllCheckboxTab(9921050102)" class="checkbox-9921050102 checkbox-9921050102-0" name="checkbox-9921050102[]" value="FALSE">
                            </th>
                            <th>No</th>
                            <th>Kode Prev.</th>
                            <th>Kode Inv.</th>
                            <th>Nama</th>
                            <th>Inventor</th>
                            <th>Keterangan</th>
                            <th>Qty.</th>
                            <th>Qty. Sisa</th>
                            <th>Unit</th>
                            <th>Price</th>
                            <th>Total</th>
                            <th>Currency</th>
                            <th id="th-action">Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr style="background: #BBDEFC">
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>`;

        return {
            idheader: d.noid,
            idmaster: d.idmaster,
            html: _html,
        };
    }

    function getDatatableHeader(params) {
        // defaultOperation()

        $('#table-header-'+params.idmilestone).DataTable().destroy();
        mytableheader = $('#table-header-'+params.idmilestone).DataTable({
            "autoWidth": false,
            "pageLength": 10,
            "searching": true,
            "processing": true,
            "serverSide": true,
            "bInfo": true,
            "bFilter": false,
            "bLengthChange": false,
            "columns": [
                {className:'headers-control',orderable:false,data:null,defaultContent:''},
                {mData:'keterangan'},
                {mData:'action',orderable:false},
            ],
            "columnDefs": [
                {'targets':0,'width':'5px'},
                // {'targets':1,'width':'5px'},
                // {'targets':1,'width':'5%'},
                // {'targets':2,'width':'5%'},
                // {'targets':3,'width':'15%'},
                // {'targets':4,'width':'15%'},
                // {'targets':5,'width':'15%'},
                // {'targets':6,'width':'15%'},
                // {'targets':7,'width':'15%'},
            ],
            "infoCallback": function( settings, start, end, max, total, pre ) {
                var options = '';
                $.each(settings.aLengthMenu, function(k, v) {
                    options += '<option value="'+v+'" id="alm'+v+'" class="alm">'+v+'</option>'
                })
                if ((end-start) == settings._iDisplayLength-1) {
                    var valuepage = end/settings._iDisplayLength;
                } else {
                    var valuepage = ((start-1)/settings._iDisplayLength)+1;
                }
                if (start == 1) {
                    var prevdisabled = 'disabled'
                } else {
                    var prevdisabled = '';
                }
                if (end == total) {
                    var nextdisabled = 'disabled'
                } else {
                    var nextdisabled = '';
                }
                // if (settings.oPreviousSearch.sSearch == $('#searchform').val()) {
                //     alert($('#searchform').val())
                //     var valuesearch = $('#searchform').val()
                // } else {
                //     var valuesearch = $('#searchform').val()
                //     alert('else'+$('#searchform').val())
                // }
                if (settings.oPreviousSearch.sSearch != '') {
                    statussearch = true
                    searchtablewidth = 200
                    valuesearch = settings.oPreviousSearch.sSearch
                } else {
                    statussearch = false
                    searchtablewidth = 30
                    valuesearch = ''
                }
                // console.log('ngentot')
                // console.log(settings)
                // console.log(start)
                // console.log(end)
                // console.log(max)
                // console.log(total)
                // console.log(pre)
                // $('#appendInfoDatatableHeader-'+settings.json.data[0].idmilestone).html(`
                // <div class="row" style="background: #90CAF9; padding-left: 15px; padding-right: 15px;">
                //     <div class="col-md-12">
                //         Page
                //         <button id="prevpagemilestone" onclick="prevpagemilestone()" class="btn btn-sm default prev `+prevdisabled+`" style="background-color: #2196F3;">
                //             <i class="fa fa-angle-left">
                //             </i>
                //         </button>
                //         <input type="text" id="valuepagemilestone" onkeyup="valuepagemilestone()" class="input-mini" value="`+valuepage+`" style="text-align: center; border: 0px; margin: 5px 5px 5px 5px">
                //         <button id="nextpagemilestone" onclick="nextpagemilestone()" class="btn btn-sm default next `+nextdisabled+`" style="background-color: #2196F3;">
                //             <i class="fa fa-angle-right">
                //             </i>
                //         </button> of <span class="pagination-panel-total">`+(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)+`</span>
                //         <span class="seperator">|</span>
                //         Rec/Page 
                //         <select id="alengthmenumilestone" onchange="alengthmenumilestone()" name="table_master_length" aria-controls="table_master" class="input-inline input-xsmall input-sm recofdt" style="border: 0px; margin: 5px 5px 5px 5px">
                //             `+options+`
                //         </select>
                //         <span class="seperator">|</span>Total `+settings._iRecordsTotal+` records
                //         <input id="searchformmilestone" onkeyup="searchformmilestone()" value="`+valuesearch+`" type="search" class="form-control input-inline input-sm clicked" placeholder="" aria-controls="table_master" style="width: `+searchtablewidth+`px; position: absolute; right: 0px; top: 4px; height: 80%; border: 0;">
                //         <div onclick="searchtablemilestone()" class="btn fa fa-search text-right" style="position: absolute; right: 0px; top: 4px; height: 80%; background: transparent; margin: 0px 0px 0px 0px; padding: 5px 0px 5px 0px">
                //             <i class="fa fa-search"></i>
                //         </div>
                //     </div>
                // </div>`)
                console.log('settings')
                console.log(settings)
                console.log(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)
                $('.alm').removeAttr('selected');
                $('#alm'+settings._iDisplayLength).attr('selected', 'selected');
                // console.log(valuepage)
                $('.dataTables_info').css('display', 'none')
                $('.dataTables_paginate').css('display', 'none')
            },
            "ajax": {
                "url": "/get_header"+main.urlcode,
                "type": "POST",
                "dataType":'json',
                "data": {
                    "_token": $('#token').val(),
                    // 'data': data ? data : '[]',
                    // 'statusdetail': statusdetail,
                    'condition': JSON.stringify(main.condition[status.crudmaster]),
                    'idmilestone': params.idmilestone,
                    'idmaster': params.idmaster,
                    'milestone': localStorage.getItem(main.tablemilestone)
                },
            },
            "pagingType": "input",
            // "createdRow": function(row, data, dataIndex) {
            //     let right = statusdetail ? 60 : 38;
            //     $(row).attr('onmouseover', 'showButtonActionTab(this, '+dataIndex+', '+right+')');
            //     $(row).attr('onmouseout', 'resetCssTab()');
            // },
            "oLanguage":{
                "loadingRecords": "Please wait - loading...",
                "sProcessing": "<div id='loadernya'></div>",
                "sInfo": "&nbsp;|&nbsp;Total&nbsp;_TOTAL_ records",
                "sLengthMenu": "&nbsp;|&nbsp;Rec/Page&nbsp;_MENU_",
                "sInfoFiltered": "",
                "sInfoEmpty": "| No records found to show",
                "sEmptyTable": "No data available in table",
                "sZeroRecords": "No matching records found",
                "sSearch": "<button onclick='toggleButtonSearch()' class='btn flat dtfilter_search_btn' style='background-color: white'><i class='fa fa-search'></i></button>",
                "oPaginate": {
                    "sPrevious": "<i class='fa fa-angle-left'></i>",
                    "sNext": "<i class='fa fa-angle-right'></i> ",
                }
            },
            // footerCallback: function ( row, data, start, end, display ) {
            //     var api = this.api(), data;
            //     var intVal = function ( i ) {
            //         return typeof i === 'string' ?
            //             i.replace(/[\$,]/g, '')*1 :
            //             typeof i === 'number' ?
            //                 i : 0;
            //     };
    
            //     var sum = function ( i ) {
            //         var value = 0;
            //         $.each(api.column( i ).data(), function(k, v) {
            //             var _split = v.split('.');
            //             var value2 = '';
            //             $.each(_split, function(k2, v2) {
            //                 value2 += v2
            //             })
            //             value += Number(value2)
            //         })
            //         return value
            //     }
            //     var addcommas = function addCommas(nStr) {
            //         nStr += '';
            //         var x = nStr.split('.');
            //         var x1 = x[0];
            //         var x2 = x.length > 1 ? '.' + x[1] : '';
            //         var rgx = /(\d+)(\d{3})/;
            //         while (rgx.test(x1)) {
            //             x1 = x1.replace(rgx, '$1' + '.' + '$2');
            //         }
            //         return x1 + x2;
            //     }

            //     pageTotal = api
            //         .column( 9, { page: 'current'} )
            //         .data()
            //         .reduce( function (a, b) {
            //             return intVal(a) + intVal(b);
            //         }, 0 );
            //     // Update footer
            //     // console.log('akak')
            //     // console.log(eval(api.column(9).data()[0]))
            //     $( api.column(2).footer() ).html(data.length);
            //     $( api.column(7).footer() ).html(api.column(7).data().reduce( function (a, b) { return parseInt(a)+parseInt(b); }, 0 ));
            //     $( api.column(8).footer() ).html(api.column(8).data().reduce( function (a, b) { return parseInt(a)+parseInt(b); }, 0 ));
            //     $( api.column(11).footer() ).html(api.column(11).data().reduce( function (a, b) { return pageTotal; }, 0 ));
            //     var $th = $(row).find('th').eq(11);
            //     $th.text( $.fn.dataTable.render.number(',', '.', 0, 'Rp.').display( $th.text() )) 
            //     // $( api.column(6).footer() ).html(data.length);
            // }
        });

        // if (!statusgetheader) {
            $('#table-header-'+params.idmilestone+' tbody').on('click', 'td.headers-control', function(){
                var tr = $(this).closest('tr');
                var row = mytableheader.row( tr );

                if(row.child.isShown()){
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                } else {
                    // Open this row
                    let _formatdata = formatHeader(row.data());
                    row.child(_formatdata.html).show();
                    tr.addClass('shown');
                    
                    getDatatableDetail({
                        idheader: _formatdata.idheader,
                        idmaster: _formatdata.idmaster,
                    })
                }
            });
        //     statusgetheaders = true;
        // }
    }

    function showModalDelete(noid) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You want to delete this?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            target: '#pagepanel'
        }).then((result) => {
            if (result.isConfirmed) {
                removeData(noid);
                Swal.fire({
                    icon: 'success',
                    title: 'Deleted!',
                    text: 'Delete data successfully.',
                    target: '#pagepanel',
                });
            }
        })
        return false;
        $('#remove-data').attr('onclick', 'removeData('+noid+')')
        $('#modal-delete').modal('show')
    }

    function hideModalDelete() {
        $('#modal-delete').modal('hide')
    }

    function showModalSendToOrder(noid, fromview=false) {
        // Swal.fire({
        //     title: 'Are you sure?',
        //     text: 'You wanna '+main.sendtonext+'?',
        //     icon: 'warning',
        //     showCancelButton: true,
        //     confirmButtonColor: '#3085d6',
        //     cancelButtonColor: '#d33',
        //     confirmButtonText: 'Yes',
        //     target: '#pagepanel'
        // }).then((result) => {
        //     if (result.isConfirmed) {
        //         sendToNext(noid);
        //         Swal.fire({
        //             icon: 'success',
        //             title: 'Success!',
        //             text: main.sendtonext+' successfully.',
        //             target: '#pagepanel',
        //         });
        //         if (fromview) {
        //             cancelData()
        //         }
        //     }
        // })
        // return false;
        $('#form-modal-stn #idcardsupplier-stn').val('').trigger('change')
        $('#send-to-next').attr('onclick', 'sendToNext('+noid+')')
        $('#modal-send-to-next').modal('show')
    }

    function hideModalSendToOrder() {
        $('#modal-send-to-next').modal('hide')
    }

    function showModalSNST(noid, idtypetranc, idstatustranc, statustranc, fromview=true) {
        if (idstatustranc == 6024) {
            $('#modal-snst #answer-snst').val('').trigger('change');
            $('#modal-snst #answer-div').show();
        } else {
            $('#modal-snst #answer-div').hide();
        }

        if (fromview) {
            $('#modal-snst .modal-title').text('SET TO '+statustranc);
            $('#btn-snst span').text('Set to '+statustranc);
            $('#btn-snst').attr('onclick', 'SNST('+noid+','+idstatustranc+',\''+statustranc+'\','+fromview+')')
        } else {
            $('#modal-snst .modal-title').text('SAVE TO '+statustranc);
            $('#btn-snst span').text('Save to '+statustranc);
            $('#btn-snst').attr('onclick', `saveData(`+idstatustranc+`, '`+statustranc+`', '`+$('#keterangan-snst').val()+`')`)
        }

        // Rules
        $('#modal-snst #answer-snst').on('select2:close',function(e){$('#modal-snst #keterangan-snst').focus()})
        $('#modal-snst #keterangan-snst').on('keypress',function(e){if(e.keyCode==13){$('#modal-snst #btn-snst').focus();}})
        
        $('#idtypetranc-snst').val(idtypetranc);
        $('#form-modal-snst #keterangan-snst').val('');
        $('#modal-snst').modal('show');
        $('#modal-snst').on('shown.bs.modal', () => {
            idstatustranc == 6024
                ? $('#modal-snst #answer-snst').select2('focus')
                : $('#modal-snst #keterangan-snst').focus();
        })
        
    }

    function hideModalSNST() {
        $('#modal-snst').modal('hide')
        $('#form-modal-snst #idtypetranc-snst').val('').trigger('change')
        $('#form-modal-snst #keterangan-snst').val('');
    }

    function showModalDIV(noid, idtypetranc, idstatustranc) {
        $('#modal-div .modal-title').text('SET STATUS');
        $('#btn-div span').text('Set Status');
        $('#btn-div').attr('onclick', 'DIV('+noid+', '+idstatustranc+')');  
        $('#idtypetranc-div').val(idtypetranc);
        $('#modal-div #select-div').show();
        $('#modal-div #select-spcf').hide();
        $('#form-modal-div #idstatustranc-div').val('').trigger('change');
        $('#form-modal-div #keterangan-div').val('');
        $('#modal-div').modal('show')
    }

    function hideModalDIV() {
        $('#modal-div').modal('hide')
    }

    function showModalSAPCF(params) {
        let noid = params.noid;
        let idtypetranc = params.idtypetranc;
        let idstatustranc = params.idstatustranc;

        // Reset Select2
        $('#modal-sapcf #idstatustranc-sapcf').select2({
            templateResult: function(option) {
                var myOption = $('#modal-sapcf #idstatustranc-sapcf').find('option[value="' + option.id + '"]');
                if (option.id != idstatustranc) {
                        return option.text;
                }
            }
        });
        $('.select2').css('width', '100%');
        $('.select2 .selection .select2-selection').css('height', '35px');
        $('.select2 .selection .select2-selection .select2-selection__rendered').css('margin-top', '-6px');
        $('.select2 .selection .select2-selection .select2-selection__arrow').css('margin-top', '4px');

        
        $('#modal-sapcf').modal('show')
        $('#modal-sapcf #idstatustranc-sapcf').select2('focus')
        $('#modal-sapcf #idstatustranc-sapcf').on('select2:close',function(e){$('#modal-sapcf #keterangan-sapcf').focus()})
        
        $('#modal-sapcf .modal-title').text('SET STATUS');
        $('#btn-sapcf span').text('Set Status');
        $('#btn-sapcf').attr('onclick', 'SAPCF('+noid+', '+idstatustranc+')');  
        $('#idtypetranc-sapcf').val(idtypetranc);
        $('#modal-sapcf #idstatustranc-sapcf').val('').trigger('change');
        $('#modal-sapcf #keterangan-sapcf').val('');
    }

    function hideModalSAPCF() {
        $('#modal-sapcf').modal('hide')
    }

    function showModalSetPending(noid, idtypetranc) {
        $('#idtypetranc-pending').val(idtypetranc);
        $('#set-pending').attr('onclick', 'setPending('+noid+')')
        $('#modal-set-pending').modal('show')
    }

    function hideModalSetPending() {
        $('#modal-set-pending').modal('hide')
    }

    function showModalSetCanceled(noid, idtypetranc) {
        $('#idtypetranc-canceled').val(idtypetranc);
        $('#set-canceled').attr('onclick', 'setCanceled('+noid+')')
        $('#modal-set-canceled').modal('show')
    }

    function hideModalSetCanceled() {
        $('#modal-set-canceled').modal('hide')
    }

    function showModalLog(noid, kode, typetranc) {
        getDatatableLog(noid, kode)
        $('#modal-log #titlelog-log').text(typetranc)
        $('#modal-log #titlekode-log').text(kode)
        $('#modal-log').modal('show')
    }

    function hideModalLog() {
        $('#modal-log').modal('hide')
    }

    function showModalGenerateTemplate() {
        getDatatableGenerateTemplate()
        $('#modal-generatetemplate').modal('show')
    }

    function hideModalGenerateTemplate() {
        $('#modal-generatetemplate').modal('hide')
    }

    const showModalDetailPrev = (params) => {
        let searchtypeprev = $('[name=searchtypeprev]:checked');
        let idcardsupplier = $('#form-header #idcardsupplier :selected');
        
        if (searchtypeprev.val() == 1) {
            if (idcardsupplier.val()) {
                $('#modal-detailprev .supplier-title').text(idcardsupplier.text())
                $('#modal-detailprev #title-prev').show();
                $('#modal-detailprev #title-inv').hide();
                getDatatableDetailPrev(params)
                $('#modal-detailprev').modal('show')
            } else {
                $('#form-header #idcardsupplier').select2('focus');
                $.notify({
                    message: 'Supplier harus dipilih!'
                },{
                    type: 'warning'
                });
            }
        } else {
            $('#modal-detailprev #title-inv').show();
            $('#modal-detailprev #title-prev').hide();
            getDatatableDetailPrev(params)
        }
    }

    function hideModalDetailPrev() {
        $('#modal-detailprev').modal('hide')
    }

    function showModalTanggal() {
        $('#form-modal-tanggal #tanggaltax-modal').focus()
        $('#modal-tanggal').modal('show')
    }

    function hideModalTanggal() {
        $('#modal-tanggal').modal('hide')
    }

    function showModalSupplier() {
        $('#modal-supplier').modal('show')
    }

    function hideModalSupplier() {
        $('#modal-supplier').modal('hide')
    }

    function showModalDetail() {
        $('#modal-detail').modal('show')
        $('#form-modal-detail #idinventor-modal').select2('focus')
    }

    function hideModalDetail() {
        $('#modal-detail').modal('hide')
    }
    
    function currency(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return 'Rp.'+ x1 + x2;
    }

    /////////

    var maincms = {};
    var ready = false;
    var statusadd = true;
    var statusdetail = false;
    var statusaddtab = [];
    var editid = '';
    var nextnoid = '';
    var nextnoidtab = [];
    var nownoid = '';
    var nownoidtab = '';
    var nowmaintabletab = '';
    var fieldnametab = new Array();
    var tablelookuptab = new Array();
    var input_data = new Array();
    var mainonchange = new Array();
    var maintable = new Array();
    var values = $("input[name='panel[]']").map(function(){return $(this).val();}).get();
    var values2 = $("#slider").val();
    var values3 = $("#tabel1").val();
    var select_all = false;
    var select_all_tab = new Array();
    var cursor_top = 0;
    var selecteds = new Array();
    var another_selecteds = new Array();

    var selectcheckboxmall = false;
    var selectcheckboxm = new Array();
    var selectcheckboxman = new Array();
    localStorage.setItem('sc-m-a', 'false');
    localStorage.setItem('sc-m', JSON.stringify(new Array()));
    localStorage.setItem('sc-m-an', JSON.stringify(new Array()));
    // var divform = '';
    var my_table = '';
    var mytable = '';
    var mytabtable = new Array();
    var groupbtn = '';
    var selected_filter = {
        operand: [],
        fieldname: [],
        operator: [],
        value: [],
    };
    var selectedfilter = {
        'is': 'is3rdparty',
    };
    var datefilter = {
        'code': 'a',
        'custom': {
            'status': false,
            'from': $('#did-from').val(),
            'to': $('#did-to').val(),
        }
    };
    var datefilter2 = {
        'from': null,
        'to': null
    };
    var filter_tabs = new Array();
    var selected_filter_tabs = 0;
    var filtercontenttabs = {
        filtercontentgroup: new Array(),
        filtercontentbody: new Array(),
        filtercontentrow: new Array(),
    }
    var colsorted = [0,1];
    var roles = {
        fieldname: new Array(),
        newhidden: new Array(),
        edithidden: new Array(),
    };
    var cmsfilterbutton = new Array()
    var tab_datatable = new Array()

    var functionfooter = new Array();
    var functionfootertab = new Array();
    var searchtablewidth = 30;
    var tab_searchtablewidth = new Array();
    var statussearch = '';
    var tab_statussearch = new Array();
    var valuesearch = '';
    var tab_valuesearch = new Array();

    var maincms_widget = new Array();
    var maincms_widgetgrid = new Array();

    var noids = new Array();
    var noids_tab = new Array();

    function selectCheckboxMAll() {
        let mselectall = $('#checkbox-m-all').is(':checked');
        let mselect = $('.checkbox-m');
        mselect.prop('checked', mselectall);

        if (mselectall == true) {
            $('#table_master .odd').css('background-color', '#90CAF9');
            $('#table_master .even').css('background-color', '#90CAF9');
            $('#table_master .checkbox-m').val('TRUE');
            // selectcheckboxmall = true;
            // selectcheckboxm = new Array();
            // selectcheckboxm = selectcheckboxman;
            localStorage.setItem('sc-m-a', 'true');
            localStorage.setItem('sc-m', localStorage.getItem('sc-m-an'));
        } else {
            $('#table_master .odd').css('background-color', '#ffffff');
            $('#table_master .even').css('background-color', '#BBDEFB');
            $('#table_master .checkbox-m').val('FALSE');
            localStorage.setItem('sc-m-a', 'false');
            localStorage.setItem('sc-m', JSON.stringify(new Array()));
        }
    }

    function selectCheckboxM(index) {
        let checkbox = $('#checkbox-m-'+index);

        if (checkbox.val() == 'FALSE') {
            $('#table_master .row-m-'+index).css('background-color', '#90CAF9');
            $('#table_master #checkbox-m-'+index).val('TRUE');
            let scm = JSON.parse(localStorage.getItem('sc-m'));
            selectcheckboxm.push(index);
            scm.push(index);
            localStorage.setItem('sc-m', JSON.stringify(scm));
        } else {
            if (index % 2 == 0) {
                $('#table_master .row-m-'+index).css('background-color', '#ffffff');
            } else {
                $('#table_master .row-m-'+index).css('background-color', '#BBDEFB');
            }
            $('#table_master #checkbox-m-'+index).val('FALSE')
            var replaceselecteds = new Array();
            $.each(selectcheckboxm, function(k, v) {
                if (v != index) {
                    replaceselecteds.push(v)
                }
            })
            selectcheckboxm = replaceselecteds

            let replacescm = new Array();
            $.each(JSON.parse(localStorage.getItem('sc-m')), function(k, v) {
                if (v != index) {
                    replacescm.push(v)
                }
            })
            localStorage.setItem('sc-m', JSON.stringify(replacescm));
        }
        selectcheckboxmall = false;
        localStorage.setItem('sc-m-a', 'false');
        $('#checkbox-m-all').prop('checked', false);
        // console.log(selecteds)
    }

    function selectAllCheckboxTab(panel_noid) {
        var checkboxes = $('[name="checkbox-'+panel_noid+'[]"]');
        checkboxes.prop('checked', $('#select_all_'+panel_noid).is(':checked'));

        if (select_all_tab[panel_noid] == false) {
            select_all_tab[panel_noid] = true;
            $('#tab-table-'+panel_noid+' .odd').css('background-color', '#90CAF9');
            $('#tab-table-'+panel_noid+' .even').css('background-color', '#90CAF9');
            $('#tab-table-'+panel_noid+' .checkbox-'+panel_noid).val('TRUE');
            another_selecteds[panel_noid] = noids_tab[panel_noid];
        } else {
            select_all_tab[panel_noid] = false;
            $('#tab-table-'+panel_noid+' .odd').css('background-color', '#ffffff');
            $('#tab-table-'+panel_noid+' .even').css('background-color', '#BBDEFB');
            $('#tab-table-'+panel_noid+' .checkbox-'+panel_noid).val('FALSE');
            another_selecteds[panel_noid] = new Array();
        }
        console.log('another_selecteds')
        console.log(another_selecteds)
    }
    
    function changeCheckbox(index, id) {
        var checkbox = $('.checkbox'+index);
        if (checkbox.val() == 'FALSE') {
            $('.row'+index).css('background-color', '#90CAF9');
            $('.checkbox'+index).val('TRUE');
            selecteds.push(id);
        } else {
            if (index % 2 == 0) {
                $('.row'+index).css('background-color', '#ffffff');
            } else {
                $('.row'+index).css('background-color', '#BBDEFB');
            }
            $('.checkbox'+index).val('FALSE')
            var replace_selecteds = new Array();
            $.each(selecteds, function(k, v) {
                if (v != id) {
                    replace_selecteds.push(v)
                }
            })
            selecteds = replace_selecteds
        }
        select_all = false;
        $('#select_all').prop('checked', false);
        // console.log(selecteds)
    }

    function changeCheckboxTab(panel_noid, index, id) {
        var checkbox = $('.checkbox-'+panel_noid+'-'+index);
        // alert(checkbox.val())
        if (checkbox.val() == 'FALSE') {
            $('#tab-table-'+panel_noid+' .row'+index).css('background-color', '#90CAF9');
            $('#tab-table-'+panel_noid+' .checkbox-'+panel_noid+'-'+index).val('TRUE');
            another_selecteds[panel_noid].push(id);
        } else {
            if (index % 2 == 0) {
                $('#tab-table-'+panel_noid+' .row'+index).css('background-color', '#ffffff');
            } else {
                $('#tab-table-'+panel_noid+' .row'+index).css('background-color', '#BBDEFB');
            }
            $('#tab-table-'+panel_noid+' .checkbox-'+panel_noid+'-'+index).val('FALSE')
            // another_selecteds.pop(id);
            // delete another_selecteds[index]
            var replace_another_selecteds = new Array();
            $.each(another_selecteds[panel_noid], function(k, v) {
                if (v != id) {
                    replace_another_selecteds.push(v)
                }
            })
            another_selecteds[panel_noid] = replace_another_selecteds
        }
        select_all_tab[panel_noid] = false;
        $('#select_all_'+panel_noid).prop('checked', false);
        console.log('another_selecteds')
        console.log(another_selecteds)
    }

    function mouseOver(element_id, color) {
        $('#'+element_id).css('background-color', color);
        $('#'+element_id).css('color', 'white');
    }

    function mouseOut(element_id, color) {
        $('#'+element_id).removeAttr('style');
        $('#'+element_id).css('background-color', color);
        $('#'+element_id).css('color', 'white');
    }

    function showButtonAction(element, index, right=28) {
        var rect = element.getBoundingClientRect();

        this.resetCss()

        // $('.dataTable tbody td').css('height', '100px !important')
        // $('.dataTable tbody td:nth-child('+index+')').css('background', 'black')
        // $('.my_row').css('height', 35)
        $('.button-action-'+index).css('position', 'fixed')
        $('.button-action-'+index).css('top', parseInt(rect.y+(5)))
        $('.button-action-'+index).css('right', right+'px')

        // window.scrollTo(1,0);
        // $(`#table_master tbody tr td div #btn-delete-data-${noid}`).attr('data-placement', 'left')
        // $(`#table_master tbody tr td div #btn-delete-data-${noid}`).attr('data-offset', parseInt(rect.y+(5)))
        // $('.button-action-'+index).css('overflow', 'hidden')

        // console.log(parseInt(rect.y+8))
        // console.log('Element is ' + offset + ' vertical pixels from <body>');
    }

    function showButtonActionTab(element, index, right=38, idelement) {
        var rect = element.getBoundingClientRect();

        this.resetCssTab({dataIndex:index,idelement:idelement})

        $('.button-action-tab-'+index+'-'+idelement).css('position', 'fixed')
        $('.button-action-tab-'+index+'-'+idelement).css('top', parseInt(rect.y+5))
        $('.button-action-tab-'+index+'-'+idelement).css('right', right+'px')
        // $('.button-action-'+index).css('overflow', 'hidden')

        // console.log(parseInt(rect.y+8))
        // console.log('Element is ' + offset + ' vertical pixels from <body>');
    }

    function showButtonActionTabPrev(element, index, right=38) {
        var rect = element.getBoundingClientRect();

        this.resetCssTabPrev()

        $('.button-action-dprev-'+index).css('position', 'fixed')
        $('.button-action-dprev-'+index).css('top', parseInt(rect.y+5))
        $('.button-action-dprev-'+index).css('right', right+'px')
        // $('.button-action-'+index).css('overflow', 'hidden')

        // console.log(parseInt(rect.y+8))
        // console.log('Element is ' + offset + ' vertical pixels from <body>');
    }

    function resetCss() {
        $('.button-action').removeAttr('style')
    }

    function resetCssTab(params) {
        $('.button-action-tab-'+params.dataIndex+'-'+params.idelement).removeAttr('style')
    }

    function resetCssTabPrev() {
        $('.button-action-dprev').removeAttr('style')
    }

    function mouseoverAddData(element_id) {
        $('#'+element_id).css('background', '#337AB7')
    }

    function mouseoutAddData(element_id) {
        $('#'+element_id).css('background', '#3598dc')
    }
    
    function openFullscreen() {
        document.documentElement.requestFullscreen().catch((e) => {console.log(e)})
    }

    function getFullscreenElement() {
        return document.fullscreenElement
            || document.webkitFullscreenElement
            || document.mozFullscreenElement
            || document.msFullscreenElement;
    }

    function readyFullscreen() {
        if (localStorage.getItem(main.table+'-fullscreen') == 'true') {
            $('#tool_fullscreen i').removeClass('icon-fullscreen');
            $('#tool_fullscreen i').addClass('icon-remove-sign');
            document.getElementById('pagepanel').requestFullscreen().catch((e) => {})
        } else {
            $('#tool_fullscreen i').removeClass('icon-remove-sign');
            $('#tool_fullscreen i').addClass('icon-fullscreen');
            document.exitFullscreen();
        }
    }

    function toggleFullscreen() {
        if (localStorage.getItem(main.table+'-fullscreen') == 'true') {
            localStorage.setItem(main.table+'-fullscreen', 'false');
        } else {
            localStorage.setItem(main.table+'-fullscreen', 'true');
        }
        readyFullscreen();
        
        return false;

        if (getFullscreenElement() || localStorage.setItem(main.table+'-fullscreen', 'false') == 'false') {
            $('#tool_fullscreen i').removeClass('icon-remove-sign');
            $('#tool_fullscreen i').addClass('icon-fullscreen');
            localStorage.setItem(main.table+'-fullscreen', 'false');
            document.exitFullscreen();
        } else {
            $('#tool_fullscreen i').removeClass('icon-fullscreen');
            $('#tool_fullscreen i').addClass('icon-remove-sign');
            localStorage.setItem(main.table+'-fullscreen', 'true');
            document.getElementById('pagepanel').requestFullscreen().catch((e) => {})
        }
    }

    function filterDate(code) {
        datefilter.code = code;
        if (code == 'cr') {
            datefilter.custom.status = true;
            let getfrom = $('#did-from').val().split('-');
            let getto = $('#did-to').val().split('-');
            let from = getfrom[2]+'-'+getfrom[1]+'-'+getfrom[0];
            let to = getto[2]+'-'+getto[1]+'-'+getto[0];
            let f = moment(from).format('MMM DD, YYYY');
            let t = moment(to).format('MMM DD, YYYY');
            let span = f+' - '+t;        
            $('#dropdown-date-filter span').text(span);
        } else {
            $('#dropdown-date-filter span').text($('#did-'+code).attr('data-date'));
        }
        main.datefilterdefault = code;
        hoverFilterDate(code);
        getDatatable();
    }

    function changeCustomRange() {
        datefilter.custom.from = $('#did-from').val();
        datefilter.custom.to = $('#did-to').val();
    }

    $('#did-from').on('change',function(e){$('#did-to').focus()})

    function hoverFilterDate(code) {
        $('.dropdown-item-date span').css('background-color','#f5f5f5');
        $('.dropdown-item-date span').css('color','#08c');
        $('#did-'+code+' span').css('background-color','#4b8df8')
        $('#did-'+code+' span').css('color','white')

        $('#did-'+main.datefilterdefault+' span').css('background-color','#4b8df8')
        $('#did-'+main.datefilterdefault+' span').css('color','white')
    }

    function hoverOutFilterDate(code) {
        if (code != main.datefilterdefault) {
            $('#did-'+code+' span').css('background-color','#f5f5f5')
            $('#did-'+code+' span').css('color','#08c')
        }
    }
    
    $(document).ready(() => {
        // showLoading();
        // setDefaultToggleAside();

        // $('#kt_breadcrumb').attr('style', `width: ${screen.width-265}px`);

        $('#from-departement').hide(); // Hide form
        $('#tool-kedua').hide(); // Hide cancel
        $('.datepicker').datepicker({ format: 'd-m-yyyy' });
        $('.datepicker2').datepicker({ format: 'd-m-yyyy' });
        $('.container').css('display', 'block'); // Show data
        
        customDocumentForm(); // Build CDF
        $('.modal.fade').appendTo('#pagepanel'); // Add modal on mode fullscreen
        getDatatable().then(() => hideLoading());
        readyFullscreen();
        
        ready = true;
    });



    let cdf_istabinfo = ''; //details/attributes
    let cdf_details = [];
    let cdf_checks = [];
    let cdf_selecteds = {};
    let cdf_show = 0;
    let cdf_btn_show = `<a id="cdf_btn_show_prev" class="btn btn-primary btn-sm" onclick="CDFshow({type:'prev'})"><i class="icon-arrow-left"></i></a>
                        <a id="cdf_btn_show_next" class="btn btn-primary btn-sm" onclick="CDFshow({type:'next'})"><i class="icon-arrow-right"></i></a>`;

    function CDFshowButton(params) {
        $('#fm-btn-action-'+params.elementid).animate({top: '0px'});
    }
    
    function CDFhideButton(params) {
        // $('#fm-btn-action-'+params.elementid).animate({top: '-40px'});
    }

    function CDFshowModalDocumentViewer(params) {
        console.log(cdf_checks)
        console.log(cdf_details)

        if (cdf_checks.length > 0) {
            $.each(cdf_checks, function(k, v) {
                // alert('#fm_contentitems-'+params.elementid+'-'+v)
                // $('#fm_contentitems-'+params.elementid+'-'+v).removeAttr('style')
                $('#fm_contentitems-'+params.elementid+'-'+v).attr('style', 'height: 139px; overflow: hidden;  border: 1px solid grey;');
            })
        }

        $('#modal-document-viewer-'+params.elementid).modal('show');

        if (cdf_details.length > 0) {
            let _cdf_checks = [];
            $.each(cdf_details, function(k, v) {
                // alert(2)
                $('#fm_contentitems-'+params.elementid+'-'+v).css('box-shadow', '0px 0px 5px 5px #9a12b3');
                _cdf_checks.push(v);
            })
            cdf_checks = _cdf_checks;
            $('#cdf-myfiles-'+params.elementid).trigger('click')
        } else {
            cdf_checks = [];
            if (cdf_details.length > 0) {
                // CDFtoggleFmBtn({elementid:params.elementid,type:'myfiles'});
                $('#cdf-myfiles-'+params.elementid).trigger('click')
            } else {
                $('#cdf-upload-'+params.elementid).trigger('click')
            }
        }

    }

    function CDFhideModalDocumentViewer(params) {
        $('#modal-document-viewer-'+params.elementid).modal('hide');
    }

    function CDFtoggleFmBtn(params) {
        let elementid = params.elementid;
        let type = params.type;
        let where = params.where ? params.where : 0;
        CDFgetFile({elementid:elementid,whereformat:where,wheresearch:$('#cdf-search').val()});
        $('.fm_panelcontent').hide();
        // $('#fm_meta-'+elementid).hide();

        var id = '';
        var id2 = '';
        if (type == 'upload') {
            id = '#content_upload-'+elementid;
            id2 = '#cdf-upload-'+elementid+' span';
        } else if (type == 'myfiles') {
            id = '#content_myfile-'+elementid;
            id2 = '#cdf-myfiles-'+elementid+' span';
        } else if (type == 'library') {
            id = '#content_cdf-library-'+elementid;
            id2 = '#cdf-library-'+elementid+' span';
        }

        $('.fm_btnlibrary a span').hide()
        $(id2).css('display', 'inline-block');
        $(id).show();
    }

    function CDFchooseFile(params) {
        $('#fm-btn-action-'+params.elementid).html(`<div class="fm-btn-action">
            <div class="btn btn-sm green btn_hide" id="cdf-change" onclick="CDFshowModalDocumentViewer({elementid:'`+params.elementid+`',refresh:true})">
                <span id="icon">
                    <i class="fa fa-edit text-light"></i>
                </span> 
                <span id="caption">Change</span>
            </div>
            <div class="btn btn-sm blue btn_hide" id="view_img" style="" onclick="CDFshowPreviewFile({elementid:'`+params.elementid+`',urlfile:'`+params.urlfile+`',title:'`+params.title+`'})">
                <i class="fa fa-search text-light"></i>
            </div>
            <div class="btn btn-sm red btn_hide" id="clear_img" style="" onclick="CDFremoveFile({elementid:'`+params.elementid+`'})">
                <i class="fa fa-trash text-light"></i>
            </div>
        </div>`);
        $('#content_image-'+params.elementid+' .fm-image-content img').attr('src', params.urlfile);
        $('#title_img-'+params.elementid).text(params.stitle);
        $('#modal-document-viewer-'+params.elementid).modal('hide');
        $('#'+params.elementid).val(params._noid);
    }

    function CDFremoveFile(params) {
        $('#fm-btn-action-'+params.elementid).html(`<div class="btn btn-sm green btn_hide" id="iddmslogo" onclick="CDFshowModalDocumentViewer({elementid:'`+params.elementid+`'})">
            <span id="icon">
                <i class="fa fa-plus text-light"></i>
            </span> 
            <span id="caption">File Manager</span>
        </div>`);
        $('#content_image-'+params.elementid).html(`<div class="modal-image-content" style="width: 100%; height: inherit; text-align: -webkit-center">
            <div class="fm-image-content" style="vertical-align: middle; display: table-cell; width: 100%; height: inherit; text-align: -webkit-center">
                <img src="`+location.origin+`/filemanager/default/noimage.png" style="max-width:100%;max-height:200px;">
            </div>
        </div>`);
        $('#title_img-'+params.elementid).text('File Manager');
        $('#'+params.elementid).val('');
    }

    function CDFgetFile(params) {
        let elementid = params.elementid;
        let whereformat = params.whereformat;
        let wheresearch = params.wheresearch;
        var domain = location.origin;

        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            url: "/cdf_get"+main.urlcode,
            dataType: "JSON",
            data:{
                "_token":$('#token').val(),
                "table": values3,
                "whereformat": whereformat,
                "wheresearch": wheresearch
            },
            success: function(response) {
                console.log('asek');
                console.log(response);
                var html = '';
                for (var i = 0; i<response.length; i++) {
                    let preview = '';
                    let thumbnail = '';
                    if (response[i]['fileextention'] == 'PNG' || response[i]['fileextention'] == 'JPG' || response[i]['fileextention'] == 'JPEG' || response[i]['fileextention'] == 'GIF') {
                        original = domain+`/filemanager/original/`+response[i]['filename'];
                        preview = domain+`/filemanager/preview/`+response[i]['filename'];
                        thumbnail = domain+`/filemanager/thumb/`+response[i]['filename'];
                    } else {
                        original = domain+`/filemanager/original/`+response[i]['filename'];
                        preview = domain+`/`+response[i]['fileimage'];
                        thumbnail = domain+`/`+response[i]['fileimage'];
                    }
                    html += `<div class="col-md-2 col-xs-4 fm_content_grid_items" id="fm_content_grid_item" style="margin-bottom: 4px; padding: 4px; text-align: -webkit-center; height: 100%" onmouseover="CDFmouseoverGridItem('#content_myfile-`+elementid+` .row .fm_content .row #panelcontent .row .fm_content_grid_items #fm_contenttitle-`+response[i]['noid']+`', '#content_myfile-`+elementid+` .row .fm_content .row #panelcontent .row .fm_content_grid_items #fm_contenttitle_action-`+response[i]['noid']+`')" onmouseout="CDFmouseoutGridItem('#content_myfile-`+elementid+` .row .fm_content .row #panelcontent .row .fm_content_grid_items #fm_contenttitle-`+response[i]['noid']+`', '#content_myfile-`+elementid+` .row .fm_content .row #panelcontent .row .fm_content_grid_items #fm_contenttitle_action-`+response[i]['noid']+`')">
                                <div class="fm_contentitems" id="fm_contentitems-`+elementid+`-`+response[i]['noid']+`" style="height: 139px; overflow: hidden;  border: 1px solid grey;" onclick="CDFselectContentItem({elementid:'`+elementid+`',_noid:`+response[i]['noid']+`,urlfile:'`+thumbnail+`',nama:'`+response[i]['nama']+`',title:'`+response[i]['filename']+`',size:'`+response[i]['filesize']+`',type:'`+response[i]['fileextention']+`',modified:'`+response[i]['lastupdate']+`',created:'`+response[i]['docreate']+`',ispublic:'`+response[i]['ispublic']+`',classicon:'`+response[i]['classicon']+`',classcolor:'`+response[i]['classcolor']+`',filetag:'`+response[i]['filetag']+`'})">
                                    <span class="badge badge-null badge-roundless" style="display: none;">UNSPECIFIED</span>
                                    <img src="`+thumbnail+`" class="image_ori" style="height: 130px" onclick="CDFselectContentItem({elementid:'`+elementid+`',_noid:`+response[i]['noid']+`,urlfile:'`+thumbnail+`',nama:'`+response[i]['nama']+`',title:'`+response[i]['filename']+`',size:'`+response[i]['filesize']+`',type:'`+response[i]['fileextention']+`',modified:'`+response[i]['lastupdate']+`',created:'`+response[i]['docreate']+`',ispublic:'`+response[i]['ispublic']+`',classicon:'`+response[i]['classicon']+`',classcolor:'`+response[i]['classcolor']+`',filetag:'`+response[i]['filetag']+`'})">
                                </div>
                                <div class="fm_contenttitle" id="fm_contenttitle-`+response[i]['noid']+`" onclick="CDFselectContentItem({elementid:'`+elementid+`',_noid:`+response[i]['noid']+`,urlfile:'`+thumbnail+`',nama:'`+response[i]['nama']+`',title:'`+response[i]['filename']+`',size:'`+response[i]['filesize']+`',type:'`+response[i]['fileextention']+`',modified:'`+response[i]['lastupdate']+`',created:'`+response[i]['docreate']+`',ispublic:'`+response[i]['ispublic']+`',classicon:'`+response[i]['classicon']+`',classcolor:'`+response[i]['classcolor']+`',filetag:'`+response[i]['filetag']+`'})" style="text-align: center; height: 23px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis; color: #fff; bottom: 4px; position: absolute; width: calc(100% - 8px); padding: 4px; background: rgba(0, 0, 0, .7); display: block; font-size: 80%; -webkit-transition: all 0.5s ease;">
                                    `+response[i]['nama']+`
                                    <div class="fm_contenttitle_action-`+response[i]['noid']+`" id="fm_contenttitle_action-`+response[i]['noid']+`" style="position: absolute; bottom: 4px; left: -200%; right: 0; transition: all 0.5s ease 0s;">
                                        <a href="`+original+`" download target="_blank" class="btn btn-sm blue tooltips">
                                            <i class="fa fa-download text-light"></i>
                                        </a>
                                        <a type="button" class="btn btn-sm purple tooltips" data-container="body" data-placement="top" data-original-title="Preview" id="preview" onclick="CDFshowPreviewFile({elementid:'`+elementid+`',urlfile:'`+preview+`',title:'`+response[i]['filename']+`'})">
                                            <i class="fa fa-desktop text-light"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>`
                }
                $('#content_myfile-'+elementid+' .row .fm_content .row #panelcontent .row').html(html)
                // alert(response)
                // console.log('response')
                // console.log(response)

                $.each(cdf_checks, function(k, v) {
                    $('#fm_contentitems-'+params.elementid+'-'+v).css('box-shadow', '0px 0px 5px 5px #9a12b3');
                })
                
            }
        })
    }

    function CDFeditFile(params) {
        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            url: "/cdf_edit"+main.urlcode,
            dataType: "JSON",
            data:{
                "_token":$('#token').val(),
                "table": values3,
                "noid": params._noid,
                "filename": $('#attribute_nama-'+params.elementid).val(),
                "filestatus": $('#attribute_status-'+params.elementid).val(),
                "filetag": $('#attribute_tag-'+params.elementid).val(),
            },
            success: function(response) {
                alert(response.message)
                CDFgetFile({elementid:params.elementid,whereformat:0,wheresearch:$('#cdf-search').val()});
            }
        })
        // alert($('#attribute_tag-'+noid).val())
    }

    function CDFafterPickFiles() {
        let _files = $('[name="cdf-files[]"]')[0].files;

        $("#content_upload_files").hide();
        $("#content_upload_files2").show();

        let _data = '';
        $.each(_files, function(k,v) {
            let _no = k+1;
            let _tempname = v.name;
            let _name = _tempname.substr(0, _tempname.lastIndexOf('.'));
            let _extension = _tempname.substr(_tempname.lastIndexOf('.'));

            _data += `<div class="row">
                <div class="col-md-1">
                    `+_no+`
                </div>
                <div class="col-md-8">
                    <div class="input-group mb-3">
                        <input name="cdf-filenames[]" class="falignleft form-text form-control valid" value="`+_name+`" id="filename" type="text" placeholder="Filename">
                        <div class="input-group-append" style="padding-top: 3px">
                            <span class="input-group-text" id="basic-addon2">`+_extension+`</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <input name="cdf-filepublics[]" value="`+k+`" class="form-checkbox" id="ispublic" type="checkbox" placeholder="Public">
                </div>
            </div>`;
        })

        let _html = `<div class="col-md-12" style="padding:20px;">
            <div class="row">
                <div class="col-md-1">
                    <label class="control-label ">No.</label>
                </div>
                <div class="col-md-8">
                    <label class="control-label ">Filename</label>
                </div>
                <div class="col-md-3">
                    <label class="control-label ">Public</label>
                </div>
            </div>
            `+_data+`
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="btn btn-sm purple" id="cdf_upload" onclick="CDFuploadFile()" style="margin-top:30px;">
                            <i class="fa fa-reply text-light"></i> Upload
                        </div>
                        <div class="btn btn-sm red" id="cdf_clear" onclick="CDFclearFile()" style="margin-top:30px;">
                            <i class="fa fa-trash text-light"></i> Cancel
                        </div>
                    </div>
                </div>
            </div>
        </div>`;

        $("#content_upload_files2").html(_html);
    }

    function CDFselectContentItem(params) {
        if (cdf_istabinfo == '') {
            $('#cdf-btntabdetails').trigger('click');
            cdf_istabinfo = 'details';
        }

        $(".select2tag").select2({
            tags: true,
            tokenSeparators: [',', ' ']
        })
        $('#fm_meta_filename-'+params.elementid+' strong').text(params.title);
        $('#fm_meta_image-'+params.elementid+' img').attr('src', params.urlfile);
        $('#fm_meta_size-'+params.elementid).text(params.size != 'null' ? params.size+' KB' : '-');
        $('#fm_meta_type-'+params.elementid).text(params.type != 'null' ? params.type : '-');
        $('#fm_meta_modified-'+params.elementid).text(params.modified != 'null' ? params.modified : '-');
        $('#fm_meta_created-'+params.elementid).text(params.created != 'null' ? params.created : '-');
        $('.fm_meta_status').hide();
        if (params.ispublic == '1') {
            $('#fm_meta_status_public-'+params.elementid).show();
            $('#fm_meta_attributes_ispublic-'+params.elementid).text('PUBLIC');
            $('#fm_meta_attributes_ispublic-'+params.elementid).attr('class', 'btn btn-xs green tooltips');
            $('#attribute_status-'+params.elementid).val(1);
        } else {
            $('#fm_meta_status_private-'+params.elementid).show();
            $('#fm_meta_attributes_ispublic-'+params.elementid).text('PRIVATE');
            $('#fm_meta_attributes_ispublic-'+params.elementid).attr('class', 'btn btn-xs yellow tooltips');
            $('#attribute_status-'+params.elementid).val(0);
        }

        $('#fm_meta_details_download-'+params.elementid).attr('href', params.urlfile);
        $('#fm_meta_details_preview-'+params.elementid).attr('onclick', `CDFshowPreviewFile({elementid:'`+params.elementid+`',urlfile:'`+params.urlfile+`',title:'`+params.title+`'})`);
        // $('#fm_meta_details_choose-'+params.elementid).attr('onclick', `CDFchooseFile({elementid:'`+params.elementid+`',_noid:`+params._noid+`,urlfile:'`+params.urlfile+`',title:'`+params.title+`'})`);

        $('#fm_meta_attributes_fileextention-'+params.elementid).html(`<i class="`+params.classicon+`"></i> `+params.type);
        $('#fm_meta_attributes_fileextention-'+params.elementid).attr('class', 'btn btn-xs tooltips '+params.classcolor);
        $('#fm_meta_attributes_size-'+params.elementid).text(params.size != 'null' ? params.size+' KB' : '-');
        $('#fm_meta_attributes_created-'+params.elementid).text(params.created != 'null' ? params.created : '-');
        $('#fm_meta_attributes_updated-'+params.elementid).text(params.modified != 'null' ? params.modified : '-');

        $('#editfile-'+params.elementid).attr('onclick', `CDFeditFile({elementid:'`+params.elementid+`',_noid:`+params._noid+`})`);
        $('#attribute_nama-'+params.elementid).val(params.nama);
        
        if (params.filetag) {
            if (params.filetag.split(',').length > 0) {
                $('#attribute_tag-'+params.elementid).val(params.filetag.split(','));
            }
        }

        // $('.fm_contentitems').attr('style', 'height: 139px; overflow: hidden;  border: 1px solid grey;');
        // $('#fm_contentitems-'+params.elementid+'-'+params._noid).css('box-shadow', '0px 0px 5px 5px #9a12b3');
        $('#fm_meta-'+params.elementid).show();

        // alert($('#fm_contentitems-'+params.elementid+'-'+params._noid).attr('style'))
        // alert(cdf_checks.indexOf(params._noid))
        let idx = cdf_checks.indexOf(params._noid);
        if (idx > -1) {
            cdf_checks.splice(idx, 1)
            // cdf_selecteds.splice('cdf-'+params._noid, 1)
            delete cdf_selecteds['cdf-'+params];
            $('#fm_contentitems-'+params.elementid+'-'+params._noid).attr('style', 'height: 139px; overflow: hidden;  border: 1px solid grey;')
        } else {
            cdf_checks.push(params._noid)
            cdf_selecteds['cdf-'+params._noid] = {
                noid: params._noid,
                nama: params.nama,
                urlfile: params.urlfile,
            }
            $('#fm_contentitems-'+params.elementid+'-'+params._noid).css('box-shadow', '0px 0px 5px 5px #9a12b3');
        }
    }

    function CDFshowPreviewFile(params) {
        $('#modal-preview-file-'+params.elementid+' div div div div .fm-image-content img').attr('src', params.urlfile);
        $('#modal-preview-file-'+params.elementid+' div div div .modal-title').text(params.title);
        $('#modal-preview-file-'+params.elementid).modal('show');
    }

    function CDFmouseoverGridItem(id1, id2) {
        $(id1).css('height', '95%');
        $(id2).css('left', '0%');
    }

    function CDFmouseoutGridItem(id1, id2) {
        $(id1).css('height', '23px');
        $(id2).css('left', '-200%');
    }

    function CDFuploadFile() {
        $('#cdf_upload').addClass('disabled')
        $('#cdf_clear').addClass('disabled')
        var formdata = new FormData();
        var files = $('[name="cdf-files[]"')[0].files;
        var filenames = $('[name="cdf-filenames[]"]');
        var filepublics = [];

        // console.log(formdata)
        // return false;
        formdata.append('_token', $('#token').val());
        $.each(files, function(k,v){
            formdata.append('files[]', v)
            formdata.append('filenames[]', filenames[k].value)
            filepublics.push(0)
        });
        
        $('[name="cdf-filepublics[]"]:checked').each(function () { 
            filepublics[this.value] = 1;
        }); 

        $.each(filepublics, function(k,v){
            formdata.append('filepublics[]', v)
        })

        $.ajax({
            url: "/cdf_upload"+main.urlcode,
            type: "POST",
            method: "POST",
            data: formdata,
            processData: false,  // Important!
            contentType: false,
            cache: false,
            mimeType: 'multipart/form-data',
            header:{
                'X-CSRF-TOKEN': $('#token').val(),
            },
            success: function(response){
                var response = JSON.parse(response);

                if(response['success']){ 
                    alert(response['message'])
                    $.notify({
                        message: response['message']
                    },{
                        type: 'success'
                    });
                    $('#cdf_clear').trigger('click')
                } else {
                    alert(response['message'])
                    $.notify({
                        message: response['message']
                    },{
                        type: 'danger'
                    });
                }

                $('#cdf_upload').removeClass('disabled')
                $('#cdf_clear').removeClass('disabled')
            }, 
        });
    }

    function CDFclearFile() {
        $("#content_upload_files").show();
        $("#content_upload_files2").hide();
    }

    function CDFshow(params) {
        if (params.type == 'prev') {
            // if (cdf_show < 1) {
            //     // alert('Mentok.')
            // } else {
                cdf_show = cdf_show-1;
                let _cdf_show_title = (cdf_show+1)+'. '+cdf_selecteds['cdf-'+cdf_checks[cdf_show]].nama;
                $('#cdf-show-title').text(_cdf_show_title);
                $('.cdf-show').hide();
                $('#cdf-show-'+cdf_show).show();

                (cdf_show < 1)
                    ? $('#cdf_btn_show_prev').addClass('disabled')
                    : $('#cdf_btn_show_prev').removeClass('disabled');
                $('#cdf_btn_show_next').removeClass('disabled');
            // }
        } else if (params.type == 'next') {
            // if (parseInt(cdf_show) == parseInt(cdf_checks.length)-parseInt(1)) {
            //     // alert('Mentok.')
            // } else {
                cdf_show = cdf_show+1;
                // alert('cdf-'+cdf_details[cdf_show])
                let _cdf_show_title = (cdf_show+1)+'. '+cdf_selecteds['cdf-'+cdf_details[cdf_show]].nama;
                $('#cdf-show-title').text(_cdf_show_title);
                $('.cdf-show').hide();
                $('#cdf-show-'+cdf_show).show();

                $('#cdf_btn_show_prev').removeClass('disabled');
                (parseInt(cdf_show) == parseInt(cdf_details.length)-parseInt(1)) 
                    ? $('#cdf_btn_show_next').addClass('disabled') 
                    : $('#cdf_btn_show_next').removeClass('disabled');
            // }
        }

        $('.cdf-detail').hide();
        $('#cdf-detail-'+cdf_show).show();
        // alert(cdf_show)
    }

    function CDFunset() {
        cdf_istabinfo = ''; //details/attributes
        cdf_details = [];
        cdf_checks = [];
        cdf_selecteds = {};
        cdf_show = 0;
    }
    
    function CDFsave(params) {
        let html = ``;

        if (cdf_checks.length > 0) {
            if (params.notif) {
                if (cdf_checks.length == 1) {
                    alert(cdf_checks.length+' attachment saved successfully.');
                } else {
                    alert(cdf_checks.length+' attachments saved successfully.');
                }
            }

            $('#cdf-show-title').text('1. '+cdf_selecteds['cdf-'+cdf_checks[0]].nama);
            let _cdf_details = []
            $.each(cdf_checks, function(k,v) {
                let _show = k == 0 ? 'display:block;' : 'display:none;';
                
                html += `<img src="`+cdf_selecteds['cdf-'+v].urlfile+`" id="cdf-show-`+k+`" class="cdf-show" style="max-width:100%;max-height:200px;`+_show+`">`
                _cdf_details.push(v)
            })
            cdf_details = _cdf_details;

            $('#content_image-'+params.elementid+' div div').html(html)

            $('#cdf-btn-show').html(cdf_checks.length > 0 ? cdf_btn_show : '');
            // cdf_show = cdf_details[0];
        } else {
            if (params.notif) {
                alert('No attachments are saved!');
            }
            cdf_details = cdf_checks;
            // alert(!statusadd)
            // if (!statusadd) {
                html += `<img src="`+location.origin+`/filemanager/default/noimage.png" id="cdf-show-2" class="cdf-show" style="max-width:100%;max-height:200px;">`;
                $('#content_image-'+params.elementid+' div div').html(html)
                $('#cdf-show-title').text('File Manager');
                $('#cdf-btn-show').html('');
            // }
            // cdf_show = 0;
        }
        
        if (cdf_checks.length == 1) {
            $('#cdf_btn_show_prev').addClass('disabled');
            $('#cdf_btn_show_next').addClass('disabled');
        } else if (cdf_checks.length > 1) {
            $('#cdf_btn_show_prev').addClass('disabled');
            $('#cdf_btn_show_next').removeClass('disabled');
        }

        let _html_detail = ``;
        if (cdf_checks.length > 0) {
            $.each(cdf_checks, function(k,v) {
                let ishardcopy = {'true': '', 'false': 'checked'};
                let ispublic = {'true': '', 'false': 'checked'};
                let doexpired = `${new Date().getDate()}-${new Date().toLocaleString('default', { month: 'short' })}-${new Date().getFullYear()+1}`;
                let doreviewlast = `${new Date().getDate()}-${new Date().toLocaleString('default', { month: 'short' })}-${new Date().getFullYear()}`;
                let doreviewnext = `${new Date().getDate()}-${new Date().toLocaleString('default', { month: 'short' })}-${new Date().getFullYear()+1}`;

                if (cdf_selecteds['cdf-'+v].cdf_ishardcopy != undefined && cdf_selecteds['cdf-'+v].cdf_ishardcopy == 1) {
                    ishardcopy.true = 'checked';
                    ishardcopy.false = '';
                }

                if (cdf_selecteds['cdf-'+v].cdf_ispublic != undefined && cdf_selecteds['cdf-'+v].cdf_ispublic == 1) {
                    ispublic.true = 'checked';
                    ispublic.false = '';
                }

                _html_detail += `<div id="cdf-detail-`+k+`" class="cdf-detail" style="display: `+(k==0?'block':'none')+`">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Nama</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="cdf_detail_`+k+`_nama" value="`+(cdf_selecteds['cdf-'+v].cdf_nama?cdf_selecteds['cdf-'+v].cdf_nama:cdf_selecteds['cdf-'+v].nama)+`" class="form-control" placeholder="Nama">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Keterangan</label>
                                                <div class="col-md-9">
                                                    <textarea name="cdf_detail_`+k+`_keterangan" cols="3" rows="5" class="form-control" placeholder="Keterangan">`+(cdf_selecteds['cdf-'+v].cdf_keterangan?cdf_selecteds['cdf-'+v].cdf_keterangan:'')+`</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Is Hard Copy</label>
                                                <div class="col-md-9" style="padding-top: 8px">
                                                    <input type="radio" name="cdf_detail_`+k+`_ishardcopy" value="1" `+(ishardcopy.true)+`> True
                                                    <input type="radio" name="cdf_detail_`+k+`_ishardcopy" value="0" `+(ishardcopy.false)+`> False
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Is Public</label>
                                                <div class="col-md-9" style="padding-top: 8px">
                                                    <input type="radio" name="cdf_detail_`+k+`_ispublic" value="1" `+(ispublic.true)+`> True
                                                    <input type="radio" name="cdf_detail_`+k+`_ispublic" value="0" `+(ispublic.false)+`> False
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Do Expired</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="cdf_detail_`+k+`_doexpired" value="`+(doexpired)+`" class="form-control datepicker" placeholder="Do Expired" autocomplete="off">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Do Review Last</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="cdf_detail_`+k+`_doreviewlast" value="`+(doreviewlast)+`" class="form-control datepicker" placeholder="Do Review Last" autocomplete="off">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Do Review Next</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="cdf_detail_`+k+`_doreviewnext" value="`+(doreviewnext)+`" class="form-control datepicker" placeholder="Do Review Next" autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>`;
            })
        }
        
        $('#cdf-detail').html(_html_detail);
        $('.datepicker').datepicker({
            format: 'dd-M-yyyy',
            autoclose: true,
            // startDate: '-3d'
        });

        cdf_show = 0;
        $('#qty-attachment').text(cdf_checks.length)
        CDFhideModalDocumentViewer({elementid:params.elementid});
    }

    function customDocumentForm() {
        CDFunset();
        let params = new Array();
        params.push({
            elementid: 'flowchart',
            caption: 'Document',
        });

        $.each(params, function(k, v) {
            $('#'+v.elementid).html(`<div style="display:block;" class="col-md-12 pull-left">
                <div class="form-group">
                    <label class="control-label ">`+v.caption+`</label>
                    <input type="hidden" id="'.$v->fieldsource.'-`+v.elementid+`"/>
                    <div class="">
                        <div class="popup-image_preview" style="width: 100%; height: 100%; text-align: -webkit-center; border: 1px solid #f0f0f0; padding: 8px; cursor: pointer; position: relative; min-height: 40px; overflow: hidden">
                            <div id="fm-btn-action-`+v.elementid+`" class="fm-btn-action" style="overflow: hidden; display: block; position: absolute; top: 0px; left: 0px; right: 0px; padding: 6px 0px; background: rgba(154, 18, 179, .7); height: 38px">
                                <div class="btn btn-sm green btn_hide" id="iddmslogo" onclick="CDFshowModalDocumentViewer({elementid:'`+v.elementid+`',refresh:true})">
                                    <span id="icon">
                                        <i class="fa fa-plus text-light"></i>
                                    </span> 
                                    <span id="caption">File Manager</span>
                                </div>
                            </div>
                            <div id="content_image-`+v.elementid+`" class="fm-btn-content" style="height: 200px;">
                                <div class="modal-image-content" style="width: 100%; height: inherit; text-align: -webkit-center">
                                    <div class="fm-image-content" style="vertical-align: middle; display: table-cell; width: 100%; height: inherit; text-align: -webkit-center">
                                        <img src="`+location.origin+`/filemanager/default/noimage.png" id="cdf-show-2" class="cdf-show" style="max-width:100%;max-height:200px;">
                                    </div>
                                </div>
                            </div>
                            <div class="fm-btn-title" id="title_img-`+v.elementid+`" style="position: absolute; left: 0; right: 0; bottom: 0; color: #fff; padding: 10px; background: rgba(154, 18, 179, .7); overflow: hidden; white-space: nowrap; text-overflow: ellipsis; font-weight: bold; text-align: center">
                                <div class="row">
                                    <div class="col-md-9 text-left">
                                        <span id="cdf-show-title">File Manager</span>
                                    </div>
                                    <div class="col-md-3 text-right" id="cdf-btn-show">
                                        `+(cdf_checks.length > 0 ? cdf_btn_show : '')+`
                                    </div>
                                </div>
                            </div>
                        </div>
                        <span class="help-block"></span>
                    </div>
                </div>
                <div id="modal-document-viewer-`+v.elementid+`" class="modal fade" aria-hidden="true" style="display: none;">
                    <div class="modal-lg_ modal-dialog" style="max-width: 95%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <div class="modal-title">Document Viewer</div>
                            </div>
                            <div class="modal-body_" style="padding: 0 5px; margin-top: 5px !important">
                                <div class="col-md-12 fm_panel">
                                    <div class="fm_panel_body" style="overflow: hidden; border: 4px solid #03A9F4; margin: 0 -5px 0 -4px">
                                        <div class="col-md-12 fm_panel_toolbar" style="background: #fff; padding: 4px 0 0 0; display: inline-block; border-bottom: 4px solid #03A9F4">
                                            <div class="row">
                                                <div class="col-md-6 col-xs-12 fm_btnfilter" id="filter">
                                                    <a type="button" class="btn btn-sm default" onclick="CDFtoggleFmBtn({elementid:'`+v.elementid+`',type:'myfiles'})"><i class="fa fa-copy text-dark"></i> ALL</a>
                                                    <a type="button" class="btn btn-sm blue" onclick="CDFtoggleFmBtn({elementid:'`+v.elementid+`',type:'myfiles',where:'doc'})"><i class="fa fa-file text-light"></i> DOC</a>
                                                    <a type="button" class="btn btn-sm green" onclick="CDFtoggleFmBtn({elementid:'`+v.elementid+`',type:'myfiles',where:'img'})"><i class="fa fa-file-image text-light"></i> IMG</a>
                                                    <a type="button" class="btn btn-sm blue-hoki" onclick="CDFtoggleFmBtn({elementid:'`+v.elementid+`',type:'myfiles',where:'av'})"><i class="fa fa-file-audio text-light"></i> AV</a>
                                                    <a type="button" class="btn btn-sm yellow" onclick="CDFtoggleFmBtn({elementid:'`+v.elementid+`',type:'myfiles',where:'zip'})"><i class="fa fa-file-archive text-light"></i> ZIP</a>
                                                    <a type="button" class="btn btn-sm red" onclick="CDFtoggleFmBtn({elementid:'`+v.elementid+`',type:'myfiles',where:'bin'})"><i class="fa fa-file-archive text-light"></i> BIN</a>
                                                    <a type="button" class="btn btn-sm purple" onclick="CDFtoggleFmBtn({elementid:'`+v.elementid+`',type:'myfiles',where:'other'})"><i class="fa fa-file text-light"></i> OTHER</a>
                                                </div>
                                                <div class="col-md-3 col-xs-6 fm_btnlibrary" style="text-align: right;">
                                                    <a type="button" class="btn btn-sm purple tooltips" data-container="body" data-placement="bottom" data-original-title="Upload" id="cdf-upload-`+v.elementid+`" onclick="CDFtoggleFmBtn({elementid:'`+v.elementid+`',type:'upload'})">
                                                        <i class="fa fa-cloud text-light"></i> <span style="display: inline-block">Upload</span>
                                                    </a type="button">
                                                    <a type="button" class="btn btn-sm green tooltips" data-container="body" data-placement="bottom" data-original-title="My Files" id="cdf-myfiles-`+v.elementid+`" onclick="CDFtoggleFmBtn({elementid:'`+v.elementid+`',type:'myfiles'})">
                                                        <i class="fa fa-user text-light"></i> <span style="display: none">My Files</span>
                                                    </a type="button">
                                                    <a type="button" class="btn btn-sm blue tooltips" data-container="body" data-placement="bottom" data-original-title="Save" id="cdf-save-`+v.elementid+`" onclick="CDFsave({elementid:'`+v.elementid+`',notif:true})">
                                                        <i class="fa fa-save text-light"></i> <span style="display: none">Save</span>
                                                    </a type="button">
                                                    <a type="button" class="btn btn-sm blue tooltips" data-container="body" data-placement="bottom" data-original-title="Tile View" style="display: none;">
                                                        <i class="fa fa-th text-light"></i>
                                                    </a type="button">
                                                    <a type="button" class="btn btn-sm grey tooltips" data-container="body" data-placement="bottom" data-original-title="List View" style="display: none;">
                                                        <i class="fa fa-th-list text-dark"></i>
                                                    </a type="button">
                                                </div>
                                                <div class="col-md-3 col-xs-6 fm_search">
                                                    <div class="input-icon input-icon-sm right">
                                                        <i class="fa fa-search"></i>
                                                        <input type="text" id="cdf-search" class="form-control input-sm" placeholder="Search..." style="height: 27px; margin-bottom: 4px">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 fm_panel_container" id="container" style="padding: 0px">
                                            <div class="fm_panelcontent" id="content_upload-`+v.elementid+`" style="display: block;">
                                                <div class="col-md-12">
                                                    <div id="content_upload_files" style="height: 450px;">
                                                        <br><br>
                                                        <center style="padding:0px;">
                                                            <div>
                                                                <div class="btn btn-sm green fileinput-button" style="position: relative; overflow: hidden">
                                                                    <i class="fa fa-plus text-light"></i>
                                                                    <span> Upload</span>
                                                                    <form method="post" enctype="multipart/form-data">
                                                                        <input id="files" type="file" name="cdf-files[]" multiple="" style="position: absolute; top: 0; right: 0; margin: 0; opacity: 0; font-size: 200px; direction: ltr; cursor: pointer" onchange="CDFafterPickFiles()">
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </center>
                                                    </div>
                                                    <div id="content_upload_files2" style="height: 450px; display: none">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="fm_panelcontent" id="content_myfile-`+v.elementid+`" style="display: none;">
                                                <div class="row">
                                                    <div class="col-md-9 col-xs-7 fm_content" style="padding: 4px">
                                                        <div class="row">
                                                            <div class="col-md-12 fm_content_list modal-body" id="panelcontent" style="padding: 0px; margin: 0px !important; height: 480px">
                                                                <div class="row">
                                                                    <div class="col-md-2 col-xs-4 fm_content_grid_items" id="fm_content_grid_item" style="margin-bottom: 4px; padding: 4px; text-align: -webkit-center; height: 100%" onmouseover="CDFmouseoverGridItem(\'fm_contenttitle\', \'fm_contenttitle_action\')" onmouseout="CDFmouseoutGridItem(\'fm_contenttitle\', \'fm_contenttitle_action\')">
                                                                        <div class="fm_contentitems" id="fm_contentitems-`+v.elementid+`" style="height: 139px; overflow: hidden;  border: 1px solid grey;" onclick="CDFselectContentItem({elementid:'`+v.elementid+`'})">
                                                                            <span class="badge badge-null badge-roundless" style="display: none;">UNSPECIFIED</span>
                                                                            <img src="http://dashboard.dev.lambada.id/data_lycon/filemanager/thumb/cmspage-pagecaption.jpg" class="image_ori">
                                                                        </div>
                                                                        <div class="fm_contenttitle" id="fm_contenttitle" onclick="CDFselectContentItem({elementid:'`+v.elementid+`'})" style="text-align: center; height: 23px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis; color: #fff; bottom: 4px; position: absolute; width: calc(100% - 8px); padding: 4px; background: rgba(0, 0, 0, .7); display: block; font-size: 80%; -webkit-transition: all 0.5s ease;">
                                                                            cmspage-pagecaption
                                                                            <div class="fm_contenttitle_action" id="fm_contenttitle_action" style="position: absolute; bottom: 4px; left: -200%; right: 0; transition: all 0.5s ease 0s;">
                                                                                <a type="button" class="btn btn-sm purple tooltips" data-container="body" data-placement="top" data-original-title="Preview" id="preview" onclick="CDFshowPreviewFile({elementid:'`+v.elementid+`',urlfile:'http://dashboard.dev.lambada.id/data_lycon/filemanager/thumb/cmspage-pagecaption.jpg',title:'cmspage-pagecaption.jpg'})">
                                                                                    <i class="fa fa-desktop text-light"></i>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-xs-5 fm_meta" id="fm_meta-`+v.elementid+`" style="display: none">
                                                        <ul class="nav">
                                                            <li class="nav-item active">
                                                                <a class="nav-link" href="#tab-details" id="cdf-btntabdetails" data-toggle="tab" aria-expanded="true">Details</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" href="#tab-attributes" id="cdf-btntabattributes" data-toggle="tab" aria-expanded="false">Attributes</a>
                                                            </li>
                                                            <li class="nav-item" style="display:none;">
                                                                <a class="nav-link" href="#tab-comments" id="cdf-btntabcomments" data-toggle="tab" aria-expanded="false">Comments</a>
                                                            </li>
                                                        </ul>
                                                        <div class="tabbable-custom tabs-below nav-justified modal-body" style="margin: 0px !important; height: 450px">
                                                            <div class="tab-content">
                                                                <div class="tab-pane" id="tab-details">
                                                                    <div>
                                                                        <div class="col-md-12 fm_meta_tag_function" style="text-align: center; padding: 4px; background: #000; width: 100%">
                                                                            <a href="http://dashboard.dev.lambada.id/data_lycon/filemanager/original/Screen Shot 2021-02-24 at 09.53.03.png" id="fm_meta_details_download-`+v.elementid+`" download target="_blank" class="btn btn-sm blue tooltips">
                                                                                <i class="fa fa-download text-light"></i>
                                                                            </a>
                                                                            <span class="btn btn-sm purple tooltips" data-container="body" data-placement="top" data-original-title="Preview" id="fm_meta_details_preview-`+v.elementid+`" onclick="CDFshowPreviewFile({elementid:'`+v.elementid+`',urlfile:'http://dashboard.dev.lambada.id/data_lycon/filemanager/thumb/cmspage-pagecaption.jpg',title:'cmspage-pagecaption.jpg'})">
                                                                                <i class="fa fa-desktop text-light"></i>
                                                                            </span>
                                                                        </div>
                                                                        <div class="col-md-12 fm_meta_filename" id="fm_meta_filename-`+v.elementid+`" style="padding: 8px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis; background: rgba(0, 0, 0, .7); width: 100%; color: #fff">
                                                                            <span class="btn btn-xs green">
                                                                                <i class="fa fa-file-image text-light"></i>
                                                                            </span>
                                                                            <strong>
                                                                                Screen Shot 2021-02-24 at 09.53.03.png
                                                                            </strong>
                                                                        </div>
                                                                        <div class="col-md-12" style="text-align: -webkit-center;padding: 8px;">
                                                                            <div class="fm_meta_image" id="fm_meta_image-`+v.elementid+`">
                                                                                <img src="http://dashboard.dev.lambada.id/data_lycon/filemanager/thumb/cmspage-pagecaption.jpg" style="height: 200px">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                            <div class="row">
                                                                                <div class="col-md-5 col-xs-5">Size:</div>
                                                                                <div class="col-md-7 col-xs-7" id="fm_meta_size-`+v.elementid+`">297 KB</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                            <div class="row">
                                                                                <div class="col-md-5 col-xs-5">Type:</div>
                                                                                <div class="col-md-7 col-xs-7" id="fm_meta_type-`+v.elementid+`">IMG Document</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                            <div class="row">
                                                                                <div class="col-md-5 col-xs-5">Date Modified:</div>
                                                                                <div class="col-md-7 col-xs-7" id="fm_meta_modified-`+v.elementid+`">-</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                            <div class="row">
                                                                                <div class="col-md-5 col-xs-5">Date Created:</div>
                                                                                <div class="col-md-7 col-xs-7" id="fm_meta_created-`+v.elementid+`">2021-03-02 01:42:34</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                            <div class="row">
                                                                                <div class="col-md-5 col-xs-5">Status:</div>
                                                                                <div class="col-md-7 col-xs-7">
                                                                                    <span class="badge badge-null badge-roundless fm_meta_status" id="fm_meta_status_unspecified-`+v.elementid+`" style="display: none;">UNSPECIFIED</span> 
                                                                                    <span class="badge badge-success badge-roundless fm_meta_status" id="fm_meta_status_public-`+v.elementid+`">PUBLIC</span>
                                                                                    <span class="badge badge-warning badge-roundless fm_meta_status text-light" id="fm_meta_status_private-`+v.elementid+`" style="display: none;">PRIVATE</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 fm_meta_tag_tags_list">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="tab-pane" id="tab-attributes" style="position: relative; zoom: 1;">
                                                                    <div style="display: inline-block_;">
                                                                        <div class="col-md-12 col-xs-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                            <div class="row">
                                                                                <div class="col-md-12 col-xs-12">
                                                                                    <span class="btn btn-xs green tooltips" id="fm_meta_attributes_ispublic-`+v.elementid+`" data-container="body" data-placement="top" data-original-title="Status" style="font-size: 10px; font-weight: bold; padding: 3px">
                                                                                        PUBLIC
                                                                                    </span> 
                                                                                    <span class="btn btn-xs green tooltips" id="fm_meta_attributes_fileextention-`+v.elementid+`" data-container="body" data-placement="top" data-original-title="File Type" style="font-size: 10px; font-weight: bold; padding: 3px">
                                                                                        <i class="fa fa-file-image text-light"></i> IMG
                                                                                    </span> 
                                                                                    <span class="btn btn-xs grey tooltips" id="fm_meta_attributes_size-`+v.elementid+`" data-container="body" data-placement="top" data-original-title="File Size" style="font-size: 10px; font-weight: bold; padding: 3px">
                                                                                        297 KB
                                                                                    </span> 
                                                                                    <span class="btn btn-xs blue pull-right tooltips" id="editfile-`+v.elementid+`" data-container="body" data-placement="top" data-original-title="Save Document" style="font-size: 10px; font-weight: bold; padding: 3px">
                                                                                        <i class="fa fa-save"></i> Save
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 col-xs-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                            <div class="row">
                                                                                <div class="col-md-12 col-xs-12">
                                                                                    <span class="btn btn-xs green tooltips" id="fm_meta_attributes_created-`+v.elementid+`" data-container="body" data-placement="top" data-original-title="Created By" style="width:100%;text-align:left; font-size: 10px; font-weight: bold; padding: 3px">
                                                                                        2021-03-02 01:42:34 (GUEST)
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 col-xs-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                            <div class="row">
                                                                                <div class="col-md-12 col-xs-12">
                                                                                    <span class="btn btn-xs yellow tooltips" id="fm_meta_attributes_updated-`+v.elementid+`" data-container="body" data-placement="top" data-original-title="Updated by" style="width:100%;text-align:left; font-size: 10px; font-weight: bold; padding: 3px">- (GUEST)</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 col-xs-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                            <div class="row">
                                                                                <div class="col-md-12 col-xs-12">Name:</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 col-xs-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                            <div class="row">
                                                                                <div class="col-md-12 col-xs-12">
                                                                                    <input id="attribute_nama-`+v.elementid+`" type="text" style="width:100%;">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 col-xs-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                            <div class="row">
                                                                                <div class="col-md-12 col-xs-12">Status:</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 col-xs-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                            <div class="row">
                                                                                <div class="col-md-12 col-xs-12">
                                                                                    <select id="attribute_status-`+v.elementid+`">
                                                                                        <option value="0">Private</option>
                                                                                        <option value="1">Public</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 col-xs-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                            <div class="row">
                                                                                <div class="col-md-12 col-xs-12">Tags:</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 col-xs-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                            <div class="row">
                                                                                <div class="col-md-12 col-xs-12" style="display:table;">
                                                                                    <select multiple class="form-control form-control-solid select2tag" id="attribute_tag-`+v.elementid+`" style="width: 100%">
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="tab-pane" id="tab-comments">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer" style="padding: 5px 0 0 0 !important">
                                <a href="javascript:;" title="Close" class="fancybox-item fancybox-close" data-dismiss="modal" aria-hidden="true"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal" id="modal-preview-file-`+v.elementid+`" data-backdrop="static" aria-hidden="false" style="display: none; padding-left: 0px;">
                    <div class="modal-lg modal-dialog" style="width:90%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <div class="modal-title">
                                    Anjas_CV.jpg
                                </div>
                            </div>
                            <div class="modal-body" style="margin-top: 5px !important">
                                <div class="modal-image-content" style="width: 100%; height: inherit; text-align: -webkit-center">
                                    <div class="fm-image-content" style="vertical-align: middle; display: table-cell; width: 100%; height: inherit;">
                                        <img src="http://dashboard.dev.lambada.id/data_lycon/filemanager/preview/cmspage-pagecaption.jpg" data-testing="testing" style="max-width:100%;max-height:100%;">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                            </div>
                            <a href="javascript:;" title="Close" class="fancybox-item fancybox-close" data-dismiss="modal" aria-hidden="true"></a>
                        </div>
                    </div>
                </div>
            </div>`);

            $('#cdf-search').val('')

            $('#cdf-search').on('keydown', function(e) {
                if (e.keyCode == 13) {
                    CDFgetFile({elementid:v.elementid,whereformat:0,wheresearch:$('#cdf-search').val()});
                }
            })

            $("#modal-document-viewer-"+v.elementid).on('hidden.bs.modal', function () {
                let _cdf_details = [];
                $.each(cdf_details, function(k,v){
                    _cdf_details.push(v);
                })
                cdf_checks = _cdf_details;
            });
        });

        $('#qty-attachment').text(cdf_checks.length)
    }

    function getCmsFilterButton(noid) {
        var buttonfilter = new Array();

        if (noid) {
            var each = JSON.parse(maincms_widget[noid]['configwidget'])['WidgetFilter'];
            var configwidget = JSON.parse(maincms_widget[noid]['configwidget']);
            var vnoid = noid;
        } else {
            var each = JSON.parse(maincms['widget'][0]['configwidget'])['WidgetFilter'];
            var configwidget = JSON.parse(maincms['widget'][0]['configwidget']);
            var vnoid = maincms['widget'][0]['noid'];
        }
        
        $.each(each, function(k, v) {
            // buttonfilter[k] = new Array();
            buttonfilter[k] = {
                'groupcaption' : v.groupcaption,
                'groupcaptionshow' : v.groupcaptionshow,
                'groupdropdown' : v.groupdropdown,
                'data' : new Array()
            }
            
            $.each(v.button, function(k2, v2) {
                var icon = '';
                if (v2.classicon != '') {
                    icon = '<i class="fa '+v2.classicon+'"></i> ';
                }
                var pecah = v2.condition[0].querywhere[0].fieldname.split('.');
                var buttoncaption = v2.buttoncaption.split(' ');
                var uniqeidbutton = '';
                if (buttoncaption.length > 1) {
                    $.each(buttoncaption, function(k3, v3) {
                        uniqeidbutton += v3
                    })
                } else {
                    uniqeidbutton = v2.buttoncaption
                }
                var oncl = 'onclick="filterData(\'btn-filter-'+uniqeidbutton+'-'+vnoid+'\', \'btn-filter-'+k+'-'+vnoid+'\', '+k+', \''+v2.buttoncaption+'\', \''+v2.classcolor+'\', '+v.groupdropdown+', false, \''+maincms['widget'][0]['maintable']+'\', \''+v2.condition[0].querywhere[0].operand+'\', \''+pecah[1]+'\', \''+v2.condition[0].querywhere[0].operator+'\', \''+v2.condition[0].querywhere[0].value+'\')" ';
                var button = '<button class="btn btn-outline-secondary btn-sm flat btn-filter-'+k+'-'+vnoid+'" id="btn-filter-'+uniqeidbutton+'-'+vnoid+'" style="background-color:#E3F2FD; color: '+v2.classcolor+'"'+oncl+'>'+icon+v2.buttoncaption+' </button>';
                if (v.groupdropdown == 1) {
                    button = '<button class="flat dropdown-item" id="btn-filter-'+uniqeidbutton+'-'+vnoid+'" '+oncl+'>'+icon+v2.buttoncaption+' </button>';
                }
                buttonfilter[k].data[k2] = {
                    'nama' : v2.buttoncaption,
                    'buttonactive' : v2.buttonactive,
                    'taga' : button,
                    'operand' : v2.condition[0].querywhere[0].operand,
                    'fieldname' : pecah[1],
                    'operator' : v2.condition[0].querywhere[0].operator,
                    'value' : v2.condition[0].querywhere[0].value,
                };
            })
        })

        
        var json_data = {
            "filterButton" : buttonfilter,
            "configWidget" : configwidget,
        };
        
        // console.log('test')
        $.each(json_data.filterButton, function(k, v) {
            $.each(v.data, function(k2, v2) {
                if (v2 != undefined) {
                    // console.log(v2.buttonactive)  
                }
            })
        })

        cmsfilterbutton = json_data;
        // return json_data;
    }

    function getFilterButton(values3, noid) {
        // $.ajax({
        //     type: "POST",
        //     header:{
        //         'X-CSRF-TOKEN':$('#token').val()
        //     },
        //     url: "/get_filter_button_tmd",
        //     dataType: "json",
        //     data:{
        //         "_token":$('#token').val(),
        //         table: maincms['menu']['noid'],
        //         maincms: JSON.stringify(maincms)
        //     },
        //     success: function(response) {
                getCmsFilterButton(noid)
                console.log('cmsfilterbutton')
                console.log(cmsfilterbutton)
                var response = cmsfilterbutton;

                if (noid) {
                    var vnoid = noid;
                } else {
                    var vnoid = maincms['widget'][0]['noid'];
                }
                // if (response == response2) {
                //     console.log('COCOKKKKK')
                // }
                // console.log(response)
                // console.log(response2)
                // return false

                var buttonactive = new Array();
                var filter_button = '';
                var groupdropdown = new Array();
                $.each(response.filterButton, function(k, v) {
                    // console.log(v.groupdropdown)
                    buttonactive.push('all-'+k+'-'+vnoid);
                    if (v.groupdropdown == 1) {
                        // alert(1)
                        filter_button += `<button class="btn btn-outline-secondary btn-sm flat btn-filter-all btn-filter-`+k+`-`+vnoid+`" id="btn-filter-all-`+k+`-`+vnoid+`" style="background-color:#90CAF9; font-weight: bold; color: black;">`+v.groupcaption+`</button>`;
                        filter_button += `<div class="dropdown btn-group" style="padding-right: 5px">`
                    } else {
                        // alert(0)
                        filter_button += `<button class="btn btn-outline-secondary btn-sm flat btn-filter-all btn-filter-`+k+`-`+vnoid+`" id="btn-filter-all-`+k+`-`+vnoid+`" style="background-color:#E3F2FD;" onclick="filterData('btn-filter-all-`+k+`-`+vnoid+`', 'btn-filter-`+k+`-`+vnoid+`', `+k+`, 'ALL', 'grey', 0, true)">ALL</button>`;
                    }
                    selected_filter.operand.push('')
                    selected_filter.fieldname.push('')
                    selected_filter.operator.push('')
                    selected_filter.value.push('')

                    $.each(v.data, function(k2, v2) {
                        if (v2 != undefined) {
                            if (v.groupdropdown == 1) {
                                if (k2 == 0) {
                                    if (v2.buttonactive) {
                                        buttonactive[k] = v2.nama;
                                        selected_filter.operand[k] = v2.operand;
                                        selected_filter.fieldname[k] = v2.fieldname;
                                        selected_filter.operator[k] = v2.operator;
                                        selected_filter.value[k] = v2.value;
                                        filter_button += `<button class="btn btn-secondary btn-sm flat dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <span id="filter-dropdown-active-`+k+`-`+vnoid+`">`+v2.nama+`</span>
                                                        </button>`;
                                    } else {
                                        filter_button += `<button class="btn btn-secondary btn-sm flat dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <span id="filter-dropdown-active-`+k+`-`+vnoid+`">ALL</span>
                                                        </button>`;
                                    }
                                    filter_button += `<div class="dropdown-menu flat" aria-labelledby="dropdownMenuButton">
                                                        <button class="dropdown-item btn-filter-all btn-filter-`+k+`-`+vnoid+`" id="btn-filter-all-`+k+`-`+vnoid+`"  onclick="filterData('btn-filter-all-`+k+`-`+vnoid+`', 'btn-filter-`+k+`-`+vnoid+`', `+k+`, 'ALL', 'grey', 1, true)">ALL</button>`;
                                }
                                groupdropdown.push(true);
                            } else {
                                if (v2.buttonactive) {
                                    buttonactive[k] = v2.nama;
                                    selected_filter.operand[k] = v2.operand;
                                    selected_filter.fieldname[k] = v2.fieldname;
                                    selected_filter.operator[k] = v2.operator;
                                    selected_filter.value[k] = v2.value;
                                }
                                groupdropdown.push(false);
                            }
                            filter_button += v2.taga;
                        }
                    })
                    
                    if (v.groupdropdown == 1) {
                        filter_button += `</div>
                                        </div>`;
                    }
                    filter_button += `<div class="btn" style="padding: 0px; width: 5px"></div>`
                })
                
                if (filter_button.length > 0) {
                    if (noid) {
                        $('#space_filter_button-'+noid).html(filter_button)
                    } else {
                        $('#space_filter_button').html(filter_button)
                    }
                    // $('#space_filter_button').css('margin-bottom', '20px')
                }
                
                console.log('buttonactive')
                console.log(buttonactive)
                console.log(groupdropdown)
                $.each(buttonactive, function(k, v) {
                    if (groupdropdown[k] == false) {
                        // alert('#btn-filter-'+buttonactive[k]+'-'+vnoid)
                        $('#btn-filter-'+buttonactive[k]).css('background', '#90CAF9');
                        $('#btn-filter-'+buttonactive[k]).css('color', 'black');
                        $('#btn-filter-'+buttonactive[k]).css('font-weight', 'bold');
                    }
                    // console.log(groupdropdown[k])
                    // console.log(buttonactive[k])
                })

                if (buttonactive.length > 0) {
                    destroyDatatable()
                    getDatatable()
                }
                
                
                //Datatable filter
                $.each(response.configWidget.WidgetFilter, function(k, v) {
                    var filtercontentgroup = '';
                    var filtercontentbody = '';

                    var classcolor = 'default';
                    var textcolor = 'text-dark';
                    var display = 'none';
                    if (k == 0) {
                        classcolor = 'purple';
                        textcolor = 'text-white'
                        display = 'block';
                        filter_tabs.push(true)
                    } else {
                        filter_tabs.push(false)
                    }
                    filtercontentgroup += `<input type="hidden" name="tabs[]" value="`+k+`">
                                            <div style="margin:5px 5px 0 0;" id="filter-content-group-`+k+`" onclick="changeTabFilter(`+k+`)" class="filter-content-group formbtn btn default `+classcolor+`">
                                                <span class="`+textcolor+`">
                                                    <i class="fa fa-indent `+textcolor+`"></i> `+k+`
                                                </span>
                                            </div>`;
                    filtercontenttabs.filtercontentgroup.push(filtercontentgroup)

                    var groupcaptionshow = `<option value="1">True</option>
                                            <option value="0" selected>False</option>`;
                    if (v.groupcaptionshow) {
                        groupcaptionshow = `<option value="1" selected>True</option>
                                            <option value="0">False</option>`
                    }

                    var groupdropdown = '';
                    if (v.groupdropdown == -1) {
                        groupdropdown = `<option value="-1" selected>Auto</option>
                                            <option value="0">False</option>
                                            <option value="1">True</option>`
                    } else if (v.groupdropdown == 0) {
                        groupdropdown = `<option value="-1">Auto</option>
                                            <option value="0" selected>False</option>
                                            <option value="1">True</option>`
                    } else if (v.groupdropdown == 1) {
                        groupdropdown = `<option value="-1">Auto</option>
                                            <option value="0">False</option>
                                            <option value="1" selected>True</option>`
                    }
                    filtercontentbody += `<div style="display: `+display+`" id="filter-content-body-`+k+`" class="filter-content-body formgroup col-md-12">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Group Caption</label>
                                                        <input name="groupcaption[]" id="groupcaption-`+k+`" class="form-control input-sm" value="`+v.groupcaption+`" placeholder="Group Caption">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Group Caption Showed</label>
                                                        <select name="groupcaptionshow[]" id="groupcaptionshow-`+k+`" class="form-control input-sm">
                                                            `+groupcaptionshow+`
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Group Dropdown Showed</label>
                                                        <select name="groupdropdown[]" id="groupdropdown-`+k+`" class="form-control input-sm">
                                                            `+groupdropdown+`
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="filter-content-row-`+k+`">
                                            </div>
                                            </div>`
                    filtercontenttabs.filtercontentbody.push(filtercontentbody)

                    filtercontenttabs.filtercontentrow[k] = new Array()
                    var _k2 = 0;
                    $.each(v.button, function(k2, v2) {
                        var filtercontentrow = '';

                        var condition = '';
                        $.each(v2.condition, function(k3, v3) {
                            condition += v3.groupoperand+'#'
                            $.each(v3.querywhere, function(k4, v4) {
                                condition += v4.fieldname+';'
                                condition += v4.operator+';'
                                condition += v4.value+';'
                                condition += v4.operand
                            })
                        })

                        var buttonactive = `<option value="1">True</option>
                                            <option value="0" selected>False</option>`
                        if (v2.buttonactive) {
                            buttonactive += `<option value="1" selected>True</option>
                                                <option value="0">False</option>`
                        }

                        filtercontentrow += `<input type="hidden" name="tabsrow[]" value="`+k+`">
                                            <div class="row filter-content-row-`+k+`">
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Caption</label>
                                                        <input class="form-control input-sm" name="buttoncaption[]" id="buttoncaption-`+k+`-`+_k2+`" value="`+v2.buttoncaption+`" placeholder="Caption">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Condition</label>
                                                        <input class="form-control input-sm" name="condition[]" id="condition-`+k+`-`+_k2+`" value="`+condition+`" placeholder="groupoperand#fieldname;operator;value;operand">
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Active</label>
                                                        <select class="form-control input-sm" name="buttonactive[]" id="buttonactive-`+k+`-`+_k2+`" value="1">
                                                            `+buttonactive+`
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Icon</label>
                                                        <input class="form-control input-sm" name="classicon[]" id="classicon-`+k+`-`+_k2+`" value="`+v2.classicon+`" placeholder="Icon">
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Color</label>
                                                        <input class="form-control input-sm" name="classcolor[]" id="classcolor-`+k+`-`+_k2+`" value="`+v2.classcolor+`" placeholder="Color">
                                                    </div>
                                                </div>
                                                <div class="col-md-1" style="text-align:center;padding:21px 0 0 0">
                                                    <div class="form-group">
                                                        <button id="del" onclick="delRow(`+_k2+`)" type="button" class="btn btn-sm red" style="height: 27px; width: 38px">
                                                            <i class="fa fa-minus-circle text-white"></i>
                                                        </button>
                                                        <button id="add" onclick="addRow()" type="button" class="btn btn-sm green" style="height: 27px; width: 38px">
                                                            <i class="fa fa-plus text-white"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>`
                        filtercontenttabs.filtercontentrow[k].push(filtercontentrow)
                        _k2++;
                    })
                    // filtercontentbody += `</div>
                    //                     </div>`
                })

                //Set to HTML
                var html_filtercontentgroup = '';
                var html_filtercontentbody = '';
                $.each(filtercontenttabs.filtercontentgroup, function(k, v) {
                    html_filtercontentgroup += v
                    html_filtercontentbody += filtercontenttabs.filtercontentbody[k]
                })
                $('#filtercontentgroup').html(html_filtercontentgroup);
                $('#filtercontentbody').html(html_filtercontentbody);

                $.each(filtercontenttabs.filtercontentrow, function(k, v) {
                    $('#filter-content-row-'+k).html(v);
                })
                //End Set to HTML

                // console.log(html_filtercontentrow)
                // console.log(filtercontenttabs)
            // },
        // });
    }

    // function addCommas(nStr) {
    //     nStr += '';
    //     var x = nStr.split('.');
    //     var x1 = x[0];
    //     var x2 = x.length > 1 ? '.' + x[1] : '';
    //     var rgx = /(\d+)(\d{3})/;
    //     while (rgx.test(x1)) {
    //         x1 = x1.replace(rgx, '$1' + ',' + '$2');
    //     }
    //     return x1 + x2;
    // }

    function getCmsTabColumns() {
        $.each(maincms['widget'], function(k, v) {
            if (k > 0) {
                var theadcolumns = `<th><input type="checkbox" id="select_all" onchange="selectAllCheckboxTab()"></th>
                                <th>No</th>`;
                var tfootcolumns = `<th></th>
                                    <th></th>`
                // console.log(maincms['widgetgridfield'])
                $.each(maincms['widgetgridfield'][k], function(k2, v2) {
                    if (v2.colshow == 1) {
                        theadcolumns += `<th class="text-`+v2.colheaderalign+`" style="width: `+v2.colwidth+`px; height: `+v2.colheight+`px;">`+v2.fieldcaption+`</th>`
                        tfootcolumns += `<th></th>`
                    }
                })
                theadcolumns += `<th>Action</th>`
                tfootcolumns += `<th></th>`
                $('#tab-thead-columns-'+v.maintable).html(theadcolumns)
                $('#tab-tfoot-columns'+v.maintable).html(tfootcolumns)
            }
        })
    }

    function getForm(isadd, widget_noid, element_id, responsefind=null) {
        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            url: "/get_form_tmd",
            dataType: "json",
            data:{
                "_token":$('#token').val(),
                table: maincms['menu']['noid'],
                maincms: JSON.stringify(maincms),
                isadd: isadd,
                key: 0,
                widget_noid: widget_noid,
            },
            beforeSend: function () {
                $('#'+element_id).LoadingOverlay("show");
            },
            success: function (response) {
                // console.log(response);
                var divform = ''
                divform += '<input type="hidden" id="noid" name="noid" value="">';
                // var colgroupname = [
                //     0: 'Administrasi'
                // ];
                // var colgroupname_data = [
                //     0: [
                //         //,
                //         //,
                //         //
                //     ]
                // ];
                // var colgroupname = new Array();
                // var colgroupname_data = new Array();
                $.each(response.field, function(key, value) {
                    // roles.fieldname.push(value.fieldname);
                    // roles.newhidden.push(value.newhidden);
                    // roles.edithidden.push(value.edithidden);

                    // if (value.colgroupname) {
                    //     if (colgroupname.indexOf(value.colgroupname) === -1) {
                    //         colgroupname.push(value.colgroupname);
                    //     }
                    //     colgroupname_data.push(value.field);
                    // } else {
                    //     // alert(key)
                        if (value.fieldname != 'idmaster') {
                            divform += value.field;
                        }
                    // }

                    // if (value.colsorted == 0) {
                    //     colsorted.push(key+1);
                    // }
                    
                });
                // colsorted[colsorted.length] = roles.fieldname.length+2;
                // colsorted.push(roles.fieldname.length+2);
                
                var divgroup = '';
                $.each(response.formgroupname, function(k, v) {
                    var formgroupname = k.split('-')

                    divgroup += `<div class="col-md-12">
                                    <div class="sub-card-custom">
                                        <div class="card-header card-header_ bg-main-1 d-flex">
                                            <div class="text-white" style="font-size: 14px">`+formgroupname[1].split(';')[1]+`</div>
                                        </div>
                                        <div class="card-body card-body-custom card-body-custom-group flat">
                                            <div class="right-tabs-group">`;
                                           
                    var key2 = 0;
                    var tab_widget = `<ul class="nav nav-tabs" id="tab-widget-`+k+`" role="tablist">`;
                    var tab_widget_content = `<div class="tab-content" id="tab-widget-content-`+k+`">`;
                    $.each(v, function(k2, v2) {
                        var active = '';
                        if (key2 == 0) {
                            active = 'active';
                        }

                        tab_widget += `<li class="nav-item">
                                        <a class="nav-link nav-link-custom flat `+active+`" id="tab-`+k2+`" data-toggle="tab" href="#`+k2+`" role="tab" aria-controls="`+k2+`"
                                        aria-selected="true" onclick="setTabActive(`+k2+`, '#tab-widget-`+k+` li a', '#tab-widget-`+k+` li #tab-`+k2+`', '#tab-widget-content-`+k+` .tab-widget-content-`+k+`', '#tab-widget-content-`+k+` #`+k2+`')">`+k2+`</a>
                                    </li>`;

                        tab_widget_content += `<div class="tab-pane tab-widget-content-`+k+` fade show `+active+`" id="`+k2+`" role="tabpanel" aria-labelledby="`+k2+`-tab">
                                                    <div class="row">`;
                        $.each(v2, function(k3, v3) {
                            tab_widget_content += v3;
                        })
                        tab_widget_content += `     </div>
                                                </div>`;
                        key2++;
                    });
                    tab_widget += `</ul>`;
                    tab_widget_content += `</div>`;
                    divgroup += tab_widget;
                    divgroup += tab_widget_content;

                    divgroup +=             `</div>
                                        </div>
                                    </div>
                                </div>`;
                });
                divform += divgroup;
                // console.log(colgroupname);
                // console.log(colgroupname_data);

                $('#'+element_id).html(divform);

                if (statusadd) {
                    $('#'+element_id).LoadingOverlay("hide");
                    // $('#another-card').show();
                } else {
                    if (element_id == 'divform') {
                        // console.log(widget_noid)
                        // console.log(divform)
                        
                        // $.ajax({
                        //     url: '/find_tmd',
                        //     type: "POST",
                        //     dataType: 'json',
                        //     header:{
                        //         'X-CSRF-TOKEN':$('#token').val()
                        //     },
                        //     data: {
                        //         "id" : nownoid,
                        //         "namatable" : $("#namatable").val(),
                        //         "_token": $('#token').val(),
                        //         maincms: JSON.stringify(maincms)
                        //     },
                        //     success: function (response) {
                                // alert(response.length);return false;
                                if (responsefind != null) {
                                    nownoid = responsefind.nownoid;

                                    var maincms_panel = new Array();
                                    $.each(maincms['panel'], function(k, v) {
                                        $.each(maincms['widget'], function(k2, v2) {
                                            if (v.idwidget == v2.noid) {
                                                maincms_panel[v.noid] = v2;
                                            }
                                        })
                                    })

                                    // console.log(responsefind.result_detail)
                                    var key = 0;
                                    $.each(responsefind.result_detail, function(k, v) {
                                        if (key == 0) {
                                            nownoidtab = maincms_panel[k].noid
                                        }
                                        // alert(k)
                                        // console.log(v)
                                        // localStorage.removeItem('input-data-'+k)
                                        localStorage.setItem('input-data-'+k, JSON.stringify(v))
                                        localStorage.setItem('tablename-'+k, maincms_panel[k].maintable)
                                        // console.log(v)
                                        key++;
                                    })
                                    // $('#noid').val(id)
                                    // console.log('responsefind')
                                    // console.log(responsefind)
                                    $.each(responsefind, function(k, v) {
                                        if (v.formtypefield == 11) {
                                            $("#"+v.tablename+'-'+v.field+' input').val(v.value);
                                        } else if (v.formtypefield == 31){
                                            if (v.value == '1') {
                                                $("#"+v.tablename+'-'+v.field).attr('checked', true);
                                            }
                                        } else {
                                            $("#"+v.tablename+'-'+v.field).val(v.value);
                                        }

                                        if (v.disabled) {
                                            $("#"+v.tablename+'-'+v.field).attr('disabled', 'disabled');
                                        }
                                        if (statusdetail) {
                                            $("#"+v.tablename+'-'+v.field).attr('disabled', 'disabled');
                                        }
                                        if (v.edithidden) {
                                            $("#div-"+v.field).css('display', 'none');
                                        }
                                    });
                                    $('#'+element_id).LoadingOverlay("hide");
                                    console.log(divform)
                                }
                            // }
                        // });
                    }
                }

                $.each(response.field, function(k, v) {
                    if (v.newhidden == 1) {
                        $('#div-'+v.fieldname).css('display', 'none');
                    }
                })    

                $.each(response.allfield, function(k, v) {
                    // if (v.tablename == 'fincashbankdetail' && v.fieldname == 'nominal') {
                    //     console.log('cekkkkkk')
                    //     console.log(JSON.parse(v.onchange))
                    // }
                    if (v.onchange) {
                        var onchange = JSON.parse(v.onchange);
                        $.each(onchange, function(k2, v2) {
                            if (v2.TriggerOn == 'OnChange') {
                                var variables = new Array();
                                $.each(v2.sourcefield, function(k3, v3) {
                                    variables.push(v3.alias)
                                })

                                $.each(v2.sourcefield, function(k3, v3) {
                                    localStorage.removeItem(v.tablename+'-'+v2.targetfield+'-$'+v3.alias);
                                    // localStorage.setItem('$'+v3.alias, 0);
                                    // alert(v3.fieldname)
                                    // if (v3.fieldname == 'nominal') {
                                    //     console.log('////////////////')
                                    //     console.log(v2.targetfield)
                                    // }
                                    if (v3.sourcetype == 'EditBox') {
                                        var value_onkeyup = document.querySelector('#'+v.tablename+'-'+v3.fieldname).getAttribute('onKeyUp');
                                        if (value_onkeyup == null) {
                                            value_onkeyup = ''
                                        }
                                        $('#'+v.tablename+'-'+v3.fieldname).attr('onKeyUp', value_onkeyup+`triggerOn(this, 'EditBox', '`+v.tablename+`', '`+JSON.stringify(variables)+`', '`+v2.throwtype+`', '`+v2.targettable+`', '`+v2.targetfield+`', '`+v3.alias+`', '`+v3.fieldname+`', '`+v3.fieldvalue+`', '`+v2.formula+`');`);
                                    } else {
                                        var value_onchange = document.querySelector('#'+v.tablename+'-'+v3.fieldname).getAttribute('onChange');
                                        if (value_onchange == null) {
                                            value_onchange = ''
                                        }
                                        $('#'+v.tablename+'-'+v3.fieldname).attr(v2.TriggerOn, value_onchange+`triggerOn(this, 'NotEditBox', '`+v.tablename+`', '`+JSON.stringify(variables)+`', '`+v2.throwtype+`', '`+v2.targettable+`', '`+v2.targetfield+`', '`+v3.alias+`', '`+v3.fieldname+`', '`+v3.fieldvalue+`', '`+v2.formula+`');`);
                                        $('#'+v.tablename+'-'+v3.fieldname).trigger('change')
                                        // console.log(v.tablename+'-'+v3.fieldname);
                                    }
                                })
                            } else if (v2.TriggerOn == 'OnSave') {
                                mainonchange.push(v2)
                            }
                        })
                    }

                    // if (v.newreadonly) {
                    //     $('#'+v.tablename+'-'+v.fieldname).
                    // }
                })    
                
                // console.log(mainonchange)

                // $.each(response.addonchange, function(k, v) {
                //     $('#'+v).attr('onChange', 'alert("add_onchange")')
                // })

                $('.datepicker').datepicker({
                    format: 'd-M-yyyy'
                    // startDate: '-3d'
                });

                //select2
                $('.select2').select2();
                $('.select2').css('width', '100%');
                $('.select2 .selection .select2-selection').css('height', '35px');
                $('.select2 .selection .select2-selection .select2-selection__rendered').css('margin-top', '-6px');
                $('.select2 .selection .select2-selection .select2-selection__arrow').css('margin-top', '4px');

                $.each(response.allfield, function(k, v) {
                    if (v.newreadonly) {
                        // alert('#select2-'+v.tablename+'-'+v.fieldname+'-container');
                        $('#div-'+v.tablename+'-'+v.fieldname+' span span .select2-selection--single').css('background-color', '#E5E5E5');
                    }
                })
                //select2readonly
                // $('.select2readonly').select2();
                // $('.select2readonly').css('width', '100%');
                // $('.currency').currency({region:'IDR'});
                // console.log(roles)  
                if (element_id != 'divform') {
                    $('#'+element_id).LoadingOverlay("hide");
                }
            },
        });
    }

    function triggerOn(context, sourcetype, tablename, variables, throwtype, targettable, targetfield, alias, fieldname, fieldvalue, formula) {
        // alert($('#idcurrency').val())
        // alert('#'+tablename+'-'+fieldname)
        var tt = tablename+'-'+targetfield;
        var tf = tablename+'-'+fieldname;
        
        if (sourcetype == 'EditBox') {
            localStorage.setItem(tt+'-$'+alias, $('#'+tf).val());
            // console.log(formula)
            // localStorage.removeItem('$'+alias);

            var replaceformula = formula;
            var f = formula.split('*');
            $.each(JSON.parse(variables), function(k, v) {
                var ls = localStorage.getItem(tt+'-$'+v);
                console.log('ls-'+tt+'-$'+v+' = '+localStorage.getItem(tt+'-$'+v))
                if (ls) {
                    // for (var i=0; i<10; i++) {
                        replaceformula = replaceformula.replace('$'+v, ls);
                        // console.log('ceking = '+ls)
                    // }
                } else {
                    replaceformula = replaceformula.replace('$'+v, 0);
                }
            })
            
            if (formula.split(' ')[0] != 'if') {
                $('#'+tt).val(eval(replaceformula))
            }
            // console.log('#'+tt)
            // console.log(replaceformula)
            // console.log(eval(replaceformula))
            // console.log(fieldname)
            // console.log(fieldvalue)
            // console.log(formula)
        } else {
            localStorage.setItem(tt+'-$'+alias, $('#'+tf+' :selected').val());
            // console.log('isi = '+$('#'+tf+' :selected').val())

            var getattribute = 'data-'+tf+'-'+$('#'+tf+' :selected').val();
            // console.log('testing = '+context.getAttribute(getattribute));
            // console.log('#'+tt)
            // alert(1)
            if (throwtype == 'FILTER') {
                $('#'+tt).attr('data-'+tf, $('#'+tf+' :selected').val())
                
                // console.log(context)
                // console.log('data-'+tt+'-querylookup')
                // console.log(context.getAttribute(getattribute))
                // console.log(document.querySelector('#'+tt))

                var frml = formula.split(';');
                var frml_where = '';
                $.each(frml, function(k, v) {
                    var _v = v.split(':');
                    if (_v[0] == 'WHERE') {
                        __v = _v[1].split(',');
                        frml_where += __v[0]+' '+__v[1]+' '+__v[2]+' '+__v[3]+' ';
                    }  
                })

                var querylookup = document.querySelector('#'+tt).getAttribute('data-'+tt+'-querylookup').split(';');
                var select = querylookup[0].split(':')[1];
                var from = querylookup[1].split(':')[1];
                var where = querylookup[2].split(':')[1].split('AND');
                var order = querylookup[3].split(':')[1].split(',');
        
                var query = 'SELECT '+select+' FROM '+from+' WHERE ';
                var add_frml = false;
                $.each(where, function(k, v) {
                    if (v) {
                        if (k > 0) {
                            query += 'AND ';
                            add_frml = true;
                        }
                        var _v = v.split(',');
                        query += _v[0]+' '+_v[1]+' '+_v[2]+' ';
                    }
                })
                if (add_frml) {
                    frml_where = frml_where.substr(4);
                }
                query += frml_where;
                
                query += ' ORDER BY '+order[0]+' '+order[1];
                query = query.replace('$'+alias, document.querySelector('#'+tt).getAttribute('data-'+tf))
                // console.log(frml_where.substr(4))

                getSelectChild(query, '#'+tt)

                // console.log('query = '+query)
                // console.log('filter = '+document.querySelector('#'+tt).getAttribute('data-'+tf))

            } else {
                $('#'+tt).val(context.getAttribute(getattribute))
            }
        }


        if (formula.split(' ')[0] == 'if') {
            var replaceformula = formula;
            $.each(JSON.parse(variables), function(k, v) {
                var ls = localStorage.getItem(tt+'-$'+v);
                console.log('ls-'+tt+'-$'+v+' = '+localStorage.getItem(tt+'-$'+v))
                if (ls) {
                    // for (var i=0; i<10; i++) {
                        replaceformula = replaceformula.replace('$'+v, ls);
                        // console.log('ceking = '+ls)
                    // }
                } else {
                    replaceformula = replaceformula.replace('$'+v, 0);
                }
            })
            eval("var fn = function(){ "+replaceformula+" }")
            // alert(replaceformula)
            // alert(fn())
            $('#'+tt).val(fn())
        }

        $('#'+tt).trigger('change')
        $('#'+tt).trigger('keyup')

        // console.log(targetfield)
        // console.log(context.getAttribute(getattribute))
        // console.log(context.getAttribute(getattribute))
        // if (targettable == 'self') {
        //     targettable = 'accmcurrency';
        //     $.ajax({
        //         type: "POST",
        //         header:{
        //             'X-CSRF-TOKEN':$('#token').val()
        //         },
        //         url: "/get_trigger_on_tmd",
        //         dataType: "json",
        //         data:{
        //             "_token":$('#token').val(),
        //             query: `SELECT `+fieldvalue+` AS `+alias+` FROM `+targettable+` WHERE noid='`+context.getAttribute(getattribute)+`' LIMIT 1`,
        //             alias: alias,
        //             formula: formula,
        //         },
        //         success: function(response) {
        //             $('#'+targetfield).val(response)
        //             console.log(response)
        //         }
        //     })
        // }
    }

    function getSelectChild(query, element_target) {
        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            url: "/get_select_child_tmd",
            data:{
                "_token":$('#token').val(),
                query: query,
            },
            success: function (response) {
                $(element_target).html(response)
            }
        })
    }

    function getFormTab() {
        var tab_form = '';
        var tab_form_content = '';
        $.each(maincms['widget'], function(k0, v0) {
            if (k0 > 0) {
                var active = '';
                if (k0 == 1) {
                    active = 'active';
                }

                $.ajax({
                    type: "POST",
                    header:{
                        'X-CSRF-TOKEN':$('#token').val()
                    },
                    url: "/get_form_tmd",
                    dataType: "json",
                    data:{
                        "_token":$('#token').val(),
                        table: maincms['menu']['noid'],
                        maincms: JSON.stringify(maincms),
                        isadd: true,
                        key: 1,
                    },
                    success: function (response) {
                        console.log('DDDDD');
                        console.log(response);
                        tab_form = `<li class="nav-item">
                                        <a class="nav-link flat `+active+`" id="tab-`+v0.maintable+`" data-toggle="tab" href="#`+v0.maintable+`" role="tab" aria-controls="`+v0.maintable+`"
                                        aria-selected="true" onclick="setTabActive(`+v0.maintable+`, '#tab-widget-`+v0.maintable+` li a', '#tab-widget-`+v0.maintable+` li #tab-`+v0.maintable+`', '#tab-widget-content-`+v0.maintable+` div', '#tab-widget-content-`+v0.maintable+` #`+v0.maintable+`')">`+k0+`</a>
                                    </li>`;
                        tab_form_content += `<form id="form-`+v0.maintable+`">
                                        <div class="row">`;
                        $.each(response.field, function(key, value) {
                            if (value.tablemaster != maincms['widget'][0]['maintable']) {
                                tab_form_content += value.field;
                            }
                        });
                        tab_form_content += `   </div>
                                    </form>`;

                        var tab_action = `<li class="nav-item" style="padding-top: 5px">
                                                <button class="btn btn-sm btn-primary"><i class="fa fa-plus"></i></button>
                                                <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                          </li>`;

                        $('#tab-form-'+v0.maintable).append(tab_form+tab_action);
                        $('#tab-form-content-'+v0.maintable).append(tab_form_content);

                        $('.datepicker').datepicker({
                            format: 'd-M-yyyy'
                        });
                    },
                });
            }
        });
    }

    function setTabActive(panel_noid, element_class, element_id, element_content_class, element_content_id) {
        // alert('setTabActive('+element_class+', '+element_id+', '+element_content_class+', '+element_content_id+')')
        nownoidtab = panel_noid
        another_selecteds = [];
        $(element_class).removeClass('active')
        $(element_id).addClass('active')
        $(element_content_class).removeClass('active')
        $(element_content_id).addClass('active')
        // alert(element_content_id)
    }

    function getCmsTab(statusadd) {
        var tab_widget = '';
        var tab_widget_content = '';
        // var maincms_widget = new Array();
        // $.each(maincms['widget'], function(k, v) {
        //     maincms_widget[v.noid] = v;
        // })
        var maincms_widgetgridfield = new Array();
        $.each(maincms['widgetgridfield'], function(k, v) {
            $.each(v, function(k2, v2) {
                maincms_widgetgridfield[v2.idcmswidgetgrid] = v;
            })
        })
        // console.log(maincms_widgetgridfield)
        $.each(maincms['panel'], function(k, v) {
        // console.log(maincms_widgetgridfield[v.idwidget].colshow)
            var v_widget = maincms_widget[v.idwidget];
            if (k > 0) {
                statusaddtab[v.noid] = true;

                var active = '';
                if (k == 1) {
                    active = 'active';
                }

                var tabcaption = v.tabcaption.split(';');

                tab_widget += `<li class="nav-item">
                                    <a class="nav-link nav-link-custom flat `+active+`" id="btn-tab-`+v.noid+`" data-toggle="tab" href="#`+v.noid+`" role="tab" aria-controls="`+v.noid+`"
                                    aria-selected="true" onclick="setTabActive(`+v.noid+`, '#tab-widget li a', '#tab-widget-`+v.noid+` li #btn-tab-`+v.noid+`', '#tab-widget-content .tab-widget-content', '#tab-widget-content-`+v.noid+`')">`+tabcaption[1]+`</a>
                                </li>`;

                tab_widget_content += `<div class="tab-pane tab-widget-content fade show `+active+`" id="tab-widget-content-`+v.noid+`" role="tabpanel" aria-labelledby="`+v.noid+`-tab">
                                            <div class="card card-custom flat">
                                                <div class="card-header card-header_ bg-main-1 d-flex flat" >
                                                    <div class="card-title">
                                                        <span class="card-icon">
                                                            <i class="`+v_widget.widgeticon+` text-white"></i>
                                                        </span>
                                                        <h3 class="card-label text-white">
                                                            `+v_widget.widgetcaption+`
                                                        </h3>
                                                    </div>`;

                // if (!statusadd) {
                    tab_widget_content += `         <div class="card-toolbar" >
                                                        <div id="add-data-`+v.noid+`">
                                                            <a onclick="addDataTab('`+v.noid+`')" href="javascript:;" id="btn-add-data-`+v.noid+`" style="color: #FFFFFF;margin-right: 5px;" class="btn btn-sm flat" onmouseover="mouseoverAddData('btn-add-data-`+v.noid+`')" onmouseout="mouseoutAddData('btn-add-data-`+v.noid+`')"><i style="color:#FFFFFF" class="fa fa fa-save"></i> Tambah Data</a>
                                                        </div>
                                                        <div id="save-data-`+v.noid+`" style="display: none">
                                                            <a onclick="saveDataTab(`+v.noid+`)" id="btn-save-data-`+v.noid+`" class="btn btn-sm btn-success flat" style="margin-right: 5px;">
                                                                <i class="fa fa-save"></i> Save
                                                            </a>
                                                        </div>
                                                        <div id="cancel-data-`+v.noid+`" style="display: none">
                                                            <a onclick="cancelDataTab('`+v.noid+`')" class="btn btn-sm btn-default flat" style="margin-right: 5px;">
                                                                <i class="fa fa-reply"></i> Cancel
                                                            </a>
                                                        </div>
                                                        <div id="back-data-`+v.noid+`" style="display: none">
                                                            <a id="btn-back-`+v.noid+`" class="btn btn-sm btn-success flat" onclick="cancelDataTab()"  style="margin-right: 5px;">
                                                                <i class="fa fa-reply"></i> Back
                                                            </a>
                                                        </div>
                                                        <div class="dropdown" style="margin-right: 5px;">
                                                            <a style="background-color: transparent !important; border: 1px solid #95c9ed; color: #aad4f0;" class="btn btn-default btn-sm flat" href="javascript:;" data-toggle="dropdown">
                                                                <i style="color:#FFFFFF" class="fa fa-cogs"></i>
                                                                    Tools
                                                                <i style="color:#FFFFFF" class="fa fa-angle-down"></i>
                                                            </a>
                                                            <ul style="" id="another-drop_export" class="dropdown-menu dropdown-menu-right">
                                                                <li id="another-space-button-export-`+v.noid+`"></li>
                                                            </ul>
                                                        </div>
                                                        <a style="background-image: none !important; margin-right: 5px; /* height: 27px; */ width: 27px !important; text-align: center; padding: 2px !important; border-radius: 50% !important; background-color: #2196F3; color: #FFF;" id="tool_refresh-`+v.noid+`" href="javascript:;" onclick="refresh(`+v.noid+`)" class="bg-main-1"><i style="color:#FFFFFF" class="fa fa-sync-alt"></i></a>
                                                        <a style="background-image: none !important; margin-right: 5px; /* height: 27px; */ width: 27px !important; text-align: center; padding: 2px !important; border-radius: 50% !important; background-color: #2196F3; color: #FFF;" id="tool_filter-`+v.noid+`" href="javascript:;" onclick="showModalFilter(`+v.noid+`)" class="bg-main-1"><i style="color:#FFFFFF" class="fa fa-filter"></i></a>
                                                        <a style="background-image: none !important; /* height: 27px; */ width: 27px !important; text-align: center; padding: 2px !important; border-radius: 50% !important; background-color: #2196F3; color: #FFF;" id="tool_refresh_up-`+v.noid+`" onClick="toggleButtonCollapse(`+v.noid+`)" href="javascript:;" class="bg-main-1"><i style="color:#FFFFFF" class="fa fa-sort"></i></a>
                                                    </div>`;
                // }

                         tab_widget_content += `</div>
                                                <div class="card-body card-body-tab-custom">
                                                    <div class="row" style="display: block">
                                                        <div id="space_filter_button-`+v.noid+`" style="margin-bottom: 10px">
                                                        </div>
                                                    </div>`
                             tab_widget_content += `<form data-toggle="validator" class="form" id="tab-form-`+v.noid+`" style="display: none">
                                                        <div class="row" id="tab-form-div-`+v.noid+`">
                                                        </div>
                                                    </form>`

                            // comment
                            //  tab_widget_content += `<table id="tab-table-`+v.noid+`" class="table table-striped table-bordered table-condensed nowrap" cellspacing="0" width="100%" style="background-color:#BBDEFB !important; border-collapse: collapse !important;">
                            //                             <thead>
                            //                                 <tr id="tab-thead-`+v.noid+`" class="bg-main">`

                            //             var colspan = 0;
                            //             $.each(maincms_widgetgridfield[v.idwidget], function(k2, v2) {
                            //                 if (v2.colshow == 1) {
                            //                     colspan++
                            //                 }
                            //             })
                            //             tab_widget_content += `<th style="text-align:center" colspan="2">#</th>`;
                            //             tab_widget_content += `<th style="text-align:center" colspan="`+colspan+`"></th>`;
                            //             tab_widget_content += `<th colspan="1" style="text-align:center">---</th>`;

                            //          tab_widget_content += `</tr>
                            //                                 <tr id="tab-thead-columns-`+v.noid+`">`

                                    
                            //             tab_widget_content += `<th><input type="checkbox" id="select_all_`+v.noid+`" onchange="selectAllCheckboxTab(`+v.noid+`)" class="checkbox-`+v.noid+` checkbox-`+v.noid+`-0" name="checkbox-`+v.noid+`[]" value="FALSE"></th>
                            //                                    <th>No</th>`;
                            //      $.each(maincms_widgetgridfield[v.idwidget], function(k2, v2) {
                            //          if (v2.colshow == 1) {
                            //              tab_widget_content += `<th class="text-`+v2.colheaderalign+`" style="width: `+v2.colwidth+`px; height: `+v2.colheight+`px;">`+v2.fieldcaption+`</th>`
                            //          }
                            //      })

                            //             tab_widget_content += `<th>Action</th>`

                            //          tab_widget_content += `</tr>
                            //                             </thead>
                            //                         </table>`
                            // endcomment


                            tab_widget_content += `<div id="tab-appendInfoDatatable-`+v.noid+`" style="border: 1px solid #3598dc;"></div>
                                                    <div id="tab-appendButtonDatatable-`+v.noid+`"></div>
                                                    <div id="ts-`+v.noid+`" class="table-scrollable" style="border: 1px solid #3598dc;">`
                             tab_widget_content += `<table bg-color="blue" class="nowrap table table-striped table-bordered table-hover  dataTable no-footer" id="tab-table-`+v.noid+`" style="background-color:#BBDEFB !important;"  aria-describedby="table_master_info" role="grid">
                                                        <thead>
                                                            <tr id="tab-thead-`+v.noid+`" class="bg-main">`

                                        var colspan = 0;
                                        var colspanfooter = 0;
                                        var tfootcolumns = `<tfoot>
                                                                <tr>`
                                        var index = 2;
                                        var data_fft = new Array();
                                        $.each(maincms_widgetgridfield[v.idwidget], function(k2, v2) {
                                            if (v2.colshow == 1) {
                                                colspan++
                                                if (v2.colsummaryfield || v2.colsummaryfield != null) {
                                                    tfootcolumns += `<th class="text-right" colspan="`+colspanfooter+`" data-function="" style="border: 1px solid #95c9ed"></th>`
                                                    // console.log('collll '+colspanfooter)
                                                    colspanfooter = 1
                                                    var dataconfig = JSON.parse(v2.colsummaryfield).dataconfig[0].summarytype+'('+index+')'
                                                    data_fft.push({
                                                        "index": index,
                                                        "function": dataconfig,
                                                    })
                                                } else {
                                                    colspanfooter++
                                                }
                                                index++;
                                            }
                                            if (maincms_widgetgridfield[v.idwidget].length == k2+1) {
                                                // console.log('terakhir '+colspanfooter)
                                                tfootcolumns += `<th class="text-right" colspan="`+(colspanfooter)+`" style="border: 1px solid #95c9ed"></th>`
                                            }
                                        })
                                        
                                        functionfootertab[v.idwidget] = data_fft;
                                        
                                        tfootcolumns += `</tr>
                                                        </tfoot>`


                                        tab_widget_content += `<th style="text-align:center" colspan="2">#</th>`;
                                        tab_widget_content += `<th style="text-align:center" colspan="`+colspan+`"></th>`;
                                        tab_widget_content += `<th colspan="1" style="text-align:center">---</th>`;

                                     tab_widget_content += `</tr>
                                                            <tr id="tab-thead-columns-d-`+v.noid+`">`

                                    
                                        tab_widget_content += `<th style="width: 30px;"><input type="checkbox" id="select_all_`+v.noid+`" onchange="selectAllCheckboxTab(`+v.noid+`)" class="checkbox-`+v.noid+` checkbox-`+v.noid+`-0" name="checkbox-`+v.noid+`[]" value="FALSE"></th>
                                                               <th style="width: 30px;">No</th>`;
                                 $.each(maincms_widgetgridfield[v.idwidget], function(k2, v2) {
                                     if (v2.colshow == 1) {
                                         tab_widget_content += `<th class="text-`+v2.colheaderalign+`" style="width: `+v2.colwidth+`px; height: `+v2.colheight+`px;">`+v2.fieldcaption+`</th>`
                                     }
                                 })

                                        tab_widget_content += `<th style="width: auto;">Action</th>`

                                     tab_widget_content += `</tr>
                                                        </thead>`
                                     tab_widget_content += tfootcolumns
                                     tab_widget_content += `</table>`
                                                        
                         tab_widget_content += `</div>
                                            </div>
                                        </div>
                                    </div>`
            }
        })
        $('#tab-widget').html(tab_widget);
        $('#tab-widget-content').html(tab_widget_content);
    }

    function saveDataTab(panel_noid) {
        // localStorage.removeItem('input-data-'+panel_noid);

        // alert('nextnoid='+nextnoid);
        // alert('nownoid='+nownoid);
        // return false;
        var onrequired = false;
        var errormessage = '';
        if (statusaddtab[panel_noid] == true) {
            var idd = {};
            var idd_fix = new Array();
            $.each($('#tab-form-'+panel_noid).serializeArray(), function(k, v) {
                if (k == 0) {
                    idd['idmaster'] = statusadd ? nextnoid : nownoid;
                    idd['noid'] = nextnoidtab[panel_noid];
                    nextnoidtab[panel_noid]++;
                } else {
                    idd[v.name] = v.value
                    var tf = maintable[panel_noid]+'-'+v.name;
                    var coltypefield = document.querySelector('#'+tf).getAttribute('data-'+tf+'-coltypefield');
                    if (coltypefield == 96) {
                        idd[v.name+'_lookup'] = $('#'+tf+' :selected').text();
                        // alert(idd[v.name+'_lookup'])
                    }

                    var formcaption = $('#'+maintable[panel_noid]+'-'+v.name).attr('data-'+maintable[panel_noid]+'-formcaption');
                    if ($('#'+tf).attr('data-'+maintable[panel_noid]+'-newreq') == 1) {
                        if (v.value == '') {
                            onrequired = true;
                            errormessage += formcaption+' is required!<br>'
                        }
                    }
                }
            })

            if (onrequired) {
                $.notify({
                    message: errormessage
                },{
                    type: 'danger'
                });
                return false;
            }

            idd_fix[0] = idd;
            current_idd = localStorage.getItem('input-data-'+panel_noid);

            if (current_idd == null) {
                localStorage.setItem('input-data-'+panel_noid, JSON.stringify(idd_fix))
            } else {
                merge_idd = JSON.parse(current_idd).concat(idd_fix)
                localStorage.removeItem('input-data-'+panel_noid);
                localStorage.setItem('input-data-'+panel_noid, JSON.stringify(merge_idd))
            }   
            // console.log('SAVEDATATAB')
            // console.log(JSON.parse(localStorage.getItem('input-data-'+panel_noid)))
            // console.log(idd)
            // return false
            // refreshSumTab(0, panel_noid)
        } else {
            var idd = {};
            var idd_fix = new Array();
            $.each($('#tab-form-'+panel_noid).serializeArray(), function(k, v) {
                if (k == 0) {
                    idd['idmaster'] = nextnoid
                    idd['noid'] = nextnoidtab[panel_noid];
                    nextnoidtab[panel_noid]++;
                } else {
                    idd[v.name] = v.value
                    var tf = maintable[panel_noid]+'-'+v.name;

                    var formcaption = $('#'+maintable[panel_noid]+'-'+v.name).attr('data-'+maintable[panel_noid]+'-formcaption');
                    if ($('#'+tf).attr('data-'+maintable[panel_noid]+'-editreq') == 1) {
                        if (v.value == '') {
                            onrequired = true;
                            errormessage += formcaption+' is required!<br>'
                        }
                    }
                }
            })

            if (onrequired) {
                $.notify({
                    message: errormessage
                },{
                    type: 'danger'
                });
                return false;
            }

            idd_fix[0] = idd;
            current_idd = localStorage.getItem('input-data-'+panel_noid);

            var replace_current_idd = new Array();
            $.each(JSON.parse(current_idd), function(k, v) {
                if (v.noid == nownoidtab) {
                    replace_current_idd[k] = idd;
                    replace_current_idd[k]['noid'] = nownoidtab;
                    replace_current_idd[k]['idmaster'] = nownoid;
                } else {
                    replace_current_idd[k] = v;
                }
            })
            localStorage.removeItem('input-data-'+panel_noid);
            localStorage.setItem('input-data-'+panel_noid, JSON.stringify(replace_current_idd))
            // console.log(replace_current_idd)
            // console.log(idd)
        }

        $.each(mainonchange, function(k, v) {
            var current_idd_mo = JSON.parse(localStorage.getItem('input-data-'+panel_noid))
            // console.log(current_idd_mo)
            $.each(v.sourcefield, function(k2, v2) {
                if (v2.sourcetype == 'Summary') {
                    var summary = 0;
                    $.each(current_idd_mo, function(k3, v3) {
                        // console.log(v3)
                        summary = parseInt(summary)+parseInt(v3[v2.fieldname]);
                    })
                    $('#'+maintable[v.targettable]+'-'+v.targetfield).val(summary)
                    $('#'+maintable[v.targettable]+'-'+v.targetfield).trigger('change')
                    $('#'+maintable[v.targettable]+'-'+v.targetfield).trigger('keyup')
                    console.log('#'+maintable[v.targettable]+'-'+v.targetfield+' = '+summary)
                }
            })
        })


        refreshTab()
        cancelDataTab(panel_noid)
        // localStorage.removeItem('input-data-'+panel_noid);
        // console.log(JSON.parse(localStorage.getItem('input-data-'+panel_noid)))
        
        // console.log('aku cek ini')
        // console.log(JSON.parse(localStorage.getItem('input-data-'+panel_noid)))
        // console.log(panel_noid)
        // return false
    }

    function refreshSumTab(id, panel_noid) {

///////////////////

        // var maincms_widgetgrid = new Array();
        // $.each(maincms['widgetgrid'], function(k, v) {
        //     maincms_widgetgrid[v.idcmswidget] = v;
        // })

        if (maincms_widgetgrid[panel_noid]['gridconfig']) {
            // var wgb_onsave = maincms_widgetgrid[panel_noid]['gridconfig']['buttonevent']['OnSave'];
            var wgb_ondelete = JSON.parse(maincms_widgetgrid[panel_noid]['gridconfig'])['buttonevent']['OnDelete'];
            if (wgb_ondelete['status']) {
                $.each(wgb_ondelete['Parameter'], function(k, v) {
                console.log(v.Source['sourcetype']);
                // return false;
                    if (v.typeaction == 'EXEC_QUERY') {
                        // return false;
                        if (v['Source']['sourcetype'] == 'sum') {
                            var current_idd_mo = JSON.parse(localStorage.getItem('input-data-'+panel_noid))
                            var summary = 0;
                            var idtypetranc = '';
                            var idmaster = '';
                            var noid = '';
                            var globaluser = 59;
                            $.each(current_idd_mo, function(k2, v2) {
                                summary = parseInt(summary)+parseInt(v2[v['Source']['fieldname']]);
                                if (k2 == id) {
                                    idtypetranc = v2['idtypetranc'];
                                    idmaster = v2['idmaster'];
                                    noid = parseInt(nownoidtab)+parseInt(k2)+1;
                                }
                            })
                            if (v['Target']['targettype'] == 'EXEC_QUERY') {
                                var query = v['Query']['query'].replace('$idtypetranc', idtypetranc).replace('$idmaster', idmaster).replace('$noid', noid).replace('$GLOBAL_USER', globaluser)
                                // console.log(query)
                                // return false;
                                
                                $.ajax({
                                    type: "POST",
                                    header:{
                                        'X-CSRF-TOKEN':$('#token').val()
                                    },
                                    url: "/save_log_tmd",
                                    data:{
                                        "_token": $('#token').val(),
                                        "idconnection": v['Query']['idconnection'],
                                        "query": query,
                                    },
                                    success: function(response) {
                                    }
                                })
                            }
                        }
                    }
                })
            }
        }
    }
    
    function removeSelected(selecteds, scma=null) {
        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            url: "/delete_select"+main.urlcode,
            dataType: "json",
            data:{
                "_token": $('#token').val(),
                "selecteds": selecteds,
                "selectcheckboxmall": scma
            },
            success: function(response) {
                if (!response.success) {
                    $.notify({message: response.message},{type: 'danger'});
                } else {
                    $.notify({message: response.message},{type: 'success'});
                    selectcheckboxm = new Array();
                    refresh()
                }
            },
        });
    }

    function removeSelectedTab(selecteds) {
        var maincms_panel = new Array();
        $.each(maincms['panel'], function(k, v) {
            $.each(maincms['widget'], function(k2, v2) {
                if (v.idwidget == v2.noid) {
                    maincms_panel[v.noid] = v2;
                }
            })
        })

        // $.ajax({
        //     type: "POST",
        //     header:{
        //         'X-CSRF-TOKEN':$('#token').val()
        //     },
        //     url: "/delete_selected_tmd",
        //     dataType: "json",
        //     data:{
        //         "_token": $('#token').val(),
        //         "table" : maincms_panel[nownoidtab].maintable,
        //         "selecteds": selecteds,
        //     },
        //     success: function(response) {
                // var replace_input_data = localStorage.getItem('input-data-'+nownoidtab);
                var replaceinputdata = new Array();
                $.each(JSON.parse(localStorage.getItem('input-data-'+nownoidtab)), function(k, v) {
                    var notinput = false;
                    
                    $.each(selecteds, function(k2, v2) {
                        if (v.noid == v2) {
                            notinput = true
                        }
                    })

                    if (!notinput) {
                        replaceinputdata.push(v);
                    }
                })
                // console.log('data tidak terhapus')
                // console.log(replaceinputdata)
                // return false
                localStorage.setItem('input-data-'+nownoidtab, JSON.stringify(replaceinputdata))

                // console.log(replaceinputdata);
                // return false;

                $.notify({
                    message: 'Delete data has been succesfullly.' 
                },{
                    type: 'success'
                });
                refreshTab()
                // var filter_button = '';
                // for (var i=0; i<response.filterButton[0].length; i++) {
                //     filter_button += response.filterButton[0][i].taga;
                // }
                // $('#space_filter_button').html(filter_button)
                // console.log(filter_button);
        //     },
        // });
    }

    function getCmsThead() {
        var count_wgf = 0;
        var colgroupname = new Array();
        $.each(maincms['widgetgridfield'][0], function(k, v) {
            if (v.colshow == 1) {
                if (v.colgroupname) {
                    colgroupname.push(v.colgroupname);
                }
                count_wgf++;
            }
        })
        const unique = (value, index, self) => {
            return self.indexOf(value) === index
        }
        colgroupname = colgroupname.filter(unique)
        console.log(colgroupname)

        if (maincms['widgetgrid'][0]['colcheckbox']) {
            var thead = `<th style="text-align:center" colspan="2">#</th>`;
        } else {
            var thead = `<th style="text-align:center" colspan="1">#</th>`;
        }
        // $.each(maincms['widgetgridfield'], function(k, v) {
            thead += `<th style="text-align:center" colspan="`+count_wgf+`"></th>`;
        // })
        thead += `<th colspan="1" style="text-align:center">---</th>`;
        $('#thead').html(thead);
    }

    function getCmsTabThead(panel_idwidget) {
        $.each(maincms['widgetgridfield'], function(k, v) {
            if (k > 0) {
                if (v.idcmswidgetgrid == panel_idwidget)
                    if (v.colshow == 1) {
                        var thead = `<th style="text-align:center" colspan="2">#</th>`;
                        // $.each(maincms['widgetgridfield'], function(k, v) {
                            thead += `<th style="text-align:center" colspan="`+v.length+`"></th>`;
                        // })
                        thead += `<th colspan="1" style="text-align:center">---</th>`;
                        return thead;
                        // $('#tab-thead-'+maincms['widget'][k].maintable).html(thead);
                        // console.log('#tab-thead-'+maincms['widget'][k].maintable)
                    }
            }
        })
    }

    function saveFilter() {
        // $('#modal-notif').css('display', 'none')
        // var config_widget = {
        //     WidgetFilter: '',
        //     WidgetConfig: '',
        // }
        var formdata = $('#formfilter').serialize();

        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            url: "/save_filter_tmd",
            dataType: "json",
            data:{
                "_token":$('#token').val(),
                "data" :{
                    'data': formdata,
                    'url_code': $('#url_code').val()
                }
            },
            success:function(response){
                // console.log(response)
                if (response.result) {
                    // alert('Success')
                    closeModalFilter()
                    $.notify({
                        message: 'Save data has been succesfullly.' 
                    },{
                        type: 'success'
                    });
                    setInterval(function(){
                        location.reload()
                    }, 3000)
                    // location.reload()
                } else {
                    // $('#modal-notif').css('display', 'block')
                    // setInterval(function(){
                    //     $('#modal-notif').css('display', 'none')
                    // }, 3000)
                    
                    $.notify({
                        message: 'Save data has been error.' 
                    },{
                        type: 'danger'
                    });
                }
            }
        });
    }

    function delRow(key) {
        filtercontenttabs.filtercontentrow[selected_filter_tabs][key] = '';
        getFilterContentTabs()
    }

    function addRow() {
        filtercontenttabs.filtercontentrow[selected_filter_tabs].push(`<input type="hidden" name="tabsrow[]" value="`+selected_filter_tabs+`">
                                                                        <div class="row filter-content-row-`+selected_filter_tabs+`">
                                                                            <div class="col-md-2">
                                                                                <div class="form-group">
                                                                                    <label class="col-md-12 control-label">Caption</label>
                                                                                    <input class="form-control input-sm" name="buttoncaption[]" id="buttoncaption-`+selected_filter_tabs+`-`+filtercontenttabs.filtercontentrow[selected_filter_tabs].length+`" value="" placeholder="Caption">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label class="col-md-12 control-label">Condition</label>
                                                                                    <input class="form-control input-sm" name="condition[]" id="condition-`+selected_filter_tabs+`-`+filtercontenttabs.filtercontentrow[selected_filter_tabs].length+`" value="" placeholder="groupoperand#fieldname;operator;value;operand">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1">
                                                                                <div class="form-group">
                                                                                    <label class="col-md-12 control-label">Active</label>
                                                                                    <select class="form-control input-sm" name="buttonactive[]" id="buttonactive-`+selected_filter_tabs+`-`+filtercontenttabs.filtercontentrow[selected_filter_tabs].length+`" value="1">
                                                                                        <option value="1">True</option>
                                                                                        <option value="0" selected>False</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1">
                                                                                <div class="form-group">
                                                                                    <label class="col-md-12 control-label">Icon</label>
                                                                                    <input class="form-control input-sm" name="classicon[]" id="classicon-`+selected_filter_tabs+`-`+filtercontenttabs.filtercontentrow[selected_filter_tabs].length+`" value="" placeholder="Icon">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1">
                                                                                <div class="form-group">
                                                                                    <label class="col-md-12 control-label">Color</label>
                                                                                    <input class="form-control input-sm" name="classcolor[]" id="classcolor-`+selected_filter_tabs+`-`+filtercontenttabs.filtercontentrow[selected_filter_tabs].length+`" value="" placeholder="Color">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1" style="text-align:center;padding:21px 0 0 0">
                                                                                <div class="form-group">
                                                                                    <button id="del" onclick="delRow(`+filtercontenttabs.filtercontentrow[selected_filter_tabs].length+`)" type="button" class="btn btn-sm red" style="height: 27px; width: 38px">
                                                                                        <i class="fa fa-minus-circle text-white"></i>
                                                                                    </button>
                                                                                    <button id="add" onclick="addRow()" type="button" class="btn btn-sm green" style="height: 27px; width: 38px">
                                                                                        <i class="fa fa-plus text-white"></i>
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </div>`)
        getFilterContentTabs()
        // console.log(filtercontenttabs)
    }

    $('#delgroup').on('click', function() {
        filter_tabs.pop()
        filtercontenttabs.filtercontentgroup.pop()
        filtercontenttabs.filtercontentbody.pop()
        filtercontenttabs.filtercontentrow.pop()
        getFilterContentTabs()
    })

    $('#addgroup').on('click', function() {
        filter_tabs.push(false)

        filtercontenttabs.filtercontentgroup.push(`<input type="hidden" name="tabs[]" value="`+filtercontenttabs.filtercontentgroup.length+`">
                                                    <div style="margin:5px 5px 0 0;" id="filter-content-group-`+filtercontenttabs.filtercontentgroup.length+`" onclick="changeTabFilter(`+filtercontenttabs.filtercontentgroup.length+`)" class="filter-content-group formbtn btn default default">
                                                        <span class="text-dark">
                                                            <i class="fa fa-indent text-dark"></i> `+filtercontenttabs.filtercontentgroup.length+`
                                                        </span>
                                                    </div>`)

        filtercontenttabs.filtercontentbody.push(`<div style="display: none" id="filter-content-body-`+filtercontenttabs.filtercontentbody.length+`" class="filter-content-body formgroup col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="col-md-12 control-label">Group Caption</label>
                                                                <input name="groupcaption[]" id="groupcaption-`+filtercontenttabs.filtercontentbody.length+`" class="form-control input-sm" value="" placeholder="Group Caption">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="col-md-12 control-label">Group Caption Showed</label>
                                                                <select name="groupcaptionshow[]" id="groupcaptionshow-`+filtercontenttabs.filtercontentbody.length+`" class="form-control input-sm">
                                                                    <option value="1">True</option>
                                                                    <option value="0" selected>False</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="col-md-12 control-label">Group Dropdown Showed</label>
                                                                <select name="groupdropdown[]" id="groupdropdown-`+filtercontenttabs.filtercontentbody.length+`" class="form-control input-sm">
                                                                    <option value="-1" selected>Auto</option>
                                                                    <option value="0" selected>False</option>
                                                                    <option value="1">True</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="filter-content-row-`+filtercontenttabs.filtercontentbody.length+`">
                                                    </div>
                                                </div>`)
        filtercontenttabs.filtercontentrow.push(new Array())
        getFilterContentTabs()
    })

    function getFilterContentTabs() {
        //Set to HTML
        var html_filtercontentgroup = '';
        var html_filtercontentbody = '';
        var value = {
            groupcaption: new Array(),
            groupcaptionshow: new Array(),
            groupdropdown: new Array(),
            buttoncaption: new Array(),
            condition: new Array(),
            buttonactive: new Array(),
            classicon: new Array(),
        };

        //SET
        $.each(filtercontenttabs.filtercontentgroup, function(k, v) {
            value.buttoncaption[k] = new Array();
            value.condition[k] = new Array();
            value.buttonactive[k] = new Array();
            value.classicon[k] = new Array();

            if ($('#groupcaption-'+k).val() == undefined) { value.groupcaption.push('') } else { value.groupcaption.push($('#groupcaption-'+k).val()) }
            if ($('#groupcaptionshow-'+k).val() == undefined) { value.groupcaptionshow.push(0) } else { value.groupcaptionshow.push($('#groupcaptionshow-'+k).val()) }
            if ($('#groupdropdown-'+k).val() == undefined) { value.groupdropdown.push(-1) } else { value.groupdropdown.push($('#groupdropdown-'+k).val()) }
            
            $.each(filtercontenttabs.filtercontentrow[k], function(k2, v2) {
                if ($('#buttoncaption-'+k+'-'+k2).val() == undefined) { value.buttoncaption[k][k2] = '' } else { value.buttoncaption[k][k2] = $('#buttoncaption-'+k+'-'+k2).val() }
                if ($('#condition-'+k+'-'+k2).val() == undefined) { value.condition[k][k2] = '' } else { value.condition[k][k2] = $('#condition-'+k+'-'+k2).val() }
                if ($('#buttonactive-'+k+'-'+k2).val() == undefined) { value.buttonactive[k][k2] = 0 } else { value.buttonactive[k][k2] = $('#buttonactive-'+k+'-'+k2).val() }
                if ($('#classicon-'+k+'-'+k2).val() == undefined) { value.classicon[k][k2] = '' } else { value.classicon[k][k2] = $('#classicon-'+k+'-'+k2).val() }
            })

            html_filtercontentgroup += v
            html_filtercontentbody += filtercontenttabs.filtercontentbody[k]
        })

        $('#filtercontentgroup').html(html_filtercontentgroup);
        $('#filtercontentbody').html(html_filtercontentbody);

        $.each(filtercontenttabs.filtercontentbody, function(k, v) {
            if (filter_tabs[k] == true) {
                changeTabFilter(k)
            }
            $('#groupcaption-'+k).val(value.groupcaption[k])
            $('#groupcaptionshow-'+k).val(value.groupcaptionshow[k])
            $('#groupdropdown-'+k).val(value.groupdropdown[k])
        })

        $.each(filtercontenttabs.filtercontentrow, function(k, v) {
            $('#filter-content-row-'+k).html(v);

            $.each(v, function(k2, v2) {
                $('#buttoncaption-'+k+'-'+k2).val(value.buttoncaption[k][k2])
                $('#condition-'+k+'-'+k2).val(value.condition[k][k2])
                $('#buttonactive-'+k+'-'+k2).val(value.buttonactive[k][k2])
                $('#classicon-'+k+'-'+k2).val(value.classicon[k][k2])
            })
        })
        //End Set to HTML
        // console.log(value)
    }
    
    function changeTabFilter(key) {
        $('.filter-content-group').removeClass('purple').addClass('default')
        $('.filter-content-group span').removeClass('text-white').addClass('text-dark')
        $('.filter-content-group span i').removeClass('text-white').addClass('text-dark')
        $('#filter-content-group-'+key).removeClass('default').addClass('purple')
        $('#filter-content-group-'+key+' span').removeClass('text-dark').addClass('text-white')
        $('#filter-content-group-'+key+' span i').removeClass('text-dark').addClass('text-white')
        $('.filter-content-body').css('display', 'none')
        $('#filter-content-body-'+key).css('display', 'block')
        selected_filter_tabs = key;
        
        $.each(filter_tabs, function(k, v) {
            if (k == key) {
                filter_tabs[k] = true
            } else {
                filter_tabs[k] = false
            }
        })
    }

    function closeModalFilter() {
        $('#modal-filter').modal('hide')
    }

    function showModalFilter() {
        $('#modal-filter').modal('show')
    }

    function showModalCKEditor() {
        $('#modal-ckeditor').modal('show')
    }
    
    function getData() {
        // if (!JSON.parse(localStorage.getItem('maincms-'+values3))) {
            // localStorage.removeItem('maincms');
            $.ajax({
                type: "POST",
                header:{
                    'X-CSRF-TOKEN':$('#token').val()
                },
                url: "/get_data_tmd",
                dataType: "JSON",
                data:{
                    "_token":$('#token').val(),
                    "table": values3
                },
                success: function(response) {
                    localStorage.setItem('maincms-'+values3, JSON.stringify(response))
                }
            })
        // }
        maincms = JSON.parse(localStorage.getItem('maincms-'+values3))
        if (maincms == null) {
            location.reload()
        }
    }
    
    function searchform() {
        $("#searchform").keyup(function(e) {
            if(e.keyCode == 13) {
                mytable.search($("#searchform").val()).draw('page');
            }
        })
    }

    function searchformdetail(params) {
        $("#searchformdetail-"+params.idelement).keyup(function(e) {
            if(e.keyCode == 13) {
                mytabtable[params.idelement].search($("#searchformdetail-"+params.idelement).val()).draw('page');
            }
        })
    }

    function searchformlog() {
        $("#searchformlog").keyup(function(e) {
            if(e.keyCode == 13) {
                mytablelog.search($("#searchformlog").val()).draw('page');
            }
        })
    }

    function searchformcostcontrol() {
        $("#searchformcostcontrol").keyup(function(e) {
            if(e.keyCode == 13) {
                mytablegeneratetemplate.search($("#searchformcostcontrol").val()).draw('page');
            }
        })
    }

    function searchtable() {
        if (searchtablewidth == 30) {
            searchtablewidth = 200
        } else {
            searchtablewidth = 30
        }
        $("#searchform").animate({ 
            width: searchtablewidth+'px' 
        }, 100); 
    }

    function searchtabledetail(params) {
        if (searchtablewidth == 30) {
            searchtablewidth = 200
        } else {
            searchtablewidth = 30
        }
        $("#searchformdetail-"+params.idelement).animate({ 
            width: searchtablewidth+'px' 
        }, 100); 
    }

    function searchtabledetailprev(params) {
        if (searchtablewidth == 30) {
            searchtablewidth = 200
        } else {
            searchtablewidth = 30
        }
        $("#searchformdetailprev-"+params.idelement).animate({ 
            width: searchtablewidth+'px' 
        }, 100); 
    }

    function searchtablelog() {
        if (searchtablewidth == 30) {
            searchtablewidth = 200
        } else {
            searchtablewidth = 30
        }
        $("#searchformlog").animate({ 
            width: searchtablewidth+'px' 
        }, 100); 
    }

    function searchtablegeneratetemplate() {
        if (searchtablewidth == 30) {
            searchtablewidth = 200
        } else {
            searchtablewidth = 30
        }
        $("#searchformgeneratetemplate").animate({ 
            width: searchtablewidth+'px' 
        }, 100); 
    }

    // function valuepage() {
    //     $("#valuepage").keyup(function(e) {
    //         if(e.keyCode == 13) {
    //             alert('masuk')
    //             mytable.page(3).draw('page');
    //         }
    //     })
    // }

    function prevpage() {
        mytable.page('previous').draw('page');
    }

    function prevpagedetail(params) {
        mytabtable[params.idelement].page('previous').draw('page');
    }

    function prevpagedetailprev() {
        mytabledetailprev.page('previous').draw('page');
    }

    function prevpagelog() {
        mytablelog.page('previous').draw('page');
    }

    function prevpagecostcontrol() {
        mytablegeneratetemplate.page('previous').draw('page');
    }

    function nextpage() {
        mytable.page('next').draw('page');
    }

    function nextpagedetail(params) {
        mytabtable[params.idelement].page('next').draw('page');
    }

    function nextpagedetailprev() {
        mytabledetailprev.page('next').draw('page');
    }

    function nextpagelog() {
        mytablelog.page('next').draw('page');
    }

    function nextpagecostcontrol() {
        mytablegeneratetemplate.page('next').draw('page');
    }

    function alengthmenu() {
        mytable.page.len($('#alengthmenu :selected').val()).draw();
    }

    function alengthmenudetail(params) {
        mytabtable[params.idelement].page.len($('#alengthmenudetail-'+params.idelement+' :selected').val()).draw();
    }

    function alengthmenudetailprev(params) {
        mytabledetailprev.page.len($('#alengthmenudetailprev :selected').val()).draw();
    }

    function alengthmenulog() {
        mytablelog.page.len($('#alengthmenulog :selected').val()).draw();
    }

    function alengthmenugeneratetemplate() {
        mytablegeneratetemplate.page.len($('#alengthmenugeneratetemplate :selected').val()).draw();
    }

    function getCmsTabDatatable() {
        var maincms_widgetgridfield = new Array();
        
        $.each(maincms['widgetgridfield'], function(k, v) {
            $.each(v, function(k2, v2) {
                maincms_widgetgridfield[v2.idcmswidgetgrid] = v;
            })
        })

        $.each(maincms['panel'], function(k, v) {
            if (k > 0) {
                // console.log('aku cek ini')
                // console.log(JSON.parse(localStorage.getItem('input-data-'+v.noid)))
                // return false
                // console.log('maincms_widgetgrid')
                // console.log(maincms_widgetgrid[v.noid])
                if (!maincms_widgetgrid[v.noid]['panelfilter']) {
                    $('#space_filter_button-'+v.noid).hide()
                    $('#tool_filter'+v.noid).hide()
                } else {
                    getFilterButton(values3, v.noid)
                }

                // console.log('idd_fix')
                // console.log(JSON.parse(localStorage.getItem('input-data-'+v.noid)))
                // return false
                console.log(localStorage.getItem('input-data-'+v.noid))
                $.each(maincms_widgetgridfield[v.idwidget], function(k2, v2) {
                    if (k2 == 0) {
                        fieldnametab[v.noid] = new Array();
                        tablelookuptab[v.noid] = new Array();
                    }
                        // alert(v3.idcmswidgetgrid+' '+v.idwidget)
                    if (v2.colshow == 1) {
                        if (v2.coltypefield == 96) {
                            fieldnametab[v.noid].push(v2.fieldname+'_lookup');
                        } else {
                            fieldnametab[v.noid].push(v2.fieldname);
                        }
                        tablelookuptab[v.noid].push(v2.tablelookup);
                    }
                })
                // console.log('cekkkk')
                // console.log(fieldnametab[v.noid])
                // tab_datatable.push(v.maintable);


                var columns = new Array();
                var columndefs = new Array();
                var index = 1;
                var noncolsorted = new Array();
                var noncolsearch = new Array();
                if (maincms['widgetgrid'][k]['colcheckbox']) {
                    // columndefs.push({"targets": [0,1,21],"orderable": false})
                    noncolsorted.push(0)
                    noncolsorted.push(1)
                    noncolsearch.push(0)
                    noncolsearch.push(1)
                    columns.push({"data": 0,"class": "text-left"})
                    columns.push({"data": 1,"class": "text-left"})
                    index = 2;
                } else {
                    noncolsorted.push(0)
                    noncolsearch.push(0)
                    columndefs.push({"targets": [0,20],"orderable": false})
                    columns.push({"data": 0,"class": "text-left"})
                }
                $.each(maincms['widgetgridfield'][k], function(k, v) {
                    if (k == 0) {
                        if (maincms['widgetgrid'][k]['colcheckbox']) {
                            columndefs.push({
                                "name": v.fieldsource,
                                "height": '60px',
                                "targets": index
                            })
                        }
                        columndefs.push({
                            "name": v.fieldsource,
                            "height": '60px',
                            "targets": index
                        })
                    }
                    if (v.colshow) {
                        if (v.colwidth != 0) {
                            var colwidth = v.colwidth+'px';
                        } else {
                            var colwidth = '100px';
                        }
                        if (!v.colsorted) {
                            noncolsorted.push(index)
                        }
                        if (!v.colsearch) {
                            noncolsearch.push(index)
                        }
                        columns.push({
                            "data": index,
                            "width": colwidth,
                            "class": "text-"+v.colalign.toLowerCase()
                        })
                        columndefs.push({
                            "name": v.fieldsource,
                            "height": '60px',
                            "targets": index
                        })
                        index++
                    }
                })

                noncolsorted.push(index)
                noncolsearch.push(index)
                columndefs.push({"targets": noncolsorted,"orderable": false})
                columndefs.push({"targets": noncolsearch,"searchable": false})
                columns.push({"data": index,"class": "text-left"})
                columndefs.push({"height": '60px',"targets": index})
                console.log('columndefstabs')
                console.log(columns)
                // console.log(maincms['widgetgrid'][0].recperpage)
                // console.log('columndefs')
                // console.log(columndefs)
                // return false

                // alert('#tab-table-'+v.noid)
                my_tab_table = $('#tab-table-'+v.noid).DataTable({
                    "autoWidth": false,
                    "pageLength": maincms['widgetgrid'][k].recperpage,
                    "searching": true,
                    "processing": true,
                    "serverSide": true,
                    "bInfo": true,
                    "bFilter": false,
                    "bLengthChange": false,
                    "columns": columns,
                    "columnDefs": columndefs,
                    // "dom": '<"top dtfiler"<"">>',
                    // "dom": '<"info1 top dtfiler"pli<""f><"clear">>rt ',
                    // "dom": '<lf<t>ip>',
                    "infoCallback": function( settings, start, end, max, total, pre ) {
                        var options = '';
                        $.each(settings.aLengthMenu, function(k, v) {
                            options += '<option value="'+v+'" id="alm'+v+'" class="alm">'+v+'</option>'
                        })
                        if ((end-start) == settings._iDisplayLength-1) {
                            var valuepage = end/settings._iDisplayLength;
                        } else {
                            var valuepage = ((start-1)/settings._iDisplayLength)+1;
                        }
                        if (start == 1) {
                            var prevdisabled = 'disabled'
                        } else {
                            var prevdisabled = '';
                        }
                        if (end == total) {
                            var nextdisabled = 'disabled'
                        } else {
                            var nextdisabled = '';
                        }
                        // if (settings.oPreviousSearch.sSearch == $('#searchform').val()) {
                        //     alert($('#searchform').val())
                        //     var valuesearch = $('#searchform').val()
                        // } else {
                        //     var valuesearch = $('#searchform').val()
                        //     alert('else'+$('#searchform').val())
                        // }
                        if (settings.oPreviousSearch.sSearch != '') {
                            tab_statussearch[v.noid] = true
                            tab_searchtablewidth[v.noid] = 200
                            tab_valuesearch[v.noid] = settings.oPreviousSearch.sSearch
                        } else {
                            tab_statussearch[v.noid] = false
                            tab_searchtablewidth[v.noid] = 30
                            tab_valuesearch[v.noid] = ''
                        }
                        $('#tab-appendInfoDatatable-'+v.noid).html(`
                        <div class="row" style="background: #90CAF9; padding-left: 15px; padding-right: 15px;">
                            <div class="col-md-12">
                                Page
                                <button id="prevpage" onclick="prevpage(`+v.noid+`)" class="btn btn-sm default prev `+prevdisabled+`" style="background-color: #2196F3;">
                                    <i class="fa fa-angle-left">
                                    </i>
                                </button>
                                <input type="text" id="valuepage" onkeyup="valuepage()" class="input-mini" value="`+valuepage+`" style="text-align: center; border: 0px; margin: 5px 5px 5px 5px">
                                <button id="nextpage" onclick="nextpage(`+v.noid+`)" class="btn btn-sm default next `+nextdisabled+`" style="background-color: #2196F3;">
                                    <i class="fa fa-angle-right">
                                    </i>
                                </button> of <span class="pagination-panel-total">`+(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)+`</span>
                                <span class="seperator">|</span>
                                Rec/Page 
                                <select id="alengthmenu-`+v.noid+`" onchange="alengthmenu(`+v.noid+`)" name="table_master_length" aria-controls="table_master" class="input-inline input-xsmall input-sm recofdt" style="border: 0px; margin: 5px 5px 5px 5px">
                                    `+options+`
                                </select>
                                <span class="seperator">|</span>Total `+settings._iRecordsTotal+` records
                                <input id="searchform-`+v.noid+`" onkeyup="searchform(`+v.noid+`)" value="`+tab_valuesearch[v.noid]+`" type="search" class="form-control input-inline input-sm clicked" placeholder="" aria-controls="table_master" style="width: `+tab_searchtablewidth[v.noid]+`px; position: absolute; right: 0px; top: 4px; height: 80%; border: 0;">
                                <div onclick="searchtable(`+v.noid+`)" class="btn fa fa-search text-right" style="position: absolute; right: 0px; top: 4px; height: 80%; background: transparent; margin: 0px 0px 0px 0px; padding: 5px 0px 5px 0px">
                                    <i class="fa fa-search"></i>
                                </div>
                            </div>
                        </div>`)
                        console.log('settings '+v.noid)
                        console.log(settings)
                        console.log(parseInt(settings._iRecordsTotal/settings._iDisplayLength)+1)
                        $('.alm').removeAttr('selected');
                        $('#alm'+settings._iDisplayLength).attr('selected', 'selected');
                        // console.log(valuepage)
                        $('.dataTables_info').css('display', 'none')
                        $('.dataTables_paginate').css('display', 'none')
                        // return '<"top dtfiler"<"">>';
                    },
                    "ajax": {
                        "url": "/get_tab_data_tmd",
                        "type": "POST",
                        "dataType":'json',
                        "data": {
                            "_token":$('#token').val(),
                            key: k-1,
                            id: v.idwidget,
                            table: maintable[v.idwidget],
                            selected_filter: selected_filter,
                            maincms: JSON.stringify(maincms),
                            data: localStorage.getItem('input-data-'+v.noid),
                            tablename: localStorage.getItem('tablename-'+v.noid),
                            panelnoid: v.noid,
                            masternoid: nownoid,
                            fieldnametab: fieldnametab[v.noid],
                            tablelookuptab: tablelookuptab[v.noid],
                            statusadd: statusadd,
                        },
                        beforeSend: function () {
                            $(".dataTables_scroll").LoadingOverlay("show");
                        },
                        complete: function (response) {
                            $(".dataTables_scroll").LoadingOverlay("hide");
                            
                            if (response.responseJSON.noids_tab[v.noid].length > 0) {
                                noids_tab[v.noid] = response.responseJSON.noids_tab[v.noid];
                                select_all_tab[v.noid] = response.responseJSON.select_all_tab[v.noid];
                            }
                            // alert('asd')
                            // $.each(maincms['widgetgridfield'][0], function(k, v) {
                            //     $('#example_baru th:nth-child('+k+')').attr('style', 'padding: 0px 5px 0px 5px; width: '+v.colwidth+'px;')
                            //     $('#example_baru td:nth-child('+k+')').attr('style', 'padding: 0px 5px 0px 5px; width: '+v.colwidth+'px;')
                            //     alert(document.querySelector('.dataTables_scrollHeadInner #thead-columns th:nth-child('+k+')').getAttribute('style'))
                            // })
                            // $('#example_baru .sorting_1').attr('style', 'width: 103px')
                        },
                    },
                    "pagingType": "input",
                    "createdRow": function(row, data, dataIndex) {
                        // console.log(data)
                        $(row).addClass('my_row');
                        $(row).addClass('row'+dataIndex);
                        
                        console.log('hwgf')
                        $(row).css('height', '35px');
                        $(row).attr('onmouseover', 'showButtonActionTab(this, '+dataIndex+')');
                        // alert(dataindex)
                        $(row).attr('onmouseout', 'resetCssTab()');
                        // $(row).attr('onmouseover', 'getPositionXY(this)');
                    },
                    "oLanguage":{
                        "loadingRecords": "Please wait - loading...",
                        "sProcessing": "<div id='loadernya'></div>",
                        "sInfo": "&nbsp;|&nbsp;Total&nbsp;_TOTAL_ records",
                        "sLengthMenu": "&nbsp;|&nbsp;Rec/Page&nbsp;_MENU_",
                        "sInfoFiltered": "",
                        "sInfoEmpty": "| No records found to show",
                        "sEmptyTable": "No data available in table",
                        "sZeroRecords": "No matching records found",
                        "sSearch": "<button onclick='toggleButtonSearch()' class='btn flat dtfilter_search_btn' style='background-color: white'><i class='fa fa-search'></i></button>",
                        // "oPaginate": {
                        //   "previous": "<i class='fa fa-angle-left'></i>",
                        //   "next": "<i class='fa fa-angle-right'></i>",
                        //   "page": "Page",
                        //   "pageOf": "of"
                        // },
                        "oPaginate": {
                            "sPrevious": "<i class='fa fa-angle-left'></i>",
                            "sNext": "<i class='fa fa-angle-right'></i> ",
                        // "sPage": "<i id='page_prev'>Page</i>",
                        // "sPageOf": "<span id='page_next'> of </span>"
                        }
                    },
                    footerCallback: function ( row, data, start, end, display ) {
                        var api = this.api(), data;
                        var intVal = function ( i ) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '')*1 :
                                typeof i === 'number' ?
                                    i : 0;
                        };
                        // var _split = function ( i ) {
                        //     return typeof i === 'string' ?
                        //         parseInt(i.replace('.', '')).toString() :
                        //         typeof i === 'number' ?
                        //             i : 0;
                        // };
                        var count = function ( i ) {
                            return api.column( 5 )
                                    .data()
                                    .reduce( function (a, b) {
                                        return intVal(end);
                                    }, 0 )
                        }
                        var sum = function ( i ) {
                            return i
                            var value = 0;
                            $.each(api.column( i ).data(), function(k, v) {
                                var _split = v.split('.');
                                var value2 = '';
                                $.each(_split, function(k2, v2) {
                                    value2 += v2
                                })
                                value += Number(value2)
                            })
                            return value
                        }
                        var addcommas = function addCommas(nStr) {
                            nStr += '';
                            var x = nStr.split('.');
                            var x1 = x[0];
                            var x2 = x.length > 1 ? '.' + x[1] : '';
                            var rgx = /(\d+)(\d{3})/;
                            while (rgx.test(x1)) {
                                x1 = x1.replace(rgx, '$1' + '.' + '$2');
                            }
                            return x1 + x2;
                        }

                        $.each(functionfootertab[v.noid], function(k, v) {
                            $( api.column(v.index).footer() ).html(api.column(v.index).data().reduce( function (a, b) { return addcommas(eval(v.function)) }, 0 ));
                        })
                        // console.log('try')
                        // console.log(row)
                        console.log('functionfootertab')
                        console.log(functionfootertab)
                    }
                });

                mytabtable[v.noid] = my_tab_table

                // my_tab_table = $('#tab-table-'+v.noid).DataTable({
                //     "scrollX": true,
                //     "processing": true,
                //     "serverSide": true,
                //     "info": false,
                //     // "order": [[ 0, "asc" ]],
                //     "columnDefs": [ 
                //         { 
                //             orderable: false,
                //             targets: colsorted
                //             // targets: [0,1,30]
                //         }
                //     ],
                //     "pagingType": "input",
                //     "searching": false,
                //     "ajax": {
                //         "url": "/get_tab_data_tmd",
                //         "type": "POST",
                //         "dataType":'json',
                //         "data": {
                //             "_token":$('#token').val(),
                //             key: k-1,
                //             id: v.idwidget,
                //             table: 'fincashbankdetail',
                //             selected_filter: selected_filter,
                //             maincms: JSON.stringify(maincms),
                //             data: localStorage.getItem('input-data-'+v.noid),
                //             tablename: localStorage.getItem('tablename-'+v.noid),
                //             panelnoid: v.noid,
                //             masternoid: nownoid,
                //             fieldnametab: fieldnametab[v.noid],
                //             statusadd: statusadd,
                //         },
                //         beforeSend: function () {
                //             $(".dataTables_scroll").LoadingOverlay("show");
                //         },
                //         complete: function () {
                //             $(".dataTables_scroll").LoadingOverlay("hide");
                //         },
                //     },
                //     // "sPaginationType":"input",
                //     "dom": '<"top dtfiler"pli<""f><"clear">>rt ',
                //     "oLanguage":{
                //         "loadingRecords": "Please wait - loading...",
                //         "sProcessing": "<div id='loadernya'></div>",
                //         "sInfo": "&nbsp;|&nbsp;Total&nbsp;_TOTAL_ records",
                //         "sLengthMenu": "&nbsp;|&nbsp;Rec/Page&nbsp;_MENU_",
                //         "sInfoFiltered": "",
                //         "sInfoEmpty": "| No records found to show",
                //         "sEmptyTable": "No data available in table",
                //         "sZeroRecords": "No matching records found",
                //         "sSearch": `<button onclick="toggleButtonSearchTab('`+v.noid+`')" class="btn flat dtfilter_search_btn" style="background-color: white"><i class="fa fa-search"></i></button>`,
                //         // "oPaginate": {
                //         //   "previous": "<i class='fa fa-angle-left'></i>",
                //         //   "next": "<i class='fa fa-angle-right'></i>",
                //         //   "page": "Page",
                //         //   "pageOf": "of"
                //         // },
                //         "oPaginate": {
                //             "sPrevious": "<i class='fa fa-angle-left'></i>",
                //             "sNext": "<i class='fa fa-angle-right'></i> ",
                //         // "sPage": "<i id='page_prev'>Page</i>",
                //         // "sPageOf": "<span id='page_next'> of </span>"
                //         }
                //     },
                //     "createdRow": function(row, data, dataIndex) {
                //         // console.log(data)
                //         $(row).addClass('my_row');
                //         $(row).addClass('row'+dataIndex);
                //         // $(row).css('height', '5px');
                //         $(row).attr('onmouseover', 'showButtonActionTab(this, '+dataIndex+')');
                //         $(row).attr('onmouseout', 'resetCssTab()');
                //         // $(row).attr('onmouseover', 'getPositionXY(this)');
                //     },
                //     deferRender: true,
                //     // footerCallback: function ( row, data, start, end, display ) {
                //     //     var api = this.api(), data;
                //     //     var intVal = function ( i ) {
                //     //         return typeof i === 'string' ?
                //     //             i.replace(/[\$,]/g, '')*1 :
                //     //             typeof i === 'number' ?
                //     //                 i : 0;
                //     //     };
                //     //     var monTotal = api.column( 1 )
                //     //         .data()
                //     //         .reduce( function (a, b) {
                //     //             return intVal(a) + intVal(b);
                //     //         }, 0 );

                //     //     $( api.column( 3 ).footer() ).html(monTotal);
                //     // }
                // });
                
                new $.fn.dataTable.Buttons( my_tab_table, {
                    buttons: [
                        {
                            autoFilter: true,
                            text:      '<i class="fa fa-trash"></i> Delete Selected',
                            titleAttr: 'Delete Selected',
                            className: ' dropdown-item flat',
                            action:  function(e, dt, button, config) {
                                if (another_selecteds[v.noid].length == 0) {
                                    alert("There's no selected")
                                    // console.log(another_selecteds)
                                } else {
                                        // alert(another_selecteds)
                                        // alert(v.maintable)
                                    if (confirm('Are you sure you want to delete this?')) {
                                        removeSelectedTab(another_selecteds[v.noid])
                                    }
                                }
                            },
                        },{
                            autoFilter: true,
                            text:      '<i class="fa fa-trash"></i> Delete All',
                            titleAttr: 'Delete All',
                            className: ' dropdown-item flat',
                            action:  function(e, dt, button, config) {
                                if (noids_tab[v.noid].length == 0) {
                                    alert("There's no selected")
                                } else {
                                    if (confirm('Are you sure you want to delete this?')) {
                                        removeSelectedTab(noids_tab[v.noid])
                                    }
                                }
                            },
                        },{
                            extend:    'excel',
                            autoFilter: true,
                            text:      '<i class="fa fa-file-excel"></i> Export to Excel',
                            titleAttr: 'Export to Excel',
                            title: v.widgetcaption,
                            className: ' dropdown-item flat',
                            exportOptions: {
                                modifier: {
                                    search: 'applied',
                                    order: 'applied'
                                },
                                columns: [ 1,2,3, ':visible' ]
                                //                  <a style="font-size: 14px;min-width: 175px;
                                // text-align: left;" href="javascript:;"  class="dropdown-item">
                                // <i style="margin-top:10px;margin-right: 5px;" class="fa fa-file-excel"></i> Export to Excel </a>
                                                // columns: [ 0, ':visible' ]
                            },
                            action:  function(e, dt, button, config) {
                            // $('.loading').fadeIn();
                                var that = this;
                                setTimeout(function () {
                                    $.fn.dataTable.ext.buttons.excelHtml5.action.call(that,e, dt, button, config);
                                    // console.log(that);
                                    // console.log(e);
                                    // console.log(dt);
                                    // console.log(button);
                                    // console.log(config);
                                    $('.loading').fadeOut();
                                },500);
                            },
                        }
                    ]
                    // infoDatatable()
                });
                // infoDatatable()
                $('#tab-table-'+v.noid+'_first').hide();
                $('#tab-table-'+v.noid+'_last').hide();
                
                my_tab_table.buttons().container().appendTo('#another-space-button-export-'+v.noid);

                getForm(statusadd, v.idwidget, 'tab-form-div-'+v.noid);

                // $('#'+element_id).LoadingOverlay("hide");
                $('#another-card').show();
            }
        })

        if (!statusadd) {
            // $('#another-card').show();
        }
    }

    function destroyDatatable() {
        // $('#example_baru').DataTable().destroy();
        $('#table_master').DataTable().destroy();
    }

    function destroyTabDatatable() {
        $.each(maincms['panel'], function(k, v) {
            if (k > 0) {
                $('#tab-table-'+v.noid).DataTable().destroy();
            }
        })
    }
// 

    function toggleButtonCollapse(noid) {
        if (noid) {
            $('#tab-widget-content-'+noid+' .card .card-body').toggleClass('collapse');
        } else {
            $('#kt_content .container .card .card-body').toggleClass('collapse');
        }
        // $('#kt_content .container .card .card-body').toggleClass('collapse').delay('slow').fadeOut();
    }

    function toggleButtonSearch() {
        $('#example_baru_filter label input').addClass('form-control flat')
        $('#example_baru_filter label input').toggleClass('clicked');
    }

    function toggleButtonSearchTab(element_id) {
        $('#tab-table-'+element_id+'_filter label input').addClass('form-control flat')
        $('#tab-table-'+element_id+'_filter label input').toggleClass('clicked');
    }

    function filterData(params) {
        // if (is_all) {
        //     selectedfilter[fieldname] = null;
        // } else {
        //     selectedfilter[fieldname] = value;
        // }

        selectedfilter['is'] = params.noid;
        
        $(`.${params.class}`).css('background', '#E3F2FD');
        $(`.${params.class}`).css('color', 'grey');
        $(`.${params.class}`).css('font-weight', 'normal');
        $(`#${params.class}-${params.noid}`).css('background', '#90CAF9');
        $(`#${params.class}-${params.noid}`).css('color', 'black');
        $(`#${params.class}-${params.noid}`).css('font-weight', 'bold');

        // if (groupdropdown == 1) {
        //     $('#filter-dropdown-active-'+key).text(buttoncaption)
        //     // alert(buttoncaption)
        // } else {
        //     $('.'+element_class).css('background', '#E3F2FD');
        //     $('.'+element_class).css('color', classcolor);
        //     $('.'+element_class).css('font-weight', 'normal');
        //     $('#'+element_id).css('background', '#90CAF9');
        //     $('#'+element_id).css('color', 'black');
        //     $('#'+element_id).css('font-weight', 'bold');
        // }
        
        getDatatable();
    }

    function getSlider(id) {
        var owl = $('.owl-carousel').owlCarousel({
            loop:true,
            smartSpeed: 100,
            autoplay: true,
            autoplaySpeed: 100,
            mouseDrag: false,
            margin:10,
            animateIn: 'slideInUp',
            animateOut: 'fadeOut',
            nav:false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });

        $.ajax({
            type: "POST",
            header:{
            'X-CSRF-TOKEN':$('#token').val()
            },
            url: "/get_slide",
            dataType: "json",
            data:{
                "_token":$('#token').val(),
                "id_slider": id
            },
            success: function (response) {
                // console.log(response.hasil);
                var base_url = '{{ url('/') }}';
                if(isNaN(base_url)) {
                    base_url = '';
                }
                var name1 = '';
                var name2 = '';

                $.each(response.hasil, function(i, item) {
                    // console.log(item.mainimage);
                    name1 = 'public/'+item.mainimage;

                    // console.log(name1);
                    var random_num =  Math.floor(Math.random()*60);
                    owl.trigger('add.owl.carousel', [jQuery('<div class="notification-message"> <img src="'+base_url+name1+'" class="user-image" alt=""> <div class="user-name">'+ item.newstitle+' <span class="lighter">from '+item.idnews+'</span></div> <div class="bought-details">Bought This <br>'+random_num+' minutes ago</div> </div>')]);
                });
                owl.trigger('refresh.owl.carousel');
            },
        });
    }

    function chartBatang(response,id_cart) {
        var dataatas = response.hasil;
        var dataProvideratas = [];// this variable you have to pass in dataProvider inside chart
        for(var key in dataatas) {
            dataProvideratas.push({
                category: dataatas[key]['categories'],
                visits: dataatas[key]['jumlahdata'],
            });
        }
        var chart = AmCharts.makeChart("kt_amcharts_"+id_cart, {
            "rtl": KTUtil.isRTL(),
            "type": "serial",
            "hideCredits":true,
            "theme": "light",
            "dataProvider": dataProvideratas,
            "valueAxes": [{
                "gridColor": "#FFFFFF",
                "gridAlpha": 0.2,
                "dashLength": 0
            }],
            "series": [{
                "type": "LineSeries",
                "stroke": "#ff0000",
                "strokeWidth": 3
            }],
            "valueAxes": [ {
                "position": "left",
                "title": "Jumlah Data"
            }, {
                "position": "bottom",
                "title": response.widget.widget_widgetcaption
            } ],
            "legend": {
                "useGraphSettings": true
            },
            "titles": [
                {
                    "size": 15,
                    "text":response.widget.widget_widgetcaption
                },
                {
                    "text": response.widget.widget_widgetsubcaption,
                    "bold": false
                }
            ],
            "gridAboveGraphs": true,
            "startDuration": 1,
            "graphs": [{
                "balloonText": "[[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "visits",
                "title": response.widget.widget_widgetcaption
            }],
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "category",
            "categoryAxis": {
                "gridPosition": "start",
            },
            "export": {
                "enabled": true
            }
        });
    }

    function chartPie(response,id_cart) {
        var data = response.hasil;
        var dataProvider = [];// this variable you have to pass in dataProvider inside chart
        for(var key in data) {
            dataProvider.push({
            country: data[key]['messagestatus'],
            litres: data[key]['jumlahdata'],
            });
        }

        var chart = AmCharts.makeChart("kt_amcharts_"+id_cart,{
            "type"    : "pie",
            "hideCredits"    : true,
            "theme"    : "light",
            "addClassNames": true,
            "titles": [
                {
                    "size": 15,
                    "text": response.widget.widget_widgetcaption
                },
                {
                    "text": response.widget.widget_widgetsubcaption,
                    "bold": false
                }
            ],
            // "pullOutDuration": 0,
            // "pullOutRadius": 0,

            "labelsEnabled": false,
            "titleField"  : "country",
            "valueField"  : "litres",
            "dataProvider"  : dataProvider,
            "numberFormatter": {
                "precision": -1,
                "decimalSeparator": ",",
                "thousandsSeparator": ""
            },
            "legend": {
                "align": "center",
                "position": "bottom",
                "marginRight": 0,
                "labelText": "[[title]]",
                "valueText": "",
                "valueWidth": 50,
                "textClickEnabled": true
            },
            "export": {
                "enabled": true
            }

        });
    }

    function setCheckbox(element_id) {
        if ($('#'+element_id).attr('checked')) {
            $('#'+element_id).val(0);
        } else {
            $('#'+element_id).val(1);
        }
    }

    $("#savedata__").click(function(e){
        e.preventDefault();
        // alert(nextnoid)
        // return false
        // localStorage.setItem('input-data', response)
        // console.log($("#formdepartement").serializeArray())
        var input_data_master = {};
        var mstrtbl = maincms['widget'][0].maintable;
        var onrequired = false;
        var errormessage = '';
        $.each($("#formdepartement").serializeArray(), function(k, v) {
            var formcaption = $('#'+mstrtbl+'-'+v.name).attr('data-'+mstrtbl+'-formcaption');
            if (v.name == 'noid') {
                input_data_master[v.name] = nextnoid
            } else {
                input_data_master[v.name] = v.value

                if (statusadd) {
                    if ($('#'+mstrtbl+'-'+v.name).attr('data-'+mstrtbl+'-newreq') == 1) {
                        if (v.value == '') {
                            onrequired = true;
                            errormessage += formcaption+' is required!<br>'
                        }
                    }
                } else {
                    if ($('#'+mstrtbl+'-'+v.name).attr('data-'+mstrtbl+'-editreq') == 1) {
                        if (v.value == '') {
                            onrequired = true;
                            errormessage += formcaption+' is required!<br>'
                        }
                    }
                }
            }
        })
        
        if (onrequired) {
            $.notify({
                message: errormessage
            },{
                type: 'danger'
            });
            return false;
        }


        if (!statusadd) {
            input_data_master['noid'] = nownoid;
        }
        
        localStorage.setItem('input-data', JSON.stringify(input_data_master))

        // var maincms_widget = new Array();
        // $.each(maincms['widget'], function(k, v) {
        //     maincms_widget[v.noid] = v;
        // })

        var input_data_detail = {};
        $.each(maincms['panel'], function(k, v) {
            if (k > 0) {
                input_data_detail[v.noid] = {
                    'data': JSON.parse(localStorage.getItem('input-data-'+v.noid)),
                    'table': maincms_widget[v.idwidget].maintable
                }
            }
        })
        // console.log(JSON.stringify(maincms))
        // console.log(input_data_detail)
        // return false
        
        /////////////////////////////////

        var kode = $("#kode").val();
        var nama = $("#nama").val();
        var namatable =$('#namatable').val();
        var formdata = $("#formdepartement").serialize();
        var formdatatab = [];
        
        if (statusadd) {
            $.each(maincms['widget'], function(k, v) {
                if (k > 0) {
                    formdatatab.push($('#form-'+v.maintable).serialize())
                }
            })
        }
        // console.log('input_data_detail')
        // console.log(input_data_detail)
        // return false

        $.ajax({
            type: "POST",
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            url: "/save_tmd",
            dataType: "json",
            data:{
                "_token":$('#token').val(),
                "data" :{
                    'data' : formdata,
                    'datatab' : formdatatab,
                    'tabel': namatable,
                },
                "table": namatable,
                "editid": editid,
                "statusadd": statusadd,
                "masternoid": nownoid,
                maincms: JSON.stringify(maincms),
                "input_data": {
                    "master": input_data_master,
                    "detail": JSON.stringify(input_data_detail),
                }
            },
            success:function(result){
                // return false
                if (result.status == 'add') {
                    // $("#notiftext").text('Data  Berhasil di Tambah');
                    $.notify({
                        message: 'Add data has been succesfullly.' 
                    },{
                        type: 'success'
                    });
                } else {
                    // $("#notiftext").text('Data  Berhasil di Edit');
                    $.notify({
                        message: 'Edit data has been succesfullly.' 
                    },{
                        type: 'success'
                    });
                }

                //
                $('#another-card').hide();
                divform = '';
                another_selecteds = [];
                destroyTabDatatable()

                document.getElementById("formdepartement").reset();
                resetData()
                refresh();
                // $('#divsucces').show(0).delay(5000).hide(0);
                var x = document.getElementById("from-departement");
                var x2 = document.getElementById("tabledepartemen");
                var y = document.getElementById("tool-kedua");
                var y2 = document.getElementById("tool-pertama");
                x.style.display = "none";
                x2.style.display = "block";
                y.style.display = "none";
                y2.style.display = "block";
            }
        });
    });

    function refresh(noid) {
        if (noid) {
            $('#tab-table-'+noid).DataTable().ajax.reload();
        } else {
            $('#table_master').DataTable().ajax.reload();
        }
    }

    function refreshTab() {
        destroyTabDatatable()
        getCmsTabDatatable()
    }

    function changeInput() {
        // console.log($('#formdepartement').serialize())
    }

    function getNextNoid() {
        $.ajax({
            url: '/get_next_noid_tmd',
            type: "POST",
            dataType: 'json',
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            data: {
                "table" : maincms['widget'][0]['maintable'],
                "_token": $('#token').val(),
                maincms: JSON.stringify(maincms)
            },
            success: function (response) {
                nextnoid = response
                localStorage.setItem('input-data-noid', response)
                // console.log(localStorage.getItem('input-data-noid'))
                // alert(nextnoid)
            }
        })
    }

    function getNextNoidTab(tablename, panel_noid) {
        $.ajax({
            url: '/get_next_noid_tmd',
            type: "POST",
            dataType: 'json',
            header:{
                'X-CSRF-TOKEN':$('#token').val()
            },
            data: {
                "table" : tablename,
                "_token": $('#token').val(),
                maincms: JSON.stringify(maincms)
            },
            success: function (response) {
                nextnoidtab[panel_noid] = response
                // console.log(nextnoidtab[panel_noid])
            }
        })
    }

    function cancelDataTab(panel_noid) {
        $('#form-detail_').hide();

        $('#tab-form-'+panel_noid).hide()
        $('#add-data-'+panel_noid).show()
        $('#save-data-'+panel_noid).hide()
        $('#cancel-data-'+panel_noid).hide()
        $('#tab-table-'+panel_noid+'_wrapper').show()
        $('#tab-appendInfoDatatable-'+panel_noid).show()
        $('#ts-'+panel_noid).show()
        $('#space_filter_button-'+panel_noid).show();
        $('#tool_refresh-'+panel_noid).show();
        $('#tool_filter-'+panel_noid).show()
        $('#tool_refresh_up-'+panel_noid).show();
    }

    function addDataTab(panel_noid) {
        console.log(nextnoidtab)
        resetDataTab(panel_noid)
        statusaddtab[panel_noid] = true;

        $('#form-detail_').show();
        $('#tab-form-'+panel_noid).show()
        $('#add-data-'+panel_noid).hide()
        $('#save-data-'+panel_noid).show()
        $('#cancel-data-'+panel_noid).show()
        $('#tab-table-'+panel_noid+'_wrapper').hide()
        $('#tab-appendInfoDatatable-'+panel_noid).hide()
        $('#ts-'+panel_noid).hide()
        $('#space_filter_button-'+panel_noid).hide();
        $('#tool_refresh-'+panel_noid).hide();
        $('#tool_filter-'+panel_noid).hide()
        $('#tool_refresh_up-'+panel_noid).hide();
    }

    const addData = () => {
        status.crudmaster = 'create';
        status.cruddetail = 'read';
        statusadd = true;
        statusdetail = false;
        
        // $('#iddmslogo').show() // Show button add file
        // setTab('tab-general', 'Header') // Set default tab
        // unsetFormMilestoneAndHeader() // Unset milestone dan header
        // conditionDetail(); // Check kondisi detail
        // getInfo(); // Get data info
        // customDocumentForm(); // Build CDF

        // Unfocus
        $('.select2-container-active').removeClass('select2-container-active');
        $(':focus').blur();

        // Focus
        $('#kode').focus();

        // Show save
        $('#btn-save-data').show();


        $('#tools-dropdown').hide(); // Hide button tools
        $('#tabledepartemen').hide(); // Hide datatable
        $('#tool-pertama').hide(); // Show buton add data
        $('#space_filter_button').hide(); // Hide filter di datatable
        $('#tool_refresh').hide(); // Hide button refresh
        $('#from-departement').show(); // Show form
        $('#tool-kedua').show(); // Show button cancel
    }

    //RULES MILESTONE
    $('#kode').on('keydown',function(e){if(e.keyCode==13){$('#barcode').focus();}})
    $('#barcode').on('keydown',function(e){if(e.keyCode==13){$('#barcode2d').focus();}})
    $('#barcode2d').on('keydown',function(e){if(e.keyCode==13){$('#nama').focus();}})
    $('#nama').on('keydown',function(e){if(e.keyCode==13){$('#namaalias').focus();}})
    $('#namaalias').on('keydown',function(e){if(e.keyCode==13){$('#keterangan').focus();}})
    $('#keterangan').on('keydown',function(e){if(e.keyCode==13){$('#idinvmgroup').select2('focus');}})
    $('#idinvmgroup').on('select2:close',function(e){$('#idinvmkategori').select2('focus')})
    $('#idinvmkategori').on('select2:close',function(e){$('#idsatuan').select2('focus')})
    $('#idsatuan').on('select2:close',function(e){$('#konversi').focus()})
    $('#konversi').on('keydown',function(e){if(e.keyCode==13){$('#hpp').focus();}})
    $('#hpp').on('keydown',function(e){if(e.keyCode==13){$('#salesprice').focus();}})
    $('#salesprice').on('keydown',function(e){if(e.keyCode==13){$('#purchaseprice').focus();}})
    $('#purchaseprice').on('keydown',function(e){if(e.keyCode==13){$('#istaxed').focus();}})

    $('#form-dmilestone #milestone-nama').on('keydown',function(e){if(e.keyCode==13){$('#form-dmilestone #milestone-keterangan').focus();}})
    $('#form-dmilestone #milestone-idcardpj').on('select2:close',function(e){$('#form-dmilestone #milestone-idcardpjwakil').select2('focus')})
    $('#form-dmilestone #milestone-idcardpjwakil').on('select2:close',function(e){$('#form-dmilestone #milestone-tanggalstart').focus()})
    $('#form-dmilestone #milestone-tanggalstart').on('change',function(e){
        $('#form-dmilestone #milestone-tanggalstop').focus();
    });
    $('#form-dmilestone #milestone-tanggalstop').on('change',function(e){
        $('#form-dmilestone #milestone-tanggalfinished').focus();
    });
    $('#form-dmilestone #milestone-tanggalfinished').on('change',function(e){
        $('#btn-save-milestone').focus();
    });

    //#1
    $('#milestone-tanggalstart').on('change', function(){
        let _tanggal = new Date($('#form-header #tanggal').val());
        let _tanggaldue = new Date($('#form-header #tanggaldue').val());
        let _start = new Date($('#milestone-tanggalstart').val());
        let _stop = new Date($('#milestone-tanggalstop').val());
        let _issuccess = true;
        let _message = '';
        if (_start < _tanggal) {_message = "'Tanggal Start' harus lebih besar dari 'Tanggal'"; _issuccess = false;}
        if (_start > _tanggaldue) {_message = "'Tanggal Start' harus lebih kecil dari 'Tanggal Due'"; _issuccess = false;}
        if (_start > _stop) {_message = "'Tanggal Start' harus lebih kecil dari 'Tanggal Stop'"; _issuccess = false;}
        if (!_issuccess) {$('#milestone-tanggalstart').val('');$.notify({message: _message},{type: 'danger'});return false;}
    })
    $('#milestone-tanggalstop').on('change', function(){
        let _tanggal = new Date($('#form-header #tanggal').val());
        let _tanggaldue = new Date($('#form-header #tanggaldue').val());
        let _start = new Date($('#milestone-tanggalstart').val());
        let _stop = new Date($('#milestone-tanggalstop').val());
        let _issuccess = true;
        let _message = '';
        if (_stop < _tanggal) {_message = "'Tanggal Stop' harus lebih besar dari 'Tanggal'"; _issuccess = false;}
        if (_stop > _tanggaldue) {_message = "'Tanggal Stop' harus lebih kecil dari 'Tanggal Due'"; _issuccess = false;}
        if (_stop < _start) {_message = "'Tanggal Stop' harus lebih besar dari 'Tanggal Start'"; _issuccess = false;}
        if (!_issuccess) {$('#milestone-tanggalstop').val('');$.notify({message: _message},{type: 'danger'});return false;}
    })

    function htmlHeader(params) {
        return `<div class="row">
            <div class="col-md-12" style="padding: 0px !important">
                <div class="card" id="card-${params.v2.idelement}" style="margin: 0px !important; border-radius: 0.42rem !important; border: 1px solid #E3A6BF;">
                    <div class="card-header" style="padding: 5px; background: #E3A6BF; border-radius: 0.32rem 0.32rem 0rem 0rem !important;">
                        <div class="row">                                        
                            <div class="col-md-6">
                                <b id="title-${params.v2.idelement}" style="font-size: 13px; padding-top: 6px" onclick="collapseHeader({idelement:'${params.v2.idelement}',idmaster:${params.v2.idmaster},idheader:${params.v2.noid}})"><i class="fa fa-angle-right text-light"></i> ${params.v2.nourut}. ${params.v2.nama}_(PJ=${params.v2.idcardpj_nama},${params.v2.tanggalstart} s/d ${params.v2.tanggalstop})</b>
                            </div>
                            <div class="col-md-3">
                                <input type="text" name="searchprev" id="searchprev-${params.v2.idelement}" class="form-control" placeholder="Search to add detail">
                            </div>
                            <div class="col-md-3 text-right">
                                <button type="button" id="btn-cancel-${params.v2.idelement}" class="btn btn-default btn-sm btn-icon c-rounded" onclick="cancelHeader({idelement:'${params.v2.idelement}'})" style="display:none"><i class="fa fa-arrow-left"></i></button>
                                <button type="button" id="btn-save-${params.v2.idelement}" class="btn btn-success btn-sm btn-icon c-rounded" onclick="saveHeader({type:'update',idelement:'${params.v2.idelement}',keyheader:${params.k2},idmilestone:${params.v2.idmilestone}})" style="display:none"><i class="fa fa-save"></i></button>
                                <button type="button" id="btn-savedetail-${params.v2.idelement}" class="btn btn-success btn-sm btn-icon c-rounded" onclick="saveDetail({idelement:'${params.v2.idelement}'})" style="display:none"><i class="fa fa-save"></i></button>
                                <button type="button" id="btn-view-${params.v2.idelement}" class="btn btn-info btn-sm btn-icon c-rounded" onclick="editHeader({type:'view',idelement:'${params.v2.idelement}',keyheader:${params.k2},idmilestone:${params.v2.idmilestone}})"><i class="fa fa-eye"></i></button>
                                <button type="button" id="btn-edit-${params.v2.idelement}" class="btn btn-warning btn-sm btn-icon c-rounded" onclick="editHeader({type:'edit',idelement:'${params.v2.idelement}',keyheader:${params.k2},idmilestone:${params.v2.idmilestone}})"><i class="fa fa-edit"></i></button>
                                <button type="button" id="btn-remove-${params.v2.idelement}" class="btn btn-danger btn-sm btn-icon c-rounded" onclick="removeHeader({keyheader:${params.k2},idmilestone:${params.v2.idmilestone}})"><i class="fa fa-trash"></i></button>
                                <button type="button" id="btn-collapse-${params.v2.idelement}" class="btn btn-default btn-sm btn-icon c-rounded" onclick="collapseHeader({idelement:'${params.v2.idelement}',idmaster:${params.v2.idmaster},idheader:${params.v2.noid}})" style="display:none"><i class="fa fa-angle-right"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body collapse" id="div-formadddetail-${params.v2.idelement}">
                        <form id="form-adddetail-${params.v2.idelement}">
                            <div class="card" style="margin: 0px 0px 10px 0px !important;">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Milestone</label>
                                                <input type="hidden" value="${params.v.noid}" name="idmilestone" id="detail-idmilestone-${params.v2.idelement}" class="form-control">
                                                <input type="text" value="${params.v.nama}" class="form-control" style="background-color: #E9ECEF !important" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Header</label>
                                                <input type="hidden" value="${params.v2.noid}" name="idheader" id="detail-idheader-${params.v2.idelement}" class="form-control">
                                                <input type="text" value="${params.v2.nama}" class="form-control" style="background-color: #E9ECEF !important" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Kode Purchase Request</label>
                                                <input type="text" name="kodeprev" id="detail-kodeprev-${params.v2.idelement}" class="form-control" style="background-color: #E9ECEF !important" placeholder="Kode Purchase Offer" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Kode Inventor</label>
                                                <input type="hidden" id="keydetail">
                                                <input type="hidden" id="idmilestone">
                                                <input type="hidden" id="idheader">
                                                <input type="hidden" id="idmaster">
                                                <select class="form-control" style="background-color: #E9ECEF !important" name="idinventor" id="detail-idinventor-${params.v2.idelement}" readonly>
                                                    <option value="" style="display:none">[Choose Inventor]</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Nama Inventor Original</label>
                                                <input type="text" name="namainventor" id="detail-namainventor-${params.v2.idelement}" class="form-control" style="background-color: #E9ECEF !important" placeholder="Nama Inventor Original" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Nama Inventor Updated</label>
                                                <input type="text" name="namainventor2" id="detail-namainventor2-${params.v2.idelement}" class="form-control" style="background-color: #E9ECEF !important" placeholder="Nama Inventor Updated" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Qty Original</label>
                                                <input type="text" id="detail-unitqtyoriginal-${params.v2.idelement}" class="form-control" style="background-color: #E9ECEF !important" placeholder="Qty" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Qty Updated <span class="text-danger">*</span></label>
                                                <input type="text" name="unitqty" id="detail-unitqty-${params.v2.idelement}" class="form-control" placeholder="Qty Updated">
                                            </div>
                                        </div>
                                        <!-- <div class="col-md-1"> -->
                                            <!-- <div class="form-group" style="margin-top: 24px">
                                                <button type="button" class="btn btn-outline-secondary">
                                                        <i class="icon-eye-open" style="padding: 0px"></i>
                                                </button>
                                            </div> -->
                                        <!-- </div> -->
                                        <div class="col-md-8">
                                            <div class="form-group pull-right" style="margin-top: 24px">
                                                <button type="button" id="cancel-detail" class="btn btn-default" onclick="cancelDetail()" style="display: none">
                                                    <i class="fa fa-arrow-left"></i> Cancel
                                                </button>
                                                <button type="button" id="" class="btn btn-success" onclick="updateDetail()" style="display: none">
                                                    <i class="fa fa-save"></i> Update
                                                </button>
                                                <button type="button" id="save-detail" class="btn btn-success" onclick="saveDetail({idelement:'${params.v2.idelement}'})" style="display: none">
                                                    <i class="fa fa-save"></i> Add
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    
                    <div class="card-body collapse" id="div-form-${params.v2.idelement}">
                        <form id="form-${params.v2.idelement}">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Milestone <span class="text-danger">*</span></label>
                                        <div class="col-md-9">
                                            <input type="hidden" name="noid" id="${params.v2.idelement}-noid">
                                            <select name="idmilestone" class="form-control select2" id="${params.v2.idelement}-idmilestone">
                                                <option value="">[Choose Milestone]</option>
                                                ${params._milestoneoptions}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Nama <span class="text-danger">*</span></label>
                                        <div class="col-md-7">
                                            <input type="text" name="nama" class="form-control" id="${params.v2.idelement}-nama" placeholder="Nama">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" name="nourut" class="form-control" style="background-color: #E9ECEF !important" id="${params.v2.idelement}-nourut" placeholder="No Urut" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Keterangan</label>
                                        <div class="col-md-9">
                                            <textarea name="keterangan" cols="3" rows="5" class="form-control" id="${params.v2.idelement}-keterangan" placeholder="Keterangan"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">PJ <span class="text-danger">*</span></label>
                                        <div class="col-md-4">
                                            <select class="form-control select2" name="idcardpj" id="${params.v2.idelement}-idcardpj">
                                                <option value="">[Choose PJ]</option>
                                                ${params._mcardoptions}
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <select class="form-control select2" name="idcardpjwakil" id="${params.v2.idelement}-idcardpjwakil">
                                                <option value="">[Choose Wakil PJ]</option>
                                                ${params._mcardoptions}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Tgl. <span class="text-danger">*</span></label>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <input type="text" name="tanggalstart" class="form-control datepicker" id="${params.v2.idelement}-tanggalstart" placeholder="Tanggal Start">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">s/d</span>
                                                </div>
                                                <input type="text" name="tanggalstop" class="form-control datepicker" id="${params.v2.idelement}-tanggalstop" placeholder="Tanggal Stop">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="card-body collapse" id="div-table-${params.v2.idelement}" style="padding: 0px 0px 0px 10px">
                        <div id="appendInfoDatatableDetail-${params.v2.idelement}" style="border-top: 2px solid #3598dc; border-right: 3px solid #3598dc; border-left: 2px solid #3598dc; border-bottom: 2px solid #3598dc;"></div>
                        <div class="table-scrollable" style="margin-top: 0px !important; margin-bottom: 0px !important; border: 1px solid #3598dc !important">
                            <table bg-color="blue" id="table-${params.v2.idelement}" class="nowrap table table-striped table-bordered table-hover  dataTable no-footer" style="background-color:#BBDEFB !important; margin: 0 !important; border: 1px solid #3598dc"  aria-describedby="table_master_info" role="grid">
                                <thead>
                                    <tr style="height: 40px">
                                        <th style="width: 30px;">
                                            <input type="checkbox" id="select_all_9921050102" onchange="selectAllCheckboxTab(9921050102)" class="checkbox-9921050102 checkbox-9921050102-0" name="checkbox-9921050102[]" value="FALSE">
                                        </th>
                                        <th>No</th>
                                        <th>Kode Prev.</th>
                                        <th>Kode Inv.</th>
                                        <th>Nama</th>
                                        <th>Inventor</th>
                                        <th>Keterangan</th>
                                        <th>Qty.</th>
                                        <th>Qty. Sisa</th>
                                        <th>Unit</th>
                                        <th>Price</th>
                                        <th>Total</th>
                                        <th>Currency</th>
                                        <th id="th-action">Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>`;
    }

    const getMilestoneAndHeader = (params={}) => {
        $.ajax({
            type: 'POST',
            header: {
                'X-CSRF-TOKEN': $('#token').val()
            },
            url: '/get_milestone_and_header'+main.urlcode,
            dataType: 'json',
            data: {
                _token: $('#token').val(),
                idmaster: $('#noid').val(),
                tanggal: $('#form-header #tanggal').val(),
                tanggaldue: $('#form-header #tanggaldue').val(),
                data: 'data' in params ? JSON.stringify(params.data) : null
            },
            beforeSend: function(){
                showLoading();
            },
            success: function(response) {
                let _milestone = response.data.milestone;
                let _listmilestone = response.data.list.milestone;
                let _listimimilestone = response.data.list.imimilestone;
                let _listimiheader = response.data.list.imiheader;
                let _listmcard = response.data.list.mcard;

                localStorage.removeItem(main.tablemilestone);
                localStorage.setItem(main.tablemilestone, JSON.stringify(_milestone));

                let _html = '';
                let _htmllistheader = '';
                let _declarekeydown = [];
                let _subtotal = 0;
                let _total = 0;
                if (_milestone.length > 0) {
                    let _milestoneoptions = _listmilestone.map(i => `<option value="${i.noid}">${i.nama}</option>`)
                        .reduce((acc,curr) => acc+curr);
                    let _imimilestoneoptions = _listimimilestone.map(i => `<option 
                                value="${i.noid}" 
                                data-invminventory-nama="${i.nama}"
                            >${i.nama}</option>`)
                        .reduce((acc,curr) => acc+curr);
                    let _imiheaderoptions = _listimiheader.map(i => `<option 
                                value="${i.noid}" 
                                data-invminventory-nama="${i.nama}"
                            >${i.nama}</option>`)
                        .reduce((acc,curr) => acc+curr);
                    let _mcardoptions = _listmcard.map(i => `<option value="${i.noid}">${i.nama}</option>`)
                        .reduce((acc,curr) => acc+curr);

                    $.each(_milestone, function(k,v){
                        let _imimilestoneoptionsselected = _listimimilestone
                            .map(i => {
                                let selected = i.noid == v.idinventor ? 'selected="selected"' : '';
                                return `<option value="${i.noid}" data-invminventory-nama="${i.nama}" ${selected}>${i.nama}</option>`
                            })
                            .reduce((acc,curr) => acc+curr);
                        let _header = '';
                        let _mtstart = v.tanggalstart;
                        let _mtstop = v.tanggalstop;
                        let _mtfinished = v.tanggalfinished;

                        $.each(v.header, function(k2,v2){
                            if (v2.detail.length > 0) {
                                $.each(v2.detail, function(k3,v3){
                                    _subtotal += parseInt(`${v3.subtotal}`.replace(/[^0-9]/gi, ''));
                                    _total += parseInt(`${v3.hargatotal}`.replace(/[^0-9]/gi, ''));
                                })
                            }
                            _declarekeydown.push(v2.idelement);
                            _header += htmlHeader({
                                k:k,
                                k2:k2,
                                v:v,
                                v2:v2,
                                _milestoneoptions:_milestoneoptions,
                                _mcardoptions:_mcardoptions
                            });
                        });
                        
                        if ('idelement' in params) {
                            if (params.idelement == 'milestone-'+v.noid) {
                                _htmllistheader = _header;
                            }
                        }


                        _html += `<div class="row" `+(k==0?`style="margin-top: 10px"`:``)+`>
                            <div class="col-md-12" style="padding: 0px !important">
                                <div class="card" id="card-${v.idelement}" style="margin: 0px !important; border-radius: 0.42rem !important; border: 1px solid #C3D5A8;">
                                    <div class="card-header" style="padding: 5px; background: #C3D5A8; border-radius: 0.32rem 0.32rem 0rem 0rem !important;">
                                        <div class="row">          
                                            <div class="col-md-10">                              
                                                <b id="title-${v.idelement}" style="font-size: 13px; padding-top: 6px" onclick="collapseMilestone({idelement:'${v.idelement}'})"><i class="fa fa-angle-right text-light"></i> ${v.nourut}. ${v.nama}_(PJ=${v.idcardpj_nama},${v.tanggalstart} s/d ${v.tanggalstop})</b>
                                            </div>
                                            <div class="col-md-2 text-right">
                                                <button type="button" class="btn btn-primary btn-sm btn-icon c-rounded" id="btn-addheader-${v.idelement}" onclick="addHeader({idelement:'${v.idelement}',mtstart:'${_mtstart}',mtstop:'${_mtstop}',mtfinished:'${_mtfinished}'})"><i class="fa fa-plus"></i></button>
                                                <button type="button" class="btn btn-default btn-sm btn-icon c-rounded" id="btn-cancel-${v.idelement}" onclick="cancelMilestone({idelement:'${v.idelement}'})" style="display:none"><i class="fa fa-arrow-left"></i></button>
                                                <button type="button" class="btn btn-success btn-sm btn-icon c-rounded" id="btn-save-${v.idelement}" onclick="saveMilestone({type:'update',idelement:'${v.idelement}',keymilestone:${k}})" style="display:none"><i class="fa fa-save"></i></button>
                                                <button type="button" class="btn btn-success btn-sm btn-icon c-rounded" id="btn-saveheader-${v.idelement}" onclick="saveHeader({type:'save',idelement:'${v.idelement}'})" style="display:none"><i class="fa fa-save"></i></button>
                                                <button type="button" class="btn btn-info btn-sm btn-icon c-rounded" id="btn-view-${v.idelement}" onclick="editMilestone({type:'view',idelement:'${v.idelement}',keymilestone:${k}})"><i class="fa fa-eye"></i></button>
                                                <button type="button" class="btn btn-warning btn-sm btn-icon c-rounded" id="btn-edit-${v.idelement}" onclick="editMilestone({type:'edit',idelement:'${v.idelement}',keymilestone:${k}})"><i class="fa fa-edit"></i></button>
                                                <button type="button" class="btn btn-danger btn-sm btn-icon c-rounded" id="btn-remove-${v.idelement}" onclick="removeMilestone({keymilestone:${k}})"><i class="fa fa-trash"></i></button>
                                                <button type="button" class="btn btn-default btn-sm btn-icon c-rounded" id="btn-collapse-${v.idelement}" onclick="collapseMilestone({idelement:'${v.idelement}'})" style="display:none"><i class="fa fa-angle-right"></i></button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card-body collapse" id="div-formaddheader-${v.idelement}" style="padding: 10px">
                                        <form id="form-dheader-${v.idelement}">
                                            <div class="card" style="margin: 0px 0px 10px 0px !important;">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Milestone <span class="text-danger">*</span></label>
                                                                <div class="col-md-9">
                                                                    <input type="hidden" value="${v.noid}" name="idmilestone" id="header-idmilestone-${v.idelement}" class="form-control">
                                                                    <input type="text" value="${v.nama}" class="form-control" style="background-color: #E9ECEF !important" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Inventor <span class="text-danger">*</span></label>
                                                                <div class="col-md-7">
                                                                    <input type="hidden" name="idelement" value="${v.idelement}">
                                                                    <select class="form-control select2" name="idinventor" id="header-idinventor-${v.idelement}">
                                                                        <option value="">[Choose Inventor]</option>
                                                                        ${_imiheaderoptions}
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <input type="text" name="nourut" class="form-control" style="background-color: #E9ECEF !important" id="header-nourut-${v.idelement}" placeholder="No Urut" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Keterangan</label>
                                                                <div class="col-md-9">
                                                                    <textarea name="keterangan" id="header-keterangan-${v.idelement}" cols="3" rows="5" class="form-control" placeholder="Keterangan"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">PJ <span class="text-danger">*</span></label>
                                                                <div class="col-md-4">
                                                                    <select class="form-control select2" name="idcardpj" id="header-idcardpj-${v.idelement}">
                                                                        <option value="">[Choose PJ]</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <select class="form-control select2" name="idcardpjwakil" id="header-idcardpjwakil-${v.idelement}">
                                                                        <option value="">[Choose Wakil PJ]</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label">Tanggal <span class="text-danger">*</span></label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group">
                                                                        <input type="text" name="tanggalstart" class="form-control" id="header-tanggalstart-${v.idelement}" placeholder="Tanggal Start">
                                                                        <div class="input-group-append">
                                                                            <span class="input-group-text">s/d</span>
                                                                        </div>
                                                                        <input type="text" name="tanggalstop" class="form-control" id="header-tanggalstop-${v.idelement}" placeholder="Tanggal Stop">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group pull-right" style="margin-top: 24px">
                                                                <button type="button" id="cancel-detail" class="btn btn-default" onclick="cancelDetail()" style="display: none">
                                                                    <i class="fa fa-arrow-left"></i> Cancel
                                                                </button>
                                                                <button type="button" id="btn-updateheader" class="btn btn-success" onclick="" style="display: none">
                                                                    <i class="fa fa-save"></i> Update
                                                                </button>
                                                                <button type="button" id="btn-saveheader" class="btn btn-success" onclick="saveHeader({type:'save'})" style="display: none">
                                                                    <i class="fa fa-save"></i> Save
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="card-body collapse" id="div-form-${v.idelement}" style="padding: 10px">
                                        <form id="form-${v.idelement}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label">Nama <span class="text-danger">*</span></label>
                                                        <div class="col-md-7">
                                                            <input type="hidden" name="idelement" value="${v.idelement}">
                                                            <input type="hidden" name="noid" id="${v.idelement}-noid">
                                                            <select class="form-control select2" name="idinventor" id="${v.idelement}-idinventor">
                                                                <option value="">[Choose Inventor]</option>
                                                                ${_imimilestoneoptionsselected}
                                                            </select>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <input type="text" name="nourut" class="form-control" style="background-color: #E9ECEF !important" id="${v.idelement}-nourut" placeholder="No Uruttt" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label">Keterangan</label>
                                                        <div class="col-md-9">
                                                            <textarea name="keterangan" id="${v.idelement}-keterangan" cols="3" rows="5" class="form-control" placeholder="Keterangan"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label">PJ <span class="text-danger">*</span></label>
                                                        <div class="col-md-4">
                                                            <select class="form-control select2" name="idcardpj" id="${v.idelement}-idcardpj">
                                                                <option value="">[Choose PJ]</option>
                                                                ${_mcardoptions}
                                                            </select>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <select class="form-control select2" name="idcardpjwakil" id="${v.idelement}-idcardpjwakil">
                                                                <option value="">[Choose Wakil PJ]</option>
                                                                ${_mcardoptions}
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label">Tgl. <span class="text-danger">*</span></label>
                                                        <div class="col-md-9">
                                                            <div class="input-group">
                                                                <input type="text" name="tanggalstart" class="form-control" id="${v.idelement}-tanggalstart" placeholder="Tanggal Start">
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">s/d</span>
                                                                </div>
                                                                <input type="text" name="tanggalstop" class="form-control" id="${v.idelement}-tanggalstop" placeholder="Tanggal Stop">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="card-body collapse" id="div-table-${v.idelement}" style="padding: 0px 0px 0px 10px; display: none;">
                                        ${_header==''?'No Header Data':_header}
                                    </div>
                                </div>
                            </div>
                        </div>`;
                    });
                } else {
                    _html = 'No Detail Data';
                }

                if ('idelement' in params) {
                    $('#div-table-'+params.idelement).html(_htmllistheader)
                    cancelMilestone({idelement:params.idelement})
                } else {
                    $('#milestoneandheader').html(_html);
                }
                // console.log(_milestone);


                let _tanggal = new Date($('#form-header #tanggal').val());
                let _tanggaldue = new Date($('#form-header #tanggaldue').val());

                $.each(_milestone, function(k,v){
                    if (!v.iscollapse) {
                        collapseMilestone({idelement:v.idelement})
                    }
                    if (v.header.length > 0) {
                        $.each(v.header, function(k2,v2){
                            if (!v2.iscollapse) {
                                collapseHeader({idelement:v2.idelement})
                                getDatatableDetail({idelement:v2.idelement,idmaster:v2.idmaster,idheader:v2.noid})
                            }


                            $(`#${v2.idelement}-tanggalstart`).datepicker({
                                format: 'dd-M-yyyy',
                                startDate: new Date($('#form-header #tanggal').val()),
                                endDate: new Date($('#form-header #tanggaldue').val()),
                                todayHighlight: true,
                                autoclose: true,
                                container: '#pagepanel'
                            });
                            $(`#${v2.idelement}-tanggalstart`).on('change', function(){
                                let _tanggal = new Date($('#form-header #tanggal').val());
                                let _tanggaldue = new Date($('#form-header #tanggaldue').val());
                                let _start = new Date($(`#${v2.idelement}-tanggalstart`).val());
                                let _stop = new Date($(`#${v2.idelement}-tanggalstop`).val());
                                let _mtstart = new Date(v.tanggalstart);
                                let _mtstop = new Date(v.tanggalstop);
                                let _issuccess = true;
                                let _message = '';
                                if (_start < _tanggal) {_message = "'Tanggal Start' harus lebih besar dari 'Tanggal'"; _issuccess = false;}
                                if (_start > _tanggaldue) {_message = "'Tanggal Start' harus lebih kecil dari 'Tanggal Due'"; _issuccess = false;}
                                if (_start > _stop) {_message = "'Tanggal Start' harus lebih kecil dari 'Tanggal Stop'"; _issuccess = false;}
                                if (_start < _mtstart) {_message = "'Tanggal Start' harus lebih besar dari 'Tanggal Start Milestone'"; _issuccess = false;}
                                if (_start > _mtstop) {_message = "'Tanggal Start' harus lebih kecil dari 'Tanggal Stop Milestone'"; _issuccess = false;}
                                if (!_issuccess) {$(`#${v2.idelement}-tanggalstart`).val('');$.notify({message: _message},{type: 'danger'});return false;}
                            })
                            $(`#${v2.idelement}-tanggalstop`).datepicker({
                                format: 'dd-M-yyyy',
                                startDate: new Date($('#form-header #tanggal').val()),
                                endDate: new Date($('#form-header #tanggaldue').val()),
                                todayHighlight: true,
                                autoclose: true,
                                container: '#pagepanel'
                            });
                            $(`#${v2.idelement}-tanggalstop`).on('change', function(){
                                let _tanggal = new Date($('#form-header #tanggal').val());
                                let _tanggaldue = new Date($('#form-header #tanggaldue').val());
                                let _start = new Date($(`#${v2.idelement}-tanggalstart`).val());
                                let _stop = new Date($(`#${v2.idelement}-tanggalstop`).val());
                                let _mtstart = new Date(v.tanggalstart);
                                let _mtstop = new Date(v.tanggalstop);
                                let _issuccess = true;
                                let _message = '';
                                if (_stop < _tanggal) {_message = "'Tanggal Stop' harus lebih besar dari 'Tanggal'"; _issuccess = false;}
                                if (_stop > _tanggaldue) {_message = "'Tanggal Stop' harus lebih kecil dari 'Tanggal Due'"; _issuccess = false;}
                                if (_stop < _start) {_message = "'Tanggal Stop' harus lebih besar dari 'Tanggal Start'"; _issuccess = false;}
                                if (_stop < _mtstart) {_message = "'Tanggal Start' harus lebih besar dari 'Tanggal Start Milestone'"; _issuccess = false;}
                                if (_stop > _mtstop) {_message = "'Tanggal Start' harus lebih kecil dari 'Tanggal Stop Milestone'"; _issuccess = false;}
                                if (!_issuccess) {$(`#${v2.idelement}-tanggalstop`).val('');$.notify({message: _message},{type: 'danger'});return false;}
                            })
                        })
                    }


                    $(`#${v.idelement}-tanggalstart`).datepicker({
                        format: 'dd-M-yyyy',
                        startDate: new Date($('#form-header #tanggal').val()),
                        endDate: new Date($('#form-header #tanggaldue').val()),
                        todayHighlight: true,
                        autoclose: true,
                        container: '#pagepanel'
                    });
                    $(`#${v.idelement}-tanggalstart`).on('change', function(){
                        let _tanggal = new Date($('#form-header #tanggal').val());
                        let _tanggaldue = new Date($('#form-header #tanggaldue').val());
                        let _start = new Date($(`#${v.idelement}-tanggalstart`).val());
                        let _stop = new Date($(`#${v.idelement}-tanggalstop`).val());
                        let _issuccess = true;
                        let _message = '';
                        if (_start < _tanggal) {_message = "'Tanggal Start' harus lebih besar dari 'Tanggal'"; _issuccess = false;}
                        if (_start > _tanggaldue) {_message = "'Tanggal Start' harus lebih kecil dari 'Tanggal Due'"; _issuccess = false;}
                        if (_start > _stop) {_message = "'Tanggal Start' harus lebih kecil dari 'Tanggal Stop'"; _issuccess = false;}
                        if (!_issuccess) {$(`#${v.idelement}-tanggalstart`).val('');$.notify({message: _message},{type: 'danger'});return false;}
                    })
                    $(`#${v.idelement}-tanggalstop`).datepicker({
                        format: 'dd-M-yyyy',
                        startDate: new Date($('#form-header #tanggal').val()),
                        endDate: new Date($('#form-header #tanggaldue').val()),
                        todayHighlight: true,
                        autoclose: true,
                        container: '#pagepanel'
                    });
                    $(`#${v.idelement}-tanggalstop`).on('change', function(){
                        let _tanggal = new Date($('#form-header #tanggal').val());
                        let _tanggaldue = new Date($('#form-header #tanggaldue').val());
                        let _start = new Date($(`#${v.idelement}-tanggalstart`).val());
                        let _stop = new Date($(`#${v.idelement}-tanggalstop`).val());
                        let _issuccess = true;
                        let _message = '';
                        if (_stop < _tanggal) {_message = "'Tanggal Stop' harus lebih besar dari 'Tanggal'"; _issuccess = false;}
                        if (_stop > _tanggaldue) {_message = "'Tanggal Stop' harus lebih kecil dari 'Tanggal Due'"; _issuccess = false;}
                        if (_stop < _start) {_message = "'Tanggal Stop' harus lebih besar dari 'Tanggal Start'"; _issuccess = false;}
                        if (!_issuccess) {$(`#${v.idelement}-tanggalstop`).val('');$.notify({message: _message},{type: 'danger'});return false;}
                    })
                })

                $.each(_declarekeydown, function(k,v){
                    $('#searchprev-'+v).on('keydown',function(e){if(e.keyCode==13){
                        $('#btn-addheader-'+v).hide();
                        $('#btn-add-'+v).hide();
                        $('#btn-cancel-'+v).show();
                        $('#btn-save-'+v).hide();
                        $('#btn-savedetail-'+v).show();
                        $('#btn-view-'+v).hide();
                        $('#btn-edit-'+v).hide();
                        $('#btn-remove-'+v).hide();
                        $('#btn-collapse-'+v).hide();
                        $('#div-form-'+v).slideUp();
                        showModalDetailPrev({idelement: v,searchprev:$('#searchprev-'+v).val()});
                        $('#detail-unitqty-'+v).on('keydown',function(e){if(e.keyCode==13){$('#btn-savedetail-'+v).focus();}})
                        
                    }})
                })

                $('#form-footer #subtotal').val(_subtotal);
                $('#form-footer #total').val(_total);
                
                $('.select2').select2({dropdownParent: $('#pagepanel')});
                $('.select2').css('width', '100%');
                $('.select2 .selection .select2-selection').css('height', '31px');
                $('.select2 .selection .select2-selection .select2-selection__rendered').css('margin-top', '-6px');
                $('.select2 .selection .select2-selection .select2-selection__arrow').css('margin-top', '4px');
                

                // alert(_tanggal+'  '+_tanggaldue)
            
                
                setTimeout(hideLoading(), 500);
            }
        })
    }

    function addMilestone() {
        // hideFormHeader();
        unsetFormMilestone({idelement:'milestone'});
        getNoUrut({type:'milestone'});
        showFormMilestone();
    }

    function getNoUrut(params) {
        let _idmilestone = $('#header-idmilestone').val();
        let _milestone = JSON.parse(localStorage.getItem(main.tablemilestone));
        let _nourut;

        if (params.type == 'milestone') {
            _nourut = _milestone && _milestone.length > 0
                ? parseInt(_milestone[_milestone.length-1]['nourut'])+1 
                : 1;
        } else if (params.type == 'header') {
            if (_idmilestone) {
                let _header = _milestone.filter(i => i.noid == _idmilestone)[0]['header'];
                _nourut = _header.length > 0 ? parseInt(_header[_header.length-1]['nourut'])+1 : 1;
            } else {
                _nourut = '';
            }
        }

        $('#'+params.type+'-nourut').val(_nourut);
    }

    function getNoUrutHeader(params) {
        let _idmilestone = $('#header-idmilestone-'+params.idelement).val();
        let _milestone = JSON.parse(localStorage.getItem(main.tablemilestone));
        let _nourut;

        if (_idmilestone) {
            let _header = _milestone.filter(i => i.noid == _idmilestone)[0]['header'];
            _nourut = _header.length > 0 ? parseInt(_header[_header.length-1]['nourut'])+1 : 1;
        } else {
            _nourut = '';
        }

        $('#'+params.type+'-nourut-'+params.idelement).val(_nourut);
    }

    function unsetFormMilestone(params) {
        $('#'+params.idelement+'-nama').val('');
        $('#'+params.idelement+'-keterangan').val('');
        $('#'+params.idelement+'-idcardpj').val('').trigger('change');
        $('#'+params.idelement+'-idcardpjwakil').val('').trigger('change');
        $('#'+params.idelement+'-nourut').val('');
        $('#'+params.idelement+'-tanggalstart').val('');
        $('#'+params.idelement+'-tanggalstop').val('');
        $('#'+params.idelement+'-tanggalfinished').val('');
    }

    function collapseMilestone(params) {
        let _alwaysopen = 'alwaysopen' in params ? params.alwaysopen : false;
        let _iscollapse = true;
        if ($('#div-table-'+params.idelement).css('display') == 'none' || _alwaysopen) {
            $('#title-'+params.idelement+' i').removeClass('fa-angle-right');
            $('#title-'+params.idelement+' i').addClass('fa-angle-down');
            // $('#btn-addheader-'+params.idelement).hide();
            // $('#btn-view-'+params.idelement).hide();
            // $('#btn-edit-'+params.idelement).hide();
            // $('#btn-remove-'+params.idelement).hide();
            $('#btn-collapse-'+params.idelement).html('<i class="fa fa-angle-down">');
            $('#div-table-'+params.idelement).slideDown();
            _iscollapse = false;
        } else {
            $('#title-'+params.idelement+' i').removeClass('fa-angle-down');
            $('#title-'+params.idelement+' i').addClass('fa-angle-right');
            // $('#btn-addheader-'+params.idelement).show();
            // $('#btn-view-'+params.idelement).show();
            // $('#btn-edit-'+params.idelement).show();
            // $('#btn-remove-'+params.idelement).show();
            $('#btn-collapse-'+params.idelement).html('<i class="fa fa-angle-right">');
            $('#div-table-'+params.idelement).slideUp();
            _iscollapse = true;
        }

        let _milestone = [];
        $.each(JSON.parse(localStorage.getItem(main.tablemilestone)), function(k,v){
            if (v.idelement == params.idelement) {
                v.iscollapse = _iscollapse;
            }
            _milestone.push(v);
        })
        localStorage.removeItem(main.tablemilestone)
        localStorage.setItem(main.tablemilestone, JSON.stringify(_milestone))
    }

    function getListDateOfBetweenDate(params) {
        let tanggal = new Date(params.startdate);
        let tanggaldue = new Date(params.enddate);

        let listDate = [];
        let startDate = tanggal.toISOString().split('T')[0];
        let endDate = tanggaldue.toISOString().split('T')[0];
        let dateMove = new Date(startDate);
        let strDate = startDate;
        while (strDate <= endDate) {
            strDate = dateMove.toISOString().slice(0, 10);
            listDate.push(strDate);
            dateMove.setDate(dateMove.getDate() + 1);
        };

        return listDate;
    }

    function showFormMilestone() {
        let tanggal = $('#form-header #tanggal');
        let tanggaldue = $('#form-header #tanggaldue');
        let betweendate = getListDateOfBetweenDate({startdate: tanggal.val(), enddate: tanggaldue.val()});
        let tanggalstart = $('#form-dmilestone #milestone-tanggalstart');
        let tanggalstop = $('#form-dmilestone #milestone-tanggalstop');
        let rulesdatepicker = {
            inline: true,
            sideBySide: true,
            format: 'dd-M-yyyy',
            autoclose: true,
            container: '#pagepanel',
            todayHighlight: true,
            startDate: new Date(tanggal.val()),
            endDate: new Date(tanggaldue.val()),
            enabledDates: betweendate
        };

        tanggalstart.datepicker(rulesdatepicker);
        tanggalstop.datepicker(rulesdatepicker);

        setTimeout(function(){ $('#form-dmilestone div').slideDown(100, function() {
            $('#form-dmilestone #milestone-idinventor').val('').trigger('change').select2('focus');
        }); }, 100);
        
        $('#form-dmilestone #milestone-idinventor').on('select2:close',function(e){$('#form-dmilestone #milestone-keterangan').focus()})
        $('#form-dmilestone #milestone-keterangan').on('keydown',function(e){if(e.keyCode==13){$('#form-dmilestone #milestone-idcardpj').select2('focus');}})
        $('#form-dmilestone #milestone-idcardpj').on('select2:close',function(e){$('#form-dmilestone #milestone-idcardpjwakil').select2('focus')})
        $('#form-dmilestone #milestone-idcardpjwakil').on('select2:close',function(e){$('#form-dmilestone #milestone-tanggalstart').select2('focus')})
        $('#form-dmilestone #milestone-tanggalstart').on('keydown',function(e){if(e.keyCode==13){$('#form-dmilestone #milestone-tanggalstop').focus();}})
        $('#form-dmilestone #milestone-tanggalstop').on('keydown',function(e){if(e.keyCode==13){$('#btn-save-milestone').focus();}})

        $('#btn-generate-milestone').hide();
        $('#btn-save-milestone').show();
        $('#btn-add-milestone').hide();
        $('#btn-cancel-header').hide();
        $('#btn-save-header').hide();
        $('#btn-cancel-milestone').show();
    }

    function hideFormMilestone() {
        setTimeout(function(){ $('#form-dmilestone div').slideUp(100, function() {
            $('#form-dmilestone #milestone-nama').focus();
        }); }, 100);
        $('#btn-generate-milestone').show();
        $('#btn-cancel-milestone').hide();
        $('#btn-save-milestone').hide();
        $('#btn-add-milestone').show();
        $('#btn-cancel-header').hide();
        $('#btn-save-header').hide();
        // $('#btn-add-header').show();
    }

    function cancelMilestone(params) {
        $('#btn-addheader-'+params.idelement).show();
        $('#btn-add-'+params.idelement).show();
        $('#btn-cancel-'+params.idelement).hide();
        $('#btn-save-'+params.idelement).hide();
        $('#btn-saveheader-'+params.idelement).hide();
        $('#btn-view-'+params.idelement).show();
        $('#btn-edit-'+params.idelement).show();
        $('#btn-remove-'+params.idelement).show();
        // $('#btn-collapse-'+params.idelement).show();
        $('#div-formaddheader-'+params.idelement).slideUp();
        $('#div-form-'+params.idelement).slideUp();
    }

    function changeMilestone() {
        alert($(this).val())
    }

    function saveMilestone(params) {
        let _noid = $('#form-header #noid').val();
        let _formmilestone = {};
        let _form = params.type == 'update'
            ? $('#form-'+params.idelement).serializeArray()
            : $('#form-dmilestone').serializeArray()
        let _validation = {
            nama: {
                required: {
                    status: true,
                    message: 'Nama is required!'
                }
            },
            idcardpj: {
                required: {
                    status: true,
                    message: 'PJ is required!'
                }
            },
            idcardpjwakil: {
                required: {
                    status: true,
                    message: 'Wakil PJ is required!'
                }
            },
            nourut: {
                required: {
                    status: true,
                    message: 'No Urut is required!'
                }
            },
            tanggalstart: {
                required: {
                    status: true,
                    message: 'Tanggal Start is required!'
                }
            },
            tanggalstop: {
                required: {
                    status: true,
                    message: 'Tanggal Stop is required!'
                }
            },
        };
        let _showvalidation = {
            status: false,
            message: ''
        };

        $.each(_form, function(k,v) {
            if (v.name in _validation) {
                if (_validation[v.name].required.status) {
                    if (v.value == '') {
                        _showvalidation.status = true;
                        _showvalidation.message += _validation[v.name].required.message+'<br>'
                    }
                }
            }

            if (v.name != 'idelement') {
                _formmilestone[v.name] = v.value;
            } else {
                _formmilestone['nama'] = $(`#${v.value}-idinventor :selected`).attr('data-invminventory-nama');
            }
        });
        
        if ('noid' in _formmilestone) {
            let _findmilestone = JSON.parse(localStorage.getItem(main.tablemilestone))
                .filter(i => i.noid == _formmilestone.noid);

            _formmilestone.iscollapse = _findmilestone.length > 0
                ? _findmilestone[0].iscollapse
                : true;
        }


        if (_showvalidation.status) {
            $.notify({
                message: _showvalidation.message
            },{
                type: 'danger'
            });
            return false;
        }

        $.ajax({
            type: 'POST',
            header: {
                'X-CSRF-TOKEN': $('#token').val()
            },
            url: '/save_milestone'+main.urlcode,
            dataType: 'json',
            data: {
                _token: $('#token').val(),
                statusadd: statusadd,
                statusadd2: params.type,
                noid: _noid,
                formmilestone: _formmilestone,
                milestone: localStorage.getItem(main.tablemilestone),
                header: localStorage.getItem(main.tablemilestone)
                    ? JSON.parse(localStorage.getItem(main.tablemilestone))[params.keymilestone]
                        ? JSON.stringify(JSON.parse(localStorage.getItem(main.tablemilestone))[params.keymilestone].header)
                        : JSON.stringify([])
                    : JSON.stringify([])
            },
            success: function(response) {
                let _dataprev = JSON.parse(localStorage.getItem(main.tablemilestone));
                let _datanext = response.data;
                let _datamerge = [];

                if (_dataprev) {
                    if (params.type == 'save') {
                        _datamerge = _dataprev.concat(_datanext);
                    } else if (params.type == 'update') {
                        _dataprev[params.keymilestone] = response.data;
                        _datamerge = _dataprev;
                    }
                    localStorage.setItem(main.tablemilestone, JSON.stringify(_datamerge));
                } else {
                    let _firstdata = [];
                    _firstdata.push(_datanext);
                    localStorage.setItem(main.tablemilestone, JSON.stringify(_firstdata));
                }

                if (params.type == 'save') hideFormMilestone()

                getMilestoneAndHeader({data:JSON.parse(localStorage.getItem(main.tablemilestone))});            
            }
        });
    }

    function addHeaderBackup() {
        hideFormMilestone();
        getSelect({
            from: JSON.parse(localStorage.getItem(main.tablemilestone)),
            selected:false,
            title: 'Milestone',
            targetelement: '#form-dheader #header-idmilestone'
        });
        unsetFormDHeader({idelement:'header'});
        showFormHeader();
    }
    
    function addHeader(params) {
        let _idel = params.idelement;
        let _mtstart = new Date(params.mtstart);
        let _mtstop = new Date(params.mtstop);
        let _fh = '#form-dheader-'+_idel;

        let rulesdatepicker = {
            format: 'dd-M-yyyy',
            startDate: _mtstart,
            endDate: _mtstop,
            todayHighlight: true,
            autoclose: true,
            container: '#pagepanel'
        };

        getNoUrutHeader({type:'header',idelement:_idel});

        $('#btn-addheader-'+_idel).hide();
        $('#btn-add-'+_idel).hide();
        $('#btn-cancel-'+_idel).show();
        $('#btn-save-'+_idel).hide();
        $('#btn-saveheader-'+_idel).show();
        $('#btn-view-'+_idel).hide();
        $('#btn-edit-'+_idel).hide();
        $('#btn-remove-'+_idel).hide();
        $('#btn-collapse-'+_idel).hide();
        $('#div-formaddheader-'+_idel).slideDown();
        
        $('#header-idinventor-'+_idel).select2('focus');
        $('#header-idinventor-'+_idel).on('select2:close',function(e){$('#header-keterangan-'+_idel).focus()})
        $('#header-keterangan-'+_idel).on('keydown',function(e){if(e.keyCode==13){$('#header-idcardpj-'+_idel).select2('focus');}})
        $('#header-idcardpj-'+_idel).on('select2:close',function(e){$('#header-idcardpjwakil-'+_idel).select2('focus')})
        $('#header-idcardpjwakil-'+_idel).on('select2:close',function(e){$('#header-tanggalstart-'+_idel).focus()})
        $('#header-tanggalstart-'+_idel).on('change',function(e){
            $('#header-tanggalstop-'+_idel).focus();
        });
        $('#header-tanggalstop-'+_idel).on('change',function(e){
            $('#btn-saveheader-'+_idel).focus();
        });


        $('#header-tanggalstart-'+_idel).datepicker(rulesdatepicker);
        $('#header-tanggalstart-'+_idel).on('change', function(){
            let _tanggal = new Date($('#form-header #tanggal').val());
            let _tanggaldue = new Date($('#form-header #tanggaldue').val());
            let _start = new Date($('#header-tanggalstart-'+_idel).val());
            let _stop = new Date($('#header-tanggalstop-'+_idel).val());
            let _issuccess = true;
            let _message = '';
            if (_start < _tanggal) {_message = "'Tanggal Start' harus lebih besar dari 'Tanggal'"; _issuccess = false;}
            if (_start > _tanggaldue) {_message = "'Tanggal Start' harus lebih kecil dari 'Tanggal Due'"; _issuccess = false;}
            if (_start > _stop) {_message = "'Tanggal Start' harus lebih kecil dari 'Tanggal Stop'"; _issuccess = false;}
            if (_start < _mtstart) {_message = "'Tanggal Start' harus lebih besar dari 'Tanggal Start Milestone'"; _issuccess = false;}
            if (_start > _mtstop) {_message = "'Tanggal Start' harus lebih kecil dari 'Tanggal Stop Milestone'"; _issuccess = false;}
            if (!_issuccess) {$('#header-tanggalstart-'+_idel).val('');$.notify({message: _message},{type: 'danger'});return false;}
        })
        $('#header-tanggalstop-'+_idel).datepicker(rulesdatepicker);
        $('#header-tanggalstop-'+_idel).on('change', function(){
            let _tanggal = new Date($('#form-header #tanggal').val());
            let _tanggaldue = new Date($('#form-header #tanggaldue').val());
            let _start = new Date($('#header-tanggalstart-'+_idel).val());
            let _stop = new Date($('#header-tanggalstop-'+_idel).val());
            let _issuccess = true;
            let _message = '';
            if (_stop < _tanggal) {_message = "'Tanggal Stop' harus lebih besar dari 'Tanggal'"; _issuccess = false;}
            if (_stop > _tanggaldue) {_message = "'Tanggal Stop' harus lebih kecil dari 'Tanggal Due'"; _issuccess = false;}
            if (_stop < _start) {_message = "'Tanggal Stop' harus lebih besar dari 'Tanggal Start'"; _issuccess = false;}
            if (_stop < _mtstart) {_message = "'Tanggal Stop' harus lebih besar dari 'Tanggal Stop Milestone'"; _issuccess = false;}
            if (_stop > _mtstop) {_message = "'Tanggal Stop' harus lebih kecil dari 'Tanggal Stop Milestone'"; _issuccess = false;}
            if (!_issuccess) {$('#header-tanggalstop-'+_idel).val('');$.notify({message: _message},{type: 'danger'});return false;}
        })
    }

    function unsetFormDHeader(params) {
        $('#'+params.idelement+'-nama').val('');
        $('#'+params.idelement+'-keterangan').val('');
        $('#'+params.idelement+'-idcardpj').val('').trigger('change');
        $('#'+params.idelement+'-idcardpjwakil').val('').trigger('change');
        $('#'+params.idelement+'-nourut').val('');
        $('#'+params.idelement+'-tanggalstart').val('');
        $('#'+params.idelement+'-tanggalstop').val('');
        $('#'+params.idelement+'-tanggalfinished').val('');
    }

    function collapseHeader(params) {
        let _alwaysopen = 'alwaysopen' in params ? params.alwaysopen : false;
        let _iscollapse;
        if ($('#div-table-'+params.idelement).css('display') == 'none' || _alwaysopen) {
            $('#title-'+params.idelement+' i').removeClass('fa-angle-right');
            $('#title-'+params.idelement+' i').addClass('fa-angle-down');
            getDatatableDetail({idelement:params.idelement,idmaster:params.idmaster,idheader:params.idheader})
            // $('#btn-view-'+params.idelement).hide();
            // $('#btn-edit-'+params.idelement).hide();
            // $('#btn-remove-'+params.idelement).hide();
            $('#btn-collapse-'+params.idelement).html('<i class="fa fa-angle-down">');
            $('#div-table-'+params.idelement).slideDown();
            _iscollapse = false;
        } else {
            $('#title-'+params.idelement+' i').removeClass('fa-angle-down');
            $('#title-'+params.idelement+' i').addClass('fa-angle-right');
            // $('#btn-view-'+params.idelement).show();
            // $('#btn-edit-'+params.idelement).show();
            // $('#btn-remove-'+params.idelement).show();
            $('#btn-collapse-'+params.idelement).html('<i class="fa fa-angle-right">');
            $('#div-table-'+params.idelement).slideUp();
            _iscollapse = true;
        }

        let _milestone = [];
        $.each(JSON.parse(localStorage.getItem(main.tablemilestone)), function(k,v){
            let _header = [];
            $.each(v.header, function(k2,v2){
                if (v2.idelement == params.idelement) {
                    v2.iscollapse = _iscollapse;
                }
                _header.push(v2);
            })
            v.header = _header;
            _milestone.push(v);
        })
        localStorage.removeItem(main.tablemilestone)
        localStorage.setItem(main.tablemilestone, JSON.stringify(_milestone))
    }

    function showFormHeader() {
        setTimeout(function(){ $('#form-dheader div').slideDown(100, function() {
            $('#form-dheader #header-idmilestone').select2('focus');
        }); }, 100);
        
        $('#btn-cancel-milestone').hide();
        $('#btn-save-milestone').hide();
        $('#btn-add-milestone').hide();
        $('#btn-cancel-header').show();
        $('#btn-save-header').show();
    }

    function hideFormHeader() {
        setTimeout(function(){ $('#form-dheader div').slideUp(100, function() {
            $('#form-dheader #milestone-nama').focus();
        }); }, 100);
        $('#btn-cancel-header').hide();
        $('#btn-save-header').hide();
        // $('#btn-add-header').show();
        $('#btn-cancel-milestone').hide();
        $('#btn-save-milestone').hide();
        $('#btn-add-milestone').show();
    }

    $('#form-detail #detail-idmilestone').on('change', function() {
        let _header = [];
        let _idmilestone = $(this).val();

        $.each(JSON.parse(localStorage.getItem(main.tablemilestone)), function(k,v){
            if (_idmilestone == v.noid) {
                _header = _header.concat(v.header);
            }
        });

        getSelect({
            from:_header,
            selected: false,
            title: 'Header',
            targetelement: '#form-detail #detail-idheader'
        })
    });

    function getSelect(params) {
        let _html = '<option value="">[Choose '+params.title+']</option>';
        
        $.each(params.from, function(k,v){
            _html += '<option value="'+v.noid+'">'+v.nama+'</option>';
        });

        $(params.targetelement).html(_html);
        $('.select2').select2({
            dropdownParent: $('#pagepanel')
        });
        $('.select2').css('width', '100%');
        $('.select2 .selection .select2-selection').css('height', '31px');
        $('.select2 .selection .select2-selection .select2-selection__rendered').css('margin-top', '-6px');
        $('.select2 .selection .select2-selection .select2-selection__arrow').css('margin-top', '4px');
    
        if (params.selected) {
            $(params.targetelement).val(params.selected).trigger('change')
        }
    }

    function cancelHeader(params) {
        $('#btn-cancel-'+params.idelement).hide();
        $('#btn-save-'+params.idelement).hide();
        $('#btn-savedetail-'+params.idelement).hide();
        $('#btn-view-'+params.idelement).show();
        $('#btn-edit-'+params.idelement).show();
        $('#btn-remove-'+params.idelement).show();
        // $('#btn-collapse-'+params.idelement).show();
        $('#div-formadddetail-'+params.idelement).slideUp();
        $('#div-form-'+params.idelement).slideUp();
    }

    function saveHeader(params) {
        let _noid = $('#form-header #noid').val();
        let _formheader = {};
        let _form = params.type == 'update'
            ? $('#form-'+params.idelement).serializeArray()
            : $('#form-dheader-'+params.idelement).serializeArray()
        console.log(_form);
        let _validation = {
            idmilestone: {
                required: {
                    status: true,
                    message: 'Milestone is required!'
                }
            },
            nama: {
                required: {
                    status: true,
                    message: 'Nama is required!'
                }
            },
            idcardpj: {
                required: {
                    status: true,
                    message: 'PJ is required!'
                }
            },
            idcardpjwakil: {
                required: {
                    status: true,
                    message: 'Wakil PJ is required!'
                }
            },
            nourut: {
                required: {
                    status: true,
                    message: 'No Urut is required!'
                }
            },
            tanggalstart: {
                required: {
                    status: true,
                    message: 'Tanggal Start is required!'
                }
            },
            tanggalstop: {
                required: {
                    status: true,
                    message: 'Tanggal Stop is required!'
                }
            },
            tanggalfinished: {
                required: {
                    status: true,
                    message: 'Tanggal Finished is required!'
                }
            },
        };
        let _showvalidation = {
            status: false,
            message: ''
        };

        $.each(_form, function(k,v) {
            if (v.name in _validation) {
                if (_validation[v.name].required.status) {
                    if (v.value == '') {
                        _showvalidation.status = true;
                        _showvalidation.message += _validation[v.name].required.message+'<br>'
                    }
                }
            }
            
            if (v.name != 'idelement') {
                _formheader[v.name] = v.value;
            } else {
                _formheader['nama'] = $(`#header-idinventor-${v.value} :selected`).attr('data-invminventory-nama');
            }
        });

        if ('noid' in _formheader) {
            let _findheader = JSON.parse(localStorage.getItem(main.tablemilestone))
                .filter(i => i.noid == params.idmilestone)[0].header
                .filter(i => i.noid == _formheader.noid);

            _formheader.iscollapse = _findheader.length > 0
                ? _findheader[0].iscollapse
                : true;
        }

        if (_showvalidation.status) {
            $.notify({
                message: _showvalidation.message
            },{
                type: 'danger'
            });
            return false;
        }

        let _findmilestone = JSON.parse(localStorage.getItem(main.tablemilestone))
            .filter(i => i.noid == $('#'+params.idelement+'-idmilestone').val());

        // let _appendheader = JSON.parse(localStorage.getItem(main.tablemilestone))
        //     .filter(i => i.noid == _formheader.idmilestone);
        // console.table(_formheader);return false;

        $.ajax({
            type: 'POST',
            header: {
                'X-CSRF-TOKEN': $('#token').val()
            },
            url: '/save_header'+main.urlcode,
            dataType: 'json',
            data: {
                _token: $('#token').val(),
                noid: _noid,
                statusadd: statusadd,
                statusadd2: params.type,
                formheader: _formheader,
                milestone: localStorage.getItem(main.tablemilestone),
                detail: params.type == 'update'
                    ? JSON.stringify(_findmilestone[0]['header'][params.keyheader]['detail'])
                    : JSON.stringify([]),
            },
            success: function(response) {
                let _datamilestoneprev = JSON.parse(localStorage.getItem(main.tablemilestone));
                let _datamilestonemerge = [];
                let _dataprev = [];
                let _datanext = response.data;
                let _datamerge = [];

                $.each(_datamilestoneprev, function(k,v){
                    if (v.noid == response.data.idmilestone) {
                        _dataprev = v.header;
                    }
                });

                if (params.type == 'save') {
                    // if (_dataprev) {
                        _datamerge = _dataprev.concat(_datanext);
                        $.each(_datamilestoneprev, function(k,v){
                            if (v.noid == response.data.idmilestone) {
                                _datamilestonemerge[k] = v;
                                _datamilestonemerge[k].header = _datamerge;
                            } else {
                                _datamilestonemerge[k] = v;
                            }
                        });
                        console.log(_datamilestonemerge)
                        localStorage.setItem(main.tablemilestone, JSON.stringify(_datamilestonemerge));
                    // }
                } else if (params.type == 'update') {
                    if (_dataprev) {
                        // _datamerge = _dataprev;
                        _datamerge = _datanext;
                        // console.table(_datamilestoneprev)
                        // _datamerge[params.keyheader] = _datanext;
                        $.each(_datamilestoneprev, function(k,v){
                            _datamilestonemerge[k] = v;
                            if (v.header.length > 0) {
                                $.each(v.header, function(k2,v2){
                                    if (v.noid == params.idmilestone && k2 == params.keyheader) {
                                        _datamilestonemerge[k].header[k2] = _datamerge;
                                    } else {
                                        _datamilestonemerge[k].header[k2] = v2;
                                    }
                                });
                            }
                        });
                        localStorage.setItem(main.tablemilestone, JSON.stringify(_datamilestonemerge));
                    }
                }

                if (params.type == 'save') hideFormHeader()

                collapseMilestone({idelement: params.idelement, alwaysopen: true})

                // getMilestoneAndHeader({data:JSON.parse(localStorage.getItem(main.tablemilestone)),idelement:params.idelement});
                getMilestoneAndHeader({data:JSON.parse(localStorage.getItem(main.tablemilestone))});
            }
        });
    }

    //DOCUMENT
    // $('#pagepanel').keyup(function(e) {
    //     if (e.keyCode === 13) {
    //         alert('asd');
    //         localStorage.setItem(main.table+'-fullscreen', 'false');
    //     }
    // });

    //FOCUS 
    $('#form-header #kode').on('keydown',function(e){if(e.keyCode==13){$('#form-header #kodereff').focus();}})
    $('#form-header #kodereff').on('keydown',function(e){if(e.keyCode==13){$('#form-header #tanggal').focus();}})
    $('#form-header #tanggal').on('change',function(e){
        changeTanggal('tanggal', 'tanggal-modal')
        $('#form-header #tanggaldue').focus()
    })
    $('#form-header #tanggaldue').on('change',function(e){
        let _tanggal = new Date($('#form-header #tanggal').val());
        let _tanggaldue = new Date($('#form-header #tanggaldue').val());

        if (_tanggal > _tanggaldue) {
            alert("'Tanggal Due' harus lebih besar dari 'Tanggal'");
            $('#form-header #tanggaldue').val('')
            return false
        }

        changeTanggal('tanggaldue', 'tanggaldue-modal')
        $('#form-header #idtypecostcontrol').select2('focus').trigger('change')
        thenTanggalDue(ready ? true : false)
    })

    $('#form-header #idtypecostcontrol').on('select2:close',function(e){$('#form-header #projectname').focus()})
    $('#form-header #projectname').on('keypress',function(e){if(e.keyCode==13){$('#form-header #projectlocation').focus()}})
    $('#form-header #projectlocation').on('keypress',function(e){if(e.keyCode==13){$('#form-header #idcardsales').select2('focus')}})
    $('#form-header #idcardsales').on('select2:close',function(e){$('#form-header #idcardcashier').select2('focus')})
    $('#form-header #idcardcashier').on('select2:close',function(e){$('#form-header #idcardcustomer').select2('focus')})
    $('#form-header #idcardcustomer').on('select2:close',function(e){$('#form-header #idgudang').select2('focus')})
    $('#form-header #idgudang').on('select2:close',function(e){$('#form-header #idcardpj').select2('focus')})
    $('#form-header #idcardpj').on('select2:close',function(e){$('#form-header #idcardkoor').select2('focus')})
    $('#form-header #idcardkoor').on('select2:close',function(e){$('#form-header #idcardsitekoor').select2('focus')})
    $('#form-header #idcardsitekoor').on('select2:close',function(e){$('#form-header #idcarddrafter').select2('focus')})
    // $('#form-header #idcarddrafter').on('select2:close',function(e){$('#form-header #idgudang').select2('focus')})

    // $('#form-internal #idcashbankbayar').on('select2:close',function(e){$('#form-internal #idakunpersediaan').select2('focus')})
    // $('#form-internal #idakunpersediaan').on('select2:close',function(e){$('#form-internal #idakunpurchase').select2('focus')})
    // $('#form-internal #idakunpurchase').on('select2:close',function(e){$('#form-detail #idinventor').select2('focus')})

    function thenIDCardSales(ready) {
        // ready ? $('#form-header #idcardsales').select2('focus') : $('#form-header #idcardsales').select2('close');
    }

    function thenIDCardCashier(ready) {
        // ready ? $('#form-header #idcardcashier').select2('focus') : $('#form-header #idcardcashier').select2('close');
    }

    function thenIDCustomer(ready) {
        // ready ? $('#form-header #idcardcustomer').select2('focus') : $('#form-header #idcardcustomer').select2('close');
    }

    function thenIDCompany(ready) {
        // ready ? $('#form-header #idcompany').select2('focus') : $('#form-header #idcompany').select2('close');
    }

    function thenIDDepartment(ready) {
        // ready ? $('#form-header #idgudang').select2('focus') : $('#form-header #idgudang').select2('close');
    }
    
    function thenIDCashbankBayar(ready) {
        // ready ? $('#form-header #idakunpersediaan').select2('focus') : $('#form-header #idakunpersediaan').select2('close');
    }

    function thenTanggalDue(ready) {
        // ready ? showModalCostControl() : hideModalCostControl();
        // $('#form-header #idtypecostcontrol').select2('focus')
    }

    $('#form-header #kodecostcontrol').on('keydown',function(e){if(e.keyCode==13){$('#form-header #cost-control').focus();}})

    $('#tab-internal').on('click',function(e){
        if (!$('#form-internal #idcashbankbayar').val()) {
            $('#form-internal #idcashbankbayar').select2('focus')
        } else {
            if (!$('#form-internal #idakunpersediaan').val()) {
                $('#form-internal #idakunpersediaan').select2('focus')
            } else {
                if (!$('#form-internal #idakunpurchase').val()) {
                    $('#form-internal #idakunpurchase').select2('focus')
                }
            }
        }
    })

    function keydownDetail(params) {
        alert($('#searchprev-'+params.idelement).val());return false
        // $('#btn-addheader-'+params.idelement).hide();
        // $('#btn-add-'+params.idelement).hide();
        // $('#btn-cancel-'+params.idelement).show();
        // $('#btn-save-'+params.idelement).hide();
        // $('#btn-saveheader-'+params.idelement).show();
        // $('#btn-view-'+params.idelement).hide();
        // $('#btn-edit-'+params.idelement).hide();
        // $('#btn-remove-'+params.idelement).hide();
        // $('#btn-collapse-'+params.idelement).hide();
        // $('#div-formadddetail-'+params.idelement).slideDown();
        showModalDetailPrev({searchprev:$('#searchprev-'+params.idelement).val()});
    }

    $('#searchprev').on('keydown',function(e){if(e.keyCode==13){
        showModalDetailPrev();
    }})

    $('#form-detail #idinventor').on("select2:close", function(e) {
        changeKodeInventor();
        $('#form-detail #namainventor').focus();
    });
    $('#form-detail #namainventor').on('keydown',function(e){if(e.keyCode==13){$('#form-detail #unitprice').focus();}})
    $('#form-detail #unitprice').on('keydown',function(e){if(e.keyCode==13){$('#form-detail #unitqty').focus();}})
    $('#form-detail #unitqty').on('keydown',function(e){if(e.keyCode==13){
        if ($('#form-detail #unitprice').val() || $('#form-detail #unitqty').val()) {
            $('#form-detail #save-detail').focus();
            saveDetail();
            $('#form-detail #idinventor').select2('focus');
        } else {
            $.notify({
                message: 'Complete data, please!'
            },{
                type: 'danger'
            });
        }
    }})
    $('#form-detail #save-detail').on('click',function(e){if(e.keyCode==13){$('#form-detail #idinventor').select2('focus');}})
    $('#form-modal-detail #idsatuan-modal').on('change',function(e){
        let konversi = $('#form-modal-detail #idsatuan-modal :selected').attr('data-invmsatuan-konversi')
        $('#form-modal-detail #konvsatuan-modal').val(konversi)
    })
    $('#form-modal-detail #idtax-modal').on('change',function(e){
        let prosentax = $('#form-modal-detail #idtax-modal :selected').attr('data-accmpajak-prosentax')
        sumTotal();
    })

    //MODAL DETAIL
    let midinventor = $('#form-modal-detail #idinventor-modal');
    let mnamainventor = $('#form-modal-detail #namainventor-modal');
    let mketerangan = $('#form-modal-detail #keterangan-modal');
    let midcompany = $('#form-modal-detail #idcompany-modal');
    let middepartment = $('#form-modal-detail #iddepartment-modal');
    let midgudang = $('#form-modal-detail #idgudang-modal');
    let midsatuan = $('#form-modal-detail #idsatuan-modal');
    let mkonvsatuan = $('#form-modal-detail #konvsatuan-modal');
    let munitqty = $('#form-modal-detail #unitqty-modal');
    let munitqtysisa = $('#form-modal-detail #unitqtysisa-modal');
    let munitprice = $('#form-modal-detail #unitprice-modal');
    let msubtotal = $('#form-modal-detail #subtotal-modal');
    let mdiscountvar = $('#form-modal-detail #discountvar-modal');
    let mdiscount = $('#form-modal-detail #discount-modal');
    let mnilaidisc = $('#form-modal-detail #nilaidisc-modal');
    let midtypetax = $('#form-modal-detail #idtypetax-modal');
    let midtax = $('#form-modal-detail #idtax-modal');
    let mprosentax = $('#form-modal-detail #prosentax-modal');
    let mnilaitax = $('#form-modal-detail #nilaitax-modal');
    let mhargatotal = $('#form-modal-detail #hargatotal-modal');
    let mupdatedetail = $('#form-modal-detail #update-detail');


    midinventor.on('select2:close', e => {
        midsatuan.val($('#form-modal-detail #idinventor-modal :selected').attr('data-invminventory-idsatuan')).trigger('change');
        munitprice.val($('#form-modal-detail #idinventor-modal :selected').attr('data-invminventory-purchaseprice'))
        validationSumTotal() && mnamainventor.focus();
        sumTotal();
    });

    mnamainventor.on('keypress', e => {
        if (e.keyCode == 13) {
            mketerangan.focus();
        }
    });

    mketerangan.on('keypress', e => {
        if (e.keyCode == 13) {
            midsatuan.select2('focus');
        }
    });

    midsatuan.on('select2:close', e => {
        validationSumTotal() && munitqty.focus();
        sumTotal();
    });

    munitqty.on('keyup', () => sumTotal());
    munitqty.on('keypress', e => {
        if (e.keyCode == 13 && validationSumTotal()) {
            munitprice.focus();
        }
    });

    munitprice.on('keyup', () => sumTotal());
    munitprice.on('keypress', e => { e.keyCode == 13 && mdiscount.focus(); });
    
    mdiscount.on('keyup', () => sumTotal());
    mdiscount.on('keypress', e => {
        if (e.keyCode == 13 && validationSumTotal()) {
            midtax.select2('focus');
        }
    });

    midtax.on('select2:close', e => {
        validationSumTotal() && mupdatedetail.focus();
        sumTotal();
    });

    function validationSumTotal() {
        if (midinventor.val() == '') {
            alert('Inventor is required');
            midinventor.select2('focus');
            return false;
        } else if (midsatuan.val() == '') {
            alert('Satuan is required');
            midsatuan.select2('focus');
            return false;
        } else if (munitqty.val() == '') {
            alert('Unit QTY is required');
            munitqty.focus();
            return false;
        } else if (mdiscount.val() == '') {
            alert('Discount is required');
            mdiscount.focus();
            return false;
        } else if (midtax.val() == '') {
            alert('Tax is required');
            midtax.select2('focus');
            return false;
        }

        return true;
    }

    function sumTotal() {
        // alert($('[name=searchtypeprev]:checked').val())
        let unitqty = $('#form-modal-detail #unitqty-modal');
        let unitqtysisa = $('#form-modal-detail #unitqtysisa-modal');
        let unitprice = $('#form-modal-detail #unitprice-modal');
        let subtotal = $('#form-modal-detail #subtotal-modal');
        let discount = $('#form-modal-detail #discount-modal');
        let nilaidisc = $('#form-modal-detail #nilaidisc-modal');
        let prosentax = $('#form-modal-detail #prosentax-modal');
        let _prosentax = $('#form-modal-detail #idtax-modal :selected').attr('data-accmpajak-prosentax');
        let nilaitax = $('#form-modal-detail #nilaitax-modal');
        let hargatotal = $('#form-modal-detail #hargatotal-modal');
        let resultsubtotal = parseInt(unitqty.val().replace(/[^0-9]/gi, ''))*parseInt(unitprice.val().replace(/[^0-9]/gi, ''));
        let resultnilaidisc = parseInt(resultsubtotal)*parseInt(discount.val().replace(/[^0-9]/gi, ''))/100;
        let resultprosentax = parseInt(_prosentax);
        let resultnilaitax = parseInt(resultsubtotal)*(_prosentax == undefined ? 0 : parseInt(_prosentax.replace(/[^0-9]/gi, '')))/100;
        let resulthargatotal = resultsubtotal-resultnilaidisc+resultnilaitax;

        unitqtysisa.val(unitqty.val());
        subtotal.val(resultsubtotal);
        nilaidisc.val(resultnilaidisc);
        prosentax.val(resultprosentax);
        nilaitax.val(resultnilaitax);
        hargatotal.val(resulthargatotal);
    }

    function getGenerateCode() {
        let noid = $('#form-header #noid').val();
        let formheader = {};
        let forminternal = {};
        let formfooter = {};
        let formmodaltanggal = {};
        let formmodalsupplier = {};

        $.each($('#form-header').serializeArray(), function(k, v){
            formheader[v.name] = v.value;
        });
        $.each($('#form-internal').serializeArray(), function(k, v){
            forminternal[v.name] = v.value;
        });
        $.each($('#form-footer').serializeArray(), function(k, v){
            formfooter[v.name] = v.value;
        });
        $.each($('#form-modal-tanggal').serializeArray(), function(k, v){
            formmodaltanggal[v.name] = v.value;
        });
        $.each($('#form-modal-supplier').serializeArray(), function(k, v){
            formmodalsupplier[v.name] = v.value;
        });

        $.ajax({
            type: 'POST',
            header:{
                'X-CSRF-TOKEN': $('#token').val()
            },
            url: '/generate_code'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('#token').val(),
                'idcostcontrol': $('#form-header #idcostcontrol').val(),
                'kodecostcontrol': $('#form-header #kodecostcontrol').val(),
                'noid': noid,
                'formheader': formheader,
                'forminternal': forminternal,
                'formfooter': formfooter,
                'formmodaltanggal': formmodaltanggal,
                'formmodalsupplier': formmodalsupplier
            },
            success: function(response) {
                $('#form-header #nourut').val(response.data.nourut);
                $('#form-header #kode').val(response.data.kode);
            }
        });
    }

    function getInfo() {
        $.ajax({
            type: 'POST',
            header: {
                'X-CSRF-TOKEN': $('#token').val()
            },
            url: '/get_info'+main.urlcode,
            dataType: 'json',
            data: {
                '_token': $('#token').val(),
            },
            success: function(response) {
                let pr = response.data.pr;
                let mttts = response.data.mttts;

                $('#btn-saveto').html(getButtonSaveTo(pr,mttts,false));
                
                $('#idstatustranc').val(response.data.mtypetranctypestatus.noid);
                $('#idstatustranc-info').css('background-color', response.data.mtypetranctypestatus.classcolor);
                $('.idstatustranc-info').text(response.data.mtypetranctypestatus.nama);
                $('#idstatuscard').val(response.data.mcarduser.noid);
                $('#idstatuscard-info').text(response.data.mcarduser.nama);
                $('#dostatus').val(response.data.dostatus);
                $('#dostatus-info').text(response.data.dostatus);

                $.when(
                    $('#form-header #idtypecostcontrol').val(response.data.idtypecostcontrol).trigger('change')
                ).then(function(){
                    getGenerateCode();
                })
                
                $('#form-header #idgudang').val(response.data.idgudang).trigger('change');
                $('#form-header #idcardsales').val('').trigger('change');
                $('#form-header #idcardcashier').val('').trigger('change');
                $('#form-header #idcardcustomer').val('').trigger('change');
            }
        })
    }

    function getLastSupplier() {
        // $.ajax({
        //     type: 'POST',
        //     header:{
        //         'X-CSRF-TOKEN': $('#token').val()
        //     },
        //     url: '/find_last_supplier'+main.urlcode,
        //     dataType: 'json',
        //     data: {
        //         '_token': $('#token').val(),
        //     },
        //     success: function(response) {
        //         $('#form-header #idcardsupplier').val(response.data.idcardsupplier).trigger('change');
        //         $('#form-modal-supplier #idakunsupplier-modal').val(response.data.idakunsupplier).trigger('change');
        //         $('#form-modal-supplier #suppliernama-modal').val(response.data.suppliernama);
        //         $('#form-modal-supplier #supplieraddress1-modal').val(response.data.supplieraddress1);
        //         $('#form-modal-supplier #supplieraddress2-modal').val(response.data.supplieraddress2);
        //         $('#form-modal-supplier #supplieraddressrt-modal').val(response.data.supplieraddressrt);
        //         $('#form-modal-supplier #supplieraddressrw-modal').val(response.data.supplieraddressrw);
        //         $('#form-modal-supplier #supplierlokasikel-modal').val(response.data.supplierlokasikel);
        //         $('#form-modal-supplier #supplierlokasikec-modal').val(response.data.supplierlokasikec);
        //         $('#form-modal-supplier #supplierlokasikab-modal').val(response.data.supplierlokasikab);
        //         $('#form-modal-supplier #supplierlokasiprop-modal').val(response.data.supplierlokasiprop);
        //         $('#form-modal-supplier #supplierlokasineg-modal').val(response.data.supplierlokasineg);
        //     }
        // });

        $('#form-header #idcardsupplier').val('').trigger('change');
        $('#form-modal-supplier #idakunsupplier-modal').val(0).trigger('change');
        $('#form-modal-supplier #suppliernama-modal').val('');
        $('#form-modal-supplier #supplieraddress1-modal').val('');
        $('#form-modal-supplier #supplieraddress2-modal').val('');
        $('#form-modal-supplier #supplieraddressrt-modal').val('');
        $('#form-modal-supplier #supplieraddressrw-modal').val('');
        $('#form-modal-supplier #supplierlokasikel-modal').val('');
        $('#form-modal-supplier #supplierlokasikec-modal').val('');
        $('#form-modal-supplier #supplierlokasikab-modal').val('');
        $('#form-modal-supplier #supplierlokasiprop-modal').val('');
        $('#form-modal-supplier #supplierlokasineg-modal').val('');
    }

    function resetForm() {
        $('#divform').html('')
    }

    function resetData() {
        $.each(maincms['panel'], function(k, v) {
            if (k == 0) {
                localStorage.removeItem('input-data');
            } else {
                localStorage.removeItem('input-data-'+v.noid);
            }
        })
    }

    function resetDataTab(panel_noid) {
        var maincms_widgetgridfield = new Array();
        $.each(maincms['widgetgridfield'], function(k, v) {
            $.each(v, function(k2, v2) {
                if (k2 == 0) {
                    maincms_widgetgridfield[panel_noid] = new Array();    
                }
                maincms_widgetgridfield[panel_noid][v2.fieldname] = v2;
            })
        })
        console.log('default')
        console.log(maincms_widgetgridfield)
        
        $.each(JSON.parse(localStorage.getItem('input-data-'+panel_noid)), function(k, v) {
            $.each(fieldnametab[panel_noid], function(k2, v2) {
                if (!v2.search('_lookup')) {
                    $("#tab-form-div-"+panel_noid+" div div #"+v2).attr('disabled', false);
                    $("#tab-form-div-"+panel_noid+" div div #"+v2).val(maincms_widgetgridfield[panel_noid][v2].coldefault);
                }
            })
        })
    }

    const showLoading = () => {
        localStorage.getItem(main.table+'-fullscreen') == 'true'
            ? $('#loading-fullscreen').show()
            : $('#loading').show();
    }

    const hideLoading = () => {
        localStorage.getItem(main.table+'-fullscreen') == 'true'
            ? $('#loading-fullscreen').hide()
            : $('#loading').hide();
    }

    function cancelData() {
        setTimeout(hideLoading(), 500);
        divform = '';
        another_selecteds = [];
        $('#form-detail div').hide();
        $('.btn-view').hide();
        $('#btn-saveto').html('');
        destroyTabDatatable()
        hideFormMilestone()

        undisableForm();
        undisableFormEmployee();
        undisableFormCustomer();
        undisableFormSupplier();
        unsetForm();
        unsetFormGeneral();
        unsetFormAddress();
        unsetFormContact();
        unsetFormUser();
        unsetFormPrivilidge();
        unsetFormEmployee();
        unsetFormCustomer();
        unsetFormSupplier();
        hideFormOthers();

        $('#tool_refresh').show(); // Hide button refresh
        $('#from-departement').hide(); // Show form
        $('#tool-kedua').hide(); // Show button cancel
        $('#tools-dropdown').show(); // Hide button tools
        $('#tabledepartemen').show(); // Hide datatable
        $('#tool-pertama').show(); // Show buton add data
        $('#space_filter_button').show(); // Hide filter di datatable
    }

    function disableForm() {
        $('#kode').attr('disabled', 'disabled');
        $('#firstname').attr('disabled', 'disabled');
        $('#middlename').attr('disabled', 'disabled');
        $('#lastname').attr('disabled', 'disabled');
        $('#namaalias').attr('disabled', 'disabled');
        $('#gelar').attr('disabled', 'disabled');
        $('#email').attr('disabled', 'disabled');
        $('#isgroupuser').attr('disabled', 'disabled');
        $('#isuser').attr('disabled', 'disabled');
        $('#isemployee').attr('disabled', 'disabled');
        $('#iscustomer').attr('disabled', 'disabled');
        $('#issupplier').attr('disabled', 'disabled');
        $('#is3rdparty').attr('disabled', 'disabled');
        $('#isactive').attr('disabled', 'disabled');
    }

    function disableFormGeneral() {
        $('#mcardgeneral_idgender').attr('disabled', 'disabled');
        $('#mcardgeneral_idreligion').attr('disabled', 'disabled');
        $('#mcardgeneral_idmarital').attr('disabled', 'disabled');
        $('#mcardgeneral_idnationality').attr('disabled', 'disabled');
        $('#mcardgeneral_idrace').attr('disabled', 'disabled');
        $('#mcardgeneral_idbloodtype').attr('disabled', 'disabled');
        $('#mcardgeneral_idlanguage1').attr('disabled', 'disabled');
        $('#mcardgeneral_idlanguage2').attr('disabled', 'disabled');
        $('#mcardgeneral_idlokasilahir').attr('disabled', 'disabled');
        $('#mcardgeneral_tempatlahir').attr('disabled', 'disabled');
        $('#mcardgeneral_dobirth').attr('disabled', 'disabled');
    }

    function disableFormAddress() {
        $('#mcardaddress_ismaincontact').attr('disabled', 'disabled');
        $('#mcardaddress_idtypeaddress').attr('disabled', 'disabled');
        $('#mcardaddress_alamat1').attr('disabled', 'disabled');
        $('#mcardaddress_alamat2').attr('disabled', 'disabled');
        $('#mcardaddress_alamatrt').attr('disabled', 'disabled');
        $('#mcardaddress_alamatrw').attr('disabled', 'disabled');
        $('#mcardaddress_idlokasiprop').attr('disabled', 'disabled');
        $('#mcardaddress_idlokasikab').attr('disabled', 'disabled');
        $('#mcardaddress_idlokasikec').attr('disabled', 'disabled');
        $('#mcardaddress_idlokasikel').attr('disabled', 'disabled');
    }

    function disableFormContact() {
        $('#mcardcontact_idtypecontact').attr('disabled', 'disabled');
        $('#mcardcontact_contactname').attr('disabled', 'disabled');
        $('#mcardcontact_contactvar').attr('disabled', 'disabled');
    }

    function disableFormUser() {
        $('#mcarduser_myusername').attr('disabled', 'disabled');
        $('#mcarduser_mypassword').attr('disabled', 'disabled');
        $('#mcarduser_groupuser').attr('disabled', 'disabled');
    }

    function disableFormPrivilidge() {
        $('#mcardprivilidge_idmenu').attr('disabled', 'disabled');
        $('#mcardprivilidge_menuparent').attr('disabled', 'disabled');
        $('#mcardprivilidge_menugroup').attr('disabled', 'disabled');
        $('#mcardprivilidge_isvisible').attr('disabled', 'disabled');
        $('#mcardprivilidge_isenable').attr('disabled', 'disabled');
        $('#mcardprivilidge_isadd').attr('disabled', 'disabled');
        $('#mcardprivilidge_isedit').attr('disabled', 'disabled');
        $('#mcardprivilidge_isdelete').attr('disabled', 'disabled');
        $('#mcardprivilidge_isdeleteother').attr('disabled', 'disabled');
        $('#mcardprivilidge_isview').attr('disabled', 'disabled');
        $('#mcardprivilidge_isreport').attr('disabled', 'disabled');
        $('#mcardprivilidge_isreportdetail').attr('disabled', 'disabled');
        $('#mcardprivilidge_isconfirm').attr('disabled', 'disabled');
        $('#mcardprivilidge_isunconfirm').attr('disabled', 'disabled');
        $('#mcardprivilidge_ispost').attr('disabled', 'disabled');
        $('#mcardprivilidge_isunpost').attr('disabled', 'disabled');
        $('#mcardprivilidge_isprintdaftar').attr('disabled', 'disabled');
        $('#mcardprivilidge_isprintdetail').attr('disabled', 'disabled');
        $('#mcardprivilidge_isshowaudit').attr('disabled', 'disabled');
        $('#mcardprivilidge_isshowlog').attr('disabled', 'disabled');
        $('#mcardprivilidge_isexporttoxcl').attr('disabled', 'disabled');
        $('#mcardprivilidge_isexporttocsv').attr('disabled', 'disabled');
        $('#mcardprivilidge_isexporttopdf').attr('disabled', 'disabled');
        $('#mcardprivilidge_isexporttohtml').attr('disabled', 'disabled');
        $('#mcardprivilidge_iscustom1').attr('disabled', 'disabled');
        $('#mcardprivilidge_iscustom2').attr('disabled', 'disabled');
        $('#mcardprivilidge_iscustom3').attr('disabled', 'disabled');
        $('#mcardprivilidge_iscustom4').attr('disabled', 'disabled');
        $('#mcardprivilidge_iscustom5').attr('disabled', 'disabled');
        $('#mcardprivilidge_isproduction01').attr('disabled', 'disabled');
        $('#mcardprivilidge_isproduction02').attr('disabled', 'disabled');
        $('#mcardprivilidge_isproduction03').attr('disabled', 'disabled');
        $('#mcardprivilidge_isproduction04').attr('disabled', 'disabled');
        $('#mcardprivilidge_isproduction05').attr('disabled', 'disabled');
        $('#mcardprivilidge_isproduction06').attr('disabled', 'disabled');
    }

    function disableFormEmployee() {
        $('#mce_unitkerja').attr('disabled', 'disabled');
        $('#mce_nip').attr('disabled', 'disabled');
        $('#mce_idcompany').attr('disabled', 'disabled');
        $('#mce_iddepartment').attr('disabled', 'disabled');
        $('#mce_idgudang').attr('disabled', 'disabled');
        $('#mce_reportto').attr('disabled', 'disabled');
        $('#mce_gelardepan').attr('disabled', 'disabled');
        $('#mce_gelarbelakang').attr('disabled', 'disabled');
        $('#mce_namalengkap').attr('disabled', 'disabled');
        $('#mce_idpangkatgol').attr('disabled', 'disabled');
        $('#mce_pangkatgol').attr('disabled', 'disabled');
        $('#mce_pangkatgolkode').attr('disabled', 'disabled');
        $('#mce_dotmtpangkat').attr('disabled', 'disabled');
        $('#mce_jabatannama').attr('disabled', 'disabled');
        $('#mce_idstrata').attr('disabled', 'disabled');
        $('#mce_pendidikannama').attr('disabled', 'disabled');
        $('#mce_pendidikanlulus').attr('disabled', 'disabled');
        $('#mce_tempatlahir').attr('disabled', 'disabled');
        $('#mce_dob').attr('disabled', 'disabled');
        $('#mce_idgender').attr('disabled', 'disabled');
        $('#mce_idreligion').attr('disabled', 'disabled');
        $('#mce_idmarital').attr('disabled', 'disabled');
        $('#mce_alamat').attr('disabled', 'disabled');
        $('#mce_alamatjl').attr('disabled', 'disabled');
        $('#mce_idlokasiprop').attr('disabled', 'disabled');
        $('#mce_idlokasikota').attr('disabled', 'disabled');
        $('#mce_idlokasikec').attr('disabled', 'disabled');
        $('#mce_idlokasikel').attr('disabled', 'disabled');
        $('#mce_nomobile').attr('disabled', 'disabled');
        $('#mce_nophone').attr('disabled', 'disabled');
    }

    function disableFormCustomer() {
        $('#mcc_kode').attr('disabled', 'disabled');
        $('#mcc_nama').attr('disabled', 'disabled');
        $('#mcc_namaalias').attr('disabled', 'disabled');
        $('#mcc_keterangan').attr('disabled', 'disabled');
        $('#mcc_isactive').attr('disabled', 'disabled');
        $('#mcc_npwp').attr('disabled', 'disabled');
        $('#mcc_alamat1').attr('disabled', 'disabled');
        $('#mcc_alamat2').attr('disabled', 'disabled');
        $('#mcc_alamatrt').attr('disabled', 'disabled');
        $('#mcc_alamatrw').attr('disabled', 'disabled');
        $('#mcc_idlokasiprop').attr('disabled', 'disabled');
        $('#mcc_idlokasikota').attr('disabled', 'disabled');
        $('#mcc_idlokasikec').attr('disabled', 'disabled');
        $('#mcc_idlokasikel').attr('disabled', 'disabled');
        $('#mcc_nophone').attr('disabled', 'disabled');
        $('#mcc_nofax').attr('disabled', 'disabled');
        $('#mcc_email').attr('disabled', 'disabled');
    }

    function disableFormSupplier() {
        $('#mcs_nama').attr('disabled', 'disabled');
        $('#mcs_alamat').attr('disabled', 'disabled');
        $('#mcs_alamatjl').attr('disabled', 'disabled');
        $('#mcs_idlokasiprop').attr('disabled', 'disabled');
        $('#mcs_idlokasikota').attr('disabled', 'disabled');
        $('#mcs_idlokasikec').attr('disabled', 'disabled');
        $('#mcs_idlokasikel').attr('disabled', 'disabled');
        $('#mcs_alamatkec').attr('disabled', 'disabled');
        $('#mcs_corebussiness').attr('disabled', 'disabled');
        $('#mcs_npwp').attr('disabled', 'disabled');
        $('#mcs_nomobile').attr('disabled', 'disabled');
        $('#mcs_nophone').attr('disabled', 'disabled');
        $('#mcs_nofax').attr('disabled', 'disabled');
        $('#mcs_companyemail').attr('disabled', 'disabled');
        $('#mcs_companyweb').attr('disabled', 'disabled');
        $('#mcs_termijnpayment').attr('disabled', 'disabled');
    }

    function undisableForm() {
        $('#kode').removeAttr('disabled');
        $('#firstname').removeAttr('disabled');
        $('#middlename').removeAttr('disabled');
        $('#lastname').removeAttr('disabled');
        $('#namaalias').removeAttr('disabled');
        $('#gelar').removeAttr('disabled');
        $('#email').removeAttr('disabled');
        $('#isgroupuser').removeAttr('disabled');
        $('#isuser').removeAttr('disabled');
        $('#isemployee').removeAttr('disabled');
        $('#iscustomer').removeAttr('disabled');
        $('#issupplier').removeAttr('disabled');
        $('#is3rdparty').removeAttr('disabled');
        $('#isactive').removeAttr('disabled');
    }

    function undisableFormGeneral() {
        $('#mcardgeneral_idgender').removeAttr('disabled');
        $('#mcardgeneral_idreligion').removeAttr('disabled');
        $('#mcardgeneral_idmarital').removeAttr('disabled');
        $('#mcardgeneral_idnationality').removeAttr('disabled');
        $('#mcardgeneral_idrace').removeAttr('disabled');
        $('#mcardgeneral_idbloodtype').removeAttr('disabled');
        $('#mcardgeneral_idlanguage1').removeAttr('disabled');
        $('#mcardgeneral_idlanguage2').removeAttr('disabled');
        $('#mcardgeneral_idlokasilahir').removeAttr('disabled');
        $('#mcardgeneral_tempatlahir').removeAttr('disabled');
        $('#mcardgeneral_dobirth').removeAttr('disabled');
    }

    function undisableFormAddress() {
        $('#mcardaddress_ismaincontact').removeAttr('disabled');
        $('#mcardaddress_idtypeaddress').removeAttr('disabled');
        $('#mcardaddress_alamat1').removeAttr('disabled');
        $('#mcardaddress_alamat2').removeAttr('disabled');
        $('#mcardaddress_alamatrt').removeAttr('disabled');
        $('#mcardaddress_alamatrw').removeAttr('disabled');
        $('#mcardaddress_idlokasiprop').removeAttr('disabled');
        $('#mcardaddress_idlokasikab').removeAttr('disabled');
        $('#mcardaddress_idlokasikec').removeAttr('disabled');
        $('#mcardaddress_idlokasikel').removeAttr('disabled');
    }

    function undisableFormContact() {
        $('#mcardcontact_idtypecontact').removeAttr('disabled');
        $('#mcardcontact_contactname').removeAttr('disabled');
        $('#mcardcontact_contactvar').removeAttr('disabled');
    }

    function undisableFormUser() {
        $('#mcarduser_myusername').removeAttr('disabled');
        $('#mcarduser_mypassword').removeAttr('disabled');
        $('#mcarduser_groupuser').removeAttr('disabled');
    }

    function undisableFormPrivilidge() {
        $('#mcardprivilidge_idmenu').removeAttr('disabled');
        $('#mcardprivilidge_menuparent').removeAttr('disabled');
        $('#mcardprivilidge_menugroup').removeAttr('disabled');
        $('#mcardprivilidge_isvisible').removeAttr('disabled');
        $('#mcardprivilidge_isenable').removeAttr('disabled');
        $('#mcardprivilidge_isadd').removeAttr('disabled');
        $('#mcardprivilidge_isedit').removeAttr('disabled');
        $('#mcardprivilidge_isdelete').removeAttr('disabled');
        $('#mcardprivilidge_isdeleteother').removeAttr('disabled');
        $('#mcardprivilidge_isview').removeAttr('disabled');
        $('#mcardprivilidge_isreport').removeAttr('disabled');
        $('#mcardprivilidge_isreportdetail').removeAttr('disabled');
        $('#mcardprivilidge_isconfirm').removeAttr('disabled');
        $('#mcardprivilidge_isunconfirm').removeAttr('disabled');
        $('#mcardprivilidge_ispost').removeAttr('disabled');
        $('#mcardprivilidge_isunpost').removeAttr('disabled');
        $('#mcardprivilidge_isprintdaftar').removeAttr('disabled');
        $('#mcardprivilidge_isprintdetail').removeAttr('disabled');
        $('#mcardprivilidge_isshowaudit').removeAttr('disabled');
        $('#mcardprivilidge_isshowlog').removeAttr('disabled');
        $('#mcardprivilidge_isexporttoxcl').removeAttr('disabled');
        $('#mcardprivilidge_isexporttocsv').removeAttr('disabled');
        $('#mcardprivilidge_isexporttopdf').removeAttr('disabled');
        $('#mcardprivilidge_isexporttohtml').removeAttr('disabled');
        $('#mcardprivilidge_iscustom1').removeAttr('disabled');
        $('#mcardprivilidge_iscustom2').removeAttr('disabled');
        $('#mcardprivilidge_iscustom3').removeAttr('disabled');
        $('#mcardprivilidge_iscustom4').removeAttr('disabled');
        $('#mcardprivilidge_iscustom5').removeAttr('disabled');
        $('#mcardprivilidge_isproduction01').removeAttr('disabled');
        $('#mcardprivilidge_isproduction02').removeAttr('disabled');
        $('#mcardprivilidge_isproduction03').removeAttr('disabled');
        $('#mcardprivilidge_isproduction04').removeAttr('disabled');
        $('#mcardprivilidge_isproduction05').removeAttr('disabled');
        $('#mcardprivilidge_isproduction06').removeAttr('disabled');
    }

    function undisableFormEmployee() {
        $('#mce_unitkerja').removeAttr('disabled');
        $('#mce_nip').removeAttr('disabled');
        $('#mce_idcompany').removeAttr('disabled');
        $('#mce_iddepartment').removeAttr('disabled');
        $('#mce_idgudang').removeAttr('disabled');
        $('#mce_reportto').removeAttr('disabled');
        $('#mce_gelardepan').removeAttr('disabled');
        $('#mce_gelarbelakang').removeAttr('disabled');
        $('#mce_namalengkap').removeAttr('disabled');
        $('#mce_idpangkatgol').removeAttr('disabled');
        $('#mce_pangkatgol').removeAttr('disabled');
        $('#mce_pangkatgolkode').removeAttr('disabled');
        $('#mce_dotmtpangkat').removeAttr('disabled');
        $('#mce_jabatannama').removeAttr('disabled');
        $('#mce_idstrata').removeAttr('disabled');
        $('#mce_pendidikannama').removeAttr('disabled');
        $('#mce_pendidikanlulus').removeAttr('disabled');
        $('#mce_tempatlahir').removeAttr('disabled');
        $('#mce_dob').removeAttr('disabled');
        $('#mce_idgender').removeAttr('disabled');
        $('#mce_idreligion').removeAttr('disabled');
        $('#mce_idmarital').removeAttr('disabled');
        $('#mce_alamat').removeAttr('disabled');
        $('#mce_alamatjl').removeAttr('disabled');
        $('#mce_idlokasiprop').removeAttr('disabled');
        $('#mce_idlokasikota').removeAttr('disabled');
        $('#mce_idlokasikec').removeAttr('disabled');
        $('#mce_idlokasikel').removeAttr('disabled');
        $('#mce_nomobile').removeAttr('disabled');
        $('#mce_nophone').removeAttr('disabled');
    }

    function undisableFormCustomer() {
        $('#mcc_kode').removeAttr('disabled');
        $('#mcc_nama').removeAttr('disabled');
        $('#mcc_namaalias').removeAttr('disabled');
        $('#mcc_keterangan').removeAttr('disabled');
        $('#mcc_isactive').removeAttr('disabled');
        $('#mcc_npwp').removeAttr('disabled');
        $('#mcc_alamat1').removeAttr('disabled');
        $('#mcc_alamat2').removeAttr('disabled');
        $('#mcc_alamatrt').removeAttr('disabled');
        $('#mcc_alamatrw').removeAttr('disabled');
        $('#mcc_idlokasiprop').removeAttr('disabled');
        $('#mcc_idlokasikota').removeAttr('disabled');
        $('#mcc_idlokasikec').removeAttr('disabled');
        $('#mcc_idlokasikel').removeAttr('disabled');
        $('#mcc_nophone').removeAttr('disabled');
        $('#mcc_nofax').removeAttr('disabled');
        $('#mcc_email').removeAttr('disabled');
    }

    function undisableFormSupplier() {
        $('#mcs_nama').removeAttr('disabled');
        $('#mcs_alamat').removeAttr('disabled');
        $('#mcs_alamatjl').removeAttr('disabled');
        $('#mcs_idlokasiprop').removeAttr('disabled');
        $('#mcs_idlokasikota').removeAttr('disabled');
        $('#mcs_idlokasikec').removeAttr('disabled');
        $('#mcs_idlokasikel').removeAttr('disabled');
        $('#mcs_alamatkec').removeAttr('disabled');
        $('#mcs_corebussiness').removeAttr('disabled');
        $('#mcs_npwp').removeAttr('disabled');
        $('#mcs_nomobile').removeAttr('disabled');
        $('#mcs_nophone').removeAttr('disabled');
        $('#mcs_nofax').removeAttr('disabled');
        $('#mcs_companyemail').removeAttr('disabled');
        $('#mcs_companyweb').removeAttr('disabled');
        $('#mcs_termijnpayment').removeAttr('disabled');
    }

    function unsetForm() {
        $('#kode').val('');
        $('#firstname').val('');
        $('#middlename').val('');
        $('#lastname').val('');
        $('#gelar').val('');
        $('#email').val('');
        $('#isgroupuser').prop('checked', false);
        $('#isuser').prop('checked', false);
        $('#isemployee').prop('checked', false);
        $('#iscustomer').prop('checked', false);
        $('#issupplier').prop('checked', false);
        $('#is3rdparty').prop('checked', false);
        $('#isactive').prop('checked', false);
    }

    function unsetFormGeneral() {
        $('#mcardgeneral_idgender').val('').trigger('change');
        $('#mcardgeneral_idreligion').val('').trigger('change');
        $('#mcardgeneral_idmarital').val('').trigger('change');
        $('#mcardgeneral_idnationality').val('').trigger('change');
        $('#mcardgeneral_idrace').val('').trigger('change');
        $('#mcardgeneral_idbloodtype').val('').trigger('change');
        $('#mcardgeneral_idlanguage1').val('').trigger('change');
        $('#mcardgeneral_idlanguage2').val('').trigger('change');
        $('#mcardgeneral_idlokasilahir').val('').trigger('change');
        $('#mcardgeneral_tempatlahir').val('');
        $('#mcardgeneral_dobirth').val('');
    }

    function unsetFormAddress() {
        $('#mcardaddress_ismaincontact').prop('checked', false);
        $('#mcardaddress_idtypeaddress').val('').trigger('change');
        $('#mcardaddress_alamat1').val('');
        $('#mcardaddress_alamat2').val('');
        $('#mcardaddress_alamatrt').val('');
        $('#mcardaddress_alamatrw').val('');
        $('#mcardaddress_idlokasiprop').val('').trigger('change');
        $('#mcardaddress_idlokasikab').val('').trigger('change');
        $('#mcardaddress_idlokasikec').val('').trigger('change');
        $('#mcardaddress_idlokasikel').val('').trigger('change');
    }

    function unsetFormContact() {
        $('#mcardcontact_idtypecontact').val('').trigger('change');
        $('#mcardcontact_contactname').val('');
        $('#mcardcontact_contactvar').val('');
    }

    function unsetFormUser() {
        $('#mcarduser_myusername').val('');
        $('#mcarduser_mypassword').val('');
        $('#mcarduser_groupuser').val('');
    }

    function unsetFormPrivilidge() {
        $('#mcardprivilidge_idmenu').val('');
        $('#mcardprivilidge_menuparent').val('');
        $('#mcardprivilidge_menugroup').val('');
        $('#mcardprivilidge_isvisible').prop('checked', false);
        $('#mcardprivilidge_isenable').prop('checked', false);
        $('#mcardprivilidge_isadd').prop('checked', false);
        $('#mcardprivilidge_isedit').prop('checked', false);
        $('#mcardprivilidge_isdelete').prop('checked', false);
        $('#mcardprivilidge_isdeleteother').prop('checked', false);
        $('#mcardprivilidge_isview').prop('checked', false);
        $('#mcardprivilidge_isreport').prop('checked', false);
        $('#mcardprivilidge_isreportdetail').prop('checked', false);
        $('#mcardprivilidge_isconfirm').prop('checked', false);
        $('#mcardprivilidge_isunconfirm').prop('checked', false);
        $('#mcardprivilidge_ispost').prop('checked', false);
        $('#mcardprivilidge_isunpost').prop('checked', false);
        $('#mcardprivilidge_isprintdaftar').prop('checked', false);
        $('#mcardprivilidge_isprintdetail').prop('checked', false);
        $('#mcardprivilidge_isshowaudit').prop('checked', false);
        $('#mcardprivilidge_isshowlog').prop('checked', false);
        $('#mcardprivilidge_isexporttoxcl').prop('checked', false);
        $('#mcardprivilidge_isexporttocsv').prop('checked', false);
        $('#mcardprivilidge_isexporttopdf').prop('checked', false);
        $('#mcardprivilidge_isexporttohtml').prop('checked', false);
        $('#mcardprivilidge_iscustom1').prop('checked', false);
        $('#mcardprivilidge_iscustom2').prop('checked', false);
        $('#mcardprivilidge_iscustom3').prop('checked', false);
        $('#mcardprivilidge_iscustom4').prop('checked', false);
        $('#mcardprivilidge_iscustom5').prop('checked', false);
        $('#mcardprivilidge_isproduction1').prop('checked', false);
        $('#mcardprivilidge_isproduction2').prop('checked', false);
        $('#mcardprivilidge_isproduction3').prop('checked', false);
        $('#mcardprivilidge_isproduction4').prop('checked', false);
        $('#mcardprivilidge_isproduction5').prop('checked', false);
        $('#mcardprivilidge_isproduction6').prop('checked', false);
    }

    function unsetFormEmployee() {
        $('#mce_unitkerja').val('').trigger('change');
        $('#mce_nip').val('');
        $('#mce_idcompany').val('').trigger('change');
        $('#mce_iddepartment').val('').trigger('change');
        $('#mce_idgudang').val('').trigger('change');
        $('#mce_reportto').val('').trigger('change');
        $('#mce_gelardepan').val('');
        $('#mce_gelarbelakang').val('');
        $('#mce_namalengkap').val('');
        $('#mce_idpangkatgol').val('').trigger('change');
        $('#mce_pangkatgol').val('');
        $('#mce_pangkatgolkode').val('');
        $('#mce_dotmtpangkat').val('');
        $('#mce_jabatannama').val('');
        $('#mce_idstrata').val('');
        $('#mce_pendidikannama').val('');
        $('#mce_pendidikanlulus').val('');
        $('#mce_tempatlahir').val('');
        $('#mce_dob').val('');
        $('#mce_idgender').val('').trigger('change');
        $('#mce_idreligion').val('').trigger('change');
        $('#mce_idmarital').val('').trigger('change');
        $('#mce_alamat').val('');
        $('#mce_alamatjl').val('');
        $('#mce_idlokasiprop').val('').trigger('change');
        $('#mce_idlokasikota').val('').trigger('change');
        $('#mce_idlokasikec').val('').trigger('change');
        $('#mce_idlokasikel').val('').trigger('change');
        $('#mce_nomobile').val('');
        $('#mce_nophone').val('');
    }

    function unsetFormCustomer() {
        $('#mcc_kode').val('');
        $('#mcc_nama').val('');
        $('#mcc_namaalias').val('');
        $('#mcc_keterangan').val('');
        $('#mcc_isactive').val('');
        $('#mcc_npwp').val('');
        $('#mcc_alamat1').val('');
        $('#mcc_alamat2').val('');
        $('#mcc_alamatrt').val('');
        $('#mcc_alamatrw').val('');
        $('#mcc_idlokasiprop').val('').trigger('change');
        $('#mcc_idlokasikota').val('').trigger('change');
        $('#mcc_idlokasikec').val('').trigger('change');
        $('#mcc_idlokasikel').val('').trigger('change');
        $('#mcc_nophone').val('');
        $('#mcc_nofax').val('');
        $('#mcc_email').val('');
    }

    function unsetFormSupplier() {
        $('#mcs_nama').val('');
        $('#mcs_alamat').val('');
        $('#mcs_alamatjl').val('');
        $('#mcs_idlokasiprop').val('').trigger('change');
        $('#mcs_idlokasikota').val('').trigger('change');
        $('#mcs_idlokasikec').val('').trigger('change');
        $('#mcs_idlokasikel').val('').trigger('change');
        $('#mcs_alamatkec').val('');
        $('#mcs_corebussiness').val('');
        $('#mcs_npwp').val('');
        $('#mcs_nomobile').val('');
        $('#mcs_nophone').val('');
        $('#mcs_nofax').val('');
        $('#mcs_companyemail').val('');
        $('#mcs_companyweb').val('');
        $('#mcs_termijnpayment').val('');
    }

    function hideFormOthers() {
        $('#mcardothers').hide();
    }

    function unsetFormHeader() {
        $('#form-header #kode').val('');
        $('#form-header #kodereff').val('');
        $('#form-header #idstatustranc').val(1).trigger('change');
        $('#form-header #idstatustranc').removeAttr('style');
        // $('#form-header #idstatustranc-info').css('background-color', '<?= @$color[@$mtypetranctypestatus_classcolor] ?>');
        $('#form-header #idstatustranc-info b').text('<?= @$mtypetranctypestatus_nama ?>');
        if (!$('#form-header #idstatuscard').val() == '') {
            $('#form-header #idstatuscard').val('').trigger('change');
        }
        $('#form-header #dostatus').val('');
        $('#form-header #tanggal').val('<?= @$defaulttanggal ?>');
        $('#form-header #tanggaldue').val('<?= @$defaulttanggaldue ?>');
        // if (!$('#form-header #idcardsupplier').val() == '') {
        //     $('#form-header #idcardsupplier').val('').trigger('change');
        // }
        if (!$('#form-header #idcardsales').val() == '') {
            $('#form-header #idcardsales #idcardsales-empty').attr('selected', 'selected');
            $('#select2-idcardsales-container').text($('#form-header #idcardsales #idcardsales-empty').text());
        }
        if (!$('#form-header #idcardcashier').val() == '') {
            $('#form-header #idcardcashier #idcardcashier-empty').attr('selected', 'selected');
            $('#select2-idcardcashier-container').text($('#form-header #idcardcashier #idcardcashier-empty').text());
        }
        if (!$('#form-header #idcardcustomer').val() == '') {
            $('#form-header #idcardcustomer #idcardcustomer-empty').attr('selected', 'selected');
            $('#select2-idcardcustomer-container').text($('#form-header #idcardcustomer #idcardcustomer-empty').text());
        }
        if (!$('#form-header #idcompany').val() == '') {
            $('#form-header #idcompany #idcompany-empty').attr('selected', 'selected');
            $('#select2-idcompany-container').text($('#form-header #idcompany #idcompany-empty').text());
        }
        if (!$('#form-header #iddepartment').val() == '') {
            $('#form-header #iddepartment #iddepartment-empty').attr('selected', 'selected');
            $('#select2-iddepartment-container').text($('#form-header #iddepartment #iddepartment-empty').text());
        }
        if (!$('#form-header #idgudang').val() == '') {
            $('#form-header #idgudang #idgudang-empty').attr('selected', 'selected');
            $('#select2-idgudang-container').text($('#form-header #idgudang #idgudang-empty').text());
        }
        if (!$('#form-header #idcardpj').val() == '') {
            $('#form-header #idcardpj #idcardpj-empty').attr('selected', 'selected');
            $('#select2-idcardpj-container').text($('#form-header #idcardpj #idcardpj-empty').text());
        }
        if (!$('#form-header #idcardkoor').val() == '') {
            $('#form-header #idcardkoor #idcardkoor-empty').attr('selected', 'selected');
            $('#select2-idcardkoor-container').text($('#form-header #idcardkoor #idcardkoor-empty').text());
        }
        if (!$('#form-header #idcardsitekoor').val() == '') {
            $('#form-header #idcardsitekoor #idcardsitekoor-empty').attr('selected', 'selected');
            $('#select2-idcardsitekoor-container').text($('#form-header #idcardsitekoor #idcardsitekoor-empty').text());
        }
        if (!$('#form-header #idcarddrafter').val() == '') {
            $('#form-header #idcarddrafter #idcarddrafter-empty').attr('selected', 'selected');
            $('#select2-idcarddrafter-container').text($('#form-header #idcarddrafter #idcarddrafter-empty').text());
        }
        $('#form-header #idtypecostcontrol').val('').trigger('change');
        $('#form-header #idcostcontrol').val('');
        $('#form-header #kodecostcontrol').val('');
        $('#form-header #projectname').val('');
        $('#form-header #projectlocation').val('');
    }

    function unsetFormInternal() {
        if (!$('#form-internal #idcashbankbayar').val() == '') {
            $('#form-internal #idcashbankbayar #idcashbankbayar-empty').attr('selected', 'selected');
            $('#select2-idcashbankbayar-container').text($('#form-internal #idcashbankbayar #idcashbankbayar-empty').text());
        }
        if (!$('#form-internal #idakunpersediaan').val() == '') {
            $('#form-internal #idakunpersediaan #idakunpersediaan-empty').attr('selected', 'selected');
            $('#select2-idakunpersediaan-container').text($('#form-internal #idakunpersediaan #idakunpersediaan-empty').text());
        }
        if (!$('#form-internal #idakunpurchase').val() == '') {
            $('#form-internal #idakunpurchase #idakunpurchase-empty').attr('selected', 'selected');
            $('#select2-idakunpurchase-container').text($('#form-internal #idakunpurchase #idakunpurchase-empty').text());
        }
    }

    function unsetFormModalTanggal() {
        $('#form-modal-tanggal #tanggal-modal').val('<?= @$defaulttanggal ?>');
        $('#form-modal-tanggal #tanggaltax-modal').val('');
        $('#form-modal-tanggal #tanggaldue-modal').val('<?= @$defaulttanggaldue ?>');
    }

    function unsetFormModalSupplier() {
        $('#form-modal-supplier #idcardsupplier-modal').val('');
        if (!$('#form-modal-supplier #idakunsupplier').val() == '') {
            $('#form-modal-supplier #idakunsupplier').val('').trigger('change');
        }
        $('#form-modal-supplier #suppliernama-modal').val('');
        $('#form-modal-supplier #supplieraddress1-modal').val('');
        $('#form-modal-supplier #supplieraddress2-modal').val('');
        $('#form-modal-supplier #supplieraddressrt-modal').val('');
        $('#form-modal-supplier #supplieraddressrw-modal').val('');
        $('#form-modal-supplier #supplierlokasikel-modal').val('');
        $('#form-modal-supplier #supplierlokasikec-modal').val('');
        $('#form-modal-supplier #supplierlokasikab-modal').val('');
        $('#form-modal-supplier #supplierlokasiprop-modal').val('');
        $('#form-modal-supplier #supplierlokasineg-modal').val('');
    }
    
    function unsetFormFooter() {
        $('#form-footer #keterangan').val('');
        $('#form-footer #subtotal').val(0);
        $('#form-footer #total').val(0);
    }

    function view(id) {
        $.when(
            $.ajax({
                url: '/find_tmd',
                type: "POST",
                dataType: 'json',
                header:{
                    'X-CSRF-TOKEN':$('#token').val()
                },
                data: {
                    "id" : id,
                    "namatable" : $("#namatable").val(),
                    "_token": $('#token').val(),
                    maincms: JSON.stringify(maincms)
                },
                success: function (response) {
                    return response;
                }
            })
        ).then(function(responsefind) {
            statusadd = false;
            statusdetail = false;
            nownoid = id;
            $.each(maincms['widget'], function(k, v) {
                getNextNoidTab(v.maintable, v.noid)
            })
            resetForm();
            resetData();
            getCmsTab(statusadd);
            getForm(statusadd, maincms['widget'][0]['noid'], 'divform', responsefind);
            // alert(response)
            // getCmsTabDatatable();


            editid = id;

            // var x = document.getElementById("from-departement");
            // var x2 = document.getElementById("tabledepartemen");
            // var y = document.getElementById("tool-kedua");
            // var y2 = document.getElementById("tool-pertama");
            // var space_filter_button = $('#space_filter_button');

            // if (x.style.display === "none") {
            //     x.style.display = "block";
            //     x2.style.display = "none";
            //     y.style.display = "block";
            //     y2.style.display = "none";
            //     space_filter_button.css('display', 'none');
            // } else {
            //     x.style.display = "none";
            //     x2.style.display = "block";
            //     space_filter_button.css('display', 'block');
            // }

            $('#from-departement').show();
            $('#tabledepartemen').hide();
            $('#tool-kedua').show();
            $('#tool-pertama').hide();
            $('#space_filter_button').hide();
            $('#tool_refresh').hide();
            $('#tool_filter').hide()
            $('#tool_refresh_up').hide();

            // $.ajax({
            //     url: '/find_tmd',
            //     type: "POST",
            //     dataType: 'json',
            //     header:{
            //         'X-CSRF-TOKEN':$('#token').val()
            //     },
            //     data: {
            //         "id" : id,
            //         "namatable" : $("#namatable").val(),
            //         "_token": $('#token').val(),
            //         maincms: JSON.stringify(maincms)
            //     },
            //     success: function (response) {
                    nownoid = responsefind.nownoid;

                    var maincms_panel = new Array();
                    $.each(maincms['panel'], function(k, v) {
                        $.each(maincms['widget'], function(k2, v2) {
                            if (v.idwidget == v2.noid) {
                                maincms_panel[v.noid] = v2;
                            }
                        })
                    })

                    // console.log(response.result_detail)
                    var key = 0;
                    $.each(responsefind.result_detail, function(k, v) {
                        if (key == 0) {
                            nownoidtab = maincms_panel[k].noid
                        }
                        // alert(k)
                        // console.log(v)
                        // localStorage.removeItem('input-data-'+k)
                        localStorage.setItem('input-data-'+k, JSON.stringify(v))
                        localStorage.setItem('tablename-'+k, maincms_panel[k].maintable)
                        // console.log('aku cek localstoragenya')
                        // console.log(JSON.parse(localStorage.getItem('input-data-'+k)))
                        // console.log('input-data-'+k)
                        key++;
                    })
                    
                    // destroyTabDatatable('fincashbankdetail')
                    getCmsTabDatatable()
                    // alert(response['noid'])
                    // $('#noid').val(id)
                    // $.each(response, function(k, v) {
                    //     $("#"+v.field).val(v.value);
                    //     if (v.disabled) {
                    //         $("#"+v.field).attr('disabled', 'disabled');
                    //     }
                    //     if (v.edithidden) {
                    //         $("#div-"+v.field).css('display', 'none');
                    //     }
                    // });
                    // console.log(response)
                // }
            // });
        });
    }

    function viewTab(another_table, another_id, id, panel_noid) {
        resetDataTab(panel_noid)
        statusaddtab[panel_noid] = false;
        
        var maincms_panel = new Array();
        $.each(maincms['panel'], function(k, v) {
            $.each(maincms['widget'], function(k2, v2) {
                if (v.idwidget == v2.noid) {
                    maincms_panel[v.noid] = v2;
                }
            })
        })
        
        $.each(JSON.parse(localStorage.getItem('input-data-'+panel_noid)), function(k, v) {
            if (v.noid == another_id) {
                nownoidtab = v['noid'];
                $.each(fieldnametab[panel_noid], function(k2, v2) {
                    $("#tab-form-div-"+panel_noid+" div div #"+v2).attr('disabled', true);
                    $("#tab-form-div-"+panel_noid+" div div #"+v2).val(v[v2]);
                })
            }
        })

        $('#tab-form-'+panel_noid).show()
        $('#add-data-'+panel_noid).hide()
        $('#save-data-'+panel_noid).show()
        $('#cancel-data-'+panel_noid).show()
        $('#tab-table-'+panel_noid+'_wrapper').hide()
        $('#tab-appendInfoDatatable-'+panel_noid).hide()
        $('#ts-'+panel_noid).hide()
        $('#tool_refresh-'+panel_noid).hide();
        $('#tool_filter-'+panel_noid).hide()
        $('#tool_refresh_up-'+panel_noid).hide();
    }

    function editTab(another_table, another_id, id, panel_noid) {
        resetDataTab(panel_noid)
        statusaddtab[panel_noid] = false;
        // console.log(statusaddtab)
        // alert(another_table+' '+another_id+' '+id)
        var maincms_panel = new Array();
        $.each(maincms['panel'], function(k, v) {
            $.each(maincms['widget'], function(k2, v2) {
                if (v.idwidget == v2.noid) {
                    maincms_panel[v.noid] = v2;
                }
            })
        })
        // console.log(maincms_panel)
        // console.log(panel_noid)
        // console.log(maincms_panel[panel_noid].noid)

        // console.log(JSON.parse(localStorage.getItem('input-data-'+panel_noid)))
        // console.log(fieldnametab)
        $.each(JSON.parse(localStorage.getItem('input-data-'+panel_noid)), function(k, v) {
            if (v.noid == another_id) {
                nownoidtab = v['noid'];
                $.each(fieldnametab[panel_noid], function(k2, v2) {
                    var fieldsplit = v2.split('_lookup');

                    if (fieldsplit.length == 2) {
                        $("#tab-form-div-"+panel_noid+" div div div #"+localStorage.getItem('tablename-'+panel_noid)+'-'+fieldsplit[0]).val('disabled', false);
                        $("#tab-form-div-"+panel_noid+" div div div #"+localStorage.getItem('tablename-'+panel_noid)+'-'+fieldsplit[0]).val(v[fieldsplit[0]]).trigger('change');
                    } else {
                        $("#tab-form-div-"+panel_noid+" div div #"+localStorage.getItem('tablename-'+panel_noid)+'-'+v2).attr('disabled', false);
                        $("#tab-form-div-"+panel_noid+" div div #"+localStorage.getItem('tablename-'+panel_noid)+'-'+v2).val(v[v2]);
                    }

                    // console.log("#"+v2+'     '+v[v2])
                    // if (v.disabled) {
                    //     $("#"+v.field).attr('disabled', 'disabled');
                    // }
                    // if (v.edithidden) {
                    //     $("#div-"+v.field).css('display', 'none');
                    // }
                })
            }
        })
        // getForm(statusadd, maincms_panel[panel_noid].noid, 'tab-form-div-'+panel_noid);

        $('#tab-form-'+panel_noid).show()
        $('#add-data-'+panel_noid).hide()
        $('#save-data-'+panel_noid).show()
        $('#cancel-data-'+panel_noid).show()
        $('#tab-table-'+panel_noid+'_wrapper').hide()
        $('#tab-appendInfoDatatable-'+panel_noid).hide()
        $('#ts-'+panel_noid).hide()
        $('#space_filter_button-'+panel_noid).hide();
        $('#tool_refresh-'+panel_noid).hide();
        $('#tool_filter-'+panel_noid).hide()
        $('#tool_refresh_up-'+panel_noid).hide();
        console.log('masok jos')
    }

    function edit(id) {
        $.when(
            $.ajax({
                url: '/find_tmd',
                type: "POST",
                dataType: 'json',
                header:{
                    'X-CSRF-TOKEN':$('#token').val()
                },
                data: {
                    "id" : id,
                    "namatable" : $("#namatable").val(),
                    "_token": $('#token').val(),
                    maincms: JSON.stringify(maincms)
                },
                success: function (response) {
                    return response;
                }
            })
        ).then(function(responsefind) {
            statusadd = false;
            statusdetail = false;
            nownoid = id;
            $.each(maincms['widget'], function(k, v) {
                getNextNoidTab(v.maintable, v.noid)
            })
            resetForm();
            resetData();
            getCmsTab(statusadd);
            getForm(statusadd, maincms['widget'][0]['noid'], 'divform', responsefind);
            // alert(response)
            // getCmsTabDatatable();


            editid = id;

            // var x = document.getElementById("from-departement");
            // var x2 = document.getElementById("tabledepartemen");
            // var y = document.getElementById("tool-kedua");
            // var y2 = document.getElementById("tool-pertama");
            // var space_filter_button = $('#space_filter_button');

            // if (x.style.display === "none") {
            //     x.style.display = "block";
            //     x2.style.display = "none";
            //     y.style.display = "block";
            //     y2.style.display = "none";
            //     space_filter_button.css('display', 'none');
            // } else {
            //     x.style.display = "none";
            //     x2.style.display = "block";
            //     space_filter_button.css('display', 'block');
            // }

            $('#from-departement').show();
            $('#tabledepartemen').hide();
            $('#tool-kedua').show();
            $('#tool-pertama').hide();
            $('#space_filter_button').hide();
            $('#tool_refresh').hide();
            $('#tool_filter').hide()
            $('#tool_refresh_up').hide();

            // $.ajax({
            //     url: '/find_tmd',
            //     type: "POST",
            //     dataType: 'json',
            //     header:{
            //         'X-CSRF-TOKEN':$('#token').val()
            //     },
            //     data: {
            //         "id" : id,
            //         "namatable" : $("#namatable").val(),
            //         "_token": $('#token').val(),
            //         maincms: JSON.stringify(maincms)
            //     },
            //     success: function (response) {
                    nownoid = responsefind.nownoid;

                    var maincms_panel = new Array();
                    $.each(maincms['panel'], function(k, v) {
                        $.each(maincms['widget'], function(k2, v2) {
                            if (v.idwidget == v2.noid) {
                                maincms_panel[v.noid] = v2;
                            }
                        })
                    })

                    // console.log(response.result_detail)
                    var key = 0;
                    $.each(responsefind.result_detail, function(k, v) {
                        if (key == 0) {
                            nownoidtab = maincms_panel[k].noid
                        }
                        // alert(k)
                        // console.log(v)
                        // localStorage.removeItem('input-data-'+k)
                        localStorage.setItem('input-data-'+k, JSON.stringify(v))
                        localStorage.setItem('tablename-'+k, maincms_panel[k].maintable)
                        select_all_tab[k] = new Array();
                        // console.log('aku cek localstoragenya')
                        // console.log(JSON.parse(localStorage.getItem('input-data-'+k)))
                        // console.log('input-data-'+k)
                        key++;
                    })
                    
                    // destroyTabDatatable('fincashbankdetail')
                    getCmsTabDatatable()
                    // alert(response['noid'])
                    // $('#noid').val(id)
                    // $.each(response, function(k, v) {
                    //     $("#"+v.field).val(v.value);
                    //     if (v.disabled) {
                    //         $("#"+v.field).attr('disabled', 'disabled');
                    //     }
                    //     if (v.edithidden) {
                    //         $("#div-"+v.field).css('display', 'none');
                    //     }
                    // });
                    // console.log(response)
                // }
            // });
        });
    }

    function removeTab(another_table, another_id, id, panel_noid) {
        if (confirm('Are you sure you want to delete this?')) {
            nownoidtab = another_id;
            current_idd = localStorage.getItem('input-data-'+panel_noid);

            var replace_current_idd = new Array();
            var key = 0;
            $.each(JSON.parse(current_idd), function(k, v) {
                if (v.noid != nownoidtab) {
                    replace_current_idd[key] = v;
                    key++;
                }
            })

            refreshSumTab(id, panel_noid)

            localStorage.removeItem('input-data-'+panel_noid);
            localStorage.setItem('input-data-'+panel_noid, JSON.stringify(replace_current_idd))

            $.notify({
                message: 'Delete data has been succesfullly.' 
            },{
                type: 'success'
            });
            refreshTab();
        }

        console.log(localStorage.getItem('input-data-'+panel_noid))
        return false





        // console.log(id);return false;
        if (confirm('Are you sure you want to delete this?')) {
            $.ajax({
                url: '/delete_tab_tmd',
                type: "POST",
                header:{
                    'X-CSRF-TOKEN':$('#token').val()
                },
                data: {
                    "id" : id,
                    "another_id" : another_id,
                    "another_table" : another_table,
                    "_token": $('#token').val(),
                },
                success: function () {
                    // $("#notiftext").text('Data  Berhasil di hapus');
                    $.notify({
                        message: 'Delete data has been succesfullly.' 
                    },{
                        type: 'success'
                    });
                    refreshTab();
                    // $('#divsucces').show(0).delay(5000).hide(0);
                    // var x = document.getElementById("from-departement");
                    // var x2 = document.getElementById("tabledepartemen");
                    // var y = document.getElementById("tool-kedua");
                    // var y2 = document.getElementById("tool-pertama");
                    // x.style.display = "none";
                    // x2.style.display = "block";
                    // y.style.display = "none";
                    // y2.style.display = "block";
                }
            });
        }
    }

    function remove(id) {
        // console.log(id);return false;
        if (confirm('Are you sure you want to delete this?')) {
            $.ajax({
                url: '/delete_tmd',
                type: "POST",
                header:{
                    'X-CSRF-TOKEN':$('#token').val()
                },
                data: {
                    "id" : id,
                    "table" : maincms['widget'][0]['maintable'],
                    "_token": $('#token').val(),
                    maincms: JSON.stringify(maincms),
                },
                success: function () {
                    // $("#notiftext").text('Data  Berhasil di hapus');
                    $.notify({
                        message: 'Delete data has been succesfullly.' 
                    },{
                        type: 'success'
                    });
                    refresh();
                    // $('#divsucces').show(0).delay(5000).hide(0);
                    var x = document.getElementById("from-departement");
                    var x2 = document.getElementById("tabledepartemen");
                    var y = document.getElementById("tool-kedua");
                    var y2 = document.getElementById("tool-pertama");
                    x.style.display = "none";
                    x2.style.display = "block";
                    y.style.display = "none";
                    y2.style.display = "block";
                }
            });
        }
    }
    
    function print(noid, urlprint) {
        window.open('http://ux.lambada.id/'+noid+'/'+urlprint, '_blank');
        return false;

        $.ajax({
                url: panelnoid+'/print/'+noid,
                type: "GET",
                header:{
                    'X-CSRF-TOKEN':$('#token').val()
                },
                success: function (response) {
                    return response
                }
        })
    }

    (function ($) {
    // function infoDatatable() {
        // console.log('tes gaes')
        
        function calcDisableClasses(oSettings) {
            var start = oSettings._iDisplayStart;
            var length = oSettings._iDisplayLength;
            var visibleRecords = oSettings.fnRecordsDisplay();
            var all = length === -1;

            // Gordey Doronin: Re-used this code from main jQuery.dataTables source code. To be consistent.
            var page = all ? 0 : Math.ceil(start / length);
            var pages = all ? 1 : Math.ceil(visibleRecords / length);

            var disableFirstPrevClass = (page > 0 ? '' : oSettings.oClasses.sPageButtonDisabled);
            var disableNextLastClass = (page < pages - 1 ? '' : oSettings.oClasses.sPageButtonDisabled);

            return {
                'first': disableFirstPrevClass,
                'previous': disableFirstPrevClass,
                'next': disableNextLastClass,
                'last': disableNextLastClass
            };
        }

        function calcCurrentPage(oSettings) {
            return Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength) + 1;
        }

        function calcPages(oSettings) {
            return Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength);
        }

        var firstClassName = 'first';
        var previousClassName = 'previous';
        var nextClassName = 'next';
        var lastClassName = 'last';

        var paginateClassName = 'paginate';
        var paginatePageClassName = 'paginate_page';
        var paginateInputClassName = 'paginate_input';
        var paginateTotalClassName = 'paginate_total';

        $.fn.dataTableExt.oPagination.input = {
            'fnInit': function (oSettings, nPaging, fnCallbackDraw) {
                var nFirst = document.createElement('span');
                var nPrevious = document.createElement('span');
                var nNext = document.createElement('span');
                var nLast = document.createElement('span');
                var nInput = document.createElement('input');
                var nTotal = document.createElement('span');
                var nInfo = document.createElement('span');

                var language = oSettings.oLanguage.oPaginate;
                var classes = oSettings.oClasses;
                var info = language.info || `&nbsp;&nbsp;
                                                <label style="padding-top: 8px">Page</label>&nbsp;
                                                _INPUT_&nbsp;
                                                <label style="padding-top: 8px">of&nbsp;_TOTAL_</label>&nbsp;`;

                nFirst.innerHTML = language.sFirst;
                nPrevious.innerHTML = language.sPrevious;
                nNext.innerHTML = language.sNext;
                nLast.innerHTML = language.sLast;

                nFirst.className = firstClassName + ' ' + classes.sPageButton;
                nPrevious.className = previousClassName + ' ' + classes.sPageButton;
                nNext.className = nextClassName + ' ' + classes.sPageButton;
                nLast.className = lastClassName + ' ' + classes.sPageButton;

                nInput.className = paginateInputClassName;
                nTotal.className = paginateTotalClassName;

                if (oSettings.sTableId !== '') {
                    nPaging.setAttribute('id', oSettings.sTableId + '_' + paginateClassName);
                    nFirst.setAttribute('id', oSettings.sTableId + '_' + firstClassName);
                    nPrevious.setAttribute('id', oSettings.sTableId + '_' + previousClassName);
                    nNext.setAttribute('id', oSettings.sTableId + '_' + nextClassName);
                    nLast.setAttribute('id', oSettings.sTableId + '_' + lastClassName);
                }

                nInput.type = 'text';

                info = info.replace(/_INPUT_/g, '</span>' + nInput.outerHTML + '<span>');
                info = info.replace(/_TOTAL_/g, '</span>' + nTotal.outerHTML + '<span>');
                nInfo.innerHTML = '<span>' + info + '</span>';

                nPaging.appendChild(nFirst);
                nPaging.appendChild(nPrevious);
                $(nInfo).children().each(function (i, n) {
                    nPaging.appendChild(n);
                });
                nPaging.appendChild(nNext);
                nPaging.appendChild(nLast);

                $(nFirst).click(function() {
                    var iCurrentPage = calcCurrentPage(oSettings);
                    if (iCurrentPage !== 1) {
                        oSettings.oApi._fnPageChange(oSettings, 'first');
                        fnCallbackDraw(oSettings);
                    }
                });

                $(nPrevious).click(function() {
                    var iCurrentPage = calcCurrentPage(oSettings);
                    if (iCurrentPage !== 1) {
                        oSettings.oApi._fnPageChange(oSettings, 'previous');
                        fnCallbackDraw(oSettings);
                    }
                });

                $(nNext).click(function() {
                    var iCurrentPage = calcCurrentPage(oSettings);
                    if (iCurrentPage !== calcPages(oSettings)) {
                        oSettings.oApi._fnPageChange(oSettings, 'next');
                        fnCallbackDraw(oSettings);
                    }
                });

                $(nLast).click(function() {
                    var iCurrentPage = calcCurrentPage(oSettings);
                    if (iCurrentPage !== calcPages(oSettings)) {
                        oSettings.oApi._fnPageChange(oSettings, 'last');
                        fnCallbackDraw(oSettings);
                    }
                });

                $(nPaging).find('.' + paginateInputClassName).keyup(function (e) {
                    // 38 = up arrow, 39 = right arrow
                    if (e.which === 38 || e.which === 39) {
                        this.value++;
                    }
                    // 37 = left arrow, 40 = down arrow
                    else if ((e.which === 37 || e.which === 40) && this.value > 1) {
                        this.value--;
                    }

                    if (this.value === '' || this.value.match(/[^0-9]/)) {
                        /* Nothing entered or non-numeric character */
                        this.value = this.value.replace(/[^\d]/g, ''); // don't even allow anything but digits
                        return;
                    }

                    var iNewStart = oSettings._iDisplayLength * (this.value - 1);
                    if (iNewStart < 0) {
                        iNewStart = 0;
                    }
                    if (iNewStart >= oSettings.fnRecordsDisplay()) {
                        iNewStart = (Math.ceil((oSettings.fnRecordsDisplay()) / oSettings._iDisplayLength) - 1) * oSettings._iDisplayLength;
                    }

                    oSettings._iDisplayStart = iNewStart;
                    oSettings.oInstance.trigger("page.dt", oSettings);
                    fnCallbackDraw(oSettings);
                });

                // Take the brutal approach to cancelling text selection.
                $('span', nPaging).bind('mousedown', function () { return false; });
                $('span', nPaging).bind('selectstart', function() { return false; });

                // If we can't page anyway, might as well not show it.
                var iPages = calcPages(oSettings);
                if (iPages <= 1) {
                    $(nPaging).hide();
                }
            },

            'fnUpdate': function (oSettings) {
                if (!oSettings.aanFeatures.p) {
                    return;
                }

                var iPages = calcPages(oSettings);
                var iCurrentPage = calcCurrentPage(oSettings);

                var an = oSettings.aanFeatures.p;
                if (iPages <= 1) // hide paging when we can't page
                {
                    $(an).hide();
                    return;
                }

                var disableClasses = calcDisableClasses(oSettings);

                $(an).show();

                // Enable/Disable `first` button.
                $(an).children('.' + firstClassName)
                    .removeClass(oSettings.oClasses.sPageButtonDisabled)
                    .addClass(disableClasses[firstClassName]);

                // Enable/Disable `prev` button.
                $(an).children('.' + previousClassName)
                    .removeClass(oSettings.oClasses.sPageButtonDisabled)
                    .addClass(disableClasses[previousClassName]);

                // Enable/Disable `next` button.
                $(an).children('.' + nextClassName)
                    .removeClass(oSettings.oClasses.sPageButtonDisabled)
                    .addClass(disableClasses[nextClassName]);

                // Enable/Disable `last` button.
                $(an).children('.' + lastClassName)
                    .removeClass(oSettings.oClasses.sPageButtonDisabled)
                    .addClass(disableClasses[lastClassName]);

                // Paginate of N pages text
                $(an).find('.' + paginateTotalClassName).html(iPages);

                // Current page number input value
                $(an).find('.' + paginateInputClassName).val(iCurrentPage);
            }
        };
    // }
    })(jQuery);
</script>
@endsection
