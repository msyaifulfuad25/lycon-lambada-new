@extends('lam_custom_layouts.default')

@section('content')
    <div class="card">
        <div class="card-body">
            <h5><b>Most Item Requested</b></h5>
            <div class="row">
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-body">
                            <h6><b>Filter</b></h6>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="month">Month & Year</label>
                                    <input type="text" class="form-control datepicker_month_and_year" id="month_and_year" autocomplete="off" placeholder="Month and Year">
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="year">Year Only</label>
                                    <input type="text" class="form-control datepicker_year_only" id="year_only" autocomplete="off" placeholder="Year Only">
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="year">Cost Control</label>
                                    <select id="id_cost_control" class="form-control select2">
                                        <option value="">All</option>
                                        <?php foreach ($cost_controls as $k => $v) { ?>
                                            <option value="{{ $v->noid }}">{{ $v->kode }} - {{ $v->projectname }}</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-primary btn-sm" onclick="getMostItemRequested()"><i class="fa fa-search"></i> Find</button>
                                    <button class="btn btn-secondary btn-sm" onclick="reset()"><i class="fa fa-times"></i> Reset</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-body">
                            <h6><b>Data</b></h6>
                            <table id="table_most_item_requested" class="table compact">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Inventori</th>
                                        <th>Jumlah</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modal_detail" tabindex="-1" role="dialog" aria-labelledby="modal_detailLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_detailLabel">Detail <b id="modal_detail_name"></b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table id="table_most_item_requested_detail" class="table compact">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Inventori</th>
                            <th>Keterangan</th>
                            <th>Purchase Request</th>
                            <th>Purchase Offer</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            </div>
            </div>
        </div>
    </div>
@endsection

@section('styles')
    <!-- Datatable -->
    <link href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <!-- Select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection
    
@section('scripts')
    <!-- Datatable -->
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <!-- Select2 -->
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    
    <!-- Custom -->
    <script type="text/javascript">
        var main = {
            urlcode: 40020101
        };

        $(document).ready(function() {
            getSelect2();
            getMostItemRequested();
        })

        const getMostItemRequested = () => {
            $('#table_most_item_requested').DataTable().destroy();
            $('#table_most_item_requested').DataTable({
                serverSide: true,
                order: [[2, 'desc']],
                pageLength: 5,
                processing: true,
                lengthMenu: [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, 'All'],
                ],
                columns: [
                    { mData: 'no', orderable: false, class: 'text-right' },
                    { mData: 'name' },
                    { mData: 'qty' },
                    { mData: 'action', orderable: false },
                ],
                ajax: {
                    url: `/getMostItemRequested_${main.urlcode}`,
                    type: 'POST',
                    dataType:'json',
                    data: {
                        _token:$('meta[name="csrf-token"]').attr('content'),
                        month_and_year: $('#month_and_year').val(),
                        year_only: $('#year_only').val(),
                        id_cost_control: $('#id_cost_control :selected').val(),
                    },
                },
            });
            $('<button class="btn btn-secondary ml-2" onclick="getMostItemRequested()"><i class="icon-refresh"></i></button>').appendTo('#table_most_item_requested_filter');
        }

        const getMostItemRequestedDetail = (params) => {
            $('#table_most_item_requested_detail').DataTable().destroy();
            $('#table_most_item_requested_detail').DataTable({
                serverSide: true,
                order: [[1, 'asc']],
                pageLength: 10,
                processing: true,
                lengthMenu: [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, 'All'],
                ],
                columns: [
                    { mData:'no', orderable: false, class: 'text-right' },
                    { mData: 'name' },
                    { mData: 'description' },
                    { mData: 'pr_code' },
                    { mData: 'pof_code' },
                ],
                ajax: {
                    url: `/getMostItemRequestedDetail_${main.urlcode}`,
                    type: 'POST',
                    dataType:'json',
                    data: {
                        _token:$('meta[name="csrf-token"]').attr('content'),
                        noid: params.noid,
                        id_cost_control: params.id_cost_control,
                    },
                },
            });
            $(`<button class="btn btn-secondary ml-2" onclick="getMostItemRequestedDetail({noid: ${params.noid}, id_cost_control: ${params.id_cost_control}})"><i class="icon-refresh"></i></button>`).appendTo('#table_most_item_requested_detail_filter');
        }

        const showModal = (params) => {
            getMostItemRequestedDetail({
                noid: params.noid,
                id_cost_control: $('#id_cost_control :selected').val(),
            });
            $(`${params.element_id}_name`).text(params.name);
            $(params.element_id).modal('show');
        }

        const hideModal = (params) => {
            $(params.element_id).modal('hide');
        }

        const reset = () => {
            $('#month_and_year').val(null);
            $('#year_only').val(null);
            $('#id_cost_control').val(null).trigger('change');
            getMostItemRequested();
        }

        const getSelect2 = () => {
            $('.select2').select2();
            $('.select2').css('width', '100%');
            $('.select2 .selection .select2-selection').css('height', '31px');
            $('.select2 .selection .select2-selection .select2-selection__rendered').css('margin-top', '-6px');
            $('.select2 .selection .select2-selection .select2-selection__arrow').css('margin-top', '4px');
        }

        ////////////////////////////////


        $('#month_and_year').on('keyup', () => $('#year_only').val(null));
        $('#year_only').on('keyup', () => $('#month_and_year').val(null));

        $(".datepicker_month_and_year").datepicker( {
            format: "yyyy-mm",
            viewMode: "months", 
            minViewMode: "months",
            autoclose: true
        }).on('change', () => $('#year_only').val(null));
        
        $(".datepicker_year_only").datepicker( {
            format: "yyyy",
            viewMode: "years", 
            minViewMode: "years",
            autoclose: true
        }).on('change', () => $('#month_and_year').val(null));
    </script>
@endsection