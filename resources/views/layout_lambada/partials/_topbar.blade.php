@php
    if(Session::get('login')) {
        $an = DB::connection('mysql2')
                ->table('appnotification AS an')
                ->leftJoin('appmtypenotification AS amtn','an.idtypenotification','=','amtn.noid')
                ->select([
                    'an.noid AS an_noid',
                    'an.kodereff AS an_kodereff',
                    'an.keterangan AS an_keterangan',
                    'an.urlreff AS an_urlreff',
                    'an.isread AS an_isread',
                    'an.docreate AS an_docreate',
                    'amtn.nama AS amtn_nama',
                    'amtn.classicon AS amtn_classicon',
                    'amtn.classcolor AS amtn_classcolor'
                ])
                ->where('an.idcardassign',Session::get('noid'))
                ->orderBy('an.docreate','desc')
                ->get();
        $annotread = DB::connection('mysql2')
                        ->table('appnotification AS an')
                        ->where('an.idcardassign',Session::get('noid'))
                        ->where('an.isread',0)
                        ->count();
    } else {
        $an = [];
        $annotread = 0;
    }

    $color = [
        'white' => '#FFFFFF',
        'default' => '#E1E5EC',
        'dark' => '#2F353B',
        'blue' => '#3598DC',
        'blue-madison' => '#578EBE',
        'blue-chambray' => '#2C3E50',
        'blue-ebonyclay' => '#22313F',
        'blue-hoki' => '#67809F',
        'blue-steel' => '#4B77BE',
        'blue-soft' => '#4C87B9',
        'blue-dark' => '#5E738B',
        'blue-sharp' => '#5C9BD1',
        'blue-oleo' => '#94A0B2',
        'green' => '#32C5D2',
        'green-meadow' => '#1BBC9B',
        'green-seagreen' => '#1BA39C',
        'green-torquoise' => '#36D7B7',
        'green-haze' => '#44B6AE',
        'green-jungle' => '#26C281',
        'green-soft' => '#3FABA4',
        'green-dark' => '#4DB3A2',
        'green-sharp' => '#2AB4C0',
        'green-steel' => '#29B4B6',
        'grey' => '#E5E5E5',
        'grey-steel' => '#E9EDEF',
        'grey-cararra' => '#FAFAFA',
        'grey-gallery' => '#555555',
        'grey-cascade' => '#95A5A6',
        'grey-silver' => '#BFBFBF',
        'grey-salsa' => '#ACB5C3',
        'grey-salt' => '#BFCAD1',
        'grey-mint' => '#525E64',
        'red' => '#E7505A',
        'red-pink' => '#E08283',
        'red-sunglo' => '#E26A6A',
        'red-intense' => '#E35B5A',
        'red-thunderbird' => '#D91E18',
        'red-flamingo' => '#EF4836',
        'red-soft' => '#D05454',
        'red-haze' => '#F36A5A',
        'red-mint' => '#E43A45',
        'yellow' => '#C49F47',
        'yellow-gold' => '#E87E04',
        'yellow-casablanca' => '#F2784B',
        'yellow-crusta' => '#F3C200',
        'yellow-lemon' => '#F7CA18',
        'yellow-saffron' => '#F4D03F',
        'yellow-soft' => '#C8D046',
        'yellow-haze' => '#C5BF66',
        'yellow-mint' => '#C5B96B',
        'purple' => '#8E44AD',
        'purple-plum' => '#8775A7',
        'purple-medium' => '#BF55EC',
        'purple-studio' => '#8E44AD',
        'purple-wisteria' => '#9B59B6',
        'purple-seance' => '#9A12B3',
        'purple-intense' => '#8775A7',
        'purple-sharp' => '#796799',
        'purple-soft' => '#8877A9',
    ];
@endphp

<!-- Topbar -->
<div class="topbar">


    <div class="dropdown" style="padding-right: 5px; margin-bottom: 1px; margin-right: 20px; background: white;" onmouseover="this.style.background='#F3F6F9'" onmouseout="this.style.background='white'">
        <div class="topbar-item">
            <i class="icon-list text-primary" style="margin: 8px 5px 5px 5px;"></i>
        </div>
    </div>

    <div class="dropdown" style="padding-right: 5px; margin-bottom: 1px; margin-right: 20px; background: white;" onmouseover="this.style.background='#F3F6F9'" onmouseout="this.style.background='white'">
        <div class="topbar-item">
            <i class="icon-envelope text-primary" style="margin: 5px;"></i>
        </div>
    </div>

    <div class="dropdown" id="n-notification" style="padding-right: 5px; margin-bottom: 1px; margin-right: 20px; background: white;" onmouseover="this.style.background='#F3F6F9'" onmouseout="this.style.background='white'">
        <div class="topbar-item text-hover-primary" data-toggle="dropdown" data-offset="10px,0px">
            <i class="icon-bell text-primary" style="margin: 5px;"></i>
            @if($annotread > 0)
                <span class="badge" style="background-color: red; color: white; margin: -15px 0px 0px -5px">{{$annotread}}</span>
            @endif
            <span class="pulse-ring"></span>
        </div>
        <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg" style="">
            <form>
                <div class="d-flex flex-column pt-12 pb-10 bgi-size-cover bgi-no-repeat rounded-top" style="background: #334EB5">
                    <h4 class="d-flex flex-center rounded-top">
                        <span class="text-white">User Notifications</span>
                        <span class="btn btn-text btn-success btn-sm font-weight-bold btn-font-md ml-2">{{$annotread}} new</span>
                    </h4>
                </div>
                <div class="tab-content">
                    <div class="tab-pane active" id="topbar_notifications_events" role="tabpanel">
                        <div class="navi navi-hover scroll my-4 ps ps--active-y" data-scroll="true" data-height="300" data-mobile-height="200" style="height: 300px; overflow: hidden;">
                            @if(count($an) > 0)
                                @foreach($an as $k => $v)
                                    <a class="navi-item" onclick="readNotification({noid:{{$v->an_noid}},urlreff:'{{$v->an_urlreff}}',kodereff:'{{$v->an_kodereff}}'})">
                                        <div class="navi-link" style="background: <?= $v->an_isread == 1 ? 'white' : '#F3F6F9' ?>">
                                            <div class="navi-icon mr-2">
                                                <i class="{{@$v->amtn_classicon}}" style="color: {{@$color[@$v->amtn_classcolor]}}"></i>
                                            </div>
                                            <div class="navi-text">
                                                <div class="font-weight-bold">{{$v->an_keterangan}}</div>
                                                <div class="text-muted">{{\App\Helpers\MyHelper::dbdateTohumandateWithDay($v->an_docreate)}}</div>
                                            </div>
                                        </div>
                                    </a>
                                @endforeach
                            @else
                                <div class="tab-pane" id="topbar_notifications_logs" role="tabpanel">
                                    <div class="d-flex flex-center text-center text-muted min-h-200px">All caught up! 
                                    <br>No new notifications.</div>
                                </div>
                            @endif
                        <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px; height: 300px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 109px;"></div></div></div>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <!-- User -->
    @if (config('layout.extras.user.display'))
        @if (config('layout.extras.user.layout') == 'offcanvas')
            <div class="topbar-item">
                <div class="btn btn-icon w-auto btn-clean d-flex align-items-center btn-lg px-2" id="kt_quick_user_toggle">
                    @if (@$noid == 'null')
                        <!-- <a href="#" class="btn btn-success font-weight-bold mr-1">
                                <i class="la la-sign-in">Login</i>
                            </a> -->
                        <span class="text-muted font-weight-bold font-size-base d-none d-md-inline mr-1"></span>
                        <span class="text-dark-50 font-weight-bolder font-size-base d-none d-md-inline mr-3">Login</span>
                        <span class="symbol symbol-35 symbol-light-success">
                            <span class="symbol-label font-size-h5 font-weight-bold"><i class="la la-sign-in"></i></span>
                        </span>
                    @else
                        <span class="text-muted font-weight-bold font-size-base d-none d-md-inline mr-1">Hi,</span>
                        <span class="text-dark-50 font-weight-bolder font-size-base d-none d-md-inline mr-3">{{ Session::get('firstname').' '.Session::get('middlename') }}</span>
                        <span class="symbol symbol-35 symbol-light-success">
                            <span class="symbol-label font-size-h5 font-weight-bold"><i class="icon-user"></i></span>
                        </span>
                    @endif
                </div>
            </div>
        @else
            <div class="dropdown">
                <!-- Toggle -->
                <div class="topbar-item" data-toggle="dropdown" data-offset="0px,0px">
                    @if ($noid == 'null')
                        <div class="btn btn-icon w-auto btn-clean d-flex align-items-center btn-lg px-2">
                            <span class="text-muted font-weight-bold font-size-base d-none d-md-inline mr-1">Hioooo,</span>
                            <span class="text-dark-50 font-weight-bolder font-size-base d-none d-md-inline mr-3">Seankkk</span>
                            <span class="symbol symbol-35 symbol-light-success">
                                <span class="symbol-label font-size-h5 font-weight-bold">S</span>
                            </span>
                        </div>
                    @else
                        <div class="btn btn-icon w-auto btn-clean d-flex align-items-center btn-lg px-2">
                            <span class="text-muted font-weight-bold font-size-base d-none d-md-inline mr-1">Hi,</span>
                            <span class="text-dark-50 font-weight-bolder font-size-base d-none d-md-inline mr-3">Seen</span>
                            <span class="symbol symbol-35 symbol-light-success">
                                <span class="symbol-label font-size-h5 font-weight-bold">S</span>
                            </span>
                        </div>
                    @endif
                </div>

                <!-- Dropdown -->
                <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg p-0">
                    @include('layout.partials.extras.dropdown._user')
                </div>
            </div>
        @endif
    @endif
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.js"></script>
<script>
    const socket = io(`<?= env('APP_URL') ?>:3000`);
    socket.on('connection')
    
    function readNotification(params) {
        $.ajax({
            type: 'POST',
            header: {
                'X-CSRF-TOKEN': $('#token').val()
            },
            url: '/read_notification',
            dataType: 'json',
            data: {
                '_token': $('#token').val(),
                'noid': params.noid,
                'kodereff': params.kodereff,
            },
            success: function (response) {
                if (!response.success) {
                    $.notify({
                        message: response.message
                    },{
                        type: 'danger'
                    });
                }
                
                let host = window.location.origin;
                window.location = host+'/'+params.urlreff+'?s='+params.kodereff;
            }
        })
    }
</script>