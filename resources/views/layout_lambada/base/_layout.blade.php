@if(config('layout.self.layout') == 'blank')
    <div class="d-flex flex-column flex-root">
        @yield('content')
    </div>
@else
    @include('layout_lambada.base._header-mobile')
    <div class="d-flex flex-column flex-root">
        <div class="d-flex flex-row flex-column-fluid page">
                @if($page->showleftbar == 1)
                    @include('layout_lambada.base._aside')
                @else

                @endif

            <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper" style="background-color: #F0F2F9; padding-top: 45px">
                @if($page->showtopbar == 0)
                    @include('layout_lambada.base._header2')
                @else
                    @include('layout_lambada.base._header')
                @endif

                @if($page->showtitle == 1)
                    <div class="subheader subheader-transparent" id="kt_subheader" style="padding: 10px 10px 0px 10px !important">
                        <h3 class="page-title" style="margin-bottom: 0px">
                            <span id="pagecaption">{{ $page->pagecaption }}</span>
                            <small id="pagesubcaption">{{ $page->pagesubcaption }}</small>
                        </h3>
                    </div>
                @endif

                    <div class="content" id="kt_content">
                        @if($page->showbreadcrumb == 1)
                        <?php //dd($breadcrumb) ?>
                            @if(isset($breadcrumb))
                                <!-- <div style="padding-left: 1.5% !important;padding-right: 1.5% !important"> -->
                                <!-- <div class="row"> -->
                                    <div id="kt_breadcrumb">
                                        <nav aria-label="breadcrumb">
                                            <ol class="breadcrumb flat" style="background-color:white">
                                                <li><i class="fa fa-home" style="margin-right:5px"></i></li>
                                                <li class="breadcrumb-item">
                                                    <a href="{{ url('/') }}">&nbsp;Home</a>
                                                </li>
                                                <?php for ($i=count($breadcrumb['text'])-1; $i>=0; $i--) { ?>
                                                    @if($breadcrumb['noid'][$i] == $page->noid)
                                                        <li class="breadcrumb-item active" aria-current="page"><span style="color: #666;">{{ $breadcrumb['text'][$i] }}</span></li>
                                                    @else
                                                        <li class="breadcrumb-item"><a href="{{ url('/'.$breadcrumb['url'][$i]) }}">    {{ $breadcrumb['text'][$i] }}</a></li>
                                                    @endif
                                                <?php } ?>
                                            </ol>
                                        </nav>
                                    </div>
                                <!-- </div> -->
                                <!-- {{$page->pagesubcaption}} -->
                                <!-- <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Library</li>
                                    </ol>
                                </nav> -->
                            @endif
                        @endif
                            @include('layout.base._content')
                    </div>

                @if($page->showfooter === 1)
                    @include('layout.base._footer')
                @else

                @endif
            </div>
        </div>
    </div>
@endif

@if (config('layout.self.layout') != 'blank')

    @if (config('layout.extras.search.layout') == 'offcanvas')
        @include('layout.partials.extras.offcanvas._quick-search')
    @endif

    @if (config('layout.extras.notifications.layout') == 'offcanvas')
        @include('layout.partials.extras.offcanvas._quick-notifications')
    @endif

    @if (config('layout.extras.quick-actions.layout') == 'offcanvas')
        @include('layout.partials.extras.offcanvas._quick-actions')
    @endif

    @if (config('layout.extras.user.layout') == 'offcanvas')
                  @include('layout_lambada.partials._quick-user')
    @endif

    @if (config('layout.extras.quick-panel.display'))
        @include('layout.partials.extras.offcanvas._quick-panel')
    @endif

    @if (config('layout.extras.chat.display'))
        @include('layout.partials.extras._chat')
    @endif

    @include('layout.partials.extras._scrolltop')

@endif
