{{-- Extends layout --}}

<!-- 10: Stat Box

23: Chart

71: Google Maps

151: News Slider

1011: DataTables -->

@extends('layout_lambada.default')

@section('content')

  <h3>Ini Contoh Isi View Unauthorized Page</h3>  
  <div class="error error-4 d-flex flex-row-fluid bgi-size-cover bgi-position-center" style="background-image: url({{asset('media/error/bg4.jpg') }}">
	<!--begin::Content-->
	<div class="container d-flex flex-row-fluid flex-column justify-content-md-center p-32">
		<p class="display-1 text-danger font-weight-boldest mt-md-0 line-height-md">
			You are not authorized to access this page.
		</p>
	</div>
	<!--end::Content-->
</div>

@endsection

