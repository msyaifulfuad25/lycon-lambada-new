{{-- Extends layout --}}
<!-- 10: Stat Box
23: Chart
71: Google Maps
151: News Slider
1011: DataTables -->
@extends('layout_lambada.default')
@section('content')
<h3>Ini Contooh Isi View  Under Maintenance</h3>
<div class="error error-5 d-flex flex-row-fluid bgi-size-cover bgi-position-center" style="background-image: url({{ asset('media/error/bg5.jpg')}} );">
	<!--begin::Content-->
	<div class="container d-flex flex-row-fluid flex-column justify-content-md-center p-12">
		<h1 class="error-title font-weight-boldest text-info ">Oops!</h1>
		<p class="font-weight-boldest display-4">
			This Page controller does not exits
		</p>
    <div class="col-xl-8">
<div class="card card-custom bg-light-success card-stretch gutter-b">
    <div class="card-header border-0">
        <h3 class="card-title font-weight-bolder text-success">All</h3>
    </div>
    <div class="card-body pt-2">
        <!--begin::Item-->
        <div class="d-flex align-items-center mb-10">
            <!--begin::Symbol-->
            <div class="symbol symbol-40 symbol-light-white mr-5">
                <div class="symbol-label">
                  <i class="fa fa-leaf"></i>
                    <!-- <img  class="h-75 align-self-end" alt=""> -->
                </div>
            </div>
            <div class="d-flex flex-column font-weight-bold">
                <a href="#" class="text-dark text-hover-primary mb-1 font-size-lg">{{ $page->pagecontroller }}</a>
            </div>
        </div>
        <div class="d-flex align-items-center mb-10">
            <!--begin::Symbol-->
            <div class="symbol symbol-40 symbol-light-white mr-5">
                <div class="symbol-label">
                  <i class="fa fa-info-circle"></i>

                </div>
            </div>
            <!--end::Symbol-->

            <!--begin::Text-->
            <div class="d-flex flex-column font-weight-bold">
                <a href="#" class="text-dark text-hover-primary mb-1 font-size-lg">This Page " {{ $page->pagecontroller }} "</a>
            </div>
            <!--end::Text-->
        </div>

    </div>
    <!--end::Body-->
</div>
<!--end::List Widget 2-->
    </div>


	</div>
	<!--end::Content-->
</div>
<!-- <p><h2> Error Controller not Found</h2> </p> -->
@endsection
