{{-- Extends layout --}}

<!-- 10: Stat Box

23: Chart

71: Google Maps

151: News Slider

1011: DataTables -->

@extends('layout_lambada.default')

@section('content')
<h3>Ini Contoh Isi View  Under Maintenance</h3>
 <div class="d-flex flex-column flex-root">
		 <!--begin::Error-->
 <div class="error error-6 d-flex flex-row-fluid bgi-size-cover bgi-position-center" style="background-image: url({{asset('media/error/bg6.jpg') }}">
 	<!--begin::Content-->
 	<div class="d-flex flex-column flex-row-fluid text-center">
 		<h1 class="error-title font-weight-boldest text-white mb-12" style="margin-top: 12rem;">Oops...</h1>
 		<p class="display-2 font-weight-bold text-white">
 			 This Site is currently.<br>
 			 down for maintenance
 		</p>
	 </div>
	 <!--end::Content-->
 </div>
 <!--end::Error-->
	</div>

@endsection
