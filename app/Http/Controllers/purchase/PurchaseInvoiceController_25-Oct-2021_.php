<?php

namespace App\Http\Controllers\purchase;

use App\Http\Controllers\Controller;
use App\Model\Purchase\PurchaseInvoice as MP;
use App\Model\Purchase\PurchaseReturn as MP2;
use App\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PurchaseInvoiceController extends Controller {

    public $color = [
        'white' => '#FFFFFF',
        'default' => '#E1E5EC',
        'dark' => '#2F353B',
        'blue' => '#3598DC',
        'blue-madison' => '#578EBE',
        'blue-chambray' => '#2C3E50',
        'blue-ebonyclay' => '#22313F',
        'blue-hoki' => '#67809F',
        'blue-steel' => '#4B77BE',
        'blue-soft' => '#4C87B9',
        'blue-dark' => '#5E738B',
        'blue-sharp' => '#5C9BD1',
        'blue-oleo' => '#94A0B2',
        'green' => '#32C5D2',
        'green-meadow' => '#1BBC9B',
        'green-seagreen' => '#1BA39C',
        'green-torquoise' => '#36D7B7',
        'green-haze' => '#44B6AE',
        'green-jungle' => '#26C281',
        'green-soft' => '#3FABA4',
        'green-dark' => '#4DB3A2',
        'green-sharp' => '#2AB4C0',
        'green-steel' => '#29B4B6',
        'grey' => '#E5E5E5',
        'grey-steel' => '#E9EDEF',
        'grey-cararra' => '#FAFAFA',
        'grey-gallery' => '#555555',
        'grey-cascade' => '#95A5A6',
        'grey-silver' => '#BFBFBF',
        'grey-salsa' => '#ACB5C3',
        'grey-salt' => '#BFCAD1',
        'grey-mint' => '#525E64',
        'red' => '#E7505A',
        'red-pink' => '#E08283',
        'red-sunglo' => '#E26A6A',
        'red-intense' => '#E35B5A',
        'red-thunderbird' => '#D91E18',
        'red-flamingo' => '#EF4836',
        'red-soft' => '#D05454',
        'red-haze' => '#F36A5A',
        'red-mint' => '#E43A45',
        'yellow' => '#C49F47',
        'yellow-gold' => '#E87E04',
        'yellow-casablanca' => '#F2784B',
        'yellow-crusta' => '#F3C200',
        'yellow-lemon' => '#F7CA18',
        'yellow-saffron' => '#F4D03F',
        'yellow-soft' => '#C8D046',
        'yellow-haze' => '#C5BF66',
        'yellow-mint' => '#C5B96B',
        'purple' => '#8E44AD',
        'purple-plum' => '#8775A7',
        'purple-medium' => '#BF55EC',
        'purple-studio' => '#8E44AD',
        'purple-wisteria' => '#9B59B6',
        'purple-seance' => '#9A12B3',
        'purple-intense' => '#8775A7',
        'purple-sharp' => '#796799',
        'purple-soft' => '#8877A9',
    ];
    public static $main = [
        'table' => 'purchaseinvoice',
        'tabledetail' => 'purchaseinvoicedetail',
        'table-title' => 'Purchase Invoice',
        'table2' => 'purchasereturn',
        'tabledetail2' => 'purchasereturndetail',
        'table2-title' => 'Purchase Return',
        'generatecode' => 'generatecode/505/1',
        'idtypetranc' => 505,
        'idtypetranc2' => 506,
        'noid' => 40021502,
        'noidnew' => 40021501,
        'printcode' => 'print_purchase_invoice',
        'condition' => [
            'mttts' => [
                [],
                [
                    'mttts' => 'draft',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => true,
                        'd' => true,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => true,
                        'd' => true,
                    ],
                    'draft' => false,
                    'issued' => true,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => false,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'issued',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => true,
                    'issued' => false,
                    'verified' => true,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => false,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'verified',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => true,
                    'verified' => false,
                    'approved' => true,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => false,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'approved',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'pending',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'canceled',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'finished',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
            ],
        ]
    ];

    public static function index() {
        $idtypepage = static::$main['noid'];
        $sessionnoid = Session::get('noid');
        $sessionname = Session::get('myusername');
        $sessionidcompany = Session::get('idcompany');
        $sessioniddepartment = Session::get('iddepartment');
        $sessionidgudang = Session::get('idgudang');

        $maintable = static::$main['table'];
        $breadcrumb = Menu::getBreadcrumb(static::$main['noid']);
        $pagenoid = static::$main['noid'];

        $defaulttanggal = date('d-M-Y');
        $defaulttanggaldue = date('d-M-Y', strtotime('+7 day'));
        $data = MP::getData(static::$main['table'],['noid','idstatustranc','idstatuscard','dostatus','idtypetranc','kode','kodereff','tanggal','tanggaltax','tanggaldue']);

        $columns = [
            ['data'=>'checkbox','orderable'=>false,'class'=>'text-center'],
            ['data'=>'no','orderable'=>false,'class'=>'text-right'],
            // ['data'=>'noid'],
            ['data'=>'idtypetranc'],
            ['data'=>'idstatustranc'],
            // ['data'=>'idstatuscard'],
            // ['data'=>'dostatus'],
            ['data'=>'kode'],
            ['data'=>'kodereff'],
            ['data'=>'tanggal'],
            ['data'=>'generaltotal','class'=>'text-right'],
            ['data'=>'keterangan'],
            ['data'=>'idcompany'],
            ['data'=>'iddepartment'],
            ['data'=>'idgudang'],
            ['data'=>'action','orderable'=>false],
        ];
        $columnslog = [
            ['data'=>'checkbox','orderable'=>false,'class'=>'text-center'],
            ['data'=>'no','orderable'=>false,'class'=>'text-right'],
            ['data'=>'idtypetranc'],
            ['data'=>'idtypeaction'],
            ['data'=>'logsubject'],
            ['data'=>'keterangan'],
            ['data'=>'idreff'],
            ['data'=>'idcreate'],
            ['data'=>'docreate'],
            ['data'=>'action','orderable'=>false],
        ];
        $columnscostcontrol = [
            ['data'=>'checkbox','orderable'=>false,'class'=>'text-center'],
            ['data'=>'no','orderable'=>false,'class'=>'text-right'],
            ['data'=>'idstatustranc'],
            ['data'=>'idtypetranc'],
            ['data'=>'kode'],
            ['data'=>'kodereff'],
            ['data'=>'idcompany'],
            ['data'=>'iddepartment'],
            ['data'=>'generaltotal','class'=>'text-right'],
            ['data'=>'keterangan'],
            ['data'=>'tanggal'],
            ['data'=>'action','orderable'=>false],
        ];
        $columnsdetail = [
            ['data'=>'checkbox','orderable'=>false,'class'=>'text-center'],
            ['data'=>'no','orderable'=>false,'class'=>'text-right'],
            ['data'=>'kodeinventor'],
            ['data'=>'namainventor'],
            ['data'=>'namainventor2'],
            ['data'=>'keterangan'],
            // ['data'=>'idgudang'],
            // ['data'=>'idcompany'],
            // ['data'=>'iddepartment'],
            // ['data'=>'idsatuan'],
            // ['data'=>'konvsatuan'],
            ['data'=>'unitqty','class'=>'text-right'],
            ['data'=>'namasatuan'],
            ['data'=>'unitprice','class'=>'text-right','render'=>"$.fn.dataTable.render.number( ',', '.', 3, 'Rp' )"],
            // ['data'=>'subtotal'],
            // ['data'=>'discountvar'],
            // ['data'=>'discount'],
            // ['data'=>'nilaidisc'],
            // ['data'=>'idtypetax'],
            // ['data'=>'idtax'],
            ['data'=>'hargatotal','class'=>'text-right'],
            ['data'=>'namacurrency'],
            // ['data'=>'idakunhpp'],
            // ['data'=>'idakunsales'],
            // ['data'=>'idakunpersediaan'],
            // ['data'=>'idakunsalesreturn'],
            // ['data'=>'idakunpurchasereturnn'],
            // ['data'=>'serialnumber'],
            // ['data'=>'garansitanggal'],
            // ['data'=>'garansiexpired'],
            // ['data'=>'typetrancprev'],
            // ['data'=>'trancprev'],
            // ['data'=>'trancprevd'],
            // ['data'=>'typetrancorder'],
            // ['data'=>'trancorder'],
            // ['data'=>'trancorderd'],
            ['data'=>'action','orderable'=>false],
        ];

        $columndefs = [
            ['targets'=>0,'width'=>'5%'],
            ['targets'=>1,'width'=>'5%'],
            ['targets'=>2,'width'=>'5%'],
            ['targets'=>3,'width'=>'15%'],
            ['targets'=>4,'width'=>'15%'],
            ['targets'=>5,'width'=>'15%'],
            ['targets'=>6,'width'=>'15%'],
            ['targets'=>7,'width'=>'15%'],
            ['targets'=>8,'width'=>'5%'],
            ['targets'=>9,'width'=>'5%'],
            ['targets'=>10,'width'=>'5%'],
            ['targets'=>10,'width'=>'5%']
        ];

        $columndefslog = [
            ['targets'=>0,'width'=>'5%'],
            ['targets'=>1,'width'=>'5%'],
            ['targets'=>2,'width'=>'5%'],
            ['targets'=>3,'width'=>'15%'],
            ['targets'=>4,'width'=>'15%'],
            ['targets'=>5,'width'=>'15%'],
            ['targets'=>6,'width'=>'15%'],
            ['targets'=>7,'width'=>'15%'],
            ['targets'=>8,'width'=>'5%'],
            ['targets'=>9,'width'=>'5%'],
            ['targets'=>10,'width'=>'5%']
        ];

        $columndefscostcontrol = [
            ['targets'=>0,'width'=>'5%'],
            ['targets'=>1,'width'=>'5%'],
            ['targets'=>2,'width'=>'5%'],
            ['targets'=>3,'width'=>'15%'],
            ['targets'=>4,'width'=>'15%'],
            ['targets'=>5,'width'=>'15%'],
            ['targets'=>6,'width'=>'15%'],
            ['targets'=>7,'width'=>'15%'],
            ['targets'=>8,'width'=>'5%'],
            ['targets'=>9,'width'=>'5%'],
            ['targets'=>10,'width'=>'5%'],
            ['targets'=>10,'width'=>'5%']
        ];

        for ($i = 0; $i <= 13; $i++) {
            if ($i <= 8) {
                $columndefsdetail[] = ['target'=>$i];
            }
        }
        
        $columns = json_encode($columns);
        $columndefs = json_encode($columndefs);
        $columnsdetail = json_encode($columnsdetail);
        $columndefsdetail = json_encode($columndefsdetail);
        $columnslog = json_encode($columnslog);
        $columndefslog = json_encode($columndefslog);
        $columnscostcontrol = json_encode($columnscostcontrol);
        $columndefscostcontrol = json_encode($columndefscostcontrol);

        $mtypecostcontrol = MP::getData('mtypecostcontrol',['noid','kode','nama']);
        $mcardsupplier = MP::getMCardSupplier();
        $mtt = MP::findMTypeTranc();
        $mtypetranc_noid = $mtt->noid;
        $mtypetranc_nama = $mtt->nama;
        $mtypetranc_kode = $mtt->kode;
        $mtypetranctypestatus = MP::getMTypeTrancTypeStatus(static::$main['idtypetranc']);
        
        $btnexternal = [];
        foreach ($mtypetranctypestatus as $k => $v) {
            if ($v->isinternal == 0 && $v-> isexternal == 1) {
                $btnexternal[] = [
                    'noid' => $v->noid,
                    'idstatusbase' => $v->idstatusbase,
                    'nama' => $v->nama,
                    'classicon' => $v->classicon,
                    'classcolor' => (new static)->color[$v->classcolor],
                ];
            }
        }
        
        $conditions = [];
        foreach ($mtypetranctypestatus as $k => $v) {
            if ($k != 0) {
                $updatedropdown[strtolower($v->idstatusbase)] = [
                    'noid' => $v->noid,
                    'kode' => $v->kode,
                    'nama' => $v->nama,
                    'classicon' => $v->classicon,
                    'classcolor' => (new static)->color[$v->classcolor],
                ];
            }
            $conditions[$v->noid] = [
                'noid' => $v->noid,
                'idstatusbase' => $v->idstatusbase,
                'nama' => $v->nama,
                'prev_noid' => @$mtypetranctypestatus[$k-1]->noid ? $mtypetranctypestatus[$k-1]->noid : null,
                'prev_idstatusbase' => @$mtypetranctypestatus[$k-1]->idstatusbase ? $mtypetranctypestatus[$k-1]->idstatusbase : null,
                'prev_nama' => @$mtypetranctypestatus[$k-1]->nama ? $mtypetranctypestatus[$k-1]->nama : null,
                'prev_classicon' => @$mtypetranctypestatus[$k-1]->classicon ? $mtypetranctypestatus[$k-1]->classicon : null,
                'prev_classcolor' => @$mtypetranctypestatus[$k-1]->classcolor ? (new static)->color[$mtypetranctypestatus[$k-1]->classcolor] : null,
                'next_noid' => @$mtypetranctypestatus[$k+1]->noid ? $mtypetranctypestatus[$k+1]->noid : null,
                'next_idstatusbase' => @$mtypetranctypestatus[$k+1]->idstatusbase ? $mtypetranctypestatus[$k+1]->idstatusbase : null,
                'next_nama' => @$mtypetranctypestatus[$k+1]->nama ? $mtypetranctypestatus[$k+1]->nama : null,
                'next_classicon' => @$mtypetranctypestatus[$k+1]->classicon ? $mtypetranctypestatus[$k+1]->classicon : null,
                'next_classcolor' => @$mtypetranctypestatus[$k+1]->classcolor ? (new static)->color[$mtypetranctypestatus[$k+1]->classcolor] : null,
                'next_table' => static::$main['table2-title'],
                'isinternal' => $v->isinternal == 1 ? 1 : 0,
                'isexternal' => $v->isexternal == 1 ? 1 : 0,
                'btnexternal' => $btnexternal,
                'actview' => $v->actview,
                'actedit' => $v->actedit,
                'actdelete' => $v->actdelete,
                'actreportdetail' => $v->actreportdetail,
                'actreportmaster' => $v->actreportmaster,
                'actsetprevstatus' => $v->actsetprevstatus,
                'actsetnextstatus' => $v->actsetnextstatus,
                'actsetstatus' => $v->actsetstatus,
                'appmtypeaction_noid' => $v->appmtypeaction_noid,
                'appmtypeaction_nama' => $v->appmtypeaction_nama
            ];
        }
        Session::put(static::$main['noid'].'-conditions', $conditions);
        $mttts = MP::findMTypeTrancTypeStatus(static::$main['idtypetranc']);
        $mtypetranctypestatus_noid = $mttts->noid;
        $mtypetranctypestatus_nama = $mttts->nama;
        $mtypetranctypestatus_classcolor = $mttts->classcolor;
        $mcard = MP::getData('mcard',['noid','kode','nama']);
        $accmkodeakun = MP::getData('accmkodeakun');
        $accmcashbank = MP::getData('accmcashbank',['noid','kode','nama']);
        $accmcashbank = MP::getData('accmcashbank',['noid','kode','nama']);
        $accmcurrency = MP::getData('accmcurrency',['noid','kode','nama']);
        $genmgudang = MP::getData('genmgudang',['noid','kode','namalias']);
        $genmcompany = MP::getData('genmcompany',['noid','kode','nama']);
        // $genmcompany = MP::getGenMCompany();
        $genmdepartment = MP::getData('genmdepartment',['noid','kode','nama']);
        $mtypetranc = MP::getData('mtypetranc',['noid','kode','nama']);
        $imi = MP::getInvMInventory();
        foreach ($imi as $k => $v) {
            $invminventory[$k] = $v;
            $invminventory[$k]->purchaseprice2 = 'RP.'.number_format($v->purchaseprice2);
        }
        $invmsatuan = MP::getData('invmsatuan',['noid','kode','nama','konversi']);
        $accmpajak = MP::getData('accmpajak',['noid','kode','nama','prosentax']);
        $color = (new static)->color;
        $lastsupplier = MP::findLastSupplier();
            $lastdata = MP::getLastMonth();
            $lastdata = $lastdata->_year == null && $lastdata->_month == null ? (object)['_year'=>date('y'),'_month'=>date('m')] : $lastdata;
        $datefilter = [
            'a' => 'All',
            't' => date('M d, Y').' - '.date('M d, Y'),
            'y' => date('M d, Y', strtotime('-1 day')).' - '.date('M d, Y', strtotime('-1 day')),
            'l7d' => date('M d, Y').' - '.date('M d, Y', strtotime('-7 day')),
            'l30d' => date('M d, Y').' - '.date('M d, Y', strtotime('1 month')),
            'tm' => date('M d, Y', strtotime('first day of this month')).' - '.date('M d, Y', strtotime('last day of this month')),
            'lm' => date('M d, Y', strtotime('first day of previous month')).' - '.date('M d, Y', strtotime('last day of previous month')),
        ];
        $datefilterdefault = 'tm';
        $tabletitle = static::$main['table-title'];
        $sendto = static::$main['table2-title'];

        $noidnew = explode('/', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH))[1];
        $triggercreate = $noidnew == static::$main['noidnew'] ? true : false;
        
        return view('lam_custom_transaksi_master_detail.'.static::$main['table'].'.index', compact(
            'idtypepage',
            'sessionnoid',
            'sessionname',
            'sessionidcompany',
            'sessioniddepartment',
            'sessionidgudang',
            'maintable',
            'breadcrumb',
            'pagenoid',
            'defaulttanggal',
            'defaulttanggaldue',
            'data',
            'columns',
            'columndefs',
            'columnsdetail',
            'columndefsdetail',
            'columnslog',
            'columndefslog',
            'columnscostcontrol',
            'columndefscostcontrol',
            'mtypecostcontrol',
            'mcardsupplier',
            'mtypetranc_noid',
            'mtypetranc_nama',
            'mtypetranc_kode',
            'mtypetranctypestatus',
            'updatedropdown',
            'mtypetranctypestatus_noid',
            'mtypetranctypestatus_nama',
            'mtypetranctypestatus_classcolor',
            'mcard',
            'accmkodeakun',
            'accmcashbank',
            'accmcurrency',
            'genmgudang',
            'genmcompany',
            'genmdepartment',
            'mtypetranc',
            'invminventory',
            'invmsatuan',
            'accmpajak',
            'color',
            'lastsupplier',
            'datefilter',
            'datefilterdefault',
            'tabletitle',
            'sendto',
            'triggercreate'
        ));
    }

    public function get(Request $request) {
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->get('search')['value'];
        $orderbycolumn = $request->get('columns')[$request->get('order')[0]['column']]['data'];
        if ($orderbycolumn == 'checkbox' || $orderbycolumn == 'no') {
            $orderbycolumn = 'noid';
        }
        $orderby = [
            'column' => $orderbycolumn,
            'sort' => $request->get('order')[0]['dir']
        ];
        $filter = $request->get('filter');
        $datefilter = $request->get('datefilter');
        $datefilter2 = $request->get('datefilter2');

        $result = MP::getPurchase($start, $length, $search, $orderby, $filter, $datefilter, $datefilter2);
        $data = $result['data'];
        $allnoid = MP::getAllNoid(static::$main['table']);

        $resulttotal = count(MP::getPurchase(null, null, $search, $orderby, $filter, $datefilter, $datefilter2)['data']);

        $resultsnst = MP::getMTypeTrancTypeStatus(static::$main['idtypetranc']);
        foreach ($resultsnst as $k => $v) {
            $snst[$v->nourut] = $v;
        }

        $conditions = Session::get(static::$main['noid'].'-conditions');
        $no = $start+1;
        foreach ($data as $k => $v) {
            $checkbox['checkbox'] = '<input type="checkbox" value="FALSE" class="checkbox-m" id="checkbox-m-'.$v->noid.'" onchange="selectCheckboxM('.$v->noid.')">';
            $_no['no'] = $no;
            $data[$k]->mtypetranctypestatus_nama = '<span style="padding: 2px; background: '.(new static)->color[$v->mtypetranctypestatus_classcolor].'">'.$v->mtypetranctypestatus_nama.'</span>';
            $data[$k]->tanggal = $this->dbdateTohumandate($data[$k]->tanggal);
            $data[$k]->tanggaldue = $this->dbdateTohumandate($data[$k]->tanggaldue);
            $data[$k]->totalsubtotal = $v->totalsubtotal;
            $data[$k]->generaltotal = $v->generaltotal;
            $array = (array)$data[$k];

            $btnprev = $conditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase'] && $conditions[$v->mtypetranctypestatus_noid]['isinternal'] 
                ? '<a onclick="showModalSNST('.$v->noid.','.$v->mtypetranc_noid.', '.$snst[$conditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']]->noid.', \''.ucfirst(strtolower($snst[$conditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']]->nama)).'\')" title="Set to '.ucfirst(strtolower($snst[$conditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']]->nama)).'" class="btn btn-sm flat" style="background-color: '.(new static)->color[$snst[$conditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']]->classcolor].'"><i class="'.$snst[$conditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']]->classicon.' icon-xs text-light" style="text-align:center"></i> </a>' 
                : '';
            $btnnext = $conditions[$v->mtypetranctypestatus_noid]['next_idstatusbase'] && $conditions[$v->mtypetranctypestatus_noid]['isinternal'] 
                ? $conditions[$v->mtypetranctypestatus_noid]['next_idstatusbase'] == 4
                    ? '<a onclick="showModalSendToOrder('.$v->noid.','.$v->mtypetranc_noid.')" title="Send to '.static::$main['table2-title'].'" class="btn btn-sm flat" style="background-color: '.(new static)->color[$snst[$conditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->classcolor].'"><i class="'.$snst[$conditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->classicon.' icon-xs text-light" style="text-align:center"></i> </a>'
                    : '<a onclick="showModalSNST('.$v->noid.','.$v->mtypetranc_noid.', '.$snst[$conditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->noid.', \''.ucfirst(strtolower($snst[$conditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->nama)).'\')" title="Set to '.ucfirst(strtolower($snst[$conditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->nama)).'" class="btn btn-sm flat" style="background-color: '.(new static)->color[$snst[$conditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->classcolor].'"><i class="'.$snst[$conditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->classicon.' icon-xs text-light" style="text-align:center"></i> </a>' 
                : '';

            $btnsapcf = $conditions[$v->mtypetranctypestatus_noid]['actsetstatus'] ? '<a onclick="showModalSAPCF('.$v->noid.','.$v->mtypetranc_noid.','.$v->mtypetranctypestatus_noid.')" title="Set Status" class="btn btn-dark btn-sm flat"><i class="icon-cogs icon-xs text-light" style="text-align:center"></i> </a>' : '';
            $btnprint = '<a onclick="printData('.static::$main['noid'].',\''.base64_encode(static::$main['printcode'].'/'.$v->noid.'/'.date('Ymdhis')).'\')" title="Print Data" class="btn btn-sm flat" style="background-color: '.(new static)->color['blue-soft'].'"><i class="fa fa-file icon-xs text-light" style="text-align:center;"></i> </a>';
            $btnlog = '<a onclick="showModalLog('.$v->noid.', \''.$v->kode.'\', \''.$v->mtypetranc_nama.'\')" title="Show Log Data" class="btn btn-sm flat" style="background-color: #8775A7"><i class="icon-file-text icon-xs text-light"></i> </a>';
            $btnview = $conditions[$v->mtypetranctypestatus_noid]['actview'] ? '<a onclick="viewData('.$v->noid.')" title="View Data" class="btn btn-info btn-sm flat"><i class="fa fa-eye icon-xs"></i> </a>' : '';
            $btnedit = $conditions[$v->mtypetranctypestatus_noid]['actedit'] ? '<a onclick="editData('.$v->noid.')" title="Edit Data" class="btn btn-warning btn-sm flat"><i class="fa fa-edit icon-xs" style="text-align:center"></i> </a>' : '';
            $btndelete = $conditions[$v->mtypetranctypestatus_noid]['actdelete'] ? '<a  onclick="showModalDelete('.$v->noid.')" title="Delete Data" class="btn btn-danger btn-sm flat"><i class="fa fa-trash icon-xs"></i> </a>' : '';

            $allbtn = $btnprev;
            $allbtn .= $btnnext;
            $allbtn .= $btnsapcf;
            $allbtn .= $btnprint;
            $allbtn .= $btnlog;
            $allbtn .= $btnview;
            $allbtn .= $btnedit;
            $allbtn .= $btndelete;

            $action = [
                'action' => '<div class="d-flex align-items-center pull-right button-action button-action-'.$k.'">
                    '.$allbtn.'
                </div>'
            ];
            $array['idtypetranc'] = $data[$k]->mtypetranc_nama;
            $array['idstatustranc'] = $data[$k]->mtypetranctypestatus_nama;
            $array['idcardsupplier'] = $data[$k]->mcardsupplier_nama;
            $data[$k] = (object)array_merge($checkbox, $_no, $array, $action);
            $no++;
        }
        
        return response()->json([
            'data' => $data,
            'draw' => intval($request['draw']),
            'recordsFiltered' => $resulttotal,
            'recordsTotal' => $resulttotal,
            'allnoid' => $allnoid,
            'datefilter' => $result['datefilter']
        ]);
    }

    public function get_log(Request $request) {
        $statusadd = $request->get('statusadd');
        $noid = $request->get('noid');
        $kode = $request->get('kode');
        $noidmore = $request->get('noidmore');
        $fieldmore = $request->get('fieldmore');
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->get('search')['value'];
        $orderbycolumn = $request->get('columns')[$request->get('order')[0]['column']]['data'];
        if ($orderbycolumn == 'checkbox' || $orderbycolumn == 'no') {
            $orderbycolumn = 'noid';
        }
        $orderby = [
            'column' => $orderbycolumn,
            'sort' => $request->get('order')[0]['dir']
        ];

        $data = MP::getLog($noid, $kode, $start, $length, $search, $orderby);
        
        if ($search) {
            $resulttotal = count($data);
        } else {
            $resulttotal = count(MP::findLog($noid, $kode));
        }
        // $resulttotal = count($data);

        $no = $start+1;
        foreach ($data as $k => $v) {
            // $checkbox['checkbox'] = '<input type="checkbox" id="checkbox-'.$k.'" onchange="selectCheckbox(\'checkbox-'.$k.'\')">';
            $_no['no'] = $no;
            // $data[$k]->keterangan = substr($v->keterangan, 0, 39).' <button class="btn btn-info btn-sm" onclick="getDatatableLog('.$v->idreff.', \''.$v->kode.'\', '.$v->idreff.', \'keterangan\')">More</button>';
            $data[$k]->logsubjectfull = $v->logsubject;
            $data[$k]->keteranganfull = $v->keterangan;


            // $btnlogsubject = strlen($v->logsubject) <= 44 ? '' : ' <button id="morelog-logsubject-'.$v->noid.'" class="btn btn-success btn-sm" onclick="moreLog('.$v->noid.', \'logsubject\')"><i class="icon-angle-right"></i></button>';
            // $btnketerangan = strlen($v->keterangan) <= 44 ? '' : ' <button id="morelog-keterangan-'.$v->noid.'" class="btn btn-success btn-sm" onclick="moreLog('.$v->noid.', \'keterangan\')"><i class="icon-angle-right"></i></button>';

            $btnlogsubject = strlen($v->logsubject) <= 44 ? '' : '...';
            $btnketerangan = strlen($v->keterangan) <= 44 ? '' : '...';

            $data[$k]->logsubject = substr($v->logsubject, 0, 44).'<span id="logsubject-'.$v->noid.'" style="display: none">'.substr($v->logsubject, 44).'</span>'.$btnlogsubject;
            $data[$k]->keterangan = substr($v->keterangan, 0, 44).'<span id="keterangan-'.$v->noid.'" style="display: none">'.substr($v->keterangan, 44).'</span>'.$btnketerangan;
            $data[$k]->docreate = $this->dbdateTohumandate($data[$k]->docreate);
            $array = (array)$data[$k];
            $data[$k] = (object)array_merge($_no, $array);
            $no++;
        }
        
        return response()->json([
            'data' => $data,
            'draw' => intval($request['draw']),
            'recordsFiltered' => $resulttotal,
            'recordsTotal' => $resulttotal
        ]);
    }

    public function get_costcontrol(Request $request) {
        $statusadd = $request->get('statusadd');
        $statusdetail = $request->get('statusdetail');
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->get('search')['value'];
        $orderbycolumn = $request->get('columns')[$request->get('order')[0]['column']]['data'];
        if ($orderbycolumn == 'checkbox' || $orderbycolumn == 'no') {
            $orderbycolumn = 'noid';
        }
        $orderby = [
            'column' => $orderbycolumn,
            'sort' => $request->get('order')[0]['dir']
        ];

        $data = MP::getCostControl($start, $length, $search, $orderby);
        
        if ($search) {
            $resulttotal = count($data);
        } else {
            $resulttotal = count(MP::getData('salesorder',['noid']));
        }

        $no = $start+1;
        foreach ($data as $k => $v) {
            $checkbox['checkbox'] = '<input type="checkbox" id="checkbox-'.$k.'" onchange="selectCheckbox(\'checkbox-'.$k.'\')">';
            $_no['no'] = $no;
            $data[$k]->tanggal = $this->dbdateTohumandate($data[$k]->tanggal);
            $array = (array)$data[$k];
            $action = [
                'action' => $statusdetail == 'true' ? '' : '<div class="d-flex align-items-center text-center button-action button-action-'.$k.'">
                    <a onclick="chooseCostControl('.$v->noid.', \''.$v->kode.'\', '.$v->idtypecostcontrol.')" title="Choose" class="btn btn-success btn-sm flat"><i class="fa fa-hand-o-up icon-xs" style="text-align:center"></i> Choose</a>
                </div>'
            ];
            $data[$k] = (object)array_merge($checkbox, $_no, $array, $action);
            $no++;
        }
        
        return response()->json([
            'data' => $data,
            'draw' => intval($request['draw']),
            'recordsFiltered' => $resulttotal,
            'recordsTotal' => $resulttotal
        ]);
    }

    public function get_detailpi(Request $request) {
        $statusdetail = json_decode($request->get('statusdetail'));
        $idcardsupplier = json_decode($request->get('idcardsupplier'));
        $idcompany = json_decode($request->get('idcompany'));
        $iddepartment = json_decode($request->get('iddepartment'));
        $idgudang = json_decode($request->get('idgudang'));
        $detailprev = json_decode($request->get('detailprev'));
        $unitqtyminpo = json_decode($request->get('unitqtyminpo'));
        $stp = $request->get('searchtypeprev');
        $searchprev = $request->get('searchprev');
        $search = $request->get('search')['value'];
        $start = $request->get('start');
        $length = $request->get('length');
        $nextnoid = MP::getNextNoid(static::$main['table']);
        $nextnoiddetail = MP::getNextNoid(static::$main['tabledetail']);
        
        $params = [
            'idcardsupplier' => $idcardsupplier,
            'idcompany' => $idcompany,
            'iddepartment' => $iddepartment,
            'idgudang' => $idgudang,
            'detailprev' => $detailprev,
            'searchtypeprev' => $stp,
            'searchprev' => $searchprev,
            'search' => $search,
            'start' => $start,
            'length' => $length,
        ];
        $data = $stp == 1 ? MP::getDetailPrev($params) : MP::getDetailInv($params);

        if ($search) {
            $qty = count($data);
        } else {
            $params['start'] = null;
            $params['length'] = null;
            $qty = count($stp == 1 ? MP::getDetailPrev($params) : MP::getDetailInv($params));
        }

        $no = $start+1;
        $index = 0;
        $result = [];
        $pattern = "/".$search."/i";

        foreach ($data as $k => $v) {
            $v = (array)$v;
            $v['checkbox'] = '<input type="checkbox" id="checkbox-detail-'.$k.'" onchange="selectCheckbox(\'checkbox-detail-'.$k.'\')">';
            $v['no'] = $no;
            if ($stp == 2) {
                $v['noid'] = $nextnoid;
                $v['idmaster'] = '-';
                $v['kodeprev'] = '-';
                $v['namainventor2'] = '-';
                $v['keterangan'] = '-';
                $v['unitqty'] = '-';
                $v['unitqtysisa'] = '-';
                $v['subtotal'] = '-';
                $v['discountvar'] = '-';
                $v['discount'] = '-';
                $v['nilaidisc'] = '-';
                $v['idtypetax'] = '-';
                $v['idtax'] = '-';
                $v['prosentax'] = '-';
                $v['nilaitax'] = '-';
                $v['hargatotal'] = '-';
                $v['namacurrency'] = '-';
                $v['idcompany'] = $idcompany;
                $v['iddepartment'] = $iddepartment;
                $v['idgudang'] = $idgudang;
            } 

            $isaction = true;
            foreach ($detailprev as $k2 => $v2) {
                if ($v2->noid == $v['noid']) {
                    if ($stp == 1) {
                        $v['unitqtysisa'] = $v['unitqtysisa']-$v2->unitqty < 0 ? 0 : $v['unitqtysisa']-$v2->unitqty;
                        $isaction = false;
                        break;
                    }
                }
            }

            $v['action'] = $isaction ? '<div class="d-flex align-items-center text-center button-action-dprev button-action-dprev-'.$k.'">
                <a onclick="chooseDetailPrev(
                    '.$v['noid'].',
                    \''.$v['idmaster'].'\',
                    \''.$v['kodeprev'].'\',
                    \''.$v['idinventor'].'\',
                    \''.$v['kodeinventor'].'\',
                    \''.$v['namainventor'].'\',
                    \''.$v['namainventor2'].'\',
                    \''.$v['keterangan'].'\',
                    \''.$v['idgudang'].'\',
                    \''.$v['idcompany'].'\',
                    \''.$v['iddepartment'].'\',
                    \''.$v['idsatuan'].'\',
                    \''.$v['namasatuan'].'\',
                    \''.$v['konvsatuan'].'\',
                    \''.$v['unitqty'].'\',
                    \''.$v['unitqtysisa'].'\',
                    \''.$v['unitprice'].'\',
                    \''.$v['subtotal'].'\',
                    \''.$v['discountvar'].'\',
                    \''.$v['discount'].'\',
                    \''.$v['nilaidisc'].'\',
                    \''.$v['idtypetax'].'\',
                    \''.$v['idtax'].'\',
                    \''.$v['prosentax'].'\',
                    \''.$v['nilaitax'].'\',
                    \''.$v['hargatotal'].'\',
                    \''.$v['namacurrency'].'\'
                )" title="Choose" class="btn btn-success btn-sm flat"><i class="icon-hand-up icon-xs" style="text-align:center"></i> Choose</a>
            </div>' : '';

                $result[] = $v;
            $no++;
            $nextnoid;
        }

        return response()->json([
            'data' => $result,
            'draw' => intval($request['draw']),
            'recordsFiltered' => $qty,
            'recordsTotal' => $qty,
            'length' => $request->get('length')
        ]);
    }

    public function get_detail(Request $request) {
        $data = json_decode($request->get('data'));
        $statusdetail = json_decode($request->get('statusdetail'));
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->get('search')['value'];

        $qtyshow = 0;
        $qtyall = 0;
        $no = 1;
        $index = 0;
        $result = [];
        $pattern = "/".$search."/i";
        // dd($start);

        foreach ($data as $k => $v) {
            if ($v != null) {
                if ($k >= (int)$start && $qtyshow <= $length) {
                    $insertrecord = false;
                    foreach ($v as $k2 => $v2) {
                        if ($v2->name != 'idinventor') {
                            if (!array_key_exists('value', (array)$v2)) {
                                $v2->value = 0;
                            }
                            if (preg_match($pattern, $v2->value)) {
                                $insertrecord = true;
                            }
                            if ($v2->name == 'subtotal') {
                                $result[$index][$v2->name] = is_numeric($v2->value) ? 'Rp.'.number_format($v2->value) : $v2->value;
                            } else if ($v2->name == 'unitprice' || $v2->name == 'hargatotal') {
                                $result[$index][$v2->name] = filter_var($v2->value, FILTER_SANITIZE_NUMBER_INT);
                            } else {
                                $result[$index][$v2->name] = $v2->value;
                            }
                        }
                    }
                    if (!$insertrecord) {
                        array_pop($result);
                        continue;
                    }
                    $result[$index]['checkbox'] = '<input type="checkbox" id="checkbox-detail-'.$k.'" onchange="selectCheckbox(\'checkbox-detail-'.$k.'\')">';
                    $result[$index]['no'] = $no;

                    if ($statusdetail) {
                        $btneditordetail = '
                            <a onclick="editDetail(\''.$k.'\', '.$result[$index]['noid'].')"  class="btn btn-success btn-sm flat mr-2"><i class="fa fa-search icon-xs"></i> </a>
                        ';
                    } else {
                        $btneditordetail = '
                            <a onclick="editDetail(\''.$k.'\', '.$result[$index]['noid'].')" class="btn btn-warning btn-sm flat mr-1"><i class="fa fa-edit icon-xs" style="text-align:center"></i> </a>
                            <a  onclick="removeDetail(\''.$k.'\', '.$result[$index]['noid'].')" class="btn btn-danger btn-sm flat mr-0"><i class="fa fa-trash icon-xs"></i> </a>
                        ';
                    }

                    $result[$index]['action'] = '<div class="d-flex align-items-center button-action-tab button-action-tab-'.$k.'">
                        '.$btneditordetail.'
                    </div>';
                    if (count($result) == $length || count($data) == $k+1) {
                        break;
                    }
                    $index++;
                    $qtyshow++;
                }
                $no++;
            }
        }

        return response()->json([
            'data' => $result,
            'draw' => intval($request['draw']),
            'recordsFiltered' => $search ? count($result) : count($data),
            'recordsTotal' => $search ? count($result) : count($data),
            'length' => $request->get('length')
        ]);
    }

    private function humandateTodbdate($date) {
        if ($date) {
            $arrdate = explode('-', $date);
            $arrtime = explode(' ', $date);
            $year = explode(' ',$arrdate[2])[0];
            $month = $arrdate[1];
            $day = $arrdate[0];
            $time = count(explode(' ',$arrdate[2])) == 2 ? explode(' ',$arrdate[2])[1] : '';

            if ($month == 'Jan') {
                $month = '01';
            } else if ($month == 'Feb') {
                $month = '02';
            } else if ($month == 'Mar') {
                $month = '03';
            } else if ($month == 'Apr') {
                $month = '04';
            } else if ($month == 'May') {
                $month = '05';
            } else if ($month == 'Jun') {
                $month = '06';
            } else if ($month == 'Jul') {
                $month = '07';
            } else if ($month == 'Aug') {
                $month = '08';
            } else if ($month == 'Sep') {
                $month = '09';
            } else if ($month == 'Oct') {
                $month = '10';
            } else if ($month == 'Nov') {
                $month = '11';
            } else if ($month == 'Dec') {
                $month = '12';
            }

            return $year.'-'.$month.'-'.$day.' '.$time;
        } else {
            return NULL;
        }
    }

    private function dbdateTohumandate($date) {
        if ($date) {
            $arrdate = explode('-', $date);
            $arrtime = explode(' ', $date);
            $year = $arrdate[0];
            $month = $arrdate[1];
            $day = explode(' ',$arrdate[2])[0];

            if ($month == '01') {
                $month = 'Jan';
            } else if ($month == '02') {
                $month = 'Feb';
            } else if ($month == '03') {
                $month = 'Mar';
            } else if ($month == '04') {
                $month = 'Apr';
            } else if ($month == '05') {
                $month = 'May';
            } else if ($month == '06') {
                $month = 'Jun';
            } else if ($month == '07') {
                $month = 'Jul';
            } else if ($month == '08') {
                $month = 'Aug';
            } else if ($month == '09') {
                $month = 'Sep';
            } else if ($month == '10') {
                $month = 'Oct';
            } else if ($month == '11') {
                $month = 'Nov';
            } else if ($month == '12') {
                $month = 'Dec';
            }

            return count($arrtime) > 1 ? $day.'-'.$month.'-'.$year.' '.$arrtime[1] : $day.'-'.$month.'-'.$year;
        } else {
            return NULL;
        }
    }

    public function save(Request $request) {
        $statusadd = json_decode($request->get('statusadd'));
        $next_idstatustranc = json_decode($request->get('next_idstatustranc'));
        $next_nama = json_decode($request->get('next_nama'));
        $next_keterangan = json_decode($request->get('next_keterangan'));
        $noid = $request->get('noid');
        $nextnoid = MP::getNextNoid(static::$main['table']);
        $nextnoiddetail = MP::getNextNoid(static::$main['tabledetail']);

        foreach ($request->get('formheader') as $k => $v) {
            if ($k == 'kodetypetranc') {
                continue;
            } else if ($k == 'dostatus' || $k == 'tanggal' || $k == 'tanggaldue') {
                $formheader[$k] = $this->humandateTodbdate($v);
            } else if ($k == 'idstatustranc') {
                $formheader[$k] = $next_idstatustranc ? $next_idstatustranc : 5051;
            } else {
                $formheader[$k] = $v;
            }
        }
        // $nextnourut = MP::getNextNourut(['table'=>static::$main['table'],'where'=>$formheader['idcostcontrol']]);
        // $formheader['nourut'] = $nextnourut;

        foreach ($request->get('formmodaltanggal') as $k => $v) {
            $formmodaltanggal[$k] = $this->humandateTodbdate($v);
        }
        foreach ($request->get('formfooter') as $k => $v) {
            if ($k == 'subtotal') {
                $formfooter['totalsubtotal'] = filter_var($v, FILTER_SANITIZE_NUMBER_INT);
            } else if ($k == 'total') {
                $formfooter['generaltotal'] = filter_var($v, FILTER_SANITIZE_NUMBER_INT);
            } else {
                $formfooter[$k] = $v;
            }
        }

        $input = [
            'formheader' => $formheader,
            'forminternal' => $request->get('forminternal'),
            'formfooter' => $formfooter,
            'formmodaltanggal' => $formmodaltanggal,
            'formmodalsupplier' => $request->get('formmodalsupplier'),
            'prd' => $request->get('purchasedetail')
        ];

        $table = array_merge(
            [
                'noid' => $statusadd ? $nextnoid : $noid,
                'idtypetranc' => 0,
                'idcreate' => Session::get('noid'),
                'docreate' => date('Y-m-d H:i:s'),
                'idupdate' => Session::get('noid'),
                'lastupdate' => date('Y-m-d H:i:s'),
            ],
            $input['formheader'],
            $input['forminternal'],
            $input['formmodaltanggal'],
            $input['formmodalsupplier'],
            $input['formfooter']
        );

        $tabledetail = [];
        if ($input['prd'] != null) {
            foreach ($input['prd'] as $k => $v) {
                $tabledetail[$k]['noid'] = $nextnoiddetail;
                $tabledetail[$k]['idmaster'] = $statusadd ? $nextnoid : $noid;
                foreach ($v as $k2 => $v2) {
                    if (!array_key_exists('value', (array)$v2)) {
                        $v2['value'] = 0;
                    }
                    if ($v2['name'] == 'kodeprev' || $v2['name'] == 'unitqtysisaprev') {
                        if ($statusadd) {
                            $tabledetail[$k][$v2['name']] = $v2['value'];
                        } else {
                            continue;
                        }
                    } else if ($v2['name'] == 'noid' || $v2['name'] == 'kodeinventor' || $v2['name'] == 'namainventor' || $v2['name'] == 'idcurrency' || $v2['name'] == 'namacurrency' || $v2['name'] == 'namasatuan') {
                        continue;
                    } else if ($v2['name'] == 'namainventor2') {
                        $tabledetail[$k]['namainventor'] = $v2['value'];
                    } else if ($v2['name'] == 'unitprice' || $v2['name'] == 'subtotal' || $v2['name'] == 'hargatotal') {
                        $tabledetail[$k][$v2['name']] = filter_var($v2['value'], FILTER_SANITIZE_NUMBER_INT);
                    } else if ($v2['name'] == 'discountvar') {
                        $tabledetail[$k][$v2['name']] = $v2['value'] ? $v2['value'] : 0;
                    } else {
                        $tabledetail[$k][$v2['name']] = $v2['value'];
                    }
                }
                $nextnoiddetail++;
            }
        }

        $data = [
            static::$main['table'] => $table,
            static::$main['tabledetail'] => $tabledetail,
        ];
        
        if ($statusadd) {
            $savetonext = $next_idstatustranc ? ' (Save to '.$next_nama.')' : '';
            $logsubject = 'Data '.$request->get('formheader')['kodetypetranc'].' Kode = '.$data[static::$main['table']]['kode'].' CREATE'.$savetonext;
            if ($next_idstatustranc) {
                $keterangan = $next_keterangan;
            } else {
                $keterangan = 'Data '.$request->get('formheader')['kodetypetranc'].' Kode = '.$data[static::$main['table']]['kode'].' CREATE'.$savetonext.' BY '.Session::get('myusername').' ON '.date('Y-m-d H:i:s');
            }
        } else {
            $savetonext = $next_idstatustranc ? ' (Edit to '.$next_nama.')' : '';
            $logsubject = 'Data '.$request->get('formheader')['kodetypetranc'].' Kode = '.$data[static::$main['table']]['kode'].' EDIT'.$savetonext;
            if ($next_idstatustranc) {
                $keterangan = $next_keterangan;
            } else {
                $keterangan = 'Data '.$request->get('formheader')['kodetypetranc'].' Kode = '.$data[static::$main['table']]['kode'].' EDIT'.$savetonext.' BY '.Session::get('myusername').' ON '.date('Y-m-d H:i:s');
            }
        }

        $datalog = [
            'idtypetranc' => $data[static::$main['table']]['idtypetranc'],
            'idtypeaction' => $statusadd ? 1 : 2,
            'logsubject' => $logsubject,
            'keterangan' => $keterangan,
            'idreff' => $data[static::$main['table']]['noid'],
            'idreffd' => 0,
            'iddevice' => 0,
            'devicetoken' => $request->get('_token'),
            'idcreate' => $data[static::$main['table']]['idcreate'],
            'docreate' => $data[static::$main['table']]['docreate'],
        ];
        
        if ($statusadd) {
            $result = MP::saveData($data, $datalog);
        } else {
            $result = MP::updateData($data, $noid, $datalog);
        }

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => $statusadd ? 'Insert Successfully' : 'Update Successfully'
            ]);
        } else {
            return response()->json([
                'success' => true,
                'message' => $statusadd ? 'Insert Error' : 'Update Error'
            ]);
        }
    }

    public function find(Request $request) {
        $noid = $request->get('noid');
        $result = MP::findData($noid);
        $result['purchase']->dostatus = $this->dbdateTohumandate($result['purchase']->dostatus);
        $result['purchase']->tanggal = $this->dbdateTohumandate($result['purchase']->tanggal);
        $result['purchase']->tanggaltax = $this->dbdateTohumandate($result['purchase']->tanggaltax);
        $result['purchase']->tanggaldue = $this->dbdateTohumandate($result['purchase']->tanggaldue);
        $result['purchase']->mtypetranctypestatus_nama = $result['purchase']->idstatustranc ? $result['purchase']->mtypetranctypestatus_nama : 'DRAFT';
        $result['purchase']->mtypetranctypestatus_classcolor = $result['purchase']->idstatustranc ? (new static)->color[$result['purchase']->mtypetranctypestatus_classcolor] : 'grey';

        $prd = [];
        foreach ($result['purchasedetail'] as $k => $v) {
            $prd[$k] = [];
            foreach ($v as $k2 => $v2) {
                $prd[$k][] = [
                    'name' => $k2,
                    'value' => $v2,
                ];
            }
            $prd[$k][] = ['name' => 'idcurrency','value' => $result['purchase']->idcurrency];
            $prd[$k][] = ['name' => 'namacurrency','value' => $result['purchase']->namacurrency];
        }
        $result['purchasedetail'] = $prd;
        $result['mttts'] = Session::get(static::$main['noid'].'-conditions')[$result['purchase']->idstatustranc];

        return response()->json([
            'success' => true,
            'message' => 'Find Successfully',
            'data' => $result
        ]);
    }
    
    public function generate_code(Request $request) {
        $noid = $request->get('noid');
        $nextnoid = MP::getNextNoid(static::$main['table']);

        foreach ($request->get('formheader') as $k => $v) {
            if ($k == 'kodetypetranc') {
                continue;
            } else if ($k == 'idcostcontrol') {
                $formheader[$k] = $v ? $v : 0;
            } else if ($k == 'dostatus' || $k == 'tanggal' || $k == 'tanggaldue') {
                $formheader[$k] = $this->humandateTodbdate($v);
            } else if ($k == 'idstatustranc') {
                $formheader[$k] = @$next_idstatustranc ? $next_idstatustranc : 5011;
            } else {
                $formheader[$k] = $v;
            }
        }
        // $nextnourut = MP::getNextNourut(['table'=>static::$main['table'],'where'=>$formheader['idcostcontrol']]);
        // $formheader['nourut'] = $nextnourut;

        foreach ($request->get('formmodaltanggal') as $k => $v) {
            $formmodaltanggal[$k] = $this->humandateTodbdate($v);
        }
        foreach ($request->get('formfooter') as $k => $v) {
            if ($k == 'subtotal') {
                $formfooter['totalsubtotal'] = filter_var($v, FILTER_SANITIZE_NUMBER_INT);
            } else if ($k == 'total') {
                $formfooter['generaltotal'] = filter_var($v, FILTER_SANITIZE_NUMBER_INT);
            } else {
                $formfooter[$k] = $v;
            }
        }

        $input = [
            'formheader' => $formheader,
            'forminternal' => $request->get('forminternal'),
            'formfooter' => $formfooter,
            'formmodaltanggal' => $formmodaltanggal,
            'formmodalsupplier' => $request->get('formmodalsupplier'),
            'prd' => $request->get('purchasedetail')
        ];

        $table = array_merge(
            [
                'noid' => $nextnoid,
                'idtypetranc' => 0,
                'idcreate' => Session::get('noid'),
                'docreate' => date('Y-m-d H:i:s'),
                'idupdate' => Session::get('noid'),
                'lastupdate' => date('Y-m-d H:i:s'),
            ],
            $input['formheader'],
            $input['forminternal'],
            $input['formmodaltanggal'],
            $input['formmodalsupplier'],
            $input['formfooter']
        );

        $params = [
            'tablemaster' => static::$main['table'],
            'generatecode' => static::$main['generatecode'],
            'idcostcontrol' => $request->get('idcostcontrol'),
            'kodecostcontrol' => $request->get('kodecostcontrol'),
            'data' => $table
        ];
        
        $result = MP::generateCode($params);

        return response()->json([
            'success' => true,
            'message' => 'Find Successfully',
            'data' => $result
        ]);
    }

    public function get_info() {
        $costcontrol = MP::getDefaultCostControl();

        return response()->json([
            'success' => true,
            'message' => 'Find Successfully',
            'data' => [
                'pr' => (object)[
                    'noid' => null,
                    'idtypetranc' => static::$main['idtypetranc']
                ],
                'mttts' => Session::get(static::$main['noid'].'-conditions')[static::$main['idtypetranc'].'1'],
                'mtypetranctypestatus' => [
                    'noid' => MP::findMTypeTrancTypeStatus(static::$main['idtypetranc'])->noid,
                    'nama' => 'DRAFT',
                    'classcolor' => (new static)->color['grey-silver']
                ],
                'mcarduser' => [
                    'noid' => Session::get('noid'),
                    'nama' => Session::get('myusername')
                ],
                'idtypecostcontrol' => $costcontrol->idtypecostcontrol,
                'idcostcontrol' => $costcontrol->noid,
                'kodecostcontrol' => $costcontrol->kode,
                'dostatus' => date('d-M-Y H:i:s'),
                'idcompany' => Session::get('idcompany'),
                'iddepartment' => Session::get('iddepartment'),
                'idgudang' => Session::get('idgudang'),
            ]
        ]);
    }

    public function find_last_supplier() {
        $result = MP::findLastSupplier();

        return response()->json([
            'success' => true,
            'message' => 'Find Successfully',
            'data' => $result
        ]);
    }

    public function send_to_order(Request $request) {
        $devicetoken = $request->get('_token');
        $noid = $request->get('noid');
        $find = MP::findData($noid);
        $noidorder = MP::getNextNoid(static::$main['table2']);
        $noidorderdetail = MP::getNextNoid(static::$main['tabledetail2']);
        $nextkodetypetranc = MP::getKode('mtypetranc',static::$main['idtypetranc2']);
        $params = [
            'tablemaster' => static::$main['table2'],
            'generatecode' => static::$main['generatecode2'],
            'data' => (array)$find['purchase']
        ];
        $gcpo = MP2::generateCode($params);
        $typetranc = $find['purchase']->kodetypetranc;
        $idreffd = '';
        if ($find['purchasedetail']) {
            foreach ($find['purchasedetail'] as $k => $v) {
                $idreffd .= $v->noid;
            }
        }

        if ($find['purchase']->idstatustranc == 7) {
            $sendtopo = 'OTHERS (Send to '.static::$main['table2-title'].')';
        } else {
            $sendtopo = 'SET APPROVED (Approved to '.static::$main['table2-title'].')';
        }

        $datalog = [
            [
                'idtypetranc' => static::$main['idtypetranc'],
                'idtypeaction' => $find['purchase']->idstatustranc == 5057 ? 99 : 10,
                'logsubject' => 'Data '.$find['purchase']->kodetypetranc.' Kode = '.$find['purchase']->kode.' '.$sendtopo,
                'keterangan' => 'Data '.$find['purchase']->kodetypetranc.' Kode = '.$find['purchase']->kode.' '.$sendtopo.' BY '.Session::get('myusername').' ON '.date('Y-m-d H:i:s'),
                'idreff' => $find['purchase']->noid,
                'idreffd' => $idreffd,
                'iddevice' => 0,
                'devicetoken' => $devicetoken,
                'idcreate' => Session::get('noid'),
                'docreate' => date('Y-m-d H:i:s'),
            ],[
                'idtypetranc' => static::$main['idtypetranc2'],
                'idtypeaction' => 1,
                'logsubject' => 'Data '.$nextkodetypetranc.' Kode = '.$gcpo['kode'].' CREATE',
                'keterangan' => 'Data '.$nextkodetypetranc.' Kode = '.$gcpo['kode'].' CREATE BY '.Session::get('myusername').' ON '.date('Y-m-d H:i:s'),
                'idreff' => $gcpo['nourut'],
                'idreffd' => $idreffd,
                'iddevice' => 0,
                'devicetoken' => $devicetoken,
                'idcreate' => Session::get('noid'),
                'docreate' => date('Y-m-d H:i:s'),
            ]
        ];

        $result = MP::sendToNext($noid, $noidorder, $noidorderdetail, $datalog);

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Send to '.static::$main['table2-title'].' Successfully',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Send to '.static::$main['table2-title'].' Error!',
            ]);
        }
    }

    public function snst(Request $request) {
        $noid = $request->get('noid');
        $find = MP::findData($noid);
        $idtypetranc = $find['purchase']->idtypetranc;
        $idstatustranc = $request->get('idstatustranc');
        $statustranc = $request->get('statustranc');

        $typeaction = Session::get(static::$main['noid'].'-conditions')[$idstatustranc]['appmtypeaction_nama'];
        $idtypeaction = Session::get(static::$main['noid'].'-conditions')[$idstatustranc]['appmtypeaction_noid'];
        
        $idreff = $find['purchase']->noid;
        $logsubject = 'Data '.$find['purchase']->kodetypetranc.' Kode = '.$find['purchase']->kode.' '.$typeaction;

        $data = [
            'idstatustranc' => $idstatustranc
        ];

        $datalog = [
            'idtypetranc' => $idtypetranc,
            'idtypeaction' => $idtypeaction,
            'logsubject' => $logsubject,
            'keterangan' => $request->get('keterangan'),
            'idreff' => $idreff,
            'idreffd' => 0,
            'iddevice' => 0,
            'devicetoken' => $request->get('_token'),
            'idcreate' => Session::get('noid'),
            'docreate' => date('Y-m-d H:i:s'),
        ];

        $result = MP::snst($data, $noid, $datalog);

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Set '.$statustranc.' Successfully',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Set '.$statustranc.' Error!',
            ]);
        }
    }

    public function div(Request $request) {
        $devicetoken = $request->get('_token');
        $noid = $request->get('noid');
        $find = MP::findData($noid);
        $idtypetranc = $request->get('idtypetranc');
        $typetranc = $find['purchase']->kodetypetranc;
        $idstatustranc = $request->get('idstatustranc');
        $statustranc = $request->get('statustranc');
        $keterangan = $request->get('keterangan');
        $idreff = $find['purchase']->noid;
        $idreffd = '';
        // if ($find['purchasedetail']) {
        //     foreach ($find['purchasedetail'] as $k => $v) {
        //         $idreffd .= $v->noid;
        //     }
        // }
        if ($idstatustranc == 1) {
            $idtypeaction = 7;
            $typeaction = 'SET DRAFT';
        } else if ($idstatustranc == 2) {
            $idtypeaction = 8;
            $typeaction = 'SET ISSUED';
        } else if ($idstatustranc == 3) {
            $idtypeaction = 9;
            $typeaction = 'SET VERIFIED';
        }
        $logsubject = 'Data '.$typetranc.' Kode = '.$find['purchase']->kode.' '.$typeaction;

        $data = [
            'idstatustranc' => $idstatustranc
        ];

        $datalog = [
            'idtypetranc' => $idtypetranc,
            'idtypeaction' => $idtypeaction,
            'logsubject' => $logsubject,
            'keterangan' => $keterangan,
            'idreff' => $idreff,
            'idreffd' => $idreffd,
            'iddevice' => 0,
            'devicetoken' => $devicetoken,
            'idcreate' => Session::get('noid'),
            'docreate' => date('Y-m-d H:i:s'),
        ];

        $result = MP::div($data, $noid, $datalog);

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Set '.$statustranc.' Successfully',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Set '.$statustranc.' Error!',
            ]);
        }
    }

    public function sapcf(Request $request) {
        $devicetoken = $request->get('_token');
        $noid = $request->get('noid');
        $find = MP::findData($noid);
        $idtypetranc = $request->get('idtypetranc');
        $typetranc = $find['purchase']->kodetypetranc;
        $idstatustranc = $request->get('idstatustranc');
        $statustranc = $request->get('statustranc');
        $keterangan = $request->get('keterangan');
        $idreff = $find['purchase']->noid;
        $idreffd = '';
        // if ($find['purchasedetail']) {
        //     foreach ($find['purchasedetail'] as $k => $v) {
        //         $idreffd .= $v->noid;
        //     }
        // }
        $idtypeaction = Session::get(static::$main['noid'].'-conditions')[$idstatustranc]['appmtypeaction_noid'];
        $typeaction = Session::get(static::$main['noid'].'-conditions')[$idstatustranc]['appmtypeaction_nama'];
        $logsubject = 'Data '.$typetranc.' Kode = '.$find['purchase']->kode.' '.$typeaction;

        $data = [
            'idstatustranc' => $idstatustranc
        ];

        $datalog = [
            'idtypetranc' => $idtypetranc,
            'idtypeaction' => $idtypeaction,
            'logsubject' => $logsubject,
            'keterangan' => $keterangan,
            'idreff' => $idreff,
            'idreffd' => $idreffd,
            'iddevice' => 0,
            'devicetoken' => $devicetoken,
            'idcreate' => Session::get('noid'),
            'docreate' => date('Y-m-d H:i:s'),
        ];

        $result = MP::sapcf($data, $noid, $datalog);

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Set '.$statustranc.' Successfully',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Set '.$statustranc.' Error!',
            ]);
        }
    }

    public function set_pending(Request $request) {
        $noid = $request->get('noid');

        $data = [
            'idstatustranc' => 5
        ];

        $datalog = [
            'idtypetranc' => $request->get('idtypetranc'),
            'idtypeaction' => 9,
            'logsubject' => 'PURCHASE-REQUEST/PENDING/'.$noid,
            'keterangan' => $request->get('keterangan'),
            'idreff' => 0,
            'idreffd' => 0,
            'iddevice' => 0,
            'devicetoken' => $request->get('_token'),
            'idcreate' => Session::get('noid'),
            'docreate' => date('Y-m-d H:i:s'),
        ];

        $result = MP::setPending($data, $noid, $datalog);

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Set Pending Successfully',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Set Pending Error!',
            ]);
        }
    }

    public function set_canceled(Request $request) {
        $noid = $request->get('noid');

        $data = [
            'idstatustranc' => 6
        ];

        $datalog = [
            'idtypetranc' => $request->get('idtypetranc'),
            'idtypeaction' => 9,
            'logsubject' => 'PURCHASE-REQUEST/CANCELED/'.$noid,
            'keterangan' => $request->get('keterangan'),
            'idreff' => 0,
            'idreffd' => 0,
            'iddevice' => 0,
            'devicetoken' => $request->get('_token'),
            'idcreate' => Session::get('noid'),
            'docreate' => date('Y-m-d H:i:s'),
        ];

        $result = MP::setCanceled($data, $noid, $datalog);

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Set Canceled Successfully',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Set Canceled Error!',
            ]);
        }
    }

    public function delete_select_m(Request $request) {
        $selecteds = $request->get('selecteds');
        $selectcheckboxmall = $request->get('selectcheckboxmall');

        if ($selectcheckboxmall) {
            $datalog = [
                'idtypetranc' => static::$main['idtypetranc'],
                'idtypeaction' => 3,
                'logsubject' => 'PURCHASE-REQUEST/DELETEALL/All',
                'keterangan' => 'Data All Deleted',
                'idreff' => 0,
                'idreffd' => 0,
                'iddevice' => 0,
                'devicetoken' => $request->get('_token'),
                'idcreate' => Session::get('noid'),
                'docreate' => date('Y-m-d H:i:s'),
            ];
        } else {
            $datalog = [];
            foreach ($selecteds as $k => $v) {
                $datalog[$v] = [
                    'idtypetranc' => static::$main['idtypetranc'],
                    'idtypeaction' => 3,
                    'logsubject' => 'PURCHASE-REQUEST/DELETE/'.$v,
                    'keterangan' => 'Data Deleted',
                    'idreff' => 0,
                    'idreffd' => 0,
                    'iddevice' => 0,
                    'devicetoken' => $request->get('_token'),
                    'idcreate' => Session::get('noid'),
                    'docreate' => date('Y-m-d H:i:s'),
                ];
            }
        }

        $result = MP::deleteSelectM($selecteds, $selectcheckboxmall, $datalog);

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Delete Successfully',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Delete Error!',
            ]);
        }
    }

    public function delete(Request $request) {
        $noid = $request->get('noid');
        $find = MP::findData($noid);
        $datalog = [
            'idtypetranc' => $find['purchase']->idtypetranc,
            'idtypeaction' => 3,
            'logsubject' => 'Data '.$find['purchase']->kodetypetranc.' Kode = '.$find['purchase']->kode.' DELETE',
            'keterangan' => 'Data '.$find['purchase']->kodetypetranc.' Kode = '.$find['purchase']->kode.' DELETE BY '.Session::get('myusername').' ON '.date('Y-m-d H:i:s'),
            'idreff' => $find['purchase']->noid,
            'idreffd' => 0,
            'iddevice' => 0,
            'devicetoken' => $request->get('_token'),
            'idcreate' => Session::get('noid'),
            'docreate' => date('Y-m-d H:i:s'),
        ];
        $result = MP::deleteData($noid, $datalog);

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Delete Successfully',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Delete Error!',
            ]);
        }
    }
}