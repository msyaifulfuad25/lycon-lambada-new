<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class LoginUserController extends Controller {

    public function loginPost(Request $request){
      // dd('ok');
      // $hashedpass = bcrypt('ADM');
      // print_r($hashedpass);die();
      // $pw = 123456;
      //  $hashed = Hash::make($pw);
      //  print_r(Hash::check($pw, $hashed));die();
        $myusername = $request->username;
        $password = $request->password;

        // $data = User::where('myusername',$myusername)->first();
        $data = User::findData($myusername);
        $position = User::findPosition($myusername);
        // $programs = DB::connection('mysql2')->select("SELECT * FROM mcarduser WHERE myusername = '".$data->myusername."' ");
        if ($data){ //apakah email tersebut ada atau tidak
            if ($data->mcarduser_mypassword == sha1($password)){
                Session::put('myusername', $data->mcarduser_myusername);
                Session::put('idhomepage', $data->mcarduser_idhomepage);
                Session::put('noid', $data->mcarduser_noid);
                Session::put('groupuser', $data->mcarduser_groupuser);
                Session::put('nama', $data->mcard_nama);
                Session::put('firstname', $data->mcard_firstname);
                Session::put('middlename', $data->mcard_middlename);
                Session::put('lastname', $data->mcard_lastname);
                Session::put('email', $data->mcard_email);
                Session::put('idcompany', $data->mcardemployee_idcompany);
                Session::put('iddepartment', $data->mcardemployee_iddepartment);
                Session::put('idgudang', $data->mcardemployee_idgudang);
                Session::put('position', $position);
                Session::put('page',[]);
                Session::put('login',TRUE);
                // print(Session::get('groupuser'));die;
                return redirect()->back();
                // return redirect('/');
            } else {
                return redirect()->back();
                // return redirect('login')->with('alert','Password atau Email, Salah !');
            }
        } else {
            return redirect()->back();
            // return redirect('login')->with('alert','Password atau Email, Salah!');
        }
    }

    public function logout(){
        Session::flush();
        return redirect('/');
    }

    public function register(Request $request){
        return view('register');
    }

    public function registerPost(Request $request){
        $this->validate($request, [
            'name' => 'required|min:4',
            'email' => 'required|min:4|email|unique:users',
            'password' => 'required',
            'confirmation' => 'required|same:password',
        ]);

        $data =  new ModelUser();
        $data->name = $request->name;
        $data->email = $request->email;
        $data->password = bcrypt($request->password);
        $data->save();
        return redirect('login')->with('alert-success','Kamu berhasil Register');
    }
}
