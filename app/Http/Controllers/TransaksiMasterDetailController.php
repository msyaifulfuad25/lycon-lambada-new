<?php

namespace App\Http\Controllers;

use App\Http\Controllers\lam_system\FrontPageController;
use App\Http\Controllers\lam_error\ErrorController;
use App\Http\Controllers\lam_custom\ErrorController as ErrorControllerFunc;
use App\Http\Controllers\lam_system\ErrorController as ErrorControllerFunc2;
Use DB;
use App\User;
use App\Menu;
use App\Page;
use App\Panel;
use App\Widget;
use App\WidgetGrid;
use App\WidgetGridField;
use App\Departement;
use App\Company;
use App\ViewGridField;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Schema;
use Carbon;

class TransaksiMasterDetailController extends Controller {

    public static function index($id) {
        //testing gaes
        if (!Session::get('login')) {
            return view('pages.login');
        }
    
        $code = $id;
        $page = Page::where('noid',$id)->first();
        $breadcrumb = Menu::getBreadcrumb($id);
        $idhomepagelink = $id;
        $user = User::where('noid', Session::get('noid'))->first();
        $idhomepage =  $user->idhomepage;
        $noid = Session::get('noid');
        $myusername = Session::get('myusername');
        $amtn = DB::connection('mysql2')->table('appmtypenotification')->get();
        $resultan = DB::connection('mysql2')->table('appnotification')->get();
        
        $an = [];
        foreach ($resultan as $k => $v) {
            if (@$an[$v->idtypenotification] == null) {
                $an[$v->idtypenotification] = [];
            }
            array_push($an[$v->idtypenotification], $v);
        }
        
        if ($page == null) {
            $page = Page::where('noid', 2)->first();
            $page_title = $page->pagetitle;
            $page_description = $page->pagetitle;
          
            return view('lam_custom.notfound', compact(
                'noid',
                'myusername',
                'idhomepage',
                'idhomepagelink',
                'page',
                'page_title',
                'page_description'
            ));
        } else {
            $panel = Panel::where('idpage',$page->noid)->orderBy('idwidget', 'ASC')->first();
            $page_title = $page->pagetitle;
            $page_description = $page->pagetitle;
            $widgetgridfield = WidgetGridField::where('idcmswidgetgrid', $panel->idwidget)->where('colshow', 1)->get();
            $heightwgf = 35;
            foreach ($widgetgridfield as $k => $v) {
                if ($v->colheight > $heightwgf) {
                    $heightwgf = $v->colheight;
                }
            }

            $resultallnoid = Widget::where('kode', $page->noid)->select('noid', 'panelcolor')->get();
            $hwgf = [];
            $pc = [];
            $pn = $panel->noid;
            foreach ($resultallnoid as $k => $v) {
                $pc[$v->noid] = $v->panelcolor;
                $resulthwgf = WidgetGridField::where('idcmswidgetgrid', $v->noid)->where('colshow', 1)->get();
                $hwgf[$v->noid] = 35;
                foreach ($resulthwgf as $k2 => $v2) {
                    if ($v2->colheight > $hwgf[$v->noid]) {
                        $hwgf[$v->noid] = $v2->colheight;
                    }
                }
            }
            $hwgf = json_encode($hwgf);
            // dd($pc[$pn]);
        }


        return view('lam_transaksi_master_detail.index', compact(
            'code',
            'noid',
            'myusername',
            'idhomepagelink',
            'page',
            'page_title',
            'idhomepage',
            'page_description',
            'breadcrumb',
            'heightwgf',
            'hwgf',
            'pc',
            'pn',
            'amtn',
            'an'
        ));


    }

    public function get_data_tmd(Request $request) {
        $code = $request->get('table');
        $menu = Menu::where('noid',$code)->first();
        $page = Page::where('noid',$menu->linkidpage)->first();
        $panel = Panel::where('idpage',$page->noid)->orderBy('noid', 'asc')->get();
        
        $panelidwidget = [];
        foreach ($panel as $k => $v) {
            $panelidwidget[] = $v->idwidget;
        }
        $widget = Widget::whereIn('noid', $panelidwidget)->orderBy('noid', 'asc')->get();
        
        $widgetnoid = [];
        foreach ($widget as $k => $v) {
            $widgetnoid[] = $v->noid;
        }
        $widgetgrid = WidgetGrid::whereIn('idcmswidget', $widgetnoid)->orderBy('noid', 'asc')->get();

        $widgetgridfield = [];
        foreach ($widgetgrid as $k => $v) {
            $widgetgridfield[] = WidgetGridField::where('idcmswidgetgrid', $v->noid)->where('colshow',1)->orderBy('nourut','asc')->orderBy('noid','asc')->get();
        }
        
        $widgetgridfield2 = [];
        foreach ($widgetgrid as $k => $v) {
            $widgetgridfield2[] = WidgetGridField::where('idcmswidgetgrid', $v->noid)->orderBy('nourut','asc')->orderBy('noid','asc')->get();
        }

        $widgetgridfield3 = [];
        foreach ($widgetgrid as $k => $v) {
            $widgetgridfield3[] = WidgetGridField::where('idcmswidgetgrid', $v->noid)->where('newshow',1)->orderBy('nourut','asc')->orderBy('noid','asc')->get();
        }

        $widgetgridfield4 = [];
        foreach ($widgetgrid as $k => $v) {
            $widgetgridfield4[] = WidgetGridField::where('idcmswidgetgrid', $v->noid)->where('editshow',1)->orderBy('nourut','asc')->orderBy('noid','asc')->get();
        }
        
        $response = [
            'menu' => $menu,
            'page' => $page,
            'panel' => $panel,
            'widget' => $widget,
            'widgetgrid' => $widgetgrid,
            'widgetgridfield' => $widgetgridfield,
            'widgetgridfield2' => $widgetgridfield2,
            'widgetgridfield3' => $widgetgridfield3,
            'widgetgridfield4' => $widgetgridfield4,
        ];
        echo json_encode($response);
    }
    
    public function get_tmd(Request $request) {
        $cek = '';
        $maincms = json_decode($request->get('maincms'));
        $kode = $request->get('table');

        $menu = $maincms->menu;
        $page = $maincms->page;
        $panel = $maincms->panel;
        $widget = $maincms->widget[$request->get('key')];
        $filtertable = json_decode($widget->configwidget);
        $fieldtabel = $filtertable->WidgetFilter;
        $table = $widget->maintable;
        $widgetgrid = $maincms->widgetgrid[0];
        // $widgetgridfield = $maincms->widgetgridfield[0];
        $widgetgridfield = array_values(array_filter($maincms->widgetgridfield[0], function($v, $k) {
            if ($v->colshow == 1) {
                return $v;
            }
        }, ARRAY_FILTER_USE_BOTH));
        $widgetgridfield_wherecolsearch = array_values(array_filter($widgetgridfield, function($v, $k) {
            if ($v->colsearch == 1) {
                return $v;
            }
        }, ARRAY_FILTER_USE_BOTH));
        // $cek = $widgetgridfield_wherecolsearch;

        $data['buttonfilter'] = [];
        
        // $cek = json_decode($widget->configwidget);
        foreach ($fieldtabel as $key => $value) {
            $data['buttonfilter'][$key] = [
                'groupcaption' => $value->groupcaption,
                'groupcaptionshow' => $value->groupcaptionshow,
                'groupdropdown' => $value->groupdropdown,
                'data' => []
            ];
            foreach ($value->button as $key2 => $value2) {
                $pecah = explode('.', $value2->condition[0]->querywhere[0]->fieldname);
                $oncl = 'onclick="filterData(\'btn-filter-'.$value2->buttoncaption.'\', \'btn-filter-'.$key.'\', '.$key.', \''.$value2->buttoncaption.'\', \''.$value2->classcolor.'\', '.$value->groupdropdown.', false, \''.$table.'\', \''.$value2->condition[0]->querywhere[0]->operand.'\', \''.$pecah[1].'\', \''.$value2->condition[0]->querywhere[0]->operator.'\', \''.$value2->condition[0]->querywhere[0]->value.'\')" ';
                $data['buttonfilter'][$key]['data'][] = array(
                    'nama' => $value2->buttoncaption,
                    'buttonactive' => $value2->buttonactive,
                    'taga' => '<button class="btn btn-outline-secondary btn-sm flat btn-filter-'.$key.'" id="btn-filter-'.$value2->buttoncaption.'" style="background-color:#E3F2FD;" '.$oncl.'>'.$value2->buttoncaption.' </button>',
                    'operand' => $value2->condition[0]->querywhere[0]->operand,
                    'fieldname' => $pecah[1],
                    'operator' => $value2->condition[0]->querywhere[0]->operator,
                    'value' => $value2->condition[0]->querywhere[0]->value,
                );
            }
        }

        $data['columns'] = array('<input type="checkbox" onchange="changeCheckbox()" class="checkbox checkbox'.$key.'" name="checkbox[]" value="FALSE">','No');
        $data['columns2'] = array();
        $data['width'] = array();
        $data['colheaderalign'] = array();
        $data['coltypefield'] = array();
        $data['colgroupname'] = array();
        $data['colheaderalign'] = array();
        $data['coldefault'] = array();
        $data['colsearch'] = array();

        // for ($i=0; $i <count($widgetgridfield) ; $i++) {
        //     $data['columns2'][] =  $widgetgridfield[$i]->fieldname;
        // }

        foreach ($widgetgridfield_wherecolsearch as $k => $v) {
            $data['columns2'][] =  $widgetgridfield[$k]->fieldname;
        }

        for ($i=0; $i <count($widgetgridfield) ; $i++) {
            array_push($data['columns'], $widgetgridfield[$i]->fieldcaption);
            array_push($data['width'], $widgetgridfield[$i]->colwidth.'%');
            array_push($data['colheaderalign'], $widgetgridfield[$i]->colheaderalign);
            array_push($data['coltypefield'], $widgetgridfield[$i]->coltypefield);
            array_push($data['colgroupname'], $widgetgridfield[$i]->colgroupname);
            array_push($data['coldefault'], $widgetgridfield[$i]->coldefault);
            array_push($data['colsearch'], $widgetgridfield[$i]->colsearch);
        }

        if (!empty($data['colgroupname'])) {
            $data['header_atas'] = '<tr class="atas" role="group-header">';
            $data['header_atas'] .= '<th style="text-align:center" colspan="2">#</th>';
            $data['header_atas'] .= '<th style="text-align:center" colspan="'.count($widgetgridfield).'">---</th>';
            $data['header_atas'] .= '<th colspan="1" style="text-align:center">---</th>';
            $data['header_atas'] .= '</tr>';
        } else {
            $datahead = array_count_values($data['colgroupname']);
            $data['header_atas'] = '<tr class="footter">';
            $data['header_atas'] .= '<th style="text-align:center" colspan="2">#</th>';
            foreach ($datahead as $key => $value) {
                $data['header_atas'] .= '<th style="text-align:center" colspan="'.$value.'">'.$key.'</th>';
            }
            $data['header_atas'] .= '<th colspan="1" style="text-align:center">---</th>';
            $data['header_atas'] .= '</tr>';
        }
        array_push($data['columns'],'Action');
        $columns =$data['columns'];
        $limit = $request->input('length');
        $start = $request->input('start');
        // $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        // dd($start);
        // dd($request->input());

        $columnIndex_arr = $request->get('order');
        // dd($columnIndex_arr);
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value
        // dd($columnName);
        
        //--------------------------------------- MAKE QUERY
        $select = '';
        $from = '';
        $join = '';
        $where = '1=1 ';
        $limit = $limit;
        $offset = $start;
        $order_by = 'ORDER BY ';
        $uniquetable = [];
        foreach ($widgetgridfield as $k => $v) {
            if ($k == 0) {
                $select .= "$v->tablename.noid AS noid,";
            }
            $tablelookup = explode(';', $v->tablelookup);
            if ($v->tablelookup) {
                $select .= "$tablelookup[3].$tablelookup[2] AS $tablelookup[3]_$tablelookup[2],";
                if ($tablelookup[3] != $v->tablename) {
                    if (in_array($tablelookup[3], $uniquetable)) {
                        $join .= "LEFT JOIN $tablelookup[3] AS $tablelookup[3]$k ON $v->tablename.$tablelookup[0] = $tablelookup[3]$k.$tablelookup[1] ";
                    } else {
                        $join .= "LEFT JOIN $tablelookup[3] ON $v->tablename.$tablelookup[0] = $tablelookup[3].$tablelookup[1] "; 
                    }
                    $uniquetable[] = $tablelookup[3];
                }
                if ($request->get('order')[0]['column'] != 0 && $k == $request->get('order')[0]['column']-2) {
                    $order_by .= "$tablelookup[3]_$tablelookup[2] ".$request->get('order')[0]['dir'];
                }
            } else {
                $select .= "$v->tablename.$v->fieldsource AS $v->tablename"."_"."$v->fieldsource,";
                if ($request->get('order')[0]['column'] != 0 && $k == $request->get('order')[0]['column']-2) {
                    $order_by .= $v->tablename."_"."$v->fieldsource ".$request->get('order')[0]['dir'];
                }
            }
            $from = $v->tablename;
        }
        // print_r($join);die;
        $select = substr($select, 0, -1);
        if ($order_by == 'ORDER BY ') {
            $order_by = '';
        }
        if (@$_POST['selected_filter']) {
            for ($i=0; $i<count($_POST['selected_filter']['operand']); $i++) {
                if ($_POST['selected_filter']['operand'][$i] != '') {
                    $where .= $_POST['selected_filter']['operand'][$i].' '.$_POST['selected_filter']['fieldname'][$i].' '.$_POST['selected_filter']['operator'][$i].' "'.$_POST['selected_filter']['value'][$i].'" ';
                }
            }
        }
        // print_r($where);return false;
        $like = '';
        if (!empty($request->input('search.value'))) {
            $like .= 'AND ';
            foreach (explode(',', $select) as $k => $v) {
                $fieldsource = explode(' ', $v);
                // if ($k == 0) {
                    $like .= $fieldsource[0]." LIKE '%".$request->input('search.value')."%' OR ";
                // } else {
                //     $like .= $fieldsource[2]." LIKE '%".$request->input('search.value')."%' OR ";
                // }
            }
            $like = substr($like, 0, -3);
        }
        
        $query = "SELECT $select FROM $from $join WHERE $where $like $order_by LIMIT $limit OFFSET $offset";
        $query_total = "SELECT $select FROM $from $join WHERE $where $like $order_by";
        
        $result = DB::connection('mysql2')->select($query);
        $result_total = count(DB::connection('mysql2')->select($query_total));
        // $cek = $result;

        
        $data['footter'] = '<tr>';
        $data['footter'] .= '<th style="text-align:right" colspan="4">'.count($result).'</th>';
        $data['footter'] .= '<th style="text-align:right" colspan="8"></th>';
        $data['footter'] .= '</tr>';
              // dd($start);
        // }

        if ($start == 0) {
            $no = 1;
        } else {
            $no = $start+1;
        }
        // dd($sql);
        $data['data'] = array();
        $noids = array();
        foreach ($result as $key => $value) {
            // dd($value);
            $row = array();
            if ($widgetgrid->colcheckbox) {
                array_push($row,'<input type="checkbox" onchange="changeCheckbox('.$key.', '.$value->noid.')" class="checkbox checkbox'.$key.'" name="checkbox[]" value="FALSE">');
            } else {
                // array_push($row,'');
            }
            // array_push($row,'Check');
            array_push($row,$no++);
            // array_push($row,$value->noid);
            // dd($widgetgridfield[$key]->coltypefield);
            // $string = (string)$widgetgridfield[$key]->fieldname;
            //     array_push($row,$value->$string);

            for ($i=0; $i <count($widgetgridfield) ; $i++) {
                $tablename = $widgetgridfield[$i]->tablename;
                $valuecolumn = '';
                $tablelookup = explode(';', $widgetgridfield[$i]->tablelookup);
                if ($widgetgridfield[$i]->tablelookup) {
                    $fs = $tablelookup[3].'_'.$tablelookup[2];
                    $valuecolumn = $value->$fs;
                } else {
                    $fs = $tablename.'_'.$widgetgridfield[$i]->fieldsource;
                    $valuecolumn = $value->$fs;
                }

                if ($widgetgridfield[$i]->coltypefield == 4) {
                    // dd($widgetgridfield[$i]->coltypefield);
                    // $string = (string)$widgetgridfield[$i]->fieldname;
                    // $isiketrangan = \Illuminate\Support\Str::limit($value->$string, 100, $end='...');
                    // array_push($row, '<div class="text-'.$widgetgridfield[$i]->colalign.'">'.$valuecolumn.'</div>');
                    $html = htmlentities(substr($valuecolumn, 0, $widgetgridfield[$i]->maxlength));
                    $buttonmore = $html == '' ? '' : '<a class="btn_more label-sm label-success" style="color: white; padding:3px !important;margin-left:-40px;" onclick="showModalCKEditor()">More</a>';
                    $valuecolumn = '<div class="context" style="width:300px;text-overflow:ellipsis;float:left;overflow:hidden;margin-right:45px;">'.$html.'</div>'.$buttonmore;
                    array_push($row, $valuecolumn);
                } else if ($widgetgridfield[$i]->coltypefield == 11) {
                    $fs = $tablename.'_'.$widgetgridfield[$i]->fieldsource;
                    $valuecolumn = date($widgetgridfield[$i]->colformat, strtotime($value->$fs));
                    // array_push($row, '<div class="text-'.$widgetgridfield[$i]->colalign.'">'.$valuecolumn.'</div>');
                    array_push($row, $valuecolumn);
                } else if ($widgetgridfield[$i]->coltypefield == 22) {
                    array_push($row, number_format($valuecolumn,0,",","."));
                } else if ($widgetgridfield[$i]->coltypefield == 31) {
                    // $string = (string)$widgetgridfield[$i]->fieldname;
                    // $check = '<div class="text-'.$widgetgridfield[$i]->colalign.'">
                    //             <input type="checkbox" checked id="'.$widgetgridfield[$i]->fieldname.'_'.$valuecolumn.'" name="'.$widgetgridfield[$i]->fieldname.'_'.$valuecolumn.'" value="'.$valuecolumn.'">';
                    //         '</div>';
                    $checked = $valuecolumn == '1' ? 'checked' : '';
                    $check = '<input type="checkbox" '.$checked.' id="'.$widgetgridfield[$i]->fieldname.'_'.$valuecolumn.'" name="'.$widgetgridfield[$i]->fieldname.'_'.$valuecolumn.'" value="'.$valuecolumn.'">';
                    array_push($row,$check);
                } else if ($widgetgridfield[$i]->coltypefield == 96) {
                    // $string2 = (string)$widgetgridfield[$i]->fieldname;
                    // array_push($row, '<div class="text-'.$widgetgridfield[$i]->colalign.'">'.$valuecolumn.'</div>');
                    array_push($row, $valuecolumn);
                    // array_push($row, '<div class="text-'.$widgetgridfield[$i]->colalign.'">'.$value->$string2.'</div>');
                    // }else if ($widgetgridfield[$i]->coltypefield == 11) {
                    //     $string = (string)$widgetgridfield[$i]->fieldname;
                    //   // dd($string);
                    //     array_push($row,$value->idcompany_nama);
                } else {
                    // array_push($row, '<div class="text-'.$widgetgridfield[$i]->colalign.'">'.$valuecolumn.'</div>');
                    array_push($row, $valuecolumn);
                }
            }
            $urlprint = base64_encode('print/'.$value->noid.'/'.date('Ymdhis'));
            $buttons = '<div class="d-flex align-items-center button-action button-action-'.$key.'">';
            if ($widgetgrid->actprint) {
                $buttons .= '<a onclick="print('.$maincms->widget[0]->kode.', \''.$urlprint.'\')" class="btn btn-info btn-sm flat mr-2"><i class="fa fa-file icon-xs"></i> </a>';
            }
            if ($widgetgrid->actedit) {
                $buttons .= '<a onclick="edit('.$value->noid.')" class="btn btn-warning btn-sm flat mr-2"><i class="fa fa-edit icon-xs" style="text-align:center"></i> </a>';
            }
            if ($widgetgrid->actview) {
                $buttons .= '<a onclick="view('.$value->noid.')"  class="btn btn-success btn-sm flat mr-2"><i class="fa fa-search icon-xs"></i> </a>';
            }
            if ($widgetgrid->actdelete) {
                $buttons .= '<a onclick="remove('.$value->noid.')" class="btn btn-danger btn-sm flat mr-2"><i class="fa fa-trash icon-xs"></i> </a>';
            }
            $buttons .= '</div>';
            array_push($row,$buttons);
            $data['data'][] = $row;
            $noids[] = $value->noid;
        }
        // print_r($data);
        // return false;
        //--------------------------------------- END MAKE QUERY

                            // $TEXT_QUERY = '';
                            // $TEXT_QUERY2 = '';
                            // $TEXT_QUERY3 = '';
                            // $TEXT_QUERY_order = '';
                            // $TEXT_order = '';
                            // $BY_NYA = '';
                            // $SELECT_QUERY = "SELECT a.noid, ";
                            // $SELECT_QUERY3 = "SELECT ";
                            // $SELECT_QUERY2 = " ";
                            // $WHERE_QUERY = "FROM ".$table." a";
                            // $WHERE_QUERY4 = "FROM ".$table." a";
                            // $WHERE_QUERY3 = " ";
                            // $Join = " ";
                            // $TABEL_ALIAS = "a";

                            // // $cek = $widgetgridfield;
                            // foreach ($widgetgridfield as $key => $value) {
                            //     // dd($value->fieldvalidation);
                            //     if ($widgetgridfield[$key]->coltypefield == 96) {
                            //         $TABEL_ALIAS = chr(ord($TABEL_ALIAS) + 1);
                            //         $koma = ( $key !== count( $widgetgridfield ) -1 ) ? "," : " ";
                            //         $SELECT_QUERY .= "a.noid, a.".$value->fieldsource." AS ".$value->fieldsource.', ';
                            //         // $TEXT_QUERY_order .= $value->fieldname.' ,';
                            //         $isirelasi = str_replace(";"," ",$widgetgridfield[$key]->tablelookup);
                            //         // dd($widgetgridfield[$key]->tablelookup);
                            //         $ex = explode(' ', $isirelasi);
                            //         if ( strpos($ex[2], ",") !== false ) {
                            //             $ex2 = explode(',', $ex[2]);
                            //             ${'FIELD_FOREIGN'.$key} =  $TABEL_ALIAS.'.'.$ex2[0].' , '.$TABEL_ALIAS.'.'.$ex2[1];
                            //         } else {
                            //             ${'FIELD_FOREIGN'.$key} = $TABEL_ALIAS.'.'.$ex[2];
                            //         }
                            //         ${'FIELD_NAME'.$key} = $ex[0];
                            //         ${'FIELD_RELASI'.$key} = $ex[1];
                            //         ${'TABLE_RELASI'.$key} = $ex[3];
                            //         $SELECT_QUERY .= ' CONCAT('.${'FIELD_FOREIGN'.$key}.') as '.  ${'FIELD_NAME'.$key}.'_nama '.$koma;
                            //         $TEXT_QUERY_order .=  ${'FIELD_NAME'.$key}.'_nama '.$koma;
                            //         $WHERE_QUERY .= " LEFT JOIN ".${'TABLE_RELASI'.$key}." ".$TABEL_ALIAS." ON a.".  ${'FIELD_NAME'.$key}."=".$TABEL_ALIAS.".".${'FIELD_RELASI'.$key};
                            //     } else {
                            //         $koma = ( $key !== count( $widgetgridfield ) -1 ) ? "," : " ";
                            //         $SELECT_QUERY2 .= "a.".$value->fieldsource.$koma;
                            //         $SELECT_QUERY .= "a.".$value->fieldsource.$koma;
                            //         $TEXT_QUERY_order .= "a.".$value->fieldsource.$koma;
                            //     }
                            // }

                            // // dd($fieldvalidations[ (int) $columnName ]);

                            // $BY_NYA = 'limit '.$limit.' offset '.$start.'  ';
                            // // $BY_NYA2 = ' order by a.noid '.$dir.' ';
                            // $BY_NYA2 = '  ';
                            // $TEXT_QUERY .= $SELECT_QUERY;
                            // $TEXT_QUERY .= $WHERE_QUERY.' where a.noid != 0 ';
                            // $WHERE_QUERY3 .= 'where a.noid != 0 ';

                            // $TEXT_QUERY3 .= $SELECT_QUERY2;
                            // $TEXT_QUERY2 .= $SELECT_QUERY;
                            // $TEXT_QUERY2 .= $WHERE_QUERY.' where a.noid != 0  ';
                            // // if (@$request->get('tab_noid')) {
                            // //     $TEXT_QUERY .= 'and idmaster = '.$request->get('tab_noid').' ';
                            // //     $TEXT_QUERY2 .= 'and idmaster = '.$request->get('tab_noid'.' ');
                            // // }
                            // // dd($TEXT_QUERY);

                            // if (@$_POST['selected_filter']) {
                            //     for ($i=0; $i<count($_POST['selected_filter']['operand']); $i++) {
                            //         if ($_POST['selected_filter']['operand'][$i] != '') {
                            //             $TEXT_QUERY .= $_POST['selected_filter']['operand'][$i].' '.$_POST['selected_filter']['fieldname'][$i].' '.$_POST['selected_filter']['operator'][$i].' "'.$_POST['selected_filter']['value'][$i].'" ';
                            //         }
                            //     }
                            // }

                            // // $cek = $TEXT_QUERY;
                            // if (empty($request->input('search.value'))) {
                            //     if ($columnName == 0) {
                            //         $TEXT_QUERY .=  $BY_NYA;
                            //     } else {
                            //         $str = ltrim($TEXT_QUERY_order, "SELECT");
                            //         // dd($str);
                            //         $fieldvalidations = explode(',',$str);
                            //         $TEXT_order = ' order by '.$fieldvalidations[ (int) $columnName-2 ].'  '.$columnSortOrder.'  ';
                            //         // $cek = $fieldvalidations;
                            //         $TEXT_QUERY .=  $TEXT_order;
                            //         $TEXT_QUERY .=  $BY_NYA;
                            //     }
                                
                            //     $sql =  DB::connection('mysql2')->select($TEXT_QUERY);
                            //     $sql_total =  DB::connection('mysql2')->select($TEXT_QUERY2);
                            //     $totalsql = count($sql_total);
                            //     // dd($TEXT_QUERY);
                            // } else {
                            //     // dd($request->order);
                            //     $orderby = $request->order;
                            //     // dd($orderby);
                            //     $search = '%'.$request->input('search.value').'%';
                            //     $querylikes = array();
                            //     $str = ltrim($TEXT_QUERY3, "SELECT");
                            //     $fieldvalidations = explode(',',$str);
                            //     // dd($TEXT_QUERY2);
                            //     $where_baru = ' ';
                            //     $ref = 0;
                            //     foreach ($fieldvalidations as $key => $value) {
                            //         // if ($value != '') {
                            //             $koma = ( $key !== count( $fieldvalidations ) -1 ) ? " OR " : " ";
                            //             $where_baru .= $value.' LIKE "'.$search .'" '. $koma;
                            //         // }
                            //     }
                            //     $TEXT_QUERY .= '  AND'.$where_baru;
                            //     $TEXT_QUERY .=$BY_NYA2;
                            //     // dd($TEXT_QUERY);

                            //     $sql =  DB::connection('mysql2')->select($TEXT_QUERY);
                            //     // dd($sql);
                            //     $totalsql = count($sql);
                            // }
                            // dd($request->order);
                            // dd($sql);

                            // $sql = $result;
                            // $totalsql = count($result);

                            // $data['footter'] = '<tr>';
                            // $data['footter'] .= '<th style="text-align:right" colspan="4">'.$totalsql.'</th>';
                            // $data['footter'] .= '<th style="text-align:right" colspan="8"></th>';
                            // $data['footter'] .= '</tr>';
                            //       // dd($start);
                            // // }

                            // if ($start == 0) {
                            //     $no = 1;
                            // } else {
                            //     $no = $start+1;
                            // }
                            // // dd($sql);
                            // $data['data'] = array();
                            // foreach ($sql as $key => $value) {
                            //     // dd($value);
                            //     $row = array();
                            //     array_push($row,'<input type="checkbox" onchange="changeCheckbox('.$key.', '.$value->noid.')" class="checkbox checkbox'.$key.'" name="checkbox[]" value="FALSE">');
                            //     array_push($row,$no++);
                            //     // array_push($row,$value->noid);
                            //     // dd($widgetgridfield[$key]->coltypefield);
                            //     // $string = (string)$widgetgridfield[$key]->fieldname;
                            //     //     array_push($row,$value->$string);

                            //     for ($i=0; $i <count($widgetgridfield) ; $i++) {
                            //         if ($widgetgridfield[$i]->coltypefield == 4) {
                            //             // dd($widgetgridfield[$i]->coltypefield);
                            //             $string = (string)$widgetgridfield[$i]->fieldname;
                            //             $isiketrangan = \Illuminate\Support\Str::limit($value->$string, 100, $end='...');
                            //             array_push($row, '<div class="text-'.$widgetgridfield[$i]->colalign.'">'.$isiketrangan.'</div>');
                            //         } else if ($widgetgridfield[$i]->coltypefield == 96) {
                            //             $string = (string)$widgetgridfield[$i]->fieldname.'_nama';
                            //             // $string2 = (string)$widgetgridfield[$i]->fieldname;
                            //             array_push($row, '<div class="text-'.$widgetgridfield[$i]->colalign.'">'.$value->$string.'</div>');
                            //             // array_push($row, '<div class="text-'.$widgetgridfield[$i]->colalign.'">'.$value->$string2.'</div>');
                            //             // }else if ($widgetgridfield[$i]->coltypefield == 11) {
                            //             //     $string = (string)$widgetgridfield[$i]->fieldname;
                            //             //   // dd($string);
                            //             //     array_push($row,$value->idcompany_nama);
                            //         } else if ($widgetgridfield[$i]->coltypefield == 31) {
                            //             $string = (string)$widgetgridfield[$i]->fieldname;
                            //             $check = '<div class="text-'.$widgetgridfield[$i]->colalign.'">
                            //                         <input type="checkbox" checked id="'.$widgetgridfield[$i]->fieldname.'_'.$value->$string.'" name="'.$widgetgridfield[$i]->fieldname.'_'.$value->$string.'" value="'.$value->$string.'">';
                            //                     '</div>';
                            //             array_push($row,$check);
                            //         } else {
                            //             $string = (string)$widgetgridfield[$i]->fieldname;
                            //             array_push($row, '<div class="text-'.$widgetgridfield[$i]->colalign.'">'.$value->$string.'</div>');
                            //         }
                            //     }
                            //     array_push($row,'<div class="d-flex align-items-center button-action button-action-'.$key.'">
                            //     <a onclick="edit('.$value->noid.')" class="btn btn-warning btn-sm flat mr-2"><i class="fa fa-edit icon-xs" style="text-align:center"></i> </a>
                            //     <a onclick="view('.$value->noid.')"  class="btn btn-success btn-sm flat mr-2"><i class="fa fa-search icon-xs"></i> </a>
                            //     <a  onclick="remove('.$value->noid.')" class="btn btn-danger btn-sm flat mr-2"><i class="fa fa-trash icon-xs"></i> </a>
                            //     </div>');
                            //     $data['data'][] = $row;
                            // }

                            // array(
                            //     "draw"            => intval( $request['draw'] ),
                            //     "recordsTotal"    => intval( $recordsTotal ),
                            //     "recordsFiltered" => intval( $recordsFiltered ),
                            //     "data"            => $my_data
                            // );
        $json_data = array(
            "draw" => intval($request['draw']),
            "recordsTotal" => $result_total,
            "recordsFiltered" => $result_total,
            "data" => $data['data'],
            "noids" => $noids
            // "cms" => [
            //     "nospan" => $panel[0]->nospan
            // ]
            // "cek" => $widgetgrid->colcheckbox,
            // "widgetgridfield" => $widgetgridfield,
            // "widgetgridfield_wherecolsearch" => $widgetgridfield_wherecolsearch,
        );

        header("Content-type: application/json");
        header("Cache-Control: no-cache, must-revalidate");
        echo json_encode($json_data);
    }

    public function get_filter_button_tmd(Request $request) {
        $kode = $request->get('table');
        $maincms = $request->get('maincms');
        // dd($kode);
        $menu = $maincms['menu'];

        $page = $maincms['page'];
        $panel = $maincms['panel'];
        $widget = $maincms['widget'];
        $table = $widget['maintable'];
        $filtertable = json_decode($widget['configwidget']);
        $fieldtabel = $filtertable->WidgetFilter;
        
        $data['buttonfilter'] = [];
        $data['buttonactive'] = [];

        foreach ($fieldtabel as $k => $v) {
            $data['buttonactive'][$k] = ''; 
            $data['buttonfilter'][$k] = [
                'groupcaption' => $v->groupcaption,
                'groupcaptionshow' => $v->groupcaptionshow,
                'groupdropdown' => $v->groupdropdown,
                'data' => []
            ];
            foreach ($v->button as $k2 => $v2) {
                $icon = '';
                if ($v2->classicon != '') {
                    $icon = '<i class="fa '.$v2->classicon.'"></i> ';
                }
                if ($v2->buttonactive) {
                    $data['buttonactive'][$k] = $v2->buttoncaption; 
                }
                $pecah = explode('.', $v2->condition[0]->querywhere[0]->fieldname);
                $oncl = 'onclick="filterData(\'btn-filter-'.$v2->buttoncaption.'\', \'btn-filter-'.$k.'\', '.$k.', \''.$v2->buttoncaption.'\', \''.$v2->classcolor.'\', '.$v->groupdropdown.', false, \''.$table.'\', \''.$v2->condition[0]->querywhere[0]->operand.'\', \''.$pecah[1].'\', \''.$v2->condition[0]->querywhere[0]->operator.'\', \''.$v2->condition[0]->querywhere[0]->value.'\')" ';
                $button = '<button class="btn btn-outline-secondary btn-sm flat btn-filter-'.$k.'" id="btn-filter-'.$v2->buttoncaption.'" style="background-color:#E3F2FD; color: '.$v2->classcolor.'"'.$oncl.'>'.$icon.$v2->buttoncaption.' </button>';
                if ($v->groupdropdown == 1) {
                    $button = '<button class="flat dropdown-item" id="btn-filter-'.$v2->buttoncaption.'" '.$oncl.'>'.$icon.$v2->buttoncaption.' </button>';
                }
                $data['buttonfilter'][$k]['data'][] = array(
                    'nama' => $v2->buttoncaption,
                    'buttonactive' => $v2->buttonactive,
                    'taga' => $button,
                    'operand' => $v2->condition[0]->querywhere[0]->operand,
                    'fieldname' => $pecah[1],
                    'operator' => $v2->condition[0]->querywhere[0]->operator,
                    'value' => $v2->condition[0]->querywhere[0]->value,
                );
            }
        }

        
        $json_data = array(
            "filterButton" => $data['buttonfilter'],
            // "activeButton" => $data['buttonactive'],
            "configWidget" => $filtertable,
        );

        header("Content-type: application/json");
        header("Cache-Control: no-cache, must-revalidate");
        echo json_encode($json_data);
    }
    
    public function get_form_tmd(Request $request) {
        $cek = '';
        $maincms = json_decode($request->get('maincms'));
        $code = $request->get('table');
        $widget = $maincms->widget;
        $tablemaster = $maincms->widget[0]->maintable;
        $key = $request->get('key');
        $widget_noid = $request->get('widget_noid');

        $table = $widget[$key]->maintable;
        $widgetgrid = $maincms->widgetgrid;

        $maincms_widgetgridfield = [];
        foreach ($maincms->widgetgridfield as $k => $v) {
            foreach ($v as $k2 => $v2) {
                $maincms_widgetgridfield[$v2->idcmswidgetgrid][] = $v2;
            }
        }

        if ($request->get('isadd')) {
            $widgetgridfield = array_values(array_filter($maincms_widgetgridfield[$widget_noid], function($v, $k) {
                if ($v->newshow == 1) {
                    return $v;
                }
            }, ARRAY_FILTER_USE_BOTH));;
        } else {
            $widgetgridfield = array_values(array_filter($maincms_widgetgridfield[$widget_noid], function($v, $k) {
                if ($v->editshow == 1) {
                    return $v;
                }
            }, ARRAY_FILTER_USE_BOTH));;
        }
        // $widgetgridfield = $maincms->widgetgridfield[0];
        // $widgetgridfield = WidgetGridField::where('idcmswidgetgrid', $widgetgrid->noid)->where('newshow',1)->orderBy('formurut','asc')->get();
        $forminput = '';

        foreach ($widgetgridfield as $k => $v) {
            if ($key == 0) {
                $inputname = $v->fieldname;
            } else {
                $inputname = $v->tablename.'_'.$v->fieldname;
            }

            if ($v->tablelookup) {
                $tabledetail = explode(';', $v->tablelookup)[3];
            } else {
                $tabledetail = '';
            }
            
            if ($request->get('isadd') == 'true') {
                if ($v->newreq == 1) {
                    $required = 'required';
                    $formcaption = $v->formcaption.' <span class="text-danger">*</span>';
                } else {
                    $required = '';
                    $formcaption = $v->formcaption;
                }
    
                if ($v->newreadonly == 1) {
                    $disabled = 'readonly';
                    $cssreadonly = 'background-color: #E5E5E5 !important;';
                } else {
                    $disabled = '';
                    $cssreadonly = '';
                }
            } else {
                if ($v->editreq == 1) {
                    $required = 'required';
                    $formcaption = $v->formcaption.' <span class="text-danger">*</span>';
                } else {
                    $required = '';
                    $formcaption = $v->formcaption;
                }
    
                if ($v->editreadonly == 1) {
                    $disabled = 'readonly';
                    $cssreadonly = 'background-color: #E5E5E5 !important;';
                } else {
                    $disabled = '';
                    $cssreadonly = '';
                }
            }

            if ($v->formcaptionpos == 'TOP') {
                $row = '';
                $endrow = '';
                $collabel = '';
                $colinput = '';
            } elseif ($v->formcaptionpos == 'LEFT') {
                $row = '<div class="row">';
                $endrow = '</div>';
                $collabel = 'col-md-3';
                $colinput = 'col-md-9';
            }

            if ($v->formalign == 'LEFT') {
                $formalign = 'text-left';
            } elseif ($v->formalign == 'CENTER') {
                $formalign = 'text-center';
            } elseif ($v->formalign == 'RIGHT') {
                $formalign = 'text-right';
            } else {
                $formalign = '';
            }

            if($v->maxlength != 0) {
                $maxlength = 'maxlength="'.$v->maxlength.'"';
            } else {
                $maxlength = '';
            }

            if ($v->formdefault != null) {
                $formdefault = explode('/', $v->formdefault);
                if ($formdefault[0] == 'getdate') {
                    if ($formdefault[1] == 'DATE') {
                        $formdefault = date('d-M-Y');
                    } else if ($formdefault[1] == 'DATETIME') {
                        $formdefault = date('d-M-Y h:i');
                    }
                } else if ($formdefault[0] == 'generatecode') {
                    $result_appmkode = DB::connection('mysql2')->table('appmkode')->where('noid', $formdefault[1])->first();
                    $result_appmkodedetail = DB::connection('mysql2')->table('appmkodedetail')->where('idkode', $formdefault[1])->orderBy('nourut', 'ASC')->get();

                    $formdefault = $result_appmkode->kode;
                    foreach ($result_appmkodedetail as $k2 => $v2) {
                        if ($v2->kodename == 'KODE') {
                            $formdefault .= $v2->kodevalue;
                        } else if ($v2->kodename == 'NOURUT') {
                            $fungsigroupby = explode(';', $v2->fungsigroupby);
                            // $query_nourut = "SELECT MAX(noid) AS noid FROM $tablemaster WHERE $fungsigroupby[2] = $v2->idkode GROUP BY ";
                            $query_nourut = "SELECT MAX(noid) AS noid FROM $tablemaster ";
                            foreach ($fungsigroupby as $k3 => $v3) {
                                if ($v3 != '') {
                                    if ($k3 == 0) {
                                        $query_nourut .= 'GROUP BY ';
                                    }
                                    if ($v3) {
                                        $query_nourut .= "$v3,";
                                    }
                                }
                            }
                            $query_nourut = substr($query_nourut, 0, -1);
                            $query_nourut .= " LIMIT 1";
                            // return $fungsigroupby;
                            $result_nourut = DB::connection('mysql2')->select($query_nourut);
                            if ($result_nourut) {
                                $nourut = substr($v2->kodeformat, 0, strlen($v2->kodeformat)-strlen($result_nourut[0]->noid+1)).$result_nourut[0]->noid+1;
                                // return $nourut;
                            } else {
                                $nourut = 0;
                            }
                            $formdefault .= $nourut;
                        } else if ($v2->kodename == 'BULAN') {
                            $check1 = explode('[', $v2->kodevalue)[0];
                            if (substr($check1, 0, 1) == '/') {
                                $check2 = explode(']', explode('[', $v2->kodevalue)[1]);
                                if (count($check2) > 1) {
                                    $function = $check2[0];
                                }
                            } else {
                                $function = 'FALSE';
                            }
                            if ($function == $v2->fungsi) {
                                $kodeformat = explode('%', $v2->kodeformat);
                                $formdefault .= '/'.date($kodeformat[1]);
                            }
                        } else if ($v2->kodename == 'TAHUN') {
                            $kodeformat = explode('%', $v2->kodeformat);
                            $formdefault .= '/'.date($kodeformat[1]);
                        }
                    }
                } else {
                    $formdefault = '';
                }
            } else {
                $formdefault = $v->formdefault;
            }

            if ($v->onchange) {
                $onchange = $v->onchange;
            } else {
                $onchange = '';
            }
            
            $datanewreq = 'data-'.$v->tablename.'-newreq="'.$v->newreq.'"';
            $dataeditreq = 'data-'.$v->tablename.'-editreq="'.$v->editreq.'"';
            $dataformcaption = 'data-'.$v->tablename.'-formcaption="'.$v->formcaption.'"';

            if ($v->formtypefield == 1) {
                $forminput = '<div id="div-'.$v->fieldname.'" class="col-md-'.$v->formwidth.' col-xs-'.$v->formwidthxs.' '.$v->formpull.'" title="'.$v->formhint.'">
                                <div class="form-group">
                                    '.$row.'
                                        <label class="control-label '.$collabel.'">'.$formcaption.'</label>
                                        <div class="'.$colinput.'">
                                            <input name="'.$inputname.'" class="form-control flat '.$formalign.'" style="height: '.$v->formheight.'px; '.$cssreadonly.'" type="text" id="'.$v->tablename.'-'.$v->fieldname.'" value="'.$formdefault.'" placeholder="'.$v->formplaceholder.'" '.$maxlength.' '.$dataformcaption.' '.$datanewreq.' '.$dataeditreq.' '.$required.' '.$disabled.'>
                                            <span class="help-block"></span>
                                        </div>
                                    '.$endrow.'
                                </div>
                            </div><br>';
            } elseif ($v->formtypefield == 4 ) {
                $forminput = '<div id="div-'.$v->fieldname.'" class="col-md-'.$v->formwidth.' col-xs-'.$v->formwidthxs.' '.$v->formpull.'" title="'.$v->formhint.'">
                                <div class="form-group">
                                    '.$row.'
                                        <label class="control-label '.$collabel.'">'.$formcaption.'</label>
                                        <div class="'.$colinput.'">
                                            <textarea class="form-control flat '.$formalign.'" style="height: '.$v->formheight.'px; '.$cssreadonly.'" id="'.$v->tablename.'-'.$v->fieldname.'" name="'.$inputname.'" value="'.$formdefault.'" placeholder="'.$v->formplaceholder.'" '.$maxlength.' '.$dataformcaption.' '.$datanewreq.' '.$dataeditreq.' '.$required.' '.$disabled.'></textarea>
                                        </div>
                                    '.$endrow.'
                                </div>
                            </div><br>';
            } elseif ($v->formtypefield == 7) {
                $forminput = '<div id="div-'.$v->fieldname.'" class="col-md-'.$v->formwidth.' col-xs-'.$v->formwidthxs.' '.$v->formpull.'" title="'.$v->formhint.'">
                                <div class="form-group">
                                    '.$row.'
                                        <label class="control-label '.$collabel.'">'.$formcaption.'</label>
                                        <div class="'.$colinput.'">
                                            <input type="password" class="form-control flat '.$formalign.'" style="height: '.$v->formheight.'px; '.$cssreadonly.'" id="'.$v->tablename.'-'.$v->fieldname.'" value="'.$formdefault.'" name="'.$$inputname.'" placeholder="'.$v->formplaceholder.'" '.$maxlength.' '.$dataformcaption.' '.$datanewreq.' '.$dataeditreq.' '.$required.' '.$disabled.'>
                                        </div>
                                    '.$endrow.'
                                </div>
                            </div><br>';
            } elseif ($v->formtypefield == 11) {
                // $colformat = \Carbon\Carbon::now()->format($v->colformat);
                $placeholder = 'Select date &amp; time';
                
                if ($disabled) {
                    $datepicker = '';
                } else {
                    $datepicker = 'datepicker';
                }

                $forminput = '<div id="div-'.$v->fieldname.'" class="col-md-'.$v->formwidth.' col-xs-'.$v->formwidthxs.' '.$v->formpull.'" title="'.$v->formhint.'">
                                <div class="form-group">
                                    <label class="control-label">'.$formcaption.'</label>
                                    <div class="input-group date" id="'.$v->tablename.'-'.$v->fieldname.'" data-target-input="nearest">
                                        <input type="text" name="'.$inputname.'" class="'.$datepicker.' form-control flat datetimepicker-input '.$formalign.'" style="height: '.$v->formheight.'px; '.$cssreadonly.'" value="'.$formdefault.'" placeholder="'.$v->formplaceholder.'" data-target="#kt_datetimepicker_1" '.$maxlength.' '.$dataformcaption.' '.$datanewreq.' '.$dataeditreq.' '.$required.' '.$disabled.'>
                                        <div class="input-group-append" data-target="#'.$v->fieldname.'" data-toggle="datetimepicker">
                                            <span class="input-group-text">
                                                <i class="ki ki-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div><br>';
            } elseif ($v->formtypefield == 12  ) {
                $placeholder = 'Select date &amp; time';

                if ($disabled) {
                    $datepicker = '';
                } else {
                    $datepicker = 'datepicker';
                }

                if ($formdefault == '') {
                    $formdefault = date('d-M-Y h:i');
                }

                $forminput = '<div id="div-'.$v->fieldname.'" class="col-md-'.$v->formwidth.' col-xs-'.$v->formwidthxs.' '.$v->formpull.'" title="'.$v->formhint.'">
                                <div class="form-group">
                                    <label class="control-label">'.$formcaption.'</label>
                                    <div class="input-group date" id="'.$v->tablename.'-'.$v->fieldname.'" data-target-input="nearest">
                                        <input type="text" name="'.$inputname.'" class="'.$datepicker.' form-control flat datetimepicker-input '.$formalign.'" style="height: '.$v->formheight.'px; '.$cssreadonly.'" value="'.$formdefault.'" placeholder="'.$v->formplaceholder.'" data-target="#kt_datetimepicker_1" '.$maxlength.' '.$dataformcaption.' '.$datanewreq.' '.$dataeditreq.' '.$required.' '.$disabled.'>
                                        <div class="input-group-append" data-target="#'.$v->fieldname.'" data-toggle="datetimepicker">
                                            <span class="input-group-text">
                                                <i class="ki ki-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div><br>';
            } elseif ($v->formtypefield == 16) {

            } elseif ($v->formtypefield == 21) {
                $forminput = '<div id="div-'.$v->fieldname.'" class="col-md-'.$v->formwidth.' col-xs-'.$v->formwidthxs.' '.$v->formpull.'" title="'.$v->formhint.'">
                                <div class="form-group">
                                    '.$row.'
                                        <label class="control-label '.$collabel.'">'.$formcaption.'</label>
                                        <div class="'.$colinput.'">
                                            <input type="text" class="form-control flat '.$formalign.'" style="height: '.$v->formheight.'px; '.$cssreadonly.'" id="'.$v->tablename.'-'.$v->fieldname.'" name="'.$inputname.'" value="'.$formdefault.'" placeholder="'.$v->formplaceholder.'" '.$maxlength.' '.$dataformcaption.' '.$datanewreq.' '.$dataeditreq.' '.$required.' '.$disabled.'>
                                        </div>
                                    '.$endrow.'
                                </div>
                            </div><br>';
            } elseif ($v->formtypefield == 22) {
                $forminput = '<div id="div-'.$v->fieldname.'" class="col-md-'.$v->formwidth.' col-xs-'.$v->formwidthxs.' '.$v->formpull.'" title="'.$v->formhint.'">
                                <div class="form-group">
                                    '.$row.'
                                        <label class="control-label '.$collabel.'">'.$formcaption.'</label>
                                        <div class="'.$colinput.'">
                                            <input name="'.$inputname.'" class="form-control currency flat '.$formalign.'" style="height: '.$v->formheight.'px; '.$cssreadonly.'" type="text" id="'.$v->tablename.'-'.$v->fieldname.'" value="'.$formdefault.'" placeholder="'.$v->formplaceholder.'" '.$maxlength.' '.$dataformcaption.' '.$datanewreq.' '.$dataeditreq.' '.$required.' '.$disabled.'>
                                            <span class="help-block"></span>
                                        </div>
                                    '.$endrow.'
                                </div>
                            </div><br>';
            } elseif ($v->formtypefield == 31 ) {
                $forminput = '<div id="div-'.$v->fieldname.'" class="col-md-'.$v->formwidth.' col-xs-'.$v->formwidthxs.' '.$v->formpull.'" title="'.$v->formhint.'">
                                <div class="form-group">
                                    '.$row.'
                                        <label class="control-label '.$collabel.'">'.$formcaption.'</label>
                                        <br>
                                        <div class="custom-control custom-checkbox" style="height: '.$v->formheight.'px">
                                            <input type="checkbox" class="custom-control-input '.$formalign.'" onchange="setCheckbox(\''.$v->tablename.'-'.$v->fieldname.'\')" style="'.$cssreadonly.'" id="'.$v->tablename.'-'.$v->fieldname.'" name="'.$inputname.'" value="'.$formdefault.'" placeholder="'.$v->formplaceholder.'" '.$maxlength.' '.$dataformcaption.' '.$datanewreq.' '.$dataeditreq.' '.$required.' '.$disabled.'>
                                            <label class="custom-control-label" for="'.$v->tablename.'-'.$v->fieldname.'">'.$v->fieldname.'</label>
                                        </div>
                                    '.$endrow.'
                                </div>
                            </div><br>';
            } elseif ($v->formtypefield == 41 ) {
                $forminput = '<div id="div-'.$v->fieldname.'" class="col-md-'.$v->formwidth.' col-xs-'.$v->formwidthxs.' '.$v->formpull.'" title="'.$v->formhint.'">
                                <div class="checkbox-list">
                                    <label class="checkbox">
                                        <input type="checkbox" class="form-control flat'.$formalign.'" style="height: '.$v->formheight.'px; '.$cssreadonly.'" id="'.$v->tablename.'-'.$v->fieldname.'" name="'.$inputname.'" value="'.$formdefault.'" placeholder="'.$v->formplaceholder.'" '.$maxlength.' '.$dataformcaption.' '.$datanewreq.' '.$dataeditreq.' '.$required.' '.$disabled.'>
                                        <span></span>
                                        '.$formcaption.'
                                    </label>
                                </div>
                            </div><br>';
            } elseif ($v->formtypefield == 51  ) {
                $forminput = '<div id="div-'.$v->fieldname.'" class="col-md-'.$v->formwidth.' col-xs-'.$v->formwidthxs.' '.$v->formpull.'" title="'.$v->formhint.'">
                                <div class="checkbox-list">
                                    <label class="checkbox">
                                        <input type="checkbox" class="form-control flat '.$formalign.'" style="height: '.$v->formheight.'px; '.$cssreadonly.'" id="'.$v->tablename.'-'.$v->fieldname.'" name="'.$inputname.'" value="'.$formdefault.'" placeholder="'.$v->formplaceholder.'" '.$maxlength.' '.$dataformcaption.' '.$datanewreq.' '.$dataeditreq.' '.$required.' '.$disabled.'>
                                        <span></span>
                                        '.$formcaption.'
                                    </label>
                                </div>
                            </div><br>';
            } elseif ($v->formtypefield == 95) {
                $forminput = $this->getFieldMasterTable($v,$v->fieldname,$v->fieldcaption,$v->newreq,$inputname,$dataformcaption,$datanewreq,$dataeditreq,$request->get('isadd'));
            } elseif ($v->formtypefield == 96) {
                $forminput = $this->getFieldMasterTable($v,$v->fieldname,$v->fieldcaption,$v->newreq,$inputname,$dataformcaption,$datanewreq,$dataeditreq,$request->get('isadd'));
            } elseif ($v->formtypefield == 97) {
                $forminput = $this->getFieldMasterTable($v,$v->fieldname,$v->fieldcaption,$v->newreq,$inputname,$dataformcaption,$datanewreq,$dataeditreq,$request->get('isadd'));
            } else {
                $forminput = '';
            }

            if ($v->newshow == 1) {
                if (empty($v->formgroupname)) {
                    $data['field'][]= array(
                        'label' => $v->fieldcaption,
                        'kode' => $v->formtypefield,
                        'field' => $forminput,
                        'formgroupname' => $v->formgroupname,
                        'colsorted' => $v->colsorted,
                        'fieldname' => $v->fieldname,
                        'newhidden' => $v->newhidden,
                        'edithidden' => $v->edithidden,
                        'tablemaster' => $tabledetail,
                        'tablename' => $v->tablename,
                        'onchange' => $onchange,
                    );
                }

                $data['allfield'][]= array(
                    'label' => $v->fieldcaption,
                    'kode' => $v->formtypefield,
                    'field' => $forminput,
                    'formgroupname' => $v->formgroupname,
                    'colsorted' => $v->colsorted,
                    'fieldname' => $v->fieldname,
                    'newhidden' => $v->newhidden,
                    'newreadonly' => $v->newreadonly,
                    'edithidden' => $v->edithidden,
                    'editreadonly' => $v->editreadonly,
                    'tablemaster' => $tabledetail,
                    'tablename' => $v->tablename,
                    'onchange' => $onchange,
                );
            }
            // dd($data);
            $formtabname = explode(';', $v->formtabname);
            if ($v->formgroupname) { 
                // if ($v->fieldname == 'lastupdate') {
                //     print_r($forminput);die;
                // }
                $data['formgroupname'][$v->idcmswidgetgrid.'-'.$v->formgroupname][$formtabname[1]][] = $forminput;  
            }
        }
        
        $data['cek'] = $this->getFieldMasterTable($widgetgridfield[1],$widgetgridfield[1]->fieldname,$widgetgridfield[1]->fieldcaption,$widgetgridfield[1]->newreq,'fieldname',$dataformcaption,$datanewreq,$dataeditreq,$request->get('isadd'));
        $data['formgroupname'] = (@$data['formgroupname']) ? $data['formgroupname'] : '';
        $data['widgetgridfield'] = $widgetgridfield;
        $data['maincms_widgetgridfield'] = $maincms_widgetgridfield;
        $data['widget_noid'] = $widget_noid;
        // $data['addonchange'] = $addonchange;
        // dd($data['field']);

        header("Content-type: application/json");
        header("Cache-Control: no-cache, must-revalidate");
        echo json_encode($data);
    }

    public function save_log_tmd(Request $request) {
        $connection = 'mysql'.$request->get('idconnection');
        $query = $request->get('query');
        DB::connection($connection)->select($query);
    }

    public function get_select_child_tmd(Request $request) {
        $query = $request->get('query');
        $result = DB::connection('mysql2')->select($query);

        $options = '';
        if ($result) {
            foreach ($result as $k => $v) {
                $options .= "<option value='$v->noid'>$v->nama</option>";
            }
        }
        return $options;
    }

    public function getFieldMasterTable($v, $val2, $val3, $req, $inputname, $dataformcaption, $datanewreq, $dataeditreq, $isadd) {
        if ($v->querylookup) {
            if ($req == 1) {
                $required = 'required';
                $formcaption = $v->formcaption.' <span class="text-danger">*</span>';
            } else {
                $required = '';
                $formcaption = $v->formcaption;
            }
            
            if ($isadd == 'true') {
                if ($v->newreadonly == 1) {
                    $readonly = 'readonly disabled';
                    $cssreadonly = 'background-color: #E5E5E5 !important;';
                    $classselect2 = '';
                } else {
                    $readonly = '';
                    $cssreadonly = '';
                    $classselect2 = 'select2';
                }
            } else {
                if ($v->editreadonly == 1) {
                    $readonly = 'readonly disabled';
                    $cssreadonly = 'background-color: #E5E5E5 !important;';
                    $classselect2 = '';
                } else {
                    $readonly = '';
                    $cssreadonly = '';
                    $classselect2 = 'select2';
                }
            }
    
            $querylookup = explode(';', $v->querylookup);
            $select = explode(':', $querylookup[0])[1];
            $from = explode(':', $querylookup[1])[1];
            $where = explode('AND', explode(':', $querylookup[2])[1]);
            $order = explode(',', explode(':', $querylookup[3])[1]);
    
            $query = "SELECT $select FROM $from WHERE ";
            foreach ($where as $k2 => $v2) {
                if ($v2) {
                    if ($k2 > 0) {
                        $query .= 'AND ';
                    }
                    $_v2 = explode(',', $v2);
                    $query .= "$_v2[0] $_v2[1] $_v2[2]";
                }
            }
            
            $query .= " ORDER BY $order[0] $order[1]";
            $result = DB::connection('mysql2')->select($query);
    
            if ($v->colinline) {
                $html_select = '<div class="col-md-12" style="padding: 0px 0px 0px 0px">';
            } else {
                $html_select = '';
            }

            $html_select .= '<div class="col-md-'.$v->formwidth.' col-xs-'.$v->formwidthxs.' '.$v->formpull.'">
                                <div class="form-group">
                                    <label class="control-label">'.$formcaption.'</label>
                                    <div id="div-'.$v->tablename.'-'.$v->fieldname.'">
                                        <select width="'.$v->colheight.'" '.$required.' class="'.$cssreadonly.' form-control '.$classselect2.' flat" style="height: '.$v->formheight.'px;" ';
            if ($isadd == 'true') {
                if (!$v->newreadonly) {
                    $html_select .= 'id="'.$v->tablename.'-'.$v->fieldname.'" ';
                }
            } else {
                if (!$v->editreadonly) {
                    $html_select .= 'id="'.$v->tablename.'-'.$v->fieldname.'" ';
                }
            }
            $html_select .= 'name="'.$inputname.'" '.$readonly.' ';
            $options = '';
            $formfordisabled = '';
            foreach ($result as $k2 => $v2) {
                $v2 = array_values((array)$v2);
                if ($k2 == 0) {
                    $formfordisabled = '<input type="hidden" id="'.$v->tablename.'-'.$v->fieldname.'" name="'.$inputname.'" value="'.$v2[0].'" '.$dataformcaption.' '.$datanewreq.' '.$dataeditreq.'>';
                }
                $html_select .= ' data-'.$v->tablename.'-'.$v->fieldname.'-'.$v2[0].'="'.((@$v2[2]) ? $v2[2] : $v2[0]).'"';
    
                $options .= '<option value="'.$v2[0].'">'.$v2[1].'</option>';
            }
            // if ($v->fieldsource == 'idcurrency') {
            //     print_r($options);die;
            // }
            $html_select .= ' data-'.$v->tablename.'-'.$v->fieldname.'-querylookup="'.$v->querylookup.'"';
            $html_select .= ' data-'.$v->tablename.'-'.$v->fieldname.'-coltypefield="'.$v->coltypefield.'"';
            $html_select .= '>'.$options;
            $html_select .= '</select>';
            if ($isadd == 'true') {
                if ($v->newreadonly) {
                    $html_select .= $formfordisabled;
                }
            } else {
                if ($v->editreadonly) {
                    $html_select .= $formfordisabled;
                }
            }
            $html_select .= '</div>
                        </div>
                    </div>';
            if ($v->colinline) {
                @$html_select .= '</div>';
            }
    
            return $html_select;
            // print_r($html_select);die;
        } else {
            $this->getFieldMasterTable_backup($v, $val2, $val3, $req, $inputname, $dataformcaption, $datanewreq, $dataeditreq, $isadd);
        }
    }

    public static function getFieldMasterTable_backup($value, $val2, $val3, $req, $inputname, $dataformcaption, $datanewreq, $dataeditreq, $isadd) {

        if ($req == 1) {
            $required = 'required';
        } else {
            $required = '';
        }

        $conv = json_decode($value->lookupconfig, true);
        if ($conv == null) {
            $table = $value->tablename;

            $list = $value->lookupconfig;
            if ($list == null) {
                $select = '';
            } else {
                $stripped =explode(";",$list);
                array_pop($stripped);
                $result_slice = array_slice($stripped, -2);
                $selct = implode(",",$result_slice);
                // $selct = implode(",",$stripped);
                $stripped2 =explode(";",$list);
                $end = end($stripped2);
                $selct2 = "select ".$selct." from ".$end;
                // dd($selct2);
                $hasi = DB::connection('mysql2')->table($end)->select($result_slice)->get();
                $select = '';
                $readonly = '';
                if ($value->newreadonly == 1) {
                    $readonly = 'readonly';
                } else {
                    $readonly = '';
                }
                $sel = array('<div class="col-md-'.$value->formwidth.' col-xs-'.$value->formwidthxs.' '.$value->formpull.'">
                                    <div class="form-group">
                                        <label class="control-label">'.$value->formcaption.'</label>
                                        <div>
                                            <select width="'.$value->colheight.'" '.$required.' class="form-control flat" style="height: '.$value->formheight.'px" id="'.$value->fieldname.'" name="'.$inputname.'" '.$readonly.' ');
                $options = '';
                foreach ($hasi as $key2 => $value2) {
                    array_push($sel, ' data-'.$value->tablename.'-'.$value->fieldname.'-'.$value2->noid.'="'.$value2->noid.'"');

                    $value2 = array_values((array)$value2);
                    $options .= '<option value="'.$value2[0].'">'.$value2[1].'</option>';
                }
                array_push($sel, '>');
                array_push($sel, $options);
                
                array_push($sel,            '</select>
                                        </div>
                                    </div>
                                </div>');
                
                foreach ($sel as $key3 => $value3) {
                    $select .= $sel[$key3];
                }
            }
        } else {
            $convselect = $conv['LookupConfig']['FormLookup']['dataquery'];
            $select = '';
            $hasil = DB::connection('mysql2')->select($convselect);
            $sel = array('<select '.$required.' width="'.$value->colheight.'" class="form-control" id="'.$val3.'" name="'.$inputname.'">');
            
            foreach ($hasil as $key2 => $value2) {
                array_push($sel, '<option value="'.$value2->noid.'" >'.$value2->nama.'</option>');
            }
            
            array_push($sel, '</select>');

            foreach ($sel as $key3 => $value3) {
                $select .= $sel[$key3];
            }
        }
        
        return $select;
    }

    public function get_trigger_on_tmd(Request $request) {
        $query = $request->get('query');
        $alias = $request->get('alias');
        $formula = $request->get('formula');
        $result = DB::connection('mysql2')->select($query)[0];
        // print_r(str_replace($formula, $result->$alias, $formula));die;
        return str_replace($formula, $result->$alias, $formula);
    }

    public function get_tab_data_tmd(Request $request) {
        $data = json_decode($request->get('data'));
        $tablename = $request->get('tablename');
        $panelnoid = $request->get('panelnoid');
        $masternoid = json_decode($request->get('masternoid'));
        $fieldnametab = $request->get('fieldnametab');
        $tablelookuptab = $request->get('tablelookuptab');
        $statusadd = $request->get('statusadd');
        $draw = $request['draw'];
        $limit = $request->input('length');
        $start = $request->input('start');
        $search = $request->input('search.value');
        $ordercolumn = $request->get('order')[0]['column'];
        $orderdir = $request->get('order')[0]['dir'];
        $namecolumn = $request->get('columns')[$ordercolumn]['name'];

        $datalookup = [];
        $checkduplicatetable = [];
        foreach ($fieldnametab as $k => $v) {
            $fieldlookup = explode('_lookup', $v);
            if (count($fieldlookup) == 2) {
                $_tablelookup = explode(';', $tablelookuptab[$k]);
                $_tl0 = $_tablelookup[0];
                $_tl1 = $_tablelookup[1];
                $_tl2 = $_tablelookup[2];
                $_tl3 = $_tablelookup[3];

                if (!array_key_exists($_tl1.$_tl2.$_tl3, $checkduplicatetable)) {
                    $_result = DB::connection('mysql2')->table($_tl3)->select($_tl1,$_tl2)->get();
                    
                    if (count($_result) > 0) {
                        foreach($_result as $k2 => $v2) {
                            $datalookup[$_tl0][$v2->$_tl1] = $v2->$_tl2;
                        }
                    } else {
                        $datalookup[$_tl0] = [];
                    }

                    $checkduplicatetable[$_tl1.$_tl2.$_tl3] = @$datalookup[@$_tl0];
                } else {
                    $datalookup[$_tl0] = $checkduplicatetable[$_tl1.$_tl2.$_tl3];
                }
            }
        }

        $qty_data = 0;
        $qty_data_additional = 0;
        $showsearchrecord = 0;
        $data_order = [];

        if ($ordercolumn != 0) {
            if ($request->get('order')[0]['dir'] == 'asc') {
                $ordercolumndir = SORT_ASC;
            } else {
                $ordercolumndir = SORT_DESC;
            }
    
            $columns = array_column($data, $namecolumn);
            array_multisort($columns, $ordercolumndir, $data);
        
            // print_r('start='.$start.', limit='.$limit.', ordercolumn='.$fieldnametab[$ordercolumn-2].', orderdir='.$orderdir);
        } else {
            // print_r('start='.$start.', limit='.$limit.', ordercolumn='.$fieldnametab[$ordercolumn].', orderdir='.$orderdir);
        }

        if ($data) {
            $response = [];
            $no = 1;
            $key = 0;
            $noids_tab = [];
            $select_all_tab[$panelnoid] = false;
            foreach ($data as $k => $v) {
                if ($search) {
                    $plused_qty_data = false;
                    if ($showsearchrecord < $limit) {
                        if ($k >= $start) {
                            if ($k < ($start+$limit)) {
                                $syncsearch = false;
                                $response[$key][] = '<input type="checkbox" onchange="changeCheckboxTab('.$panelnoid.', '.$k.', '.$v->noid.')" class="checkbox-'.$panelnoid.' checkbox-'.$panelnoid.'-'.$k.'" name="checkbox-'.$panelnoid.'[]" value="FALSE">';
                                $response[$key][] = $no;
                                foreach ($fieldnametab as $k2 => $v2) {
                                    if (@$v->$v2) {
                                        $response[$key][] = $v->$v2;
                                        $pattern = "/".$search."/i";
                                        // $newtag = preg_replace ("/[^a-zA-Z0-9 ]/", "", $v->$v2);
                                        if (preg_match($pattern, $v->$v2)) {
                                            $syncsearch = true;
                                        }
                                        // if ($v2 == 'keterangan') {
                                        //     print_r($search.'//');die;
                                        // }
                                    } else {
                                        $response[$key][] = '';
                                    }
                                }
                                $response[$key][] = '<div class="d-flex align-items-center button-action-tab button-action-tab-'.$k.'">
                                                        <a onclick="editTab(\''.$tablename.'\', \''.$v->noid.'\', \''.$k.'\', '.$panelnoid.')" class="btn btn-warning btn-sm flat mr-2"><i class="fa fa-edit icon-xs" style="text-align:center"></i> </a>
                                                        <a onclick="viewTab(\''.$tablename.'\', \''.$v->noid.'\', \''.$k.'\', '.$panelnoid.')"  class="btn btn-success btn-sm flat mr-2"><i class="fa fa-search icon-xs"></i> </a>
                                                        <a  onclick="removeTab(\''.$tablename.'\', \''.$v->noid.'\', \''.$k.'\', '.$panelnoid.')" class="btn btn-danger btn-sm flat mr-2"><i class="fa fa-trash icon-xs"></i> </a>
                                                    </div>';
                                if ($syncsearch) {
                                    $key++;
                                    $qty_data++;
                                    $no++;
                                    $plused_qty_data = true;
                                } else {
                                    if (count($response) > 0) {
                                        array_pop($response);
                                    }
                                }   
                            }
                            // print_r($k);die;
                            $showsearchrecord++;
                        }
                    }

                    if (!$plused_qty_data) {
                        $qty_data_additional++;
                        // $no++;
                    }
                } else {
                    if ($k >= $start) {
                        if ($k < ($start+$limit)) {
                            $response[$key][0] = '<input type="checkbox" onchange="changeCheckboxTab('.$panelnoid.', '.$k.', '.@$v->noid.')" class="checkbox-'.$panelnoid.' checkbox-'.$panelnoid.'-'.$k.'" name="checkbox-'.$panelnoid.'[]" value="FALSE">';
                            $response[$key][1] = $no;
                            $key_column = 2;
                            foreach ($fieldnametab as $k2 => $v2) {
                                if (@$v->$v2) {
                                    $response[$key][$key_column] = $v->$v2;
                                } else {
                                    $_v2 = str_replace('_lookup', '', $v2);
                                    if (@$v->$_v2) {
                                        $response[$key][$key_column] = $datalookup[$_v2][$v->$_v2];
                                    } else {
                                        $response[$key][$key_column] = '';
                                    }
                                }

                                if ($ordercolumn == $key_column) {
                                    // print_r($v);
                                    // print_r($_v2);
                                    // print_r($v->$_v2);die;
                                    $data_order['_'.$k] = $v->$_v2;
                                }

                                $key_column++;
                            }
                            $response[$key][] = '<div class="d-flex align-items-center button-action-tab button-action-tab-'.$k.'">
                                                    <a onclick="editTab(\''.$tablename.'\', \''.@$v->noid.'\', \''.$masternoid.'\', '.$panelnoid.')" class="btn btn-warning btn-sm flat mr-2"><i class="fa fa-edit icon-xs" style="text-align:center"></i> </a>
                                                    <a onclick="viewTab(\''.$tablename.'\', \''.@$v->noid.'\', \''.$masternoid.'\', '.$panelnoid.')"  class="btn btn-success btn-sm flat mr-2"><i class="fa fa-search icon-xs"></i> </a>
                                                    <a  onclick="removeTab(\''.$tablename.'\', \''.@$v->noid.'\', \''.$masternoid.'\', '.$panelnoid.')" class="btn btn-danger btn-sm flat mr-2"><i class="fa fa-trash icon-xs"></i> </a>
                                                </div>';
                            $noids_tab[$panelnoid][] = @$v->noid;
                            $key++;
                        } else {
                            // print_r($response);die;
                            // if ()
                            // $qty_data++;
                        }
                    }
                }
                // if (count($response) )
                if (!$search) {
                    $qty_data++;
                    $no++;
                }
            }
        } else {
            $response = [
            ];
        }

        if (($qty_data+$start) >= $limit) {
            $qty_data += $qty_data_additional;
        }

        // print_r($response);
        // die;
        // print_r($response);
        // die;

        // print_r($request->get('order')[0]['column']);die;

        echo json_encode([
            'draw' => intval($draw),
            'recordsFiltered' => $qty_data,
            'recordsTotal' => $qty_data,
            'data' => $response,
            'data___' => $data == null ? [] : $data,
            'fieldnametab' => $fieldnametab,
            'statusadd' => $statusadd,
            'limit' => $limit,
            'start' => $start,
            'showsearchrecord' => count($response),
            'qtydataadditional' => $qty_data_additional,
            'noids_tab' => @$noids_tab,
            'select_all_tab' => @$select_all_tab,
        ]);
    }

    public function get_tab_data_tmd_backup(Request $request) {
        $maincms = json_decode($request->get('maincms'));
        $id = $request->get('id');
        $key = $request->get('key');
        $table = $maincms->widget[0]->maintable;

        $tab_data = [];
        $tab_wgf = [];
        $cek = '';

        foreach ($maincms->widget as $k => $v) {
            if ($k > 0) {
                if ($v->noid == $id) {
                    $tab_wgf[] = array_values(array_filter($maincms->widgetgridfield[$k], function($v2, $k2) {
                        if ($v2->editshow == 1 && $v2->colshow == 1) {
                            return $v2;
                        }
                    }, ARRAY_FILTER_USE_BOTH));
                    
                    $select = '';
                    $join = '';
                    foreach ($tab_wgf[$k-1] as $k2 => $v2) {
                        if ($k2 == 0) {
                            $select .= "$v2->tablename.noid,";
                        }

                        if ($v2->tablelookup) {
                            $tablelookup = explode(';', $v2->tablelookup);
                            $select .= "$tablelookup[3].".$tablelookup[2]." AS $tablelookup[3]_$tablelookup[2],";
                            if ($tablelookup[3] != $table) {
                                $join .= "LEFT JOIN $tablelookup[3] ON $v->maintable.$tablelookup[0] = $tablelookup[3].$tablelookup[1] ";
                            }
                        } else {
                            $select .= "$v2->tablename.".$v2->fieldsource." AS $v2->tablename"."_"."$v2->fieldsource,";
                        }
                    }
                    $select = substr($select, 0, -1);

                    $query = "SELECT $select FROM $table LEFT JOIN $v->maintable ON $table.noid = $v->maintable.idmaster $join WHERE $table.noid = $id";
                    $result = DB::connection('mysql2')->select("SELECT $select FROM $table JOIN $v->maintable ON $table.noid = $v->maintable.idmaster $join WHERE $table.noid = $id");
                    $data_convert = [];
                    $no = 1;
                    foreach ($result as $k2 => $v2) {
                        $first_row = [];
                        array_push($first_row,'<input type="checkbox" onchange="changeCheckboxTab('.$panelnoid.', '.$k2.', '.$v2->noid.')" class="checkbox-'.$panelnoid.' checkbox-'.$panelnoid.'-'.$k2.'" name="checkbox-'.$panelnoid.'[]" value="FALSE">');
                        array_push($first_row, $no);
                        // array_push((array)$v2,'<div class="d-flex align-items-center button-action button-action-'.$k2.'">
                        //     <a onclick="edit('.$v->noid.')" class="btn btn-warning btn-sm flat mr-2"><i class="fa fa-edit icon-xs" style="text-align:center"></i> </a>
                        //     <a onclick="view('.$v->noid.')"  class="btn btn-success btn-sm flat mr-2"><i class="fa fa-search icon-xs"></i> </a>
                        //     <a  onclick="remove('.$v->noid.')" class="btn btn-danger btn-sm flat mr-2"><i class="fa fa-trash icon-xs"></i> </a>
                        //     </div>');
                        $last_row = [];
                        array_push($last_row, '<div class="d-flex align-items-center button-action-tab button-action-tab-'.$k.'">
                            <a onclick="editTab(\''.$v->maintable.'\', \''.$v2->noid.'\', \''.$v->noid.'\')" class="btn btn-warning btn-sm flat mr-2"><i class="fa fa-edit icon-xs" style="text-align:center"></i> </a>
                            <a onclick="viewTab(\''.$v->maintable.'\', \''.$v2->noid.'\', \''.$v->noid.'\')"  class="btn btn-success btn-sm flat mr-2"><i class="fa fa-search icon-xs"></i> </a>
                            <a  onclick="removeTab(\''.$v->maintable.'\', \''.$v2->noid.'\', \''.$v->noid.'\')" class="btn btn-danger btn-sm flat mr-2"><i class="fa fa-trash icon-xs"></i> </a>
                            </div>');
                        
                        $middle_row = (array)$v2;
                        array_shift($middle_row);
                        $data_convert[] = array_values(array_merge($first_row, $middle_row, $last_row));
                        // $cek = array_values(array_merge((array)$v2, $first_row));
                        $no++;
                    }
                    $tab_data = $data_convert;
                }
            }
        }

        // $data_array_numeric = [];
        // foreach ($tab_data[$key] as $k => $v) {
        //     $data_array_numeric[] = $v;
        // }

        $response = [
            'draw' => 1,
            'recordsFiltered' => count($tab_data),
            'recordsTotal' => count($tab_data),
            'data' => $tab_data,
            'cek' => $cek,
            'query' => $query,
            'result' => $result,
            'tab_wgf' => $tab_wgf,
        ];
        echo json_encode($response);
        // foreach ($tab_join as $k => $v) {
        // }
    }

    public function find_tmd_backup(Request $request) {
        $maincms = json_decode($request->get('maincms'));
    }

    public function find_tmd(Request $request) {
        $maincms = json_decode($request->get('maincms'));
        
        $tab_join = [];
        foreach ($maincms->widget as $k => $v) {
            if ($k > 0) {
                $tab_join[] = $v->maintable;
            }
        }

        $data = $request->all();
        $menu = $maincms->menu;

        $page = $maincms->page;
        $panel = $maincms->panel[0];
        $widget = $maincms->widget[0];
        $filtertable = json_decode($widget->configwidget);
        $fieldtabel = $filtertable->WidgetFilter;
        $table = $widget->maintable;
        $data = $request->all();
        $widget = $maincms->widget[0];
        $widgetgrid = $maincms->widgetgrid[0];
        $widgetgridfield = array_values(array_filter($maincms->widgetgridfield[0], function($v, $k) {
            if ($v->editshow == 1 && $v->colshow == 1) {
                return $v;
            }
        }, ARRAY_FILTER_USE_BOTH));
        // $widgetgridfield = WidgetGridField::where('idcmswidgetgrid', $widgetgrid->noid)->where('editshow',1)->get();
        $id = $data['id'];

        $datatab = DB::connection('mysql2')->table($table)->where('noid',$id)->first();
        $query = [];
        $tab_data = [];
        $tab_wgf = [];
        foreach ($tab_join as $k => $v) {
            $tab_wgf[$k] = array_values(array_filter($maincms->widgetgridfield[$k+1], function($v2, $k2) {
                if ($v2->editshow == 1) {
                    return $v2;
                }
            }, ARRAY_FILTER_USE_BOTH));
            
            $select = '';
            foreach ($tab_wgf[$k] as $k2 => $v2) {
                $select .= "$v2->tablename.".$v2->fieldsource.',';
            }
            $select = substr($select, 0, -1);
            // $cek = $tab_wgf[$k][0];

            $query[$k] = "SELECT $select FROM $table LEFT JOIN $v ON $table.noid = $v.idmaster WHERE $table.noid = $id";
            $tab_data[$k] = DB::connection('mysql2')->select($query[$k]);
        }

        // FIND DETAIL
        $maincms_widget = [];
        foreach ($maincms->widget as $k => $v) {
            foreach ($v as $k2 => $v2) {
                $maincms_widget[$v->noid] = $v;
            }
        }

        $result_detail = [];
        foreach ($maincms->panel as $k => $v) {
            if ($k > 0) {
                $select = ['idmaster','noid'];
                foreach ($maincms->widgetgridfield as $k2 => $v2) {
                    if ($k2 > 0) {
                            foreach ($v2 as $k3 => $v3) {
                                if ($v3->idcmswidgetgrid == $v->noid) {
                                    if ($v3->colshow) {
                                        $select[] = $v3->fieldsource;
                                    }
                                }
                            }
                    }
                }
                $result_detail[$v->noid] = DB::connection('mysql2')->table($maincms_widget[$v->idwidget]->maintable)->select($select)->where('idmaster',$datatab->noid)->get();
            }
        }
        // $hasil[] = [
        //     'field' => 'noid',
        //     'value' => $id,
        //     'disabled' => false,
        //     'hidden' => false,
        // ];
        foreach ($widgetgridfield as $k => $v) {
            $fs = $v->fieldsource;

            $value = '';
            if ($v->coltypefield == 11) {
                $value = date("d-M-Y", strtotime($datatab->$fs));
            } else {
                $value = $datatab->$fs;
            }

            $hasil[] = [
                'field' => $v->fieldname,
                'tablename' => $v->tablename,
                'value' => $value,
                'disabled' => ($v->editreadonly == 1) ? true : false,
                'newhidden' => ($v->newhidden == 1) ? true : false,
                'edithidden' => ($v->edithidden == 1) ? true : false,
                'coltypefield' => $v->coltypefield,
                'formtypefield' => $v->formtypefield
            ];
        }
        // $hasil['noid'] = $id;
        $hasil['query'] = $query;
        $hasil['widgetgridfield'] = $widgetgridfield;
        $hasil['tab_data'] = $tab_data;
        $hasil['tab_wgf'] = $tab_wgf;
        $hasil['nownoid'] = $datatab->noid;
        $hasil['datatab'] = $datatab;
        $hasil['result_detail'] = $result_detail;
        echo json_encode($hasil);
    }

    public function filter_tmd() {
        DB::enableQueryLog();
        $data = DB::connection('mysql2')->table($_POST['table'])->where($_POST['fieldname'], $_POST['operator'], $_POST['value'])->get();

        $json_data = array(
            "draw" => 1,
            "recordsTotal" => count($data),
            "recordsFiltered" => count($data),
            "data" => $data,
            "cek" => DB::getQueryLog()
        );
        echo json_encode($json_data);
    }

    public function get_next_noid_tmd(Request $request) {
        $table = $request->get('table');
        $nextnoid = DB::connection('mysql2')->table($table)->select('noid')->orderBy('noid','desc')->first();
        if ($nextnoid) {
            $nextnoid = $nextnoid->noid+1;
        } else {
            $nextnoid = 0;
        }
        echo json_encode($nextnoid);
    }

    public function generate_code($tablemaster, $formdefault) {
        $formdefault = explode('/', $formdefault);
        
        $result_appmkode = DB::connection('mysql2')->table('appmkode')->where('noid', $formdefault[1])->first();
        $result_appmkodedetail = DB::connection('mysql2')->table('appmkodedetail')->where('idkode', $formdefault[1])->orderBy('nourut', 'ASC')->get();

        $formdefault = $result_appmkode->kode;
        foreach ($result_appmkodedetail as $k2 => $v2) {
            if ($v2->kodename == 'KODE') {
                $formdefault .= $v2->kodevalue;
            } else if ($v2->kodename == 'NOURUT') {
                $fungsigroupby = explode(';', $v2->fungsigroupby);
                // $query_nourut = "SELECT MAX(noid) AS noid FROM $tablemaster WHERE $fungsigroupby[2] = $v2->idkode GROUP BY ";
                $query_nourut = "SELECT MAX(noid) AS noid FROM $tablemaster ";
                foreach ($fungsigroupby as $k3 => $v3) {
                    if ($v3 != '') {
                        if ($k3 == 0) {
                            $query_nourut .= 'GROUP BY ';
                        }
                        if ($v3) {
                            $query_nourut .= "$v3,";
                        }
                    }
                }
                $query_nourut = substr($query_nourut, 0, -1);
                $query_nourut .= " LIMIT 1";
                $result_nourut = DB::connection('mysql2')->select($query_nourut);
                if ($result_nourut) {
                    $nourut = substr($v2->kodeformat, 0, strlen($v2->kodeformat)-strlen($result_nourut[0]->noid)).$result_nourut[0]->noid;
                } else {
                    $nourut = 0;
                }
                $formdefault .= $nourut;
            } else if ($v2->kodename == 'BULAN') {
                $check1 = explode('[', $v2->kodevalue)[0];
                if (substr($check1, 0, 1) == '/') {
                    $check2 = explode(']', explode('[', $v2->kodevalue)[1]);
                    if (count($check2) > 1) {
                        $function = $check2[0];
                    }
                } else {
                    $function = 'FALSE';
                }
                if ($function == $v2->fungsi) {
                    $kodeformat = explode('%', $v2->kodeformat);
                    $formdefault .= '/'.date($kodeformat[1]);
                }
            } else if ($v2->kodename == 'TAHUN') {
                $check1 = explode('[', $v2->kodevalue)[0];
                if (substr($check1, 0, 1) == '/') {
                    $check2 = explode(']', explode('[', $v2->kodevalue)[1]);
                    if (count($check2) > 1) {
                        $function = $check2[0];
                    }
                } else {
                    $function = 'FALSE';
                }
                if ($function == $v2->fungsi) {
                    $kodeformat = explode('%', $v2->kodeformat);
                    $formdefault .= '/'.date($kodeformat[1]);
                }
            }
        }
        return [
            'kode' => $formdefault,
            'nourut' => $nourut,
        ];
    }
    
    public function save_tmd(Request $request) {
        $maincms = json_decode($request->get('maincms'));
        $statusadd = json_decode($request->get('statusadd'));
        $tablemaster = $maincms->widget[0]->maintable;
        $inputdatamaster = $request->get('input_data')['master'];
        $inputdatadetail = json_decode($request->get('input_data')['detail']);
        
        $maincms_widgetgridfield = [];
        $fieldsource_generatecode = [];
        $formdefault_generatecode = [];
        
        foreach ($maincms->widgetgridfield2 as $k => $v) {
            foreach ($v as $k2 => $v2) {
                if ($k == 0) {
                    if ($statusadd) {
                        if ($v2->fieldsource == 'docreate') {
                            $inputdatamaster['docreate'] = date('Y-m-d H:i:s');
                        }
                    } else {
                        unset($inputdatamaster['docreate']);
                    }

                    if ($v2->fieldsource == 'lastupdate') {
                        $inputdatamaster['lastupdate'] = date('Y-m-d H:i:s');
                    }   
                }
            }
        }

        foreach ($maincms->widgetgridfield as $k => $v) {
            foreach ($v as $k2 => $v2) {
                $maincms_widgetgridfield[$v2->idcmswidgetgrid][$v2->fieldsource] = $v2;

                if ($k == 0) {
                    if ($v2->coltypefield == 11) {
                        $originaldate = $inputdatamaster[$v2->fieldsource];
                        $inputdatamaster[$v2->fieldsource] = date("Y-m-d", strtotime($originaldate));
                    }
                }

                if (substr($v2->formdefault, 0, 12) == 'generatecode') {
                    $fieldsource_generatecode[] = $v2->fieldsource;
                    $formdefault_generatecode[] = $v2->formdefault;
                }
            }
        }

        foreach ($fieldsource_generatecode as $k => $v) {
            $generatecode = $this->generate_code($tablemaster, $formdefault_generatecode[$k]);
            $inputdatamaster[$v] = $generatecode['kode'];
            $inputdatamaster['nourut'] = $generatecode['nourut'];
        }

        // return response()->json($inputdatamaster);
        if ($statusadd) {
            $result_master = DB::connection('mysql2')->table($tablemaster)->insert($inputdatamaster);
        } else {
            $result_master = DB::connection('mysql2')->table($tablemaster)->where('noid', $inputdatamaster['noid'])->update($inputdatamaster);
        }
        $result_detail = [];
        foreach ((array)$maincms->panel as $k => $v) {
            if ($k > 0) {
                $panel_noid = $v->noid;
                $tabledetail = $inputdatadetail->$panel_noid->table;
                $inputdetail = $inputdatadetail->$panel_noid->data ? $inputdatadetail->$panel_noid->data : [];
                // print_r($inputdetail);
                // die;
                if (count($inputdetail) > 0) {
                    foreach ($inputdetail as $k2 => $v2) {
                        $v2 = (array)$v2;
                        
                        $replace_v2 = [];
                        foreach ($v2 as $k3 => $v3) {
                            if (!strpos($k3, '_lookup')) {
                                $replace_v2[$k3] = $v3;
                            }
                        }

                        foreach ($maincms->widgetgridfield2[$k] as $k3 => $v3) {
                            if ($statusadd) {
                                if (@$v3->fieldsource == 'docreate') {
                                    $replace_v2['docreate'] = date('Y-m-d H:i:s');
                                }
                            } else {
                                unset($replace_v2['docreate']);
                            }
    
                            if (@$v3->fieldsource == 'lastupdate') {
                                $replace_v2['lastupdate'] = date('Y-m-d H:i:s');
                            }
                        }

                        $inputdetail[$k2] = $replace_v2;
                    }
                    
                    if ($statusadd) {
                        $result_detail[] = DB::connection('mysql2')->table($tabledetail)->insert($inputdetail);
                    } else {
                        $result = DB::connection('mysql2')->table($tabledetail)->where('idmaster',$inputdatamaster['noid'])->delete();
                        // if ($result) {
                            foreach ($inputdetail as $k2 => $v2) {
                                $result_detail[] = DB::connection('mysql2')->table($tabledetail)->insert($inputdetail[$k2]);
                            }
                        // }
                    }
                } else {
                    $result_detail[] = DB::connection('mysql2')->table($tabledetail)->where('idmaster',$inputdatamaster['noid'])->delete();
                }
            }
        }
        
        echo json_encode(array(
            'status' => 'add',
            'result_master' => $result_master,
            'result_detail' => $result_detail,
        ));
        // print_r($result_detail);
        return false;
        // $x = '9921050102';
        // echo json_encode($request->get('table'));





        
        $data = $request->all();
        $datapost =  array();
        $dataposttab =  array();
        $dataposttabfix =  array();
        $tabletab =  array();
        parse_str($data['data']['data'], $datapost);
        
        if (count($data['data']['datatab']) > 0) {
            foreach ($data['data']['datatab'] as $k => $v) {
                parse_str($data['data']['datatab'][$k], $dataposttab[$k]);

                foreach ($dataposttab[$k] as $k2 => $v2) {
                    $inputname = explode('_', $k2);
                    $tabletab[$k] = $inputname[0];
                    $dataposttabfix[$k][$inputname[1]] = $v2;
                }
            }
        }

        if ($request->get('statusadd') == 'true') {
            $nextnoid = DB::connection('mysql2')->table($table)->select('noid')->orderBy('noid','desc')->first()->noid+1;
            
            $datapost['noid'] = $nextnoid;
            
            $result_master = DB::connection('mysql2')->table($table)->insert($datapost);
            
            if (count($dataposttabfix) > 0) {
                $result_detail = [];
                foreach ($tabletab as $k => $v) {
                    $nextnoidtab = DB::connection('mysql2')->table($v)->select('noid')->orderBy('noid','desc')->first()->noid+1;
                    $dataposttabfix[$k]['noid'] = $nextnoidtab;
                    $dataposttabfix[$k]['idmaster'] = $nextnoid;
                    $result_detail[] = DB::connection('mysql2')->table($v)->insert($dataposttabfix[$k]);
                }
            }
            
            echo json_encode(array(
                'status' => 'add',
                'table' => $table,
                'success' => true,
                'result_master' => $result_master,
                'result_detail' => $result_detail,
            ));
        } else {
            $array = \array_diff($datapost, ["noid" => $datapost['noid']]);
            $total = DB::connection('mysql2')->table($table)->where('noid', $request->get('editid'))->update($array);
            
            echo json_encode(array(
                'status' => 'edit',
                'table' => $table,
                'success' => $total,
                'array' => $array,
                'editid' => $request->get('editid')
            ));
        }
    }
    
    public function delete_tab_tmd(Request $request) {
        $id = $request->get('id');
        $another_id = $request->get('another_id');
        $another_table = $request->get('another_table');

        // $result = DB::connection('mysql2')->table($another_table)->where('noid',$another_id)->delete();
        if (true) {
            echo json_encode([
                'success' => true,
                'message' => 'Deleted has been successfully' 
            ]);
        } else {
            echo json_encode([
                'success' => false,
                'message' => 'Deleted error.' 
            ]);
        }
    }

    public function delete_tmd(Request $request) {
        $maincms = json_decode($request->get('maincms'));
        $id = $request->get('id');
        $table = $request->get('table');
        $result_master = DB::connection('mysql2')->table($table)->where('noid',$id)->delete();

        $result_detail = [];
        foreach ($maincms->widget as $k => $v) {
            if ($k > 0) {
                $result_detail[] = DB::connection('mysql2')->table($v->maintable)->where('idmaster',$id)->delete();
            }
        }

        if ($result_master && $result_detail) {
            echo json_encode([
                'success' => true,
                'message' => 'Deleted has been successfully' 
            ]);
        } else {
            echo json_encode([
                'success' => false,
                'message' => 'Deleted error.' 
            ]);
        }
    }

    public function delete_selected_tmd(Request $request) {
        $selecteds = $request->get('selecteds');
        $table = json_decode($request->get('table'));
        $result = DB::connection('mysql2')->table($table[0])->whereIn('noid',$selecteds)->delete();
        if ($result) {
            foreach ($table as $k => $v) {
                if ($k > 0) {
                    $result_detail = DB::connection('mysql2')->table($v)->whereIn('idmaster',$selecteds)->delete();
                    if (!$result_detail) {
                        return response()->json($result_detail);
                    }
                }
            }
        }

        if ($result_detail) {
            echo json_encode([
                'success' => true,
                'message' => 'Deleted has been successfully' 
            ]);
        } else {
            echo json_encode([
                'success' => false,
                'message' => 'Deleted error.',
                'table' => $table,
                'selecteds' => $selecteds,
            ]);
        }
    }
    
    public function get_slide(Request $request) {
        $id_slider = $request->get('id_slider');
        $widget = Menu::getAllPanelSlide($id_slider);
        $hasi = json_decode($widget[0]->widget_configwidget);
        $hasil = $hasi->Widget_Slider->datasource->dataquery;
        // dd($hasil);
        $result =Menu::ambilQuery($hasil);
        header("Content-type: application/json");
        header("Cache-Control: no-cache, must-revalidate");
        echo json_encode(array(
            'hasil'=>$result
        ));
    }

    public function save_filter_tmd(Request $request) {
        $data = $request->all()['data']['data'];
        $url_code = $request->all()['data']['url_code'];
        $parse_data = array();
        parse_str($data, $parse_data);

        $widget = Widget::where('kode', $url_code)->first();

        $widget_config = json_decode($widget->configwidget)->WidgetConfig;
        $widget_filter = [];
        
        foreach ($parse_data['tabs'] as $k => $v) {
            $widget_filter[$k] = (object)[
                'groupcaption' => $parse_data['groupcaption'][$k],
                'groupcaptionshow' => ($parse_data['groupcaptionshow'][$k] == 1) ? true : false,
                'groupdropdown' => (int)$parse_data['groupdropdown'][$k],
                'button' => []
            ];

            foreach ($parse_data['tabsrow'] as $k2 => $v2) {
                if ($v == $v2) {
                    $explode1 = explode('#', $parse_data['condition'][$k2]);
                    $explode2 = explode(';', $explode1[1]);
                    $groupoperand = $explode1[0];
                    $fieldname = $explode2[0];
                    $operator = $explode2[1];
                    $value = $explode2[2];
                    $operand = $explode2[3];

                    $widget_filter[$k]->button[$k2] =  (object)[
                        'buttoncaption' => $parse_data['buttoncaption'][$k2],
                        'buttonactive' => ($parse_data['buttonactive'][$k2] == 1) ? true : false,
                        'classcolor' => $parse_data['classcolor'][$k2],
                        'classicon' => $parse_data['classicon'][$k2],
                        'condition' => [
                            (object)[
                                'groupoperand' => $groupoperand,
                                'querywhere' => [
                                    (object)[
                                        'fieldname' => $fieldname,
                                        'operator' => $operator,
                                        'value' => $value,
                                        'operand' => $operand
                                    ]
                                ]
                            ]
                        ]
                    ];
                }
            }
        }

        $config_widget = (object)[
            'WidgetFilter' => $widget_filter,
            'WidgetConfig' => $widget_config
        ];

        if($widget) {
            $result = DB::table('cmswidget')->where('kode', $url_code)->update(['configwidget' => json_encode($config_widget)]);
        }

        echo json_encode(array(
            'response' => $parse_data,
            'config_widget' => json_encode($config_widget),
            'result' => @$result
        ));
    }

}