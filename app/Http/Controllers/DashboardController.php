<?php

namespace App\Http\Controllers;
Use DB;
use App\User;
use App\Menu;
use App\Page;
use App\Panel;
use App\Widget;
use App\WidgetGrid;
use App\WidgetGridField;
use App\Departement;
use App\Company;
use App\ViewGridField;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Schema;
use Carbon;
use PDF;

class DashboardController extends Controller {

    public static function index($id) {
        
        if(!Session::get('login')){
            return view('pages.login');
        }

        $code = $id;
        $menu = Menu::where('noid',$id)->orWhere('urlmasking', $id)->first();
        $id = $menu->noid;
        $page = Page::where('noid',$id)->first();
        $breadcrumb = Menu::getBreadcrumb($id);
        $idhomepagelink = $id;
        $user = User::where('noid', Session::get('noid'))->first();
        $idhomepage =  $user->idhomepage;
        $noid = Session::get('noid');
        $myusername = Session::get('myusername');
        // $menu = Menu::where('noid',$id)->first();
        // if (strpos($menu->menucaption, ' ') !== false) {
        //     $pecah = explode(' ', $menu->menucaption);
        //     $namaview = strtolower($pecah[1]);
        // } else {
        //     $namaview = strtolower($menu->menucaption);
        // }
        // $widget = Widget::where('kode', $id)->first();
        // $namatable = $id;
        // $public = $menu->ispublic;
        // $panel = Panel::where('idpage', $page->noid)->orderBy('norow','asc')->orderBy('nocol','asc')->get();
        $page_title = $page->pagetitle;
        $page_description = $page->pagetitle;
        $noid = Session::get('noid');
        $widget = Menu::getAllPanel($id);
        // $error = 0;
        // $dashboard = 0;
        // $menu2 = Menu::where('noid',$id)->first();
        // $page2 = Page::where('noid',$menu2->linkidpage)->first();
        // $panel2 = Panel::where('idpage',$page2->noid)->first();
        // $widget2 = Widget::where('noid', $panel2->idwidget)->first();
        // $table = $widget2->maintable;
        // $widgetgrid2 = WidgetGrid::where('idcmswidget', $widget2->noid)->first();
        // if (isset($widgetgrid2)) {
        //     $widgetgridfield2 = WidgetGridField::where('idcmswidgetgrid', $widgetgrid2->noid)->where('colshow',1)->orderBy('nourut','asc')->orderBy('noid','asc')->get();
        //     $data['columns'] = array();
        //     $data['colgroupname'] = array();
        //     $data['sort'] = array();

        //     for ($i=0; $i <count($widgetgridfield2) ; $i++) {
        //         if ($widgetgridfield2[$i]->colsorted == 1) {
        //             $cals_sort = 'sortr';
        //         } else {
        //             $cals_sort = 'no-sorter';
        //         }
        //         array_push($data['columns'], '<th class="'.$cals_sort.'"  style="width:'.$widgetgridfield2[$i]->colwidth.' px" >'.$widgetgridfield2[$i]->fieldcaption.'</th>');
        //         array_push($data['colgroupname'], $widgetgridfield2[$i]->colgroupname);
        //         array_push($data['sort'], $widgetgridfield2[$i]->colsorted);
        //     }
        //     // $columns = $data['columns'];
        //     if (!empty($data['colgroupname'])) {
        //         $data['header_atas'] = '<tr class="atas" role="group-header">';
        //         $data['header_atas'] .= '<th style="text-align:center" colspan="2">#</th>';
        //         $data['header_atas'] .= '<th style="text-align:center" colspan="'.count($widgetgridfield2).'">---</th>';
        //         $data['header_atas'] .= '<th colspan="1" style="text-align:center">---</th>';
        //         $data['header_atas'] .= '</tr>';
        //     } else {
        //         $datahead = array_count_values($data['colgroupname']);
        //         $data['header_atas'] = '<tr class="footter">';
        //         $data['header_atas'] .= '<th style="text-align:center" colspan="2">#</th>';
            
        //         foreach ($datahead as $key => $value) {
        //             $data['header_atas'] .= '<th style="text-align:center" colspan="'.$value.'">'.$key.'</th>';
        //         }
        //         $data['header_atas'] .= '<th colspan="1" style="text-align:center">---</th>';
        //         $data['header_atas'] .= '</tr>';
        //     }
        //     $header_atas =   $data['header_atas'];
        // } else {
        //     $header_atas = '';
        // }
        
        // dd($widget);
        $idtypepage = $idhomepagelink;
        $pagenoid = $idhomepagelink;
        $tabletitle = $page_title;

        if ($page->pagecontroller == null) {
            // dd('ok2');
            $vienya = 'dashboard';
            return view('lam_dashboard.'.$vienya, compact(
                'noid',
                'myusername',
                'idhomepage',
                'idhomepagelink',
                'page',
                'page_title',
                'page_description',
                // 'columns',
                // 'header_atas',
                // 'error',
                // 'dashboard',
                // 'widgetgrid2',
                // 'namatable',
                'breadcrumb',
                // 'public',
                'widget',
                'idtypepage',
                'pagenoid',
                'tabletitle'
                // 'panel',
                // 'menu',
                // 'namatable'
            ));
        } else {
            // dd('ok');
            // $view = $page->pagecontroller;
            // $keywords = explode("-", str_replace(array("\n", "\t", "\r", "\a", "/", "//",":"), "-",$view));
            // $controllerView = $keywords[3];
            // $viewRedi = $keywords[4];
            // $folder = $keywords[0];
            // $urlnya = 'App\Http\Controllers';
            // $existsfile = $urlnya." \ ".$folder." \ ".$controllerView;
            // $tamba = $urlnya." \ ".$folder." \ ".$controllerView;
            // $tamba = str_replace(" ","",$tamba);
            // $existsfile = str_replace(" ","",$existsfile);
            // if (!class_exists($tamba)) {
            //     return ErrorController::UnderMaintenance($id);
            // } else if (!method_exists($existsfile,$viewRedi)) {
            //     return ErrorController::UnderMaintenanceFun($id);
            // } else {
            //     return app($tamba)->$viewRedi($id);
            // }
        }
    }

    public function ambildata_filterbutton(Request $request) {
        $kode = $request->get('table');
        // dd($kode);
        $menu = Menu::where('noid',$kode)->first();

        $page = Page::where('noid',$menu->linkidpage)->first();
        $panel = Panel::where('idpage',$page->noid)->first();
        $widget = Widget::where('noid', $panel->idwidget)->first();
        $filtertable = json_decode($widget->configwidget);
        $fieldtabel = $filtertable->WidgetFilter;
        
        $data['buttonfilter'] = [];
        foreach ($fieldtabel as $key => $value) {
            foreach ($value->button as $key2 => $value2) {
                $pecah = explode('.', $value2->condition[0]->querywhere[0]->fieldname);
                $oncl = 'onclick="caridata(\''.$pecah[1].'\')" ';
                $data['buttonfilter'][$key][] = array(
                    'nama'=>$value2->buttoncaption,
                    'buttonactive'=>$value2->buttonactive,
                    'fieldname'=>$pecah[1],
                    'taga'=>'<a href="#" class="btn btn-outline-secondary btn-sm" style="background-color:#E3F2FD;"'.$oncl.'>'.$value2->buttoncaption.' </a>',
                    'value'=>$value2->condition[0]->querywhere[0]->value
                );
            }
        }

        $json_data = array(
            "filterButton" => $data['buttonfilter']
        );

        header("Content-type: application/json");
        header("Cache-Control: no-cache, must-revalidate");
        echo json_encode($json_data);
    }

    public function ambildata_tabel(Request $request) {
        $cek = '';
        $kode = $request->get('table');
        // dd($kode);
        $menu = Menu::where('noid',$kode)->first();

        $page = Page::where('noid',$menu->linkidpage)->first();
        $panel = Panel::where('idpage',$page->noid)->first();
        $widget = Widget::where('noid', $panel->idwidget)->first();
        $filtertable = json_decode($widget->configwidget);
        $fieldtabel = $filtertable->WidgetFilter;
        $table = $widget->maintable;
        $widgetgrid = WidgetGrid::where('idcmswidget', $widget->noid)->first();
        $widgetgridfield2 = WidgetGridField::where('idcmswidgetgrid', $widgetgrid->noid)->where('colshow',1)->orderBy('nourut','asc')->orderBy('noid','asc')->get();
        $widgetgridfield = WidgetGridField::where('idcmswidgetgrid', $widgetgrid->noid)->where('colshow',1)->orderBy('nourut','asc')->orderBy('noid','asc')->get();
        $widgetgridfield3 = WidgetGridField::where('idcmswidgetgrid', $widgetgrid->noid)->where('colshow',1)->where('colsearch',1)->orderBy('nourut','asc')->orderBy('noid','asc')->get();
        // dd($widgetgrid->recperpage);
        // $widgetgridfield2 = WidgetGridField::where('idcmswidgetgrid', $widgetgrid->noid)->orderBy('nourut','asc')->orderBy('noid','asc')->get();

        $data['buttonfilter'] = [];
        foreach ($fieldtabel as $key => $value) {
            foreach ($value->button as $key2 => $value2) {
                $pecah = explode('.', $value2->condition[0]->querywhere[0]->fieldname);
                $oncl = 'onclick="caridata("'.$pecah[1].'")" ';
                $data['buttonfilter'][$key][] = array(
                'nama'=>$value2->buttoncaption,
                'buttonactive'=>$value2->buttonactive,
                'fieldname'=>$pecah[1],
                'taga'=>'<a href="#" class="btn btn-outline-secondary btn-sm"   style="background-color:#E3F2FD;"  '.$oncl.'   >'.$value2->buttoncaption.' </a>',
                'value'=>$value2->condition[0]->querywhere[0]->value
            );
        }
    }

    // dd($data['buttonfilter']);
    $data['columns'] = array('<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike">','No');
    $data['columns2'] = array();
    $data['width'] = array();
    $data['colheaderalign'] = array();
    $data['coltypefield'] = array();
    $data['colgroupname'] = array();
    $data['colheaderalign'] = array();
    $data['coldefault'] = array();
    $data['colsearch'] = array();

   for ($i=0; $i <count($widgetgridfield3) ; $i++) {
     $data['columns2'][] =  $widgetgridfield3[$i]->fieldname;
   }

   for ($i=0; $i <count($widgetgridfield) ; $i++) {

     array_push($data['columns'], $widgetgridfield[$i]->fieldcaption);
     array_push($data['width'], $widgetgridfield[$i]->colwidth.'%');
     array_push($data['colheaderalign'], $widgetgridfield[$i]->colheaderalign);
     array_push($data['coltypefield'], $widgetgridfield[$i]->coltypefield);
     array_push($data['colgroupname'], $widgetgridfield[$i]->colgroupname);
     array_push($data['coldefault'], $widgetgridfield[$i]->coldefault);
     array_push($data['colsearch'], $widgetgridfield[$i]->colsearch);
   }

   // dd($data['colheaderalign']);
   if(!empty($data['colgroupname'])){
       $data['header_atas'] = '<tr class="atas" role="group-header">';
       $data['header_atas'] .= '<th style="text-align:center" colspan="2">#</th>';
         $data['header_atas'] .= '<th style="text-align:center" colspan="'.count($widgetgridfield).'">---</th>';
         $data['header_atas'] .= '<th colspan="1" style="text-align:center">---</th>';
       $data['header_atas'] .= '</tr>';
   }else{
       $datahead = array_count_values($data['colgroupname']);
             $data['header_atas'] = '<tr class="footter">';
             $data['header_atas'] .= '<th style="text-align:center" colspan="2">#</th>';
             foreach ($datahead as $key => $value) {
               $data['header_atas'] .= '<th style="text-align:center" colspan="'.$value.'">'.$key.'</th>';
             }
               $data['header_atas'] .= '<th colspan="1" style="text-align:center">---</th>';
             $data['header_atas'] .= '</tr>';
   }
   // dd($data['footter']);
   array_push($data['columns'],'Action');
        $columns =$data['columns'];
        $limit = $request->input('length');
             $start = $request->input('start');
             // $order = $columns[$request->input('order.0.column')];
             $dir = $request->input('order.0.dir');
             // dd($start);
             // dd($request->input());

   $columnIndex_arr = $request->get('order');
   // dd($columnIndex_arr);
   $columnName_arr = $request->get('columns');
   $order_arr = $request->get('order');
   $search_arr = $request->get('search');

   $columnIndex = $columnIndex_arr[0]['column']; // Column index
   $columnName = $columnName_arr[$columnIndex]['data']; // Column name
   $columnSortOrder = $order_arr[0]['dir']; // asc or desc
   $searchValue = $search_arr['value']; // Search value
   // dd($columnName);

        $TEXT_QUERY = '';
        $TEXT_QUERY2 = '';
        $TEXT_QUERY3 = '';
        $TEXT_QUERY_order = '';
        $TEXT_order = '';
        $BY_NYA = '';
        $SELECT_QUERY = "SELECT a.noid, ";
        $SELECT_QUERY3 = "SELECT ";
        $SELECT_QUERY2 = " ";
        $WHERE_QUERY = "FROM ".$table." a";
        $WHERE_QUERY4 = "FROM ".$table." a";
        $WHERE_QUERY3 = " ";
        $Join = " ";
        $TABEL_ALIAS = "a";


        foreach ($widgetgridfield2 as $key => $value) {
          // dd($value->fieldvalidation);
          if ($widgetgridfield2[$key]->coltypefield == 96) {
            $TABEL_ALIAS = chr(ord($TABEL_ALIAS) + 1);
              $koma = ( $key !== count( $widgetgridfield2 ) -1 ) ? "," : " ";
            $SELECT_QUERY .= "a.noid, a.".$value->fieldname." AS ".$value->fieldname.', ';
            // $TEXT_QUERY_order .= $value->fieldname.' ,';
            $isirelasi = str_replace(";"," ",$widgetgridfield2[$key]->tablelookup);
            // dd($widgetgridfield2[$key]->tablelookup);
            $ex = explode(' ', $isirelasi);
            if( strpos($ex[2], ",") !== false ) {
                $ex2 = explode(',', $ex[2]);
                  ${'FIELD_FOREIGN'.$key} =  $TABEL_ALIAS.'.'.$ex2[0].' , '.$TABEL_ALIAS.'.'.$ex2[1];
            }else{
                ${'FIELD_FOREIGN'.$key} = $TABEL_ALIAS.'.'.$ex[2];
            }
            ${'FIELD_NAME'.$key} = $ex[0];
            ${'FIELD_RELASI'.$key} = $ex[1];
            ${'TABLE_RELASI'.$key} = $ex[3];
            $SELECT_QUERY .= ' CONCAT('.${'FIELD_FOREIGN'.$key}.') as '.  ${'FIELD_NAME'.$key}.'_nama '.$koma;
            $TEXT_QUERY_order .=  ${'FIELD_NAME'.$key}.'_nama '.$koma;
            $WHERE_QUERY .= " LEFT JOIN ".${'TABLE_RELASI'.$key}." ".$TABEL_ALIAS." ON a.".  ${'FIELD_NAME'.$key}."=".$TABEL_ALIAS.".".${'FIELD_RELASI'.$key};


          }else{
            $koma = ( $key !== count( $widgetgridfield2 ) -1 ) ? "," : " ";
             $SELECT_QUERY2 .= "a.".$value->fieldname.$koma;
            $SELECT_QUERY .= "a.".$value->fieldname.$koma;
            $TEXT_QUERY_order .= "a.".$value->fieldname.$koma;
          }
        }

        // dd($fieldvalidations[ (int) $columnName ]);


        $BY_NYA = 'limit '.$limit.' offset '.$start.'  ';
        // $BY_NYA2 = ' order by a.noid '.$dir.' ';
        $BY_NYA2 = '  ';
        $TEXT_QUERY .= $SELECT_QUERY;
        $TEXT_QUERY .= $WHERE_QUERY.' where a.noid != 0 ';
        $WHERE_QUERY3 .= 'where a.noid != 0 ';

        $TEXT_QUERY3 .= $SELECT_QUERY2;
        $TEXT_QUERY2 .= $SELECT_QUERY;
        $TEXT_QUERY2 .= $WHERE_QUERY.' where a.noid != 0  ';


        // dd($TEXT_QUERY);




          if(empty($request->input('search.value'))   )
             {
               if ($columnName == 0) {
                 $TEXT_QUERY .=  $BY_NYA;
               }else{
                 $str = ltrim($TEXT_QUERY_order, "SELECT");
                 // dd($str);
                 $fieldvalidations = explode(',',$str);
                 $TEXT_order = ' order by '.$fieldvalidations[ (int) $columnName-2 ].'  '.$columnSortOrder.'  ';
                 $cek = $fieldvalidations;
                 $TEXT_QUERY .=  $TEXT_order;
                         $TEXT_QUERY .=  $BY_NYA;
               }


                  $sql =  DB::connection('mysql2')->select($TEXT_QUERY);
                  $sql_total =  DB::connection('mysql2')->select($TEXT_QUERY2);
                    $totalsql = count($sql_total);
                    // dd($TEXT_QUERY);
             }
             else {
               // dd($request->order);
                $orderby = $request->order;
                // dd($orderby);
                 $search = '%'.$request->input('search.value').'%';
                 $querylikes = array();
                 $str = ltrim($TEXT_QUERY3, "SELECT");
                 $fieldvalidations = explode(',',$str);
                 // dd($TEXT_QUERY2);
                   $where_baru = ' ';
                     $ref = 0;
                   foreach ($fieldvalidations as $key => $value) {
                        $koma = ( $key !== count( $fieldvalidations ) -1 ) ? " OR " : " ";
                     $where_baru .= $value.' LIKE "'.$search .'" '. $koma;
                   }
                     $TEXT_QUERY .= '  AND'.$where_baru;
                      $TEXT_QUERY .=$BY_NYA2;
                      // dd($TEXT_QUERY);

                 $sql =  DB::connection('mysql2')->select($TEXT_QUERY);
                 // dd($sql);
                  $totalsql = count($sql);

             }

             // dd($request->order);


             // dd($sql);



              $data['footter'] = '<tr>';
              $data['footter'] .= '<th style="text-align:right" colspan="4">'.$totalsql.'</th>';
              $data['footter'] .= '<th style="text-align:right" colspan="8"></th>';
              $data['footter'] .= '</tr>';
              // dd($start);


      // }

      if ($start == 0) {
                 $no = 1;
      }else{
        $no = $start+1;
      }
      // dd($sql);
        $data['data'] = array();
      foreach ($sql as $key => $value) {
        // dd($value);
        $row = array();
        array_push($row,'<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike">');
        array_push($row,$no++);
        // array_push($row,$value->noid);
        // dd($widgetgridfield2[$key]->coltypefield);
        // $string = (string)$widgetgridfield2[$key]->fieldname;
        //     array_push($row,$value->$string);

        for ($i=0; $i <count($widgetgridfield2) ; $i++) {
          if ($widgetgridfield2[$i]->coltypefield == 4) {
            // dd($widgetgridfield2[$i]->coltypefield);
            $string = (string)$widgetgridfield2[$i]->fieldname;
            $isiketrangan = \Illuminate\Support\Str::limit($value->$string, 100, $end='...');
            array_push($row,$isiketrangan);
          }else if ($widgetgridfield2[$i]->coltypefield == 96) {
              $string = (string)$widgetgridfield2[$i]->fieldname.'_nama';
              array_push($row,$value->$string);
          // }else if ($widgetgridfield2[$i]->coltypefield == 11) {
          //     $string = (string)$widgetgridfield2[$i]->fieldname;
          //   // dd($string);
          //     array_push($row,$value->idcompany_nama);
          }else if ($widgetgridfield2[$i]->coltypefield == 31) {
                $string = (string)$widgetgridfield2[$i]->fieldname;
              $check = '<input type="checkbox" checked id="'.$widgetgridfield2[$i]->fieldname.'_'.$value->$string.'" name="'.$widgetgridfield2[$i]->fieldname.'_'.$value->$string.'" value="'.$value->$string.'">';
              array_push($row,$check);
          }else{
            $string = (string)$widgetgridfield2[$i]->fieldname;
              array_push($row,$value->$string);
          }

        }
        array_push($row,'<div class="d-flex align-items-center">
        <a onclick="edit('.$value->noid.')" class="btn btn-warning btn-sm mr-2"><i class="fa fa-edit icon-xs" style="text-align:center"></i> </a>
        <a onclick="view('.$value->noid.')"  class="btn btn-success btn-sm mr-2"><i class="fa fa-search icon-xs"></i> </a>
        <a  onclick="hapus('.$value->noid.')" class="btn btn-danger btn-sm mr-2"><i class="fa fa-trash icon-xs"></i> </a>
        </div>');
          $data['data'][] = $row;
      }



         // array(
         //     "draw"            => intval( $request['draw'] ),
         //     "recordsTotal"    => intval( $recordsTotal ),
         //     "recordsFiltered" => intval( $recordsFiltered ),
         //     "data"            => $my_data
         // );
         $json_data = array(
                            "draw"            => intval($request['draw']),
                            // "draw"            => 1,
                            "recordsTotal"    => intval($totalsql),
                            "recordsFiltered"    => intval($totalsql),
                            // "recordsFiltered" => 10,
                            "data"            => $data['data'],
                            "filterButton" => $data['buttonfilter']
                          );

                            header("Content-type: application/json");
                            header("Cache-Control: no-cache, must-revalidate");
                            echo json_encode($json_data);



         // dd($data['data']);
             // $output = array(
             //     "recordsTotal" =>$totalData,
             //     "recordsFiltered" => $limit,
             //     "data" => $sql,
             // );

   // $datanyaa = array(
   //         'draw' => $draw,
   //         'recordsTotal' => $total_members,
   //         'recordsFiltered' => $total_members,
   //         'data' => $data['data'],
   //     );

       // echo json_encode($datanyaa);
   // echo json_encode($data);
 }

 public function ambildata_from(Request $request)
 {
   $kode = $request->get('table');
   $menu = Menu::where('noid',$kode)->first();
   $page = Page::where('noid',$menu->linkidpage)->first();
   $widget = Widget::where('kode', $kode)->first();

   $table = $widget->maintable;
   $widgetgrid = WidgetGrid::where('idcmswidget', $widget->noid)->first();
  // $widgetgridfield = WidgetGridField::where('idcmswidgetgrid', $widgetgrid->noid)->where('colshow',1)->orderBy('nourut','asc')->orderBy('noid','asc')->get();
   $widgetgridfield = WidgetGridField::where('idcmswidgetgrid', $widgetgrid->noid)->where('newshow',1)->orderBy('formurut','asc')->get();
   // foreach ($widgetgridfield as $key => $value) {
   //   $dd[] = $value->coltypefield;
   // }
   // dd($widgetgridfield);
   $isifield = '';
   foreach ($widgetgridfield as $key => $value) {
     if ($value->newreq == 1) {
       $required = 'required';
     }else{
       $required = '';
     }
     if ($value->coltypefield == 96) {
       $select = WidgetGridField::getField($value,$value->fieldname,$value->fieldcaption,$value->newreq);
       // dd($select);
       $isifield = $select;
     }elseif ($value->coltypefield == 1) {
       $isifield = '<input width="'.$value->colheight.'"  '.$required.' type="text" class="form-control form-control"
       id="'.$value->fieldname.'" name="'.$value->fieldname.'" placeholder="'.$value->fieldcaption.'">';
     }elseif ($value->coltypefield == 4 ) {
       $isifield = '<textarea width="'.$value->colheight.'" '.$required.' class="form-control form-control"  id="'.$value->fieldname.'" name="'.$value->fieldname.'" placeholder="'.$value->fieldcaption.'"></textarea>';
     }elseif ($value->coltypefield == 31 ) {
       $isifield = '<div class="checkbox-list">
           <label class="checkbox">
               <input width="'.$value->colheight.'" '.$required.' type="checkbox" id="'.$value->fieldname.'" name="'.$value->fieldname.'">
               <span></span>
               '.$value->fieldname.'
           </label>
           </div>';
     }elseif ($value->coltypefield == 41 ) {
       $isifield = '<div class="checkbox-list">
           <label class="checkbox">
               <input width="'.$value->colheight.'" '.$required.' type="checkbox" id="'.$value->fieldname.'" name="'.$value->fieldname.'">
               <span></span>
               '.$value->fieldname.'
           </label>
           </div>';
     }elseif ($value->coltypefield == 51  ) {
       $isifield = '<div class="checkbox-list">
           <label class="checkbox">
               <input width="'.$value->colheight.'" '.$required.' type="checkbox" id="'.$value->fieldname.'" name="'.$value->fieldname.'">
               <span></span>
               '.$value->fieldname.'
           </label>
           </div>';
     }elseif ($value->coltypefield == 12  ) {
       $mytime = \Carbon\Carbon::now()->format('d-m-Y');
       $isifield = '<div class="input-group date">
         <input type="text" '.$required.' width="'.$value->colheight.'" class="datepicker form-control" data-provide="datepicker" readonly="readonly" value="'.$mytime.'"  id="'.$value->fieldname.'" name="'.$value->fieldname.'">
         <div class="input-group-append">
           <span class="input-group-text">
             <i class="la la-calendar"></i>
           </span>
         </div>
       </div>';
     }elseif ($value->coltypefield == 21  ) {
       $mytime = \Carbon\Carbon::now()->format('d-m-Y H:i');
       $isifield = '<div class="input-group date" id="kt_datetimepicker_1" data-target-input="nearest">
        <input type="text" class="datepicker2 form-control datetimepicker-input" placeholder="Select date &amp; time" data-target="#kt_datetimepicker_1">
        <div class="input-group-append" data-target="#kt_datetimepicker_1" data-toggle="datetimepicker">
          <span class="input-group-text">
            <i class="ki ki-calendar"></i>
          </span>
        </div>
      </div>';
       // $isifield = '<div class="input-group date">
       //   <input type="text" '.$required.' class="datepicker form-control" data-provide="datepicker" readonly="readonly" value="'.$mytime.'"  id="'.$value->fieldname.'" name="'.$value->fieldname.'">
       //   <div class="input-group-append">
       //     <span class="input-group-text">
       //       <i class="la la-calendar"></i>
       //     </span>
       //   </div>
       // </div>';
     }else{
       $isifield = '';
     }
// dd($isifield);
     $data['field'][]= array(
       'label'=>$value->fieldcaption,
       'kode'=>$value->coltypefield,
       'field'=>$isifield,
     );
             // dd($data);
   }
   // dd($data['field']);

   header("Content-type: application/json");
   header("Cache-Control: no-cache, must-revalidate");
   echo json_encode($data);
 }

 public function savedata(Request $request)
 {
   $data = $request->all();
   // dd($kode);
   $menu = Menu::where('noid',$data['data']['tabel'])->first();
   // dd($menu);

   $page = Page::where('noid',$menu->linkidpage)->first();
   $panel = Panel::where('idpage',$page->noid)->first();
   $widget = Widget::where('noid', $panel->idwidget)->first();
   $filtertable = json_decode($widget->configwidget);
   $fieldtabel = $filtertable->WidgetFilter;
   $table = $widget->maintable;
   $data = $request->all();
   $datapost=  array();
   parse_str($data['data']['data'], $datapost);
   if ($datapost['noid'] == '') {
       $total = DB::connection('mysql2')->table($table)->orderBy('noid','desc')->first();
       foreach($datapost as $key => $value)
         {
           $datapost['noid'] = $total->noid+1;
         }
   $in =   DB::connection('mysql2')->table($table)->insert(
       $datapost
   );
   echo json_encode(array(
     'status'=>'add',
     'table'=>'Company',
     'success'=>true
     ));
   }else{
      $array = \array_diff($datapost, ["noid" => $datapost['noid']]);
     $total = DB::connection('mysql2')->table($table)->where('noid',$datapost['noid'])->update($array);
     echo json_encode(array(
       'status'=>'edit',
       'table'=>'Company',
       'success'=>true
       ));
   }
 }


     public function edit(Request $request)
     {
             $data = $request->all();
       $menu = Menu::where('noid',$data['namatable'])->first();

       $page = Page::where('noid',$menu->linkidpage)->first();
       $panel = Panel::where('idpage',$page->noid)->first();
       $widget = Widget::where('noid', $panel->idwidget)->first();
       $filtertable = json_decode($widget->configwidget);
       $fieldtabel = $filtertable->WidgetFilter;
       $table = $widget->maintable;
             $data = $request->all();
             $widget = Widget::where('maintable', $table)->first();
             $widgetgrid = WidgetGrid::where('idcmswidget', $widget->noid)->first();
             $widgetgridfield = WidgetGridField::where('idcmswidgetgrid', $widgetgrid->noid)->where('editshow',1)->get();
             $id = $data['id'];
             $datatab = DB::connection('mysql2')->table($table)->where('noid',$id)->first();
             $no = 1;

             foreach ($widgetgridfield as $key => $value) {
               $string = (string)$widgetgridfield[$key]->fieldname;
               $hasil[] = array(
                 $widgetgridfield[$key]->fieldname =>$datatab->$string,
               );
             }
             $hasil2[] = array(
               'noid' =>$id,
             );
             $hasilsemua = array_merge($hasil,$hasil2);
                       echo json_encode($hasilsemua);

     }

     public function hapus(Request $request)
     {
       $namatable = $_POST['namatable'];
       $menu = Menu::where('noid',$namatable)->first();
       $page = Page::where('noid',$menu->linkidpage)->first();
       $panel = Panel::where('idpage',$page->noid)->first();
       $widget = Widget::where('noid', $panel->idwidget)->first();
       $filtertable = json_decode($widget->configwidget);
       $fieldtabel = $filtertable->WidgetFilter;
       $table = $widget->maintable;
       // dd($_POST);
       $in =   DB::connection('mysql2')->table($table)->where('noid',$_POST['id'])->delete();
       echo json_encode('Berhasil');
     }




}
