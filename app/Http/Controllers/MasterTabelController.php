<?php

namespace App\Http\Controllers;

use App\Http\Controllers\lam_system\FrontPageController;
use App\Http\Controllers\lam_error\ErrorController;
use App\Http\Controllers\lam_custom\ErrorController as ErrorControllerFunc;
use App\Http\Controllers\lam_system\ErrorController as ErrorControllerFunc2;
Use DB;
use App\User;
use App\Menu;
use App\Page;
use App\Panel;
use App\Widget;
use App\WidgetGrid;
use App\WidgetGridField;
use App\Departement;
use App\Company;
use App\ViewGridField;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Schema;
use Carbon;
use ImageResize;

class MasterTabelController extends Controller {

    public static function index($id) {

        if (!Session::get('login')) {
            return view('pages.login');
        }
    
        $code = $id;
        $page = Page::where('noid',$id)->first();
        $breadcrumb = Menu::getBreadcrumb($id);
        $idhomepagelink = $id;
        $user = User::where('noid', Session::get('noid'))->first();
        $idhomepage =  $user->idhomepage;
        $noid = Session::get('noid');
        $myusername = Session::get('myusername');
        
        if ($page == null) {
            $page = Page::where('noid', 2)->first();
            $page_title = $page->pagetitle;
            $page_description = $page->pagetitle;
          
            return view('lam_custom.notfound', compact(
                'noid',
                'myusername',
                'idhomepage',
                'idhomepagelink',
                'page',
                'page_title',
                'page_description'
            ));
        } else {
            $panel = Panel::where('idpage',$page->noid)->first();
            $page_title = $page->pagetitle;
            $page_description = $page->pagetitle;
            $widgetgridfield = WidgetGridField::where('idcmswidgetgrid', $panel->idwidget)->where('colshow', 1)->get();
            $heightwgf = 35;
            foreach ($widgetgridfield as $k => $v) {
                if ($v->colheight > $heightwgf) {
                    $heightwgf = $v->colheight;
                }
            }
        }

        return view('lam_master_tabel.index', compact(
            'code',
            'noid',
            'myusername',
            'idhomepagelink',
            'page',
            'page_title',
            'idhomepage',
            'page_description',
            'breadcrumb',
            'heightwgf'
        ));


    }

    public function get_data_master_table(Request $request) {
        $code = $request->get('table');
        $menu = Menu::where('noid',$code)->first();
        $page = Page::where('noid',$menu->linkidpage)->first();
        $panel = Panel::where('idpage',$page->noid)->first();
        $widget = Widget::where('noid', $panel->idwidget)->first();
        $widgetgrid = WidgetGrid::where('idcmswidget', $widget->noid)->first();
        $widgetgridfield = WidgetGridField::where('idcmswidgetgrid', $widgetgrid->noid)->where('colshow',1)->orderBy('nourut','asc')->orderBy('noid','asc')->get();
        $widgetgridfield2 = WidgetGridField::where('idcmswidgetgrid', $widgetgrid->noid)->orderBy('formurut','asc')->orderBy('noid','asc')->get();
        $widgetgridfield3 = WidgetGridField::where('idcmswidgetgrid', $widgetgrid->noid)->where('newshow',1)->orderBy('nourut','asc')->orderBy('noid','asc')->get();
        $widgetgridfield4 = WidgetGridField::where('idcmswidgetgrid', $widgetgrid->noid)->where('editshow',1)->orderBy('nourut','asc')->orderBy('noid','asc')->get();
        
        $response = [
            'menu' => $menu,
            'page' => $page,
            'panel' => $panel,
            'widget' => $widget,
            'widgetgrid' => $widgetgrid,
            'widgetgridfield' => $widgetgridfield,
            'widgetgridfield2' => $widgetgridfield2,
            'widgetgridfield3' => $widgetgridfield3,
            'widgetgridfield4' => $widgetgridfield4,
        ];
        echo json_encode($response);
    }
    
    public function get_master_table(Request $request) {
        $cek = '';
        $maincms = json_decode($request->get('maincms'));
        $kode = $request->get('table');

        $menu = $maincms->menu;
        $page = $maincms->page;
        $panel = $maincms->panel;
        $widget = $maincms->widget;
        $filtertable = json_decode($widget->configwidget);
        $fieldtabel = $filtertable->WidgetFilter;
        $table = $widget->maintable;
        $widgetgrid = $maincms->widgetgrid;
        $widgetgridfield = $maincms->widgetgridfield;
        $widgetgridfield_wherecolsearch = array_values(array_filter($maincms->widgetgridfield, function($v, $k) {
            if ($v->colsearch == 1) {
                return $v;
            }
        }, ARRAY_FILTER_USE_BOTH));

        $data['buttonfilter'] = [];
        
        $cek = json_decode($widget->configwidget);
        foreach ($fieldtabel as $key => $value) {
            $data['buttonfilter'][$key] = [
                'groupcaption' => $value->groupcaption,
                'groupcaptionshow' => $value->groupcaptionshow,
                'groupdropdown' => $value->groupdropdown,
                'data' => []
            ];
            foreach ($value->button as $key2 => $value2) {
                $pecah = explode('.', $value2->condition[0]->querywhere[0]->fieldname);
                $oncl = 'onclick="filterData(\'btn-filter-'.$value2->buttoncaption.'\', \'btn-filter-'.$key.'\', '.$key.', \''.$value2->buttoncaption.'\', \''.$value2->classcolor.'\', '.$value->groupdropdown.', false, \''.$table.'\', \''.$value2->condition[0]->querywhere[0]->operand.'\', \''.$pecah[1].'\', \''.$value2->condition[0]->querywhere[0]->operator.'\', \''.$value2->condition[0]->querywhere[0]->value.'\')" ';
                $data['buttonfilter'][$key]['data'][] = array(
                    'nama' => $value2->buttoncaption,
                    'buttonactive' => $value2->buttonactive,
                    'taga' => '<button class="btn btn-outline-secondary btn-sm flat btn-filter-'.$key.'" id="btn-filter-'.$value2->buttoncaption.'" style="background-color:#E3F2FD;" '.$oncl.'>'.$value2->buttoncaption.' </button>',
                    'operand' => $value2->condition[0]->querywhere[0]->operand,
                    'fieldname' => $pecah[1],
                    'operator' => $value2->condition[0]->querywhere[0]->operator,
                    'value' => $value2->condition[0]->querywhere[0]->value,
                );
            }
        }

        $data['columns'] = array('<input type="checkbox" onchange="changeCheckbox()" class="checkbox checkbox'.@$key.'" name="checkbox[]" value="FALSE">','No');
        $data['columns2'] = array();
        $data['width'] = array();
        $data['colheaderalign'] = array();
        $data['coltypefield'] = array();
        $data['colgroupname'] = array();
        $data['colheaderalign'] = array();
        $data['coldefault'] = array();
        $data['colsearch'] = array();

        // for ($i=0; $i <count($widgetgridfield) ; $i++) {
        //     $data['columns2'][] =  $widgetgridfield[$i]->fieldname;
        // }

        foreach ($widgetgridfield_wherecolsearch as $k => $v) {
            $data['columns2'][] =  $widgetgridfield[$k]->fieldname;
        }

        for ($i=0; $i <count($widgetgridfield) ; $i++) {
            array_push($data['columns'], $widgetgridfield[$i]->fieldcaption);
            array_push($data['width'], $widgetgridfield[$i]->colwidth.'%');
            array_push($data['colheaderalign'], $widgetgridfield[$i]->colheaderalign);
            array_push($data['coltypefield'], $widgetgridfield[$i]->coltypefield);
            array_push($data['colgroupname'], $widgetgridfield[$i]->colgroupname);
            array_push($data['coldefault'], $widgetgridfield[$i]->coldefault);
            array_push($data['colsearch'], $widgetgridfield[$i]->colsearch);
        }

        if (!empty($data['colgroupname'])) {
            $data['header_atas'] = '<tr class="atas" role="group-header">';
            $data['header_atas'] .= '<th style="text-align:center" colspan="2">#</th>';
            $data['header_atas'] .= '<th style="text-align:center" colspan="'.count($widgetgridfield).'">---</th>';
            $data['header_atas'] .= '<th colspan="1" style="text-align:center">---</th>';
            $data['header_atas'] .= '</tr>';
        } else {
            $datahead = array_count_values($data['colgroupname']);
            $data['header_atas'] = '<tr class="footter">';
            $data['header_atas'] .= '<th style="text-align:center" colspan="2">#</th>';
            foreach ($datahead as $key => $value) {
                $data['header_atas'] .= '<th style="text-align:center" colspan="'.$value.'">'.$key.'</th>';
            }
            $data['header_atas'] .= '<th colspan="1" style="text-align:center">---</th>';
            $data['header_atas'] .= '</tr>';
        }
        array_push($data['columns'],'Action');
        $columns =$data['columns'];
        $limit = $request->input('length');
        $start = $request->input('start');
        // $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        // dd($start);
        // dd($request->input());

        $columnIndex_arr = $request->get('order');
        // dd($columnIndex_arr);
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value
        // dd($columnName);
        



        //--------------------------------------- MAKE QUERY
        $select = '';
        $from = '';
        $join = '';
        $where = '1=1 ';
        $limit = $limit;
        $offset = $start;
        $order_by = 'ORDER BY ';
        $uniquetable = [];
        foreach ($widgetgridfield as $k => $v) {
            if ($k == 0) {
                $select .= "$v->tablename.noid AS noid,";
            }
            $tablelookup = explode(';', $v->tablelookup);
            if ($v->tablelookup) {
                $select .= "$tablelookup[3].$tablelookup[2] AS $tablelookup[3]_$tablelookup[2],";
                if ($tablelookup[3] != $v->tablename) {
                    if (!in_array($tablelookup[3], $uniquetable)) {
                        $uniquetable[] = $tablelookup[3];
                        $join .= "LEFT JOIN $tablelookup[3] ON $v->tablename.$tablelookup[0] = $tablelookup[3].$tablelookup[1] ";
                    }
                }
                if ($request->get('order')[0]['column'] != 0 && $k == $request->get('order')[0]['column']-2) {
                    $order_by .= "$tablelookup[3]_$tablelookup[2] ".$request->get('order')[0]['dir'];
                }
            } else {
                $select .= "$v->tablename.$v->fieldsource AS $v->tablename"."_"."$v->fieldsource,";
                if ($request->get('order')[0]['column'] != 0 && $k == $request->get('order')[0]['column']-2) {
                    $order_by .= $v->tablename."_"."$v->fieldsource ".$request->get('order')[0]['dir'];
                }
            }
            $from = $v->tablename;
        }
        $select = substr($select, 0, -1);
        if ($order_by == 'ORDER BY ') {
            $order_by = '';
        }
        if (@$_POST['selected_filter']) {
            for ($i=0; $i<count($_POST['selected_filter']['operand']); $i++) {
                if ($_POST['selected_filter']['operand'][$i] != '') {
                    $where .= $_POST['selected_filter']['operand'][$i].' '.$from.'.'.$_POST['selected_filter']['fieldname'][$i].' '.$_POST['selected_filter']['operator'][$i].' "'.$_POST['selected_filter']['value'][$i].'" ';
                }
            }
        }
        // dd($where);
        // print_r($where);return false;
        $like = '';
        if (!empty($request->input('search.value'))) {
            $like .= 'AND ';
            foreach (explode(',', $select) as $k => $v) {
                $fieldsource = explode(' ', $v);
                // if ($k == 0) {
                    $like .= $fieldsource[0]." LIKE '%".$request->input('search.value')."%' OR ";
                // } else {
                //     $like .= $fieldsource[2]." LIKE '%".$request->input('search.value')."%' OR ";
                // }
            }
            $like = substr($like, 0, -3);
        }
        
        $query = "SELECT $select FROM $from $join WHERE $where $like $order_by LIMIT $limit OFFSET $offset";
        $query_total = "SELECT $select FROM $from $join WHERE $where $like $order_by";

        $result = DB::connection('mysql2')->select($query);
        $result_total = count(DB::connection('mysql2')->select($query_total));
        // $cek = $result;

        
        $data['footter'] = '<tr>';
        $data['footter'] .= '<th style="text-align:right" colspan="4">'.count($result).'</th>';
        $data['footter'] .= '<th style="text-align:right" colspan="8"></th>';
        $data['footter'] .= '</tr>';
              // dd($start);
        // }

        if ($start == 0) {
            $no = 1;
        } else {
            $no = $start+1;
        }
        // dd($sql);
        $data['data'] = array();
        foreach ($result as $key => $value) {
            // dd($value);
            $row = array();
            array_push($row,'<input type="checkbox" onchange="changeCheckbox('.$key.', '.$value->noid.')" class="checkbox checkbox'.$key.'" name="checkbox[]" value="FALSE">');
            array_push($row,$no++);
            // array_push($row,$value->noid);
            // dd($widgetgridfield[$key]->coltypefield);
            // $string = (string)$widgetgridfield[$key]->fieldname;
            //     array_push($row,$value->$string);

            for ($i=0; $i <count($widgetgridfield) ; $i++) {
                $tablename = $widgetgridfield[$i]->tablename;
                $valuecolumn = '';
                $tablelookup = explode(';', $widgetgridfield[$i]->tablelookup);
                if ($widgetgridfield[$i]->tablelookup) {
                    $fs = $tablelookup[3].'_'.$tablelookup[2];
                    $valuecolumn = $value->$fs;
                } else {
                    $fs = $tablename.'_'.$widgetgridfield[$i]->fieldsource;
                    $valuecolumn = $value->$fs;
                }

                if ($widgetgridfield[$i]->coltypefield == 4) {
                    // dd($widgetgridfield[$i]->coltypefield);
                    // $string = (string)$widgetgridfield[$i]->fieldname;
                    // $isiketrangan = \Illuminate\Support\Str::limit($value->$string, 100, $end='...');
                    array_push($row, '<div class="text-'.$widgetgridfield[$i]->colalign.'">'.$valuecolumn.'</div>');
                } else if ($widgetgridfield[$i]->coltypefield == 96) {
                    // $string2 = (string)$widgetgridfield[$i]->fieldname;
                    array_push($row, '<div class="text-'.$widgetgridfield[$i]->colalign.'">'.$valuecolumn.'</div>');
                    // array_push($row, '<div class="text-'.$widgetgridfield[$i]->colalign.'">'.$value->$string2.'</div>');
                    // }else if ($widgetgridfield[$i]->coltypefield == 11) {
                    //     $string = (string)$widgetgridfield[$i]->fieldname;
                    //   // dd($string);
                    //     array_push($row,$value->idcompany_nama);
                } else if ($widgetgridfield[$i]->coltypefield == 31) {
                    // $string = (string)$widgetgridfield[$i]->fieldname;
                    
                    $checked = $valuecolumn == '1' ? 'checked' : '';
                    $check = '<div class="text-'.$widgetgridfield[$i]->colalign.'">
                                <input type="checkbox" '.$checked.' id="'.$widgetgridfield[$i]->fieldname.'_'.$valuecolumn.'" name="'.$widgetgridfield[$i]->fieldname.'_'.$valuecolumn.'" value="'.$valuecolumn.'">';
                            '</div>';
                    array_push($row,$check);
                } else {
                    array_push($row, '<div class="text-'.$widgetgridfield[$i]->colalign.'">'.$valuecolumn.'</div>');
                }
            }
            array_push($row,'<div class="d-flex align-items-center button-action button-action-'.$key.'">
            <a onclick="edit('.$value->noid.')" class="btn btn-warning btn-sm flat mr-2"><i class="fa fa-edit icon-xs" style="text-align:center"></i> </a>
            <a onclick="view('.$value->noid.')"  class="btn btn-success btn-sm flat mr-2"><i class="fa fa-search icon-xs"></i> </a>
            <a  onclick="remove('.$value->noid.')" class="btn btn-danger btn-sm flat mr-2"><i class="fa fa-trash icon-xs"></i> </a>
            </div>');
            $data['data'][] = $row;
        }
        // print_r($data);
        // return false;
        //--------------------------------------- END MAKE QUERY



                                            // $TEXT_QUERY = '';
                                            // $TEXT_QUERY2 = '';
                                            // $TEXT_QUERY3 = '';
                                            // $TEXT_QUERY_order = '';
                                            // $TEXT_order = '';
                                            // $BY_NYA = '';
                                            // $SELECT_QUERY = "SELECT a.noid, ";
                                            // $SELECT_QUERY3 = "SELECT ";
                                            // $SELECT_QUERY2 = " ";
                                            // $WHERE_QUERY = "FROM ".$table." a";
                                            // $WHERE_QUERY4 = "FROM ".$table." a";
                                            // $WHERE_QUERY3 = " ";
                                            // $Join = " ";
                                            // $TABEL_ALIAS = "a";

                                            // // $cek = $widgetgridfield;
                                            // foreach ($widgetgridfield as $key => $value) {
                                            //     // dd($value->fieldvalidation);
                                            //     if ($widgetgridfield[$key]->coltypefield == 96) {
                                            //         $TABEL_ALIAS = chr(ord($TABEL_ALIAS) + 1);
                                            //         $koma = ( $key !== count( $widgetgridfield ) -1 ) ? "," : " ";
                                            //         $SELECT_QUERY .= "a.noid, a.".$value->fieldsource." AS ".$value->fieldsource.', ';
                                            //         // $TEXT_QUERY_order .= $value->fieldname.' ,';
                                            //         $isirelasi = str_replace(";"," ",$widgetgridfield[$key]->tablelookup);
                                            //         // dd($widgetgridfield[$key]->tablelookup);
                                            //         $ex = explode(' ', $isirelasi);
                                            //         if ( strpos($ex[2], ",") !== false ) {
                                            //             $ex2 = explode(',', $ex[2]);
                                            //             ${'FIELD_FOREIGN'.$key} =  $TABEL_ALIAS.'.'.$ex2[0].' , '.$TABEL_ALIAS.'.'.$ex2[1];
                                            //         } else {
                                            //             ${'FIELD_FOREIGN'.$key} = $TABEL_ALIAS.'.'.$ex[2];
                                            //         }
                                            //         ${'FIELD_NAME'.$key} = $ex[0];
                                            //         ${'FIELD_RELASI'.$key} = $ex[1];
                                            //         ${'TABLE_RELASI'.$key} = $ex[3];
                                            //         $SELECT_QUERY .= ' CONCAT('.${'FIELD_FOREIGN'.$key}.') as '.  ${'FIELD_NAME'.$key}.'_nama '.$koma;
                                            //         $TEXT_QUERY_order .=  ${'FIELD_NAME'.$key}.'_nama '.$koma;
                                            //         $WHERE_QUERY .= " LEFT JOIN ".${'TABLE_RELASI'.$key}." ".$TABEL_ALIAS." ON a.".  ${'FIELD_NAME'.$key}."=".$TABEL_ALIAS.".".${'FIELD_RELASI'.$key};
                                            //     } else {
                                            //         $koma = ( $key !== count( $widgetgridfield ) -1 ) ? "," : " ";
                                            //         $SELECT_QUERY2 .= "a.".$value->fieldsource.$koma;
                                            //         $SELECT_QUERY .= "a.".$value->fieldsource.$koma;
                                            //         $TEXT_QUERY_order .= "a.".$value->fieldsource.$koma;
                                            //     }
                                            // }

                                            // // dd($fieldvalidations[ (int) $columnName ]);

                                            // $BY_NYA = 'limit '.$limit.' offset '.$start.'  ';
                                            // // $BY_NYA2 = ' order by a.noid '.$dir.' ';
                                            // $BY_NYA2 = '  ';
                                            // $TEXT_QUERY .= $SELECT_QUERY;
                                            // $TEXT_QUERY .= $WHERE_QUERY.' where a.noid != 0 ';
                                            // $WHERE_QUERY3 .= 'where a.noid != 0 ';

                                            // $TEXT_QUERY3 .= $SELECT_QUERY2;
                                            // $TEXT_QUERY2 .= $SELECT_QUERY;
                                            // $TEXT_QUERY2 .= $WHERE_QUERY.' where a.noid != 0  ';
                                            // // dd($TEXT_QUERY);

                                            // if (@$_POST['selected_filter']) {
                                            //     for ($i=0; $i<count($_POST['selected_filter']['operand']); $i++) {
                                            //         if ($_POST['selected_filter']['operand'][$i] != '') {
                                            //             $TEXT_QUERY .= $_POST['selected_filter']['operand'][$i].' '.$_POST['selected_filter']['fieldname'][$i].' '.$_POST['selected_filter']['operator'][$i].' "'.$_POST['selected_filter']['value'][$i].'" ';
                                            //         }
                                            //     }
                                            // }

                                            // // $cek = $TEXT_QUERY;
                                            // if (empty($request->input('search.value'))) {
                                            //     if ($columnName == 0) {
                                            //         $TEXT_QUERY .=  $BY_NYA;
                                            //     } else {
                                            //         $str = ltrim($TEXT_QUERY_order, "SELECT");
                                            //         // dd($str);
                                            //         $fieldvalidations = explode(',',$str);
                                            //         $TEXT_order = ' order by '.$fieldvalidations[ (int) $columnName-2 ].'  '.$columnSortOrder.'  ';
                                            //         // $cek = $fieldvalidations;
                                            //         $TEXT_QUERY .=  $TEXT_order;
                                            //         $TEXT_QUERY .=  $BY_NYA;
                                            //     }
                                                
                                            //     $sql =  DB::connection('mysql2')->select($TEXT_QUERY);
                                            //     $sql_total =  DB::connection('mysql2')->select($TEXT_QUERY2);
                                            //     $totalsql = count($sql_total);
                                            //     // dd($TEXT_QUERY);
                                            // } else {
                                            //     // dd($request->order);
                                            //     $orderby = $request->order;
                                            //     // dd($orderby);
                                            //     $search = '%'.$request->input('search.value').'%';
                                            //     $querylikes = array();
                                            //     $str = ltrim($TEXT_QUERY3, "SELECT");
                                            //     $fieldvalidations = explode(',',$str);
                                            //     // dd($TEXT_QUERY2);
                                            //     $where_baru = ' ';
                                            //     $ref = 0;
                                            //     foreach ($fieldvalidations as $key => $value) {
                                            //         $koma = ( $key !== count( $fieldvalidations ) -1 ) ? " OR " : " ";
                                            //         $where_baru .= $value.' LIKE "'.$search .'" '. $koma;
                                            //     }
                                            //     $TEXT_QUERY .= '  AND'.$where_baru;
                                            //     $TEXT_QUERY .=$BY_NYA2;
                                            //     // dd($TEXT_QUERY);

                                            //     $sql =  DB::connection('mysql2')->select($TEXT_QUERY);
                                            //     // dd($sql);
                                            //     $totalsql = count($sql);
                                            // }
                                            // // dd($request->order);
                                            // // dd($sql);

                                            // $data['footter'] = '<tr>';
                                            // $data['footter'] .= '<th style="text-align:right" colspan="4">'.$totalsql.'</th>';
                                            // $data['footter'] .= '<th style="text-align:right" colspan="8"></th>';
                                            // $data['footter'] .= '</tr>';
                                            //     // dd($start);
                                            // // }

                                            // if ($start == 0) {
                                            //     $no = 1;
                                            // } else {
                                            //     $no = $start+1;
                                            // }
                                            // // dd($sql);
                                            // $data['data'] = array();
                                            // foreach ($sql as $key => $value) {
                                            //     // dd($value);
                                            //     $row = array();
                                            //     array_push($row,'<input type="checkbox" onchange="changeCheckbox('.$key.', '.$value->noid.')" class="checkbox checkbox'.$key.'" name="checkbox[]" value="FALSE">');
                                            //     array_push($row,$no++);
                                            //     // array_push($row,$value->noid);
                                            //     // dd($widgetgridfield[$key]->coltypefield);
                                            //     // $string = (string)$widgetgridfield[$key]->fieldname;
                                            //     //     array_push($row,$value->$string);

                                            //     for ($i=0; $i <count($widgetgridfield) ; $i++) {
                                            //         if ($widgetgridfield[$i]->coltypefield == 4) {
                                            //             // dd($widgetgridfield[$i]->coltypefield);
                                            //             $string = (string)$widgetgridfield[$i]->fieldname;
                                            //             $isiketrangan = \Illuminate\Support\Str::limit($value->$string, 100, $end='...');
                                            //             array_push($row, '<div class="text-'.$widgetgridfield[$i]->colalign.'">'.$isiketrangan.'</div>');
                                            //         } else if ($widgetgridfield[$i]->coltypefield == 96) {
                                            //             $string = (string)$widgetgridfield[$i]->fieldname.'_nama';
                                            //             array_push($row, '<div class="text-'.$widgetgridfield[$i]->colalign.'">'.$value->$string.'</div>');
                                            //             // }else if ($widgetgridfield[$i]->coltypefield == 11) {
                                            //             //     $string = (string)$widgetgridfield[$i]->fieldname;
                                            //             //   // dd($string);
                                            //             //     array_push($row,$value->idcompany_nama);
                                            //         } else if ($widgetgridfield[$i]->coltypefield == 31) {
                                            //             $string = (string)$widgetgridfield[$i]->fieldname;
                                            //             $check = '<div class="text-'.$widgetgridfield[$i]->colalign.'">
                                            //                         <input type="checkbox" checked id="'.$widgetgridfield[$i]->fieldname.'_'.$value->$string.'" name="'.$widgetgridfield[$i]->fieldname.'_'.$value->$string.'" value="'.$value->$string.'">';
                                            //                     '</div>';
                                            //             array_push($row,$check);
                                            //         } else {
                                            //             $string = (string)$widgetgridfield[$i]->fieldname;
                                            //             array_push($row, '<div class="text-'.$widgetgridfield[$i]->colalign.'">'.$value->$string.'</div>');
                                            //         }
                                            //     }
                                            //     array_push($row,'<div class="d-flex align-items-center button-action button-action-'.$key.'">
                                            //     <a onclick="edit('.$value->noid.')" class="btn btn-warning btn-sm flat mr-2"><i class="fa fa-edit icon-xs" style="text-align:center"></i> </a>
                                            //     <a onclick="view('.$value->noid.')"  class="btn btn-success btn-sm flat mr-2"><i class="fa fa-search icon-xs"></i> </a>
                                            //     <a  onclick="remove('.$value->noid.')" class="btn btn-danger btn-sm flat mr-2"><i class="fa fa-trash icon-xs"></i> </a>
                                            //     </div>');
                                            //     $data['data'][] = $row;
                                            // }

        // array(
        //     "draw"            => intval( $request['draw'] ),
        //     "recordsTotal"    => intval( $recordsTotal ),
        //     "recordsFiltered" => intval( $recordsFiltered ),
        //     "data"            => $my_data
        // );
        $json_data = array(
            "draw" => intval($request['draw']),
            "recordsTotal" => $result_total,
            "recordsFiltered" => $result_total,
            "data" => $data['data'],
            // "query" => $query,
            // "cek" => json_decode($request->get('maincms')),
            // "widgetgridfield" => $widgetgridfield,
            // "widgetgridfield_wherecolsearch" => $widgetgridfield_wherecolsearch,
        );

        header("Content-type: application/json");
        header("Cache-Control: no-cache, must-revalidate");
        echo json_encode($json_data);
    }

    public function get_filter_button_master_table(Request $request) {
        $kode = $request->get('table');
        $maincms = $request->get('maincms');
        // dd($kode);
        $menu = $maincms['menu'];

        $page = $maincms['page'];
        $panel = $maincms['panel'];
        $widget = $maincms['widget'];
        $table = $widget['maintable'];
        $filtertable = json_decode($widget['configwidget']);
        $fieldtabel = $filtertable->WidgetFilter;
        
        $data['buttonfilter'] = [];
        $data['buttonactive'] = [];

        foreach ($fieldtabel as $k => $v) {
            $data['buttonactive'][$k] = ''; 
            $data['buttonfilter'][$k] = [
                'groupcaption' => $v->groupcaption,
                'groupcaptionshow' => $v->groupcaptionshow,
                'groupdropdown' => $v->groupdropdown,
                'data' => []
            ];
            foreach ($v->button as $k2 => $v2) {
                $icon = '';
                if ($v2->classicon != '') {
                    $icon = '<i class="fa '.$v2->classicon.'"></i> ';
                }
                if ($v2->buttonactive) {
                    $data['buttonactive'][$k] = $v2->buttoncaption; 
                }
                $pecah = explode('.', $v2->condition[0]->querywhere[0]->fieldname);
                $oncl = 'onclick="filterData(\'btn-filter-'.$v2->buttoncaption.'\', \'btn-filter-'.$k.'\', '.$k.', \''.$v2->buttoncaption.'\', \''.$v2->classcolor.'\', '.$v->groupdropdown.', false, \''.$table.'\', \''.$v2->condition[0]->querywhere[0]->operand.'\', \''.$pecah[1].'\', \''.$v2->condition[0]->querywhere[0]->operator.'\', \''.$v2->condition[0]->querywhere[0]->value.'\')" ';
                $button = '<button class="btn btn-outline-secondary btn-sm flat btn-filter-'.$k.'" id="btn-filter-'.$v2->buttoncaption.'" style="background-color:#E3F2FD; color: '.$v2->classcolor.'"'.$oncl.'>'.$icon.$v2->buttoncaption.' </button>';
                if ($v->groupdropdown == 1) {
                    $button = '<button class="flat dropdown-item" id="btn-filter-'.$v2->buttoncaption.'" '.$oncl.'>'.$icon.$v2->buttoncaption.' </button>';
                }
                $data['buttonfilter'][$k]['data'][] = array(
                    'nama' => $v2->buttoncaption,
                    'buttonactive' => $v2->buttonactive,
                    'taga' => $button,
                    'operand' => $v2->condition[0]->querywhere[0]->operand,
                    'fieldname' => $pecah[1],
                    'operator' => $v2->condition[0]->querywhere[0]->operator,
                    'value' => $v2->condition[0]->querywhere[0]->value,
                );
            }
        }

        
        $json_data = array(
            "filterButton" => $data['buttonfilter'],
            // "activeButton" => $data['buttonactive'],
            "configWidget" => $filtertable,
        );

        header("Content-type: application/json");
        header("Cache-Control: no-cache, must-revalidate");
        echo json_encode($json_data);
    }
    
    public function get_form_master_table(Request $request) {
        $cek = '';
        $maincms = json_decode($request->get('maincms'));
        $isadd = $request->get('isadd');
        $code = $request->get('table');
        $widget = $maincms->widget;
        $table = $widget->maintable;
        $widgetgrid = $maincms->widgetgrid;

        if ($isadd) {
            $maincms_widgetgridfield = $maincms->widgetgridfield3;
        } else {
            $maincms_widgetgridfield = $maincms->widgetgridfield4;
        }

        $widgetgridfield = array_values(array_filter($maincms_widgetgridfield, function($v, $k) {
            if ($v->newshow == 1) {
                return $v;
            }
        }, ARRAY_FILTER_USE_BOTH));;
        // $widgetgridfield = WidgetGridField::where('idcmswidgetgrid', $widgetgrid->noid)->where('newshow',1)->orderBy('formurut','asc')->get();
        $forminput = '';

        foreach ($widgetgridfield as $k => $v) {
            if ($v->tablelookup) {
                $tabledetail = explode(';', $v->tablelookup)[3];
            } else {
                $tabledetail = '';
            }

            if ($v->newreq == 1) {
                $required = 'required';
            } else {
                $required = '';
            }

            if ($v->newreadonly == 1) {
                $disabled = 'disabled';
            } else {
                $disabled = '';
            }

            // if ($request->get('isadd') == 'true') {
                if ($v->newreq == 1) {
                    $required = 'required';
                    $formcaption = $v->formcaption.' <span class="text-danger">*</span>';
                } else {
                    $required = '';
                    $formcaption = $v->formcaption;
                }
    
                if ($v->newreadonly == 1) {
                    $disabled = 'readonly';
                    $cssreadonly = 'background-color: #E5E5E5 !important;';
                } else {
                    $disabled = '';
                    $cssreadonly = '';
                }
            // } else {
            //     if ($v->editreq == 1) {
            //         $required = 'required';
            //         $formcaption = $v->formcaption.' <span class="text-danger">*</span>';
            //     } else {
            //         $required = '';
            //         $formcaption = $v->formcaption;
            //     }
    
            //     if ($v->editreadonly == 1) {
            //         $disabled = 'readonly';
            //         $cssreadonly = 'background-color: #E5E5E5 !important;';
            //     } else {
            //         $disabled = '';
            //         $cssreadonly = '';
            //     }
            // }

            if ($v->formcaptionpos == 'TOP') {
                $row = '';
                $endrow = '';
                $collabel = '';
                $colinput = '';
            } elseif ($v->formcaptionpos == 'LEFT') {
                $row = '<div class="row">';
                $endrow = '</div>';
                $collabel = 'col-md-3';
                $colinput = 'col-md-9';
            }

            if ($v->formalign == 'LEFT') {
                $formalign = 'text-left';
            } elseif ($v->formalign == 'CENTER') {
                $formalign = 'text-center';
            } elseif ($v->formalign == 'RIGHT') {
                $formalign = 'text-right';
            } else {
                $formalign = '';
            }

            if($v->maxlength != 0) {
                $maxlength = 'maxlength="'.$v->maxlength.'"';
            } else {
                $maxlength = '';
            }
            
            if ($v->onchange) {
                $onchange = $v->onchange;
            } else {
                $onchange = '';
            }
            
            $datanewreq = 'data-'.$v->tablename.'-newreq="'.$v->newreq.'"';
            $dataeditreq = 'data-'.$v->tablename.'-editreq="'.$v->editreq.'"';
            $dataformcaption = 'data-'.$v->tablename.'-formcaption="'.$v->formcaption.'"';

            if ($v->formtypefield == 1) {
                $forminput = '<div id="div-'.$v->fieldname.'" class="col-md-'.$v->formwidth.' col-xs-'.$v->formwidthxs.' '.$v->formpull.'" title="'.$v->formhint.'">
                                <div class="form-group">
                                    '.$row.'
                                        <label class="control-label '.$collabel.'">'.$formcaption.'</label>
                                        <div class="'.$colinput.'">
                                            <input name="'.$v->fieldname.'" class="form-control '.$formalign.'" style="height: '.$v->formheight.'px; '.$cssreadonly.'" type="text" id="'.$v->tablename.'-'.$v->fieldname.'" value="'.$v->coldefault.'" placeholder="'.$v->formplaceholder.'" '.$maxlength.' '.$dataformcaption.' '.$datanewreq.' '.$dataeditreq.' '.$required.' '.$disabled.'>
                                            <span class="help-block"></span>
                                        </div>
                                    '.$endrow.'
                                </div>
                            </div><br>';
            } elseif ($v->formtypefield == 4 ) {
                $forminput = '<div id="div-'.$v->fieldname.'" class="col-md-'.$v->formwidth.' col-xs-'.$v->formwidthxs.' '.$v->formpull.'" title="'.$v->formhint.'">
                                <div class="form-group">
                                    '.$row.'
                                        <label class="control-label '.$collabel.'">'.$formcaption.'</label>
                                        <div class="'.$colinput.'">
                                            <textarea class="form-control '.$formalign.'" style="height: '.$v->formheight.'px; '.$cssreadonly.'" id="'.$v->tablename.'-'.$v->fieldname.'" name="'.$v->fieldname.'" value="'.$v->coldefault.'" placeholder="'.$v->formplaceholder.'" '.$maxlength.' '.$dataformcaption.' '.$datanewreq.' '.$dataeditreq.' '.$required.' '.$disabled.'></textarea>
                                        </div>
                                    '.$endrow.'
                                </div>
                            </div><br>';
            } elseif ($v->formtypefield == 7) {
                $forminput = '<div id="div-'.$v->fieldname.'" class="col-md-'.$v->formwidth.' col-xs-'.$v->formwidthxs.' '.$v->formpull.'" title="'.$v->formhint.'">
                                <div class="form-group">
                                    '.$row.'
                                        <label class="control-label '.$collabel.'">'.$formcaption.'</label>
                                        <div class="'.$colinput.'">
                                            <input type="password" class="form-control '.$formalign.'" style="height: '.$v->formheight.'px; '.$cssreadonly.'" id="'.$v->tablename.'-'.$v->fieldname.'" value="'.$v->coldefault.'" name="'.$v->fieldname.'" placeholder="'.$v->formplaceholder.'" '.$maxlength.' '.$dataformcaption.' '.$datanewreq.' '.$dataeditreq.' '.$required.' '.$disabled.'>
                                        </div>
                                    '.$endrow.'
                                </div>
                            </div><br>';
            } elseif ($v->formtypefield == 11) {
                $colformat = \Carbon\Carbon::now()->format($v->colformat);
                if ($v->coldefault != null) {
                    $colformat = $v->coldefault;
                }
                $placeholder = 'Select date &amp; time';

                $forminput = '<div id="div-'.$v->fieldname.'" class="col-md-'.$v->formwidth.' col-xs-'.$v->formwidthxs.' '.$v->formpull.'" title="'.$v->formhint.'">
                                <div class="input-group date" id="kt_datetimepicker_1" data-target-input="nearest">
                                    <input type="text" class="datepicker2 form-control datetimepicker-input '.$formalign.'" style="height: '.$v->formheight.'px; '.$cssreadonly.'" value="'.$colformat.'" placeholder="'.$v->formplaceholder.'" data-target="#kt_datetimepicker_1" '.$maxlength.' '.$dataformcaption.' '.$datanewreq.' '.$dataeditreq.' '.$required.' '.$disabled.'>
                                    <div class="input-group-append" data-target="#kt_datetimepicker_1" data-toggle="datetimepicker">
                                        <span class="input-group-text">
                                            <i class="ki ki-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                            </div><br>';
            } elseif ($v->formtypefield == 12  ) {
                $colformat = \Carbon\Carbon::now()->format($v->colformat);
                if ($v->coldefault != null) {
                    $colformat = $v->coldefault;
                }

                if ($v->formdefault != null && $v->formdefault == 'datetimenow') {
                    $formdefault = \Carbon\Carbon::now()->format($v->formfieldformat);
                    $formdefaulthidden = date('Y-m-d H:i:s');
                }
                
                if ($v->newreadonly == 1) {
                    $readonly = 'disabled';
                    $inputhidden = '<input type="hidden" name="'.$v->fieldsource.'" value="'.@$formdefaulthidden.'"/>';
                } else {
                    $readonly = '';
                }

                // $mytime = \Carbon\Carbon::now()->format('d-m-Y');
                $forminput = '<div id="div-'.$v->fieldname.'" class="col-md-'.$v->formwidth.' col-xs-'.$v->formwidthxs.' '.$v->formpull.'" title="'.$v->formhint.'">
                                <div class="form-group">
                                    <label class="control-label">'.$formcaption.'</label>
                                    <div class="input-group date">
                                        '.@$inputhidden.'
                                        <input type="text" class="datepicker form-control '.$formalign.'" style="height: '.$v->formheight.'px; '.$cssreadonly.'" data-provide="datepicker" readonly="readonly" value="'.@$formdefault.'"  id="'.$v->tablename.'-'.$v->fieldname.'" name="'.$v->fieldname.'" placeholder="'.$v->formplaceholder.'" '.$maxlength.' '.$dataformcaption.' '.$datanewreq.' '.$dataeditreq.' '.$readonly.' '.$required.' '.$disabled.'>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div><br>';
            } elseif ($v->formtypefield == 16) {

            } elseif ($v->formtypefield == 21) {
                $forminput = '<div id="div-'.$v->fieldname.'" class="col-md-'.$v->formwidth.' col-xs-'.$v->formwidthxs.' '.$v->formpull.'" title="'.$v->formhint.'">
                                <div class="form-group">
                                    '.$row.'
                                        <label class="control-label '.$collabel.'">'.$formcaption.'</label>
                                        <div class="'.$colinput.'">
                                            <input type="text" class="form-control '.$formalign.'" style="height: '.$v->formheight.'px; '.$cssreadonly.'" id="'.$v->tablename.'-'.$v->fieldname.'" name="'.$v->fieldname.'" value="'.$v->coldefault.'" placeholder="'.$v->formplaceholder.'" '.$maxlength.' '.$dataformcaption.' '.$datanewreq.' '.$dataeditreq.' '.$required.' '.$disabled.'>
                                        </div>
                                    '.$endrow.'
                                </div>
                            </div><br>';
            } elseif ($v->formtypefield == 31 ) {
                $forminput = '<div id="div-'.$v->fieldname.'" class="col-md-'.$v->formwidth.' col-xs-'.$v->formwidthxs.' '.$v->formpull.'" title="'.$v->formhint.'">
                                <div class="form-group">
                                    '.$row.'
                                        <label class="control-label '.$collabel.'">'.$formcaption.'</label>
                                        <br>
                                        <div class="'.$colinput.'" style="display: inline-block; vertical-align: middle; height: '.$v->formheight.'px">
                                            <input type="checkbox" class="'.$formalign.'" style="'.$cssreadonly.'" id="'.$v->tablename.'-'.$v->fieldname.'" name="'.$v->fieldname.'" value="'.$v->coldefault.'" placeholder="'.$v->formplaceholder.'" '.$maxlength.' '.$dataformcaption.' '.$datanewreq.' '.$dataeditreq.' '.$required.' '.$disabled.'>
                                            <span>'.$v->fieldname.'</span>
                                            <span class="help-block"></span>  
                                        </div>
                                    '.$endrow.'
                                </div>
                            </div><br>';
            } elseif ($v->formtypefield == 41 ) {
                $forminput = '<div id="div-'.$v->fieldname.'" class="col-md-'.$v->formwidth.' col-xs-'.$v->formwidthxs.' '.$v->formpull.'" title="'.$v->formhint.'">
                                <div class="checkbox-list">
                                    <label class="checkbox">
                                        <input type="checkbox" class="'.$formalign.'" style="height: '.$v->formheight.'px; '.$cssreadonly.'" id="'.$v->tablename.'-'.$v->fieldname.'" name="'.$v->fieldname.'" value="'.$v->coldefault.'" placeholder="'.$v->formplaceholder.'" '.$maxlength.' '.$dataformcaption.' '.$datanewreq.' '.$dataeditreq.' '.$required.' '.$disabled.'>
                                        <span></span>
                                        '.$v->fieldname.'
                                    </label>
                                </div>
                            </div><br>';
            } elseif ($v->formtypefield == 51  ) {
                $forminput = '<div id="div-'.$v->fieldname.'" class="col-md-'.$v->formwidth.' col-xs-'.$v->formwidthxs.' '.$v->formpull.'" title="'.$v->formhint.'">
                                <div class="form-group">
                                    '.$row.'
                                        <label class="control-label '.$collabel.'">'.$formcaption.'</label>
                                        <br>
                                        <div class="'.$colinput.'" style="display: inline-block; vertical-align: middle; height: '.$v->formheight.'px">
                                            <input type="checkbox" class="'.$formalign.'" style="'.$cssreadonly.'" id="'.$v->tablename.'-'.$v->fieldname.'" name="'.$v->fieldname.'" value="'.$v->coldefault.'" placeholder="'.$v->formplaceholder.'" '.$maxlength.' '.$dataformcaption.' '.$datanewreq.' '.$dataeditreq.' '.$required.' '.$disabled.'>
                                            <span>'.$v->fieldname.'</span>
                                            <span class="help-block"></span>  
                                        </div>
                                    '.$endrow.'
                                </div>
                            </div><br>';
            } elseif ($v->formtypefield == 95) {
                $forminput = $this->getFieldMasterTable($v,$v->fieldname,$v->fieldcaption,$v->newreq);
            } elseif ($v->formtypefield == 96) {
                $forminput = $this->getFieldMasterTable($v,$v->fieldname,$v->fieldcaption,$v->newreq);
            } elseif ($v->formtypefield == 97) {
                $forminput = $this->getFieldMasterTable($v,$v->fieldname,$v->fieldcaption,$v->newreq);
            } elseif ($v->formtypefield == 161) {
                $forminput = '<div style="display:block;" class="col-md-6 col-xs-12 pull-left">
                    <div class="form-group">
                        <label class="control-label ">'.$v->formcaption.'</label>
                        <input type="hidden" id="'.$v->fieldsource.'-'.$v->noid.'" name="'.$v->fieldsource.'"/>
                        <div class="" onmouseover="showBtnFileManager('.$v->noid.')" onmouseout="hideBtnFileManager('.$v->noid.')">
                            <div class="popup-image_preview" style="width: 100%; height: 100%; text-align: -webkit-center; border: 1px solid #f0f0f0; padding: 8px; cursor: pointer; position: relative; min-height: 40px; overflow: hidden">
                                <div id="fm-btn-action-'.$v->noid.'" class="fm-btn-action" style="overflow: hidden; display: block; position: absolute; top: -40px; left: 0px; right: 0px; padding: 6px 0px; background: rgba(154, 18, 179, .7)">
                                    <div class="btn btn-sm green btn_hide" id="iddmslogo" onclick="showModalDocumentViewer('.$v->noid.')">
                                        <span id="icon">
                                            <i class="fa fa-plus text-light"></i>
                                        </span> 
                                        <span id="caption">File Manager</span>
                                    </div>
                                </div>
                                <div id="content_image-'.$v->noid.'" class="fm-btn-content" style="height: 200px;">
                                    <div class="modal-image-content" style="width: 100%; height: inherit; text-align: -webkit-center">
                                        <div class="fm-image-content" style="vertical-align: middle; display: table-cell; width: 100%; height: inherit; text-align: -webkit-center">
                                            <img src="http://dashboard.dev.lambada.id/data_lycon/filemanager/noimage.png" style="max-width:100%;max-height:200px;">
                                        </div>
                                    </div>
                                </div>
                                <div class="fm-btn-title" id="title_img-'.$v->noid.'" style="position: absolute; left: 0; right: 0; bottom: 0; color: #fff; padding: 10px; background: rgba(154, 18, 179, .7); overflow: hidden; white-space: nowrap; text-overflow: ellipsis; font-weight: bold; text-align: center">File Manager</div>
                            </div>
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div id="modal-document-viewer-'.$v->noid.'" class="modal fade" aria-hidden="true" style="display: none;">
                        <div class="modal-lg_ modal-dialog" style="max-width: 95%">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="modal-title">Document Viewer</div>
                                </div>
                                <div class="modal-body" style="margin-top: 5px !important">
                                    <div class="col-md-12 fm_panel">
                                        <div class="fm_panel_body" style="overflow: hidden; border: 4px solid #03A9F4; margin: 0 -5px 0 -4px">
                                            <div class="col-md-12 fm_panel_toolbar" style="background: #fff; padding: 4px 0 0 0; display: inline-block; border-bottom: 4px solid #03A9F4">
                                                <div class="row">
                                                    <div class="col-md-6 col-xs-12 fm_btnfilter" id="filter">
                                                        <a type="button" class="btn btn-sm default" onclick="toggleFmBtn('.$v->noid.', \''.$v->fieldsource.'\', \'myfiles\')"><i class="fa fa-copy text-dark"></i> ALL</a>
                                                        <a type="button" class="btn btn-sm blue" onclick="toggleFmBtn('.$v->noid.', \''.$v->fieldsource.'\', \'myfiles\', \'doc\')"><i class="fa fa-file text-light"></i> DOC</a>
                                                        <a type="button" class="btn btn-sm green" onclick="toggleFmBtn('.$v->noid.', \''.$v->fieldsource.'\', \'myfiles\', \'img\')"><i class="fa fa-file-image text-light"></i> IMG</a>
                                                        <a type="button" class="btn btn-sm blue-hoki" onclick="toggleFmBtn('.$v->noid.', \''.$v->fieldsource.'\', \'myfiles\', \'av\')"><i class="fa fa-file-audio text-light"></i> AV</a>
                                                        <a type="button" class="btn btn-sm yellow" onclick="toggleFmBtn('.$v->noid.', \''.$v->fieldsource.'\', \'myfiles\', \'zip\')"><i class="fa fa-file-archive text-light"></i> ZIP</a>
                                                        <a type="button" class="btn btn-sm red" onclick="toggleFmBtn('.$v->noid.', \''.$v->fieldsource.'\', \'myfiles\', \'bin\')"><i class="fa fa-file-archive text-light"></i> BIN</a>
                                                        <a type="button" class="btn btn-sm purple" onclick="toggleFmBtn('.$v->noid.', \''.$v->fieldsource.'\', \'myfiles\', \'other\')"><i class="fa fa-file text-light"></i> OTHER</a>
                                                    </div>
                                                    <div class="col-md-3 col-xs-6 fm_btnlibrary" style="text-align: right;">
                                                        <a type="button" class="btn btn-sm purple tooltips" data-container="body" data-placement="bottom" data-original-title="Upload" id="upload-'.$v->noid.'" onclick="toggleFmBtn('.$v->noid.', \''.$v->fieldsource.'\', \'upload\')">
                                                            <i class="fa fa-cloud text-light"></i> <span style="display: inline-block">Upload</span>
                                                        </a type="button">
                                                        <a type="button" class="btn btn-sm green tooltips" data-container="body" data-placement="bottom" data-original-title="My Files" id="myfiles-'.$v->noid.'" onclick="toggleFmBtn('.$v->noid.', \''.$v->fieldsource.'\', \'myfiles\')">
                                                            <i class="fa fa-user text-light"></i> <span style="display: none">My Files</span>
                                                        </a type="button">
                                                        <a type="button" class="btn btn-sm yellow tooltips" data-container="body" data-placement="bottom" data-original-title="Library" id="library-'.$v->noid.'" onclick="toggleFmBtn('.$v->noid.', \''.$v->fieldsource.'\', \'library\')">
                                                            <i class="fa fa-link text-light"></i> <span style="display: none">Library</span>
                                                        </a type="button">
                                                        <a type="button" class="btn btn-sm blue tooltips" data-container="body" data-placement="bottom" data-original-title="Tile View" style="display: none;">
                                                            <i class="fa fa-th text-light"></i>
                                                        </a type="button">
                                                        <a type="button" class="btn btn-sm grey tooltips" data-container="body" data-placement="bottom" data-original-title="List View" style="display: none;">
                                                            <i class="fa fa-th-list text-dark"></i>
                                                        </a type="button">
                                                    </div>
                                                    <div class="col-md-3 col-xs-6 fm_search">
                                                        <div class="input-icon input-icon-sm right">
                                                            <i class="fa fa-search"></i>
                                                            <input type="text" id="search" class="form-control input-sm" placeholder="Search..." style="height: 27px; margin-bottom: 4px">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 fm_panel_container" id="container" style="height:70vh; padding: 0px">
                                                <div class="fm_panelcontent" id="content_myfile-'.$v->noid.'" style="display: none;">
                                                    <div class="row">
                                                        <div class="col-md-9 col-xs-7 fm_content" style="padding: 4px">
                                                            <div class="row">
                                                                <div class="col-md-12 fm_content_list modal-body" id="panelcontent" style="padding: 0px; margin: 0px !important;">
                                                                    <div class="row">
                                                                        <div class="col-md-2 col-xs-4 fm_content_grid_items" id="fm_content_grid_item" style="margin-bottom: 4px; padding: 4px; text-align: -webkit-center; height: 100%" onmouseover="mouseoverGridItem(\'fm_contenttitle\', \'fm_contenttitle_action\')" onmouseout="mouseoutGridItem(\'fm_contenttitle\', \'fm_contenttitle_action\')">
                                                                            <div class="fm_contentitems" id="fm_contentitems-'.$v->noid.'" style="height: 139px; overflow: hidden;  border: 1px solid grey" onclick="selectContentItem('.$v->noid.')">
                                                                                <span class="badge badge-null badge-roundless" style="display: none;">UNSPECIFIED</span>
                                                                                <img src="http://dashboard.dev.lambada.id/data_lycon/filemanager/thumb/cmspage-pagecaption.jpg" class="image_ori">
                                                                            </div>
                                                                            <div class="fm_contenttitle" id="fm_contenttitle" onclick="selectContentItem('.$v->noid.')" style="text-align: center; height: 23px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis; color: #fff; bottom: 4px; position: absolute; width: calc(100% - 8px); padding: 4px; background: rgba(0, 0, 0, .7); display: block; font-size: 80%; -webkit-transition: all 0.5s ease;">
                                                                                cmspage-pagecaption
                                                                                <div class="fm_contenttitle_action" id="fm_contenttitle_action" style="position: absolute; bottom: 4px; left: -200%; right: 0; transition: all 0.5s ease 0s;">
                                                                                    <a type="button" class="btn btn-sm purple tooltips" data-container="body" data-placement="top" data-original-title="Preview" id="preview" onclick="showPreviewFile('.$v->noid.', \'http://dashboard.dev.lambada.id/data_lycon/filemanager/thumb/cmspage-pagecaption.jpg\', \'cmspage-pagecaption.jpg\')">
                                                                                        <i class="fa fa-desktop text-light"></i>
                                                                                    </a>
                                                                                    <span class="btn btn-sm yellow tooltips" data-container="body" data-placement="top" data-original-title="Attach" id="setdocument" onclick="chooseFile('.$v->noid.', \'http://dashboard.dev.lambada.id/data_lycon/filemanager/thumb/cmspage-pagecaption.jpg\', \'cmspage-pagecaption.jpg\')">
                                                                                        <i class="fa fa-upload text-light"></i>
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-xs-5 fm_meta" id="fm_meta-'.$v->noid.'" style="display: none">
                                                            <ul class="nav">
                                                                <li class="nav-item active">
                                                                    <a class="nav-link" href="#tab_detail_1614655295201" data-toggle="tab" aria-expanded="true">Details</a>
                                                                </li>
                                                                <li class="nav-item">
                                                                    <a class="nav-link" href="#tab_attributes_1614655295201" data-toggle="tab" aria-expanded="false">Attributes</a>
                                                                </li>
                                                                <li class="nav-item" style="display:none;">
                                                                    <a class="nav-link" href="#tab_comment_1614655295201" data-toggle="tab" aria-expanded="false">Comments</a>
                                                                </li>
                                                            </ul>
                                                            <div class="tabbable-custom tabs-below nav-justified modal-body" style="margin: 0px !important">
                                                                <div class="tab-content">
                                                                    <div class="tab-pane active" id="tab_detail_1614655295201">
                                                                        <div>
                                                                            <div class="col-md-12 fm_meta_tag_function" style="text-align: center; padding: 4px; background: #000; width: 100%">
                                                                                <a href="http://dashboard.dev.lambada.id/data_lycon/filemanager/original/Screen Shot 2021-02-24 at 09.53.03.png" id="fm_meta_details_download-'.$v->noid.'" download target="_blank" class="btn btn-sm blue tooltips">
                                                                                    <i class="fa fa-download text-light"></i>
                                                                                </a>
                                                                                <span class="btn btn-sm purple tooltips" data-container="body" data-placement="top" data-original-title="Preview" id="fm_meta_details_preview-'.$v->noid.'" onclick="showPreviewFile('.$v->noid.', \'http://dashboard.dev.lambada.id/data_lycon/filemanager/thumb/cmspage-pagecaption.jpg\', \'cmspage-pagecaption.jpg\')">
                                                                                    <i class="fa fa-desktop text-light"></i>
                                                                                </span>
                                                                                <span class="btn btn-sm red tooltips" data-container="body" data-placement="top" data-original-title="Edit Document" id="fm_meta_details_edit-'.$v->noid.'">
                                                                                    <i class="fa fa-edit text-light"></i>
                                                                                </span>
                                                                                <span class="btn btn-sm yellow tooltips" data-container="body" data-placement="top" data-original-title="Attach" id="fm_meta_details_choose-'.$v->noid.'">
                                                                                    <i class="fa fa-upload text-light"></i>
                                                                                </span>
                                                                            </div>
                                                                            <div class="col-md-12 fm_meta_filename" id="fm_meta_filename-'.$v->noid.'" style="padding: 8px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis; background: rgba(0, 0, 0, .7); width: 100%; color: #fff">
                                                                                <span class="btn btn-xs green">
                                                                                    <i class="fa fa-file-image text-light"></i>
                                                                                </span>
                                                                                <strong>
                                                                                    Screen Shot 2021-02-24 at 09.53.03.png
                                                                                </strong>
                                                                            </div>
                                                                            <div class="col-md-12" style="text-align: -webkit-center;padding: 8px;">
                                                                                <div class="fm_meta_image" id="fm_meta_image-'.$v->noid.'">
                                                                                    <img src="http://dashboard.dev.lambada.id/data_lycon/filemanager/thumb/cmspage-pagecaption.jpg" style="width: 100%">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                                <div class="row">
                                                                                    <div class="col-md-5 col-xs-5">Size:</div>
                                                                                    <div class="col-md-7 col-xs-7" id="fm_meta_size-'.$v->noid.'">297 KB</div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                                <div class="row">
                                                                                    <div class="col-md-5 col-xs-5">Type:</div>
                                                                                    <div class="col-md-7 col-xs-7" id="fm_meta_type-'.$v->noid.'">IMG Document</div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                                <div class="row">
                                                                                    <div class="col-md-5 col-xs-5">Date Modified:</div>
                                                                                    <div class="col-md-7 col-xs-7" id="fm_meta_modified-'.$v->noid.'">-</div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                                <div class="row">
                                                                                    <div class="col-md-5 col-xs-5">Date Created:</div>
                                                                                    <div class="col-md-7 col-xs-7" id="fm_meta_created-'.$v->noid.'">2021-03-02 01:42:34</div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                                <div class="row">
                                                                                    <div class="col-md-5 col-xs-5">Status:</div>
                                                                                    <div class="col-md-7 col-xs-7">
                                                                                        <span class="badge badge-null badge-roundless fm_meta_status" id="fm_meta_status_unspecified-'.$v->noid.'" style="display: none;">UNSPECIFIED</span> 
                                                                                        <span class="badge badge-success badge-roundless fm_meta_status" id="fm_meta_status_public-'.$v->noid.'">PUBLIC</span>
                                                                                        <span class="badge badge-warning badge-roundless fm_meta_status text-light" id="fm_meta_status_private-'.$v->noid.'" style="display: none;">PRIVATE</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12 fm_meta_tag_tags_list">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="tab-pane" id="tab_attributes_1614655295201" style="position: relative; zoom: 1;">
                                                                        <div style="display: inline-block_;">
                                                                            <div class="col-md-12 col-xs-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-xs-12">
                                                                                        <span class="btn btn-xs green tooltips" id="fm_meta_attributes_ispublic-'.$v->noid.'" data-container="body" data-placement="top" data-original-title="Status" style="font-size: 10px; font-weight: bold; padding: 3px">
                                                                                            PUBLIC
                                                                                        </span> 
                                                                                        <span class="btn btn-xs green tooltips" id="fm_meta_attributes_fileextention-'.$v->noid.'" data-container="body" data-placement="top" data-original-title="File Type" style="font-size: 10px; font-weight: bold; padding: 3px">
                                                                                            <i class="fa fa-file-image text-light"></i> IMG
                                                                                        </span> 
                                                                                        <span class="btn btn-xs grey tooltips" id="fm_meta_attributes_size-'.$v->noid.'" data-container="body" data-placement="top" data-original-title="File Size" style="font-size: 10px; font-weight: bold; padding: 3px">
                                                                                            297 KB
                                                                                        </span> 
                                                                                        <span class="btn btn-xs blue pull-right tooltips" id="editfile-'.$v->noid.'" data-container="body" data-placement="top" data-original-title="Save Document" style="font-size: 10px; font-weight: bold; padding: 3px">
                                                                                            <i class="fa fa-save"></i> Save
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12 col-xs-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-xs-12">
                                                                                        <span class="btn btn-xs green tooltips" id="fm_meta_attributes_created-'.$v->noid.'" data-container="body" data-placement="top" data-original-title="Created By" style="width:100%;text-align:left; font-size: 10px; font-weight: bold; padding: 3px">
                                                                                            2021-03-02 01:42:34 (GUEST)
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12 col-xs-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-xs-12">
                                                                                        <span class="btn btn-xs yellow tooltips" id="fm_meta_attributes_updated-'.$v->noid.'" data-container="body" data-placement="top" data-original-title="Updated by" style="width:100%;text-align:left; font-size: 10px; font-weight: bold; padding: 3px">- (GUEST)</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12 col-xs-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-xs-12">Name:</div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12 col-xs-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-xs-12">
                                                                                        <input id="attribute_nama-'.$v->noid.'" type="text" style="width:100%;">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12 col-xs-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-xs-12">Status:</div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12 col-xs-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-xs-12">
                                                                                        <select id="attribute_status-'.$v->noid.'">
                                                                                            <option value="0">Private</option>
                                                                                            <option value="1">Public</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12 col-xs-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-xs-12">Tags:</div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12 col-xs-12 fm_meta_tag" style="margin-bottom: 4px">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-xs-12" style="display:table;">
                                                                                        <select multiple class="form-control form-control-solid select2tag" id="attribute_tag-'.$v->noid.'">
                                                                                            <option value="testing">testing</option>
                                                                                            <option value="sample">sample</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="tab-pane" id="tab_comment_1614655295201">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="fm_panelcontent" id="content_upload-'.$v->noid.'" style="display: block;">
                                                    <div class="col-md-12">
                                                        <div id="content_upload_files">
                                                            <center style="padding:20px;">
                                                                <div>
                                                                    <div class="btn btn-sm green fileinput-button" style="position: relative; overflow: hidden">
                                                                        <i class="fa fa-plus text-light"></i>
                                                                        <span> Upload</span>
                                                                        <form method="post" enctype="multipart/form-data">
                                                                            <input id="files" type="file" name="files" multiple="" style="position: absolute; top: 0; right: 0; margin: 0; opacity: 0; font-size: 200px; direction: ltr; cursor: pointer" onchange="afterPickFiles(\'files\')">
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </center>
                                                        </div>
                                                        <div id="content_upload_files2">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer" style="padding: 5px 0 0 0 !important">
                                    <a href="javascript:;" title="Close" class="fancybox-item fancybox-close" data-dismiss="modal" aria-hidden="true"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal" id="modal-preview-file-'.$v->noid.'" data-backdrop="static" aria-hidden="false" style="display: none; padding-left: 0px;">
                        <div class="modal-lg modal-dialog" style="width:90%">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="modal-title">
                                        Anjas_CV.jpg
                                    </div>
                                </div>
                                <div class="modal-body" style="margin-top: 5px !important">
                                    <div class="modal-image-content" style="width: 100%; height: inherit; text-align: -webkit-center">
                                        <div class="fm-image-content" style="vertical-align: middle; display: table-cell; width: 100%; height: inherit;">
                                            <img src="http://dashboard.dev.lambada.id/data_lycon/filemanager/preview/cmspage-pagecaption.jpg" data-testing="testing" style="max-width:100%;max-height:100%;">
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                </div>
                                <a href="javascript:;" title="Close" class="fancybox-item fancybox-close" data-dismiss="modal" aria-hidden="true"></a>
                            </div>
                        </div>
                    </div>
                </div>';
            } else {
                $forminput = '';
            }

            if ($v->newshow == 1) {
                if (empty($v->formgroupname)) {
                    $data['field'][]= array(
                        'label' => $v->fieldcaption,
                        'kode' => $v->formtypefield,
                        'field' => $forminput,
                        'colgroupname' => $v->colgroupname,
                        'formgroupname' => $v->formgroupname,
                        'colsorted' => $v->colsorted,
                        'fieldname' => $v->fieldname,
                        'newhidden' => $v->newhidden,
                        'edithidden' => $v->edithidden,
                        'tablemaster' => $tabledetail,
                        'tablename' => $v->tablename,
                        'onchange' => $onchange,
                    );
                }

                $data['allfield'][]= array(
                    'label' => $v->fieldcaption,
                    'kode' => $v->formtypefield,
                    'field' => $forminput,
                    'formgroupname' => $v->formgroupname,
                    'colsorted' => $v->colsorted,
                    'fieldname' => $v->fieldname,
                    'newhidden' => $v->newhidden,
                    'newreadonly' => $v->newreadonly,
                    'edithidden' => $v->edithidden,
                    'editreadonly' => $v->editreadonly,
                    'tablemaster' => $tabledetail,
                    'tablename' => $v->tablename,
                    'onchange' => $onchange,
                );
            }

            $formtabname = explode(';', $v->formtabname);
            if ($v->formgroupname) { 
                // if ($v->fieldname == 'lastupdate') {
                //     print_r($forminput);die;
                // }
                $data['formgroupname'][$v->idcmswidgetgrid.'-'.$v->formgroupname][$formtabname[1]][] = $forminput;  
            }

            // $data['field'][]= array(
            //     'label' => $v->fieldcaption,
            //     'kode' => $v->coltypefield,
            //     'field' => $forminput,
            //     'colgroupname' => $v->colgroupname,
            //     'formgroupname' => $v->formgroupname,
            //     'colsorted' => $v->colsorted,
            //     'fieldname' => $v->fieldname,
            //     'newhidden' => $v->newhidden,
            //     'edithidden' => $v->edithidden,
            // );
            // dd($data);
        }
        $data['cek'] = $cek;
        $data['formgroupname'] = (@$data['formgroupname']) ? $data['formgroupname'] : '';
        // $data['wgf'] = $this->getFieldMasterTable($widgetgridfield[28],$widgetgridfield[28]->fieldname,$widgetgridfield[28]->fieldcaption,$widgetgridfield[28]->newreq);
        // dd($data['field']);

        header("Content-type: application/json");
        header("Cache-Control: no-cache, must-revalidate");
        echo json_encode($data);
    }

    public function get_file_master_table(Request $request) {
        $where = $request->get('where');
        
        $querywhere = '';
        if ($where) {
            if ($where == 'img') {
                $querywhere = "WHERE dmsmtypefile.fileextention = 'png'
                            OR dmsmtypefile.fileextention = 'jpg'
                            OR dmsmtypefile.fileextention = 'jpeg'
                            OR dmsmtypefile.fileextention = 'gif' ";
            } else {
                $querywhere = "WHERE dmsmtypefile.fileextention = '$where'";
            }
        }

        $query = "SELECT dmsmfile.*, dmsmtypefile.fileextention AS fileextention, dmsmtypefile.classicon AS classicon, dmsmtypefile.classcolor AS classcolor
                    FROM dmsmfile
                    JOIN dmsmtypefile ON dmsmfile.idtypefile = dmsmtypefile.noid
                    $querywhere
                    ORDER BY dmsmfile.docreate DESC";
        $result = DB::connection('mysql2')->select($query);
    
        $data = [];
        foreach ($result as $k => $v) {
            $data[] = $v;
        }

        return response()->json($data);
    }

    public function edit_file_master_table(Request $request) {$filetag = '';
        foreach ($request->get('filetag') as $k => $v) {
            $filetag .= count($request->get('filetag'))-1 == $k ? $v : $v.',';
        }

        $result = DB::connection('mysql2')
                    ->table('dmsmfile')
                    ->where('noid', $request->get('noid'))
                    ->update([
                        'nama' => $request->get('filename'),
                        'ispublic' => $request->get('filestatus'),
                        'filetag' => $filetag
                    ]);
        
        return response()->json($result);
    }
    
    public function upload_file_master_table(Request $request) {
        $validator = Validator::make($request->all(), [
            'file' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $request->get('file')
            ]);
        }

        $resultlastnoid = DB::connection('mysql2')->table('dmsmfile')->select('noid')->orderBy('noid', 'desc')->first();
        $lastnoid = ($resultlastnoid != null) ? $resultlastnoid->noid+1 : 0; 
        $filelocation = 'filemanager/original';
        $filepreview = 'filemanager/preview';
        $filethumbnail = 'filemanager/thumb';
        $file = $request->file('file');
        $file_name = $file->getClientOriginalName();
        $filesize = $file->getSize();
        $explodefe = explode('.', $file_name);
        $fileextension = end($explodefe);

        if ($fileextension == 'png' ||
            $fileextension == 'jpg' ||
            $fileextension == 'jpeg' ||
            $fileextension == 'gif') {
            $imageresize = ImageResize::make($file->path());
            //RESIZE TO PREVIEW
            $imageresize->resize(900, 300, function ($constraint) {
                $constraint->aspectRatio();
            })->save('storage/'.$filepreview.'/'.$file_name);
            //RESIZE TO THUMBNAIL
            $imageresize->resize(900, 300, function ($constraint) {
                $constraint->aspectRatio();
            })->save('storage/'.$filethumbnail.'/'.$file_name);
        }

        $ispublic = $request->get('ispublic');
        $uploadfileoriginal = $file->storeAs('public/'.$filelocation, $file_name);
        $resulttypefile = DB::connection('mysql2')->table('dmsmtypefile')->select('noid')->where('fileextention', $fileextension)->first();
        $idtypefile = ($resulttypefile != null) ? $resulttypefile->noid : 9;

        $data = [
            'noid' => $lastnoid,
            'nama' => $file_name,
            'filename' => $file_name,
            'filelocation' => $filelocation,
            'filepreview' => $filepreview,
            'filethumbnail' => $filethumbnail,
            'filesize' => $filesize,
            'idtypefile' => $idtypefile,
            'ispublic' => $ispublic,
            'docreate' => date('Y-m-d H:i:s'),
        ];

        $insert = DB::connection('mysql2')->table('dmsmfile')->insert($data);

        return response()->json([
            'success' => true,
            'message' => 'Upload '.strtoupper($fileextension).' successfully',
            'data' => json_encode($request->file('file'))
        ]);
    }

    public static function getFieldMasterTable($v,$val2, $val3,$req) {
        if ($v->formdefault == 'globalparam/$GLOBAL_LOGIN_ID') {
            if (Session::get('noid')) {
                $selectednoid = Session::get('noid');
            }
        }

        if ($req == 1) {
            $required = 'required';
            $formcaption = $v->formcaption.' <span class="text-danger">*</span>';
        } else {
            $required = '';
            $formcaption = $v->formcaption;
        }
        
        // if ($isadd == 'true') {
            if ($v->newreadonly == 1) {
                $readonly = 'readonly disabled';
                $cssreadonly = 'background-color: #E5E5E5 !important;';
                $classselect2 = '';
                $inputhidden = '<input type="hidden" id="'.$v->tablename.'-'.$v->fieldname.'-input" name="'.$v->fieldsource.'" value="'.@$selectednoid.'"/>';
            } else {
                $readonly = '';
                $cssreadonly = '';
                $classselect2 = 'select2';
            }
        // } else {
        //     if ($v->editreadonly == 1) {
        //         $readonly = 'readonly disabled';
        //         $cssreadonly = 'background-color: #E5E5E5 !important;';
        //         $classselect2 = '';
        //     } else {
        //         $readonly = '';
        //         $cssreadonly = '';
        //         $classselect2 = 'select2';
        //     }
        // }

        $conv = json_decode($v->lookupconfig, true);

        if ($conv == null) {
            $table = $v->tablename;

            $list = $v->lookupconfig;
            if ($list == null) {
                $select = '';
            } else {
                $stripped =explode(";",$list);
                array_pop($stripped);
                $selct = implode(",",$stripped);
                $stripped2 =explode(";",$list);
                $end = end($stripped2);
                $selct2 = "select ".$selct." from ".$end;
                // dd($selct2);
                $hasi = DB::connection('mysql2')->select($selct2);
                $select = '';
                $readonly = '';
                if ($v->newreadonly == 1) {
                    $readonly = 'disabled';
                } else {
                    $readonly = '';
                }
                $sel = array('<div class="col-md-'.$v->formwidth.' col-xs-'.$v->formwidthxs.' '.$v->formpull.'">
                                    <div class="form-group">
                                        <label class="control-label">'.$formcaption.'</label>
                                        '.@$inputhidden.'
                                        <select width="'.$v->colheight.'" '.$required.' class="form-control form-control-solid '.$classselect2.'" style="'.$cssreadonly.'" id="'.$v->tablename.'-'.$v->fieldname.'-select" name="'.$val2.'" '.$readonly.'>');
                
                foreach ($hasi as $key2 => $v2) {
                    $selected = @$selectednoid && @$selectednoid == $v2->noid ? 'selected' : '';
                    array_push($sel,        '<option value="'.$v2->noid.'" '.$selected.'>'.$v2->nama.'</option>');
                }
                
                array_push($sel,        '</select>
                                    </div>
                                </div>');
                
                foreach ($sel as $key3 => $v3) {
                    $select .= $sel[$key3];
                }
            }
        } else {
            $convselect = $conv['LookupConfig']['FormLookup']['dataquery'];
            $select = '';
            $hasil = DB::connection('mysql2')->select($convselect);
            $sel = array('<div class="col-md-'.$v->formwidth.' col-xs-'.$v->formwidthxs.' '.$v->formpull.'">
                            <div class="form-group">
                                <label class="control-label">'.$formcaption.'</label>
                                '.@$inputhidden.'
                                <select '.$required.' width="'.$v->colheight.'" class="form-control form-control-solid '.$classselect2.'" id="'.$v->tablename.'-'.$v->fieldname.'-select" name="'.$val2.'" '.$readonly.'>');
            
            foreach ($hasil as $key2 => $v2) {
                $selected = @$selectednoid && @$selectednoid == $v2->noid ? 'selected' : '';
                array_push($sel, '<option value="'.$v2->noid.'" '.$selected.'>'.$v2->nama.'</option>');
            }
            
            array_push($sel, '</select>
                            </div>
                        </div>');

            foreach ($sel as $key3 => $v3) {
                $select .= $sel[$key3];
            }
        }
        
        return $select;
    }

    public function find_master_table(Request $request) {
        $maincms = json_decode($request->get('maincms'));
        $data = $request->all();
        $menu = $maincms->menu;

        $page = $maincms->page;
        $panel = $maincms->panel;
        $widget = $maincms->widget;
        $filtertable = json_decode($widget->configwidget);
        $fieldtabel = $filtertable->WidgetFilter;
        $table = $widget->maintable;
        $data = $request->all();
        $widget = $maincms->widget;
        $widgetgrid = $maincms->widgetgrid;
        $widgetgridfield = array_values(array_filter($maincms->widgetgridfield4, function($v, $k) {
            if ($v->editshow == 1) {
                return $v;
            }
        }, ARRAY_FILTER_USE_BOTH));
        // $widgetgridfield = WidgetGridField::where('idcmswidgetgrid', $widgetgrid->noid)->where('editshow',1)->get();
        $id = $data['id'];
        $datatab = DB::connection('mysql2')->table($table)->where('noid',$id)->first();
        $no = 1;

        // $hasil[] = [
        //     'field' => 'noid',
        //     'value' => $id,
        //     'disabled' => false,
        //     'hidden' => false,
        // ];
        // return response()->json(['response'=>$maincms]);
        foreach ($widgetgridfield as $k => $v) {
            $string = (string)$widgetgridfield[$k]->fieldname;
            $hasil[] = [
                'field' => $widgetgridfield[$k]->fieldname,
                'tablename' => $v->tablename,
                'formtypefield' => $v->formtypefield,
                'value' => $datatab->$string,
                'disabled' => ($v->editreadonly == 1) ? true : false,
                'newhidden' => ($v->newhidden == 1) ? true : false,
                'edithidden' => ($v->edithidden == 1) ? true : false,
            ];
        }
        echo json_encode($hasil);
    }

    public function filter_master_table() {
        DB::enableQueryLog();
        $data = DB::connection('mysql2')->table($_POST['table'])->where($_POST['fieldname'], $_POST['operator'], $_POST['value'])->get();

        $json_data = array(
            "draw" => 1,
            "recordsTotal" => count($data),
            "recordsFiltered" => count($data),
            "data" => $data,
            "cek" => DB::getQueryLog()
        );
        echo json_encode($json_data);
    }
    
    public function get_next_noid_master_table(Request $request) {
        $table = $request->get('table');
        $nextnoid = DB::connection('mysql2')->table($table)->select('noid')->orderBy('noid','desc')->first();
        if ($nextnoid) {
            $nextnoid = $nextnoid->noid+1;
        } else {
            $nextnoid = 0;
        }
        echo json_encode($nextnoid);
    }
    
    public function save_master_table(Request $request) {
        $maincms = json_decode($request->get('maincms'));
        $table = $maincms->widget->maintable;
        // echo json_encode($request->get('table'));
        $data = $request->all();
        $datapost=  array();
        parse_str($data['data']['data'], $datapost);

        foreach ($maincms->widgetgridfield2 as $k => $v) {
            if ($datapost['noid'] == '') {
                if ($v->fieldsource == 'docreate') {
                    $datapost['docreate'] = date('Y-m-d H:i:s');
                }
            } else {
                unset($datapost['docreate']);
            }

            if ($v->fieldsource == 'lastupdate') {
                $datapost['lastupdate'] = date('Y-m-d H:i:s');
            }   
        }
        // return response()->json($datapost);

        if ($datapost['noid'] == '') {
            $total = DB::connection('mysql2')->table($table)->orderBy('noid','desc')->first();
            
            foreach($datapost as $key => $value) {
                $datapost['noid'] = $total->noid+1;
            }
            
            $in = DB::connection('mysql2')->table($table)->insert($datapost);
            
            echo json_encode(array(
                'status' => 'add',
                'table' => $table,
                'success' => true
            ));
        } else {
            $array = \array_diff($datapost, ["noid" => $datapost['noid']]);
            $total = DB::connection('mysql2')->table($table)->where('noid',$datapost['noid'])->update($array);
            
            echo json_encode(array(
                'status' => 'edit',
                'table' => $table,
                'success' => true
            ));
        }
    }
    
    public function delete_master_table(Request $request) {
        $namatable = $_POST['namatable'];
        $menu = Menu::where('noid',$namatable)->first();
        $page = Page::where('noid',$menu->linkidpage)->first();
        $panel = Panel::where('idpage',$page->noid)->first();
        $widget = Widget::where('noid', $panel->idwidget)->first();
        $filtertable = json_decode($widget->configwidget);
        $fieldtabel = $filtertable->WidgetFilter;
        $table = $widget->maintable;
        $result = DB::connection('mysql2')->table($table)->where('noid',$_POST['id'])->delete();
        if ($result) {
            echo json_encode([
                'success' => true,
                'message' => 'Deleted has been successfully' 
            ]);
        } else {
            echo json_encode([
                'success' => false,
                'message' => 'Deleted error.' 
            ]);
        }
    }

    public function delete_selected_master_table(Request $request) {
        $namatable = $_POST['namatable'];
        $menu = Menu::where('noid',$namatable)->first();
        $page = Page::where('noid',$menu->linkidpage)->first();
        $panel = Panel::where('idpage',$page->noid)->first();
        $widget = Widget::where('noid', $panel->idwidget)->first();
        $filtertable = json_decode($widget->configwidget);
        $fieldtabel = $filtertable->WidgetFilter;
        $table = $widget->maintable;
        $result = DB::connection('mysql2')->table($table)->whereIn('noid',$_POST['selecteds'])->delete();

        if ($result) {
            echo json_encode([
                'success' => true,
                'message' => 'Deleted has been successfully' 
            ]);
        } else {
            echo json_encode([
                'success' => false,
                'message' => 'Deleted error.' 
            ]);
        }
    }
    
    public function get_slide_master_table(Request $request) {
        $id_slider = $request->get('id_slider');
        $widget = Menu::getAllPanelSlide($id_slider);
        $hasi = json_decode($widget[0]->widget_configwidget);
        $hasil = $hasi->Widget_Slider->datasource->dataquery;
        // dd($hasil);
        $result =Menu::ambilQuery($hasil);
        header("Content-type: application/json");
        header("Cache-Control: no-cache, must-revalidate");
        echo json_encode(array(
            'hasil'=>$result
        ));
    }

    public function save_filter_master_table(Request $request) {
        $data = $request->all()['data']['data'];
        $url_code = $request->all()['data']['url_code'];
        $parse_data = array();
        parse_str($data, $parse_data);

        $widget = Widget::where('kode', $url_code)->first();

        $widget_config = json_decode($widget->configwidget)->WidgetConfig;
        $widget_filter = [];
        
        foreach ($parse_data['tabs'] as $k => $v) {
            $widget_filter[$k] = (object)[
                'groupcaption' => $parse_data['groupcaption'][$k],
                'groupcaptionshow' => ($parse_data['groupcaptionshow'][$k] == 1) ? true : false,
                'groupdropdown' => (int)$parse_data['groupdropdown'][$k],
                'button' => []
            ];

            foreach ($parse_data['tabsrow'] as $k2 => $v2) {
                if ($v == $v2) {
                    $explode1 = explode('#', $parse_data['condition'][$k2]);
                    $explode2 = explode(';', $explode1[1]);
                    $groupoperand = $explode1[0];
                    $fieldname = $explode2[0];
                    $operator = $explode2[1];
                    $value = $explode2[2];
                    $operand = $explode2[3];

                    $widget_filter[$k]->button[$k2] =  (object)[
                        'buttoncaption' => $parse_data['buttoncaption'][$k2],
                        'buttonactive' => ($parse_data['buttonactive'][$k2] == 1) ? true : false,
                        'classcolor' => $parse_data['classcolor'][$k2],
                        'classicon' => $parse_data['classicon'][$k2],
                        'condition' => [
                            (object)[
                                'groupoperand' => $groupoperand,
                                'querywhere' => [
                                    (object)[
                                        'fieldname' => $fieldname,
                                        'operator' => $operator,
                                        'value' => $value,
                                        'operand' => $operand
                                    ]
                                ]
                            ]
                        ]
                    ];
                }
            }
        }

        $config_widget = (object)[
            'WidgetFilter' => $widget_filter,
            'WidgetConfig' => $widget_config
        ];

        if($widget) {
            $result = DB::table('cmswidget')->where('kode', $url_code)->update(['configwidget' => json_encode($config_widget)]);
        }

        echo json_encode(array(
            'response' => $parse_data,
            'config_widget' => json_encode($config_widget),
            'result' => @$result
        ));
    }

}