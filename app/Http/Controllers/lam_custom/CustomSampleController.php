<?php

namespace App\Http\Controllers\lam_custom;

use App\Http\Controllers\Controller;
Use DB;
use App\User;
use App\Menu;
use App\Page;
use App\Panel;
use App\Widget;
use App\WidgetGrid;
use App\WidgetGridField;
use App\Departement;
use App\Company;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Schema;
use Carbon;

class CustomSampleController extends Controller
{
  public static function MasterMenu($slug)
  {
    // dd('ok');
    $menu = Menu::where('noid',$slug)->first();
    $page = Page::where('noid',$menu->linkidpage)->first();
    $idhomepagelink = $slug;
    $user = User::where('noid', Session::get('noid'))->first();
    $widget = Widget::where('kode', $slug)->first();
    // dd($widget);
      $public = $menu->ispublic;
      $idhomepage =  $user->idhomepage;
      $noid = Session::get('noid');
      $myusername = Session::get('myusername');
        $panel = Panel::where('idpage',$page->noid)->first();
        $page_title = $page->pagetitle;
        $page_description = $page->pagetitle;
        if ($widget == null) {
          $widgetgrid = null;
          $widgetgridfield = array();
          $widgetgridfield2 = array();
          $namatable = null;
        }else{
          $widgetgrid = WidgetGrid::where('idcmswidget', $widget->noid)->first();
          // dd($widgetgrid);
          if ($widgetgrid == null) {
            $widgetgridfield ='';
            $namatable ='';
            $tabledata ='';
            // code...
          }else{
            $widgetgridfield = WidgetGridField::where('idcmswidgetgrid', $widgetgrid->noid)->where('colshow',1)->get();
            $namatable = $widget->maintable;
            $tabledata =  DB::connection('mysql2')->table($widget->maintable)->first();
          }

        }

        return view('lam_custom.MasterMenu', compact(
          'noid',
          'menu',
          'widget',
          'widgetgrid',
          'widgetgridfield',
          'widgetgridfield2',
          'departement',
          'public',
          'myusername',
          'idhomepagelink',
          'page',
          'panel',
          'page_title',
          'namatable',
          'idhomepage',
           'page_description'));


  }
}
