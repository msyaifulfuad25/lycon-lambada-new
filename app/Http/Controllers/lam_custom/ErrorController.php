<?php

namespace App\Http\Controllers\lam_custom;

use App\Http\Controllers\Controller;
Use DB;
use App\User;
use App\Menu;
use App\Page;
use App\Panel;
use App\Widget;
use App\WidgetGrid;
use App\WidgetGridField;
use App\Departement;
use App\Company;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Schema;
use Carbon;
//ErrorFunction
class ErrorController extends Controller
{
    //
    public static function UnderMaintenance2($slug)
    {
      $menu = Menu::where('noid',$slug)->first();
      $page = Page::where('noid',$menu->linkidpage)->first();
      $idhomepagelink = $slug;
      $user = User::where('noid', Session::get('noid'))->first();
      $public = $menu->ispublic;
      $panel = Panel::where('idpage', $page->noid)->orderBy('norow','asc')->orderBy('nocol','asc')->get();
      $page_title = $page->pagetitle;
      $page_description = $page->pagetitle;
      $idhomepage =  $user->idhomepage;
      $noid = Session::get('noid');
      $myusername = Session::get('myusername');
      return view('lam_custom.PageError', compact(
        'noid',
        'public',
        'myusername',
        'idhomepagelink',
        'page',
        'page_title',
        'idhomepage',
        'menu',
        'namatable',
         'page_description'));
    }

    public static function ErrorFunction($slug)
    {
      $menu = Menu::where('noid',$slug)->first();
      $page = Page::where('noid',$menu->linkidpage)->first();
      $idhomepagelink = $slug;
      $user = User::where('noid', Session::get('noid'))->first();
      $public = $menu->ispublic;
      $panel = Panel::where('idpage', $page->noid)->orderBy('norow','asc')->orderBy('nocol','asc')->get();
      $page_title = $page->pagetitle;
      $page_description = $page->pagetitle;
      $idhomepage =  $user->idhomepage;
      $noid = Session::get('noid');
      $myusername = Session::get('myusername');
      return view('lam_custom.PageError', compact(
        'noid',
        'public',
        'myusername',
        'idhomepagelink',
        'page',
        'page_title',
        'idhomepage',
        'menu',
        'namatable',
         'page_description'));
    }

    public static function UnderMaintenanceFun($slug)
    {
      $menu = Menu::where('noid',$slug)->first();
      $page = Page::where('noid',$menu->linkidpage)->first();
      $idhomepagelink = $slug;
      $user = User::where('noid', Session::get('noid'))->first();
      $public = $menu->ispublic;
      $panel = Panel::where('idpage', $page->noid)->orderBy('norow','asc')->orderBy('nocol','asc')->get();
      $page_title = $page->pagetitle;
      $page_description = $page->pagetitle;
      $idhomepage =  $user->idhomepage;
      $noid = Session::get('noid');
      $myusername = Session::get('myusername');
      return view('lam_custom.FunError', compact(
        'noid',
        'public',
        'myusername',
        'idhomepagelink',
        'page',
        'page_title',
        'idhomepage',
        'menu',
        'namatable',
         'page_description'));
    }
    public static function UnderMaintenance($slug)
    {
      $menu = Menu::where('noid',$slug)->first();
      $page = Page::where('noid',$menu->linkidpage)->first();
      $idhomepagelink = $slug;
      $user = User::where('noid', Session::get('noid'))->first();
      $public = $menu->ispublic;
      $panel = Panel::where('idpage', $page->noid)->orderBy('norow','asc')->orderBy('nocol','asc')->get();
      $page_title = $page->pagetitle;
      $page_description = $page->pagetitle;
      $idhomepage =  $user->idhomepage;
      $noid = Session::get('noid');
      $myusername = Session::get('myusername');
      return view('lam_custom.PageNull', compact(
        'noid',
        'public',
        'myusername',
        'idhomepagelink',
        'page',
        'page_title',
        'idhomepage',
        'menu',
        'namatable',
         'page_description'));
    }
  
}
