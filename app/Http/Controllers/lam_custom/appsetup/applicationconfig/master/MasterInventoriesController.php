<?php

namespace App\Http\Controllers\lam_custom\appsetup\applicationconfig\master;

use App\Http\Controllers\Controller;
use App\Model\lam_custom_master\Inventory;
use App\Model\RA\RAP as MP2;
use App\Model\Sales\SalesOrder;
use App\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Helpers\MyHelper;
use DB;
use Image;
use Storage;

class MasterInventoriesController extends Controller {

    public $color = [
        'white' => '#FFFFFF',
        'default' => '#E1E5EC',
        'dark' => '#2F353B',
        'blue' => '#3598DC',
        'blue-madison' => '#578EBE',
        'blue-chambray' => '#2C3E50',
        'blue-ebonyclay' => '#22313F',
        'blue-hoki' => '#67809F',
        'blue-steel' => '#4B77BE',
        'blue-soft' => '#4C87B9',
        'blue-dark' => '#5E738B',
        'blue-sharp' => '#5C9BD1',
        'blue-oleo' => '#94A0B2',
        'green' => '#32C5D2',
        'green-meadow' => '#1BBC9B',
        'green-seagreen' => '#1BA39C',
        'green-torquoise' => '#36D7B7',
        'green-haze' => '#44B6AE',
        'green-jungle' => '#26C281',
        'green-soft' => '#3FABA4',
        'green-dark' => '#4DB3A2',
        'green-sharp' => '#2AB4C0',
        'green-steel' => '#29B4B6',
        'grey' => '#E5E5E5',
        'grey-steel' => '#E9EDEF',
        'grey-cararra' => '#FAFAFA',
        'grey-gallery' => '#555555',
        'grey-cascade' => '#95A5A6',
        'grey-silver' => '#BFBFBF',
        'grey-salsa' => '#ACB5C3',
        'grey-salt' => '#BFCAD1',
        'grey-mint' => '#525E64',
        'red' => '#E7505A',
        'red-pink' => '#E08283',
        'red-sunglo' => '#E26A6A',
        'red-intense' => '#E35B5A',
        'red-thunderbird' => '#D91E18',
        'red-flamingo' => '#EF4836',
        'red-soft' => '#D05454',
        'red-haze' => '#F36A5A',
        'red-mint' => '#E43A45',
        'yellow' => '#C49F47',
        'yellow-gold' => '#E87E04',
        'yellow-casablanca' => '#F2784B',
        'yellow-crusta' => '#F3C200',
        'yellow-lemon' => '#F7CA18',
        'yellow-saffron' => '#F4D03F',
        'yellow-soft' => '#C8D046',
        'yellow-haze' => '#C5BF66',
        'yellow-mint' => '#C5B96B',
        'purple' => '#8E44AD',
        'purple-plum' => '#8775A7',
        'purple-medium' => '#BF55EC',
        'purple-studio' => '#8E44AD',
        'purple-wisteria' => '#9B59B6',
        'purple-seance' => '#9A12B3',
        'purple-intense' => '#8775A7',
        'purple-sharp' => '#796799',
        'purple-soft' => '#8877A9',
    ];
    public static $main = [
        'table' => 'invminventory',
        'tablemilestone' => 'prjmrabmilestone',
        'tableheader' => 'prjmrabheader',
        'tabledetail' => 'prjmrabdetail',
        'tablecdf' => 'prjmrabdocument',
        'table-title' => 'Master Inventories',
        'table2' => 'prjmrap',
        'tabledetail2' => 'prjmrapdetail',
        'table2-title' => 'RAP',
        'generatecode' => 'generatecode/602/1',
        'generatecode2' => 'generatecode/604/1',
        'idtypetranc' => 602,
        'idtypetranc2' => 602,
        'noid' => 40010301,
        'noidnew' => 50011101,
        'printcode' => 'print_purchase_request',
        'condition' => [
            'mttts' => [
                [],
                [
                    'mttts' => 'draft',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => true,
                        'd' => true,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => true,
                        'd' => true,
                    ],
                    'draft' => false,
                    'issued' => true,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => false,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'issued',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => true,
                    'issued' => false,
                    'verified' => true,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => false,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'verified',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => true,
                    'verified' => false,
                    'approved' => true,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => false,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'approved',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'pending',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'canceled',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'finished',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
            ],
        ]
    ];

    public function index() {
        $tabletitle = static::$main['table-title'];
        $breadcrumb = Menu::getBreadcrumb(\Request::segment(1));
        $pagenoid = \Request::segment(1);
        $idtypepage = \Request::segment(1);
        $color = (new static)->color;
        $maintable = static::$main['table'];
        $datefilter = [
            'a' => 'All',
            't' => date('M d, Y').' - '.date('M d, Y'),
            'y' => date('M d, Y', strtotime('-1 day')).' - '.date('M d, Y', strtotime('-1 day')),
            'l7d' => date('M d, Y').' - '.date('M d, Y', strtotime('-7 day')),
            'l30d' => date('M d, Y').' - '.date('M d, Y', strtotime('1 month')),
            'tm' => date('M d, Y', strtotime('first day of this month')).' - '.date('M d, Y', strtotime('last day of this month')),
            'lm' => date('M d, Y', strtotime('first day of previous month')).' - '.date('M d, Y', strtotime('last day of previous month')),
        ];
        $datefilterdefault = 'a';
        $invmgroup = Inventory::getData(['table'=>'invmgroup','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $invmkategori = Inventory::getData(['table'=>'invmkategori','select'=>['noid','kode','nama','idinvgroup'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $invmmerk = Inventory::getData(['table'=>'invmmerk','select'=>['noid','kode','nama'],'orderby'=>[['nourut','asc']],'isactive'=>true]);
        $invmsatuan = Inventory::getData(['table'=>'invmsatuan','select'=>['noid','kode','nama'],'orderby'=>[['nourut','asc']],'isactive'=>true]);
        
        $privilege = $this->newPrivilege();
        $isadd = $privilege['iscreate'] ? 1 : 0;
        $sessionname = Session::get('nama');

        return view('lam_custom_master.'.static::$main['table'].'.index', compact(
            'tabletitle',
            'breadcrumb',
            'pagenoid',
            'idtypepage',
            'color',
            'maintable',
            'datefilter',
            'datefilterdefault',
            'invmgroup',
            'invmkategori',
            'invmmerk',
            'invmsatuan',
            'privilege',
            'isadd',
            'sessionname'
        ));
    }
    
    public function newPrivilege() {
        return MyHelper::getNewPrivilege([
            'idmenu' => Session::get('idmenu'),
            'idcard' => Session::get('noid'),
            'isroot' => Session::get('groupuser') == 0 ? true : false
        ]);
    }

    public function get(Request $request) {
        $privilege = $this->newPrivilege();

        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->get('search')['value'];
        $orderbycolumn = $request->get('columns')[$request->get('order')[0]['column']]['data'];
        if ($orderbycolumn == 'checkbox' || $orderbycolumn == 'no') {
            $orderbycolumn = 'noid';
        }
        $orderby = [
            'column' => $orderbycolumn,
            'sort' => $request->get('order')[0]['dir']
        ];
        $filter = $request->get('filter');
        $datefilter = $request->get('datefilter');
        $datefilter2 = $request->get('datefilter2');
        $invmgroup_filter = $request->get('invmgroup_filter');
        $invmkategori_filter = $request->get('invmkategori_filter');

        $result = Inventory::getMaster($start, $length, $search, $orderby, $filter, $datefilter, $datefilter2, $invmgroup_filter, $invmkategori_filter);
        $data = $result['data'];
        $allnoid = Inventory::getAllNoid(static::$main['table']);

        $resulttotal = count(Inventory::getMaster(null, null, $search, $orderby, $filter, $datefilter, $datefilter2, $invmgroup_filter, $invmkategori_filter)['data']);

        $resultsnst = Inventory::getMTypeTrancTypeStatus(static::$main['idtypetranc']);
        foreach ($resultsnst as $k => $v) {
            $snst[$v->nourut] = $v;
            if($v->isinternal == 0 && $v->isexternal != 0) {
                $sapcf[] = $v;
            }
        }

        $conditions = Session::get(\Request::segment(1).'-conditions');
        $no = $start+1;
        foreach ($data as $k => $v) {
            $checkbox['checkbox'] = '<input type="checkbox" value="FALSE" class="checkbox-m" id="checkbox-m-'.$v->noid.'" onchange="selectCheckboxM('.$v->noid.')">';
            $_no['no'] = $no;
            $array = (array)$data[$k];

            $isview = $privilege['isread'];
            $isedit = $privilege['isupdate'];
            $isdelete = $privilege['isdelete'];

            $array['istaxed'] = '<input type="checkbox" '.($array['istaxed'] == 1 ? 'checked' : '').' onclick="return false;">';
            $array['isjual'] = '<input type="checkbox" '.($array['isjual'] == 1 ? 'checked' : '').' onclick="return false;">';
            $array['isbeli'] = '<input type="checkbox" '.($array['isbeli'] == 1 ? 'checked' : '').' onclick="return false;">';
            $array['issimpan'] = '<input type="checkbox" '.($array['issimpan'] == 1 ? 'checked' : '').' onclick="return false;">';
            $array['isnote'] = '<input type="checkbox" '.($array['isnote'] == 1 ? 'checked' : '').' onclick="return false;">';
            $array['issystem'] = '<input type="checkbox" '.($array['issystem'] == 1 ? 'checked' : '').' onclick="return false;">';
            $array['isactive'] = '<input type="checkbox" '.($array['isactive'] == 1 ? 'checked' : '').' onclick="return false;">';

            $btnview = $isview ? '<button id="btn-view-data-'.$v->noid.'" onclick="viewData('.$v->noid.')" class="btn btn-info btn-sm flat" data-toggle="tooltip" data-theme="dark" title="View"><i class="fa fa-eye icon-xs"></i> </button>' : '';
            $btnedit = $isedit ? '<button id="btn-edit-data-'.$v->noid.'" onclick="editData('.$v->noid.')" class="btn btn-warning btn-sm flat" data-toggle="tooltip" data-theme="dark" title="Edit"><i class="fa fa-edit icon-xs" style="text-align:center"></i> </button>' : '';
            $btndelete = $isdelete ? '<button id="btn-delete-data-'.$v->noid.'" onclick="showModalDelete('.$v->noid.')" class="btn btn-danger btn-sm flat" data-toggle="tooltip" data-theme="dark" title="Delete"><i class="fa fa-trash icon-xs"></i> </button>' : '';

            $allbtn = @$btnprev;
            $allbtn .= @$btnnext;
            $allbtn .= @$btnsapcf;
            $allbtn .= @$btnprint;
            $allbtn .= @$btnlog;
            $allbtn .= @$btnview;
            $allbtn .= @$btnedit;
            $allbtn .= @$btndelete;

            $action = [
                'action' => '<div class="d-flex align-items-center pull-right button-action button-action-'.$k.'">
                    '.$allbtn.'
                </div>'
            ];
            $data[$k] = (object)array_merge($checkbox, $_no, $array, $action);
            $no++;
        }
        
        return response()->json([
            'data' => $data,
            'draw' => intval($request['draw']),
            'recordsFiltered' => $resulttotal,
            'recordsTotal' => $resulttotal,
            'allnoid' => $allnoid,
            'datefilter' => $result['datefilter']
        ]);
    }

    public function get_select2ajax(Request $request){
        $table = $request->table;
        $idparent = @$request->idparent;
        $fieldparent = @$request->fieldparent;
        $search = $request->search;
        $page = $request->page;
        $limit = 25;
        $offset = ($page-1)*$limit;

        $where = [];
        if ($fieldparent) {
            $where[] = [$fieldparent,'=',$idparent];
        }

        // $where[] = ['nama','like',"%$search%"];
        // if (is_null($idparent)) {
        //     $datas = Inventory::getData(['table'=>$table,'select'=>['noid','kode','nama'],'orderby'=>[['nourut','asc']],'offset'=>$offset,'limit'=>$limit,'isactive'=>true]);
        // } else {
            $where[] = ['nama','like',"%$search%"];
            $datas = Inventory::getData(['table'=>$table,'select'=>['noid','kode','nama'],'where'=>$where,'orderby'=>[['nourut','asc']],'offset'=>$offset,'limit'=>$limit,'isactive'=>true]);
        // }

        $results = [];
        foreach($datas as $k => $v){
            $results[] = array(
                'id' => $v->noid,
                'kode' => $v->kode,
                'text' => $v->nama
            );
        }

        $more = count($results) == $limit ? true : false;

        $response = [
            'results' => $results,
            'pagination' => [
                'more' => $more
            ]
        ];
        return response()->json($response);
    }

    public function save(Request $request) {
        $req = $request->all();

        $form = [];
        foreach ($req['form'] as $k => $v) {
            $form[$v['name']] = $v['value'];
        }
        $form['idinvmgroup'] = $form['idinvmgroup_input'];
        unset($form['idinvmgroup_input']);
        $form['idinvmkategori'] = $form['idinvmkategori_input'];
        unset($form['idinvmkategori_input']);
        $form['idinvmmerk'] = $form['idinvmmerk_input'];
        unset($form['idinvmmerk_input']);

        $form['hpp'] = filter_var($form['hpp'], FILTER_SANITIZE_NUMBER_INT);
        $form['salesprice'] = filter_var($form['salesprice'], FILTER_SANITIZE_NUMBER_INT);
        $form['purchaseprice'] = filter_var($form['purchaseprice'], FILTER_SANITIZE_NUMBER_INT);

        $form['istaxed'] = (array_key_exists('istaxed', $form) && $form['istaxed'] == 'on') ? 1 : 0;
        $form['taxprocent'] = is_null($form['taxprocent']) ? 0 : $form['taxprocent'];
        $form['isjual'] = (array_key_exists('isjual', $form) && $form['isjual'] == 'on') ? 1 : 0;
        $form['isbeli'] = (array_key_exists('isbeli', $form) && $form['isbeli'] == 'on') ? 1 : 0;
        $form['issimpan'] = (array_key_exists('issimpan', $form) && $form['issimpan'] == 'on') ? 1 : 0;
        $form['isnote'] = (array_key_exists('isnote', $form) && $form['isnote'] == 'on') ? 1 : 0;
        $form['issystem'] = (array_key_exists('issystem', $form) && $form['issystem'] == 'on') ? 1 : 0;
        $form['isactive'] = (array_key_exists('isactive', $form) && $form['isactive'] == 'on') ? 1 : 0;

        if ($req['statusadd'] == 'true') {
            $form['noid'] = Inventory::getNextNoid(static::$main['table']);
            // $form['nourut'] = Inventory::getNextNourut(static::$main['table']);
            $form['idcreate'] = Session::get('noid');
            $form['docreate'] = date('Y-m-d H:i:s');

            $result = Inventory::saveData(['form' => $form]);
        } else {
            $form['idupdate'] = Session::get('noid');
            $form['lastupdate'] = date('Y-m-d H:i:s');
            $result = Inventory::updateData(['form' => $form, 'noid' => $req['noid']]);
        }

        return ['success' => $result['success'], 'message' => $result['message']];
    }

    public function find(Request $request) {
        $noid = $request->get('noid');

        $result = Inventory::findData($noid);
        if (!$result) return ['success' => $result['success'], 'message' => $result['message']];

        return ['success' => true, 'message' => 'Success Find Data', 'data' => $result['data']];
    }
    
    public function generate_code(Request $request) {
        $idinvmgroup = @$request->idinvmgroup ? $request->idinvmgroup : 0;
        $idinvmkategori = @$request->idinvmkategori ? $request->idinvmkategori : 0;
        $idinvmmerk = @$request->idinvmmerk ? $request->idinvmmerk : 0;
        $exceptcode = $request->exceptcode;

        $result = Inventory::generateCode([
            'idinvmgroup' => $idinvmgroup,
            'idinvmkategori' => $idinvmkategori,
            'idinvmmerk' => $idinvmmerk,
            'exceptcode' => $exceptcode
        ]);
        
        return ['success' => $result['success'], 'message' => $result['message'], 'data' => @$result['data']];
        
        $noid = $request->get('noid');
        $nextnoid = MP::getNextNoid(static::$main['table']);

        foreach ($request->get('formheader') as $k => $v) {
            if ($k == 'kodetypetranc') {
                continue;
            } else if ($k == 'idcostcontrol') {
                $formheader[$k] = $v ? $v : 0;
            } else if ($k == 'dostatus' || $k == 'tanggal' || $k == 'tanggaldue') {
                $formheader[$k] = $this->humandateTodbdate($v);
            } else if ($k == 'idstatustranc') {
                $formheader[$k] = @$next_idstatustranc ? $next_idstatustranc : 5011;
            } else {
                $formheader[$k] = $v;
            }
        }
        // $nextnourut = MP::getNextNourut(['table'=>static::$main['table'],'where'=>$formheader['idcostcontrol']]);
        // $formheader['nourut'] = $nextnourut;

        foreach ($request->get('formmodaltanggal') as $k => $v) {
            $formmodaltanggal[$k] = $this->humandateTodbdate($v);
        }
        foreach ($request->get('formfooter') as $k => $v) {
            if ($k == 'subtotal') {
                $formfooter['totalsubtotal'] = filter_var($v, FILTER_SANITIZE_NUMBER_INT);
            } else if ($k == 'total') {
                $formfooter['generaltotal'] = filter_var($v, FILTER_SANITIZE_NUMBER_INT);
            } else {
                $formfooter[$k] = $v;
            }
        }

        $input = [
            'formheader' => $formheader,
            'forminternal' => $request->get('forminternal'),
            'formfooter' => $formfooter,
            'formmodaltanggal' => $formmodaltanggal,
            'formmodalsupplier' => $request->get('formmodalsupplier'),
            'prd' => $request->get('purchasedetail')
        ];

        $table = array_merge(
            [
                'noid' => $nextnoid,
                'idtypetranc' => 0,
                'idcreate' => Session::get('noid'),
                'docreate' => date('Y-m-d H:i:s'),
                'idupdate' => Session::get('noid'),
                'lastupdate' => date('Y-m-d H:i:s'),
            ],
            $input['formheader'],
            $input['forminternal'],
            $input['formmodaltanggal'],
            $input['formmodalsupplier'],
            $input['formfooter']
        );

        $params = [
            'tablemaster' => static::$main['table'],
            'generatecode' => static::$main['generatecode'],
            'idcostcontrol' => $request->get('idcostcontrol'),
            'kodecostcontrol' => $request->get('kodecostcontrol'),
            'data' => $table
        ];
        
        $result = MP::generateCode($params);

        return response()->json([
            'success' => true,
            'message' => 'Find Successfully',
            'data' => $result
        ]);
    }

    public function delete_select_m(Request $request) {
        return Inventory::deleteSelectM([
            'selecteds' => $request->get('selecteds'), 
            'selectcheckboxmall' => $request->get('selectcheckboxmall'), 
            'datalog' => null
        ]);
    }

    public function delete(Request $request) {
        return Inventory::deleteData(['noid' => $request->get('noid')]);
    }
}