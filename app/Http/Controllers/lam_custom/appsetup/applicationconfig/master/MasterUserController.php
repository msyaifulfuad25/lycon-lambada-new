<?php

namespace App\Http\Controllers\lam_custom\appsetup\applicationconfig\master;

use App\Http\Controllers\Controller;
use App\Model\lam_custom\appsetup\applicationconfig\master\MasterUser;
use App\Model\RA\RAP as MP2;
use App\Model\Sales\SalesOrder;
use App\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Helpers\MyHelper;
use DB;
use Image;
use Storage;

class MasterUserController extends Controller {

    public $color = [
        'white' => '#FFFFFF',
        'default' => '#E1E5EC',
        'dark' => '#2F353B',
        'blue' => '#3598DC',
        'blue-madison' => '#578EBE',
        'blue-chambray' => '#2C3E50',
        'blue-ebonyclay' => '#22313F',
        'blue-hoki' => '#67809F',
        'blue-steel' => '#4B77BE',
        'blue-soft' => '#4C87B9',
        'blue-dark' => '#5E738B',
        'blue-sharp' => '#5C9BD1',
        'blue-oleo' => '#94A0B2',
        'green' => '#32C5D2',
        'green-meadow' => '#1BBC9B',
        'green-seagreen' => '#1BA39C',
        'green-torquoise' => '#36D7B7',
        'green-haze' => '#44B6AE',
        'green-jungle' => '#26C281',
        'green-soft' => '#3FABA4',
        'green-dark' => '#4DB3A2',
        'green-sharp' => '#2AB4C0',
        'green-steel' => '#29B4B6',
        'grey' => '#E5E5E5',
        'grey-steel' => '#E9EDEF',
        'grey-cararra' => '#FAFAFA',
        'grey-gallery' => '#555555',
        'grey-cascade' => '#95A5A6',
        'grey-silver' => '#BFBFBF',
        'grey-salsa' => '#ACB5C3',
        'grey-salt' => '#BFCAD1',
        'grey-mint' => '#525E64',
        'red' => '#E7505A',
        'red-pink' => '#E08283',
        'red-sunglo' => '#E26A6A',
        'red-intense' => '#E35B5A',
        'red-thunderbird' => '#D91E18',
        'red-flamingo' => '#EF4836',
        'red-soft' => '#D05454',
        'red-haze' => '#F36A5A',
        'red-mint' => '#E43A45',
        'yellow' => '#C49F47',
        'yellow-gold' => '#E87E04',
        'yellow-casablanca' => '#F2784B',
        'yellow-crusta' => '#F3C200',
        'yellow-lemon' => '#F7CA18',
        'yellow-saffron' => '#F4D03F',
        'yellow-soft' => '#C8D046',
        'yellow-haze' => '#C5BF66',
        'yellow-mint' => '#C5B96B',
        'purple' => '#8E44AD',
        'purple-plum' => '#8775A7',
        'purple-medium' => '#BF55EC',
        'purple-studio' => '#8E44AD',
        'purple-wisteria' => '#9B59B6',
        'purple-seance' => '#9A12B3',
        'purple-intense' => '#8775A7',
        'purple-sharp' => '#796799',
        'purple-soft' => '#8877A9',
    ];
    public static $main = [
        'table' => 'mcarduser',
        'table2' => 'mcard',
        // 'tablemilestone' => 'prjmrabmilestone',
        // 'tableheader' => 'prjmrabheader',
        // 'tabledetail' => 'prjmrabdetail',
        // 'tablecdf' => 'prjmrabdocument',
        'table-title' => 'Master User',
        // 'table2' => 'prjmrap',
        // 'tabledetail2' => 'prjmrapdetail',
        // 'table2-title' => 'RAP',
        'generatecode' => 'generatecode/602/1',
        'generatecode2' => 'generatecode/604/1',
        'idtypetranc' => 602,
        'idtypetranc2' => 602,
        'noid' => 99020202,
        'noidnew' => 50011101,
        'printcode' => 'print_purchase_request',
        'condition' => [
            'mttts' => [
                [],
                [
                    'mttts' => 'draft',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => true,
                        'd' => true,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => true,
                        'd' => true,
                    ],
                    'draft' => false,
                    'issued' => true,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => false,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'issued',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => true,
                    'issued' => false,
                    'verified' => true,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => false,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'verified',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => true,
                    'verified' => false,
                    'approved' => true,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => false,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'approved',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'pending',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'canceled',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'finished',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
            ],
        ]
    ];

    public function index() {
        // dd(Menu::where('menulevel',2)->where('isactive',1)->where('idparent',40)->groupBy('groupmenu')->get());
        // dd(Menu::getMenuLevel(['menulevel'=>2,'idparent'=>40,'groupby'=>'groupmenu']));
        $isroot = Session::get('groupuser') == 0 ? true : false;

        $tabletitle = static::$main['table-title'];
        $breadcrumb = Menu::getBreadcrumb(static::$main['noid']);
        $pagenoid = static::$main['noid'];
        $idtypepage = static::$main['noid'];
        $color = (new static)->color;
        $maintable = static::$main['table'];
        $datefilter = [
            'a' => 'All',
            't' => date('M d, Y').' - '.date('M d, Y'),
            'y' => date('M d, Y', strtotime('-1 day')).' - '.date('M d, Y', strtotime('-1 day')),
            'l7d' => date('M d, Y').' - '.date('M d, Y', strtotime('-7 day')),
            'l30d' => date('M d, Y').' - '.date('M d, Y', strtotime('1 month')),
            'tm' => date('M d, Y', strtotime('first day of this month')).' - '.date('M d, Y', strtotime('last day of this month')),
            'lm' => date('M d, Y', strtotime('first day of previous month')).' - '.date('M d, Y', strtotime('last day of previous month')),
        ];
        $datefilterdefault = 'a';
        $filteris = [
            (object)[
                'noid' => 'all',
                'nama' => 'ALL',
                'active' => false
            ],
            (object)[
                'noid' => 'isgroupuser',
                'nama' => 'Group User',
                'active' => false
            ],
            (object)[
                'noid' => 'isuser',
                'nama' => 'User',
                'active' => false
            ],
            (object)[
                'noid' => 'isemployee',
                'nama' => 'Employee',
                'active' => false
            ],
            (object)[
                'noid' => 'iscustomer',
                'nama' => 'Customer',
                'active' => true
            ],
            (object)[
                'noid' => 'issupplier',
                'nama' => 'Supplier',
                'active' => false
            ],
            (object)[
                'noid' => 'is3rparty',
                'nama' => '3rparty',
                'active' => false
            ],
        ];
        $genmcompany = MasterUser::getData(['table'=>'genmcompany','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $genmdepartment = MasterUser::getData(['table'=>'genmdepartment','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $genmgudang = MasterUser::getData(['table'=>'genmgudang','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $mtypecard = MasterUser::getData(['table'=>'mtypecard','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $mlokasiprop = MasterUser::getData(['table'=>'mlokasiprop','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $mlokasikab = MasterUser::getData(['table'=>'mlokasikab','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $mlokasikec = MasterUser::getData(['table'=>'mlokasikec','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $mlokasikel = MasterUser::getData(['table'=>'mlokasikel','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $genmgender = MasterUser::getData(['table'=>'genmgender','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $genmreligion = MasterUser::getData(['table'=>'genmreligion','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $genmmarital = MasterUser::getData(['table'=>'genmmarital','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $genmpnsstatus = MasterUser::getData(['table'=>'_genmpnsstatus','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $genmnationality = MasterUser::getData(['table'=>'genmnationality','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $genmrace = MasterUser::getData(['table'=>'genmrace','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $genmbloodtype = MasterUser::getData(['table'=>'genmbloodtype','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $genmlanguage = MasterUser::getData(['table'=>'genmlanguage','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $genmlokasi = MasterUser::getData(['table'=>'genmlokasi','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $genmtypeaddress = MasterUser::getData(['table'=>'genmtypeaddress','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $genmtypecontact = MasterUser::getData(['table'=>'genmtypecontact','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);

        $privilege = $this->newPrivilege();
        $isadd = $privilege['iscreate'] ? 1 : 0;
        $sessionname = Session::get('nama');

        return view('lam_custom.appsetup.applicationconfig.master.masteruser.index', compact(
            'isroot',
            'tabletitle',
            'breadcrumb',
            'pagenoid',
            'idtypepage',
            'color',
            'maintable',
            'datefilter',
            'datefilterdefault',
            'filteris',
            'genmcompany',
            'genmdepartment',
            'genmgudang',
            'mtypecard',
            'mlokasiprop',
            'mlokasikab',
            'mlokasikec',
            'mlokasikel',
            'genmgender',
            'genmreligion',
            'genmmarital',
            'genmpnsstatus',
            'genmnationality',
            'genmrace',
            'genmbloodtype',
            'genmlanguage',
            'genmlokasi',
            'genmtypeaddress',
            'genmtypecontact',
            'privilege',
            'isadd',
            'sessionname'
        ));
    }
    
    public function newPrivilege() {
        return MyHelper::getNewPrivilege([
            'idmenu' => static::$main['noid'],
            'idcard' => Session::get('noid'),
            'isroot' => Session::get('groupuser') == 0 ? true : false
        ]);
    }

    public function get(Request $request) {
        $isroot = Session::get('groupuser') == 0 ? true : false;
        $employeecode = Session::get('myusername');
        $privilege = $this->newPrivilege();
        
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->get('search')['value'];
        $orderbycolumn = $request->get('columns')[$request->get('order')[0]['column']]['data'];
        if ($orderbycolumn == 'checkbox' || $orderbycolumn == 'no') {
            $orderbycolumn = 'noid';
        }
        $orderby = [
            'column' => $orderbycolumn,
            'sort' => $request->get('order')[0]['dir']
        ];
        $filter = $request->get('filter');
        $datefilter = $request->get('datefilter');
        $datefilter2 = $request->get('datefilter2');

        $result = MasterUser::getMaster($start, $length, $search, $orderby, $filter, $datefilter, $datefilter2, $isroot, $employeecode, $privilege['isnotonlydisplayinghisdata']);
        $data = $result['data'];
        $allnoid = MasterUser::getAllNoid(static::$main['table']);

        $resulttotal = count(MasterUser::getMaster(null, null, $search, $orderby, $filter, $datefilter, $datefilter2, $isroot, $employeecode, $privilege['isnotonlydisplayinghisdata'])['data']);

        $resultsnst = MasterUser::getMTypeTrancTypeStatus(static::$main['idtypetranc']);
        foreach ($resultsnst as $k => $v) {
            $snst[$v->nourut] = $v;
            if($v->isinternal == 0 && $v->isexternal != 0) {
                $sapcf[] = $v;
            }
        }

        $conditions = Session::get(static::$main['noid'].'-conditions');
        $no = $start+1;
        foreach ($data as $k => $v) {
            $checkbox['checkbox'] = '<input type="checkbox" value="FALSE" class="checkbox-m" id="checkbox-m-'.$v->noid.'" onchange="selectCheckboxM('.$v->noid.')">';
            $_no['no'] = $no;
            $array = (array)$data[$k];
            
            $isview = $privilege['isread'];
            $isedit = $privilege['isupdate'];
            $isdelete = $privilege['isdelete'];
            // $array['isgroupuser'] = '<input type="checkbox" '.($array['isgroupuser'] == 1 ? 'checked' : '').' onclick="return false;">';
            // $array['isuser'] = '<input type="checkbox" '.($array['isuser'] == 1 ? 'checked' : '').' onclick="return false;">';
            // $array['isemployee'] = '<input type="checkbox" '.($array['isemployee'] == 1 ? 'checked' : '').' onclick="return false;">';
            // $array['iscustomer'] = '<input type="checkbox" '.($array['iscustomer'] == 1 ? 'checked' : '').' onclick="return false;">';
            // $array['issupplier'] = '<input type="checkbox" '.($array['issupplier'] == 1 ? 'checked' : '').' onclick="return false;">';
            // $array['is3rparty'] = '<input type="checkbox" '.($array['is3rparty'] == 1 ? 'checked' : '').' onclick="return false;">';
            $array['isactive'] = '<input type="checkbox" '.($array['isactive'] == 1 ? 'checked' : '').' onclick="return false;">';
            
            
            $btnview = $isview ? '<button id="btn-view-data-'.$v->noid.'" onclick="viewData('.$v->noid.')" class="btn btn-info btn-sm flat" data-toggle="tooltip" data-theme="dark" title="View"><i class="fa fa-eye icon-xs"></i> </button>' : '';
            $btnedit = $isedit ? '<button id="btn-edit-data-'.$v->noid.'" onclick="editData('.$v->noid.')" class="btn btn-warning btn-sm flat" data-toggle="tooltip" data-theme="dark" title="Edit"><i class="fa fa-edit icon-xs" style="text-align:center"></i> </button>' : '';
            $btndelete = $isdelete ? '<button id="btn-delete-data-'.$v->noid.'" onclick="showModalDelete('.$v->noid.')" class="btn btn-danger btn-sm flat" data-toggle="tooltip" data-theme="dark" title="Delete"><i class="fa fa-trash icon-xs"></i> </button>' : '';

            $allbtn = @$btnprev;
            $allbtn .= @$btnnext;
            $allbtn .= @$btnsapcf;
            $allbtn .= @$btnprint;
            $allbtn .= @$btnlog;
            $allbtn .= @$btnview;
            $allbtn .= @$btnedit;
            $allbtn .= @$btndelete;

            $action = [
                'action' => '<div class="d-flex align-items-center pull-right button-action button-action-'.$k.'">
                    '.$allbtn.'
                </div>'
            ];
            $data[$k] = (object)array_merge($checkbox, $_no, $array, $action);
            $no++;
        }
        
        return response()->json([
            'data' => $data,
            'draw' => intval($request['draw']),
            'recordsFiltered' => $resulttotal,
            'recordsTotal' => $resulttotal,
            'allnoid' => $allnoid,
            'datefilter' => $result['datefilter']
        ]);
    }

    public function save(Request $request) {
        $isadd = $request->statusadd == 'true' ? true : false;
        $child = json_decode($request->child);
        
        $form = [];
        $formmcard = [];
        if ($isadd) {
            $form['noid'] = MasterUser::getNextNoid(static::$main['table']);
            $formmcard['noid'] = MasterUser::getNextNoid(static::$main['table2']);
            $form['id'] = $formmcard['noid'];
        } else {
            $form['noid'] = $request->noid;
        }
        
        $password = null;
        $confirmpassword = null;
        foreach ($request->formgeneral as $k => $v) {
            if ($v['name'] == 'username') {
                if ($isadd) {
                    $checkexistcode = MasterUser::checkExistUsername($v['value']);
                    if (!$checkexistcode['success']) {
                        return ['success' => false, 'message' => 'Username sudah digunakan. Mohon diganti'];
                    }
                }
                $form['myusername'] = $v['value'];
            } else if ($v['name'] == 'group'){
                $form['groupuser'] = $v['value'];
            } else if ($v['name'] == 'password'){
                if ($isadd) {
                    if (is_null($v['value'])) return ['success' => false, 'message' => 'Password is required!'];
                    if (strlen($v['value']) < 6) return ['success' => false, 'message' => 'Password minimal 6 karakter!'];
                    $form['mypassword'] = sha1($v['value']);
                    $password = $v['value'];
                } else {
                    if (!is_null($v['value'])) {
                        if (strlen($v['value']) < 6) return ['success' => false, 'message' => 'Password minimal 6 karakter!'];
                        $form['mypassword'] = sha1($v['value']);
                        $password = $v['value'];
                    }
                }
            } else if ($v['name'] == 'confirmpassword'){
                if ($isadd) {
                    if (is_null($v['value'])) return ['success' => false, 'message' => 'Confirm Password is required!'];
                    if (strlen($v['value']) < 6) return ['success' => false, 'message' => 'Konfirmasi Password minimal 6 karakter!'];
                    $confirmpassword = $v['value'];
                } else {
                    if (!is_null($password)) {
                        if (is_null($v['value'])) return ['success' => false, 'message' => 'Confirm Password is required!'];
                        if (strlen($v['value']) < 6) return ['success' => false, 'message' => 'Konfirmasi Password minimal 6 karakter!'];
                        $confirmpassword = $v['value'];
                    }
                }
            } else if ($v['name'] == 'firstname' || $v['name'] == 'middlename' || $v['name'] == 'lastname' || $v['name'] == 'email'){
                $formmcard[$v['name']] = $v['value'];
            } else {
                $form[$v['name']] = $v['value'];
            }
        }
        $formmcard['kode'] = $form['myusername'];
        $formmcard['nama'] = $formmcard['firstname'].' '.$formmcard['middlename'].' '.$formmcard['lastname'];
        $formmcard['isemployee'] = 1;

        if ($isadd) {
            if ($password != $confirmpassword) return ['success' => false, 'message' => 'Konfirmasi Password tidak cocok!'];
        }

        $form['isactive'] = array_key_exists('isactive', $form) ? 1 : 0;

        if ($isadd) {
            $form['idcreate'] = Session::get('noid');
            $form['docreate'] = date('Y-m-d H:i:s');
            $formmcard['idcreate'] = Session::get('noid');
            $formmcard['docreate'] = date('Y-m-d H:i:s');

            $result = MasterUser::saveData([
                'form' => $form,
                'formmcard' => $formmcard,
            ]);
        } else {
            $form['idupdate'] = Session::get('noid');
            $form['lastupdate'] = date('Y-m-d H:i:s');
            $formmcard['idupdate'] = Session::get('noid');
            $formmcard['lastupdate'] = date('Y-m-d H:i:s');

            $result = MasterUser::updateData([
                'noid' => $request->noid,
                'form' => $form,
                'formmcard' => $formmcard,
            ]);
        }

        return $result;
    }

    public function save_(Request $request) {
        $req = $request->all();

        // DECLARE
        $form = [];
        $formgeneral = [];
        $formaddress = [];
        $formcontact = [];

        // FOREACH
        foreach ($req['formmcard'] as $k => $v) {
            $form[$v['name']] = $v['value'];
        }
        foreach ($req['formmcardgeneral'] as $k => $v) {
            $formgeneral[str_replace('mcardgeneral_', '', $v['name'])] = $v['value'];
        }
        $formgeneral['dobirth'] = MyHelper::humandateTodbdate($formgeneral['dobirth']);
        foreach ($req['formmcardaddress'] as $k => $v) {
            $formaddress[str_replace('mcardaddress_', '', $v['name'])] = $v['value'];
        }
        foreach ($req['formmcardcontact'] as $k => $v) {
            $formcontact[str_replace('mcardcontact_', '', $v['name'])] = $v['value'];
        }

        // CHECKBOX
        $form['nama'] = $form['firstname'].' '.$form['middlename'].' '.$form['lastname'];
        $form['isgroupuser'] = (array_key_exists('isgroupuser', $form)) ? 1 : 0;
        $form['isuser'] = (array_key_exists('isuser', $form)) ? 1 : 0;
        $form['isemployee'] = (array_key_exists('isemployee', $form)) ? 1 : 0;
        $form['iscustomer'] = (array_key_exists('iscustomer', $form)) ? 1 : 0;
        $form['issupplier'] = (array_key_exists('issupplier', $form)) ? 1 : 0;
        $form['is3rparty'] = (array_key_exists('is3rparty', $form)) ? 1 : 0;
        $form['isactive'] = (array_key_exists('isactive', $form)) ? 1 : 0;
        $formaddress['ismaincontact'] = (array_key_exists('ismaincontact', $formaddress)) ? 1 : 0;

        // REPLACE
        $formuser = [];
        $formprivilidge = [];
        $formemployee = [];
        $formcustomer = [];
        $formsupplier = [];

        if ($form['isuser']) {
            foreach ($req['formuser'] as $k => $v) {
                $formuser[str_replace('mcarduser_', '', $v['name'])] = $v['value'];
            }
            $formuser['mypassword'] = sha1($formuser['mypassword']);
            foreach ($req['formprivilidge'] as $k => $v) {
                $formprivilidge[str_replace('mcardprivilidge_', '', $v['name'])] = $v['value'];
            }
            $formprivilidge['isvisible'] = (array_key_exists('isvisible', $formprivilidge)) ? 1 : 0;
            $formprivilidge['isenable'] = (array_key_exists('isenable', $formprivilidge)) ? 1 : 0;
            $formprivilidge['isadd'] = (array_key_exists('isadd', $formprivilidge)) ? 1 : 0;
            $formprivilidge['isedit'] = (array_key_exists('isedit', $formprivilidge)) ? 1 : 0;
            $formprivilidge['isdelete'] = (array_key_exists('isdelete', $formprivilidge)) ? 1 : 0;
            $formprivilidge['isdeleteother'] = (array_key_exists('isdeleteother', $formprivilidge)) ? 1 : 0;
            $formprivilidge['isview'] = (array_key_exists('isview', $formprivilidge)) ? 1 : 0;
            $formprivilidge['isreport'] = (array_key_exists('isreport', $formprivilidge)) ? 1 : 0;
            $formprivilidge['isreportdetail'] = (array_key_exists('isreportdetail', $formprivilidge)) ? 1 : 0;
            $formprivilidge['isconfirm'] = (array_key_exists('isconfirm', $formprivilidge)) ? 1 : 0;
            $formprivilidge['isunconfirm'] = (array_key_exists('isunconfirm', $formprivilidge)) ? 1 : 0;
            $formprivilidge['ispost'] = (array_key_exists('ispost', $formprivilidge)) ? 1 : 0;
            $formprivilidge['isunpost'] = (array_key_exists('isunpost', $formprivilidge)) ? 1 : 0;
            $formprivilidge['isprintdaftar'] = (array_key_exists('isprintdaftar', $formprivilidge)) ? 1 : 0;
            $formprivilidge['isprintdetail'] = (array_key_exists('isprintdetail', $formprivilidge)) ? 1 : 0;
            $formprivilidge['isshowaudit'] = (array_key_exists('isshowaudit', $formprivilidge)) ? 1 : 0;
            $formprivilidge['isshowlog'] = (array_key_exists('isshowlog', $formprivilidge)) ? 1 : 0;
            $formprivilidge['isexporttoxcl'] = (array_key_exists('isexporttoxcl', $formprivilidge)) ? 1 : 0;
            $formprivilidge['isexporttocsv'] = (array_key_exists('isexporttocsv', $formprivilidge)) ? 1 : 0;
            $formprivilidge['isexporttopdf'] = (array_key_exists('isexporttopdf', $formprivilidge)) ? 1 : 0;
            $formprivilidge['isexporttohtml'] = (array_key_exists('isexporttohtml', $formprivilidge)) ? 1 : 0;
            $formprivilidge['iscustom1'] = (array_key_exists('iscustom1', $formprivilidge)) ? 1 : 0;
            $formprivilidge['iscustom2'] = (array_key_exists('iscustom2', $formprivilidge)) ? 1 : 0;
            $formprivilidge['iscustom3'] = (array_key_exists('iscustom3', $formprivilidge)) ? 1 : 0;
            $formprivilidge['iscustom4'] = (array_key_exists('iscustom4', $formprivilidge)) ? 1 : 0;
            $formprivilidge['iscustom5'] = (array_key_exists('iscustom5', $formprivilidge)) ? 1 : 0;
            $formprivilidge['isproduction01'] = (array_key_exists('isproduction01', $formprivilidge)) ? 1 : 0;
            $formprivilidge['isproduction02'] = (array_key_exists('isproduction02', $formprivilidge)) ? 1 : 0;
            $formprivilidge['isproduction03'] = (array_key_exists('isproduction03', $formprivilidge)) ? 1 : 0;
            $formprivilidge['isproduction04'] = (array_key_exists('isproduction04', $formprivilidge)) ? 1 : 0;
            $formprivilidge['isproduction05'] = (array_key_exists('isproduction05', $formprivilidge)) ? 1 : 0;
            $formprivilidge['isproduction06'] = (array_key_exists('isproduction06', $formprivilidge)) ? 1 : 0;
        }
        if ($form['isemployee']) {
            foreach ($req['formemployee'] as $k => $v) {
                $formemployee[str_replace('mce_', '', $v['name'])] = $v['value'];
            }
        }
        if ($form['iscustomer']) {
            foreach ($req['formcustomer'] as $k => $v) {
                $formcustomer[str_replace('mcc_', '', $v['name'])] = $v['value'];
            }
        }
        if ($form['issupplier']) {
            foreach ($req['formsupplier'] as $k => $v) {
                $formsupplier[str_replace('mcs_', '', $v['name'])] = $v['value'];
            }
        }

        // CREATE OR UPDATE
        if ($req['statusadd'] == 'true') {
            $form['noid'] = MasterUser::getNextNoid(static::$main['table']);
            $form['idcreate'] = Session::get('noid');
            $form['docreate'] = date('Y-m-d H:i:s');

            if ($formgeneral) {
                $formgeneral['noid'] = $form['noid'];
                $formgeneral['idcreate'] = Session::get('noid');
                $formgeneral['docreate'] = date('Y-m-d H:i:s');
            }
            if ($formaddress) {
                $formaddress['noid'] = $form['noid'];
                $formaddress['idcreate'] = Session::get('noid');
                $formaddress['docreate'] = date('Y-m-d H:i:s');
            }
            if ($formcontact) {
                $formcontact['noid'] = $form['noid'];
                $formcontact['idcreate'] = Session::get('noid');
                $formcontact['docreate'] = date('Y-m-d H:i:s');
            }
            if ($formuser) {
                $formuser['noid'] = $form['noid'];
                $formuser['idcreate'] = Session::get('noid');
                $formuser['docreate'] = date('Y-m-d H:i:s');
            }
            if ($formprivilidge) {
                $formprivilidge['noid'] = $form['noid'];
                $formprivilidge['idcreate'] = Session::get('noid');
                $formprivilidge['docreate'] = date('Y-m-d H:i:s');
            }
            if ($formemployee) {
                $formemployee['noid'] = $form['noid'];
                $formemployee['idcreate'] = Session::get('noid');
                $formemployee['docreate'] = date('Y-m-d H:i:s');
            }
            if ($formcustomer) {
                $formcustomer['noid'] = $form['noid'];
                $formcustomer['idcreate'] = Session::get('noid');
                $formcustomer['docreate'] = date('Y-m-d H:i:s');
            }
            if ($formsupplier) {
                $formsupplier['noid'] = $form['noid'];
                $formsupplier['idcreate'] = Session::get('noid');
                $formsupplier['docreate'] = date('Y-m-d H:i:s');
            }

            $result = MasterUser::saveData([
                'form' => $form,
                'formgeneral' => $formgeneral,
                'formaddress' => $formaddress,
                'formcontact' => $formcontact,
                'formuser' => $formuser,
                'formprivilidge' => $formprivilidge,
                'formemployee' => $formemployee,
                'formcustomer' => $formcustomer,
                'formsupplier' => $formsupplier,
            ]);
        } else {
            $form['idupdate'] = Session::get('noid');
            $form['lastupdate'] = date('Y-m-d H:i:s');

            if ($formgeneral) {
                $formgeneral['noid'] = $req['noid'];
                $formgeneral['idupdate'] = Session::get('noid');
                $formgeneral['lastupdate'] = date('Y-m-d H:i:s');
                $formgeneral['idgender'] = is_null($formgeneral['idgender']) ? 0 : $formgeneral['idgender'];
                $formgeneral['idreligion'] = is_null($formgeneral['idreligion']) ? 0 : $formgeneral['idreligion'];
                $formgeneral['idmarital'] = is_null($formgeneral['idmarital']) ? 0 : $formgeneral['idgender'];
                $formgeneral['idnationality'] = is_null($formgeneral['idnationality']) ? 0 : $formgeneral['idnationality'];
                $formgeneral['idrace'] = is_null($formgeneral['idrace']) ? 0 : $formgeneral['idrace'];
                $formgeneral['idbloodtype'] = is_null($formgeneral['idbloodtype']) ? 0 : $formgeneral['idbloodtype'];
                $formgeneral['idlanguage1'] = is_null($formgeneral['idlanguage1']) ? 0 : $formgeneral['idlanguage1'];
                $formgeneral['idlanguage2'] = is_null($formgeneral['idlanguage2']) ? 0 : $formgeneral['idlanguage2'];
                $formgeneral['idlokasilahir'] = is_null($formgeneral['idlokasilahir']) ? 0 : $formgeneral['idlokasilahir'];
            }
            if ($formaddress) {
                $formaddress['noid'] = $req['noid'];
                $formaddress['idupdate'] = Session::get('noid');
                $formaddress['lastupdate'] = date('Y-m-d H:i:s');
            }
            if ($formcontact) {
                $formcontact['noid'] = $req['noid'];
                $formcontact['idupdate'] = Session::get('noid');
                $formcontact['lastupdate'] = date('Y-m-d H:i:s');
            }
            if ($formuser) {
                $formuser['noid'] = $req['noid'];
                $formuser['idupdate'] = Session::get('noid');
                $formuser['lastupdate'] = date('Y-m-d H:i:s');
            }
            if ($formprivilidge) {
                $formprivilidge['noid'] = $req['noid'];
                $formprivilidge['idupdate'] = Session::get('noid');
                $formprivilidge['lastupdate'] = date('Y-m-d H:i:s');
            }
            if ($formemployee) {
                $formemployee['noid'] = $req['noid'];
                $formemployee['idupdate'] = Session::get('noid');
                $formemployee['lastupdate'] = date('Y-m-d H:i:s');
            }
            if ($formcustomer) {
                $formcustomer['noid'] = $req['noid'];
                $formcustomer['idupdate'] = Session::get('noid');
                $formcustomer['lastupdate'] = date('Y-m-d H:i:s');
            }
            if ($formsupplier) {
                $formsupplier['noid'] = $req['noid'];
                $formsupplier['idupdate'] = Session::get('noid');
                $formsupplier['lastupdate'] = date('Y-m-d H:i:s');
            }

            $result = MasterUser::updateData([
                'form' => $form, 
                'formgeneral' => $formgeneral,
                'formaddress' => $formaddress,
                'formcontact' => $formcontact,
                'formuser' => $formuser,
                'formprivilidge' => $formprivilidge,
                'formemployee' => $formemployee,
                'formcustomer' => $formcustomer,
                'formsupplier' => $formsupplier,
                'noid' => $req['noid']
            ]);
        }

        return ['success' => $result['success'], 'message' => $result['message']];
    }

    public function find(Request $request) {
        $noid = $request->get('noid');

        $result = MasterUser::findData($noid);
        if (!$result) return ['success' => $result['success'], 'message' => $result['message']];

        $master = $result['data']['master'];
        $mcard = $result['data']['mcard'];

        return [
            'success' => true, 
            'message' => 'Success Find Data', 
            'data' => [
                'master' => $master,
                'mcard' => $mcard,
            ]
        ];
    }
    
    public function generate_code(Request $request) {
        $noid = $request->get('noid');
        $nextnoid = MP::getNextNoid(static::$main['table']);

        foreach ($request->get('formheader') as $k => $v) {
            if ($k == 'kodetypetranc') {
                continue;
            } else if ($k == 'idcostcontrol') {
                $formheader[$k] = $v ? $v : 0;
            } else if ($k == 'dostatus' || $k == 'tanggal' || $k == 'tanggaldue') {
                $formheader[$k] = $this->humandateTodbdate($v);
            } else if ($k == 'idstatustranc') {
                $formheader[$k] = @$next_idstatustranc ? $next_idstatustranc : 5011;
            } else {
                $formheader[$k] = $v;
            }
        }
        // $nextnourut = MP::getNextNourut(['table'=>static::$main['table'],'where'=>$formheader['idcostcontrol']]);
        // $formheader['nourut'] = $nextnourut;

        foreach ($request->get('formmodaltanggal') as $k => $v) {
            $formmodaltanggal[$k] = $this->humandateTodbdate($v);
        }
        foreach ($request->get('formfooter') as $k => $v) {
            if ($k == 'subtotal') {
                $formfooter['totalsubtotal'] = filter_var($v, FILTER_SANITIZE_NUMBER_INT);
            } else if ($k == 'total') {
                $formfooter['generaltotal'] = filter_var($v, FILTER_SANITIZE_NUMBER_INT);
            } else {
                $formfooter[$k] = $v;
            }
        }

        $input = [
            'formheader' => $formheader,
            'forminternal' => $request->get('forminternal'),
            'formfooter' => $formfooter,
            'formmodaltanggal' => $formmodaltanggal,
            'formmodalsupplier' => $request->get('formmodalsupplier'),
            'prd' => $request->get('purchasedetail')
        ];

        $table = array_merge(
            [
                'noid' => $nextnoid,
                'idtypetranc' => 0,
                'idcreate' => Session::get('noid'),
                'docreate' => date('Y-m-d H:i:s'),
                'idupdate' => Session::get('noid'),
                'lastupdate' => date('Y-m-d H:i:s'),
            ],
            $input['formheader'],
            $input['forminternal'],
            $input['formmodaltanggal'],
            $input['formmodalsupplier'],
            $input['formfooter']
        );

        $params = [
            'tablemaster' => static::$main['table'],
            'generatecode' => static::$main['generatecode'],
            'idcostcontrol' => $request->get('idcostcontrol'),
            'kodecostcontrol' => $request->get('kodecostcontrol'),
            'data' => $table
        ];
        
        $result = MP::generateCode($params);

        return response()->json([
            'success' => true,
            'message' => 'Find Successfully',
            'data' => $result
        ]);
    }

    public function delete_select_m(Request $request) {
        return MasterUser::deleteSelectM([
            'selecteds' => $request->get('selecteds'), 
            'selectcheckboxmall' => $request->get('selectcheckboxmall'), 
            'datalog' => null
        ]);
    }

    public function delete(Request $request) {
        return MasterUser::deleteData(['noid' => $request->get('noid')]);
    }

    public function get_select2ajax(Request $request){
        $table = $request->table;
        $idparent = @$request->idparent;
        $fieldparent = @$request->fieldparent;
        $search = $request->search;
        $page = $request->page;
        $limit = 25;
        $offset = ($page-1)*$limit;

        $where = [];
        if ($fieldparent) {
            $where[] = [$fieldparent,'=',$idparent];
        }

        $where[] = ['nama','like',"%$search%"];
        $datas = MasterUser::getData(['table'=>$table,'select'=>['noid','kode','nama'],'where'=>$where,'orderby'=>[['nourut','asc']],'offset'=>$offset,'limit'=>$limit,'isactive'=>true]);

        $results = [];
        foreach($datas as $k => $v){
            $results[] = array(
                'id' => $v->noid,
                'text' => $v->nama
            );
        }

        $more = count($results) == $limit ? true : false;

        $response = [
            'results' => $results,
            'pagination' => [
                'more' => $more
            ]
        ];
        return response()->json($response);
    }

    public function get_employeechild(Request $request) {
        $data = json_decode($request->data);

        if (count($data) == 0) {
            $idemployee = $request->noid;
            $employeechild = MasterUser::getEmployeeChild(['idemployee'=>$idemployee]);

            $fixdata = [];
            $no = 1;
            foreach ($employeechild['data'] as $k => $v) {
                $fixdata[$k]['no'] = $no;
                $fixdata[$k]['noid'] = $k;
                $fixdata[$k]['name'] = $v->name;
                $fixdata[$k]['gender'] = $v->gender;
                $fixdata[$k]['placeofbirth'] = $v->placeofbirth;
                $fixdata[$k]['dateofbirth'] = MyHelper::dbdateToHumandate($v->dateofbirth);
                $fixdata[$k]['education'] = $v->education;
                $noid = $fixdata[$k]['noid'];

                $btnview = '<button type="button" id="btn-view-child-'.$noid.'" onclick="viewChild({
                    noid: '.$noid.',
                    no: '.$fixdata[$k]['no'].',
                    name: \''.$fixdata[$k]['name'].'\',
                    gender: \''.$fixdata[$k]['gender'].'\',
                    placeofbirth: \''.$fixdata[$k]['placeofbirth'].'\',
                    dateofbirth: \''.$fixdata[$k]['dateofbirth'].'\',
                    education: \''.$fixdata[$k]['education'].'\'
                })" class="btn btn-info btn-sm flat" data-toggle="tooltip" data-theme="dark" title="View"><i class="fa fa-eye icon-xs"></i> </button>';
                $btnedit = '<button type="button" id="btn-edit-child-'.$noid.'" onclick="editChild({
                    noid: '.$noid.',
                    no: '.$fixdata[$k]['no'].',
                    name: \''.$fixdata[$k]['name'].'\',
                    gender: \''.$fixdata[$k]['gender'].'\',
                    placeofbirth: \''.$fixdata[$k]['placeofbirth'].'\',
                    dateofbirth: \''.$fixdata[$k]['dateofbirth'].'\',
                    education: \''.$fixdata[$k]['education'].'\'
                })" class="btn btn-warning btn-sm flat" data-toggle="tooltip" data-theme="dark" title="Edit"><i class="fa fa-edit icon-xs" style="text-align:center"></i> </button>';
                $btndelete = '<button type="button" id="btn-delete-child-'.$noid.'" onclick="showModalDeleteChild('.$noid.')" class="btn btn-danger btn-sm flat" data-toggle="tooltip" data-theme="dark" title="Delete"><i class="fa fa-trash icon-xs"></i> </button>';
                $fixdata[$k]['action'] = $btnview.$btnedit.$btndelete;

                $no++;
            }
    
            return response()->json([
                'data' => $fixdata,
                'draw' => intval($request['draw']),
                'recordsFiltered' => count($fixdata),
                'recordsTotal' => count($fixdata),
            ]);
        } else {
            $fixdata = [];
            $no = 1;
            foreach ($data as $k => $v) {
                $noid;
                $fixdata[$k]['no'] = $no;
                foreach ($v as $k2 => $v2) {
                    if (@$v2->name == 'noid') {
                        $noid = @$v2->value;
                    }
                    $fixdata[$k][@$v2->name] = @$v2->value;
                }

                $btnview = '<button type="button" id="btn-view-child-'.$noid.'" onclick="viewChild({
                    noid: '.$noid.',
                    no: '.$fixdata[$k]['no'].',
                    name: \''.$fixdata[$k]['name'].'\',
                    gender: \''.$fixdata[$k]['gender'].'\',
                    placeofbirth: \''.$fixdata[$k]['placeofbirth'].'\',
                    dateofbirth: \''.$fixdata[$k]['dateofbirth'].'\',
                    education: \''.$fixdata[$k]['education'].'\'
                })" class="btn btn-info btn-sm flat" data-toggle="tooltip" data-theme="dark" title="View"><i class="fa fa-eye icon-xs"></i> </button>';
                $btnedit = '<button type="button" id="btn-edit-child-'.$noid.'" onclick="editChild({
                    noid: '.$noid.',
                    no: '.$fixdata[$k]['no'].',
                    name: \''.$fixdata[$k]['name'].'\',
                    gender: \''.$fixdata[$k]['gender'].'\',
                    placeofbirth: \''.$fixdata[$k]['placeofbirth'].'\',
                    dateofbirth: \''.$fixdata[$k]['dateofbirth'].'\',
                    education: \''.$fixdata[$k]['education'].'\'
                })" class="btn btn-warning btn-sm flat" data-toggle="tooltip" data-theme="dark" title="Edit"><i class="fa fa-edit icon-xs" style="text-align:center"></i> </button>';
                $btndelete = '<button type="button" id="btn-delete-child-'.$noid.'" onclick="showModalDeleteChild('.$noid.')" class="btn btn-danger btn-sm flat" data-toggle="tooltip" data-theme="dark" title="Delete"><i class="fa fa-trash icon-xs"></i> </button>';
                $fixdata[$k]['action'] = $btnview.$btnedit.$btndelete;
                
                $no++;
            }
            return response()->json([
                'data' => $fixdata,
                'draw' => intval($request['draw']),
                'recordsFiltered' => count($fixdata),
                'recordsTotal' => count($fixdata),
            ]);
        }
    }
}