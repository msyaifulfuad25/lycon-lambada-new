<?php namespace App\Http\Controllers\transaction\purchase_modules\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;
use App\Menu;
use DB;

class DashboardController extends Controller {

    public function index() {
        $defaults = $this->defaults();
        $pagenoid = $defaults['pagenoid'];
        $idtypepage = $defaults['idtypepage'];
        $tabletitle = $defaults['tabletitle'];
        $sidebar = $defaults['sidebar'];
        $breadcrumb = $defaults['breadcrumb'];
        $cost_controls = $this->getCostControl();

        return view('transaction.purchase_modules.dashboard.dashboard.index', compact(
            'pagenoid',
            'idtypepage',
            'tabletitle',
            'sidebar',
            'breadcrumb',
            'cost_controls',
        ));
    }

    public function getMostItemRequested(Request $request) {
        $start = $request['start'];
        $length = $request['length'];
        $column = $request['columns'][$request['order'][0]['column']]['data'];
        $dir = $request['order'][0]['dir'];
        $month_and_year = $request['month_and_year'];
        $year_only = $request['year_only'];
        $search_value = $request['search']['value'];
        $id_cost_control = $request['id_cost_control'];
        
        // Query
        $is_month_and_year = !is_null($month_and_year) && $month_and_year != '';
        $is_year_only = !is_null($year_only) && $year_only != '';
        $is_search_value = !is_null($search_value) && $search_value != '';
        $is_id_cost_control = !is_null($id_cost_control) && $id_cost_control != '';

        $where_or_and = $is_month_and_year || $is_year_only ? 'AND' : 'WHERE';

        $where_month_and_year = $is_month_and_year ? "WHERE pr.tanggaldue LIKE '%$month_and_year%'" : '';
        $where_year_only = $is_year_only ? "WHERE pr.tanggaldue LIKE '%$year_only%'" : '';
        $where_search = $is_search_value ? "$where_or_and imi.nama LIKE '%$search_value%'" : '';
        $where_id_cost_control = $is_id_cost_control ? "$where_or_and pr.idcostcontrol = $id_cost_control" : '';

        $query = "SELECT
                imi.noid AS noid,
                pr.idcostcontrol AS id_cost_control,
                imi.nama AS name,
                COUNT(imi.nama) as qty
            FROM purchaserequest AS pr
            LEFT JOIN purchaserequestdetail AS prd ON prd.idmaster = pr.noid
            LEFT JOIN invminventory AS imi ON imi.noid = prd.idinventor
            LEFT JOIN salesorder AS so ON so.noid = pr.idcostcontrol
            $where_month_and_year
            $where_year_only
            $where_search
            $where_id_cost_control
            GROUP BY imi.nama
            ORDER BY $column $dir, pr.docreate DESC";
        // print_r($query);die;    

        $result = DB::connection('mysql2')->select($query);
        $length = $length == -1 ? count($result) : $length;

        $data = [];
        foreach ($result as $k => $v) {
            if ($k >= $start && count($data) < $length) {
                $no = $k+1;
                $v->no = $no;
                $v->action = "
                    <button class=\"btn btn-info btn-sm\" onclick=\"showModal({element_id: '#modal_detail', noid: $v->noid, name: '$v->name', id_cost_control: $v->id_cost_control})\"><i class=\"fa fa-eye\"></i> Detail</button
                ";
                $data[] = $v;
            }
        }

        return response()->json([
            'data' => $data,
            'draw' => intval($request['draw']),
            'recordsFiltered' => count($result),
            'recordsTotal' => count($data)
        ]);
    }

    public function getMostItemRequestedDetail(Request $request) {
        $start = $request['start'];
        $length = $request['length'];
        $column = $request['columns'][$request['order'][0]['column']]['data'];
        $dir = $request['order'][0]['dir'];
        $month_and_year = $request['month_and_year'];
        $year_only = $request['year_only'];
        $search_value = $request['search']['value'];
        $id_cost_control = $request['id_cost_control'];
        $search_value = $request['search']['value'];
        $noid = $request['noid'];

        // Query
        $is_id_cost_control = !is_null($id_cost_control) && $id_cost_control != '';
        $is_search_value = !is_null($search_value) && $search_value != '';
        $where_id_cost_control = $is_id_cost_control ? "AND pr.idcostcontrol = $id_cost_control" : '';
        $where_search = $is_search_value 
            ? "AND (imi.nama LIKE '%$search_value%'
                OR prd.keterangan LIKE '%$search_value%'
                OR pr.kode LIKE '%$search_value%')"
            : '';

        $query = "SELECT
                imi.noid,
                pr.kode AS pr_code,
                pof.kode AS pof_code,
                imi.nama AS name,
                prd.keterangan AS description
            FROM purchaserequest AS pr
            LEFT JOIN purchaserequestdetail AS prd ON prd.idmaster = pr.noid
            LEFT JOIN purchaseoffer AS pof ON pof.kodereff = pr.kode
            LEFT JOIN invminventory AS imi ON imi.noid = prd.idinventor
            LEFT JOIN salesorder AS so ON so.noid = pr.idcostcontrol
            WHERE prd.idinventor = $noid
            $where_id_cost_control
            $where_search
            ORDER BY $column $dir";
        // print_r($query);die;

        $result = DB::connection('mysql2')->select($query);
        $length = $length == -1 ? count($result) : $length;

        $data = [];
        foreach ($result as $k => $v) {
            if ($k >= $start && count($data) < $length) {
                $no = $k+1;
                $v->no = $no;
                $v->pr_code = "<a href=\"".URL::to("/40021102/?s=$v->pr_code")."\" target=\"_blank\">$v->pr_code</a>";
                $v->pof_code = "<a href=\"".URL::to("/40021202/?s=$v->pof_code")."\" target=\"_blank\">$v->pof_code</a>";
                $data[] = $v;
            }
        }

        return response()->json([
            'data' => $data,
            'draw' => intval($request['draw']),
            'recordsFiltered' => count($result),
            'recordsTotal' => count($data)
        ]);
    }

    // Private

    private function getCostControl() {
        $query = "SELECT noid, kode, projectname
            FROM salesorder
            ORDER BY kode ASC";
        return DB::connection('mysql2')->select($query);
    }
    
    private function defaults() {
        $noid = 40020101;
        $pagenoid = $noid;
        $idtypepage = 9;
        $tabletitle = 'Dashboard';
        $idtypepage = $noid; 
        $sidebar = Session::get('login')
            ? Menu::getSidebarLogin($idtypepage)
            : Menu::getSidebarLoginBaru($idtypepage);
        $breadcrumb = Menu::getBreadcrumb($noid);
            
        return [
            'noid' => $noid,
            'pagenoid' => $pagenoid,
            'idtypepage' => $idtypepage,
            'tabletitle' => $tabletitle,
            'idtypepage' => $idtypepage,
            'sidebar' => $sidebar,
            'breadcrumb' => $breadcrumb,
        ];
    }
}

?>