<?php

namespace App\Http\Controllers\sales;

use App\Http\Controllers\Controller;
use App\Model\Sales\SalesInquiry as MP;
use App\Model\Sales\SalesOrder;
use App\Model\RA\RAP;
use App\Model\RA\RAB;
use App\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Helpers\MyHelper;
use DB;
use Image;
use Storage;
use App\Model\excel\exports\SalesInquiryRAP AS SalesInquiryRAPExport;
use App\Model\excel\exports\SalesInquiryWS AS SalesInquiryWSExport;
use App\Model\excel\exports\SalesInquiryRecap AS SalesInquiryRecapExport;
use App\Model\excel\exports\SalesInquiryPS AS SalesInquiryPSExport;
use App\Model\excel\imports\SalesInquiryImportRAP;

class SalesInquiryController extends Controller {

    public $color = [
        'white' => '#FFFFFF',
        'default' => '#E1E5EC',
        'dark' => '#2F353B',
        'blue' => '#3598DC',
        'blue-madison' => '#578EBE',
        'blue-chambray' => '#2C3E50',
        'blue-ebonyclay' => '#22313F',
        'blue-hoki' => '#67809F',
        'blue-steel' => '#4B77BE',
        'blue-soft' => '#4C87B9',
        'blue-dark' => '#5E738B',
        'blue-sharp' => '#5C9BD1',
        'blue-oleo' => '#94A0B2',
        'green' => '#32C5D2',
        'green-meadow' => '#1BBC9B',
        'green-seagreen' => '#1BA39C',
        'green-torquoise' => '#36D7B7',
        'green-haze' => '#44B6AE',
        'green-jungle' => '#26C281',
        'green-soft' => '#3FABA4',
        'green-dark' => '#4DB3A2',
        'green-sharp' => '#2AB4C0',
        'green-steel' => '#29B4B6',
        'grey' => '#E5E5E5',
        'grey-steel' => '#E9EDEF',
        'grey-cararra' => '#FAFAFA',
        'grey-gallery' => '#555555',
        'grey-cascade' => '#95A5A6',
        'grey-silver' => '#BFBFBF',
        'grey-salsa' => '#ACB5C3',
        'grey-salt' => '#BFCAD1',
        'grey-mint' => '#525E64',
        'red' => '#E7505A',
        'red-pink' => '#E08283',
        'red-sunglo' => '#E26A6A',
        'red-intense' => '#E35B5A',
        'red-thunderbird' => '#D91E18',
        'red-flamingo' => '#EF4836',
        'red-soft' => '#D05454',
        'red-haze' => '#F36A5A',
        'red-mint' => '#E43A45',
        'yellow' => '#C49F47',
        'yellow-gold' => '#E87E04',
        'yellow-casablanca' => '#F2784B',
        'yellow-crusta' => '#F3C200',
        'yellow-lemon' => '#F7CA18',
        'yellow-saffron' => '#F4D03F',
        'yellow-soft' => '#C8D046',
        'yellow-haze' => '#C5BF66',
        'yellow-mint' => '#C5B96B',
        'purple' => '#8E44AD',
        'purple-plum' => '#8775A7',
        'purple-medium' => '#BF55EC',
        'purple-studio' => '#8E44AD',
        'purple-wisteria' => '#9B59B6',
        'purple-seance' => '#9A12B3',
        'purple-intense' => '#8775A7',
        'purple-sharp' => '#796799',
        'purple-soft' => '#8877A9',
    ];
    public static $main = [
        'table' => 'salesinquiry',
        'tabledetail' => 'salesinquirydetail',
        'tablecdf' => 'dmsmdocument',
        'table-title' => 'Sales Inquiry',
        'table2' => 'prjmrabp',
        'tabledetail2' => 'prjmrabpdetail',
        'table2-title' => 'RAP',
        'generatecode' => 'generatecode/401/1',
        'generatecode2' => 'generatecode/402/1',
        'idtypetranc' => 401,
        'idtypetranc2' => 402,
        'noid' => 40011102,
        'noidnew' => 40011101,
        'printcode' => 'print_purchase_request',
        'urlcode' => '_si_ctmd',
        'condition' => [
            'mttts' => [
                [],
                [
                    'mttts' => 'draft',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => true,
                        'd' => true,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => true,
                        'd' => true,
                    ],
                    'draft' => false,
                    'issued' => true,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => false,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'issued',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => true,
                    'issued' => false,
                    'verified' => true,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => false,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'verified',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => true,
                    'verified' => false,
                    'approved' => true,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => false,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'approved',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'pending',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'canceled',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'finished',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
            ],
        ]
    ];

    public function index() {
        $isdirector = MP::isRule(['mcardkode'=>Session::get('myusername'),'mpositionkode'=>'DIR']);
        $idtypepage = static::$main['noid'];
        $sessionnoid = Session::get('noid');
        $sessionmyusername = Session::get('myusername');
        $sessionname = Session::get('nama');
        $sessionidcompany = Session::get('idcompany');
        $sessioniddepartment = Session::get('iddepartment');
        $sessionidgudang = Session::get('idgudang');

        $maintable = static::$main['table'];
        $breadcrumb = Menu::getBreadcrumb(static::$main['noid']);
        $pagenoid = static::$main['noid'];

        $defaulttanggal = date('d-M-Y');
        $defaulttanggaldue = date('d-M-Y', strtotime('+7 day'));
        $data = MP::getData(['table'=>static::$main['table'],'select'=>['noid','idstatustranc','idstatuscard','dostatus','idtypetranc','kode','kodereff','tanggal','tanggaltax','tanggaldue']]);

        $columns = [
            ['data'=>'checkbox','orderable'=>false,'class'=>'text-center'],
            ['data'=>'no','orderable'=>false,'class'=>'text-right'],
            // ['data'=>'noid'],
            ['data'=>'idtypetranc'],
            ['data'=>'idstatustranc'],
            // ['data'=>'idstatuscard'],
            // ['data'=>'dostatus'],
            ['data'=>'kode'],
            ['data'=>'kodereff'],
            ['data'=>'tanggal'],
            ['data'=>'generaltotal','class'=>'text-right'],
            ['data'=>'keterangan'],
            ['data'=>'idcompany'],
            ['data'=>'iddepartment'],
            ['data'=>'idgudang'],
            ['data'=>'action','orderable'=>false],
        ];
        $columnslog = [
            ['data'=>'checkbox','orderable'=>false,'class'=>'text-center'],
            ['data'=>'no','orderable'=>false,'class'=>'text-right'],
            ['data'=>'idtypetranc'],
            ['data'=>'idtypeaction'],
            ['data'=>'logsubject'],
            ['data'=>'keterangan'],
            ['data'=>'idreff'],
            ['data'=>'idcreate'],
            ['data'=>'docreate'],
            ['data'=>'action','orderable'=>false],
        ];
        $columnscostcontrol = [
            ['data'=>'checkbox','orderable'=>false,'class'=>'text-center'],
            ['data'=>'no','orderable'=>false,'class'=>'text-right'],
            ['data'=>'idstatustranc'],
            ['data'=>'idtypetranc'],
            ['data'=>'kode'],
            ['data'=>'kodereff'],
            ['data'=>'idcompany'],
            ['data'=>'iddepartment'],
            ['data'=>'generaltotal','class'=>'text-right'],
            ['data'=>'keterangan'],
            ['data'=>'tanggal'],
            ['data'=>'action','orderable'=>false],
        ];
        $columnsdetail = [
            ['data'=>'checkbox','orderable'=>false,'class'=>'text-center'],
            ['data'=>'no','orderable'=>false,'class'=>'text-right'],
            ['data'=>'kodeinventor'],
            ['data'=>'namainventor'],
            ['data'=>'namainventor2'],
            ['data'=>'keterangan'],
            // ['data'=>'idgudang'],
            // ['data'=>'idcompany'],
            // ['data'=>'iddepartment'],
            // ['data'=>'idsatuan'],
            // ['data'=>'konvsatuan'],
            ['data'=>'unitqty','class'=>'text-right'],
            ['data'=>'namasatuan'],
            ['data'=>'unitprice','class'=>'text-right','render'=>"$.fn.dataTable.render.number( ',', '.', 3, 'Rp' )"],
            // ['data'=>'subtotal'],
            // ['data'=>'discountvar'],
            // ['data'=>'discount'],
            // ['data'=>'nilaidisc'],
            // ['data'=>'idtypetax'],
            // ['data'=>'idtax'],
            ['data'=>'hargatotal','class'=>'text-right'],
            ['data'=>'namacurrency'],
            // ['data'=>'idakunhpp'],
            // ['data'=>'idakunsales'],
            // ['data'=>'idakunpersediaan'],
            // ['data'=>'idakunsalesreturn'],
            // ['data'=>'idakunpurchasereturn'],
            // ['data'=>'serialnumber'],
            // ['data'=>'garansitanggal'],
            // ['data'=>'garansiexpired'],
            // ['data'=>'typetrancprev'],
            // ['data'=>'trancprev'],
            // ['data'=>'trancprevd'],
            // ['data'=>'typetrancorder'],
            // ['data'=>'trancorder'],
            // ['data'=>'trancorderd'],
            ['data'=>'action','orderable'=>false],
        ];

        $columndefs = [
            ['targets'=>0,'width'=>'5%'],
            ['targets'=>1,'width'=>'5%'],
            ['targets'=>2,'width'=>'5%'],
            ['targets'=>3,'width'=>'15%'],
            ['targets'=>4,'width'=>'15%'],
            ['targets'=>5,'width'=>'15%'],
            ['targets'=>6,'width'=>'15%'],
            ['targets'=>7,'width'=>'15%'],
            ['targets'=>8,'width'=>'5%'],
            ['targets'=>9,'width'=>'5%'],
            ['targets'=>10,'width'=>'5%'],
            ['targets'=>10,'width'=>'5%']
        ];

        $columndefslog = [
            ['targets'=>0,'width'=>'5%'],
            ['targets'=>1,'width'=>'5%'],
            ['targets'=>2,'width'=>'5%'],
            ['targets'=>3,'width'=>'15%'],
            ['targets'=>4,'width'=>'15%'],
            ['targets'=>5,'width'=>'15%'],
            ['targets'=>6,'width'=>'15%'],
            ['targets'=>7,'width'=>'15%'],
            ['targets'=>8,'width'=>'5%'],
            ['targets'=>9,'width'=>'5%'],
            ['targets'=>10,'width'=>'5%']
        ];

        $columndefscostcontrol = [
            ['targets'=>0,'width'=>'5%'],
            ['targets'=>1,'width'=>'5%'],
            ['targets'=>2,'width'=>'5%'],
            ['targets'=>3,'width'=>'15%'],
            ['targets'=>4,'width'=>'15%'],
            ['targets'=>5,'width'=>'15%'],
            ['targets'=>6,'width'=>'15%'],
            ['targets'=>7,'width'=>'15%'],
            ['targets'=>8,'width'=>'5%'],
            ['targets'=>9,'width'=>'5%'],
            ['targets'=>10,'width'=>'5%'],
            ['targets'=>10,'width'=>'5%']
        ];

        for ($i = 0; $i <= 12; $i++) {
            if ($i <= 8) {
                $columndefsdetail[] = ['target'=>$i];
            }
        }
        
        $columns = json_encode($columns);
        $columndefs = json_encode($columndefs);
        $columnsdetail = json_encode($columnsdetail);
        $columndefsdetail = json_encode($columndefsdetail);
        $columnslog = json_encode($columnslog);
        $columndefslog = json_encode($columndefslog);
        $columnscostcontrol = json_encode($columnscostcontrol);
        $columndefscostcontrol = json_encode($columndefscostcontrol);

        $mtypecostcontrol = MP::getData(['table'=>'mtypecostcontrol','select'=>['noid','kode','nama'],'isactive'=>true,'orderby'=>[['nama','asc']]]);
        $mcardcustomer = MP::getMCardCustomer();
        $mcardmarketing = MP::getMCardByKodePosition(['kode'=>'MAR']);
        $mcarddrafter = MP::getMCardByKodePosition(['kode'=>'DRA']);
        $mcardsupplier = MP::getMCardSupplier();
        $mtt = MP::findMTypeTranc();
        $mtypetranc_noid = $mtt->noid;
        $mtypetranc_nama = $mtt->nama;
        $mtypetranc_kode = $mtt->kode;
        $mtypetranctypestatus = MP::getMTypeTrancTypeStatus(static::$main['idtypetranc']);
        // dd($mtypetranctypestatus);

        $btnexternal = [];
        foreach ($mtypetranctypestatus as $k => $v) {
            if ($v->isinternal == 0 && $v-> isexternal == 1) {
                $btnexternal[] = [
                    'noid' => $v->noid,
                    'idstatusbase' => $v->idstatusbase,
                    'nama' => $v->nama,
                    'classicon' => $v->classicon,
                    'classcolor' => @(new static)->color[$v->classcolor],
                ];
            }
        }

        $conditions = [];
        $ps_conditions = [];
        foreach ($mtypetranctypestatus as $k => $v) {
            if ($k != 0) {
                $updatedropdown[strtolower($v->idstatusbase)] = [
                    'noid' => $v->noid,
                    'kode' => $v->kode,
                    'nama' => $v->nama,
                    'classicon' => $v->classicon,
                    'classcolor' => @(new static)->color[$v->classcolor],
                ];
            }

            // SET PREV & NEXT
            $prev_noid = @$mtypetranctypestatus[$k-1]->noid ? $mtypetranctypestatus[$k-1]->noid : null;
            $prev_idstatusbase = @$mtypetranctypestatus[$k-1]->idstatusbase ? $mtypetranctypestatus[$k-1]->idstatusbase : null;
            $prev_nama = @$mtypetranctypestatus[$k-1]->nama ? $mtypetranctypestatus[$k-1]->nama : null;
            $prev_classicon = @$mtypetranctypestatus[$k-1]->classicon ? $mtypetranctypestatus[$k-1]->classicon : null;
            $prev_classcolor = @$mtypetranctypestatus[$k-1]->classcolor ? @(new static)->color[$mtypetranctypestatus[$k-1]->classcolor] : null;
            $next_noid = @$mtypetranctypestatus[$k+1]->noid ? $mtypetranctypestatus[$k+1]->noid : null;
            $next_idstatusbase = @$mtypetranctypestatus[$k+1]->idstatusbase ? $mtypetranctypestatus[$k+1]->idstatusbase : null;
            $next_nama = @$mtypetranctypestatus[$k+1]->nama ? $mtypetranctypestatus[$k+1]->nama : null;
            $next_classicon = @$mtypetranctypestatus[$k+1]->classicon ? $mtypetranctypestatus[$k+1]->classicon : null;
            $next_classcolor = @$mtypetranctypestatus[$k+1]->classcolor ? @(new static)->color[$mtypetranctypestatus[$k+1]->classcolor] : null;
            
            // if ($v->noid == 4012) {
            //     if ($isdirector) {
                    // $prev_noid = @$mtypetranctypestatus[$k+1]->noid ? $mtypetranctypestatus[$k+1]->noid : null;
                    // $prev_idstatusbase = @$mtypetranctypestatus[$k+1]->idstatusbase ? $mtypetranctypestatus[$k+1]->idstatusbase : null;
                    // $prev_nama = @$mtypetranctypestatus[$k+1]->nama ? $mtypetranctypestatus[$k+1]->nama : null;
                    // $prev_classicon = @$mtypetranctypestatus[$k+1]->classicon ? $mtypetranctypestatus[$k+1]->classicon : null;
                    // $prev_classcolor = @$mtypetranctypestatus[$k+1]->classcolor ? @(new static)->color[$mtypetranctypestatus[$k+1]->classcolor] : null;
            //     }

            //     $next_noid = @$mtypetranctypestatus[$k+2]->noid ? $mtypetranctypestatus[$k+2]->noid : null;
            //     $next_idstatusbase = @$mtypetranctypestatus[$k+2]->idstatusbase ? $mtypetranctypestatus[$k+2]->idstatusbase : null;
            //     $next_nama = @$mtypetranctypestatus[$k+2]->nama ? $mtypetranctypestatus[$k+2]->nama : null;
            //     $next_classicon = @$mtypetranctypestatus[$k+2]->classicon ? $mtypetranctypestatus[$k+2]->classicon : null;
            //     $next_classcolor = @$mtypetranctypestatus[$k+2]->classcolor ? @(new static)->color[$mtypetranctypestatus[$k+2]->classcolor] : null;
            // } else if ($v->noid == 4013) {
            //     if ($isdirector) {
            //     } else {
            //         $prev_noid = @$mtypetranctypestatus[$k-2]->noid ? $mtypetranctypestatus[$k-2]->noid : null;
            //         $prev_idstatusbase = @$mtypetranctypestatus[$k-2]->idstatusbase ? $mtypetranctypestatus[$k-2]->idstatusbase : null;
            //         $prev_nama = @$mtypetranctypestatus[$k-2]->nama ? $mtypetranctypestatus[$k-2]->nama : null;
            //         $prev_classicon = @$mtypetranctypestatus[$k-2]->classicon ? $mtypetranctypestatus[$k-2]->classicon : null;
            //         $prev_classcolor = @$mtypetranctypestatus[$k-2]->classcolor ? @(new static)->color[$mtypetranctypestatus[$k-2]->classcolor] : null;
            //         $next_noid = @$mtypetranctypestatus[$k-1]->noid ? $mtypetranctypestatus[$k-1]->noid : null;
            //         $next_idstatusbase = @$mtypetranctypestatus[$k-1]->idstatusbase ? $mtypetranctypestatus[$k-1]->idstatusbase : null;
            //         $next_nama = @$mtypetranctypestatus[$k-1]->nama ? $mtypetranctypestatus[$k-1]->nama : null;
            //         $next_classicon = @$mtypetranctypestatus[$k-1]->classicon ? $mtypetranctypestatus[$k-1]->classicon : null;
            //         $next_classcolor = @$mtypetranctypestatus[$k-1]->classcolor ? @(new static)->color[$mtypetranctypestatus[$k-1]->classcolor] : null;
            //     }
            // }

            $conditions[$v->noid] = [
                'noid' => $v->noid,
                'idstatusbase' => $v->idstatusbase,
                'nama' => $v->nama,
                'classicon' => $v->classicon,
                'classcolor' => $v->classcolor,
                'table' => static::$main['table2-title'],
                'prev_noid' => $prev_noid,
                'prev_idstatusbase' => $prev_idstatusbase,
                'prev_nama' => $prev_nama,
                'prev_classicon' => $prev_classicon,
                'prev_classcolor' => $prev_classcolor,
                'next_noid' => $next_noid,
                'next_idstatusbase' => $next_idstatusbase,
                'next_nama' => $next_nama,
                'next_classicon' => $next_classicon,
                'next_classcolor' => $next_classcolor,
                'next_table' => static::$main['table2-title'],
                'isinternal' => $v->isinternal == 1 ? 1 : 0,
                'isexternal' => $v->isexternal == 1 ? 1 : 0,
                'btnexternal' => $btnexternal,
                'actview' => $v->actview,
                'actedit' => $v->actedit,
                'actdelete' => $v->actdelete,
                'actreportdetail' => $v->actreportdetail,
                'actreportmaster' => $v->actreportmaster,
                'actsetprevstatus' => $v->actsetprevstatus,
                'actsetnextstatus' => $v->actsetnextstatus,
                'actsetstatus' => $v->actsetstatus,
                'appmtypeaction_noid' => $v->appmtypeaction_noid,
                'appmtypeaction_nama' => $v->appmtypeaction_nama
            ];


            // SET PREV & NEXT
            if ($v->noid == 4016) {
                $ps_prev_noid = @$mtypetranctypestatus[$k-4]->noid ? $mtypetranctypestatus[$k-4]->noid : null;
                $ps_prev_idstatusbase = @$mtypetranctypestatus[$k-4]->idstatusbase ? $mtypetranctypestatus[$k-4]->idstatusbase : null;
                $ps_prev_nama = @$mtypetranctypestatus[$k-4]->nama ? $mtypetranctypestatus[$k-4]->nama : null;
                $ps_prev_classicon = @$mtypetranctypestatus[$k-4]->classicon ? $mtypetranctypestatus[$k-4]->classicon : null;
                $ps_prev_classcolor = @$mtypetranctypestatus[$k-4]->classcolor ? @(new static)->color[$mtypetranctypestatus[$k-4]->classcolor] : null;
            } else {
                $ps_prev_noid = @$mtypetranctypestatus[$k-1]->noid ? $mtypetranctypestatus[$k-1]->noid : null;
                $ps_prev_idstatusbase = @$mtypetranctypestatus[$k-1]->idstatusbase ? $mtypetranctypestatus[$k-1]->idstatusbase : null;
                $ps_prev_nama = @$mtypetranctypestatus[$k-1]->nama ? $mtypetranctypestatus[$k-1]->nama : null;
                $ps_prev_classicon = @$mtypetranctypestatus[$k-1]->classicon ? $mtypetranctypestatus[$k-1]->classicon : null;
                $ps_prev_classcolor = @$mtypetranctypestatus[$k-1]->classcolor ? @(new static)->color[$mtypetranctypestatus[$k-1]->classcolor] : null;
            }

            if ($v->noid == 4012) {
                $ps_next_noid = @$mtypetranctypestatus[$k+4]->noid ? $mtypetranctypestatus[$k+4]->noid : null;
                $ps_next_idstatusbase = @$mtypetranctypestatus[$k+4]->idstatusbase ? $mtypetranctypestatus[$k+4]->idstatusbase : null;
                $ps_next_nama = @$mtypetranctypestatus[$k+4]->nama ? $mtypetranctypestatus[$k+4]->nama : null;
                $ps_next_classicon = @$mtypetranctypestatus[$k+4]->classicon ? $mtypetranctypestatus[$k+4]->classicon : null;
                $ps_next_classcolor = @$mtypetranctypestatus[$k+4]->classcolor ? @(new static)->color[$mtypetranctypestatus[$k+4]->classcolor] : null;
            } else {
                $ps_next_noid = @$mtypetranctypestatus[$k+1]->noid ? $mtypetranctypestatus[$k+1]->noid : null;
                $ps_next_idstatusbase = @$mtypetranctypestatus[$k+1]->idstatusbase ? $mtypetranctypestatus[$k+1]->idstatusbase : null;
                $ps_next_nama = @$mtypetranctypestatus[$k+1]->nama ? $mtypetranctypestatus[$k+1]->nama : null;
                $ps_next_classicon = @$mtypetranctypestatus[$k+1]->classicon ? $mtypetranctypestatus[$k+1]->classicon : null;
                $ps_next_classcolor = @$mtypetranctypestatus[$k+1]->classcolor ? @(new static)->color[$mtypetranctypestatus[$k+1]->classcolor] : null;
            }


            if ($v->noid == 4010 || $v->noid == 4011 || $v->noid == 4012 || $v->noid == 4016 || $v->noid == 4017 || $v->noid == 4018 || $v->noid == 4019) {
                $ps_conditions[$v->noid] = [
                    'noid' => $v->noid,
                    'idstatusbase' => $v->idstatusbase,
                    'nama' => $v->nama,
                    'prev_noid' => $ps_prev_noid,
                    'prev_idstatusbase' => $ps_prev_idstatusbase,
                    'prev_nama' => $ps_prev_nama,
                    'prev_classicon' => $ps_prev_classicon,
                    'prev_classcolor' => $ps_prev_classcolor,
                    'next_noid' => $ps_next_noid,
                    'next_idstatusbase' => $ps_next_idstatusbase,
                    'next_nama' => $ps_next_nama,
                    'next_classicon' => $ps_next_classicon,
                    'next_classcolor' => $ps_next_classcolor,
                    'next_table' => static::$main['table2-title'],
                    'isinternal' => $v->isinternal == 1 ? 1 : 0,
                    'isexternal' => $v->isexternal == 1 ? 1 : 0,
                    'btnexternal' => $btnexternal,
                    'actview' => $v->actview,
                    'actedit' => $v->actedit,
                    'actdelete' => $v->actdelete,
                    'actreportdetail' => $v->actreportdetail,
                    'actreportmaster' => $v->actreportmaster,
                    'actsetprevstatus' => $v->actsetprevstatus,
                    'actsetnextstatus' => $v->actsetnextstatus,
                    'actsetstatus' => $v->actsetstatus,
                    'appmtypeaction_noid' => $v->appmtypeaction_noid,
                    'appmtypeaction_nama' => $v->appmtypeaction_nama
                ];
            }
        }
        // dd($conditions);
        Session::put(static::$main['noid'].'-conditions', $conditions);
        Session::put(static::$main['noid'].'-ps_conditions', $ps_conditions);
        $mttts = MP::findMTypeTrancTypeStatus(static::$main['idtypetranc']);
        $mtypetranctypestatus_noid = $mttts->noid;
        $mtypetranctypestatus_nama = $mttts->nama;
        $mtypetranctypestatus_classcolor = $mttts->classcolor;
        $mcard = MP::getData(['table'=>'mcard','select'=>['noid','kode','nama','isemployee','iscustomer','issupplier'],'isactive'=>true,'orderby'=>[['nama','asc']]]);
        $accmkodeakun = MP::getData(['table'=>'accmkodeakun']);
        $accmcashbank = MP::getData(['table'=>'accmcashbank','select'=>['noid','kode','nama']]);
        $accmcurrency = MP::getData(['table'=>'accmcurrency','select'=>['noid','kode','nama'],'isactive'=>true]);
        $genmgudang = MP::getData(['table'=>'genmgudang','select'=>['noid','kode','namalias'],'isactive'=>true,'orderby'=>[['nama','asc']]]);
        $genmcompany = MP::getData(['table'=>'genmcompany','select'=>['noid','kode','nama'],'isactive'=>true]);
        // $genmcompany = MP::getGenMCompany();
        $genmdepartment = MP::getData(['table'=>'genmdepartment','select'=>['noid','kode','nama']]);
        $mtypetranc = MP::getData(['table'=>'mtypetranc','select'=>['noid','kode','nama']]);
        $imi = MP::getInvMInventory();
        $invmgroup = MP::getData(['table'=>'invmgroup','select'=>['noid','kode','nama']]);
        $invmkategori = MP::getData(['table'=>'invmkategori','select'=>['noid','kode','nama']]);
        $invminventory = [];
        foreach ($imi as $k => $v) {
            $invminventory[$k] = $v;
            // $invminventory[$k]->purchaseprice2 = 'RP.'.number_format($v->purchaseprice2);
        }
        $invmsatuan = MP::getData(['table'=>'invmsatuan','select'=>['noid','kode','nama','konversi'],'isactive'=>true,'orderby'=>[['nama','asc']]]);
        $accmpajak = MP::getData(['table'=>'accmpajak','select'=>['noid','kode','nama','prosentax','operation'],'orderby'=>[['nourut','asc']],'isactive'=>true]);
        $mtypeproject = MP::getData(['table'=>'mtypeproject','select'=>['noid','kode','nama'],'isactive'=>true]);
        $color = @(new static)->color;
            $lastdata = MP::getLastMonth();
            $lastdata = $lastdata->_year == null && $lastdata->_month == null ? (object)['_year'=>date('y'),'_month'=>date('m')] : $lastdata;
        $datefilter = [
            'a' => 'All',
            't' => date('M d, Y').' - '.date('M d, Y'),
            'y' => date('M d, Y', strtotime('-1 day')).' - '.date('M d, Y', strtotime('-1 day')),
            'l7d' => date('M d, Y').' - '.date('M d, Y', strtotime('-7 day')),
            'l30d' => date('M d, Y').' - '.date('M d, Y', strtotime('1 month')),
            'tm' => date('M d, Y', strtotime('first day of this month')).' - '.date('M d, Y', strtotime('last day of this month')),
            'lm' => date('M d, Y', strtotime('first day of previous month')).' - '.date('M d, Y', strtotime('last day of previous month')),
        ];
        $datefilterdefault = 'a';
        $tabletitle = static::$main['table-title'];
        $sendto = static::$main['table2-title'];

        $noidnew = explode('/', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH))[1];
        $triggercreate = $noidnew == static::$main['noidnew'] ? true : false;
        $privilege = $this->privilege();
        $isadd = $privilege['isadd'] ? 1 : 0;
        $urlcode = static::$main['urlcode'];
        $mmilestone = MP::getData(['table'=>'mmilestone','select'=>['noid','name'],'isactive'=>false]);

        return view('lam_custom_transaksi_master_detail.'.static::$main['table'].'.index', compact(
            'idtypepage',
            'sessionnoid',
            'sessionmyusername',
            'sessionname',
            'sessionidcompany',
            'sessioniddepartment',
            'sessionidgudang',
            'maintable',
            'breadcrumb',
            'pagenoid',
            'defaulttanggal',
            'defaulttanggaldue',
            'data',
            'columns',
            'columndefs',
            'columnsdetail',
            'columndefsdetail',
            'columnslog',
            'columndefslog',
            'columnscostcontrol',
            'columndefscostcontrol',
            'mtypecostcontrol',
            'mcardcustomer',
            'mcardmarketing',
            'mcarddrafter',
            'mcardsupplier',
            'mtypetranc_noid',
            'mtypetranc_nama',
            'mtypetranc_kode',
            'mtypetranctypestatus',
            'updatedropdown',
            'mtypetranctypestatus_noid',
            'mtypetranctypestatus_nama',
            'mtypetranctypestatus_classcolor',
            'mcard',
            'accmkodeakun',
            'accmcashbank',
            'accmcurrency',
            'genmgudang',
            'genmcompany',
            'genmdepartment',
            'mtypetranc',
            'invmgroup',
            'invmkategori',
            'invminventory',
            'invmsatuan',
            'accmpajak',
            'mtypeproject',
            'color',
            'datefilter',
            'datefilterdefault',
            'tabletitle',
            'sendto',
            'triggercreate',
            'privilege',
            'isadd',
            'urlcode',
            'mmilestone'
        ));
    }

    public function validationConditions($params) {
        $_fixconditions = [];
        foreach ($params['conditions'] as $k => $v) {
            if ($params['ruleconditions'][$k]) {
                $v['prev_noid'] = @$_prev_noid;
                $v['prev_idstatusbase'] = @$_prev_idstatusbase;
                $v['prev_nama'] = @$_prev_nama;
                $v['prev_classicon'] = @$_prev_classicon;
                $v['prev_classcolor'] = @$_prev_classcolor;

                $_fixconditions[$k] = $v;

                $_prev_noid = $v['noid'];
                $_prev_idstatusbase = $v['idstatusbase'];
                $_prev_nama = $v['nama'];
                $_prev_classicon = $v['classicon'];
                $_prev_classcolor = $v['classcolor'];
            }
        }
        foreach ($_fixconditions as $k => $v) {
            if (@$currentnextnoid != $k && @$currentnoid) {
                $_fixconditions[$currentnoid]['next_noid'] = $v['noid'];
                $_fixconditions[$currentnoid]['next_idstatusbase'] = $v['idstatusbase'];
                $_fixconditions[$currentnoid]['next_nama'] = $v['nama'];
                $_fixconditions[$currentnoid]['next_classicon'] = $v['classicon'];
                $_fixconditions[$currentnoid]['next_classcolor'] = $v['classcolor'];
                $_fixconditions[$currentnoid]['next_table'] = $v['table'];
            }
            $currentnoid = $v['noid'];
            $currentnextnoid = $v['next_noid'];
        }

        return $_fixconditions;
    }

    public function privilege() {
        return MyHelper::getPrivilege([
            'idmenu' => static::$main['noid'],
            'idcard' => Session::get('noid'),
            'isroot' => Session::get('groupuser') == 0 ? true : false
        ]);
    }
    
    public function get(Request $request) {
        $isroot = Session::get('groupuser') == 0 ? true : false;
        $isdirector = MP::isRule(['mcardkode'=>Session::get('myusername'),'mpositionkode'=>'DIR']);
        $ispurchasing = MP::isRule(['mcardkode'=>Session::get('myusername'),'mpositionkode'=>'PUR']);
        $isprojecttender = MP::isRule(['mcardkode'=>Session::get('myusername'),'mpositionkode'=>'PT']);
        $ismarketing = MP::isRule(['mcardkode'=>Session::get('myusername'),'mpositionkode'=>'MAR']);
        $privilege = $this->privilege();

        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->get('search')['value'];
        $orderbycolumn = $request->get('columns')[$request->get('order')[0]['column']]['data'];
        if ($orderbycolumn == 'checkbox' || $orderbycolumn == 'no') {
            $orderbycolumn = 'noid';
        }
        $orderby = [
            'column' => $orderbycolumn,
            'sort' => $request->get('order')[0]['dir']
        ];
        $filter = $request->get('filter');
        $datefilter = $request->get('datefilter');
        $datefilter2 = $request->get('datefilter2');

        $result = MP::getSales($start, $length, $search, $orderby, $filter, $datefilter, $datefilter2);
        $data = $result['data'];
        $allnoid = MP::getAllNoid(static::$main['table']);

        $resulttotal = count(MP::getSales(null, null, $search, $orderby, $filter, $datefilter, $datefilter2)['data']);

        $resultsnst = MP::getMTypeTrancTypeStatus(static::$main['idtypetranc']);
        foreach ($resultsnst as $k => $v) {
            $snst[$v->nourut] = $v;
        }

        $conditions = Session::get(static::$main['noid'].'-conditions');
        $ps_conditions = Session::get(static::$main['noid'].'-ps_conditions');
        $no = $start+1;
        foreach ($data as $k => $v) {
            $isunspecified = $v->mtypetranctypestatus_noid == 4010 ? true : false;
            $isdraft = $v->mtypetranctypestatus_noid == 4011 ? true : false;
            $isreadytoprocess = @$v->mtypetranctypestatus_noid == 4012 ? true : false;
            $isaccbyestimator = @$v->mtypetranctypestatus_noid == 4013 ? true : false;
            $isaccbymarketing = @$v->mtypetranctypestatus_noid == 4014 ? true : false;
            $isaccbyprojectmanager = @$v->mtypetranctypestatus_noid == 4015 ? true : false;
            $isaccbydirector = @$v->mtypetranctypestatus_noid == 4016 ? true : false;
            $isok = $v->mtypetranctypestatus_noid == 4017 ? true : false;
            $isfinished = $v->mtypetranctypestatus_noid == 4018 ? true : false;
            $iscanceled = $v->mtypetranctypestatus_noid == 4019 ? true : false;

            $ruleconditions = [
                '4010' => true,
                '4011' => true,
                '4012' => true,
                '4013' => !is_null($v->idcardestimator),
                '4014' => true,
                '4015' => !is_null($v->idcardpj),
                '4016' => $v->projectvalue >= 100000000 ? true : false,
                '4017' => true,
                '4018' => true,
                '4019' => true
            ];
            $fixconditions = $this->validationConditions([
                'conditions' => $conditions,
                'ruleconditions' => $ruleconditions,
            ]);

            if ($v->idtypecostcontrol == 1 || $v->idtypecostcontrol == 2) {
                $isinternal = $fixconditions[$v->mtypetranctypestatus_noid]['isinternal'] ? true : false;
                $isprev = !is_null(@$fixconditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']);
                $isnext = !is_null(@$fixconditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']);
                $isactsetstatus = $fixconditions[$v->mtypetranctypestatus_noid]['actsetstatus'] ? true : false;
            } else if ($v->idtypecostcontrol == 3) {                
                $isinternal = $ps_conditions[$v->mtypetranctypestatus_noid]['isinternal'] ? true : false;
                $isprev = !is_null(@$ps_conditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']);
                $isnext = !is_null(@$ps_conditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']);
                $isactsetstatus = $ps_conditions[$v->mtypetranctypestatus_noid]['actsetstatus'] ? true : false;
            }

            // $isrequester = Session::get('noid') == $v->idcardrequest;
            $isprojectkoor = Session::get('noid') == @$v->idcardkoor;
            $isprojectmanager = Session::get('noid') == @$v->idcardpj;
            // $isadminsite = Session::get('noid') == $v->so_idcardsitekoor; 
            $isidcardsales = Session::get('noid') == @$v->idcardsales; 
            $isidcarddrafter = Session::get('noid') == @$v->idcarddrafter; 
            $isidcardestimator = Session::get('noid') == @$v->idcardestimator; 
            $iscreater = Session::get('noid') == @$v->idcreate;
            $isprint = $privilege['isreportdetail'];
            $isview = $privilege['isview'];
            $islog = $privilege['isshowlog'];
            $isedit = $privilege['isedit'];
            $isdelete = $privilege['isdelete'];
            
            $checkbox['checkbox'] = '<input type="checkbox" value="FALSE" class="checkbox-m" id="checkbox-m-'.$v->noid.'" onchange="selectCheckboxM('.$v->noid.')">';
            $_no['no'] = $no;
            $data[$k]->mtypetranctypestatus_nama = '<span style="padding: 2px; color: white; background: '.@(new static)->color[$v->mtypetranctypestatus_classcolor].'">'.$v->mtypetranctypestatus_nama.'</span>';
            $data[$k]->tanggal = $this->dbdateTohumandate($data[$k]->tanggal);
            $data[$k]->tanggaldue = $this->dbdateTohumandate($data[$k]->tanggaldue);
            $data[$k]->totalsubtotal = $v->totalsubtotal;
            $data[$k]->generaltotal = $v->generaltotal;
            $array = (array)$data[$k];



            // BTN PREV
            $isbtnprev = $isroot;
            if ($isprev && $isinternal) {
                if ($isunspecified) {
                    if ($isprojecttender) {
                        $isbtnprev = true;
                    }
                } else if ($isdraft) {
                    if (!$isbtnprev) {
                        if ($iscreater) {
                            $isbtnprev = true;
                        }
                    }
                } else if ($isreadytoprocess) {
                    if (!$isbtnprev) {
                        if ($iscreater || $isidcardsales) {
                            $isbtnprev = true;
                        }
                    }
                } else if ($isaccbyestimator) {
                    if (!$isbtnprev) {
                        if ($isidcardestimator) {
                            $isbtnprev = true;
                        }
                    }
                } else if ($isaccbymarketing) {
                    if (!$isbtnprev) {
                        if ($isidcardsales) {
                            $isbtnprev = true;
                        }
                    }
                } else if ($isaccbyprojectmanager) {
                    if (!$isbtnprev) {
                        if ($isprojectmanager) {
                            $isbtnprev = true;
                        }
                    }
                } else if ($isaccbydirector) {
                    if (!$isbtnprev) {
                        if ($isdirector) {
                            $isbtnprev = true;
                        }
                    }
                }
            } else {
                if ($isfinished || $iscanceled) {
                    $isbtnprev = false;
                }
            }
            
            if ($v->idtypecostcontrol == 1 || $v->idtypecostcontrol == 2) {
                $btnprev = $isbtnprev && !is_null($fixconditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']) ? '<a onclick="showModalSNST('.$v->noid.','.$v->mtypetranc_noid.', '.$snst[$fixconditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']]->noid.', \''.$snst[$fixconditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']]->nama.'\')" title="Set to '.$snst[$fixconditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']]->nama.'" class="btn btn-sm flat" style="background-color: '.@(new static)->color[$snst[$fixconditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']]->classcolor].'"><i class="fa fa-arrow-left icon-xs text-light" style="text-align:center"></i> </a>' : '';
            } else if ($v->idtypecostcontrol == 3) {                
                $btnprev = $isbtnprev && !is_null($ps_conditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']) ? '<a onclick="showModalSNST('.$v->noid.','.$v->mtypetranc_noid.', '.$snst[$ps_conditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']]->noid.', \''.$snst[$ps_conditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']]->nama.'\')" title="Set to '.$snst[$ps_conditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']]->nama.'" class="btn btn-sm flat" style="background-color: '.@(new static)->color[$snst[$ps_conditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']]->classcolor].'"><i class="fa fa-arrow-left icon-xs text-light" style="text-align:center"></i> </a>' : '';
            }


            // $btnprev = $fixconditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase'] && $fixconditions[$v->mtypetranctypestatus_noid]['isinternal'] 
            //     ? '<a onclick="showModalSNST('.$v->noid.','.$v->mtypetranc_noid.', '.$snst[$fixconditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']]->noid.', \''.$snst[$fixconditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']]->nama.'\')" title="Set to '.$snst[$fixconditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']]->nama.'" class="btn btn-sm flat" style="background-color: '.@(new static)->color[$snst[$fixconditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']]->classcolor].'"><i class="'.$snst[$fixconditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']]->classicon.' icon-xs text-light" style="text-align:center"></i> </a>' 
            //     : '';


            // BTN NEXT
            $isbtnnext = $isroot;
            if ($isnext && $isinternal) {
                if ($isunspecified) {                       // IF UNSPECIFIED
                    if ($isprojecttender) {
                        $isbtnnext = true;
                    }
                } else if ($isdraft) {                      // IF DRAFT
                    if (!$isbtnnext) {
                        if ($iscreater || $isidcardsales) {
                            $isbtnnext = true;
                        }
                    }
                } else if ($isreadytoprocess){                    // IF READY TO PROCESS
                    if (!$isbtnnext) {
                        if ($v->idtypecostcontrol == 1) {
                            if ($isidcardestimator) {
                                $isbtnnext = true;
                            }
                        } else if ($v->idtypecostcontrol == 2) {
                            if ($isidcardestimator) {
                                $isbtnnext = true;
                            }
                        } else if ($v->idtypecostcontrol == 3) {
                            if ($isdirector) {
                                $isbtnnext = true;
                            }
                        }
                    }
                } else if ($isaccbyestimator){                    // IF VERIFIED
                    if (!$isbtnnext) {
                        if ($iscreater || $isidcardsales) {
                            $isbtnnext = true;
                        }
                    }
                } else if ($isaccbymarketing){                    // IF APPROVED
                    if (!$isbtnnext) {
                        if ($isprojectmanager) {
                            $isbtnnext = true;
                        }
                    }
                } else if ($isaccbyprojectmanager){                    // IF APPROVED
                    if (!$isbtnnext) {
                        if ($isdirector) {
                            $isbtnnext = true;
                        }
                    }
                } else if ($isaccbydirector){                    // IF APPROVED
                    if (!$isbtnnext) {
                        if ($v->idtypecostcontrol == 1) {
                            if ($iscreater || $isidcardsales) {
                                $isbtnnext = true;
                            }
                        } else if ($v->idtypecostcontrol == 2) {
                            if ($iscreater || $isidcardsales) {
                                $isbtnnext = true;
                            }
                        } else if ($v->idtypecostcontrol == 3) {
                            if ($iscreater) {
                                $isbtnnext = true;
                            }
                        }
                    }
                } else if ($isok) {
                    $isbtnnext = false;
                }
                
                $nextstatus = $fixconditions[$v->mtypetranctypestatus_noid]['next_noid'];
                if ($nextstatus == 4011 && ($iscreater || $isidcardsales)) {
                    $isbtnnext = true;
                } else if ($nextstatus == 4012 && ($iscreater || $isidcardsales)) {
                    $isbtnnext = true;
                } else if ($nextstatus == 4013 && $isidcardestimator) {
                    $isbtnnext = true;
                } else if ($nextstatus == 4014 && ($iscreater || $isidcardsales)) {
                    $isbtnnext = true;
                } else if ($nextstatus == 4015 && $isprojectmanager) {
                    $isbtnnext = true;
                } else if ($nextstatus == 4016 && $isdirector) {
                    $isbtnnext = true;
                } else if ($nextstatus == 4017 && ($iscreater || $isidcardsales)) {
                    $isbtnnext = true;
                }
            } else {
                if ($isfinished || $iscanceled) {
                    $isbtnnext = false;
                }
            }

            $btnnext = '';
            if ($v->idtypecostcontrol == 1 || $v->idtypecostcontrol == 2) {
                if ($isbtnnext) {
                    $btnnext = '<a onclick="showModalSNST('.$v->noid.','.$v->mtypetranc_noid.', '.$snst[$fixconditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->noid.', \''.$snst[$fixconditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->nama.'\')" title="Set to '.$snst[$fixconditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->nama.'" class="btn btn-sm flat" style="background-color: '.@(new static)->color[$snst[$fixconditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->classcolor].'"><i class="fa fa-arrow-right icon-xs text-light" style="text-align:center"></i> </a>';
    
                    if ($isaccbydirector && ($iscreater || $isidcardsales || $isroot)) {
                        $btnnext = '<a onclick="showModalSendToOrder('.$v->noid.','.$v->mtypetranc_noid.')" title="Sent to Sales Order" class="btn btn-sm flat" style="background-color: '.@(new static)->color[$snst[$fixconditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->classcolor].'"><i class="fa fa-arrow-right icon-xs text-light" style="text-align:center"></i> </a>';
                    }
                }
            } else if ($v->idtypecostcontrol == 3) {   
                if ($isbtnnext) {
                    $btnnext = '<a onclick="showModalSNST('.@$v->noid.','.@$v->mtypetranc_noid.', '.@$snst[$ps_conditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->noid.', \''.@$snst[$ps_conditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->nama.'\')" title="Set to '.@$snst[$ps_conditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->nama.'" class="btn btn-sm flat" style="background-color: '.@(new static)->color[$snst[$ps_conditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->classcolor].'"><i class="fa fa-arrow-right icon-xs text-light" style="text-align:center"></i> </a>';
                    if ($isaccbydirector && ($iscreater || $isidcardsales || $isroot)) {
                        $btnnext = '<a onclick="showModalSendToOrder('.$v->noid.','.$v->mtypetranc_noid.')" title="Sent to Sales Order" class="btn btn-sm flat" style="background-color: '.@(new static)->color[$snst[$ps_conditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->classcolor].'"><i class="fa fa-arrow-right icon-xs text-light" style="text-align:center"></i> </a>';
                    }
                }             
            }


            // $btnnext = '';
            // if ps_($fixconditions[$v->mtypetranctypestatus_noid]['next_idstatusbase'] && $fixconditions[$v->mtypetranctypestatus_noid]['isinternal']) {
            //     if ($fixconditions[$v->mtypetranctypestatus_noid]['next_idstatusbase'] == 3) {
            //         $btnnext = '<a onclick="showModalSendToOrder('.$v->noid.','.$v->mtypetranc_noid.')" title="Sent to RAP" class="btn btn-sm flat" style="background-color: '.@(new static)->color[$snst[$fixconditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->classcolor].'"><i class="'.$snst[$fixconditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->classicon.' icon-xs text-light" style="text-align:center"></i> </a>';
            //     } else {
            //         $btnnext = '<a onclick="showModalSNST('.$v->noid.','.$v->mtypetranc_noid.', '.$snst[$fixconditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->noid.', \''.$snst[$fixconditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->nama.'\')" title="Set to '.$snst[$fixconditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->nama.'" class="btn btn-sm flat" style="background-color: '.@(new static)->color[$snst[$fixconditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->classcolor].'"><i class="'.$snst[$fixconditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->classicon.' icon-xs text-light" style="text-align:center"></i> </a>';
            //     }
            // }


            $btnedit = '';
            $btndelete = '';
            $btnstn = '';
            $isbtnedit = false;
            $isbtndelete = false;
            $isbtnstn = false;

            // BTN EDIT
            if ($isedit) {
                if ($isdraft) {
                    if ($iscreater || $isidcardsales) {
                        $isbtnedit = true;
                    }
                } else if ($isreadytoprocess) {
                    if (@$isidcardestimator) {
                        $isbtnedit = true;
                    }
                } else if ($isaccbyestimator) {
                    $isbtnedit = false;
                } else if ($isaccbymarketing) {
                    $isbtnedit = false;
                } else if ($isaccbyprojectmanager) {
                    $isbtnedit = false;
                } else if ($isaccbydirector) {
                    $isbtnedit = false;
                }

                if ($isroot) {
                    $isbtnedit = true;
                }
            }

            // BTN DELETE
            if ($isdelete) {
                if ($iscreater) {
                    if ($isdraft) {
                        $isbtndelete = true;
                    } else if ($isreadytoprocess) {
                        $isbtndelete = true;
                    } else if ($isaccbyestimator) {
                        $isbtndelete = true;
                    }
                    // $isbtndelete = $fixconditions[$v->mtypetranctypestatus_noid]['actdelete'] ? true : false;
                }

                if ($isroot) {
                    $isbtndelete = true;
                }
            }

            $btnstn = $isbtnstn ? '<a onclick="sendToNext('.$v->noid.')" title="Send To Purchase Offer" class="btn btn-sm flat" style="background-color: #2AB4C0"><i class="icon-arrow-right icon-xs text-light"></i> </a>' : '';
            $btnedit = $isbtnedit ? '<a onclick="editData('.$v->noid.')" title="Edit Data" class="btn btn-warning btn-sm flat"><i class="fa fa-edit icon-xs" style="text-align:center"></i> </a>' : '';
            $btndelete = $isbtndelete ? '<a  onclick="showModalDelete('.$v->noid.')" title="Delete Data" class="btn btn-danger btn-sm flat"><i class="fa fa-trash icon-xs"></i> </a>' : '';
            $btnsapcf = ($iscreater || $isidcardsales || $isroot) && $isactsetstatus ? '<a onclick="showModalSAPCF('.$v->noid.','.$v->mtypetranc_noid.','.$v->mtypetranctypestatus_noid.')" title="Set Status" class="btn btn-dark btn-sm flat"><i class="icon-cogs icon-xs text-light" style="text-align:center"></i> </a>' : '';
            // $btnprint = $isprint ? '<a onclick="printData('.static::$main['noid'].',\''.base64_encode(static::$main['printcode'].'/'.$v->noid.'/'.date('Ymdhis')).'\')" title="Print Data" class="btn btn-sm flat" style="background-color: '.@(new static)->color['blue-soft'].'"><i class="fa fa-file icon-xs text-light" style="text-align:center;"></i> </a>' : '';
            $btnprintrap ='<a onclick="printRAP('.static::$main['noid'].',\''.base64_encode($v->noid.'/'.date('Ymdhis')).'\')" title="Print RAP BOM" class="btn btn-sm flat" style="background-color: '.@(new static)->color['blue-soft'].'"><i class="fa fa-file icon-xs text-light" style="text-align:center;"></i> </a>';
            $btnprintws ='<a onclick="printWS('.static::$main['noid'].',\''.base64_encode($v->noid.'/'.date('Ymdhis')).'\')" title="Print Jadwal Pekerjaan" class="btn btn-sm flat" style="background-color: '.@(new static)->color['blue-soft'].'"><i class="fa fa-file icon-xs text-light" style="text-align:center;"></i> </a>';
            $btnprintrecap ='<a onclick="printRecap('.static::$main['noid'].',\''.base64_encode($v->noid.'/'.date('Ymdhis')).'\')" title="Print Rekapitulasi" class="btn btn-sm flat" style="background-color: '.@(new static)->color['blue-soft'].'"><i class="fa fa-file icon-xs text-light" style="text-align:center;"></i> </a>';
            $btnprintps ='<a onclick="printPS('.static::$main['noid'].',\''.base64_encode($v->noid.'/'.date('Ymdhis')).'\')" title="Print Jadwal Purchasing" class="btn btn-sm flat" style="background-color: '.@(new static)->color['blue-soft'].'"><i class="fa fa-file icon-xs text-light" style="text-align:center;"></i> </a>';
            $btnlog = $islog ? '<a onclick="showModalLog('.$v->noid.', \''.$v->kode.'\', \''.$v->mtypetranc_nama.'\')" title="Show Log Data" class="btn btn-sm flat" style="background-color: #8775A7"><i class="icon-file-text icon-xs text-light"></i> </a>' : '';
            $btnview = $isview ? '<a onclick="viewData('.$v->noid.')" title="View Data" class="btn btn-info btn-sm flat"><i class="fa fa-eye icon-xs"></i> </a>' : '';


            // $btnsapcf = $fixconditions[$v->mtypetranctypestatus_noid]['actsetstatus'] ? '<a onclick="showModalSAPCF({noid:'.$v->noid.',idtypetranc:'.$v->mtypetranc_noid.',idstatustranc:'.$v->mtypetranctypestatus_noid.'})" title="Set Status" class="btn btn-dark btn-sm flat"><i class="icon-cogs icon-xs text-light" style="text-align:center"></i> </a>' : '';
            // $btnprint = '<a onclick="printData('.static::$main['noid'].',\''.base64_encode(static::$main['printcode'].'/'.$v->noid.'/'.date('Ymdhis')).'\')" title="Print Data" class="btn btn-sm flat" style="background-color: '.@(new static)->color['blue-soft'].'"><i class="fa fa-file icon-xs text-light" style="text-align:center;"></i> </a>';
            // $btnlog = '<a onclick="showModalLog('.$v->noid.', \''.$v->kode.'\', \''.$v->mtypetranc_nama.'\')" title="Show Log Data" class="btn btn-sm flat" style="background-color: #8775A7"><i class="icon-file-text icon-xs text-light"></i> </a>';
            // $btnview = $isview ? '<a onclick="viewData('.$v->noid.')" title="View Data" class="btn btn-info btn-sm flat"><i class="fa fa-eye icon-xs"></i> </a>' : '';
            // $btnedit = $isedit ? '<a onclick="editData('.$v->noid.')" title="Edit Data" class="btn btn-warning btn-sm flat"><i class="fa fa-edit icon-xs" style="text-align:center"></i> </a>' : '';
            // $btndelete = $isdelete ? '<a  onclick="showModalDelete('.$v->noid.')" title="Delete Data" class="btn btn-danger btn-sm flat"><i class="fa fa-trash icon-xs"></i> </a>' : '';

            $allbtn = $btnprev;
            $allbtn .= $btnnext;
            $allbtn .= $btnsapcf;
            // $allbtn .= $btnprint;
            $allbtn .= $btnprintrap;
            $allbtn .= $btnprintws;
            $allbtn .= $btnprintrecap;
            $allbtn .= $btnprintps;
            $allbtn .= $btnlog;
            $allbtn .= $btnview;
            $allbtn .= $btnedit;
            $allbtn .= $btndelete;

            $action = [
                'action' => '<div class="d-flex align-items-center pull-right button-action button-action-'.$k.'">
                    '.$allbtn.'
                </div>'
            ];
            $array['idtypetranc'] = $data[$k]->mtypetranc_nama;
            $array['idstatustranc'] = $data[$k]->mtypetranctypestatus_nama;
            $data[$k] = (object)array_merge($checkbox, $_no, $array, $action);
            $no++;
        }
        
        return response()->json([
            'data' => $data,
            'draw' => intval($request['draw']),
            'recordsFiltered' => $resulttotal,
            'recordsTotal' => $resulttotal,
            'allnoid' => $allnoid,
            'datefilter' => $result['datefilter']
        ]);
    }

    public function download($id){
        $result = MP::getUrlDownload(['id'=>$id]);
        $file_path = public_path($result->filelocation.'/'.$result->filename);
        return response()->download( $file_path);
    }

    public function download_template(){
        $file_path = public_path('data/salesinquiry/templaterap/Template_RAP.xlsx');
        return response()->download( $file_path);
    }

    public function get_log(Request $request) {
        $statusadd = $request->get('statusadd');
        $noid = $request->get('noid');
        $kode = $request->get('kode');
        $noidmore = $request->get('noidmore');
        $fieldmore = $request->get('fieldmore');
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->get('search')['value'];
        $orderbycolumn = $request->get('columns')[$request->get('order')[0]['column']]['data'];
        if ($orderbycolumn == 'checkbox' || $orderbycolumn == 'no') {
            $orderbycolumn = 'noid';
        }
        $orderby = [
            'column' => $orderbycolumn,
            'sort' => $request->get('order')[0]['dir']
        ];

        $data = MP::getLog($noid, $kode, $start, $length, $search, $orderby);
        
        if ($search) {
            $resulttotal = count($data);
        } else {
            $resulttotal = count(MP::findLog($noid, $kode));
        }
        // $resulttotal = count($data);

        $no = $start+1;
        foreach ($data as $k => $v) {
            // $checkbox['checkbox'] = '<input type="checkbox" id="checkbox-'.$k.'" onchange="selectCheckbox(\'checkbox-'.$k.'\')">';
            $_no['no'] = $no;
            // $data[$k]->keterangan = substr($v->keterangan, 0, 39).' <button class="btn btn-info btn-sm" onclick="getDatatableLog('.$v->idreff.', \''.$v->kode.'\', '.$v->idreff.', \'keterangan\')">More</button>';
            $data[$k]->logsubjectfull = $v->logsubject;
            $data[$k]->keteranganfull = $v->keterangan;


            // $btnlogsubject = strlen($v->logsubject) <= 44 ? '' : ' <button id="morelog-logsubject-'.$v->noid.'" class="btn btn-success btn-sm" onclick="moreLog('.$v->noid.', \'logsubject\')"><i class="icon-angle-right"></i></button>';
            // $btnketerangan = strlen($v->keterangan) <= 44 ? '' : ' <button id="morelog-keterangan-'.$v->noid.'" class="btn btn-success btn-sm" onclick="moreLog('.$v->noid.', \'keterangan\')"><i class="icon-angle-right"></i></button>';

            $btnlogsubject = strlen($v->logsubject) <= 44 ? '' : '...';
            $btnketerangan = strlen($v->keterangan) <= 44 ? '' : '...';

            $data[$k]->logsubject = substr($v->logsubject, 0, 44).'<span id="logsubject-'.$v->noid.'" style="display: none">'.substr($v->logsubject, 44).'</span>'.$btnlogsubject;
            $data[$k]->keterangan = substr($v->keterangan, 0, 44).'<span id="keterangan-'.$v->noid.'" style="display: none">'.substr($v->keterangan, 44).'</span>'.$btnketerangan;
            $data[$k]->docreate = $this->dbdateTohumandate($data[$k]->docreate);
            $array = (array)$data[$k];
            $data[$k] = (object)array_merge($_no, $array);
            $no++;
        }
        
        return response()->json([
            'data' => $data,
            'draw' => intval($request['draw']),
            'recordsFiltered' => $resulttotal,
            'recordsTotal' => $resulttotal
        ]);
    }

    public function get_costcontrol(Request $request) {
        $statusadd = $request->get('statusadd');
        $statusdetail = $request->get('statusdetail');
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->get('search')['value'];
        $orderbycolumn = $request->get('columns')[$request->get('order')[0]['column']]['data'];
        if ($orderbycolumn == 'checkbox' || $orderbycolumn == 'no') {
            $orderbycolumn = 'noid';
        }
        $orderby = [
            'column' => $orderbycolumn,
            'sort' => $request->get('order')[0]['dir']
        ];

        $data = MP::getCostControl($start, $length, $search, $orderby);
        
        if ($search) {
            $resulttotal = count($data);
        } else {
            $resulttotal = count(MP::getData(['table'=>'salesorder','select'=>['noid'],'isactive'=>true]));
        }

        $no = $start+1;
        foreach ($data as $k => $v) {
            $checkbox['checkbox'] = '<input type="checkbox" id="checkbox-'.$k.'" onchange="selectCheckbox(\'checkbox-'.$k.'\')">';
            $_no['no'] = $no;
            $data[$k]->tanggal = $this->dbdateTohumandate($data[$k]->tanggal);
            $array = (array)$data[$k];
            $action = [
                'action' => $statusdetail == 'true' ? '' : '<div class="d-flex align-items-center text-center button-action button-action-'.$k.'">
                    <a onclick="chooseCostControl('.$v->noid.', \''.$v->kode.'\', '.$v->idtypecostcontrol.')" title="Choose" class="btn btn-success btn-sm flat"><i class="fa fa-hand-o-up icon-xs" style="text-align:center"></i> Choose</a>
                </div>'
            ];
            $data[$k] = (object)array_merge($checkbox, $_no, $array, $action);
            $no++;
        }
        
        return response()->json([
            'data' => $data,
            'draw' => intval($request['draw']),
            'recordsFiltered' => $resulttotal,
            'recordsTotal' => $resulttotal
        ]);
    }

    public function get_detailpi(Request $request) {
        $statusdetail = json_decode($request->get('statusdetail'));
        $idcardsupplier = json_decode($request->get('idcardsupplier'));
        $idcompany = json_decode($request->get('idcompany'));
        $iddepartment = json_decode($request->get('iddepartment'));
        $idgudang = json_decode($request->get('idgudang'));
        $detailprev = json_decode($request->get('detailprev'));
        $unitqtyminpo = json_decode($request->get('unitqtyminpo'));
        $stp = $request->get('searchtypeprev');
        $searchprev = $request->get('searchprev');
        $search = $request->get('search')['value'];
        $start = $request->get('start');
        $length = $request->get('length');
        $nextnoid = MP::getNextNoid(static::$main['table']);
        $nextnoiddetail = MP::getNextNoid(static::$main['tabledetail']);
        
        $params = [
            'idcardsupplier' => $idcardsupplier,
            'idcompany' => $idcompany,
            'iddepartment' => $iddepartment,
            'idgudang' => $idgudang,
            'detailprev' => $detailprev,
            'searchtypeprev' => $stp,
            'searchprev' => $searchprev,
            'search' => $search,
            'start' => $start,
            'length' => $length,
        ];
        $data = $stp == 1 ? MP::getDetailPrev($params) : MP::getDetailInv($params);

        if ($search) {
            $qty = count($data);
        } else {
            $params['start'] = null;
            $params['length'] = null;
            $qty = count($stp == 1 ? MP::getDetailPrev($params) : MP::getDetailInv($params));
        }

        $no = $start+1;
        $index = 0;
        $result = [];
        $pattern = "/".$search."/i";

        foreach ($data as $k => $v) {
            $v = (array)$v;
            $v['checkbox'] = '<input type="checkbox" id="checkbox-detail-'.$k.'" onchange="selectCheckbox(\'checkbox-detail-'.$k.'\')">';
            $v['no'] = $no;
            if ($stp == 2) {
                $v['noid'] = $nextnoid;
                $v['idmaster'] = '-';
                $v['kodeprev'] = '-';
                // $v['idinventor'] = '-';
                // $v['kodeinventor'] = '-';
                // $v['namainventor'] = '-';
                $v['namainventor2'] = '-';
                $v['keterangan'] = '-';
                $v['idgudang'] = '-';
                $v['idcompany'] = '-';
                $v['iddepartment'] = '-';
                $v['idsatuan'] = '-';
                $v['namasatuan'] = '-';
                $v['konvsatuan'] = '-';
                $v['unitqty'] = '-';
                $v['unitqtysisa'] = '-';
                $v['unitprice'] = '-';
                $v['subtotal'] = '-';
                $v['discountvar'] = '-';
                $v['discount'] = '-';
                $v['nilaidisc'] = '-';
                $v['idtypetax'] = '-';
                $v['idtax'] = '-';
                $v['prosentax'] = '-';
                $v['nilaitax'] = '-';
                $v['hargatotal'] = '-';
                $v['namacurrency'] = '-';
                $v['idcompany'] = $idcompany;
                $v['iddepartment'] = $iddepartment;
                $v['idgudang'] = $idgudang;
            } 

            $isaction = true;
            foreach ($detailprev as $k2 => $v2) {
                if ($v2->noid == $v['noid']) {
                    if ($stp == 1) {
                        $v['unitqtysisa'] = $v['unitqtysisa']-$v2->unitqty < 0 ? 0 : $v['unitqtysisa']-$v2->unitqty;
                        $isaction = false;
                        break;
                    }
                }
            }

            $v['action'] = $isaction ? '<div class="d-flex align-items-center text-center button-action-dprev button-action-dprev-'.$k.'">
                <a onclick="chooseDetailPrev(
                    '.$v['noid'].',
                    \''.@$v['idmaster'].'\',
                    \''.@$v['kodeprev'].'\',
                    \''.@$v['idinventor'].'\',
                    \''.@$v['kodeinventor'].'\',
                    \''.@$v['namainventor'].'\',
                    \''.@$v['namainventor2'].'\',
                    \''.@$v['keterangan'].'\',
                    \''.@$v['idgudang'].'\',
                    \''.@$v['idcompany'].'\',
                    \''.@$v['iddepartment'].'\',
                    \''.@$v['idsatuan'].'\',
                    \''.@$v['namasatuan'].'\',
                    \''.@$v['konvsatuan'].'\',
                    \''.@$v['unitqty'].'\',
                    \''.@$v['unitqtysisa'].'\',
                    \''.@$v['unitprice'].'\',
                    \''.@$v['subtotal'].'\',
                    \''.@$v['discountvar'].'\',
                    \''.@$v['discount'].'\',
                    \''.@$v['nilaidisc'].'\',
                    \''.@$v['idtypetax'].'\',
                    \''.@$v['idtax'].'\',
                    \''.@$v['prosentax'].'\',
                    \''.@$v['nilaitax'].'\',
                    \''.@$v['hargatotal'].'\',
                    \''.@$v['namacurrency'].'\'
                )" title="Choose" class="btn btn-success btn-sm flat"><i class="icon-hand-up icon-xs" style="text-align:center"></i> Choose</a>
            </div>' : '';

                $result[] = $v;
            $no++;
            $nextnoid;
        }

        return response()->json([
            'data' => $result,
            'draw' => intval($request['draw']),
            'recordsFiltered' => $qty,
            'recordsTotal' => $qty,
            'length' => $request->get('length')
        ]);
    }
    
    public function get_detail(Request $request) {
        $data = json_decode($request->get('data'));
        $statusdetail = json_decode($request->get('statusdetail'));
        $condition = json_decode($request->get('condition'));
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->get('search')['value'];

        $qtyshow = 0;
        $qtyall = 0;
        $no = 1;
        $index = 0;
        $result = [];
        $pattern = "/".$search."/i";
        // dd($start);

        foreach ($data as $k => $v) {
            if ($v != null) {
                if ($k >= (int)$start && $qtyshow <= $length) {
                    $insertrecord = false;
                    foreach ($v as $k2 => $v2) {
                        if ($v2->name != 'idinventor') {
                            if (!array_key_exists('value', (array)$v2)) {
                                $v2->value = 0;
                            }
                            if (preg_match($pattern, $v2->value)) {
                                $insertrecord = true;
                            }
                            if ($v2->name == 'subtotal') {
                                $result[$index][$v2->name] = is_numeric($v2->value) ? MyHelper::currencyConv($v2->value) : $v2->value;
                            } else if ($v2->name == 'unitprice' || $v2->name == 'hargatotal') {
                                $result[$index][$v2->name] = filter_var($v2->value, FILTER_SANITIZE_NUMBER_INT);
                            } else {
                                $result[$index][$v2->name] = $v2->value;
                            }
                        }
                    }
                    if (!$insertrecord) {
                        array_pop($result);
                        continue;
                    }
                    $result[$index]['checkbox'] = '<input type="checkbox" id="checkbox-detail-'.$k.'" onchange="selectCheckbox(\'checkbox-detail-'.$k.'\')">';
                    $result[$index]['no'] = $no;

                    $allbtn = true ? '<a onclick="editDetail({id: \''.$k.'\', update: false})"  class="btn btn-primary btn-sm flat mr-2"><i class="fa fa-plus icon-xs"></i> </a>' : '';
                    $allbtn .= $condition->v ? '<a onclick="editDetail({id: \''.$k.'\', update: false})"  class="btn btn-success btn-sm flat mr-2"><i class="fa fa-search icon-xs"></i> </a>' : '';
                    $allbtn .= $condition->u ? '<a onclick="editDetail({id: \''.$k.'\', update: true})" class="btn btn-warning btn-sm flat mr-1"><i class="fa fa-edit icon-xs" style="text-align:center"></i> </a>': '';
                    $allbtn .= $condition->d ? '<a  onclick="removeDetail(\''.$k.'\')" class="btn btn-danger btn-sm flat mr-0"><i class="fa fa-trash icon-xs"></i> </a>' : '';
    
                    $result[$index]['action'] = '<div class="text-right button-action-tab button-action-tab-'.$k.'">
                        '.$allbtn.'
                    </div>';
                    if (count($result) == $length || count($data) == $k+1) {
                        break;
                    }
                    $index++;
                    $qtyshow++;
                }
                $no++;
            }
        }

        return response()->json([
            'data' => $result,
            'draw' => intval($request['draw']),
            'recordsFiltered' => $search ? count($result) : count($data),
            'recordsTotal' => $search ? count($result) : count($data),
            'length' => $request->get('length')
        ]);
    }

    private function humandateTodbdate($date) {
        if ($date) {
            $arrdate = explode('-', $date);
            $arrtime = explode(' ', $date);
            $year = explode(' ',$arrdate[2])[0];
            $month = $arrdate[1];
            $day = $arrdate[0];
            $time = count(explode(' ',$arrdate[2])) == 2 ? explode(' ',$arrdate[2])[1] : '';

            if ($month == 'Jan') {
                $month = '01';
            } else if ($month == 'Feb') {
                $month = '02';
            } else if ($month == 'Mar') {
                $month = '03';
            } else if ($month == 'Apr') {
                $month = '04';
            } else if ($month == 'May') {
                $month = '05';
            } else if ($month == 'Jun') {
                $month = '06';
            } else if ($month == 'Jul') {
                $month = '07';
            } else if ($month == 'Aug') {
                $month = '08';
            } else if ($month == 'Sep') {
                $month = '09';
            } else if ($month == 'Oct') {
                $month = '10';
            } else if ($month == 'Nov') {
                $month = '11';
            } else if ($month == 'Dec') {
                $month = '12';
            }

            return $year.'-'.$month.'-'.$day.' '.$time;
        } else {
            return NULL;
        }
    }

    private function dbdateTohumandate($date) {
        if ($date) {
            $arrdate = explode('-', $date);
            $arrtime = explode(' ', $date);
            $year = $arrdate[0];
            $month = $arrdate[1];
            $day = explode(' ',$arrdate[2])[0];

            if ($month == '01') {
                $month = 'Jan';
            } else if ($month == '02') {
                $month = 'Feb';
            } else if ($month == '03') {
                $month = 'Mar';
            } else if ($month == '04') {
                $month = 'Apr';
            } else if ($month == '05') {
                $month = 'May';
            } else if ($month == '06') {
                $month = 'Jun';
            } else if ($month == '07') {
                $month = 'Jul';
            } else if ($month == '08') {
                $month = 'Aug';
            } else if ($month == '09') {
                $month = 'Sep';
            } else if ($month == '10') {
                $month = 'Oct';
            } else if ($month == '11') {
                $month = 'Nov';
            } else if ($month == '12') {
                $month = 'Dec';
            }

            return count($arrtime) > 1 ? $day.'-'.$month.'-'.$year.' '.$arrtime[1] : $day.'-'.$month.'-'.$year;
        } else {
            return NULL;
        }
    }

    public function save(Request $request) {
        $noid = $request->statusadd == 'true' ? MP::getNextNoid('salesinquiry') : $request->noid;
        $noidjob = MP::getNextNoid('salesinquiryjob');
        $noidgroup = MP::getNextNoid('salesinquirygroup');
        $noidsubgroup = MP::getNextNoid('salesinquirysubgroup');
        $noidinventory = MP::getNextNoid('salesinquiryinventory');
        $noidpurchaseschedule = MP::getNextNoid('salesinquirypurchaseschedule');
        $noidmilestone = MP::getNextNoid('salesinquirymilestone');
        $noidheader = MP::getNextNoid('salesinquiryheader');
        $noiddetail = MP::getNextNoid('salesinquirydetail');
        $noidcdf = MP::getNextNoid(static::$main['tablecdf']);

        $formmaster = [
            'noid' => $noid,
            'idtypetranc' => $request['formheader']['idtypetranc'],
            'idstatustranc' => $request['formheader']['idstatustranc'],
            'idstatuscard' => Session::get('noid'),
            'dostatus' => date('Y-m-d H:i:s'),
            'idtypecostcontrol' => $request['formheader']['idtypecostcontrol'],
            'kode' => $request['formheader']['kode'],
            'projectname' => $request['formheader']['projectname'],
            'projectlocation' => $request['formheader']['projectlocation'],
            'projectvalue' => filter_var($request['formheader']['projectvalue'], FILTER_SANITIZE_NUMBER_INT),
            'projecttype' => $request['formheader']['projecttype'],
            'tanggal' => MyHelper::humandateTodbdate($request['formheader']['tanggal']),
            'tanggaltax' => MyHelper::humandateTodbdate(@$request['formheader']['tanggaltax']),
            'tanggaldue' => MyHelper::humandateTodbdate($request['formheader']['tanggaldue']),
            'idcardsales' => $request['formheader']['idcardsales'],
            'idcardcustomer' => $request['formheader']['idcardcustomer'],
            'idcardpj' => $request['formheader']['idcardpj'],
            'idcardkoor' => $request['formheader']['idcardkoor'],
            'idcardsitekoor' => $request['formheader']['idcardsitekoor'],
            'idcarddrafter' => $request['formheader']['idcarddrafter'],
            'idcardestimator' => $request['formheader']['idcardestimator'],
            'keterangan' => $request['formfooter']['keterangan'],
            'nourut' => $request['formheader']['nourut'],
        ];
        
        $formjobs = [];
        $formgroups = [];
        $formsubgroups = [];
        $forminventories = [];
        $formpurchaseschedules = [];

        if (!is_null($request->jobs) && count($request->jobs) > 0) {
            foreach($request->jobs as $k => $v) {
                $formjobs[] = [
                    'noid' => $noidjob,
                    'idsalesinquiry' => $noid,
                    'name' => $v['detail']['namainventor'],
                    'idunit' => $v['detail']['idsatuan'],
                    'quantity' => $v['detail']['unitqty'],
                    'total' => $v['detail']['subtotal'],
                    'description' => $v['detail']['keterangan'],
                ];
    
                if (@$v['nodes'] && count($v['nodes']) > 0) {
                    foreach ($v['nodes'] as $k2 => $v2) {
                        $formgroups[] = [
                            'noid' => $noidgroup,
                            'idsalesinquiry' => $noid,
                            'idsalesinquiryjob' => $noidjob,
                            'idinventory' => $v2['detail']['kodeinventor'],
                            'description' => $v2['detail']['keterangan'],
                            'prdate' => @MyHelper::humandateTodbdate($v2['detail']['prdate']),
                            'reqpodate' => @MyHelper::humandateTodbdate($v2['detail']['reqpodate']),
                            'reqonsitedate' => @MyHelper::humandateTodbdate($v2['detail']['reqonsitedate']),
                        ];

                        if (@$v2['detail']['purchasedates_array'] && count($v2['detail']['purchasedates_array']) > 0) {
                            foreach ($v2['detail']['purchasedates_array'] as $pdk => $pdv) {
                                $formpurchaseschedules[] = [
                                    'noid' => $noidpurchaseschedule,
                                    'idsalesinquiry' => $noid,
                                    'idsalesinquirygroup' => $noidgroup,
                                    'plandate' => MyHelper::humandateTodbdate($pdv['plandate']),
                                    'plantotal' => $pdv['plantotal'],
                                    'actualdate' => MyHelper::humandateTodbdate($pdv['actualdate']),
                                    'actualtotal' => $pdv['actualtotal'],
                                    'description' => $v2['detail']['keterangan']
                                ];
                                $noidpurchaseschedule++;
                            }
                        }
    
                        if (@$v2['nodes'] && count($v2['nodes']) > 0) {
                            foreach ($v2['nodes'] as $k3 => $v3) {
                                $formsubgroups[] = [
                                    'noid' => $noidsubgroup,
                                    'idsalesinquiry' => $noid,
                                    'idsalesinquirygroup' => $noidgroup,
                                    'idinventory' => $v3['detail']['kodeinventor'],
                                    'description' => $v3['detail']['keterangan'],
                                ];
    
                                if (@$v3['nodes'] && count($v3['nodes']) > 0) {
                                    foreach ($v3['nodes'] as $k4 => $v4) {
                                        $forminventories[] = [
                                            'noid' => $noidinventory,
                                            'idsalesinquiry' => $noid,
                                            'idsalesinquirysubgroup' => $noidsubgroup,
                                            'idinventory' => @$v4['detail']['idinventory'],
                                            'alias' => @$v4['detail']['alias'],
                                            'description' => @$v4['detail']['description'],
                                            'idunit' => @$v4['detail']['idunit'],
                                            'quantity' => @$v4['detail']['quantity'],
                                            'buyprice' => @$v4['detail']['buyprice'],
                                            'baseprice' => @$v4['detail']['baseprice'],
                                            'offerprice' => @$v4['detail']['offerprice'],
                                            'idtax' => @$v4['detail']['idtax'],
                                            'total' => @$v4['detail']['total'],
                                        ];
            
                                        $noidinventory++;
                                    }
                                }
                                $noidsubgroup++;
                            }
                        }
                        $noidgroup++;
                    }
                }
                $noidjob++;
            }
        }

        $formmilestones = [];
        $formheaders = [];
        $formdetails = [];

        if (!is_null($request->milestones) && count($request->milestones) > 0) {
            foreach($request->milestones as $k => $v) {
                $formmilestones[] = [
                    'noid' => $noidmilestone,
                    'idsalesinquiry' => $noid,
                    // 'idmilestone' => $v['detail']['idmilestone'],
                    'name' => $v['detail']['name'],
                    'startdate' => MyHelper::humandateTodbdate($v['detail']['startdate']),
                    'stopdate' => MyHelper::humandateTodbdate($v['detail']['stopdate']),
                    'description' => $v['detail']['description'],
                    'isshowreport' => $v['detail']['isshowreport'],
                ];

                if (@$v['nodes'] && count($v['nodes']) > 0) {
                    foreach ($v['nodes'] as $k2 => $v2) {
                        $formheaders[] = [
                            'noid' => $noidheader,
                            'idsalesinquiry' => $noid,
                            'idsalesinquirymilestone' => $noidmilestone,
                            'name' => $v2['detail']['name'],
                            'startdate' => MyHelper::humandateTodbdate($v2['detail']['startdate']),
                            'stopdate' => MyHelper::humandateTodbdate($v2['detail']['stopdate']),
                            'description' => $v2['detail']['description'],
                            'isshowreport' => $v2['detail']['isshowreport'],
                        ];

                        if (@$v2['nodes'] && count($v2['nodes']) > 0) {
                            foreach ($v2['nodes'] as $k3 => $v3) {
                                $formdetails[] = [
                                    'noid' => $noiddetail,
                                    'idsalesinquiry' => $noid,
                                    'idsalesinquiryheader' => $noidheader,
                                    'name' => $v3['detail']['name'],
                                    'startdate' => MyHelper::humandateTodbdate($v3['detail']['startdate']),
                                    'stopdate' => MyHelper::humandateTodbdate($v3['detail']['stopdate']),
                                    'idunit' => $v3['detail']['idunit'],
                                    'quantity' => $v3['detail']['quantity'],
                                    'planprice' => $v3['detail']['planprice'] == '0' ? 0 : filter_var($v3['detail']['planprice'], FILTER_SANITIZE_NUMBER_INT),
                                    'actualprice' => $v3['detail']['actualprice'] == '0' ? 0 : filter_var($v3['detail']['actualprice'], FILTER_SANITIZE_NUMBER_INT),
                                    'description' => $v3['detail']['description'],
                                    'isshowreport' => $v3['detail']['isshowreport'],
                                ];

                                $noiddetail++;
                            }
                        }
                        $noidheader++;
                    }
                }
                $noidmilestone++;
            }
        }

        // FORM MODAL CUSTOMER
        $formcustomer = [];
        if (@$request->formmodalcustomer) {
            foreach ($request->formmodalcustomer as $k => $v) {
                $formcustomer[str_replace('mcc_', '', $k)] = $v;
            }
            $formcustomer['idlokasiprop'] = $formcustomer['idlokasiprop_input'];
            unset($formcustomer['idlokasiprop_input']);
            $formcustomer['idlokasikota'] = $formcustomer['idlokasikota_input'];
            unset($formcustomer['idlokasikota_input']);
            $formcustomer['idlokasikec'] = $formcustomer['idlokasikec_input'];
            unset($formcustomer['idlokasikec_input']);
            $formcustomer['idlokasikel'] = $formcustomer['idlokasikel_input'];
            unset($formcustomer['idlokasikel_input']);
        }

        // FORM CDF
        $cdf_checks = json_decode($request->get('cdf_checks'));
        $cdf_selecteds = (array)json_decode($request->get('cdf_selecteds'));
        $formcdf = (array)$request->get('formcdf');
        $cdf = [];
        if (!is_null($cdf_checks) && @count($cdf_checks) > 0) {
            foreach ($cdf_checks as $k => $v) {
                $cdf[$k] = [
                    'noid' => $noidcdf,
                    'idtypetranc' => static::$main['idtypetranc'],
                    'idreff' => $request->statusadd == 'true' ? $noid : $noid,
                    'idstatus' => '',
                    'varstatus' => '',
                    'idcardstatus' => '',
                    'dostatus' => date('Y-m-d H:i:s'),
                    'kode' => '',
                    'barcode' => '',
                    'nama' => $formcdf['cdf_detail_'.$k.'_nama'],
                    'keterangan' => $formcdf['cdf_detail_'.$k.'_keterangan'],
                    'iddmsfile' => $cdf_selecteds['cdf-'.$v]->noid,
                    'ishardcopy' => @$formcdf['cdf_detail_'.$k.'_ishardcopy']?$formcdf['cdf_detail_'.$k.'_ishardcopy']:0,
                    'ispublic' => @$formcdf['cdf_detail_'.$k.'_ispublic']?$formcdf['cdf_detail_'.$k.'_ispublic']:0,
                    'idworkflow' => '',
                    'filenamesystem' => '',
                    'filenameoriginal' => $cdf_selecteds['cdf-'.$v]->nama,
                    'filefolder' => '',
                    'iddmstype' => '',
                    'iddmsfolder' => '',
                    'iddmsfolderbase' => '',
                    'iddmskategori' => '',
                    'doctag' => '',
                    'doclabel' => '',
                    'docauthor' => '',
                    'doccreate' => '',
                    'docupdate' => '',
                    'filesize' => '',
                    'idcardupload' => '',
                    'doupload' => '',
                    'dopublish' => '',
                    'doexpired' => date('Y-m-d H:i:s', strtotime($formcdf['cdf_detail_'.$k.'_doexpired'])),
                    'doreviewlast' => date('Y-m-d H:i:s', strtotime($formcdf['cdf_detail_'.$k.'_doreviewlast'])),
                    'doreviewnext' => date('Y-m-d H:i:s', strtotime($formcdf['cdf_detail_'.$k.'_doreviewnext'])),
                ];

                if ($request->statusadd == 'true') {
                    $cdf[$k]['idcreate'] = Session::get('noid');
                    $cdf[$k]['docreate'] = date('Y-m-d H:i:s');
                } else {
                    $cdf[$k]['idupdate'] = Session::get('noid');
                    $cdf[$k]['lastupdate'] = date('Y-m-d H:i:s');
                }

                $noidcdf++;
            }
        }

        // FORM LOG
        $next_idstatustranc = json_decode($request->get('next_idstatustranc'));
        $next_nama = json_decode($request->get('next_nama'));
        $next_keterangan = json_decode($request->get('next_keterangan'));
        if ($request->statusadd == 'true') {
            $savetonext = $next_idstatustranc ? ' (Save to '.$next_nama.')' : '';
            $logsubject = 'Data '.$request->get('formheader')['kodetypetranc'].' Kode = '.$formmaster['kode'].' CREATE'.$savetonext;
            if ($next_idstatustranc) {
                $keterangan = $next_keterangan;
            } else {
                $keterangan = 'Data '.$request->get('formheader')['kodetypetranc'].' Kode = '.$formmaster['kode'].' CREATE'.$savetonext.' BY '.Session::get('myusername').' ON '.date('Y-m-d H:i:s');
            }
        } else {
            $savetonext = $next_idstatustranc ? ' (Edit to '.$next_nama.')' : '';
            $logsubject = 'Data '.$request->get('formheader')['kodetypetranc'].' Kode = '.$formmaster['kode'].' EDIT'.$savetonext;
            if ($next_idstatustranc) {
                $keterangan = $next_keterangan;
            } else {
                $keterangan = 'Data '.$request->get('formheader')['kodetypetranc'].' Kode = '.$formmaster['kode'].' EDIT'.$savetonext.' BY '.Session::get('myusername').' ON '.date('Y-m-d H:i:s');
            }
        }

        $formlog = [
            'idtypetranc' => $formmaster['idtypetranc'],
            'idtypeaction' => $request->statusadd == 'true' ? 1 : 2,
            'logsubject' => $logsubject,
            'keterangan' => $keterangan,
            'idreff' => $formmaster['noid'],
            'idreffd' => 0,
            'iddevice' => 0,
            'devicetoken' => $request->get('_token'),
            'idcreate' => Session::get('noid'),
            'docreate' => date('Y-m-d H:i:s'),
        ];

        if ($request->statusadd == 'true') {
            $formmaster['idcreate'] = Session::get('noid');
            $formmaster['docreate'] = date('Y-m-d H:i:s');

            $insert = MP::saveData([
                'formmaster' => $formmaster,
                'formjobs' => $formjobs,
                'formgroups' => $formgroups,
                'formsubgroups' => $formsubgroups,
                'forminventories' => $forminventories,
                'formpurchaseschedules' => $formpurchaseschedules,
                'formmilestones' => $formmilestones,
                'formheaders' => $formheaders,
                'formdetails' => $formdetails,
                'formcustomer' => $formcustomer,
                'formcdf' => $cdf,
                'formlog' => $formlog,
            ]);
            return response()->json($insert);
        } else {
            $idsalesinquiry = $formmaster['noid'];
            unset($formmaster['noid']);
            $formmaster['idupdate'] = Session::get('noid');
            $formmaster['lastupdate'] = date('Y-m-d H:i:s');

            $update = MP::updateData([
                'idsalesinquiry' => $idsalesinquiry,
                'formmaster' => $formmaster,
                'formjobs' => $formjobs,
                'formgroups' => $formgroups,
                'formsubgroups' => $formsubgroups,
                'forminventories' => $forminventories,
                'formpurchaseschedules' => $formpurchaseschedules,
                'formmilestones' => $formmilestones,
                'formheaders' => $formheaders,
                'formdetails' => $formdetails,
                'formcustomer' => $formcustomer,
                'formcdf' => $cdf,
                'formlog' => $formlog,
            ]);
            return response()->json($update);
        }
        
    }

    public function save_(Request $request) {
        $cdf_checks = json_decode($request->get('cdf_checks'));
        $cdf_selecteds = (array)json_decode($request->get('cdf_selecteds'));
        $statusadd = json_decode($request->get('statusadd'));
        $next_idstatustranc = json_decode($request->get('next_idstatustranc'));
        $next_nama = json_decode($request->get('next_nama'));
        $next_keterangan = json_decode($request->get('next_keterangan'));
        $noid = $request->get('noid');
        $noiddetail = [];
        $noidcdf = [];
        $resultnoiddetail = MP::getData(['table'=>static::$main['tabledetail'],'select'=>['noid'],'where'=>[['idmaster','=',$noid]]]);
        $resultnoidcdf = MP::getData(['table'=>static::$main['tablecdf'],'select'=>['noid'],'where'=>[['idreff','=',$noid],['idtypetranc','=',static::$main['idtypetranc']]]]);
        foreach ($resultnoiddetail as $k => $v) {
            $noiddetail[] = $v->noid;
        }
        foreach ($resultnoidcdf as $k => $v) {
            $noidcdf[] = $v->noid;
        }
        $nextnoid = MP::getNextNoid(static::$main['table']);
        $nextnoiddetail = MP::getNextNoid(static::$main['tabledetail']);
        $nextnoidcdf = MP::getNextNoid(static::$main['tablecdf']);

        if ($statusadd) {
            $others = [
                'noid' => $nextnoid,
                'idtypetranc' => 0,
                'idcreate' => Session::get('noid'),
                'docreate' => date('Y-m-d H:i:s'),
            ];
        } else {
            $others = [
                'noid' => $noid,
                'idtypetranc' => 0,
                'idupdate' => Session::get('noid'),
                'lastupdate' => date('Y-m-d H:i:s'),
            ];
        }

        foreach ($request->get('formheader') as $k => $v) {
            if ($k == 'kodetypetranc') {
                continue;
            } else if ($k == 'idcostcontrol') {
                $formheader[$k] = $v ? $v : 0;
            } else if ($k == 'dostatus' || $k == 'tanggal' || $k == 'tanggaldue') {
                $formheader[$k] = $this->humandateTodbdate($v);
            } else if ($k == 'idstatustranc') {
                $formheader[$k] = $statusadd
                    ? $next_idstatustranc ? $next_idstatustranc : 4011
                    : $v;
            } else if ($k == 'projectvalue') {
                $formheader[$k] = filter_var($v, FILTER_SANITIZE_NUMBER_INT);
            } else {
                $formheader[$k] = $v;
            }
        }
        foreach ($request->get('formmodaltanggal') as $k => $v) {
            $formmodaltanggal[$k] = $this->humandateTodbdate($v);
        }
        foreach ($request->get('formfooter') as $k => $v) {
            if ($k == 'subtotal') {
                $formfooter['totalsubtotal'] = filter_var($v, FILTER_SANITIZE_NUMBER_INT);
            } else if ($k == 'total') {
                $formfooter['generaltotal'] = filter_var($v, FILTER_SANITIZE_NUMBER_INT);
            } else {
                $formfooter[$k] = $v;
            }
        }

        $input = [
            'others' => $others,
            'formheader' => $formheader,
            'forminternal' => $request->get('forminternal'),
            'formfooter' => $formfooter,
            'formmodaltanggal' => $formmodaltanggal,
            'prd' => $request->get('purchasedetail')
        ];

        $table = array_merge(
            $input['others'],
            $input['formheader'],
            // $input['forminternal'],
            $input['formmodaltanggal'],
            $input['formfooter']
        );

        $tabledetail = [];
        if ($input['prd'] != null) {
            foreach ($input['prd'] as $k => $v) {
                $tabledetail[$k]['noid'] = $nextnoiddetail;
                $tabledetail[$k]['idmaster'] = $statusadd ? $nextnoid : $noid;
                foreach ($v as $k2 => $v2) {
                    if ($v2['name'] == 'noid' || $v2['name'] == 'kodeinventor' || $v2['name'] == 'namainventor' || $v2['name'] == 'idcurrency' || $v2['name'] == 'namacurrency' || $v2['name'] == 'namasatuan') {
                        continue;
                    } else if ($v2['name'] == 'namainventor2') {
                        $tabledetail[$k]['namainventor'] = $v2['value'];
                    } else if ($v2['name'] == 'unitprice' || $v2['name'] == 'subtotal' || $v2['name'] == 'hargatotal') {
                        $tabledetail[$k][$v2['name']] = filter_var($v2['value'], FILTER_SANITIZE_NUMBER_INT);
                    } else if ($v2['name'] == 'discountvar') {
                        $tabledetail[$k][$v2['name']] = $v2['value'] ? $v2['value'] : 0;
                    } else {
                        $tabledetail[$k][$v2['name']] = $v2['value'];
                    }
                }
                $nextnoiddetail++;
            }
        }

        $formcdf = (array)$request->get('formcdf');
        $cdf = [];
        if (count($cdf_checks) > 0) {
            foreach ($cdf_checks as $k => $v) {
                $cdf[$k] = [
                    'noid' => $nextnoidcdf,
                    'idtypetranc' => static::$main['idtypetranc'],
                    'idreff' => $statusadd ? $nextnoid : $noid,
                    'idstatus' => '',
                    'varstatus' => '',
                    'idcardstatus' => '',
                    'dostatus' => date('Y-m-d H:i:s'),
                    'kode' => '',
                    'barcode' => '',
                    'nama' => $formcdf['cdf_detail_'.$k.'_nama'],
                    'keterangan' => $formcdf['cdf_detail_'.$k.'_keterangan'],
                    'iddmsfile' => $cdf_selecteds['cdf-'.$v]->noid,
                    'ishardcopy' => @$formcdf['cdf_detail_'.$k.'_ishardcopy']?$formcdf['cdf_detail_'.$k.'_ishardcopy']:0,
                    'ispublic' => @$formcdf['cdf_detail_'.$k.'_ispublic']?$formcdf['cdf_detail_'.$k.'_ispublic']:0,
                    'idworkflow' => '',
                    'filenamesystem' => '',
                    'filenameoriginal' => $cdf_selecteds['cdf-'.$v]->nama,
                    'filefolder' => '',
                    'iddmstype' => '',
                    'iddmsfolder' => '',
                    'iddmsfolderbase' => '',
                    'iddmskategori' => '',
                    'doctag' => '',
                    'doclabel' => '',
                    'docauthor' => '',
                    'doccreate' => '',
                    'docupdate' => '',
                    'filesize' => '',
                    'idcardupload' => '',
                    'doupload' => '',
                    'dopublish' => '',
                    'doexpired' => date('Y-m-d H:i:s', strtotime($formcdf['cdf_detail_'.$k.'_doexpired'])),
                    'doreviewlast' => date('Y-m-d H:i:s', strtotime($formcdf['cdf_detail_'.$k.'_doreviewlast'])),
                    'doreviewnext' => date('Y-m-d H:i:s', strtotime($formcdf['cdf_detail_'.$k.'_doreviewnext'])),
                ];

                if ($statusadd) {
                    $cdf[$k]['idcreate'] = Session::get('noid');
                    $cdf[$k]['docreate'] = date('Y-m-d H:i:s');
                } else {
                    $cdf[$k]['idupdate'] = Session::get('noid');
                    $cdf[$k]['lastupdate'] = date('Y-m-d H:i:s');
                }

                $nextnoidcdf++;
            }
        }

        $data = [
            'master' => $table,
            'detail' => $tabledetail,
            'cdf' => $cdf
        ];

        if ($statusadd) {
            $savetonext = $next_idstatustranc ? ' (Save to '.$next_nama.')' : '';
            $logsubject = 'Data '.$request->get('formheader')['kodetypetranc'].' Kode = '.$data['master']['kode'].' CREATE'.$savetonext;
            if ($next_idstatustranc) {
                $keterangan = $next_keterangan;
            } else {
                $keterangan = 'Data '.$request->get('formheader')['kodetypetranc'].' Kode = '.$data['master']['kode'].' CREATE'.$savetonext.' BY '.Session::get('myusername').' ON '.date('Y-m-d H:i:s');
            }
        } else {
            $savetonext = $next_idstatustranc ? ' (Edit to '.$next_nama.')' : '';
            $logsubject = 'Data '.$request->get('formheader')['kodetypetranc'].' Kode = '.$data['master']['kode'].' EDIT'.$savetonext;
            if ($next_idstatustranc) {
                $keterangan = $next_keterangan;
            } else {
                $keterangan = 'Data '.$request->get('formheader')['kodetypetranc'].' Kode = '.$data['master']['kode'].' EDIT'.$savetonext.' BY '.Session::get('myusername').' ON '.date('Y-m-d H:i:s');
            }
        }

        $datalog = [
            'idtypetranc' => $data['master']['idtypetranc'],
            'idtypeaction' => $statusadd ? 1 : 2,
            'logsubject' => $logsubject,
            'keterangan' => $keterangan,
            'idreff' => $data['master']['noid'],
            'idreffd' => 0,
            'iddevice' => 0,
            'devicetoken' => $request->get('_token'),
            'idcreate' => $statusadd
                ? $data['master']['idcreate']
                : $data['master']['idupdate'],
            'docreate' => $statusadd
                ? $data['master']['docreate']
                : $data['master']['lastupdate'],
        ];
        
        if ($statusadd) {
            $result = MP::saveData(['data' => $data, 'datalog' => $datalog]);
        } else {
            $result = MP::updateData([
                'data' => $data, 
                'noid' => $noid, 
                'noiddetail' => $noiddetail, 
                'noidcdf' => $noidcdf, 
                'datalog' => $datalog
            ]);
        }

        return response()->json(['success' => $result['success'], 'message' => $result['message']]);
    }

    public function find(Request $request) {
        $noid = $request->get('noid');
        $result = MP::findData(['noid'=>$noid]);
        $result['master']->dostatus = $this->dbdateTohumandate($result['master']->dostatus);
        $result['master']->tanggal = $this->dbdateTohumandate($result['master']->tanggal);
        $result['master']->tanggaltax = $this->dbdateTohumandate($result['master']->tanggaltax);
        $result['master']->tanggaldue = $this->dbdateTohumandate($result['master']->tanggaldue);
        $result['master']->mtypetranctypestatus_nama = $result['master']->idstatustranc ? $result['master']->mtypetranctypestatus_nama : 'DRAFT';
        $result['master']->mtypetranctypestatus_classcolor = $result['master']->idstatustranc ? @(new static)->color[$result['master']->mtypetranctypestatus_classcolor] : 'grey';

        $result['mttts'] = Session::get(static::$main['noid'].'-conditions')[$result['master']->idstatustranc];

        // JOBS
        $jobs = [];
        foreach ($result['jobs'] as $k => $v) {
            $groups = [];
            
            foreach ($result['groups'] as $k2 => $v2) {
                if ($v2->idsalesinquiryjob == $v->noid) {
                    $subgroups = [];

                    foreach ($result['subgroups'] as $k3 => $v3) {
                        if ($v3->idsalesinquirygroup == $v2->noid) {
                            $inventories = [];
        
                            $totalinventories = 0;
                            foreach ($result['inventories'] as $k4 => $v4) {
                                if ($v4->idsalesinquirysubgroup == $v3->noid) {

                                    $iscomplete = false;
                                    if ($result['master']->idtypecostcontrol == 1 || $result['master']->idtypecostcontrol == 2) {
                                        $iscomplete = $v4->idinventory != '' && $v4->alias != '' && $v4->idunit != '' && $v4->quantity != '' && $v4->buyprice != 0 && $v4->baseprice != 0 && $v4->offerprice != 0;
                                        // ($v4->idinventory != '' ? 'true' : 'false').($v4->alias != '' ? 'true' : 'false').($v4->idunit != '' ? 'true' : 'false').($v4->quantity != '' ? 'true' : 'false').($v4->buyprice != 0 ? 'true' : 'false').($v4->baseprice != 0 ? 'true' : 'false').($v4->offerprice != 0 ? 'true' : 'false').($v4->idtax != null ? 'true' : 'false')
                                    } else {
                                        $iscomplete = $v4->idinventory != '' && $v4->alias != '' && $v4->idunit != '' && $v4->quantity != '' && $v4->offerprice != 0;
                                    }
                                    // dd(0 != null);
                                    // dd(is_null(null));
                
                                    $inventories[] = [
                                        'id' => $k4,
                                        'name' => $v4->idinventory_nama,
                                        'text' => ($iscomplete ? '' : '<span class="text-danger"><b>(!)</b></span>').' '.$v4->idinventory_nama.'
                                            <div class="row">
                                                <div class="col-md-1">
                                                </div>
                                                <div class="col-md-2">
                                                    <span>[Quantity='.$v4->quantity.']</span>
                                                </div>
                                                <div class="col-md-6">
                                                    <span>[Total='.MyHelper::currencyConv($v4->total).']</span>
                                                </div>
                                                <div class="col-md-3 pr-0">
                                                    <button type="button" class="btn btn-danger btn-sm pull-right" onclick="removeInventory({parentid: '.$k.', parentid2: '.$k2.', parentid3: '.$k3.', id: '.$k4.'})"><i class="fa fa-trash"></i></button>
                                                    <button type="button" class="btn btn-warning btn-sm pull-right" onclick="showModalInventory({type: \'update\', parentid: '.$k.', parentid2: '.$k2.', parentid3: '.$k3.', id: '.$k4.'})"><i class="fa fa-edit"></i></button>
                                                    <button type="button" class="btn btn-info btn-sm pull-right" onclick="showModalInventory({type: \'view\', parentid: '.$k.', parentid2: '.$k2.', parentid3: '.$k3.', id: '.$k4.'})"><i class="fa fa-eye"></i></button>
                                                </div>
                                            </div>'
                                        ,
                                        'parent_id' => $k3,
                                        'detail' => [
                                            'idinventory' => $v4->idinventory,
                                            'name' => $v4->idinventory_nama,
                                            'alias' => $v4->alias,
                                            'description' => $v4->description,
                                            'idunit' => $v4->idunit,
                                            'quantity' => $v4->quantity,
                                            'buyprice' => $v4->buyprice,
                                            'baseprice' => $v4->baseprice,
                                            'offerprice' => $v4->offerprice,
                                            'subtotal' => $v4->quantity*$v4->offerprice,
                                            'idtax' => $v4->idtax,
                                            'tax' => $v4->total*$v4->idtax_prosentax/100,
                                            'total' => $v4->total,
                                            'origintotal' => MyHelper::currencyConv($v4->total),
                                        ],
                                        'selectable' => true,
                                        'state' => [
                                            'checked' => false,
                                            'disabled' => false,
                                            'expanded' => true,
                                            'selected' => false
                                        ],
                                        'backColor' => '#e5e5e5',
                                        'nodes' => [],
                                    ];

                                    $totalinventories += $v4->total;
                                }
                            }
        
                            $subgroups[] = [
                                'id' => $k3,
                                'name' => $v3->idinventory_nama,
                                'text' => $v3->idinventory_nama.' [Total='.'$total_'.$k.'_'.$k2.'_'.$k3.']
                                    <button type="button" class="btn btn-danger btn-sm pull-right" onclick="removeSubgroup({parentid: '.$k.', parentid2: '.$k2.', id: '.$k3.'})"><i class="fa fa-trash"></i></button>
                                    <button type="button" class="btn btn-warning btn-sm pull-right" onclick="showModalSubgroup({type: \'update\', parentid: '.$k.', parentid2: '.$k2.', id: '.$k3.', idgroup: '.$v2->idinventory.'})"><i class="fa fa-edit"></i></button>
                                    <button type="button" class="btn btn-info btn-sm pull-right" onclick="showModalSubgroup({type: \'view\', parentid: '.$k.', parentid2: '.$k2.', id: '.$k3.', idgroup: '.$v2->idinventory.'})"><i class="fa fa-eye"></i></button>
                                    <button type="button" class="btn btn-primary btn-sm pull-right" onclick="showModalInventory({type: \'create\', parentid: '.$k.', parentid2: '.$k2.', parentid3: '.$k3.'})"><i class="fa fa-plus"></i></button>'
                                ,
                                'parent_id' => $k2,
                                'detail' => [
                                    'kodeinventor' => $v3->idinventory,
                                    'namainventor' => $v3->idinventory_nama,
                                    'keterangan' => $v3->description,
                                ],
                                'selectable' => true,
                                'state' => [
                                    'checked' => false,
                                    'disabled' => false,
                                    'expanded' => true,
                                    'selected' => false
                                ],
                                'backColor' => '#b8d8d8',
                                'nodes' => $inventories,
                            ];
                        }
                    }


                    // PURCHASE SCHEDULES

                    $purchasedates = '';
                    $purchasedates_array = [];
                    $purchasedates_array_setdates = [];
                    foreach ($result['purchaseschedules'] as $k4 => $v4) {
                        if ($v4->idsalesinquirygroup == $v2->noid) {
                            $purchasedates .= ($purchasedates == '' ? '' : ',').$v4->plandate;
                            $purchasedates_array[] = [
                                'plandate' => MyHelper::dbdateTohumandate($v4->plandate),
                                'plandate_js' => date('F j, Y', strtotime($v4->plandate)),
                                'plantotal' => $v4->plantotal,
                                'actualdate' => MyHelper::dbdateTohumandate($v4->actualdate),
                                'actualdate_js' => date('F j, Y', strtotime($v4->actualdate)),
                                'actualtotal' => $v4->actualtotal,
                            ];
                            $purchasedates_array_setdates[] = [
                                'y' => date('Y', strtotime($v4->plandate)),
                                'm' => date('n', strtotime($v4->plandate)),
                                'd' => date('j', strtotime($v4->plandate)),
                            ];
                        }
                    }

                    
                    // $purchasedates_array = [];
                    // if (@$v2->purchasedates) {
                    //     $purchasedates = explode(',',$v2->purchasedates);
                    //     foreach ($purchasedates as $pdk => $pdv) {
                    //         $purchasedates_array[] = [
                    //             'y' => date('Y', strtotime($pdv)),
                    //             'm' => date('n', strtotime($pdv)),
                    //             'd' => date('j', strtotime($pdv)),
                    //         ];
                    //     }
                    // }

                    $groups[] = [
                        'id' => $k2,
                        'name' => $v2->idinventory_nama,
                        'text' => $v2->idinventory_nama.' [Total='.'$total_'.$k.'_'.$k2.']
                            <button type="button" class="btn btn-danger btn-sm pull-right" onclick="removeGroup({parentid: '.$k.', id: '.$k2.'})"><i class="fa fa-trash"></i></button>
                            <button type="button" class="btn btn-warning btn-sm pull-right" onclick="showModalGroup({type: \'update\', parentid: '.$k.', id: '.$k2.'})"><i class="fa fa-edit"></i></button>
                            <button type="button" class="btn btn-info btn-sm pull-right" onclick="showModalGroup({type: \'view\', parentid: '.$k.', id: '.$k2.'})"><i class="fa fa-eye"></i></button>
                            <button type="button" class="btn btn-primary btn-sm pull-right" onclick="showModalSubgroup({type: \'create\', parentid: '.$k.', parentid2: '.$k2.', idgroup: '.$v2->idinventory.'})"><i class="fa fa-plus"></i></button>'
                        ,
                        'parent_id' => $k,
                        'detail' => [
                            'kodeinventor' => $v2->idinventory,
                            'namainventor' => $v2->idinventory_nama,
                            'keterangan' => $v2->description,
                            'prdate' => MyHelper::dbdateTohumandate($v2->prdate),
                            'reqpodate' => MyHelper::dbdateTohumandate($v2->reqpodate),
                            'reqonsitedate' => MyHelper::dbdateTohumandate($v2->reqonsitedate),
                            'purchasedates' => @$v2->purchasedates ? $v2->purchasedates : '',
                            'purchasedates' => $purchasedates,
                            'purchasedates_array' => $purchasedates_array,
                            'purchasedates_array_setdates' => $purchasedates_array_setdates,
                            // 'mindate' => [
                            //     'y' => date('Y', strtotime($result['master']->tanggal)),
                            //     'm' => date('n', strtotime($result['master']->tanggal)),
                            //     'd' => date('j', strtotime($result['master']->tanggal)),
                            // ],
                            // 'maxdate' => [
                            //     'y' => date('Y', strtotime($result['master']->tanggaldue)),
                            //     'm' => date('n', strtotime($result['master']->tanggaldue)),
                            //     'd' => date('j', strtotime($result['master']->tanggaldue)),
                            // ],
                        ],
                        'selectable' => true,
                        'state' => [
                            'checked' => false,
                            'disabled' => false,
                            'expanded' => true,
                            'selected' => false
                        ],
                        'backColor' => '#95bcbc',
                        'nodes' => $subgroups,
                    ];
                }
            }

            $jobs[] = [
                'id' => $k,
                'name' => $v->name,
                'text' => $v->name.' [Estimasi=<b>'.'$estimation_'.$k.'</b>][Total=<b>'.'$total_'.$k.'</b>]
                    <button type="button" class="btn btn-danger btn-sm pull-right" onclick="removeJob({id: '.$k.'})"><i class="fa fa-trash"></i></button>
                    <button type="button" class="btn btn-warning btn-sm pull-right" onclick="showModalJob({type: \'update\', id: '.$k.'})"><i class="fa fa-edit"></i></button>
                    <button type="button" class="btn btn-info btn-sm pull-right" onclick="showModalJob({type: \'view\', id: '.$k.'})"><i class="fa fa-eye"></i></button>
                    <button type="button" class="btn btn-primary btn-sm pull-right" onclick="showModalGroup({type: \'create\', parentid: '.$k.'})"><i class="fa fa-plus"></i></button>'
                ,
                'parent_id' => 0,
                'detail' => [
                    'namainventor' => $v->name,
                    'idsatuan' => $v->idunit,
                    'idsatuan_nama' => $v->idunit,
                    'unitqty' => $v->quantity,
                    'subtotal' => $v->total,
                    'keterangan' => $v->description,
                ],
                'selectable' => true,
                'state' => [
                    'checked' => false,
                    'disabled' => false,
                    'expanded' => true,
                    'selected' => false
                ],
                'backColor' => '#a1b0c9',
                'nodes' => $groups,
            ];
        }


        // MILESTONES
        $milestones = [];
        foreach ($result['milestones'] as $k => $v) {
            $headers = [];
            
            foreach ($result['headers'] as $k2 => $v2) {
                if ($v2->idsalesinquirymilestone == $v->noid) {
                    $details = [];

                    foreach ($result['details'] as $k3 => $v3) {
                        if ($v3->idsalesinquiryheader == $v2->noid) {
        
                            $details[] = [
                                'id' => $k3,
                                'name' => $v3->name,
                                'text' => $v3->name.'
                                    <button type="button" class="btn btn-danger btn-sm pull-right" onclick="removeDetail({parentid: '.$k.', parentid2: '.$k2.', id: '.$k3.'})"><i class="fa fa-trash"></i></button>
                                    <button type="button" class="btn btn-warning btn-sm pull-right" onclick="showModalDetail({type: \'update\', parentid: '.$k.', parentid2: '.$k2.', id: '.$k3.'})"><i class="fa fa-edit"></i></button>
                                    <button type="button" class="btn btn-info btn-sm pull-right" onclick="showModalDetail({type: \'view\', parentid: '.$k.', parentid2: '.$k2.', id: '.$k3.'})"><i class="fa fa-eye"></i></button>'
                                ,
                                'parent_id' => $k2,
                                'detail' => [
                                    'name' => $v3->name,
                                    'startdate' => MyHelper::dbdateTohumandate($v3->startdate),
                                    'stopdate' => MyHelper::dbdateTohumandate($v3->stopdate),
                                    'idunit' => $v3->idunit,
                                    'quantity' => $v3->quantity,
                                    'planprice' => $v3->planprice,
                                    'actualprice' => $v3->actualprice,
                                    'description' => $v3->description,
                                    'isshowreport' => $v3->isshowreport,
                                ],
                                'selectable' => true,
                                'state' => [
                                    'checked' => false,
                                    'disabled' => false,
                                    'expanded' => true,
                                    'selected' => false
                                ],
                                'backColor' => '#f2d6bc',
                                'nodes' => [],
                            ];
                        }
                    }

                    $headers[] = [
                        'id' => $k2,
                        'name' => $v2->name,
                        'text' => $v2->name.'
                            <button type="button" class="btn btn-danger btn-sm pull-right" onclick="removeHeader({parentid: '.$k.', id: '.$k2.'})"><i class="fa fa-trash"></i></button>
                            <button type="button" class="btn btn-warning btn-sm pull-right" onclick="showModalHeader({type: \'update\', parentid: '.$k.', id: '.$k2.'})"><i class="fa fa-edit"></i></button>
                            <button type="button" class="btn btn-info btn-sm pull-right" onclick="showModalHeader({type: \'view\', parentid: '.$k.', id: '.$k2.'})"><i class="fa fa-eye"></i></button>
                            <button type="button" class="btn btn-primary btn-sm pull-right" onclick="showModalDetail({type: \'create\', parentid: '.$k.', parentid2: '.$k2.'})"><i class="fa fa-plus"></i></button>'
                        ,
                        'parent_id' => $k,
                        'detail' => [
                            'name' => $v2->name,
                            'startdate' => MyHelper::dbdateTohumandate($v2->startdate),
                            'stopdate' => MyHelper::dbdateTohumandate($v2->stopdate),
                            'description' => $v2->description,
                            'isshowreport' => $v2->isshowreport,
                        ],
                        'selectable' => true,
                        'state' => [
                            'checked' => false,
                            'disabled' => false,
                            'expanded' => true,
                            'selected' => false
                        ],
                        'backColor' => '#e5b385',
                        'nodes' => $details,
                    ];
                }
            }

            $milestones[] = [
                'id' => $k,
                // 'name' => $v->idmilestone_name,
                'name' => $v->name,
                // 'text' => $v->idmilestone_name.'
                //     <button type="button" class="btn btn-danger btn-sm pull-right" onclick="removeMilestone({id: '.$k.'})"><i class="fa fa-trash"></i></button>
                //     <button type="button" class="btn btn-warning btn-sm pull-right" onclick="showModalMilestone({type: \'update\', id: '.$k.'})"><i class="fa fa-edit"></i></button>
                //     <button type="button" class="btn btn-info btn-sm pull-right" onclick="showModalMilestone({type: \'view\', id: '.$k.'})"><i class="fa fa-eye"></i></button><button type="button" class="btn btn-primary btn-sm pull-right" onclick="showModalHeader({type: \'create\', parentid: '.$k.'})"><i class="fa fa-plus"></i></button>'
                // ,
                'text' => $v->name.'
                    <button type="button" class="btn btn-danger btn-sm pull-right" onclick="removeMilestone({id: '.$k.'})"><i class="fa fa-trash"></i></button>
                    <button type="button" class="btn btn-warning btn-sm pull-right" onclick="showModalMilestone({type: \'update\', id: '.$k.'})"><i class="fa fa-edit"></i></button>
                    <button type="button" class="btn btn-info btn-sm pull-right" onclick="showModalMilestone({type: \'view\', id: '.$k.'})"><i class="fa fa-eye"></i></button><button type="button" class="btn btn-primary btn-sm pull-right" onclick="showModalHeader({type: \'create\', parentid: '.$k.'})"><i class="fa fa-plus"></i></button>'
                ,
                'parent_id' => 0,
                'detail' => [
                    // 'idmilestone' => $v->idmilestone,
                    'name' => $v->name,
                    'startdate' => MyHelper::dbdateTohumandate($v->startdate),
                    'stopdate' => MyHelper::dbdateTohumandate($v->stopdate),
                    'description' => $v->description,
                    'isshowreport' => $v->isshowreport,
                ],
                'selectable' => true,
                'state' => [
                    'checked' => false,
                    'disabled' => false,
                    'expanded' => true,
                    'selected' => false
                ],
                'backColor' => '#b78f8f',
                'nodes' => $headers,
            ];
        }

        return response()->json([
            'success' => true,
            'message' => 'Find Successfully',
            'data' => [
                'master' => $result['master'],
                'mttts' => $result['mttts'],
                'jobs' => $jobs,
                'milestones' => $milestones,
                'mcc' => $result['mcc'],
                'cdf' => $result['cdf'],
                'totalactualpurchase' => $result['totalactualpurchase'],
            ]
        ]);
    }
    
    public function generate_code(Request $request) {
        $params = [
            'tablemaster' => static::$main['table'],
            'generatecode' => static::$main['generatecode'],
            'idtypecostcontrol' => $request->get('idtypecostcontrol')
        ];
        if ($params['idtypecostcontrol'] == 0) {
            $params['kodetypecostcontrol'] = 'NA';
        } else if ($params['idtypecostcontrol'] == 1) {
            $params['kodetypecostcontrol'] = 'PRO';
        } else if ($params['idtypecostcontrol'] == 2) {
            $params['kodetypecostcontrol'] = 'TR';
        } else if ($params['idtypecostcontrol'] == 3) {
            $params['kodetypecostcontrol'] = 'PS';
        }
        $result = MP::generateCode($params);

        return response()->json([
            'success' => true,
            'message' => 'Find Successfully',
            'data' => $result
        ]);
    }

    public function get_info() {
        $costcontrol = MP::getDefaultCostControl();

        return response()->json([
            'success' => true,
            'message' => 'Find Successfully',
            'data' => [
                'pr' => (object)[
                    'noid' => null,
                    'idtypetranc' => static::$main['idtypetranc']
                ],
                'mttts' => Session::get(static::$main['noid'].'-conditions')[static::$main['idtypetranc'].'1'],
                'mtypetranctypestatus' => [
                    'noid' => MP::findMTypeTrancTypeStatus(static::$main['idtypetranc'])->noid,
                    'nama' => 'DRAFT',
                    'classcolor' => @(new static)->color['grey-silver']
                ],
                'mcarduser' => [
                    'noid' => Session::get('noid'),
                    'nama' => Session::get('myusername')
                ],
                'idtypecostcontrol' => $costcontrol->idtypecostcontrol,
                'idcostcontrol' => $costcontrol->noid,
                'kodecostcontrol' => $costcontrol->kode,
                'dostatus' => date('d-M-Y H:i:s'),
                'idcompany' => Session::get('idcompany'),
                'iddepartment' => Session::get('iddepartment'),
                'idgudang' => Session::get('idgudang'),
            ]
        ]);
    }

    public function find_last_supplier() {
        $result = MP::findLastSupplier();

        return response()->json([
            'success' => true,
            'message' => 'Find Successfully',
            'data' => $result
        ]);
    }

    public function send_to_next(Request $request) {
        $si = MP::findData(['noid'=>$request->noid]);

        if ($si['master']->idtypecostcontrol == 0) {
            $kodetypecostcontrol = 'NA';
        } else if ($si['master']->idtypecostcontrol == 1) {
            $kodetypecostcontrol = 'PRO';
        } else if ($si['master']->idtypecostcontrol == 2) {
            $kodetypecostcontrol = 'TR';
        } else if ($si['master']->idtypecostcontrol == 3) {
            $kodetypecostcontrol = 'PS';
        }

        $gc = MP::generateCode([
            'tablemaster' => 'salesorder',
            'generatecode' => 'generatecode/402/1',
            'idtypecostcontrol' => $si['master']->idtypecostcontrol,
            'kodetypecostcontrol' => $kodetypecostcontrol,
            'data' => $si['master']
        ]);

        $formmaster = (array)$si['master'];
        $formmaster['noid'] = MP::getNextNoid('salesorder');
        $formmaster['idtypetranc'] = 402;
        $formmaster['idstatustranc'] = 4021;
        $formmaster['kodereff'] = $formmaster['kode'];
        $formmaster['kode'] = $gc['kode'];
        $formmaster['nourut'] = $gc['nourut'];
        unset($formmaster['kodetypetranc']);
        unset($formmaster['mtypetranctypestatus_nama']);
        unset($formmaster['mtypetranctypestatus_classcolor']);
        unset($formmaster['mcarduser_myusername']);
        unset($formmaster['namacurrency']);
        unset($formmaster['projecttype_nama']);
        unset($formmaster['idtypecostcontrol_nama']);
        unset($formmaster['idcardcustomer_nama']);
        unset($formmaster['idcardsales_nama']);
        unset($formmaster['idcardkoor_nama']);
        unset($formmaster['idcardpj_nama']);
        unset($formmaster['idcardsitekoor_nama']);
        unset($formmaster['idcarddrafter_nama']);
        unset($formmaster['idcardestimator_nama']);
        unset($formmaster['isdirector']);

        $sendtonext = MP::sendToNext([
            'formmaster' => $formmaster
        ]);

        $rc = new Request();
        $rc->setMethod('POST');
        $rc->request->add(['noid'=>$request->noid]);
        $rc->request->add(['idstatustranc'=>4017]);
        $rc->request->add(['statustranc'=>'FINISHED']);
        $rc->request->add(['idtypetranc'=>401]);
        $rc->request->add(['contractnumber'=>$formmaster['contractnumber']]);
        $rc->request->add(['keterangan'=>$formmaster['keterangan']]);
        // dd($rc->all());
        return $this->snst($rc);
    }

    public function send_to_next_(Request $request) {
        $si = MP::findData(['noid'=>$request->noid]);
        $si['master'] = (array)$si['master'];

        $nextnoid = RAP::getNextNoid('prjmrap');
        $nextnoiddetail = RAP::getNextNoid('prjmrapmilestone');

        $gc = RAP::generateCode([
            'tablemaster' => 'prjmrap',
            'generatecode' => 'generatecode/602/1',
            'idcostcontrol' => $si['master']['noid'],
            'kodecostcontrol' => $si['master']['kode'],
            'data' => $si['master']
        ]);

        $paramsmilestone = [];
        // $nourutdetail = 1;
        // foreach ($si['detail'] as $k => $v) {
        //     $v = (array)$v;
        //     $paramsmilestone[$k] = [
        //         'noid' => $nextnoiddetail,
        //         'idmaster' => $nextnoid,
        //         'nama' => $v['namainventor'],
        //         'idinventor' => $v['idinventor'],
        //         'nourut' => $nourutdetail,
        //         'keterangan' => $v['keterangan'],
        //         'idcardpj' => null,
        //         'idcardpjwakil' => null,
        //         'tanggalstart' => null,
        //         'tanggalstop' => null,
        //         'totalqty' => 0,
        //         'totalitem' => 0,
        //         'totalsubtotal' => 0,
        //         'idcreate' => Session::get('noid'),
        //         'docreate' => date('Y-m-d H:i:s'),
        //     ];
        //     $nextnoiddetail++;
        //     $nourutdetail++;
        // }

        $paramsrab = [
            'master' => [
                'noid' => $nextnoid,
                'idstatustranc' => 6021,
                'idstatuscard' => $si['master']['idstatuscard'],
                'dostatus' => $si['master']['dostatus'],
                'idtypetranc' => 602,
                'idtypecostcontrol' => $si['master']['idtypecostcontrol'],
                'idcostcontrol' => null,
                'kode' => $gc['kode'],
                'kodereff' => $si['master']['kode'],
                'tanggal' => $si['master']['tanggal'],
                'tanggaltax' => $si['master']['tanggaltax'],
                'tanggaldue' => $si['master']['tanggaldue'],
                'projectname' => $si['master']['projectname'],
                'projectlocation' => $si['master']['projectlocation'],
                'projectvalue' => $si['master']['projectvalue'],
                'projecttype' => $si['master']['projecttype'],
                'idcardsales' => $si['master']['idcardsales'],
                'idcardcashier' => null,
                'idcardcustomer' => $si['master']['idcardcustomer'],
                'idcardpj' => $si['master']['idcardpj'],
                'idcardkoor' => $si['master']['idcardkoor'],
                'idcardsitekoor' => $si['master']['idcardsitekoor'],
                'idcarddrafter' => $si['master']['idcarddrafter'],
                'idakuncustomer' => null,
                'totalqty' => 0,
                'totalitem' => 0,
                'totalsubtotal' => 0,
                'totaldisc' => 0,
                'totaltax' => 0,
                'discnota' => 0,
                'totaldiscnota' => 0,
                'generalprice' => 0,
                'biayalain' => 0,
                'kodevoucher' => null,
                'generaltotal' => 0,
                'totalbayar' => 0,
                'totaldeposit' => 0,
                'totalhutang' => 0,
                'totalsisahutang' => 0,
                'idtypepayment' => 1,
                'paycardbank' => null,
                'paycardname' => null,
                'paycardnumber' => null,
                'paycardexpired' => null,
                'idcashbankbayar' => 0,
                'idakunbayar' => 0,
                'idakundeposit' => 0,
                'idakunbiayalain' => 0,
                'idakunhutang' => 0,
                'idakunsales' => 0,
                'idakunpersediaan' => 0,
                'idakundiscount' => 0,
                'keterangan' => $si['master']['keterangan'],
                'idcurrency' => $si['master']['idcurrency'],
                'konvcurr' => 0,
                'idgudang' => $si['master']['idgudang'],
                'nourut' => $gc['nourut'],
                'idcompany' => null,
                'iddepartment' => null,
                'idtypetrancprev' => null,
                'idtrancprev' => null,
                'idtypetrancorder' => null,
                'idtrancorder' => null,
                'idsalesinquiry' => $si['master']['noid'],
                'idcreate' => $si['master']['idcreate'],
                'docreate' => $si['master']['docreate'],
                'idupdate' => 0,
                'lastupdate' => null,
            ],
            'milestone' => $paramsmilestone
        ];

        // Insert RAP
        $insertrab = RAP::saveData($paramsrab);
        if (!$insertrab) return ['success' => false, 'message' => 'Error Insert RAP'];
        
        $resultprojecttenders = MP::getMCardByKodePosition(['kode'=>'PT']);
        $projecttenders = [];
        foreach ($resultprojecttenders as $k => $v) {
            $projecttenders[] = $v->noid;
        }

        //NOTIF NEED PROPOSAL
        $insertnotif = MP::insertNotification([
            'idtypenotification' => 0,
            'idtypetranc' => $paramsrab['master']['idtypetranc'],
            'kodereff' => $paramsrab['master']['kode'],
            'statusreff' => $paramsrab['master']['idstatustranc'],
            'urlreff' => 50011102,
            'idcardassigns' => $projecttenders,
            'tanggal' => $paramsrab['master']['tanggal'],
            'keterangan' => 'Project '.$paramsrab['master']['kode']. ' perlu proposal',
        ]);
        if (!$insertnotif) return ['success' => false, 'message' => 'Error Insert Notif'];

        return ['success' => true, 'message' => 'Success Set Need Proposal'];


        // $devicetoken = $request->get('_token');
        // $noid = $request->get('noid');
        // $find = MP::findData($noid);

        // $nextnoid = MP::getNextNoid('salesorder');
        // $nextnoiddetail = MP::getNextNoid('salesorderdetail');
        // if ($find['master']->idtypecostcontrol == 0) {
        //     $kodetypecostcontrol = 'NA';
        // } else if ($find['master']->idtypecostcontrol == 1) {
        //     $kodetypecostcontrol = 'PRO';
        // } else if ($find['master']->idtypecostcontrol == 2) {
        //     $kodetypecostcontrol = 'TR';
        // } else if ($find['master']->idtypecostcontrol == 3) {
        //     $kodetypecostcontrol = 'PS';
        // }
        // $gc = SalesOrder::generateCode([
        //     'tablemaster' => 'salesorder',
        //     'generatecode' => 'generatecode/402/1',
        //     'idtypecostcontrol' => $find['master']->idtypecostcontrol,
        //     'kodetypecostcontrol' => $kodetypecostcontrol,
        //     'data' => $find['master']
        // ]);

        // $detail = [];
        // foreach ($find['detail'] as $k => $v) {
        //     $detail[$k] = [
        //         'noid' => $nextnoiddetail,
        //         'idmaster' => $nextnoid,
        //         'kodeprev' => '',
        //         'idinventor' => $v->idinventor,
        //         'namainventor' => $v->namainventor,
        //         'keterangan' => $v->keterangan,
        //         'idgudang' => $v->idgudang,
        //         'idsatuan' => $v->idsatuan,
        //         'konvsatuan' => $v->konvsatuan,
        //         'unitqty' => $v->unitqty,
        //         'unitqtysisa' => $v->unitqtysisa,
        //         'unitprice' => $v->unitprice,
        //         'subtotal' => $v->subtotal,
        //         'discountvar' => $v->discountvar,
        //         'discount' => $v->discount,
        //         'nilaidisc' => $v->nilaidisc,
        //         'idtypetax' => $v->idtypetax,
        //         'idtax' => $v->idtax,
        //         'prosentax' => $v->prosentax,
        //         'nilaitax' => $v->nilaitax,
        //         'hargatotal' => $v->hargatotal,
        //     ];
        //     $nextnoiddetail++;
        // }
        // $master = [
        //     'noid' => $nextnoid,
        //     'idstatustranc' => 4021,
        //     'idstatuscard' => Session::get('noid'),
        //     'dostatus' => $find['master']->dostatus,
        //     'idtypetranc' => 402,
        //     'idtypecostcontrol' => $find['master']->idtypecostcontrol,
        //     'kode' => $gc['kode'],
        //     'kodereff' => $find['master']->kode,
        //     'projectname' => $find['master']->projectname,
        //     'projectlocation' => $find['master']->projectlocation,
        //     'projectvalue' => $find['master']->projectvalue,
        //     'projecttype' => $find['master']->projecttype,
        //     'tanggal' => $find['master']->tanggal,
        //     'tanggaltax' => $find['master']->tanggaltax,
        //     'tanggaldue' => $find['master']->tanggaldue,
        //     'idcardpj' => $find['master']->idcardpj,
        //     'idcardkoor' => $find['master']->idcardkoor,
        //     'idcardsitekoor' => $find['master']->idcardsitekoor,
        //     'idcarddrafter' => $find['master']->idcarddrafter,
        //     'idcardsales' => $find['master']->idcardsales,
        //     'idcardcashier' => $find['master']->idcardcashier,
        //     'idcardcustomer' => $find['master']->idcardcustomer,
        //     'idcardsponsor' => null,
        //     'totalqty' => $find['master']->totalqty,
        //     'totalitem' => $find['master']->totalitem,
        //     'totalsubtotal' => $find['master']->totalsubtotal,
        //     'totaldisc' => $find['master']->totaldisc,
        //     'totaltax' => $find['master']->totaldisc,
        //     'discnota' => $find['master']->discnota,
        //     'totaldiscnota' => $find['master']->totaldiscnota,
        //     'generalprice' => $find['master']->generalprice,
        //     'biayalain' => $find['master']->biayalain,
        //     'kodevoucher' => $find['master']->kodevoucher,
        //     'generaltotal' => $find['master']->generaltotal,
        //     'totalbayar' => $find['master']->totalbayar,
        //     'totaldeposit' => $find['master']->totaldeposit,
        //     'totalhutang' => $find['master']->totalhutang,
        //     'totalsisahutang' => $find['master']->totalsisahutang,
        //     'idtypepayment' => $find['master']->idtypepayment,
        //     'paycardbank' => $find['master']->paycardbank,
        //     'paycardname' => $find['master']->paycardname,
        //     'paycardnumber' => $find['master']->paycardnumber,
        //     'paycardexpired' => $find['master']->paycardexpired,
        //     'idcashbankbayar' => $find['master']->idcashbankbayar,
        //     'idakunbayar' => $find['master']->idakunbayar,
        //     'idakundeposit' => $find['master']->idakundeposit,
        //     'idakunbiayalain' => $find['master']->idakunbiayalain,
        //     'idakunhutang' => $find['master']->idakunhutang,
        //     'idakunsales' => $find['master']->idakunsales,
        //     'idakunpersediaan' => $find['master']->idakunpersediaan,
        //     'idakundiscount' => $find['master']->idakundiscount,
        //     'keterangan' => $find['master']->keterangan,
        //     'idcurrency' => $find['master']->idcurrency,
        //     'konvcurr' => $find['master']->konvcurr,
        //     'idgudang' => $find['master']->idgudang,
        //     'nourut' => $gc['nourut'],
        //     'idcompany' => $find['master']->idcompany,
        //     'iddepartment' => $find['master']->iddepartment,
        //     'idtypetrancprev' => $find['master']->idtypetrancprev,
        //     'idtrancprev' => $find['master']->idtrancprev,
        //     'idtypetrancorder' => $find['master']->idtypetrancorder,
        //     'idtrancorder' => $find['master']->idtrancorder,
        //     'idcreate' => Session::get('noid'),
        //     'docreate' => date('Y-m-d H:i:s'),
        // ];

        // $insertsalesorder = SalesOrder::saveData([
        //     'data' => [
        //         'salesorder' => $master,
        //         'salesorderdetail' => $detail,
        //     ]
        // ]);
        dd($insertsalesorder);
        
        $idtypecostcontrol = $find['purchase']->idtypecostcontrol;
        $kodereff = $find['purchase']->kodereff;
        $nextnoid = MP::getNextNoid(static::$main['table2']);
        $nextnoiddetail = MP::getNextNoid(static::$main['tabledetail2']);
        $nextkodetypetranc = MP::getKode('mtypetranc',static::$main['idtypetranc2']);
        $params = [
            'tablemaster' => static::$main['table2'],
            'generatecode' => static::$main['generatecode2'],
            'idtypecostcontrol' => $idtypecostcontrol
        ];
        if ($params['idtypecostcontrol'] == 0) {
            $params['kodetypecostcontrol'] = 'NA';
        } else if ($params['idtypecostcontrol'] == 1) {
            $params['kodetypecostcontrol'] = 'PRO';
        } else if ($params['idtypecostcontrol'] == 2) {
            $params['kodetypecostcontrol'] = 'TR';
        } else if ($params['idtypecostcontrol'] == 3) {
            $params['kodetypecostcontrol'] = 'PS';
        }
        $gcpo = RAP::generateCode($params);
        $typetranc = $find['purchase']->kodetypetranc;
        $idreffd = '';
        if ($find['purchasedetail']) {
            foreach ($find['purchasedetail'] as $k => $v) {
                $idreffd .= $v->noid;
            }
        }

        if ($find['purchase']->idstatustranc == 5037) {
            $sendtopo = 'OTHERS (Send to '.static::$main['table2-title'].')';
        } else {
            $sendtopo = 'SET APPROVED (Approved to '.static::$main['table2-title'].')';
        }

        $datalog = [
            [
                'idtypetranc' => static::$main['idtypetranc'],
                'idtypeaction' => $find['purchase']->idstatustranc == 5037 ? 99 : 10,
                'logsubject' => 'Data '.$find['purchase']->kodetypetranc.' Kode = '.$find['purchase']->kode.' '.$sendtopo,
                'keterangan' => 'Data '.$find['purchase']->kodetypetranc.' Kode = '.$find['purchase']->kode.' '.$sendtopo.' BY '.Session::get('myusername').' ON '.date('Y-m-d H:i:s'),
                'idreff' => $find['purchase']->noid,
                'idreffd' => $idreffd,
                'iddevice' => 0,
                'devicetoken' => $devicetoken,
                'idcreate' => Session::get('noid'),
                'docreate' => date('Y-m-d H:i:s'),
            ],[
                'idtypetranc' => static::$main['idtypetranc2'],
                'idtypeaction' => 1,
                'logsubject' => 'Data '.$nextkodetypetranc.' Kode = '.$gcpo['kode'].' CREATE',
                'keterangan' => 'Data '.$nextkodetypetranc.' Kode = '.$gcpo['kode'].' CREATE BY '.Session::get('myusername').' ON '.date('Y-m-d H:i:s'),
                'idreff' => $gcpo['nourut'],
                'idreffd' => $idreffd,
                'iddevice' => 0,
                'devicetoken' => $devicetoken,
                'idcreate' => Session::get('noid'),
                'docreate' => date('Y-m-d H:i:s'),
            ]
        ];

        $params = [
            'noid' => $noid,
            'kodereff' => $kodereff,
            'nextnoid' => $nextnoid,
            'nextnoiddetail' => $nextnoiddetail,
            'datalog' => $datalog,
            'kodetypecostcontrol' => $params['kodetypecostcontrol']
        ];
        $result = MP::sendToNext($params);

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Send to '.static::$main['table2-title'].' Successfully',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Send to '.static::$main['table2-title'].' Error!',
            ]);
        }
    }

    public function snst(Request $request) {
        $noid = $request->get('noid');
        $find = MP::findData(['noid'=>$noid]);
        $idtypetranc = $find['master']->idtypetranc;
        $idstatustranc = $request->get('idstatustranc');
        $statustranc = $request->get('statustranc');
        $contractnumber = $request->get('contractnumber');

        $isdirector = MP::isRule(['mcardkode'=>Session::get('myusername'),'mpositionkode'=>'DIR']); 
        $iscreater = Session::get('noid') == $find['master']->idcreate;
        $isestimator = @Session::get('noid') == $find['master']->idcardestimator;
        $ismarketing = Session::get('noid') == $find['master']->idcardsales;
        $isprojectmanager = Session::get('noid') == $find['master']->idcardpj;

        $typeaction = Session::get(static::$main['noid'].'-conditions')[$idstatustranc]['appmtypeaction_nama'];
        $idtypeaction = Session::get(static::$main['noid'].'-conditions')[$idstatustranc]['appmtypeaction_noid'];
        
        $idreff = $find['master']->noid;
        $logsubject = 'Data '.$find['master']->kodetypetranc.' Kode = '.$find['master']->kode.' '.$typeaction;

        $data = [
            'idstatustranc' => $idstatustranc,
            'tanggal' => $find['master']->tanggal,
            'keterangan' => $find['master']->keterangan,
            'idcreate' => $find['master']->idcreate
        ];

        $datalog = [
            'idtypetranc' => $idtypetranc,
            'idtypeaction' => $idtypeaction,
            'logsubject' => $logsubject,
            'keterangan' => $request->get('keterangan'),
            'idreff' => $idreff,
            'idreffd' => 0,
            'iddevice' => 0,
            'devicetoken' => $request->get('_token'),
            'idcreate' => Session::get('noid'),
            'docreate' => date('Y-m-d H:i:s'),
        ];

        if ($idstatustranc == 40120) {            
            $rc = new Request();
            $rc->setMethod('POST');
            $rc->request->add(['noid'=>$noid]);
            $rc->request->add(['idcarddrafter'=>$find['master']->idcarddrafter]);
            $stn = $this->send_to_next($rc);
        } else if ($idstatustranc == 40140) {
            // Update SI
            $updatesi = DB::connection('mysql2')
                ->table('salesinquiry')
                ->where('noid', $noid)
                ->update([
                    'idstatustranc' => $idstatustranc,
                    'contractnumber' => $contractnumber
                ]);
            if (!$updatesi) return ['success'=>false,'message'=>'Error update SI'];

            // Sent to SO
            $masterso = (array)$find['master'];

            $kodetypecostcontrol = '';
            if ($masterso['idtypecostcontrol'] == 0) {
                $kodetypecostcontrol = 'NA';
            } else if ($masterso['idtypecostcontrol'] == 1) {
                $kodetypecostcontrol = 'PRO';
            } else if ($masterso['idtypecostcontrol'] == 2) {
                $kodetypecostcontrol = 'TR';
            } else if ($masterso['idtypecostcontrol'] == 3) {
                $kodetypecostcontrol = 'PS';
            }
            
            $nextnoidso = MP::getNextNoid('salesorder');
            $nextnoiddetailso = MP::getNextNoid('salesorderdetail');
            $nextnoidcdfso = MP::getNextNoid('dmsmdocument');
            $gcso = MP::generateCode([
                'tablemaster' => 'salesorder',
                'generatecode' => 'generatecode/402/1',
                'idtypecostcontrol' => $masterso['idtypecostcontrol'],
                'kodetypecostcontrol' => $kodetypecostcontrol,
            ]);
            $masterso['noid'] = $nextnoidso;
            $masterso['kodereff'] = $masterso['kode'];
            $masterso['kode'] = $gcso['kode'];
            $masterso['nourut'] = $gcso['nourut'];
            $masterso['idtypetranc'] = 402;
            $masterso['idstatustranc'] = 4021;
            unset($masterso['kodetypetranc']);
            unset($masterso['mtypetranctypestatus_nama']);
            unset($masterso['mtypetranctypestatus_classcolor']);
            unset($masterso['mcarduser_myusername']);
            unset($masterso['namacurrency']);
            unset($masterso['contractnumber']);
            unset($masterso['projecttype_nama']);
            unset($masterso['idtypecostcontrol_nama']);
            unset($masterso['idcardcustomer_nama']);
            unset($masterso['idcardsales_nama']);
            unset($masterso['idcardkoor_nama']);
            unset($masterso['idcardpj_nama']);
            unset($masterso['idcardsitekoor_nama']);
            unset($masterso['idcarddrafter_nama']);
            unset($masterso['idcardestimator_nama']);

            $detailso = [];
            // foreach ($find['detail'] as $k => $v) {
            //     $detailso[$k] = (array)$v;
            //     $detailso[$k]['noid'] = $nextnoiddetailso;
            //     $detailso[$k]['idmaster'] = $nextnoidso;
            //     unset($detailso[$k]['kodeinventor']);
            //     unset($detailso[$k]['namainventor2']);
            //     unset($detailso[$k]['namasatuan']);
            //     $nextnoiddetailso++;
            // }

            // $cdfso = [];
            // foreach ($find['cdf'] as $k => $v) {
            //     $cdfso[$k] = (array)$v;
            //     $cdfso[$k]['noid'] = $nextnoidcdfso;
            //     $cdfso[$k]['idtypetranc'] = static::$main['idtypetranc2'];
            //     $cdfso[$k]['idreff'] = $nextnoidso;
            //     $cdfso[$k]['idstatus'] = $v['cdf_idstatus'];
            //     $cdfso[$k]['varstatus'] = $v['cdf_varstatus'];
            //     $cdfso[$k]['idcardstatus'] = $v['cdf_idcardstatus'];
            //     $cdfso[$k]['dostatus'] = $v['cdf_dostatus'];
            //     $cdfso[$k]['kode'] = $v['cdf_kode'];
            //     $cdfso[$k]['barcode'] = $v['cdf_barcode'];
            //     $cdfso[$k]['nama'] = $v['cdf_nama'];
            //     $cdfso[$k]['keterangan'] = $v['cdf_keterangan'];
            //     $cdfso[$k]['iddmsfile'] = $v['cdf_iddmsfile'];
            //     $cdfso[$k]['ishardcopy'] = $v['cdf_ishardcopy'];
            //     $cdfso[$k]['ispublic'] = $v['cdf_ispublic'];
            //     $cdfso[$k]['isworkflow'] = $v['cdf_isworkflow'];
            //     $cdfso[$k]['filenamesystem'] = $v['cdf_filenamesystem'];
            //     $cdfso[$k]['filenameoriginal'] = $v['cdf_filenameoriginal'];
            //     $cdfso[$k]['filefolder'] = $v['cdf_filefolder'];
            //     $cdfso[$k]['iddmstype'] = $v['cdf_iddmstype'];
            //     $cdfso[$k]['iddmsfolder'] = $v['cdf_iddmsfolder'];
            //     $cdfso[$k]['iddmsfolderbase'] = $v['cdf_iddmsfolderbase'];
            //     $cdfso[$k]['iddmskategori'] = $v['cdf_iddmskategori'];
            //     $cdfso[$k]['doctag'] = $v['cdf_doctag'];
            //     $cdfso[$k]['doclabel'] = $v['cdf_doclabel'];
            //     $cdfso[$k]['docauthor'] = $v['cdf_docauthor'];
            //     $cdfso[$k]['filesize'] = $v['cdf_filesize'];
            //     $cdfso[$k]['idcardupload'] = $v['cdf_idcardupload'];
            //     $cdfso[$k]['iddmskategori'] = $v['cdf_iddmskategori'];
            //     unset($cdfso[$k]['kodeinventor']);
            //     unset($cdfso[$k]['namainventor2']);
            //     unset($cdfso[$k]['namasatuan']);
            //     $nextnoidcdfso++;
            // }

            $insertmasterso = DB::connection('mysql2')
                ->table('salesorder')
                ->insert($masterso);
            if (!$insertmasterso) return ['success'=>false,'message'=>'Error Insert Master SO'];

            $insertdetailso = DB::connection('mysql2')
                ->table('salesorderdetail')
                ->insert($detailso);
            if (!$insertdetailso) return ['success'=>false,'message'=>'Error Insert Detail SO'];
            
            return ['success'=>true,'message'=>'Success Sent to SO'];;
        
        
            // Sent to RAB
            $rab = RAP::findData(['kodereff'=>$find['master']->kode]);
            $rab['master'] = (array)$rab['master'];

            $nextnoid = RAP::getNextNoid('prjmrab');
            $nextnoidmilestone = RAP::getNextNoid('prjmrabmilestone');
            $nextnoidheader = RAP::getNextNoid('prjmrabheader');
            $nextnoiddetail = RAP::getNextNoid('prjmrabdetail');

            $gc = RAP::generateCode([
                'tablemaster' => 'prjmrab',
                'generatecode' => 'generatecode/604/1',
                'idcostcontrol' => $rab['master']['noid'],
                'kodecostcontrol' => $rab['master']['kode'],
                'data' => $rab['master']
            ]);

            $paramsmilestone = [];
            $paramsheader = [];
            $nourutmilestone = 1;
            foreach ($rab['milestone'] as $k => $v) {
                $v = (array)$v;
                $paramsmilestone[$k] = [
                    'noid' => $nextnoidmilestone,
                    'idmaster' => $nextnoid,
                    'idinventor' => $v['idinventor'],
                    'nama' => $v['nama'],
                    'tanggalstart' => MyHelper::humandateTodbdate($v['tanggalstart']),
                    'tanggalstop' => MyHelper::humandateTodbdate($v['tanggalstop']),
                    'tanggalfinished' => MyHelper::humandateTodbdate($v['tanggalfinished']),
                    'keterangan' => $v['keterangan'],
                    'nourut' => $nourutmilestone,
                    'idcardpj' => $v['idcardpj'],
                    'idcardpjwakil' => $v['idcardpj'],
                    'totalqty' => $v['totalqty'],
                    'totalitem' => $v['totalitem'],
                    'totalsubtotal' => $v['totalsubtotal'],
                    'idcreate' => Session::get('noid'),
                    'docreate' => date('Y-m-d H:i:s'),
                ];

                $nourutheader = 1;
                foreach ($v['header'] as $k2 => $v2) {
                    $v2 = (array)$v2;
                    $paramsheader[] = [
                        'noid' => $nextnoidheader,
                        'idmaster' => $nextnoid,
                        'idmilestone' => $nextnoidmilestone,
                        'nama' => $v2['nama'],
                        'tanggalstart' => MyHelper::humandateTodbdate($v2['tanggalstart']),
                        'tanggalstop' => MyHelper::humandateTodbdate($v2['tanggalstop']),
                        'tanggalfinished' => MyHelper::humandateTodbdate($v2['tanggalfinished']),
                        'keterangan' => $v2['keterangan'],
                        'nourut' => $nourutheader,
                        'idcardpj' => $v2['idcardpj'],
                        'idcardpjwakil' => $v2['idcardpjwakil'],
                        'totalqty' => $v2['totalqty'],
                        'totalitem' => $v2['totalitem'],
                        'totalsubtotal' => $v2['totalsubtotal'],
                        'generaltotal' => $v2['generaltotal'],
                        'idcreate' => Session::get('noid'),
                        'docreate' => date('Y-m-d H:i:s'),
                    ];

                    $nourutdetail = 1;
                    foreach ($v2['detail'] as $k3 => $v3) {
                        $v3 = (array)$v3;
                        $paramsdetail[] = [
                            'noid' => $nextnoiddetail,
                            'idmaster' => $nextnoid,
                            'idheader' => $nextnoidheader,
                            'idinventor' => $v3['idinventor'],
                            'namainventor' => $v3['namainventor'],
                            'keterangan' => $v3['keterangan'],
                            'idgudang' => $v3['idgudang'],
                            'idcompany' => $v3['idcompany'],
                            'iddepartment' => $v3['iddepartment'],
                            'idsatuan' => $v3['idsatuan'],
                            'konvsatuan' => $v3['konvsatuan'],
                            'unitqty' => $v3['unitqty'],
                            'unitprice' => $v3['unitprice'],
                            'subtotal' => $v3['subtotal'],
                            'discountvar' => $v3['discountvar'],
                            'discount' => $v3['discount'],
                            'nilaidisc' => $v3['nilaidisc'],
                            'idtypetax' => $v3['idtypetax'],
                            'idtax' => $v3['idtax'],
                            'prosentax' => $v3['prosentax'],
                            'nilaitax' => $v3['nilaitax'],
                            'hargatotal' => $v3['hargatotal'],
                            'idcreate' => Session::get('noid'),
                            'docreate' => date('Y-m-d H:i:s'),
                        ];

                        $nextnoiddetail++;
                        $nourutdetail++;
                    }

                    $nextnoidheader++;
                    $nourutheader++;
                }

                $nextnoidmilestone++;
                $nourutmilestone++;
            }

            $nextnoidcdf = RAP::getNextNoid('prjmrabdocument');
            $paramscdf = [];
            foreach ($rab['cdf'] as $k => $v) {
                $v = (array)$v;
                $paramscdf[] = [
                    'noid' => $nextnoidcdf,
                    'idrab' => $nextnoid,
                    'idstatusfile' => $v['cdf_idstatusfile'],
                    'idcardstatus' => $v['cdf_idcardstatus'],
                    'dostatus' => $v['cdf_dostatus'],
                    'kode' => $v['cdf_kode'],
                    'nama' => $v['cdf_nama'],
                    'namaalias' => $v['cdf_namaalias'],
                    'keterangan' => $v['cdf_keterangan'],
                    'iddmsfile' => $v['cdf_iddmsfile'],
                    'idcreate' => Session::get('noid'),
                    'docreate' => date('Y-m-d H:i:s'),
                ];

                $nextnoidcdf++;
            }

            $paramsrab = [
                'master' => [
                    'noid' => $nextnoid,
                    'idstatustranc' => 6041,
                    'idstatuscard' => $rab['master']['idstatuscard'],
                    'dostatus' => $rab['master']['dostatus'],
                    'idtypetranc' => 604,
                    'idtypecostcontrol' => $rab['master']['idtypecostcontrol'],
                    'idcostcontrol' => null,
                    'kode' => $gc['kode'],
                    'kodereff' => $masterso['kode'],
                    'tanggal' => $rab['master']['tanggal'],
                    'tanggaltax' => $rab['master']['tanggaltax'],
                    'tanggaldue' => $rab['master']['tanggaldue'],
                    'projectname' => $rab['master']['projectname'],
                    'projectlocation' => $rab['master']['projectlocation'],
                    'projectvalue' => $rab['master']['projectvalue'],
                    'projecttype' => $rab['master']['projecttype'],
                    'idcardsales' => $rab['master']['idcardsales'],
                    'idcardcashier' => null,
                    'idcardcustomer' => $rab['master']['idcardcustomer'],
                    'idcardpj' => $rab['master']['idcardpj'],
                    'idcardkoor' => $rab['master']['idcardkoor'],
                    'idcardsitekoor' => $rab['master']['idcardsitekoor'],
                    'idcarddrafter' => $rab['master']['idcarddrafter'],
                    'idcardestimator' => $rab['master']['idcardestimator'],
                    'idakuncustomer' => null,
                    'totalqty' => 0,
                    'totalitem' => 0,
                    'totalsubtotal' => 0,
                    'totaldisc' => 0,
                    'totaltax' => 0,
                    'discnota' => 0,
                    'totaldiscnota' => 0,
                    'generalprice' => 0,
                    'biayalain' => 0,
                    'kodevoucher' => null,
                    'generaltotal' => 0,
                    'totalbayar' => 0,
                    'totaldeposit' => 0,
                    'totalhutang' => 0,
                    'totalsisahutang' => 0,
                    'idtypepayment' => 1,
                    'paycardbank' => null,
                    'paycardname' => null,
                    'paycardnumber' => null,
                    'paycardexpired' => null,
                    'idcashbankbayar' => 0,
                    'idakunbayar' => 0,
                    'idakundeposit' => 0,
                    'idakunbiayalain' => 0,
                    'idakunhutang' => 0,
                    'idakunsales' => 0,
                    'idakunpersediaan' => 0,
                    'idakundiscount' => 0,
                    'keterangan' => $rab['master']['keterangan'],
                    'idcurrency' => $rab['master']['idcurrency'],
                    'konvcurr' => 0,
                    'idgudang' => $rab['master']['idgudang'],
                    'nourut' => $gc['nourut'],
                    'idcompany' => null,
                    'iddepartment' => null,
                    'idtypetrancprev' => null,
                    'idtrancprev' => null,
                    'idtypetrancorder' => null,
                    'idtrancorder' => null,
                    // 'idsalesinquiry' => $rab['master']['noid'],
                    'idcreate' => Session::get('noid'),
                    'docreate' => date('Y-m-d H:i:s'),
                    'idupdate' => 0,
                    'lastupdate' => null,
                ],
                'milestone' => $paramsmilestone,
                'header' => $paramsheader,
                'detail' => @$paramsdetail,
                'cdf' => $paramscdf,
            ];
            
            // Insert RAB
            $insertrab = RAB::saveData($paramsrab);
            if (!$insertrab) return ['success' => false, 'message' => 'Error Insert RAB'];
            return ['success' => true, 'message' => 'Success Insert RAB'];
        }
        
        if ($idstatustranc == 4012) {
            $completecheck = MP::completeCheck([
                'noid' => $noid,
                'idtypecostcontrol' => $find['master']->idtypecostcontrol,
            ]);
            
            if (!$completecheck['success']) {
                return $completecheck;
            }
        }

        $result = MP::snst([
            'noid' => $noid,
            'kode' => $find['master']->kode,
            'data' => $data,
            'datalog' => $datalog,
            'keterangannotif' => 'Data '.$find['master']->kodetypetranc.' Kode = '.$find['master']->kode,
            'isdirector' => $isdirector,
            'iscreater' => $iscreater,
            'isestimator' => $isestimator,
            'ismarketing' => $ismarketing,
            'isprojectmanager' => $isprojectmanager,
            'estimator' => is_null($find['master']->idcardestimator) ? [] : [$find['master']->idcardestimator],
            'marketing' => is_null($find['master']->idcardsales) ? [] : [$find['master']->idcardsales],
            'projectmanager' => is_null($find['master']->idcardpj) ? [] : [$find['master']->idcardpj],
        ]);
        

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Set '.$statustranc.' Successfully',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Set '.$statustranc.' Error!',
            ]);
        }
    }

    public function div(Request $request) {
        $devicetoken = $request->get('_token');
        $noid = $request->get('noid');
        $find = MP::findData(['noid'=>$noid]);
        $idtypetranc = $request->get('idtypetranc');
        $typetranc = $find['purchase']->kodetypetranc;
        $idstatustranc = $request->get('idstatustranc');
        $statustranc = $request->get('statustranc');
        $keterangan = $request->get('keterangan');
        $idreff = $find['purchase']->noid;
        $idreffd = '';
        // if ($find['purchasedetail']) {
        //     foreach ($find['purchasedetail'] as $k => $v) {
        //         $idreffd .= $v->noid;
        //     }
        // }
        if ($idstatustranc == 1) {
            $idtypeaction = 7;
            $typeaction = 'SET DRAFT';
        } else if ($idstatustranc == 2) {
            $idtypeaction = 8;
            $typeaction = 'SET NEED APPROVED';
        } else if ($idstatustranc == 3) {
            $idtypeaction = 9;
            $typeaction = 'SET REVISION';
        }
        $logsubject = 'Data '.$typetranc.' Kode = '.$find['purchase']->kode.' '.$typeaction;

        $data = [
            'idstatustranc' => $idstatustranc
        ];

        $datalog = [
            'idtypetranc' => $idtypetranc,
            'idtypeaction' => $idtypeaction,
            'logsubject' => $logsubject,
            'keterangan' => $keterangan,
            'idreff' => $idreff,
            'idreffd' => $idreffd,
            'iddevice' => 0,
            'devicetoken' => $devicetoken,
            'idcreate' => Session::get('noid'),
            'docreate' => date('Y-m-d H:i:s'),
        ];

        $result = MP::div($data, $noid, $datalog);

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Set '.$statustranc.' Successfully',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Set '.$statustranc.' Error!',
            ]);
        }
    }

    public function sapcf(Request $request) {
        $devicetoken = $request->get('_token');
        $noid = $request->get('noid');
        $find = MP::findData(['noid'=>$noid]);
        $idtypetranc = $request->get('idtypetranc');
        $typetranc = $find['master']->kodetypetranc;
        $idstatustranc = $request->get('idstatustranc');
        $statustranc = $request->get('statustranc');
        $keterangan = $request->get('keterangan');
        $idreff = $find['master']->noid;
        $idreffd = '';
        // if ($find['purchasedetail']) {
        //     foreach ($find['purchasedetail'] as $k => $v) {
        //         $idreffd .= $v->noid;
        //     }
        // }
        $idtypeaction = Session::get(static::$main['noid'].'-conditions')[$idstatustranc]['appmtypeaction_noid'];
        $typeaction = Session::get(static::$main['noid'].'-conditions')[$idstatustranc]['appmtypeaction_nama'];
        $logsubject = 'Data '.$typetranc.' Kode = '.$find['master']->kode.' '.$typeaction;

        $data = [
            'idstatustranc' => $idstatustranc
        ];

        $datalog = [
            'idtypetranc' => $idtypetranc,
            'idtypeaction' => $idtypeaction,
            'logsubject' => $logsubject,
            'keterangan' => $keterangan,
            'idreff' => $idreff,
            'idreffd' => $idreffd,
            'iddevice' => 0,
            'devicetoken' => $devicetoken,
            'idcreate' => Session::get('noid'),
            'docreate' => date('Y-m-d H:i:s'),
        ];

        $result = MP::sapcf($data, $noid, $datalog);

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Set '.$statustranc.' Successfully',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Set '.$statustranc.' Error!',
            ]);
        }
    }

    public function set_pending(Request $request) {
        $noid = $request->get('noid');

        $data = [
            'idstatustranc' => 5
        ];

        $datalog = [
            'idtypetranc' => $request->get('idtypetranc'),
            'idtypeaction' => 9,
            'logsubject' => 'PURCHASE-REQUEST/PENDING/'.$noid,
            'keterangan' => $request->get('keterangan'),
            'idreff' => 0,
            'idreffd' => 0,
            'iddevice' => 0,
            'devicetoken' => $request->get('_token'),
            'idcreate' => Session::get('noid'),
            'docreate' => date('Y-m-d H:i:s'),
        ];

        $result = MP::setPending($data, $noid, $datalog);

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Set Pending Successfully',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Set Pending Error!',
            ]);
        }
    }

    public function set_canceled(Request $request) {
        $noid = $request->get('noid');

        $data = [
            'idstatustranc' => 6
        ];

        $datalog = [
            'idtypetranc' => $request->get('idtypetranc'),
            'idtypeaction' => 9,
            'logsubject' => 'PURCHASE-REQUEST/CANCELED/'.$noid,
            'keterangan' => $request->get('keterangan'),
            'idreff' => 0,
            'idreffd' => 0,
            'iddevice' => 0,
            'devicetoken' => $request->get('_token'),
            'idcreate' => Session::get('noid'),
            'docreate' => date('Y-m-d H:i:s'),
        ];

        $result = MP::setCanceled($data, $noid, $datalog);

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Set Canceled Successfully',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Set Canceled Error!',
            ]);
        }
    }

    public function delete_select_m(Request $request) {
        $selecteds = $request->get('selecteds');
        $selectcheckboxmall = $request->get('selectcheckboxmall');

        if ($selectcheckboxmall) {
            $datalog = [
                'idtypetranc' => static::$main['idtypetranc'],
                'idtypeaction' => 3,
                'logsubject' => 'PURCHASE-REQUEST/DELETEALL/All',
                'keterangan' => 'Data All Deleted',
                'idreff' => 0,
                'idreffd' => 0,
                'iddevice' => 0,
                'devicetoken' => $request->get('_token'),
                'idcreate' => Session::get('noid'),
                'docreate' => date('Y-m-d H:i:s'),
            ];
        } else {
            $datalog = [];
            foreach ($selecteds as $k => $v) {
                $datalog[$v] = [
                    'idtypetranc' => static::$main['idtypetranc'],
                    'idtypeaction' => 3,
                    'logsubject' => 'PURCHASE-REQUEST/DELETE/'.$v,
                    'keterangan' => 'Data Deleted',
                    'idreff' => 0,
                    'idreffd' => 0,
                    'iddevice' => 0,
                    'devicetoken' => $request->get('_token'),
                    'idcreate' => Session::get('noid'),
                    'docreate' => date('Y-m-d H:i:s'),
                ];
            }
        }

        $result = MP::deleteSelectM($selecteds, $selectcheckboxmall, $datalog);

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Delete Successfully',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Delete Error!',
            ]);
        }
    }

    public function delete(Request $request) {
        $noid = $request->get('noid');
        $find = MP::findData(['noid'=>$noid]);
        $idreffd = '';

        // foreach ($find['detail'] as $k => $v) {
        //     $idreffd .= $v->noid.',';
        // }

        $log = [
            'idtypetranc' => $find['master']->idtypetranc,
            'idtypeaction' => 3,
            'logsubject' => 'Data '.$find['master']->kodetypetranc.' Kode = '.$find['master']->kode.' DELETE',
            'keterangan' => 'Data '.$find['master']->kodetypetranc.' Kode = '.$find['master']->kode.' DELETE BY '.Session::get('nama').' ON '.date('Y-m-d H:i:s'),
            'idreff' => $find['master']->noid,
            'idreffd' => $idreffd,
            'iddevice' => 0,
            'devicetoken' => $request->get('_token'),
            'idcreate' => Session::get('noid'),
            'docreate' => date('Y-m-d H:i:s'),
        ];

        $result = MP::deleteData(['noid' => $noid, 'log' => $log]);

        return response()->json(['success' => $result['success'], 'message' => $result['message']]);
    }

    public function cdf_get(Request $request) {
        $whereformat = $request->get('whereformat');
        $wheresearch = $request->get('wheresearch');
        
        $querywhere = 'WHERE 1=1 ';
        if (@$whereformat) {
            if ($whereformat == 'img') {
                $querywhere .= "AND (dmsmtypefile.fileextention = 'png'
                                    OR dmsmtypefile.fileextention = 'jpg'
                                    OR dmsmtypefile.fileextention = 'jpeg'
                                    OR dmsmtypefile.fileextention = 'gif') ";
            } else {
                $querywhere .= "AND dmsmtypefile.fileextention = '$whereformat'";
            }
        }
        if (@$wheresearch) {
            $querywhere .= "AND (dmsmfile.nama LIKE '%$wheresearch%'
                                OR dmsmfile.filename LIKE '%$wheresearch%')";
        }

        $query = "SELECT 
                        dmsmfile.*, 
                        dmsmtypefile.fileextention AS fileextention, 
                        dmsmtypefile.classicon AS classicon, 
                        dmsmtypefile.classcolor AS classcolor,
                        dmsmtypefile.fileimage AS fileimage
                    FROM dmsmfile
                    JOIN dmsmtypefile ON dmsmfile.idtypefile = dmsmtypefile.noid
                    $querywhere
                    ORDER BY dmsmfile.docreate DESC";
        $result = DB::connection('mysql2')->select($query);
    
        $data = [];
        foreach ($result as $k => $v) {
            $data[] = $v;
        }

        return response()->json($data);
    }

    function getStringBetween($str,$from,$to) {
        $string = substr($str, strpos($str, $from) + strlen($from));
        if (strstr($string,$to,TRUE) != FALSE) {
            $string = strstr($string,$to,TRUE);
        } else {
            $string = $str;
        }
        return $string;
    }

    public function cdf_upload(Request $request) {
        $validator = Validator::make($request->all(), [
            'file' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $request->get('file')
            ]);
        }

        
        $file = $request->file('file');
        $filename = $request->get('filename');
        $filenameori = $file->getClientOriginalName();
        $filenameedit = explode('.',$filename ? $filename : $filenameori);
        $filenameonly = $filenameedit[0];
        $filecheckexist = DB::connection('mysql2')->table('dmsmfile')->select('noid','filename')->where('filename',$filename)->orderBy('noid', 'desc')->first();
        
        if ($filecheckexist) {
            $filecheckdup = DB::connection('mysql2')->table('dmsmfile')->select('noid','filename')->where('filename','like',"$filenameonly%")->orderBy('noid', 'desc')->first();

            $getdup = $this->getStringBetween($filecheckdup->filename,'(',')');
            if ($getdup == $filecheckdup->filename) {
                $filenameonlyfix = $filenameonly.' (2)';
                $filenamefix = $filenameonlyfix.'.'.$filenameedit[1];
            } else {
                $filenameonlyfix = $filenameonly.' ('.((int)$getdup+1).')';
                $filenamefix = $filenameonlyfix.'.'.$filenameedit[1];
            }
        } else {
            $filenameonlyfix = $filenameonly;
            $filenamefix = $filename;
        }

        $filesize = $file->getSize();
        $explodefe = explode('.', $filenamefix);
        $fileextension = end($explodefe);

        $resultlastnoid = DB::connection('mysql2')->table('dmsmfile')->select('noid')->orderBy('noid', 'desc')->first();
        $lastnoid = ($resultlastnoid != null) ? $resultlastnoid->noid+1 : 1; 
        
        $filelocation = 'filemanager/original';
        $filepreview = 'filemanager/preview';
        $filethumbnail = 'filemanager/thumb';

        if ($fileextension == 'png' ||
            $fileextension == 'jpg' ||
            $fileextension == 'jpeg' ||
            $fileextension == 'gif') {
            //SAVE TO ORIGINAL
            $imageresizeoriginal = Image::make($file->path());
            $imageresizeoriginal->save($filelocation.'/'.$filenamefix);
            //SAVE TO PREVIEW
            $imageresizepreview = Image::make($file->path());
            $imageresizepreview->resize(900, 300, function ($constraint) {
                $constraint->aspectRatio();
            })->save($filepreview.'/'.$filenamefix);
            //SAVE TO THUMBNAIL
            $imageresizethumb = Image::make($file->path());
            $imageresizethumb->resize(300, 300, function ($constraint) {
                $constraint->aspectRatio();
            })->save($filethumbnail.'/'.$filenamefix);
        } else if ($fileextension == 'ai' ||
                    $fileextension == 'avi' ||
                    $fileextension == 'bin' ||
                    $fileextension == 'bmp' ||
                    $fileextension == 'cdr' ||
                    $fileextension == 'css' ||
                    $fileextension == 'csv' ||
                    $fileextension == 'dat' ||
                    $fileextension == 'dll' ||
                    $fileextension == 'doc' ||
                    $fileextension == 'docx' ||
                    $fileextension == 'dwg' ||
                    $fileextension == 'eml' ||
                    $fileextension == 'eps' ||
                    $fileextension == 'exe' ||
                    $fileextension == 'fla' ||
                    $fileextension == 'flv' ||
                    $fileextension == 'html' ||
                    $fileextension == 'html' ||
                    $fileextension == 'iso' ||
                    $fileextension == 'jar' ||
                    $fileextension == 'm4p' ||
                    $fileextension == 'm4v' ||
                    $fileextension == 'mdb' ||
                    $fileextension == 'midi' ||
                    $fileextension == 'mov' ||
                    $fileextension == 'mp3' ||
                    $fileextension == 'mp4' ||
                    $fileextension == 'mpeg' ||
                    $fileextension == 'mpg' ||
                    $fileextension == 'ogg' ||
                    $fileextension == 'pdf' ||
                    $fileextension == 'php' ||
                    $fileextension == 'ppt' ||
                    $fileextension == 'pptx' ||
                    $fileextension == 'ps' ||
                    $fileextension == 'psd' ||
                    $fileextension == 'pub' ||
                    $fileextension == 'rar' ||
                    $fileextension == 'sql' ||
                    $fileextension == 'swf' ||
                    $fileextension == 'tiff' ||
                    $fileextension == 'txt' ||
                    $fileextension == 'url' ||
                    $fileextension == 'wav' ||
                    $fileextension == 'wma' ||
                    $fileextension == 'wmv' ||
                    $fileextension == 'xls' ||
                    $fileextension == 'xlsx' ||
                    $fileextension == 'xml' ||
                    $fileextension == 'zip' ||
                    $fileextension == 'zipx') {
            $request->file('file')->move(public_path().'/filemanager/original/',$filenamefix);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Format not available!'
            ]);
        }

        $ispublic = $request->get('ispublic');
        $resulttypefile = DB::connection('mysql2')->table('dmsmtypefile')->select('noid')->where('fileextention', $fileextension)->first();
        $idtypefile = ($resulttypefile != null) ? $resulttypefile->noid : 9;

        $data = [
            'noid' => $lastnoid,
            'nama' => $filenameonlyfix,
            'filename' => $filenamefix,
            'filelocation' => $filelocation,
            'filepreview' => $filepreview,
            'filethumbnail' => $filethumbnail,
            'filesize' => $filesize,
            'idtypefile' => $idtypefile,
            'ispublic' => $ispublic,
            'docreate' => date('Y-m-d H:i:s'),
        ];

        $insert = DB::connection('mysql2')->table('dmsmfile')->insert($data);

        return response()->json([
            'success' => true,
            'message' => 'Upload '.strtoupper($fileextension).' successfully',
            'data' => json_encode($request->file('file'))
        ]);
    }

    public function cdf_edit(Request $request) {
        $filetag = '';
        if ($request->get('filetag') != '') {
            foreach ($request->get('filetag') as $k => $v) {
                $filetag .= count($request->get('filetag'))-1 == $k ? $v : $v.',';
            }
        }

        $result = DB::connection('mysql2')
                    ->table('dmsmfile')
                    ->where('noid', $request->get('noid'))
                    ->update([
                        'nama' => $request->get('filename'),
                        'ispublic' => $request->get('filestatus'),
                        'filetag' => $filetag
                    ]);
        
        return response()->json([
            'success' => $result ? true : false,
            'message' => $result
                ? 'Update '.$request->get('filename').' successfully'
                : 'Update '.$request->get('filename').' error!',
            'data' => json_encode($request->file('file'))
        ]);
    }

    public function treeview_rap(Request $request) {
        return response()->json(is_null($request->data) ? json_encode([]) : $request->data);
    }

    public function treeview_ws(Request $request) {
        return response()->json($request->data);
    }

    public function get_subgroup(Request $request) {
        $subgroups = MP::getSubgroup(['idgroup' => $request->idgroup]);
        if (!$subgroups) {
            return response()->json([
                'success' => false,
                'message' => 'Error Get Subgroup'
            ]);
        } else {
            return response()->json([
                'success' => true,
                'message' => 'Success Get Subgroup',
                'data' => $subgroups
            ]);
        }
    }

    public function print_rap($urlcode, $urlprint) {
        $url = explode('/', base64_decode($urlprint));
        $name = 'RAPBOM_'.$url[1];


        $noid = $url[0];

        $master = DB::connection('mysql2')
            ->table('salesinquiry as si')
            ->select([
                'si.noid',
                'mccustomer.nama AS idcardcustomer_name',
                'si.projectname',
                'si.tanggal',
            ])
            ->leftJoin('mcard as mccustomer', 'mccustomer.noid', '=', 'si.idcardcustomer')
            ->where('si.noid', $noid)
            ->first();
        if (!$master) return ['success'=>false,'message'=>'Error get Master'];
        // $this->master = $master;

        $groups = DB::connection('mysql2')
            ->table('salesinquirygroup AS sig')
            ->select([
                'sig.noid AS sig_noid',
                'sig.idsalesinquiry AS sig_idsalesinquiry',
                'sig.idinventory AS sig_idinventory',
                'img.nama AS sig_name',

                'sis.noid AS sis_noid',
                'sis.idsalesinquirygroup AS sis_idsalesinquirygroup',
                'sis.idinventory AS sis_idinventory',
                'imk.nama AS sis_name',

                'sii.noid AS sii_noid',
                'sii.idsalesinquirysubgroup AS sii_idsalesinquirysubgroup',
                'sii.idinventory AS sii_idinventory',
                'imi.nama AS sii_name',
                'sii.alias AS sii_alias',
                'ims.nama AS idunit_name',
                'sii.quantity AS sii_quantity',
                'sii.buyprice AS sii_buyprice',
                'sii.baseprice AS sii_baseprice',
                'sii.offerprice AS sii_offerprice',
            ])
            ->leftJoin('salesinquirysubgroup AS sis', 'sis.idsalesinquirygroup', '=', 'sig.noid')
            ->leftJoin('salesinquiryinventory AS sii', 'sii.idsalesinquirysubgroup', '=', 'sis.noid')
            ->leftJoin('invmgroup AS img', 'img.noid', '=', 'sig.idinventory')
            ->leftJoin('invmkategori AS imk', 'imk.noid', '=', 'sis.idinventory')
            ->leftJoin('invminventory AS imi', 'imi.noid', '=', 'sii.idinventory')
            ->leftJoin('invmsatuan AS ims', 'ims.noid', '=', 'sii.idunit')
            ->where('sig.idsalesinquiry', $noid)
            ->orderBy('sig.noid', 'ASC')
            ->orderBy('sis.noid', 'ASC')
            ->orderBy('sii.noid', 'ASC')
            ->get();
        if (!$groups) return ['success'=>false,'message'=>'Error get Groups'];
        // $this->groups = $groups;
        if (count($groups) == 0) {
            return 'RAP belum diisi!';
        }

        $resultactualinventory = DB::connection('mysql2')
            ->table('salesinquiry AS si')
            ->select([
                // 'so.*',
                // 'pord.*',
                'pord.idinventor',
                'pord.namainventor',
                'pord.unitqty',
                'pord.unitprice',
                'imi.idinvmgroup AS idgroup',
                'imi.idinvmkategori AS idsubgroup',

                'img.noid AS sig_noid',
                'si.noid AS sig_idsalesinquiry',
                'img.noid AS sig_idinventory',
                'img.nama AS sig_name',

                'imk.noid AS sis_noid',
                'img.noid AS sis_idsalesinquirygroup',
                'imk.noid AS sis_idinventory',
                'imk.nama AS sis_name',

                'pord.noid AS sii_noid',
                'imk.noid AS sii_idsalesinquirysubgroup',
                'pord.idinventor AS sii_idinventory',
                'pord.namainventor AS sii_name',
                'pord.namainventor AS sii_alias',
                'ims.nama AS idunit_name',
                'pord.unitqty AS sii_quantity',
                'pord.unitprice AS sii_buyprice',
                'pord.unitprice AS sii_baseprice',
                'pord.unitprice AS sii_offerprice',
            ])
            ->join('salesorder AS so', 'so.kodereff', '=', 'si.kode')
            ->join('purchaserequest AS prq', 'prq.idcostcontrol', '=', 'so.noid')
            ->join('purchaseoffer AS pof', 'pof.kodereff', '=', 'prq.kode')
            ->join('purchaseorder AS por', 'por.kodereff', '=', 'pof.kode')
            ->join('purchaseorderdetail AS pord', 'pord.idmaster', '=', 'por.noid')
            ->join('invmsatuan AS ims', 'ims.noid', '=', 'pord.idsatuan')
            ->join('invminventory AS imi', 'imi.noid', '=', 'pord.idinventor')
            ->join('invmgroup AS img', 'img.noid', '=', 'pord.idgroupinv')
            // ->join('invminventory AS imisig', 'imisig.idinvmgroup', '=', 'img.noid')
            ->join('invmkategori AS imk', 'imk.noid', '=', 'pord.idsubgroupinv')
            // ->join('invminventory AS imisis', 'imisis.idinvmkategori', '=', 'imk.noid')
            ->where('si.noid', $noid)
            ->groupBy('pord.idinventor')
            ->groupBy('pord.unitqty')
            ->groupBy('pord.unitprice')
            ->get();
        if (!$resultactualinventory) return ['success'=>false,'message'=>'Error get Actual Inventory'];

        

        $inventorypergroup = [];
        foreach ($groups as $k => $v) {
            $v->isplan = true;
            $inventorypergroup[$v->sig_idinventory][$v->sis_idinventory][$v->sii_idinventory] = $v;
        }
        // dd($inventorypergroup);

        $actualinventory = [];
        foreach ($resultactualinventory as $k => $v) {
            $actualinventory[$v->idinventor][] = $v;


            if (
                array_key_exists($v->sig_idinventory, $inventorypergroup) &&
                array_key_exists($v->sis_idinventory, $inventorypergroup[$v->sig_idinventory]) &&
                array_key_exists($v->sii_idinventory, $inventorypergroup[$v->sig_idinventory][$v->sis_idinventory])
            ) {
            } else {
                $v->isplan = false;
                $inventorypergroup[$v->sig_idinventory][$v->sis_idinventory][$v->sii_idinventory] = $v;
            }
        }
        // dd($resultactualinventory);

        $allgroups = [];
        $idinventoryallgroups = [];
        $othergroups = [];
        $idinventoryothergroups = [];
        foreach ($inventorypergroup as $k => $v) {
            foreach ($v as $k2 => $v2) {
                $i = 0;
                foreach ($v2 as $k3 => $v3) {
                    if ($i == 0) {
                        $sig_noid = $v3->sig_noid;
                        $sis_noid = $v3->sis_noid;
                    }
                    $v3->sig_noid = $sig_noid;
                    $v3->sis_idsalesinquirygroup = $sig_noid;
                    $v3->sis_noid = $sis_noid;
                    $v3->sii_idsalesinquirysubgroup = $sis_noid;


                    if ($v3->isplan) {
                        $idinventoryallgroups[] = $v3->sii_idinventory;
                        $allgroups[] = $v3;
                    } else {
                        $idinventoryothergroups[] = $v3->sii_idinventory;

                        $v3->sig_idinventory = 100;
                        $v3->sig_name = 'LAIN - LAIN';

                        $v3->sis_noid = 11;
                        $v3->sis_idinventory = 1000;
                        $v3->sis_name = 'LAIN - LAIN';

                        $v3->sii_idsalesinquirysubgroup = 11;
                        
                        $othergroups[] = $v3;
                    }
                    

                    $i++;
                }
            }
        }
        $fixothergroups = [];
        foreach ($othergroups as $k => $v) {
            if (!in_array($v->sii_idinventory, $idinventoryallgroups)) {
                $fixothergroups[] = $v;
            }
        }
        $allgroups = array_merge($allgroups, $fixothergroups);
        // dd($allgroups);

        // $this->inventorypergroup = $inventorypergroup;
        // $this->allgroups = $allgroups;
        // $this->actualinventory = $actualinventory;

        return (new SalesInquiryRAPExport([
            'master' => $master,
            'groups' => $groups,
            'inventorypergroup' => $inventorypergroup,
            'allgroups' => $allgroups,
            'actualinventory' => $actualinventory,
        ]))->download($name.'.xlsx');
    }

    public function print_ws($urlcode, $urlprint) {
        $url = explode('/', base64_decode($urlprint));
        $name = 'WORKSCHEDULE_'.$url[1];


        $noid = $url[0];

        $master = DB::connection('mysql2')
            ->table('salesinquiry as si')
            ->select([
                'si.noid',
                'mccustomer.nama AS idcardcustomer_name',
                'si.projectname',
                'si.tanggal',
                'si.tanggaldue',
            ])
            ->leftJoin('mcard as mccustomer', 'mccustomer.noid', '=', 'si.idcardcustomer')
            ->where('si.noid', $noid)
            ->first();
        if (!$master) return ['success'=>false,'message'=>'Error get Master'];

        $milestones = DB::connection('mysql2')
            ->table('salesinquirymilestone AS sim')
            ->select([
                'sim.noid AS sim_noid',
                'sim.idsalesinquiry AS sim_idsalesinquiry',
                'sim.name AS sim_name',
                // 'mm.name AS sim_name',

                'sih.noid AS sih_noid',
                'sih.idsalesinquiry AS sih_idsalesinquiry',
                'sih.idsalesinquirymilestone AS sih_idsalesinquirymilestone',
                'sih.name AS sih_name',
                'sih.startdate AS sih_startdate',
                'sih.stopdate AS sih_stopdate',

                'sid.noid AS sid_noid',
                'sid.idsalesinquiry AS sid_idsalesinquiry',
                'sid.idsalesinquiryheader AS sid_idsalesinquiryheader',
                'sid.name AS sid_name',
                'sid.quantity AS sid_quantity',
                'ims.nama AS sid_idunit_name',
                'sid.startdate AS sid_startdate',
                'sid.stopdate AS sid_stopdate',
                'sid.planprice AS sid_planprice',
                'sid.actualprice AS sid_actualprice',
                'sid.isshowreport AS sid_isshowreport',
            ])
            ->leftJoin('mmilestone AS mm', 'mm.noid', '=', 'sim.idmilestone')
            ->leftJoin('salesinquiryheader AS sih', 'sih.idsalesinquirymilestone', '=', 'sim.noid')
            ->leftJoin('salesinquirydetail AS sid', 'sid.idsalesinquiryheader', '=', 'sih.noid')
            ->leftJoin('invmsatuan AS ims', 'ims.noid', '=', 'sid.idunit')
            ->where('sim.idsalesinquiry', $noid)
            ->orderBy('sim.noid', 'ASC')
            ->orderBy('sih.noid', 'ASC')
            ->orderBy('sid.noid', 'ASC')
            ->get();
        if (!$milestones) return ['success'=>false,'message'=>'Error get Milestones'];
        if (count($milestones) == 0) {
            return 'Jadwal Pekerjaan belum diisi!';
        }

        $startdate = null;
        $stopdate = null;
        foreach ($milestones as $k => $v) {
            if (is_null($startdate)) {
                $startdate = $v->sid_startdate;
                $stopdate = $v->sid_stopdate;
            } else {
                if ($v->sid_startdate < $startdate) {
                    $startdate = $v->sid_startdate;
                }
                if ($v->sid_stopdate > $stopdate) {
                    $stopdate = $v->sid_stopdate;
                }
            }
        }

        return (new SalesInquiryWSExport([
            'master' => $master,
            'milestones' => $milestones,
            'startdate' => $startdate,
            'stopdate' => $stopdate,
        ]))->download($name.'.xlsx');
    }

    public function print_recap($urlcode, $urlprint) {
        $url = explode('/', base64_decode($urlprint));
        $name = 'RECAPITULATION_'.$url[1];


        $noid = $url[0];

        $master = DB::connection('mysql2')
            ->table('salesinquiry as si')
            ->select([
                'si.noid',
                'mccustomer.nama AS idcardcustomer_name',
                'si.projectname',
                'si.projectlocation',
                'si.tanggal',
                'si.tanggaldue',
            ])
            ->leftJoin('mcard as mccustomer', 'mccustomer.noid', '=', 'si.idcardcustomer')
            ->where('si.noid', $noid)
            ->first();
        if (!$master) return ['success'=>false,'message'=>'Error get Master'];
        // $this->master = $master;

        $milestones = DB::connection('mysql2')
            ->table('salesinquirymilestone AS sim')
            ->select([
                'sim.noid AS sim_noid',
                'sim.idsalesinquiry AS sim_idsalesinquiry',
                'sim.name AS sim_name',
                // 'mm.name AS sim_name',
                'sim.isshowreport AS sim_isshowreport',

                'sih.noid AS sih_noid',
                'sih.idsalesinquiry AS sih_idsalesinquiry',
                'sih.idsalesinquirymilestone AS sih_idsalesinquirymilestone',
                'sih.name AS sih_name',
                'sih.startdate AS sih_startdate',
                'sih.stopdate AS sih_stopdate',
                'sih.isshowreport AS sih_isshowreport',

                'sid.noid AS sid_noid',
                'sid.idsalesinquiry AS sid_idsalesinquiry',
                'sid.idsalesinquiryheader AS sid_idsalesinquiryheader',
                'sid.name AS sid_name',
                'sid.quantity AS sid_quantity',
                'ims.nama AS sid_idunit_name',
                'sid.startdate AS sid_startdate',
                'sid.stopdate AS sid_stopdate',
                'sid.planprice AS sid_planprice',
                'sid.actualprice AS sid_actualprice',
                'sid.isshowreport AS sid_isshowreport',
            ])
            ->leftJoin('mmilestone AS mm', 'mm.noid', '=', 'sim.idmilestone')
            ->leftJoin('salesinquiryheader AS sih', 'sih.idsalesinquirymilestone', '=', 'sim.noid')
            ->leftJoin('salesinquirydetail AS sid', 'sid.idsalesinquiryheader', '=', 'sih.noid')
            ->leftJoin('invmsatuan AS ims', 'ims.noid', '=', 'sid.idunit')
            ->where('sim.idsalesinquiry', $noid)
            ->orderBy('sim.noid', 'ASC')
            ->orderBy('sih.noid', 'ASC')
            ->orderBy('sid.noid', 'ASC')
            ->get();
        if (!$milestones) return ['success'=>false,'message'=>'Error get Milestones'];
        // $this->milestones = $milestones;
        if (count($milestones) == 0) {
            return 'Jadwal Pekerjaan belum diisi!';
        }

        return (new SalesInquiryRecapExport([
            'master' => $master,
            'milestones' => $milestones,
        ]))->download($name.'.xlsx');
    }

    public function print_ps($urlcode, $urlprint) {
        $url = explode('/', base64_decode($urlprint));
        $name = 'PURCHASINGSCHEDULE_'.$url[1];


        $noid = $url[0];

        // $master = DB::connection('mysql2')
        //     ->table('salesinquiry as si')
        //     ->select([
        //         'si.noid',
        //         'mccustomer.nama AS idcardcustomer_name',
        //         'si.projectname',
        //         'si.tanggal',
        //         'si.tanggaldue',
        //     ])
        //     ->leftJoin('mcard as mccustomer', 'mccustomer.noid', '=', 'si.idcardcustomer')
        //     ->where('si.noid', $noid)
        //     ->first();
        // if (!$master) return ['success'=>false,'message'=>'Error get Master'];
        // $this->master = $master;

        $resultdates = DB::connection('mysql2')
            ->table('salesinquirypurchaseschedule')
            ->where('idsalesinquiry', $noid)
            ->orderBy('plandate', 'ASC')
            ->get();
        if (!$resultdates) return ['success'=>false,'message'=>'Error get Job'];
        $dates = [];
        // $dates2 = [];
        foreach ($resultdates as $k => $v) {
            // dd($dates);
            // dd(count($dates) == 0);
            if (@!array_key_exists($v->actualdate, $dates[$v->plandate])) {
                $dates[$v->plandate][$v->actualdate] = '';
            }
            // $dates2[$v->plandate] = $v->actualdate;
            // if (!is_null($v->plandate) && !in_array($v->plandate, $dates)) {
                // $dates[] = $v->actualdate;
            // }
        }
        // dd($dates);
        // $this->dates = $dates;
        if (count($dates) == 0) {
            return 'Data - data tanggal di RAP mohon dilengkapi!';
        }

        $jobs = DB::connection('mysql2')
            ->table('salesinquiryjob AS sij')
            ->select([
                'sij.noid AS sij_noid',
                'sij.idsalesinquiry AS sij_idsalesinquiry',
                'sij.name AS sij_name',

                'sig.noid AS sig_noid',
                'sig.idsalesinquiry AS sig_idsalesinquiry',
                'sig.idsalesinquiryjob AS sig_idsalesinquiryjob',
                'imisig.nama AS sig_name',

                'sis.noid AS sis_noid',
                'sis.idsalesinquiry AS sis_idsalesinquiry',
                'sis.idsalesinquirygroup AS sis_idsalesinquirygroup',

                'sii.noid AS sii_noid',
                'sii.idsalesinquiry AS sii_idsalesinquiry',
                'sii.idsalesinquirysubgroup AS sii_idsalesinquirysubgroup',
                'sii.offerprice AS sii_offerprice',

                'sips.plandate AS sips_plandate',
                'sips.plantotal AS sips_plantotal',
                'sips.actualdate AS sips_actualdate',
                'sips.actualtotal AS sips_actualtotal',
            ])
            ->leftJoin('salesinquirygroup AS sig', 'sig.idsalesinquiryjob', '=', 'sij.noid')
            ->leftJoin('salesinquirysubgroup AS sis', 'sis.idsalesinquirygroup', '=', 'sig.noid')
            ->leftJoin('salesinquiryinventory AS sii', 'sii.idsalesinquirysubgroup', '=', 'sis.noid')
            ->leftJoin('salesinquirypurchaseschedule AS sips', 'sips.idsalesinquirygroup', '=', 'sig.noid')
            ->leftJoin('invminventory AS imisig', 'imisig.noid', '=', 'sig.idinventory')
            ->where('sij.idsalesinquiry', $noid)
            ->orderBy('sij.noid', 'ASC')
            ->orderBy('sig.noid', 'ASC')
            ->orderBy('sis.noid', 'ASC')
            ->orderBy('sii.noid', 'ASC')
            ->orderBy('sips.noid', 'ASC')
            ->get();
        if (!$jobs) return ['success'=>false,'message'=>'Error get Job'];
        // $this->jobs = $jobs;


        $jobs2 = DB::connection('mysql2')
            ->table('salesinquiryjob AS sij')
            ->select([
                'sij.noid AS sij_noid',
                'sij.idsalesinquiry AS sij_idsalesinquiry',
                'sij.name AS sij_name',

                'sig.noid AS sig_noid',
                'sig.prdate AS sig_prdate',
                'sig.reqpodate AS sig_reqpodate',
                'sig.reqonsitedate AS sig_reqonsitedate',
                'sig.idsalesinquiry AS sig_idsalesinquiry',
                'sig.idsalesinquiryjob AS sig_idsalesinquiryjob',
                'imisig.nama AS sig_name',

                'sips.noid AS sips_noid',
                'sips.plandate AS sips_plandate',
                'sips.plantotal AS sips_plantotal',
                'sips.actualdate AS sips_actualdate',
                'sips.actualtotal AS sips_actualtotal',
            ])
            ->leftJoin('salesinquirygroup AS sig', 'sig.idsalesinquiryjob', '=', 'sij.noid')
            ->leftJoin('salesinquirypurchaseschedule AS sips', 'sips.idsalesinquirygroup', '=', 'sig.noid')
            ->leftJoin('invminventory AS imisig', 'imisig.noid', '=', 'sig.idinventory')
            ->where('sij.idsalesinquiry', $noid)
            ->orderBy('sij.noid', 'ASC')
            ->orderBy('sig.noid', 'ASC')
            ->orderBy('sips.noid', 'ASC')
            ->groupBy('sig.noid')
            ->get();
        if (!$jobs2) return ['success'=>false,'message'=>'Error get Job'];
        // $this->jobs2 = $jobs2;


        $jobs3 = DB::connection('mysql2')
            ->table('salesinquiryjob AS sij')
            ->select([
                'sij.noid AS sij_noid',
                'sij.idsalesinquiry AS sij_idsalesinquiry',
                'sij.name AS sij_name',

                'sig.noid AS sig_noid',
                'sig.idsalesinquiry AS sig_idsalesinquiry',
                'sig.idsalesinquiryjob AS sig_idsalesinquiryjob',
                'imisig.nama AS sig_name',

                'sips.plandate AS sips_plandate',
                'sips.plantotal AS sips_plantotal',
                'sips.actualdate AS sips_actualdate',
                'sips.actualtotal AS sips_actualtotal',
            ])
            ->leftJoin('salesinquirygroup AS sig', 'sig.idsalesinquiryjob', '=', 'sij.noid')
            ->leftJoin('salesinquirypurchaseschedule AS sips', 'sips.idsalesinquirygroup', '=', 'sig.noid')
            ->leftJoin('invminventory AS imisig', 'imisig.noid', '=', 'sig.idinventory')
            ->where('sig.idsalesinquiry', $noid)
            ->orderBy('sips.plandate', 'ASC')
            ->get();
        if (!$jobs3) return ['success'=>false,'message'=>'Error get Job'];
        // $this->jobs3 = $jobs3;

        return (new SalesInquiryPSExport([
            'dates' => $dates,
            'jobs' => $jobs,
            'jobs2' => $jobs2,
            'jobs3' => $jobs3,
        ]))->download($name.'.xlsx');
    }

    public function get_select2ajax(Request $request){
        $table = $request->table;
        $idparent = @$request->idparent;
        $fieldparent = @$request->fieldparent;
        $search = $request->search;
        $page = $request->page;
        $limit = 25;
        $offset = ($page-1)*$limit;

        $where = [];
        if ($fieldparent) {
            $where[] = [$fieldparent,'=',$idparent];
        }

        $where[] = ['nama','like',"%$search%"];
        $datas = MP::getData(['table'=>$table,'select'=>['noid','kode','nama'],'where'=>$where,'isshowzero'=>is_null($search) ? true : false,'orderby'=>[['nourut','asc']],'offset'=>$offset,'limit'=>$limit,'isactive'=>true]);

        $results = [];
        foreach($datas as $k => $v){
            $results[] = array(
                'id' => $v->noid,
                'text' => $v->nama
            );
        }

        $more = count($results) == $limit ? true : false;

        $response = [
            'results' => $results,
            'pagination' => [
                'more' => $more
            ]
        ];
        return response()->json($response);
    }

    public function import_rap(Request $request) {
		// validasi
		$this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx'
		]);

		// menangkap file excel
		$file = $request->file('file');

		// membuat nama file unik
		$nama_file = rand().$file->getClientOriginalName();

		// upload ke folder file_siswa di dalam folder public
		$file->move('data/salesinquiry/importrap',$nama_file);
        
		// import data
		\Excel::import(new SalesInquiryImportRAP, public_path('/data/salesinquiry/importrap/'.$nama_file));

        @unlink(public_path('/data/salesinquiry/importrap/'.$nama_file));

        return Session::get('salesinquiry_rap');

		// notifikasi dengan session
		// Session::flash('sukses','Data Siswa Berhasil Diimport!');

		// alihkan halaman kembali
		// return redirect('/siswa');
    }
}