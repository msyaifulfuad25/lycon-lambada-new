<?php

namespace App\Http\Controllers\sales;

use App\Http\Controllers\Controller;
use App\Model\Sales\SalesInvoice as MP;
use App\Model\RA\RAP as MP2;
use App\Model\RA\RAB;
use App\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use DB;
use Image;
use Storage;

class SalesInvoiceController extends Controller {

    public $color = [
        'white' => '#FFFFFF',
        'default' => '#E1E5EC',
        'dark' => '#2F353B',
        'blue' => '#3598DC',
        'blue-madison' => '#578EBE',
        'blue-chambray' => '#2C3E50',
        'blue-ebonyclay' => '#22313F',
        'blue-hoki' => '#67809F',
        'blue-steel' => '#4B77BE',
        'blue-soft' => '#4C87B9',
        'blue-dark' => '#5E738B',
        'blue-sharp' => '#5C9BD1',
        'blue-oleo' => '#94A0B2',
        'green' => '#32C5D2',
        'green-meadow' => '#1BBC9B',
        'green-seagreen' => '#1BA39C',
        'green-torquoise' => '#36D7B7',
        'green-haze' => '#44B6AE',
        'green-jungle' => '#26C281',
        'green-soft' => '#3FABA4',
        'green-dark' => '#4DB3A2',
        'green-sharp' => '#2AB4C0',
        'green-steel' => '#29B4B6',
        'grey' => '#E5E5E5',
        'grey-steel' => '#E9EDEF',
        'grey-cararra' => '#FAFAFA',
        'grey-gallery' => '#555555',
        'grey-cascade' => '#95A5A6',
        'grey-silver' => '#BFBFBF',
        'grey-salsa' => '#ACB5C3',
        'grey-salt' => '#BFCAD1',
        'grey-mint' => '#525E64',
        'red' => '#E7505A',
        'red-pink' => '#E08283',
        'red-sunglo' => '#E26A6A',
        'red-intense' => '#E35B5A',
        'red-thunderbird' => '#D91E18',
        'red-flamingo' => '#EF4836',
        'red-soft' => '#D05454',
        'red-haze' => '#F36A5A',
        'red-mint' => '#E43A45',
        'yellow' => '#C49F47',
        'yellow-gold' => '#E87E04',
        'yellow-casablanca' => '#F2784B',
        'yellow-crusta' => '#F3C200',
        'yellow-lemon' => '#F7CA18',
        'yellow-saffron' => '#F4D03F',
        'yellow-soft' => '#C8D046',
        'yellow-haze' => '#C5BF66',
        'yellow-mint' => '#C5B96B',
        'purple' => '#8E44AD',
        'purple-plum' => '#8775A7',
        'purple-medium' => '#BF55EC',
        'purple-studio' => '#8E44AD',
        'purple-wisteria' => '#9B59B6',
        'purple-seance' => '#9A12B3',
        'purple-intense' => '#8775A7',
        'purple-sharp' => '#796799',
        'purple-soft' => '#8877A9',
    ];
    public static $main = [
        'table' => 'salesinvoice',
        'tabledetail' => 'salesinvoicedetail',
        'tablecdf' => 'dmsmdocument',
        'table-title' => 'Sales Invoice',
        'table2' => 'prjmrap',
        'tablemilestone2' => 'prjmrapmilestone',
        'tableheader2' => 'prjmrapheader',
        'tabledetail2' => 'prjmrapdetail',
        'tablecdf2' => 'prjmrapdocument',
        'table2-title' => 'RAP',
        'generatecode' => 'generatecode/402/1',
        'generatecode2' => 'generatecode/604/1',
        'idtypetranc' => 402,
        'idtypetranc2' => 604,
        'noid' => 40013102,
        'noidnew' => 40012101,
        'printcode' => 'print_purchase_request',
        'condition' => [
            'mttts' => [
                [],
                [
                    'mttts' => 'draft',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => true,
                        'd' => true,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => true,
                        'd' => true,
                    ],
                    'draft' => false,
                    'issued' => true,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => false,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'issued',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => true,
                    'issued' => false,
                    'verified' => true,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => false,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'verified',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => true,
                    'verified' => false,
                    'approved' => true,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => false,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'approved',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'pending',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'canceled',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'finished',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
            ],
        ]
    ];

    public static function index() {
        $idtypepage = static::$main['noid'];
        $sessionnoid = Session::get('noid');
        $sessionmyusername = Session::get('myusername');
        $sessionname = Session::get('nama');
        $sessionidcompany = Session::get('idcompany');
        $sessioniddepartment = Session::get('iddepartment');
        $sessionidgudang = Session::get('idgudang');

        $maintable = static::$main['table'];
        $breadcrumb = Menu::getBreadcrumb(static::$main['noid']);
        $pagenoid = static::$main['noid'];

        $defaulttanggal = date('d-M-Y');
        $defaulttanggaldue = date('d-M-Y', strtotime('+7 day'));
        $data = MP::getData(['table'=>static::$main['table'],'select'=>['noid','idstatustranc','idstatuscard','dostatus','idtypetranc','kode','kodereff','tanggal','tanggaltax','tanggaldue'],'isactive'=>false]);

        $columns = [
            ['data'=>'checkbox','orderable'=>false,'class'=>'text-center'],
            ['data'=>'no','orderable'=>false,'class'=>'text-right'],
            // ['data'=>'noid'],
            ['data'=>'idtypetranc'],
            ['data'=>'idstatustranc'],
            // ['data'=>'idstatuscard'],
            // ['data'=>'dostatus'],
            ['data'=>'kode'],
            ['data'=>'kodereff'],
            ['data'=>'tanggal'],
            ['data'=>'generaltotal','class'=>'text-right'],
            ['data'=>'keterangan'],
            ['data'=>'idcompany'],
            ['data'=>'iddepartment'],
            ['data'=>'idgudang'],
            ['data'=>'action','orderable'=>false],
        ];
        $columnslog = [
            ['data'=>'checkbox','orderable'=>false,'class'=>'text-center'],
            ['data'=>'no','orderable'=>false,'class'=>'text-right'],
            ['data'=>'idtypetranc'],
            ['data'=>'idtypeaction'],
            ['data'=>'logsubject'],
            ['data'=>'keterangan'],
            ['data'=>'idreff'],
            ['data'=>'idcreate'],
            ['data'=>'docreate'],
            ['data'=>'action','orderable'=>false],
        ];
        $columnscostcontrol = [
            ['data'=>'checkbox','orderable'=>false,'class'=>'text-center'],
            ['data'=>'no','orderable'=>false,'class'=>'text-right'],
            ['data'=>'idstatustranc'],
            ['data'=>'idtypetranc'],
            ['data'=>'kode'],
            ['data'=>'kodereff'],
            ['data'=>'idcompany'],
            ['data'=>'iddepartment'],
            ['data'=>'generaltotal','class'=>'text-right'],
            ['data'=>'keterangan'],
            ['data'=>'tanggal'],
            ['data'=>'action','orderable'=>false],
        ];
        $columnsdetail = [
            ['data'=>'checkbox','orderable'=>false,'class'=>'text-center'],
            ['data'=>'no','orderable'=>false,'class'=>'text-right'],
            ['data'=>'kodeinventor'],
            ['data'=>'namainventor'],
            ['data'=>'namainventor2'],
            ['data'=>'keterangan'],
            // ['data'=>'idgudang'],
            // ['data'=>'idcompany'],
            // ['data'=>'iddepartment'],
            // ['data'=>'idsatuan'],
            // ['data'=>'konvsatuan'],
            ['data'=>'unitqty','class'=>'text-right'],
            ['data'=>'namasatuan'],
            ['data'=>'unitprice','class'=>'text-right','render'=>"$.fn.dataTable.render.number( ',', '.', 3, 'Rp' )"],
            // ['data'=>'subtotal'],
            // ['data'=>'discountvar'],
            // ['data'=>'discount'],
            // ['data'=>'nilaidisc'],
            // ['data'=>'idtypetax'],
            // ['data'=>'idtax'],
            ['data'=>'hargatotal','class'=>'text-right'],
            ['data'=>'namacurrency'],
            // ['data'=>'idakunhpp'],
            // ['data'=>'idakunsales'],
            // ['data'=>'idakunpersediaan'],
            // ['data'=>'idakunsalesreturn'],
            // ['data'=>'idakunpurchasereturn'],
            // ['data'=>'serialnumber'],
            // ['data'=>'garansitanggal'],
            // ['data'=>'garansiexpired'],
            // ['data'=>'typetrancprev'],
            // ['data'=>'trancprev'],
            // ['data'=>'trancprevd'],
            // ['data'=>'typetrancorder'],
            // ['data'=>'trancorder'],
            // ['data'=>'trancorderd'],
            ['data'=>'action','orderable'=>false],
        ];

        $columndefs = [
            ['targets'=>0,'width'=>'5%'],
            ['targets'=>1,'width'=>'5%'],
            ['targets'=>2,'width'=>'5%'],
            ['targets'=>3,'width'=>'15%'],
            ['targets'=>4,'width'=>'15%'],
            ['targets'=>5,'width'=>'15%'],
            ['targets'=>6,'width'=>'15%'],
            ['targets'=>7,'width'=>'15%'],
            ['targets'=>8,'width'=>'5%'],
            ['targets'=>9,'width'=>'5%'],
            ['targets'=>10,'width'=>'5%'],
            ['targets'=>10,'width'=>'5%']
        ];

        $columndefslog = [
            ['targets'=>0,'width'=>'5%'],
            ['targets'=>1,'width'=>'5%'],
            ['targets'=>2,'width'=>'5%'],
            ['targets'=>3,'width'=>'15%'],
            ['targets'=>4,'width'=>'15%'],
            ['targets'=>5,'width'=>'15%'],
            ['targets'=>6,'width'=>'15%'],
            ['targets'=>7,'width'=>'15%'],
            ['targets'=>8,'width'=>'5%'],
            ['targets'=>9,'width'=>'5%'],
            ['targets'=>10,'width'=>'5%']
        ];

        $columndefscostcontrol = [
            ['targets'=>0,'width'=>'5%'],
            ['targets'=>1,'width'=>'5%'],
            ['targets'=>2,'width'=>'5%'],
            ['targets'=>3,'width'=>'15%'],
            ['targets'=>4,'width'=>'15%'],
            ['targets'=>5,'width'=>'15%'],
            ['targets'=>6,'width'=>'15%'],
            ['targets'=>7,'width'=>'15%'],
            ['targets'=>8,'width'=>'5%'],
            ['targets'=>9,'width'=>'5%'],
            ['targets'=>10,'width'=>'5%'],
            ['targets'=>10,'width'=>'5%']
        ];

        for ($i = 0; $i <= 12; $i++) {
            if ($i <= 8) {
                $columndefsdetail[] = ['target'=>$i];
            }
        }
        
        $columns = json_encode($columns);
        $columndefs = json_encode($columndefs);
        $columnsdetail = json_encode($columnsdetail);
        $columndefsdetail = json_encode($columndefsdetail);
        $columnslog = json_encode($columnslog);
        $columndefslog = json_encode($columndefslog);
        $columnscostcontrol = json_encode($columnscostcontrol);
        $columndefscostcontrol = json_encode($columndefscostcontrol);

        $mtypecostcontrol = MP::getData(['table'=>'mtypecostcontrol','select'=>['noid','kode','nama'],'isactive'=>true,'orderby'=>[['nama','asc']]]);
        $mcardcustomer = MP::getData(['table'=>'mcardcustomer','select'=>['noid','kode','nama'],'isactive'=>true]);
        $mcardsupplier = MP::getMCardSupplier();
        $mtt = MP::findMTypeTranc();
        $mtypetranc_noid = $mtt->noid;
        $mtypetranc_nama = $mtt->nama;
        $mtypetranc_kode = $mtt->kode;
        $mtypetranctypestatus = MP::getMTypeTrancTypeStatus(static::$main['idtypetranc']);

        $btnexternal = [];
        foreach ($mtypetranctypestatus as $k => $v) {
            if ($v->isinternal == 0 && $v-> isexternal == 1) {
                $btnexternal[] = [
                    'noid' => $v->noid,
                    'idstatusbase' => $v->idstatusbase,
                    'nama' => $v->nama,
                    'classicon' => $v->classicon,
                    'classcolor' => (new static)->color[$v->classcolor],
                ];
            }
        }

        $conditions = [];
        foreach ($mtypetranctypestatus as $k => $v) {
            if ($k != 0) {
                $updatedropdown[strtolower($v->idstatusbase)] = [
                    'noid' => $v->noid,
                    'kode' => $v->kode,
                    'nama' => $v->nama,
                    'classicon' => $v->classicon,
                    'classcolor' => (new static)->color[$v->classcolor],
                ];
            }
            $conditions[$v->noid] = [
                'noid' => $v->noid,
                'idstatusbase' => $v->idstatusbase,
                'nama' => $v->nama,
                'prev_noid' => @$mtypetranctypestatus[$k-1]->noid ? $mtypetranctypestatus[$k-1]->noid : null,
                'prev_idstatusbase' => @$mtypetranctypestatus[$k-1]->idstatusbase ? $mtypetranctypestatus[$k-1]->idstatusbase : null,
                'prev_nama' => @$mtypetranctypestatus[$k-1]->nama ? $mtypetranctypestatus[$k-1]->nama : null,
                'prev_classicon' => @$mtypetranctypestatus[$k-1]->classicon ? $mtypetranctypestatus[$k-1]->classicon : null,
                'prev_classcolor' => @$mtypetranctypestatus[$k-1]->classcolor ? (new static)->color[$mtypetranctypestatus[$k-1]->classcolor] : null,
                'next_noid' => @$mtypetranctypestatus[$k+1]->noid ? $mtypetranctypestatus[$k+1]->noid : null,
                'next_idstatusbase' => @$mtypetranctypestatus[$k+1]->idstatusbase ? $mtypetranctypestatus[$k+1]->idstatusbase : null,
                'next_nama' => @$mtypetranctypestatus[$k+1]->nama ? $mtypetranctypestatus[$k+1]->nama : null,
                'next_classicon' => @$mtypetranctypestatus[$k+1]->classicon ? $mtypetranctypestatus[$k+1]->classicon : null,
                'next_classcolor' => @$mtypetranctypestatus[$k+1]->classcolor ? (new static)->color[$mtypetranctypestatus[$k+1]->classcolor] : null,
                'next_table' => static::$main['table2-title'],
                'isinternal' => $v->isinternal == 1 ? 1 : 0,
                'isexternal' => $v->isexternal == 1 ? 1 : 0,
                'btnexternal' => $btnexternal,
                'actview' => $v->actview,
                'actedit' => $v->actedit,
                'actdelete' => $v->actdelete,
                'actreportdetail' => $v->actreportdetail,
                'actreportmaster' => $v->actreportmaster,
                'actsetprevstatus' => $v->actsetprevstatus,
                'actsetnextstatus' => $v->actsetnextstatus,
                'actsetstatus' => $v->actsetstatus,
                'appmtypeaction_noid' => $v->appmtypeaction_noid,
                'appmtypeaction_nama' => $v->appmtypeaction_nama
            ];
        }
        Session::put(static::$main['noid'].'-conditions', $conditions);
        $mttts = MP::findMTypeTrancTypeStatus(static::$main['idtypetranc']);
        $mtypetranctypestatus_noid = $mttts->noid;
        $mtypetranctypestatus_nama = $mttts->nama;
        $mtypetranctypestatus_classcolor = $mttts->classcolor;
        $mcard = MP::getData(['table'=>'mcard','select'=>['noid','kode','nama','isemployee','iscustomer','issupplier'],'isactive'=>true,'orderby'=>[['nama','asc']]]);
        $accmkodeakun = MP::getData(['table'=>'accmkodeakun','isactive'=>true]);
        $accmcashbank = MP::getData(['table'=>'accmcashbank','select'=>['noid','kode','nama'],'isactive'=>false]);
        $accmcurrency = MP::getData(['table'=>'accmcurrency','select'=>['noid','kode','nama'],'isactive'=>true]);
        $genmgudang = MP::getData(['table'=>'genmgudang','select'=>['noid','kode','namalias'],'isactive'=>true]);
        $genmcompany = MP::getData(['table'=>'genmcompany','select'=>['noid','kode','nama'],'isactive'=>true]);
        // $genmcompany = MP::getGenMCompany();
        $genmdepartment = MP::getData(['table'=>'genmdepartment','select'=>['noid','kode','nama'],'isactive'=>true]);
        $mtypetranc = MP::getData(['table'=>'mtypetranc','select'=>['noid','kode','nama'],'isactive'=>false]);
        $imi = MP::getInvMInventory();
        foreach ($imi as $k => $v) {
            $invminventory[$k] = $v;
            $invminventory[$k]->purchaseprice2 = 'RP.'.number_format($v->purchaseprice2);
        }
        $invmsatuan = MP::getData(['table'=>'invmsatuan','select'=>['noid','kode','nama','konversi'],'isactive'=>true]);
        $accmpajak = MP::getData(['table'=>'accmpajak','select'=>['noid','kode','nama','prosentax'],'isactive'=>true]);
        $color = (new static)->color;
            $lastdata = MP::getLastMonth();
            $lastdata = $lastdata->_year == null && $lastdata->_month == null ? (object)['_year'=>date('y'),'_month'=>date('m')] : $lastdata;
        $datefilter = [
            'a' => 'All',
            't' => date('M d, Y').' - '.date('M d, Y'),
            'y' => date('M d, Y', strtotime('-1 day')).' - '.date('M d, Y', strtotime('-1 day')),
            'l7d' => date('M d, Y').' - '.date('M d, Y', strtotime('-7 day')),
            'l30d' => date('M d, Y').' - '.date('M d, Y', strtotime('1 month')),
            'tm' => date('M d, Y', strtotime('first day of this month')).' - '.date('M d, Y', strtotime('last day of this month')),
            'lm' => date('M d, Y', strtotime('first day of previous month')).' - '.date('M d, Y', strtotime('last day of previous month')),
        ];
        $datefilterdefault = 'tm';
        $tabletitle = static::$main['table-title'];
        $sendto = static::$main['table2-title'];

        $noidnew = explode('/', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH))[1];
        $triggercreate = $noidnew == static::$main['noidnew'] ? true : false;
        $route = 'lam_custom_transaksi_master_detail.salesinvoice.index';
        $primarycolor = '#3598DC';
        $backgroundcolor = '#EAEBF4';
        $primarywhite = '#ffffff';
        $secondarywhite = '#f2f2f2';
        $title = 'Inventory';
        $subtitle = 'Master Inventory';
        $salesorder = MP::getData(['table'=>'salesorder','select'=>['noid','kode', 'projectname', 'idstatustranc'],'where'=>[['noid','!=',0],['idstatustranc','=',4022]],'isactive'=>false]);

        return view('lam_custom_transaksi_master_detail.'.static::$main['table'].'.index', compact(
            'idtypepage',
            'sessionnoid',
            'sessionmyusername',
            'sessionname',
            'sessionidcompany',
            'sessioniddepartment',
            'sessionidgudang',
            'maintable',
            'breadcrumb',
            'pagenoid',
            'defaulttanggal',
            'defaulttanggaldue',
            'data',
            'columns',
            'columndefs',
            'columnsdetail',
            'columndefsdetail',
            'columnslog',
            'columndefslog',
            'columnscostcontrol',
            'columndefscostcontrol',
            'mtypecostcontrol',
            'mcardcustomer',
            'mcardsupplier',
            'mtypetranc_noid',
            'mtypetranc_nama',
            'mtypetranc_kode',
            'mtypetranctypestatus',
            'updatedropdown',
            'mtypetranctypestatus_noid',
            'mtypetranctypestatus_nama',
            'mtypetranctypestatus_classcolor',
            'mcard',
            'accmkodeakun',
            'accmcashbank',
            'accmcurrency',
            'genmgudang',
            'genmcompany',
            'genmdepartment',
            'mtypetranc',
            'invminventory',
            'invmsatuan',
            'accmpajak',
            'color',
            'datefilter',
            'datefilterdefault',
            'tabletitle',
            'sendto',
            'triggercreate',
            'route',
            'primarycolor',
            'backgroundcolor',
            'primarywhite',
            'secondarywhite',
            'title',
            'subtitle',
            'salesorder',
        ));
    }

    
    public function get(Request $request) {
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->get('search')['value'];
        $orderbycolumn = $request->get('columns')[$request->get('order')[0]['column']]['data'];
        if ($orderbycolumn == 'checkbox' || $orderbycolumn == 'no') {
            $orderbycolumn = 'noid';
        }
        $orderby = [
            'column' => $orderbycolumn,
            'sort' => $request->get('order')[0]['dir']
        ];
        $filter = $request->get('filter');
        $datefilter = $request->get('datefilter');
        $datefilter2 = $request->get('datefilter2');

        $result = MP::getSales($start, $length, $search, $orderby, $filter, $datefilter, $datefilter2);
        $data = $result['data'];
        $allnoid = MP::getAllNoid(static::$main['table']);

        $resulttotal = count(MP::getSales(null, null, $search, $orderby, $filter, $datefilter, $datefilter2)['data']);

        $resultsnst = MP::getMTypeTrancTypeStatus(static::$main['idtypetranc']);
        foreach ($resultsnst as $k => $v) {
            $snst[$v->nourut] = $v;
        }

        $conditions = Session::get(static::$main['noid'].'-conditions');
        $no = $start+1;
        foreach ($data as $k => $v) {
            $checkbox['checkbox'] = '<input type="checkbox" value="FALSE" class="checkbox-m" id="checkbox-m-'.$v->noid.'" onchange="selectCheckboxM('.$v->noid.')">';
            $_no['no'] = $no;
            $data[$k]->mtypetranctypestatus_nama = '<span style="padding: 2px; color: white; background: '.(new static)->color[$v->mtypetranctypestatus_classcolor].'">'.$v->mtypetranctypestatus_nama.'</span>';
            $data[$k]->tanggal = $this->dbdateTohumandate($data[$k]->tanggal);
            $data[$k]->tanggaldue = $this->dbdateTohumandate($data[$k]->tanggaldue);
            $data[$k]->totalsubtotal = $v->totalsubtotal;
            $data[$k]->generaltotal = $v->generaltotal;
            $array = (array)$data[$k];

            $btnprev = $conditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase'] && $conditions[$v->mtypetranctypestatus_noid]['isinternal'] 
                ? '<button onclick="showModalSNST('.$v->noid.','.$v->mtypetranc_noid.', '.$snst[$conditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']]->noid.', \''.$snst[$conditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']]->nama.'\')" data-toggle="tooltip" data-theme="dark" title="Set to '.$snst[$conditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']]->nama.'" class="btn btn-sm flat" style="background-color: '.(new static)->color[$snst[$conditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']]->classcolor].'"><i class="'.$snst[$conditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']]->classicon.' icon-xs text-light" style="text-align:center"></i> </button>' 
                : '';
            $btnnext = $conditions[$v->mtypetranctypestatus_noid]['next_idstatusbase'] && $conditions[$v->mtypetranctypestatus_noid]['isinternal'] 
                ? $conditions[$v->mtypetranctypestatus_noid]['next_idstatusbase'] == 4
                    ? '<button onclick="sendToNext('.$v->noid.',\''.$v->kodereff.'\')" data-toggle="tooltip" data-theme="dark" title="Send to '.static::$main['table2-title'].'" class="btn btn-sm flat" style="background-color: '.(new static)->color[$snst[$conditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->classcolor].'"><i class="'.$snst[$conditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->classicon.' icon-xs text-light" style="text-align:center"></i> </button>' 
                    : '<button onclick="showModalSNST('.$v->noid.','.$v->mtypetranc_noid.', '.$snst[$conditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->noid.', \''.$snst[$conditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->nama.'\')" data-toggle="tooltip" data-theme="dark" title="Set to '.$snst[$conditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->nama.'" class="btn btn-sm flat" style="background-color: '.(new static)->color[$snst[$conditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->classcolor].'"><i class="'.$snst[$conditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->classicon.' icon-xs text-light" style="text-align:center"></i> </button>' 
                : '';

            $btnsapcf = $conditions[$v->mtypetranctypestatus_noid]['actsetstatus'] ? '<button onclick="showModalSAPCF({noid:'.$v->noid.',idtypetranc:'.$v->mtypetranc_noid.',idstatustranc:'.$v->mtypetranctypestatus_noid.'})" data-toggle="tooltip" data-theme="dark" title="Set Status" class="btn btn-dark btn-sm flat"><i class="icon-cogs icon-xs text-light" style="text-align:center"></i> </button>' : '';
            $btnprint = '<button onclick="printData('.static::$main['noid'].',\''.base64_encode(static::$main['printcode'].'/'.$v->noid.'/'.date('Ymdhis')).'\')" data-toggle="tooltip" data-theme="dark" title="Print" class="btn btn-sm flat" style="background-color: '.(new static)->color['blue-soft'].'"><i class="fa fa-file icon-xs text-light" style="text-align:center;"></i> </button>';
            $btnlog = '<button onclick="showModalLog('.$v->noid.', \''.$v->kode.'\', \''.$v->mtypetranc_nama.'\')" data-toggle="tooltip" data-theme="dark" title="Show Log" class="btn btn-sm flat" style="background-color: #8775A7"><i class="icon-file-text icon-xs text-light"></i> </button>';
            $btnview = $conditions[$v->mtypetranctypestatus_noid]['actview'] ? '<button onclick="viewData('.$v->noid.')" data-toggle="tooltip" data-theme="dark" title="View" class="btn btn-info btn-sm flat"><i class="fa fa-eye icon-xs"></i> </button>' : '';
            $btnedit = $conditions[$v->mtypetranctypestatus_noid]['actedit'] ? '<button onclick="editData('.$v->noid.')" data-toggle="tooltip" data-theme="dark" title="Edit" class="btn btn-warning btn-sm flat"><i class="fa fa-edit icon-xs" style="text-align:center"></i> </button>' : '';
            $btndelete = $conditions[$v->mtypetranctypestatus_noid]['actdelete'] ? '<button onclick="showModalDelete('.$v->noid.')" data-toggle="tooltip" data-theme="dark" title="Delete" class="btn btn-danger btn-sm flat"><i class="fa fa-trash icon-xs"></i> </button>' : '';

            $allbtn = $btnprev;
            $allbtn .= $btnnext;
            $allbtn .= $btnsapcf;
            $allbtn .= $btnprint;
            $allbtn .= $btnlog;
            $allbtn .= $btnview;
            $allbtn .= $btnedit;
            $allbtn .= $btndelete;

            $action = [
                'action' => '<div class="d-flex align-items-center pull-right button-action button-action-'.$k.'">
                    '.$allbtn.'
                </div>'
            ];
            $array['idtypetranc'] = $data[$k]->mtypetranc_nama;
            $array['idstatustranc'] = $data[$k]->mtypetranctypestatus_nama;
            $data[$k] = (object)array_merge($checkbox, $_no, $array, $action);
            $no++;
        }
        
        return response()->json([
            'data' => $data,
            'draw' => intval($request['draw']),
            'recordsFiltered' => $resulttotal,
            'recordsTotal' => $resulttotal,
            'allnoid' => $allnoid,
            'datefilter' => $result['datefilter']
        ]);
    }

    public function get_log(Request $request) {
        $statusadd = $request->get('statusadd');
        $noid = $request->get('noid');
        $kode = $request->get('kode');
        $noidmore = $request->get('noidmore');
        $fieldmore = $request->get('fieldmore');
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->get('search')['value'];
        $orderbycolumn = $request->get('columns')[$request->get('order')[0]['column']]['data'];
        if ($orderbycolumn == 'checkbox' || $orderbycolumn == 'no') {
            $orderbycolumn = 'noid';
        }
        $orderby = [
            'column' => $orderbycolumn,
            'sort' => $request->get('order')[0]['dir']
        ];

        $data = MP::getLog($noid, $kode, $start, $length, $search, $orderby);
        
        if ($search) {
            $resulttotal = count($data);
        } else {
            $resulttotal = count(MP::findLog($noid, $kode));
        }
        // $resulttotal = count($data);

        $no = $start+1;
        foreach ($data as $k => $v) {
            // $checkbox['checkbox'] = '<input type="checkbox" id="checkbox-'.$k.'" onchange="selectCheckbox(\'checkbox-'.$k.'\')">';
            $_no['no'] = $no;
            // $data[$k]->keterangan = substr($v->keterangan, 0, 39).' <button class="btn btn-info btn-sm" onclick="getDatatableLog('.$v->idreff.', \''.$v->kode.'\', '.$v->idreff.', \'keterangan\')">More</button>';
            $data[$k]->logsubjectfull = $v->logsubject;
            $data[$k]->keteranganfull = $v->keterangan;


            // $btnlogsubject = strlen($v->logsubject) <= 44 ? '' : ' <button id="morelog-logsubject-'.$v->noid.'" class="btn btn-success btn-sm" onclick="moreLog('.$v->noid.', \'logsubject\')"><i class="icon-angle-right"></i></button>';
            // $btnketerangan = strlen($v->keterangan) <= 44 ? '' : ' <button id="morelog-keterangan-'.$v->noid.'" class="btn btn-success btn-sm" onclick="moreLog('.$v->noid.', \'keterangan\')"><i class="icon-angle-right"></i></button>';

            $btnlogsubject = strlen($v->logsubject) <= 44 ? '' : '...';
            $btnketerangan = strlen($v->keterangan) <= 44 ? '' : '...';

            $data[$k]->logsubject = substr($v->logsubject, 0, 44).'<span id="logsubject-'.$v->noid.'" style="display: none">'.substr($v->logsubject, 44).'</span>'.$btnlogsubject;
            $data[$k]->keterangan = substr($v->keterangan, 0, 44).'<span id="keterangan-'.$v->noid.'" style="display: none">'.substr($v->keterangan, 44).'</span>'.$btnketerangan;
            $data[$k]->docreate = $this->dbdateTohumandate($data[$k]->docreate);
            $array = (array)$data[$k];
            $data[$k] = (object)array_merge($_no, $array);
            $no++;
        }
        
        return response()->json([
            'data' => $data,
            'draw' => intval($request['draw']),
            'recordsFiltered' => $resulttotal,
            'recordsTotal' => $resulttotal
        ]);
    }

    public function get_costcontrol(Request $request) {
        $statusadd = $request->get('statusadd');
        $statusdetail = $request->get('statusdetail');
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->get('search')['value'];
        $orderbycolumn = $request->get('columns')[$request->get('order')[0]['column']]['data'];
        if ($orderbycolumn == 'checkbox' || $orderbycolumn == 'no') {
            $orderbycolumn = 'noid';
        }
        $orderby = [
            'column' => $orderbycolumn,
            'sort' => $request->get('order')[0]['dir']
        ];

        $data = MP::getCostControl($start, $length, $search, $orderby);
        
        if ($search) {
            $resulttotal = count($data);
        } else {
            $resulttotal = count(MP::getData(['table'=>'salesinvoice','select'=>['noid'],'isactive'=>true]));
        }

        $no = $start+1;
        foreach ($data as $k => $v) {
            $checkbox['checkbox'] = '<input type="checkbox" id="checkbox-'.$k.'" onchange="selectCheckbox(\'checkbox-'.$k.'\')">';
            $_no['no'] = $no;
            $data[$k]->tanggal = $this->dbdateTohumandate($data[$k]->tanggal);
            $array = (array)$data[$k];
            $action = [
                'action' => $statusdetail == 'true' ? '' : '<div class="d-flex align-items-center text-center button-action button-action-'.$k.'">
                    <a onclick="chooseCostControl('.$v->noid.', \''.$v->kode.'\', '.$v->idtypecostcontrol.')" title="Choose" class="btn btn-success btn-sm flat"><i class="fa fa-hand-o-up icon-xs" style="text-align:center"></i> Choose</a>
                </div>'
            ];
            $data[$k] = (object)array_merge($checkbox, $_no, $array, $action);
            $no++;
        }
        
        return response()->json([
            'data' => $data,
            'draw' => intval($request['draw']),
            'recordsFiltered' => $resulttotal,
            'recordsTotal' => $resulttotal
        ]);
    }

    public function get_detailpi(Request $request) {
        $statusdetail = json_decode($request->get('statusdetail'));
        $idcardsupplier = json_decode($request->get('idcardsupplier'));
        $idcompany = json_decode($request->get('idcompany'));
        $iddepartment = json_decode($request->get('iddepartment'));
        $idgudang = json_decode($request->get('idgudang'));
        $detailprev = json_decode($request->get('detailprev'));
        $unitqtyminpo = json_decode($request->get('unitqtyminpo'));
        $stp = $request->get('searchtypeprev');
        $searchprev = $request->get('searchprev');
        $search = $request->get('search')['value'];
        $start = $request->get('start');
        $length = $request->get('length');
        $nextnoid = MP::getNextNoid(static::$main['table']);
        $nextnoiddetail = MP::getNextNoid(static::$main['tabledetail']);
        
        $params = [
            'idcardsupplier' => $idcardsupplier,
            'idcompany' => $idcompany,
            'iddepartment' => $iddepartment,
            'idgudang' => $idgudang,
            'detailprev' => $detailprev,
            'searchtypeprev' => $stp,
            'searchprev' => $searchprev,
            'search' => $search,
            'start' => $start,
            'length' => $length,
        ];
        $data = $stp == 1 ? MP::getDetailPrev($params) : MP::getDetailInv($params);

        if ($search) {
            $qty = count($data);
        } else {
            $params['start'] = null;
            $params['length'] = null;
            $qty = count($stp == 1 ? MP::getDetailPrev($params) : MP::getDetailInv($params));
        }

        $no = $start+1;
        $index = 0;
        $result = [];
        $pattern = "/".$search."/i";

        foreach ($data as $k => $v) {
            $v = (array)$v;
            $v['checkbox'] = '<input type="checkbox" id="checkbox-detail-'.$k.'" onchange="selectCheckbox(\'checkbox-detail-'.$k.'\')">';
            $v['no'] = $no;
            if ($stp == 2) {
                $v['noid'] = $nextnoid;
                $v['idmaster'] = '-';
                $v['kodeprev'] = '-';
                $v['namainventor2'] = '-';
                $v['keterangan'] = '-';
                $v['unitqty'] = '-';
                $v['unitqtysisa'] = '-';
                $v['subtotal'] = '-';
                $v['discountvar'] = '-';
                $v['discount'] = '-';
                $v['nilaidisc'] = '-';
                $v['idtypetax'] = '-';
                $v['idtax'] = '-';
                $v['prosentax'] = '-';
                $v['nilaitax'] = '-';
                $v['hargatotal'] = '-';
                $v['namacurrency'] = '-';
                $v['idcompany'] = $idcompany;
                $v['iddepartment'] = $iddepartment;
                $v['idgudang'] = $idgudang;
            } 

            $isaction = true;
            foreach ($detailprev as $k2 => $v2) {
                if ($v2->noid == $v['noid']) {
                    if ($stp == 1) {
                        $v['unitqtysisa'] = $v['unitqtysisa']-$v2->unitqty < 0 ? 0 : $v['unitqtysisa']-$v2->unitqty;
                        $isaction = false;
                        break;
                    }
                }
            }

            $v['action'] = $isaction ? '<div class="d-flex align-items-center text-center button-action-dprev button-action-dprev-'.$k.'">
                <a onclick="chooseDetailPrev(
                    '.$v['noid'].',
                    \''.$v['idmaster'].'\',
                    \''.$v['kodeprev'].'\',
                    \''.$v['idinventor'].'\',
                    \''.$v['kodeinventor'].'\',
                    \''.$v['namainventor'].'\',
                    \''.$v['namainventor2'].'\',
                    \''.$v['keterangan'].'\',
                    \''.$v['idgudang'].'\',
                    \''.$v['idcompany'].'\',
                    \''.$v['iddepartment'].'\',
                    \''.$v['idsatuan'].'\',
                    \''.$v['namasatuan'].'\',
                    \''.$v['konvsatuan'].'\',
                    \''.$v['unitqty'].'\',
                    \''.$v['unitqtysisa'].'\',
                    \''.$v['unitprice'].'\',
                    \''.$v['subtotal'].'\',
                    \''.$v['discountvar'].'\',
                    \''.$v['discount'].'\',
                    \''.$v['nilaidisc'].'\',
                    \''.$v['idtypetax'].'\',
                    \''.$v['idtax'].'\',
                    \''.$v['prosentax'].'\',
                    \''.$v['nilaitax'].'\',
                    \''.$v['hargatotal'].'\',
                    \''.$v['namacurrency'].'\'
                )" title="Choose" class="btn btn-success btn-sm flat"><i class="icon-hand-up icon-xs" style="text-align:center"></i> Choose</a>
            </div>' : '';

                $result[] = $v;
            $no++;
            $nextnoid;
        }

        return response()->json([
            'data' => $result,
            'draw' => intval($request['draw']),
            'recordsFiltered' => $qty,
            'recordsTotal' => $qty,
            'length' => $request->get('length')
        ]);
    }
    
    public function get_detail(Request $request) {
        $data = json_decode($request->get('data'));
        $statusdetail = json_decode($request->get('statusdetail'));
        $condition = json_decode($request->get('condition'));
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->get('search')['value'];

        $qtyshow = 0;
        $qtyall = 0;
        $no = 1;
        $index = 0;
        $result = [];
        $pattern = "/".$search."/i";
        // dd($start);

        foreach ($data as $k => $v) {
            if ($v != null) {
                if ($k >= (int)$start && $qtyshow <= $length) {
                    $insertrecord = false;
                    foreach ($v as $k2 => $v2) {
                        if ($v2->name != 'idinventor') {
                            if (!array_key_exists('value', (array)$v2)) {
                                $v2->value = 0;
                            }
                            if (preg_match($pattern, $v2->value)) {
                                $insertrecord = true;
                            }
                            if ($v2->name == 'subtotal') {
                                $result[$index][$v2->name] = is_numeric($v2->value) ? 'Rp.'.number_format($v2->value) : $v2->value;
                            } else if ($v2->name == 'unitprice' || $v2->name == 'hargatotal') {
                                $result[$index][$v2->name] = filter_var($v2->value, FILTER_SANITIZE_NUMBER_INT);
                            } else {
                                $result[$index][$v2->name] = $v2->value;
                            }
                        }
                    }
                    if (!$insertrecord) {
                        array_pop($result);
                        continue;
                    }
                    $result[$index]['checkbox'] = '<input type="checkbox" id="checkbox-detail-'.$k.'" onchange="selectCheckbox(\'checkbox-detail-'.$k.'\')">';
                    $result[$index]['no'] = $no;

                    $allbtn = $condition->v || true ? '<a onclick="editDetail(\''.$k.'\', false)" title="View Detail" class="btn btn-success btn-sm flat mr-1"><i class="fa fa-search icon-xs"></i> </a>' : '';
                    $allbtn .= $condition->u || true ? '<a onclick="editDetail(\''.$k.'\', true)" title="Edit Detail" class="btn btn-warning btn-sm flat mr-1"><i class="fa fa-edit icon-xs" style="text-align:center"></i> </a>': '';
                    $allbtn .= $condition->d || true ? '<a  onclick="removeDetail(\''.$k.'\')" title="Delete Detail" class="btn btn-danger btn-sm flat mr-0"><i class="fa fa-trash icon-xs"></i> </a>' : '';
    
                    $result[$index]['action'] = '<div class="text-right button-action-tab button-action-tab-'.$k.'">
                        '.$allbtn.'
                    </div>';
                    if (count($result) == $length || count($data) == $k+1) {
                        break;
                    }
                    $index++;
                    $qtyshow++;
                }
                $no++;
            }
        }

        return response()->json([
            'data' => $result,
            'draw' => intval($request['draw']),
            'recordsFiltered' => $search ? count($result) : count($data),
            'recordsTotal' => $search ? count($result) : count($data),
            'length' => $request->get('length')
        ]);
    }

    private function humandateTodbdate($date) {
        if ($date) {
            $arrdate = explode('-', $date);
            $arrtime = explode(' ', $date);
            $year = explode(' ',$arrdate[2])[0];
            $month = $arrdate[1];
            $day = $arrdate[0];
            $time = count(explode(' ',$arrdate[2])) == 2 ? explode(' ',$arrdate[2])[1] : '';

            if ($month == 'Jan') {
                $month = '01';
            } else if ($month == 'Feb') {
                $month = '02';
            } else if ($month == 'Mar') {
                $month = '03';
            } else if ($month == 'Apr') {
                $month = '04';
            } else if ($month == 'May') {
                $month = '05';
            } else if ($month == 'Jun') {
                $month = '06';
            } else if ($month == 'Jul') {
                $month = '07';
            } else if ($month == 'Aug') {
                $month = '08';
            } else if ($month == 'Sep') {
                $month = '09';
            } else if ($month == 'Oct') {
                $month = '10';
            } else if ($month == 'Nov') {
                $month = '11';
            } else if ($month == 'Dec') {
                $month = '12';
            }

            return $year.'-'.$month.'-'.$day.' '.$time;
        } else {
            return NULL;
        }
    }

    private function dbdateTohumandate($date) {
        if ($date) {
            $arrdate = explode('-', $date);
            $arrtime = explode(' ', $date);
            $year = $arrdate[0];
            $month = $arrdate[1];
            $day = explode(' ',$arrdate[2])[0];

            if ($month == '01') {
                $month = 'Jan';
            } else if ($month == '02') {
                $month = 'Feb';
            } else if ($month == '03') {
                $month = 'Mar';
            } else if ($month == '04') {
                $month = 'Apr';
            } else if ($month == '05') {
                $month = 'May';
            } else if ($month == '06') {
                $month = 'Jun';
            } else if ($month == '07') {
                $month = 'Jul';
            } else if ($month == '08') {
                $month = 'Aug';
            } else if ($month == '09') {
                $month = 'Sep';
            } else if ($month == '10') {
                $month = 'Oct';
            } else if ($month == '11') {
                $month = 'Nov';
            } else if ($month == '12') {
                $month = 'Dec';
            }

            return count($arrtime) > 1 ? $day.'-'.$month.'-'.$year.' '.$arrtime[1] : $day.'-'.$month.'-'.$year;
        } else {
            return NULL;
        }
    }

    public function save(Request $request) {
        $cdf_checks = json_decode($request->get('cdf_checks'));
        $cdf_selecteds = (array)json_decode($request->get('cdf_selecteds'));
        $statusadd = json_decode($request->get('statusadd'));
        $next_idstatustranc = json_decode($request->get('next_idstatustranc'));
        $next_nama = json_decode($request->get('next_nama'));
        $next_keterangan = json_decode($request->get('next_keterangan'));
        $noid = $request->get('noid');
        $nextnoid = MP::getNextNoid(static::$main['table']);
        $nextnoiddetail = MP::getNextNoid(static::$main['tabledetail']);
        $nextnoidcdf = MP::getNextNoid(static::$main['tablecdf']);

        if ($statusadd) {
            $others = [
                'noid' => $nextnoid,
                'idtypetranc' => 0,
                'idcreate' => Session::get('noid'),
                'docreate' => date('Y-m-d H:i:s'),
            ];
        } else {
            $others = [
                'noid' => $noid,
                'idtypetranc' => 0,
                'idupdate' => Session::get('noid'),
                'lastupdate' => date('Y-m-d H:i:s'),
            ];
        }

        foreach ($request->get('formheader') as $k => $v) {
            if ($k == 'kodetypetranc') {
                continue;
            } else if ($k == 'idcostcontrol') {
                $formheader[$k] = $v ? $v : 0;
            } else if ($k == 'dostatus' || $k == 'tanggal' || $k == 'tanggaldue') {
                $formheader[$k] = $this->humandateTodbdate($v);
            } else if ($k == 'idstatustranc') {
                $formheader[$k] = $next_idstatustranc ? $next_idstatustranc : 4021;
            } else {
                $formheader[$k] = $v;
            }
        }
        foreach ($request->get('formmodaltanggal') as $k => $v) {
            $formmodaltanggal[$k] = $this->humandateTodbdate($v);
        }
        foreach ($request->get('formfooter') as $k => $v) {
            if ($k == 'subtotal') {
                $formfooter['totalsubtotal'] = filter_var($v, FILTER_SANITIZE_NUMBER_INT);
            } else if ($k == 'total') {
                $formfooter['generaltotal'] = filter_var($v, FILTER_SANITIZE_NUMBER_INT);
            } else {
                $formfooter[$k] = $v;
            }
        }

        $input = [
            'others' => $others,
            'formheader' => $formheader,
            'forminternal' => $request->get('forminternal'),
            'formfooter' => $formfooter,
            'formmodaltanggal' => $formmodaltanggal,
            'prd' => $request->get('purchasedetail')
        ];

        $table = array_merge(
            $input['others'],
            $input['formheader'],
            $input['formmodaltanggal'],
            $input['formfooter']
        );

        $tabledetail = [];
        if ($input['prd'] != null) {
            foreach ($input['prd'] as $k => $v) {
                $tabledetail[$k]['noid'] = $nextnoiddetail;
                $tabledetail[$k]['idmaster'] = $statusadd ? $nextnoid : $noid;
                foreach ($v as $k2 => $v2) {
                    if ($v2['name'] == 'noid' || $v2['name'] == 'kodeinventor' || $v2['name'] == 'namainventor' || $v2['name'] == 'idcurrency' || $v2['name'] == 'namacurrency' || $v2['name'] == 'namasatuan' || $v2['name'] == 'idcompany' || $v2['name'] == 'iddepartment') {
                        continue;
                    } else if ($v2['name'] == 'namainventor2') {
                        $tabledetail[$k]['namainventor'] = $v2['value'];
                    } else if ($v2['name'] == 'unitprice' || $v2['name'] == 'subtotal' || $v2['name'] == 'hargatotal') {
                        $tabledetail[$k][$v2['name']] = filter_var($v2['value'], FILTER_SANITIZE_NUMBER_INT);
                    } else if ($v2['name'] == 'discountvar') {
                        $tabledetail[$k][$v2['name']] = $v2['value'] ? $v2['value'] : 0;
                    } else {
                        $tabledetail[$k][$v2['name']] = $v2['value'];
                    }
                }
                $nextnoiddetail++;
            }
        }

        $formcdf = (array)$request->get('formcdf');
        $cdf = [];
        if ($cdf_checks) {
            foreach ($cdf_checks as $k => $v) {
                $cdf[$k] = [
                    'noid' => $nextnoidcdf,
                    'idtypetranc' => static::$main['idtypetranc'],
                    'idreff' => $statusadd ? $nextnoid : $noid,
                    'idstatus' => '',
                    'varstatus' => '',
                    'idcardstatus' => '',
                    'dostatus' => date('Y-m-d H:i:s'),
                    'kode' => '',
                    'barcode' => '',
                    'nama' => $formcdf['cdf_detail_'.$k.'_nama'],
                    'keterangan' => $formcdf['cdf_detail_'.$k.'_keterangan'],
                    'iddmsfile' => $cdf_selecteds['cdf-'.$v]->noid,
                    'ishardcopy' => @$formcdf['cdf_detail_'.$k.'_ishardcopy']?$formcdf['cdf_detail_'.$k.'_ishardcopy']:0,
                    'ispublic' => @$formcdf['cdf_detail_'.$k.'_ispublic']?$formcdf['cdf_detail_'.$k.'_ispublic']:0,
                    'idworkflow' => '',
                    'filenamesystem' => '',
                    'filenameoriginal' => $cdf_selecteds['cdf-'.$v]->nama,
                    'filefolder' => '',
                    'iddmstype' => '',
                    'iddmsfolder' => '',
                    'iddmsfolderbase' => '',
                    'iddmskategori' => '',
                    'doctag' => '',
                    'doclabel' => '',
                    'docauthor' => '',
                    'doccreate' => '',
                    'docupdate' => '',
                    'filesize' => '',
                    'idcardupload' => '',
                    'doupload' => '',
                    'dopublish' => '',
                    'doexpired' => date('Y-m-d H:i:s', strtotime($formcdf['cdf_detail_'.$k.'_doexpired'])),
                    'doreviewlast' => date('Y-m-d H:i:s', strtotime($formcdf['cdf_detail_'.$k.'_doreviewlast'])),
                    'doreviewnext' => date('Y-m-d H:i:s', strtotime($formcdf['cdf_detail_'.$k.'_doreviewnext'])),
                ];

                if ($statusadd) {
                    $cdf[$k]['idcreate'] = Session::get('noid');
                    $cdf[$k]['docreate'] = date('Y-m-d H:i:s');
                } else {
                    $cdf[$k]['idupdate'] = Session::get('noid');
                    $cdf[$k]['lastupdate'] = date('Y-m-d H:i:s');
                }

                $nextnoidcdf++;
            }
        }

        $data = [
            static::$main['table'] => $table,
            static::$main['tabledetail'] => $tabledetail,
            'cdf' => $cdf
        ];

        if ($statusadd) {
            $savetonext = $next_idstatustranc ? ' (Save to '.$next_nama.')' : '';
            $logsubject = 'Data '.$request->get('formheader')['kodetypetranc'].' Kode = '.$data[static::$main['table']]['kode'].' CREATE'.$savetonext;
            if ($next_idstatustranc) {
                $keterangan = $next_keterangan;
            } else {
                $keterangan = 'Data '.$request->get('formheader')['kodetypetranc'].' Kode = '.$data[static::$main['table']]['kode'].' CREATE'.$savetonext.' BY '.Session::get('nama').' ON '.date('Y-m-d H:i:s');
            }
        } else {
            $savetonext = $next_idstatustranc ? ' (Edit to '.$next_nama.')' : '';
            $logsubject = 'Data '.$request->get('formheader')['kodetypetranc'].' Kode = '.$data[static::$main['table']]['kode'].' EDIT'.$savetonext;
            if ($next_idstatustranc) {
                $keterangan = $next_keterangan;
            } else {
                $keterangan = 'Data '.$request->get('formheader')['kodetypetranc'].' Kode = '.$data[static::$main['table']]['kode'].' EDIT'.$savetonext.' BY '.Session::get('nama').' ON '.date('Y-m-d H:i:s');
            }
        }

        $datalog = [
            'idtypetranc' => $data[static::$main['table']]['idtypetranc'],
            'idtypeaction' => $statusadd ? 1 : 2,
            'logsubject' => $logsubject,
            'keterangan' => $keterangan,
            'idreff' => $data[static::$main['table']]['noid'],
            'idreffd' => 0,
            'iddevice' => 0,
            'devicetoken' => $request->get('_token'),
            'idcreate' => $statusadd
                ? $data[static::$main['table']]['idcreate']
                : $data[static::$main['table']]['idupdate'],
            'docreate' => $statusadd
                ? $data[static::$main['table']]['docreate']
                : $data[static::$main['table']]['lastupdate'],
        ];
        
        if ($statusadd) {
            $result = MP::saveData(['data' => $data, 'datalog' => $datalog]);
        } else {
            $result = MP::updateData(['data' => $data, 'noid' => $noid, 'datalog' => $datalog]);
        }

        return response()->json(['success' => $result['success'], 'message' => $result['message']]);
    }

    public function find(Request $request) {
        $noid = $request->get('noid');
        $result = MP::findData($noid);
        $result['master']->dostatus = $this->dbdateTohumandate($result['master']->dostatus);
        $result['master']->tanggal = $this->dbdateTohumandate($result['master']->tanggal);
        $result['master']->tanggaltax = $this->dbdateTohumandate($result['master']->tanggaltax);
        $result['master']->tanggaldue = $this->dbdateTohumandate($result['master']->tanggaldue);
        $result['master']->mtypetranctypestatus_nama = $result['master']->idstatustranc ? $result['master']->mtypetranctypestatus_nama : 'DRAFT';
        $result['master']->mtypetranctypestatus_classcolor = $result['master']->idstatustranc ? (new static)->color[$result['master']->mtypetranctypestatus_classcolor] : 'grey';

        $prd = [];
        foreach ($result['detail'] as $k => $v) {
            $prd[$k] = [];
            $prd[$k][] = ['name' => 'kodeprev','value' => '-'];
            foreach ($v as $k2 => $v2) {
                $prd[$k][] = [
                    'name' => $k2,
                    'value' => $v2,
                ];
            }
            $prd[$k][] = ['name' => 'idcurrency','value' => $result['master']->idcurrency];
            $prd[$k][] = ['name' => 'namacurrency','value' => $result['master']->namacurrency];
        }
        $result['detail'] = $prd;
        $result['mttts'] = Session::get(static::$main['noid'].'-conditions')[$result['master']->idstatustranc];

        return response()->json([
            'success' => true,
            'message' => 'Find Successfully',
            'data' => $result
        ]);
    }
    
    public function generate_code(Request $request) {
        $params = [
            'tablemaster' => static::$main['table'],
            'generatecode' => static::$main['generatecode'],
            'idtypecostcontrol' => $request->get('idtypecostcontrol')
        ];
        if ($params['idtypecostcontrol'] == 0) {
            $params['kodetypecostcontrol'] = 'NA';
        } else if ($params['idtypecostcontrol'] == 1) {
            $params['kodetypecostcontrol'] = 'PRO';
        } else if ($params['idtypecostcontrol'] == 2) {
            $params['kodetypecostcontrol'] = 'TR';
        } else if ($params['idtypecostcontrol'] == 3) {
            $params['kodetypecostcontrol'] = 'PS';
        }
        $result = MP::generateCode($params);

        return response()->json([
            'success' => true,
            'message' => 'Find Successfully',
            'data' => $result
        ]);
    }

    public function get_subjob(Request $request) {
        $idcostcenter = $request->idcostcenter;

        $result = MP::getSubjob(['idcostcenter' => $idcostcenter]);
        dd($result);

        return response()->json([
            'success' => true,
            'message' => 'Get Data Successfully',
            'data' => $result
        ]);
    }

    public function get_info() {
        $costcontrol = MP::getDefaultCostControl();

        return response()->json([
            'success' => true,
            'message' => 'Find Successfully',
            'data' => [
                'pr' => (object)[
                    'noid' => null,
                    'idtypetranc' => static::$main['idtypetranc']
                ],
                'mttts' => Session::get(static::$main['noid'].'-conditions')[static::$main['idtypetranc'].'1'],
                'mtypetranctypestatus' => [
                    'noid' => MP::findMTypeTrancTypeStatus(static::$main['idtypetranc'])->noid,
                    'nama' => 'DRAFT',
                    'classcolor' => (new static)->color['grey-silver']
                ],
                'mcarduser' => [
                    'noid' => Session::get('noid'),
                    'nama' => Session::get('myusername')
                ],
                'idtypecostcontrol' => $costcontrol->idtypecostcontrol,
                'idcostcontrol' => $costcontrol->noid,
                'kodecostcontrol' => $costcontrol->kode,
                'dostatus' => date('d-M-Y H:i:s'),
                'idcompany' => Session::get('idcompany'),
                'iddepartment' => Session::get('iddepartment'),
                'idgudang' => Session::get('idgudang'),
            ]
        ]);
    }

    public function find_last_supplier() {
        $result = MP::findLastSupplier();

        return response()->json([
            'success' => true,
            'message' => 'Find Successfully',
            'data' => $result
        ]);
    }

    public function send_to_next(Request $request) {
        $devicetoken = $request->get('_token');
        $noid = $request->get('noid');
        $kodereff = $request->get('kodereff');
        $nextnoid = MP::getNextNoid(static::$main['table2']);
        $nextnoidmilestone = MP::getNextNoid(static::$main['tablemilestone2']);
        $nextnoidheader = MP::getNextNoid(static::$main['tableheader2']);
        $nextnoiddetail = MP::getNextNoid(static::$main['tabledetail2']);
        $nextnoidcdf = MP::getNextNoid(static::$main['tablecdf2']);
        $find = MP::findRAB(['kode'=>$kodereff]);
        if (!$find['success']) return response()->json(['success'=>$find['success'],'message'=>$find['message']]);
        
        // Make Data Master
        $master = $find['data']['master'];
        $mtypetranc_kode = $master['mtypetranc_kode'];
        $gc = MP2::generateCode([
            'tablemaster' => static::$main['table2'],
            'generatecode' => static::$main['generatecode2'],
            'data' => $master
        ]);
        $master['noid'] = $nextnoid;
        $master['idstatustranc'] = 6041;
        $master['idtypetranc'] = 604;
        $master['kode'] = $gc['kode'];
        $master['kodereff'] = $find['data']['master']['salesinvoice_kode'];
        $master['idcreate'] = Session::get('noid');
        $master['docreate'] = date('Y-m-d H:i:s');
        unset($master['projectname']);
        unset($master['projectlocation']);
        unset($master['idcardpj']);
        unset($master['idcardkoor']);
        unset($master['idcardsitekoor']);
        unset($master['idcarddrafter']);
        unset($master['salesinvoice_kode']);
        unset($master['mtypetranc_kode']);

        // Make Data Milestone
        $milestone = [];
        $header = [];
        $detail = [];
        $cdf = [];

        if (count($find['data']['milestone']) != 0) {
            foreach ($find['data']['milestone'] as $k => $v) {
                unset($v->idinventor);
                
                $v->oldnoid = $v->noid;
                $v->noid = $nextnoidmilestone;
                $v->idmaster = $master['noid'];


                // Make Data Header
                if (count($find['data']['header']) != 0) {
                    foreach ($find['data']['header'] as $k2 => $v2) {
                        if ($v2->idmilestone != $v->oldnoid)  continue;

                        unset($v2->idinventor);

                        $v2->oldnoid = $v2->noid;
                        $v2->noid = $nextnoidheader;
                        $v2->idmaster = $master['noid'];
                        $v2->idmilestone = $nextnoidmilestone;


                        // Make Data Detail
                        $idreffd = '';
                        if (count($find['data']['detail']) != 0) {
                            foreach ($find['data']['detail'] as $k3 => $v3) {
                                if ($v3->idheader != $v2->oldnoid)  continue;

                                $v3->noid = $nextnoiddetail;
                                $v3->idmaster = $master['noid'];
                                $v3->idheader = $nextnoidheader;
                                $detail[] = (array)$v3;
                                $idreffd .= $v3->noid.',';
                                $nextnoiddetail++;
                            }
                        }

                        unset($v2->oldnoid);
                        $header[] = (array)$v2;
                        $nextnoidheader++;
                    }
                }
                
                unset($v->oldnoid);
                $milestone[] = (array)$v;
                $nextnoidmilestone++;
            }
        }

        if (count($find['data']['cdf']) != 0) {
            foreach ($find['data']['cdf'] as $k => $v) {
                $cdf[] = [
                    'noid' => $nextnoidcdf,
                    'idrap' => $master['noid'],
                    'idstatusfile' => $v->idstatusfile,
                    'idcardstatus' => $v->idcardstatus,
                    'dostatus' => date('Y-m-d H:i:s'),
                    'kode' => $v->kode,
                    'nama' => $v->nama,
                    'namaalias' => $v->namaalias,
                    'keterangan' => $v->keterangan,
                    'iddmsfile' => $v->iddmsfile,
                    'idcreate' => Session::get('noid'),
                    'docreate' => date('Y-m-d H:i:s'),
                ];
                $nextnoidcdf++;
            }
        }

        // Make Data Log
        $sendtonext = 'SENT TO NEXT (Send to '.static::$main['table2-title'].')';
        $rap = MP2::getData(['table'=>'mtypetranc','select'=>['noid','kode','nama'],'where'=>[['noid','=',static::$main['idtypetranc2']]]]);
        $log = [
            [
                'idtypetranc' => static::$main['idtypetranc'],
                'idtypeaction' => 0,
                'logsubject' => 'Data '.$mtypetranc_kode.' Kode = '.$master['kode'].' '.$sendtonext,
                'keterangan' => 'Data '.$mtypetranc_kode.' Kode = '.$master['kode'].' '.$sendtonext.' BY '.Session::get('nama').' ON '.date('Y-m-d H:i:s'),
                'idreff' => $master['noid'],
                'idreffd' => $idreffd,
                'iddevice' => 0,
                'devicetoken' => $devicetoken,
                'idcreate' => Session::get('noid'),
                'docreate' => date('Y-m-d H:i:s'),
            ],[
                'idtypetranc' => static::$main['idtypetranc2'],
                'idtypeaction' => 1,
                'logsubject' => 'Data '.$rap[0]->kode.' Kode = '.$gc['kode'].' CREATE',
                'keterangan' => 'Data '.$rap[0]->kode.' Kode = '.$gc['kode'].' CREATE BY '.Session::get('nama').' ON '.date('Y-m-d H:i:s'),
                'idreff' => $nextnoid,
                'idreffd' => $idreffd,
                'iddevice' => 0,
                'devicetoken' => $devicetoken,
                'idcreate' => Session::get('noid'),
                'docreate' => date('Y-m-d H:i:s'),
            ]
        ];

        $result = MP::sendToNext([
            'master' => $master,
            'milestone' => $milestone,
            'header' => $header,
            'detail' => $detail,
            'cdf' => $cdf,
            'log' => $log,
            'kodereff' => $kodereff
        ]);

        return response()->json(['success' => $result['success'], 'message' => $result['message']]);
    }

    public function snst(Request $request) {
        // dd($request->all());
        $noid = $request->get('noid');
        $find = MP::findData($noid);
        $idtypetranc = $request->get('idtypetranc');
        $idstatustranc = $request->get('idstatustranc');
        $statustranc = $request->get('statustranc');
        $typeaction = Session::get(static::$main['noid'].'-conditions')[$idstatustranc]['appmtypeaction_nama'];
        $idtypeaction = Session::get(static::$main['noid'].'-conditions')[$idstatustranc]['appmtypeaction_noid'];
        
        $logsubject = 'Data '.$find['master']->kodetypetranc.' Kode = '.$find['master']->kode.' '.$typeaction;
        $data = ['idstatustranc' => $idstatustranc];
        $datalog = [
            'idtypetranc' => $idtypetranc,
            'idtypeaction' => $idtypeaction,
            'logsubject' => $logsubject,
            'keterangan' => $request->get('keterangan'),
            'idreff' => $noid,
            'idreffd' => 0,
            'iddevice' => 0,
            'devicetoken' => $request->get('_token'),
            'idcreate' => Session::get('noid'),
            'docreate' => date('Y-m-d H:i:s'),
        ];

        $result = MP::snst([
            'noid' => $noid,
            'data' => $data,
            'datalog' => $datalog,
        ]);

        return response()->json(['success' => $result['success'], 'message' => $result['message']]);
    }

    public function div(Request $request) {
        $devicetoken = $request->get('_token');
        $noid = $request->get('noid');
        $find = MP::findData($noid);
        $idtypetranc = $request->get('idtypetranc');
        $typetranc = $find['purchase']->kodetypetranc;
        $idstatustranc = $request->get('idstatustranc');
        $statustranc = $request->get('statustranc');
        $keterangan = $request->get('keterangan');
        $idreff = $find['purchase']->noid;
        $idreffd = '';
        // if ($find['purchasedetail']) {
        //     foreach ($find['purchasedetail'] as $k => $v) {
        //         $idreffd .= $v->noid;
        //     }
        // }
        if ($idstatustranc == 1) {
            $idtypeaction = 7;
            $typeaction = 'SET DRAFT';
        } else if ($idstatustranc == 2) {
            $idtypeaction = 8;
            $typeaction = 'SET ISSUED';
        } else if ($idstatustranc == 3) {
            $idtypeaction = 9;
            $typeaction = 'SET VERIFIED';
        }
        $logsubject = 'Data '.$typetranc.' Kode = '.$find['purchase']->kode.' '.$typeaction;

        $data = [
            'idstatustranc' => $idstatustranc
        ];

        $datalog = [
            'idtypetranc' => $idtypetranc,
            'idtypeaction' => $idtypeaction,
            'logsubject' => $logsubject,
            'keterangan' => $keterangan,
            'idreff' => $idreff,
            'idreffd' => $idreffd,
            'iddevice' => 0,
            'devicetoken' => $devicetoken,
            'idcreate' => Session::get('noid'),
            'docreate' => date('Y-m-d H:i:s'),
        ];

        $result = MP::div($data, $noid, $datalog);

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Set '.$statustranc.' Successfully',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Set '.$statustranc.' Error!',
            ]);
        }
    }

    public function sapcf(Request $request) {
        $devicetoken = $request->get('_token');
        $noid = $request->get('noid');
        $find = MP::findData($noid);
        $idtypetranc = $request->get('idtypetranc');
        $typetranc = $find['master']->kodetypetranc;
        $idstatustranc = $request->get('idstatustranc');
        $statustranc = $request->get('statustranc');
        $keterangan = $request->get('keterangan');
        $idreff = $find['master']->noid;
        $idreffd = '';
        // if ($find['purchasedetail']) {
        //     foreach ($find['purchasedetail'] as $k => $v) {
        //         $idreffd .= $v->noid;
        //     }
        // }
        $idtypeaction = Session::get(static::$main['noid'].'-conditions')[$idstatustranc]['appmtypeaction_noid'];
        $typeaction = Session::get(static::$main['noid'].'-conditions')[$idstatustranc]['appmtypeaction_nama'];
        $logsubject = 'Data '.$typetranc.' Kode = '.$find['master']->kode.' '.$typeaction;

        $data = [
            'idstatustranc' => $idstatustranc
        ];

        $datalog = [
            'idtypetranc' => $idtypetranc,
            'idtypeaction' => $idtypeaction,
            'logsubject' => $logsubject,
            'keterangan' => $keterangan,
            'idreff' => $idreff,
            'idreffd' => $idreffd,
            'iddevice' => 0,
            'devicetoken' => $devicetoken,
            'idcreate' => Session::get('noid'),
            'docreate' => date('Y-m-d H:i:s'),
        ];

        $result = MP::sapcf($data, $noid, $datalog);

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Set '.$statustranc.' Successfully',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Set '.$statustranc.' Error!',
            ]);
        }
    }

    public function set_pending(Request $request) {
        $noid = $request->get('noid');

        $data = [
            'idstatustranc' => 5
        ];

        $datalog = [
            'idtypetranc' => $request->get('idtypetranc'),
            'idtypeaction' => 9,
            'logsubject' => 'PURCHASE-REQUEST/PENDING/'.$noid,
            'keterangan' => $request->get('keterangan'),
            'idreff' => 0,
            'idreffd' => 0,
            'iddevice' => 0,
            'devicetoken' => $request->get('_token'),
            'idcreate' => Session::get('noid'),
            'docreate' => date('Y-m-d H:i:s'),
        ];

        $result = MP::setPending($data, $noid, $datalog);

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Set Pending Successfully',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Set Pending Error!',
            ]);
        }
    }

    public function set_canceled(Request $request) {
        $noid = $request->get('noid');

        $data = [
            'idstatustranc' => 6
        ];

        $datalog = [
            'idtypetranc' => $request->get('idtypetranc'),
            'idtypeaction' => 9,
            'logsubject' => 'PURCHASE-REQUEST/CANCELED/'.$noid,
            'keterangan' => $request->get('keterangan'),
            'idreff' => 0,
            'idreffd' => 0,
            'iddevice' => 0,
            'devicetoken' => $request->get('_token'),
            'idcreate' => Session::get('noid'),
            'docreate' => date('Y-m-d H:i:s'),
        ];

        $result = MP::setCanceled($data, $noid, $datalog);

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Set Canceled Successfully',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Set Canceled Error!',
            ]);
        }
    }

    public function delete_select_m(Request $request) {
        $selecteds = $request->get('selecteds');
        $selectcheckboxmall = $request->get('selectcheckboxmall');

        if ($selectcheckboxmall) {
            $datalog = [
                'idtypetranc' => static::$main['idtypetranc'],
                'idtypeaction' => 3,
                'logsubject' => 'PURCHASE-REQUEST/DELETEALL/All',
                'keterangan' => 'Data All Deleted',
                'idreff' => 0,
                'idreffd' => 0,
                'iddevice' => 0,
                'devicetoken' => $request->get('_token'),
                'idcreate' => Session::get('noid'),
                'docreate' => date('Y-m-d H:i:s'),
            ];
        } else {
            $datalog = [];
            foreach ($selecteds as $k => $v) {
                $datalog[$v] = [
                    'idtypetranc' => static::$main['idtypetranc'],
                    'idtypeaction' => 3,
                    'logsubject' => 'PURCHASE-REQUEST/DELETE/'.$v,
                    'keterangan' => 'Data Deleted',
                    'idreff' => 0,
                    'idreffd' => 0,
                    'iddevice' => 0,
                    'devicetoken' => $request->get('_token'),
                    'idcreate' => Session::get('noid'),
                    'docreate' => date('Y-m-d H:i:s'),
                ];
            }
        }

        $result = MP::deleteSelectM($selecteds, $selectcheckboxmall, $datalog);

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Delete Successfully',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Delete Error!',
            ]);
        }
    }

    public function delete(Request $request) {
        $noid = $request->get('noid');
        $find = MP::findData($noid);
        $datalog = [
            'idtypetranc' => $find['master']->idtypetranc,
            'idtypeaction' => 3,
            'logsubject' => 'Data '.$find['master']->kodetypetranc.' Kode = '.$find['master']->kode.' DELETE',
            'keterangan' => 'Data '.$find['master']->kodetypetranc.' Kode = '.$find['master']->kode.' DELETE BY '.Session::get('nama').' ON '.date('Y-m-d H:i:s'),
            'idreff' => $find['master']->noid,
            'idreffd' => 0,
            'iddevice' => 0,
            'devicetoken' => $request->get('_token'),
            'idcreate' => Session::get('noid'),
            'docreate' => date('Y-m-d H:i:s'),
        ];
        
        $result = MP::deleteData(['noid' => $noid, 'datalog' => $datalog]);

        return response()->json(['success' => $result['success'], 'message' => $result['message']]);
    }

    public function cdf_get(Request $request) {
        $whereformat = $request->get('whereformat');
        $wheresearch = $request->get('wheresearch');
        
        $querywhere = 'WHERE 1=1 ';
        if (@$whereformat) {
            if ($whereformat == 'img') {
                $querywhere .= "AND (dmsmtypefile.fileextention = 'png'
                                    OR dmsmtypefile.fileextention = 'jpg'
                                    OR dmsmtypefile.fileextention = 'jpeg'
                                    OR dmsmtypefile.fileextention = 'gif') ";
            } else {
                $querywhere .= "AND dmsmtypefile.fileextention = '$whereformat'";
            }
        }
        if (@$wheresearch) {
            $querywhere .= "AND (dmsmfile.nama LIKE '%$wheresearch%'
                                OR dmsmfile.filename LIKE '%$wheresearch%')";
        }

        $tablecdf = static::$main['tablecdf'];
        $idtypetranc = static::$main['idtypetranc'];
        $query = "SELECT 
                        dmsmfile.*, 
                        dmsmtypefile.fileextention AS fileextention, 
                        dmsmtypefile.classicon AS classicon, 
                        dmsmtypefile.classcolor AS classcolor,
                        dmsmtypefile.fileimage AS fileimage,
                        IFNULL(cdf.nama, '') AS cdf_nama,
                        IFNULL(cdf.keterangan, '') AS cdf_keterangan,
                        IFNULL(cdf.ishardcopy, '') AS cdf_ishardcopy,
                        IFNULL(cdf.ispublic, '') AS cdf_ispublic,
                        IFNULL(cdf.doexpired, '') AS cdf_doexpired,
                        IFNULL(cdf.doreviewlast, '') AS cdf_doreviewlast,
                        IFNULL(cdf.doreviewnext, '') AS cdf_doreviewnext
                    FROM dmsmfile
                    JOIN dmsmtypefile ON dmsmfile.idtypefile = dmsmtypefile.noid
                    LEFT JOIN $tablecdf AS cdf ON dmsmfile.noid = cdf.iddmsfile AND cdf.idtypetranc = $idtypetranc
                    $querywhere
                    ORDER BY dmsmfile.docreate DESC";
        $result = DB::connection('mysql2')->select($query);

        $data = [];
        foreach ($result as $k => $v) {
            $data[] = $v;
        }

        return response()->json($data);
    }

    function getStringBetween($str,$from,$to) {
        $string = substr($str, strpos($str, $from) + strlen($from));
        if (strstr($string,$to,TRUE) != FALSE) {
            $string = strstr($string,$to,TRUE);
        } else {
            $string = $str;
        }
        return $string;
    }

    public function cdf_upload(Request $request) {
        $validator = Validator::make($request->all(), [
            'file' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $request->get('file')
            ]);
        }

        
        $file = $request->file('file');
        $filename = $request->get('filename');
        $filenameori = $file->getClientOriginalName();
        $filenameedit = explode('.',$filename ? $filename : $filenameori);
        $filenameonly = $filenameedit[0];
        $filecheckexist = DB::connection('mysql2')->table('dmsmfile')->select('noid','filename')->where('filename',$filename)->orderBy('noid', 'desc')->first();
        
        if ($filecheckexist) {
            $filecheckdup = DB::connection('mysql2')->table('dmsmfile')->select('noid','filename')->where('filename','like',"$filenameonly%")->orderBy('noid', 'desc')->first();

            $getdup = $this->getStringBetween($filecheckdup->filename,'(',')');
            if ($getdup == $filecheckdup->filename) {
                $filenameonlyfix = $filenameonly.' (2)';
                $filenamefix = $filenameonlyfix.'.'.$filenameedit[1];
            } else {
                $filenameonlyfix = $filenameonly.' ('.((int)$getdup+1).')';
                $filenamefix = $filenameonlyfix.'.'.$filenameedit[1];
            }
        } else {
            $filenameonlyfix = $filenameonly;
            $filenamefix = $filename;
        }

        $filesize = $file->getSize();
        $explodefe = explode('.', $filenamefix);
        $fileextension = end($explodefe);

        $resultlastnoid = DB::connection('mysql2')->table('dmsmfile')->select('noid')->orderBy('noid', 'desc')->first();
        $lastnoid = ($resultlastnoid != null) ? $resultlastnoid->noid+1 : 1; 
        
        $filelocation = 'filemanager/original';
        $filepreview = 'filemanager/preview';
        $filethumbnail = 'filemanager/thumb';

        if ($fileextension == 'png' ||
            $fileextension == 'jpg' ||
            $fileextension == 'jpeg' ||
            $fileextension == 'gif') {
            //SAVE TO ORIGINAL
            $imageresizeoriginal = Image::make($file->path());
            $imageresizeoriginal->save($filelocation.'/'.$filenamefix);
            //SAVE TO PREVIEW
            $imageresizepreview = Image::make($file->path());
            $imageresizepreview->resize(900, 300, function ($constraint) {
                $constraint->aspectRatio();
            })->save($filepreview.'/'.$filenamefix);
            //SAVE TO THUMBNAIL
            $imageresizethumb = Image::make($file->path());
            $imageresizethumb->resize(300, 300, function ($constraint) {
                $constraint->aspectRatio();
            })->save($filethumbnail.'/'.$filenamefix);
        } else if ($fileextension == 'ai' ||
                    $fileextension == 'avi' ||
                    $fileextension == 'bin' ||
                    $fileextension == 'bmp' ||
                    $fileextension == 'cdr' ||
                    $fileextension == 'css' ||
                    $fileextension == 'csv' ||
                    $fileextension == 'dat' ||
                    $fileextension == 'dll' ||
                    $fileextension == 'doc' ||
                    $fileextension == 'docx' ||
                    $fileextension == 'dwg' ||
                    $fileextension == 'eml' ||
                    $fileextension == 'eps' ||
                    $fileextension == 'exe' ||
                    $fileextension == 'fla' ||
                    $fileextension == 'flv' ||
                    $fileextension == 'html' ||
                    $fileextension == 'html' ||
                    $fileextension == 'iso' ||
                    $fileextension == 'jar' ||
                    $fileextension == 'm4p' ||
                    $fileextension == 'm4v' ||
                    $fileextension == 'mdb' ||
                    $fileextension == 'midi' ||
                    $fileextension == 'mov' ||
                    $fileextension == 'mp3' ||
                    $fileextension == 'mp4' ||
                    $fileextension == 'mpeg' ||
                    $fileextension == 'mpg' ||
                    $fileextension == 'ogg' ||
                    $fileextension == 'pdf' ||
                    $fileextension == 'php' ||
                    $fileextension == 'ppt' ||
                    $fileextension == 'pptx' ||
                    $fileextension == 'ps' ||
                    $fileextension == 'psd' ||
                    $fileextension == 'pub' ||
                    $fileextension == 'rar' ||
                    $fileextension == 'sql' ||
                    $fileextension == 'swf' ||
                    $fileextension == 'tiff' ||
                    $fileextension == 'txt' ||
                    $fileextension == 'url' ||
                    $fileextension == 'wav' ||
                    $fileextension == 'wma' ||
                    $fileextension == 'wmv' ||
                    $fileextension == 'xls' ||
                    $fileextension == 'xlsx' ||
                    $fileextension == 'xml' ||
                    $fileextension == 'zip' ||
                    $fileextension == 'zipx') {
            $request->file('file')->move(public_path().'/filemanager/original/',$filenamefix);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Format not available!'
            ]);
        }

        $ispublic = $request->get('ispublic');
        $resulttypefile = DB::connection('mysql2')->table('dmsmtypefile')->select('noid')->where('fileextention', $fileextension)->first();
        $idtypefile = ($resulttypefile != null) ? $resulttypefile->noid : 9;

        $data = [
            'noid' => $lastnoid,
            'nama' => $filenameonlyfix,
            'filename' => $filenamefix,
            'filelocation' => $filelocation,
            'filepreview' => $filepreview,
            'filethumbnail' => $filethumbnail,
            'filesize' => $filesize,
            'idtypefile' => $idtypefile,
            'ispublic' => $ispublic,
            'docreate' => date('Y-m-d H:i:s'),
        ];

        $insert = DB::connection('mysql2')->table('dmsmfile')->insert($data);

        return response()->json([
            'success' => true,
            'message' => 'Upload '.strtoupper($fileextension).' successfully',
            'data' => json_encode($request->file('file'))
        ]);
    }

    public function cdf_edit(Request $request) {
        $filetag = '';
        if ($request->get('filetag') != '') {
            foreach ($request->get('filetag') as $k => $v) {
                $filetag .= count($request->get('filetag'))-1 == $k ? $v : $v.',';
            }
        }

        $result = DB::connection('mysql2')
                    ->table('dmsmfile')
                    ->where('noid', $request->get('noid'))
                    ->update([
                        'nama' => $request->get('filename'),
                        'ispublic' => $request->get('filestatus'),
                        'filetag' => $filetag
                    ]);
        
        return response()->json([
            'success' => $result ? true : false,
            'message' => $result
                ? 'Update '.$request->get('filename').' successfully'
                : 'Update '.$request->get('filename').' error!',
            'data' => json_encode($request->file('file'))
        ]);
    }
}