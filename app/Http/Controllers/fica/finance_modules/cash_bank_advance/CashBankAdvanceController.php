<?php namespace App\Http\Controllers\fica\finance_modules\cash_bank_advance;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;
use App\Menu;
use DB;

class CashBankAdvanceController extends Controller {

    public function index() {
        $defaults = $this->defaults();
        $pagenoid = $defaults['pagenoid'];
        $idtypepage = $defaults['idtypepage'];
        $tabletitle = $defaults['tabletitle'];
        $sidebar = $defaults['sidebar'];
        $breadcrumb = $defaults['breadcrumb'];
        $path = $defaults['path'];
        $cost_controls = $this->getCostControl();
        $employees = $this->getEmployee();

        return view($path, compact(
            'pagenoid',
            'idtypepage',
            'tabletitle',
            'sidebar',
            'breadcrumb',
            'cost_controls',
            'employees',
        ));
    }

    public function getData(Request $request) {
        $start = $request['start'];
        $length = $request['length'];
        $column = $request['columns'][$request['order'][0]['column']]['data'];
        $dir = $request['order'][0]['dir'];
        $month_and_year = $request['month_and_year'];
        $year_only = $request['year_only'];
        $search_value = $request['search']['value'];
        $id_cost_control = $request['id_cost_control'];
        
        $is_search_value = !is_null($search_value) && $search_value != '';
        $where_search = $is_search_value ? "AND (
            description LIKE '%$search_value%'
            OR me.name LIKE '%$search_value%'
            OR mc.nama LIKE '%$search_value%'
            OR total LIKE '%$search_value%'
        )" : '';

        $query = "SELECT
                cba.*,
                so.kode AS cost_control,
                me.name AS name_request_by,
                mc.nama AS name_created_by
            FROM cashbankadvance AS cba
            LEFT JOIN salesorder AS so ON so.noid = cba.id_cost_control
            LEFT JOIN memployee AS me ON me.noid = cba.request_by
            LEFT JOIN mcard AS mc ON mc.noid = cba.created_by
            WHERE 1=1
            $where_search
            ORDER BY $column $dir";
        // print_r($query);die;    

        $result = DB::connection('mysql2')->select($query);
        $length = $length == -1 ? count($result) : $length;

        $data = [];
        foreach ($result as $k => $v) {
            if ($k >= $start && count($data) < $length) {
                $no = $k+1;
                $v->no = $no;
                $v->date = date("d-M-Y", strtotime($v->date));
                // <button class=\"btn btn-info btn-sm\"><i class=\"fa fa-eye\"></i></button>
                $v->action = "
                    <button class=\"btn btn-warning btn-sm\" onclick=\"editData({
                        noid: $v->noid,
                        date: '".date("d-M-Y", strtotime($v->date))."',
                        description: '$v->description',
                        total: '$v->total',
                        request_by: $v->request_by,
                        id_cost_control: $v->id_cost_control,
                    })\"><i class=\"fa fa-edit\"></i></button>
                    <button class=\"btn btn-danger btn-sm\"><i class=\"fa fa-trash\" onclick=\"deleteData({
                        noid: $v->noid
                    })\"></i></button>
                ";
                $data[] = $v;
            }
        }

        return response()->json([
            'data' => $data,
            'draw' => intval($request['draw']),
            'recordsFiltered' => count($result),
            'recordsTotal' => count($data)
        ]);
    }

    public function saveData(Request $request) {
        $form = [];
        foreach ($request['form'] as $k => $v) {
            // Date
            if ($v['name'] == 'date') {
                if (is_null($v['value'])) return [
                    'success' => false,
                    'message' => 'Tanggal wajib diisi!'
                ];
                $form['date'] = date("Y-m-d", strtotime($v['value']));
            };

            // Description
            if ($v['name'] == 'description') {
                $form['description'] = $v['value'];
            };

            // Total
            if ($v['name'] == 'total') {
                if (is_null($v['value'])) return [
                    'success' => false,
                    'message' => 'Total wajib diisi!'
                ];
                if ((int) filter_var($v['value'], FILTER_SANITIZE_NUMBER_INT) <= 0) return [
                    'success' => false,
                    'message' => 'Total tidak boleh 0!'
                ];
                $form['total'] = (int) filter_var($v['value'], FILTER_SANITIZE_NUMBER_INT);
            };

            // Request by
            if ($v['name'] == 'request_by') {
                if (is_null($v['value'])) return [
                    'success' => false,
                    'message' => 'Request By wajib diisi!'
                ];
                $form['request_by'] = $v['value'];
            };

            // Cost Control
            if ($v['name'] == 'id_cost_control') {
                if (is_null($v['value'])) return [
                    'success' => false,
                    'message' => 'Cost Control wajib diisi!'
                ];
                $form['id_cost_control'] = $v['value'];
            };
        }
        $form['created_by'] = Session::get('noid');
        $form['created_at'] = date('Y-m-d H:i:s');
        
        $save = DB::connection('mysql2')
            ->table('cashbankadvance')
            ->insert($form);

        if (!$save) {
            return [
                'success' => false,
                'message' => 'Error Save Data'
            ];
        } else {
            return [
                'success' => true,
                'message' => 'Successfuly Save Data'
            ];
        }
    }

    public function updateData(Request $request) {
        $noid = null;
        $form = [];
        foreach ($request['form'] as $k => $v) {
            // Noid
            if ($v['name'] == 'noid') $noid = $v['value'];

            // Date
            if ($v['name'] == 'date') {
                if (is_null($v['value'])) return [
                    'success' => false,
                    'message' => 'Tanggal wajib diisi!'
                ];
                $form['date'] = date("Y-m-d", strtotime($v['value']));
            };

            // Description
            if ($v['name'] == 'description') {
                $form['description'] = $v['value'];
            };

            // Total
            if ($v['name'] == 'total') {
                if (is_null($v['value'])) return [
                    'success' => false,
                    'message' => 'Total wajib diisi!'
                ];
                if ((int) filter_var($v['value'], FILTER_SANITIZE_NUMBER_INT) <= 0) return [
                    'success' => false,
                    'message' => 'Total tidak boleh 0!'
                ];
                $form['total'] = (int) filter_var($v['value'], FILTER_SANITIZE_NUMBER_INT);
            };

            // Request by
            if ($v['name'] == 'request_by') {
                if (is_null($v['value'])) return [
                    'success' => false,
                    'message' => 'Request By wajib diisi!'
                ];
                $form['request_by'] = $v['value'];
            };

            // Cost Control
            if ($v['name'] == 'id_cost_control') {
                if (is_null($v['value'])) return [
                    'success' => false,
                    'message' => 'Cost Control wajib diisi!'
                ];
                $form['id_cost_control'] = $v['value'];
            };
        }
        $form['updated_by'] = Session::get('noid');
        $form['updated_at'] = date('Y-m-d H:i:s');

        $update = DB::connection('mysql2')
            ->table('cashbankadvance')
            ->where('noid', $noid)
            ->update($form);

        if (!$update) {
            return [
                'success' => false,
                'message' => 'Error Update Data'
            ];
        } else {
            return [
                'success' => true,
                'message' => 'Successfuly Update Data'
            ];
        }
    }

    public function deleteData(Request $request) {
        $delete = DB::connection('mysql2')
            ->table('cashbankadvance')
            ->where('noid', $request['noid'])
            ->delete();
        
        if (!$delete) {
            return [
                'success' => false,
                'message' => 'Error Delete Data'
            ];
        } else {
            return [
                'success' => true,
                'message' => 'Successfuly Delete Data'
            ];
        }
    }

    // Private

    private function getCostControl() {
        $query = "SELECT noid, kode, projectname
            FROM salesorder
            ORDER BY kode ASC";
        return DB::connection('mysql2')->select($query);
    }
    
    private function getEmployee() {
        $query = "SELECT noid, code, name
            FROM memployee
            WHERE isactive = 1
            ORDER BY name ASC";
        return DB::connection('mysql2')->select($query);
    }

    private function defaults() {
        $noid = 20211001;
        $pagenoid = $noid;
        $idtypepage = 9;
        $tabletitle = 'Dashboard';
        $idtypepage = $noid; 
        $sidebar = Session::get('login')
            ? Menu::getSidebarLogin($idtypepage)
            : Menu::getSidebarLoginBaru($idtypepage);
        $breadcrumb = Menu::getBreadcrumb($noid);
        $path = 'fica.finance_modules.cash_bank_advance.cash_bank_advance.index';
            
        return [
            'noid' => $noid,
            'pagenoid' => $pagenoid,
            'idtypepage' => $idtypepage,
            'tabletitle' => $tabletitle,
            'idtypepage' => $idtypepage,
            'sidebar' => $sidebar,
            'breadcrumb' => $breadcrumb,
            'path' => $path,
        ];
    }

}