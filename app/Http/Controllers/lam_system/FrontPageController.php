<?php

namespace App\Http\Controllers\lam_system;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Menu;
use App\Page;
use App\Panel;
use App\Widget;
use App\WidgetGrid;
use App\WidgetGridField;
use App\Departement;
use App\Company;
use Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Schema;
use Carbon;

class FrontPageController extends Controller
{
  public static function index($slug)
  {
        $menu = Menu::where('noid',$slug)->first();
        $idhomepagelink = $slug;
        $page = Page::where('noid',$menu->linkidpage)->first();
        $user = User::where('noid', Session::get('noid'))->first();
        $public = $menu->ispublic;
        $idhomepage =  $user->idhomepage;
        $noid = Session::get('noid');
        $myusername = Session::get('myusername');
    return view('lam_system.index', compact(
      'noid',
      'public',
      'myusername',
      'idhomepagelink',
      'page',
      'page_title',
      'idhomepage',
      'menu',
      'namatable',
       'page_description'));

  }

  public static function AboutUs($slug){
    $menu = Menu::where('noid',$slug)->first();
    $idhomepagelink = $slug;
    $page = Page::where('noid',$menu->linkidpage)->first();
    $user = User::where('noid', Session::get('noid'))->first();
    $public = $menu->ispublic;
    $idhomepage =  $user->idhomepage;
    $noid = Session::get('noid');
    $myusername = Session::get('myusername');
return view('lam_system.AboutUs', compact(
  'noid',
  'public',
  'myusername',
  'idhomepagelink',
  'page',
  'page_title',
  'idhomepage',
  'menu',
  'namatable',
   'page_description'));
  }
}
