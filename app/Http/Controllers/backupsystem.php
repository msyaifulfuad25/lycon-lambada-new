<?php

namespace App\Http\Controllers;

use App\Http\Controllers\lam_system\FrontPageController;
use App\Http\Controllers\lam_error\ErrorController;
use App\Http\Controllers\lam_custom\ErrorController as ErrorControllerFunc;
use App\Http\Controllers\lam_system\ErrorController as ErrorControllerFunc2;
Use DB;
use App\User;
use App\Menu;
use App\Page;
use App\Panel;
use App\Widget;
use App\WidgetGrid;
use App\WidgetGridField;
use App\Departement;
use App\Company;
use App\ViewGridField;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Schema;
use Carbon;

class SystemController extends Controller
{



      public function hapus(Request $request)
      {
        $namatable = $_POST['namatable'];
        $menu = Menu::where('noid',$namatable)->first();
        $page = Page::where('noid',$menu->linkidpage)->first();
        $panel = Panel::where('idpage',$page->noid)->first();
        $widget = Widget::where('noid', $panel->idwidget)->first();
        $filtertable = json_decode($widget->configwidget);
        $fieldtabel = $filtertable->WidgetFilter;
        $table = $widget->maintable;
        // dd($_POST);
        $in =   DB::connection('mysql2')->table($table)->where('noid',$_POST['id'])->delete();
        echo json_encode('Berhasil');
      }


  public static function index($slug)
  {
    if(!Session::get('login')){
        return view('pages.login');
    }
    $menu = Menu::where('noid',$slug)->first();
    if(strpos($menu->menucaption, ' ') !== false) {
      $pecah = explode(' ', $menu->menucaption);
      $namaview = strtolower($pecah[1]);
      } else {
        $namaview = strtolower($menu->menucaption);
      }
    $idhomepagelink = $slug;
    $page = Page::where('noid',$menu->linkidpage)->first();

          $user = User::where('noid', Session::get('noid'))->first();
          $public = $menu->ispublic;
          $idhomepage =  $user->idhomepage;
          $noid = Session::get('noid');
          $myusername = Session::get('myusername');

    if ($page->pagecontroller == null) {
        return ErrorController::UnauthorizedPage($slug);
    }else{
      $view = $page->pagecontroller;
      $keywords = explode("-", str_replace(array("\n", "\t", "\r", "\a", "/", "//",":"), "-",$view));
      $controllerView = $keywords[3];
      $viewRedi = $keywords[4];
      $folder = $keywords[0];
      $urlnya = 'App\Http\Controllers';
      $existsfile = $urlnya." \ ".$folder." \ ".$controllerView;
      $tamba = $urlnya." \ ".$folder." \ ".$controllerView;
      $tamba = str_replace(" ","",$tamba);
      $existsfile = str_replace(" ","",$existsfile);
      if (!class_exists($tamba)) {
          if ($folder == 'lam_system') {
           return ErrorControllerFunc2::UnderMaintenance($slug);
         }else if ($folder == 'lam_custom') {
             return ErrorControllerFunc::UnderMaintenance($slug);
         }else{
           return ErrorController::UnderMaintenance($slug);
         }
      }else if (!method_exists($existsfile,$viewRedi)) {
        if ($folder == 'lam_system') {
           return ErrorControllerFunc2::UnderMaintenanceFun2($slug);
         }else if ($folder == 'lam_custom') {
             return ErrorControllerFunc::UnderMaintenanceFun2($slug);
         }else{
           return ErrorController::UnderMaintenanceFun($slug);
         }
      }else{
        if ($folder == 'lam_system') {
            return app($tamba)->$viewRedi($slug);
         }else if ($folder == 'lam_custom') {
             return app($tamba)->$viewRedi($slug);
         }else{
             return app($tamba)->$viewRedi($slug);
         }
      }

      }


    }


    public function savedata(Request $request)
    {
      $data = $request->all();
      // dd($kode);
      $menu = Menu::where('noid',$data['data']['tabel'])->first();
      // dd($menu);

      $page = Page::where('noid',$menu->linkidpage)->first();
      $panel = Panel::where('idpage',$page->noid)->first();
      $widget = Widget::where('noid', $panel->idwidget)->first();
      $filtertable = json_decode($widget->configwidget);
      $fieldtabel = $filtertable->WidgetFilter;
      $table = $widget->maintable;
      $data = $request->all();
      $datapost=  array();
      parse_str($data['data']['data'], $datapost);
      if ($datapost['noid'] == '') {
          $total = DB::connection('mysql2')->table($table)->orderBy('noid','desc')->first();
          foreach($datapost as $key => $value)
            {
              $datapost['noid'] = $total->noid+1;
            }
      $in =   DB::connection('mysql2')->table($table)->insert(
          $datapost
      );
      echo json_encode(array(
        'status'=>'add',
        'table'=>'Company',
        'success'=>true
        ));
      }else{
         $array = \array_diff($datapost, ["noid" => $datapost['noid']]);
        $total = DB::connection('mysql2')->table($table)->where('noid',$datapost['noid'])->update($array);
        echo json_encode(array(
          'status'=>'edit',
          'table'=>'Company',
          'success'=>true
          ));
      }
    }



    public function ambildata2(Request $request)
    {
      $kode = $request->get('table');
          $menu = Menu::where('noid',$kode)->first();
            $page = Page::where('noid',$menu->linkidpage)->first();
            // dd($page);
      $widget = Widget::where('kode', $kode)->first();
      // dd($widget);
      $table = $widget->maintable;
      // $table = $request->get('table');
      // dd($table);
      // $widget = Widget::where('maintable', $table)->first();
      $widgetgrid = WidgetGrid::where('idcmswidget', $widget->noid)->first();
      $widgetgridfield = WidgetGridField::where('idcmswidgetgrid', $widgetgrid->noid)->where('newshow',1)->orderBy('formurut','asc')->get();
      // foreach ($widgetgridfield as $key => $value) {
      //   $dd[] = $value->coltypefield;
      // }
      // dd($widgetgridfield);
      $isifield = '';
      foreach ($widgetgridfield as $key => $value) {
        if ($value->newreq == 1) {
          $required = 'required';
        }else{
          $required = '';
        }
        if ($value->coltypefield == 96) {
          $select = WidgetGridField::getField($value,$value->fieldname,$value->fieldcaption,$value->newreq);
          // dd($select);
          $isifield = $select;
        }elseif ($value->coltypefield == 1) {
          $isifield = '<input width="'.$value->colheight.'"  '.$required.' type="text" class="form-control form-control"
          id="'.$value->fieldname.'" name="'.$value->fieldname.'" placeholder="'.$value->fieldcaption.'">';
        }elseif ($value->coltypefield == 4 ) {
          $isifield = '<textarea width="'.$value->colheight.'" '.$required.' class="form-control form-control"  id="'.$value->fieldname.'" name="'.$value->fieldname.'" placeholder="'.$value->fieldcaption.'"></textarea>';
        }elseif ($value->coltypefield == 31 ) {
          $isifield = '<div class="checkbox-list">
              <label class="checkbox">
                  <input width="'.$value->colheight.'" '.$required.' type="checkbox" id="'.$value->fieldname.'" name="'.$value->fieldname.'">
                  <span></span>
                  '.$value->fieldname.'
              </label>
              </div>';
        }elseif ($value->coltypefield == 41 ) {
          $isifield = '<div class="checkbox-list">
              <label class="checkbox">
                  <input width="'.$value->colheight.'" '.$required.' type="checkbox" id="'.$value->fieldname.'" name="'.$value->fieldname.'">
                  <span></span>
                  '.$value->fieldname.'
              </label>
              </div>';
        }elseif ($value->coltypefield == 51  ) {
          $isifield = '<div class="checkbox-list">
              <label class="checkbox">
                  <input width="'.$value->colheight.'" '.$required.' type="checkbox" id="'.$value->fieldname.'" name="'.$value->fieldname.'">
                  <span></span>
                  '.$value->fieldname.'
              </label>
              </div>';
        }elseif ($value->coltypefield == 12  ) {
          $mytime = \Carbon\Carbon::now()->format('d-m-Y');
          $isifield = '<div class="input-group date">
            <input type="text" '.$required.' width="'.$value->colheight.'" class="datepicker form-control" data-provide="datepicker" readonly="readonly" value="'.$mytime.'"  id="'.$value->fieldname.'" name="'.$value->fieldname.'">
            <div class="input-group-append">
              <span class="input-group-text">
                <i class="la la-calendar"></i>
              </span>
            </div>
          </div>';
        }elseif ($value->coltypefield == 21  ) {
          $mytime = \Carbon\Carbon::now()->format('d-m-Y H:i');
          $isifield = '<div class="input-group date" id="kt_datetimepicker_1" data-target-input="nearest">
						<input type="text" class="datepicker2 form-control datetimepicker-input" placeholder="Select date &amp; time" data-target="#kt_datetimepicker_1">
						<div class="input-group-append" data-target="#kt_datetimepicker_1" data-toggle="datetimepicker">
							<span class="input-group-text">
								<i class="ki ki-calendar"></i>
							</span>
						</div>
					</div>';
          // $isifield = '<div class="input-group date">
          //   <input type="text" '.$required.' class="datepicker form-control" data-provide="datepicker" readonly="readonly" value="'.$mytime.'"  id="'.$value->fieldname.'" name="'.$value->fieldname.'">
          //   <div class="input-group-append">
          //     <span class="input-group-text">
          //       <i class="la la-calendar"></i>
          //     </span>
          //   </div>
          // </div>';
        }else{
          $isifield = '';
        }

        $data['field'][]= array(
          'label'=>$value->fieldcaption,
          'kode'=>$value->coltypefield,
          'field'=>$isifield,
        );
                // dd($data);
      }
      // dd($data['field']);

      header("Content-type: application/json");
      header("Cache-Control: no-cache, must-revalidate");
      echo json_encode($data);
    }


    public function filtertabel(Request $request)
    {
        $id = $request->get('id');
        $kode = $request->get('table');
        $widget = Widget::where('kode', $kode)->first();

      dd($widget);
    }





    public function edit(Request $request)
    {
            $data = $request->all();
      $menu = Menu::where('noid',$data['namatable'])->first();

      $page = Page::where('noid',$menu->linkidpage)->first();
      $panel = Panel::where('idpage',$page->noid)->first();
      $widget = Widget::where('noid', $panel->idwidget)->first();
      $filtertable = json_decode($widget->configwidget);
      $fieldtabel = $filtertable->WidgetFilter;
      $table = $widget->maintable;
            $data = $request->all();
            $widget = Widget::where('maintable', $table)->first();
            $widgetgrid = WidgetGrid::where('idcmswidget', $widget->noid)->first();
            $widgetgridfield = WidgetGridField::where('idcmswidgetgrid', $widgetgrid->noid)->where('editshow',1)->get();
            $id = $data['id'];
            $datatab = DB::connection('mysql2')->table($table)->where('noid',$id)->first();
            $no = 1;

            foreach ($widgetgridfield as $key => $value) {
              $string = (string)$widgetgridfield[$key]->fieldname;
              $hasil[] = array(
                $widgetgridfield[$key]->fieldname =>$datatab->$string,
              );
            }
            $hasil2[] = array(
              'noid' =>$id,
            );
            $hasilsemua = array_merge($hasil,$hasil2);
                      echo json_encode($hasilsemua);

    }



}
