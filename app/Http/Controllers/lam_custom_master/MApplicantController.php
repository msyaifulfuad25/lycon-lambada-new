<?php namespace App\Http\Controllers\lam_custom_master;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;
use App\Menu;
use DB;

class MApplicantController extends Controller {

    public function index() {
        $defaults = $this->defaults();
        $pagenoid = $defaults['pagenoid'];
        $idtypepage = $defaults['idtypepage'];
        $tabletitle = $defaults['tabletitle'];
        $sidebar = $defaults['sidebar'];
        $breadcrumb = $defaults['breadcrumb'];
        $cost_controls = $this->getCostControl();

        return view('appsetup.application_config.master.master_applicant.index', compact(
            'pagenoid',
            'idtypepage',
            'tabletitle',
            'sidebar',
            'breadcrumb',
            'cost_controls',
        ));
    }

    public function getApplicant(Request $request) {
        $start = $request['start'];
        $length = $request['length'];
        $column = $request['columns'][$request['order'][0]['column']]['data'];
        $dir = $request['order'][0]['dir'];
        $month_and_year = $request['month_and_year'];
        $year_only = $request['year_only'];
        $search_value = $request['search']['value'];
        $id_cost_control = $request['id_cost_control'];
        
        // Query
        $is_month_and_year = !is_null($month_and_year) && $month_and_year != '';
        $is_year_only = !is_null($year_only) && $year_only != '';
        $is_search_value = !is_null($search_value) && $search_value != '';
        $is_id_cost_control = !is_null($id_cost_control) && $id_cost_control != '';

        $where_or_and = $is_month_and_year || $is_year_only ? 'AND' : 'WHERE';

        $where_month_and_year = $is_month_and_year ? "WHERE pr.tanggaldue LIKE '%$month_and_year%'" : '';
        $where_year_only = $is_year_only ? "WHERE pr.tanggaldue LIKE '%$year_only%'" : '';
        $where_search = $is_search_value ? "$where_or_and imi.nama LIKE '%$search_value%'" : '';
        $where_id_cost_control = $is_id_cost_control ? "$where_or_and pr.idcostcontrol = $id_cost_control" : '';

        $query = "SELECT
                *
            FROM applicant
            $where_month_and_year
            $where_year_only
            $where_search
            $where_id_cost_control
            GROUP BY noid
            ORDER BY $column $dir, noid DESC";
        // print_r($query);die;    

        $result = DB::connection('mysql2')->select($query);
        $length = $length == -1 ? count($result) : $length;

        $data = [];
        foreach ($result as $k => $v) {
            if ($k >= $start && count($data) < $length) {
                $no = $k+1;
                $v->no = $no;
                $v->action = "
                    <button class=\"btn btn-info btn-sm\" onclick=\"showModal({
                        element_id: '#modal_detail', 
                        noid: $v->noid, 
                        jobappliedfor: '$v->jobappliedfor',
                        firstname: '$v->firstname',
                        lastname: '$v->lastname',
                        gender: '$v->gender',
                        lasteducation: '$v->lasteducation',
                        major: '$v->major',
                        college: '$v->college',
                        birthplace: '$v->birthplace',
                        birthdate: '$v->birthdate',
                        ktpaddress: '$v->ktpaddress',
                        localaddress: '$v->localaddress',
                        phone: '$v->phone',
                        whatsapp: '$v->whatsapp',
                        kknumber: '$v->kknumber',
                        ktpnumber: '$v->ktpnumber',
                        npwpnumber: '$v->npwpnumber',
                        blood: '$v->blood',
                        height: '$v->height',
                        weight: '$v->weight',
                        status: '$v->status',
                        religion: '$v->religion',
                        nationality: '$v->nationality',
                        residence: '$v->residence',
                        vehiclestatus: '$v->vehiclestatus',
                        vehicletype: '$v->vehicletype',
                        language: '$v->language',
                        sima: '$v->sima',
                        simb1: '$v->simb1',
                        simb2: '$v->simb2',
                        simc: '$v->simc',
                        simd: '$v->simd',
                        phonebrand: '$v->phonebrand',
                        phonetype: '$v->phonetype',
                        clothessize: '$v->clothessize',
                        email: '$v->email',
                        facebook: '$v->facebook',
                        instagram: '$v->instagram',
                        photo: '$v->photo'
                    })\"><i class=\"fa fa-eye\"></i> Detail</button
                ";
                $data[] = $v;
            }
        }

        return response()->json([
            'data' => $data,
            'draw' => intval($request['draw']),
            'recordsFiltered' => count($result),
            'recordsTotal' => count($data)
        ]);
    }

    public function getApplicantDetail(Request $request) {
        $emergency = DB::connection('mysql2')
            ->table('applicantemergency')
            ->where('idapplicant', $request->get('noid'))
            ->get();
        $family = DB::connection('mysql2')
            ->table('applicantfamily')
            ->where('idapplicant', $request->get('noid'))
            ->get();
        $education = DB::connection('mysql2')
            ->table('applicanteducation')
            ->where('idapplicant', $request->get('noid'))
            ->get();
        $training = DB::connection('mysql2')
            ->table('applicanttraining')
            ->where('idapplicant', $request->get('noid'))
            ->get();
        $worked = DB::connection('mysql2')
            ->table('applicantworked')
            ->where('idapplicant', $request->get('noid'))
            ->get();
        $positive = DB::connection('mysql2')
            ->table('applicantpositivenegative')
            ->where('idapplicant', $request->get('noid'))
            ->where('ispositive', 1)
            ->get();
        $negative = DB::connection('mysql2')
            ->table('applicantpositivenegative')
            ->where('idapplicant', $request->get('noid'))
            ->where('ispositive', 0)
            ->get();

        return response()->json([
            'success' => true,
            'message' => 'Get data berhasil',
            'data' => [
                'emergency' => $emergency,
                'family' => $family,
                'education' => $education,
                'training' => $training,
                'worked' => $worked,
                'positive' => $positive,
                'negative' => $negative
            ]
        ]);
    }

    // Private

    private function getCostControl() {
        $query = "SELECT noid, kode, projectname
            FROM salesorder
            ORDER BY kode ASC";
        return DB::connection('mysql2')->select($query);
    }
    
    private function defaults() {
        $noid = 99020209;
        $pagenoid = $noid;
        $idtypepage = 9;
        $tabletitle = 'Applicant';
        $idtypepage = $noid; 
        $sidebar = Session::get('login')
            ? Menu::getSidebarLogin($idtypepage)
            : Menu::getSidebarLoginBaru($idtypepage);
        $breadcrumb = Menu::getBreadcrumb($noid);
            
        return [
            'noid' => $noid,
            'pagenoid' => $pagenoid,
            'idtypepage' => $idtypepage,
            'tabletitle' => $tabletitle,
            'idtypepage' => $idtypepage,
            'sidebar' => $sidebar,
            'breadcrumb' => $breadcrumb,
        ];
    }
}

?>