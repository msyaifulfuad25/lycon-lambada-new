<?php

namespace App\Http\Controllers\lam_custom_master;

use App\Http\Controllers\Controller;
use App\Model\lam_custom_master\MTypeCard;
use App\Model\RA\RAP as MP2;
use App\Model\Sales\SalesOrder;
use App\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Helpers\MyHelper;
use DB;
use Image;
use Storage;

class MTypeCardController extends Controller {

    public $color = [
        'white' => '#FFFFFF',
        'default' => '#E1E5EC',
        'dark' => '#2F353B',
        'blue' => '#3598DC',
        'blue-madison' => '#578EBE',
        'blue-chambray' => '#2C3E50',
        'blue-ebonyclay' => '#22313F',
        'blue-hoki' => '#67809F',
        'blue-steel' => '#4B77BE',
        'blue-soft' => '#4C87B9',
        'blue-dark' => '#5E738B',
        'blue-sharp' => '#5C9BD1',
        'blue-oleo' => '#94A0B2',
        'green' => '#32C5D2',
        'green-meadow' => '#1BBC9B',
        'green-seagreen' => '#1BA39C',
        'green-torquoise' => '#36D7B7',
        'green-haze' => '#44B6AE',
        'green-jungle' => '#26C281',
        'green-soft' => '#3FABA4',
        'green-dark' => '#4DB3A2',
        'green-sharp' => '#2AB4C0',
        'green-steel' => '#29B4B6',
        'grey' => '#E5E5E5',
        'grey-steel' => '#E9EDEF',
        'grey-cararra' => '#FAFAFA',
        'grey-gallery' => '#555555',
        'grey-cascade' => '#95A5A6',
        'grey-silver' => '#BFBFBF',
        'grey-salsa' => '#ACB5C3',
        'grey-salt' => '#BFCAD1',
        'grey-mint' => '#525E64',
        'red' => '#E7505A',
        'red-pink' => '#E08283',
        'red-sunglo' => '#E26A6A',
        'red-intense' => '#E35B5A',
        'red-thunderbird' => '#D91E18',
        'red-flamingo' => '#EF4836',
        'red-soft' => '#D05454',
        'red-haze' => '#F36A5A',
        'red-mint' => '#E43A45',
        'yellow' => '#C49F47',
        'yellow-gold' => '#E87E04',
        'yellow-casablanca' => '#F2784B',
        'yellow-crusta' => '#F3C200',
        'yellow-lemon' => '#F7CA18',
        'yellow-saffron' => '#F4D03F',
        'yellow-soft' => '#C8D046',
        'yellow-haze' => '#C5BF66',
        'yellow-mint' => '#C5B96B',
        'purple' => '#8E44AD',
        'purple-plum' => '#8775A7',
        'purple-medium' => '#BF55EC',
        'purple-studio' => '#8E44AD',
        'purple-wisteria' => '#9B59B6',
        'purple-seance' => '#9A12B3',
        'purple-intense' => '#8775A7',
        'purple-sharp' => '#796799',
        'purple-soft' => '#8877A9',
    ];
    public static $main = [
        'table' => 'mtypecard',
        'tablemilestone' => 'prjmrabmilestone',
        'tableheader' => 'prjmrabheader',
        'tabledetail' => 'prjmrabdetail',
        'tablecdf' => 'prjmrabdocument',
        'table-title' => 'Master Type Card',
        'table2' => 'prjmrap',
        'tabledetail2' => 'prjmrapdetail',
        'table2-title' => 'RAP',
        'generatecode' => 'generatecode/602/1',
        'generatecode2' => 'generatecode/604/1',
        'idtypetranc' => 602,
        'idtypetranc2' => 602,
        'noid' => 99020201,
        'noidnew' => 50011101,
        'printcode' => 'print_purchase_request',
        'condition' => [
            'mttts' => [
                [],
                [
                    'mttts' => 'draft',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => true,
                        'd' => true,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => true,
                        'd' => true,
                    ],
                    'draft' => false,
                    'issued' => true,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => false,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'issued',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => true,
                    'issued' => false,
                    'verified' => true,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => false,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'verified',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => true,
                    'verified' => false,
                    'approved' => true,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => false,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'approved',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'pending',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'canceled',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'finished',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
            ],
        ]
    ];

    public static function index() {
        $tabletitle = static::$main['table-title'];
        $breadcrumb = Menu::getBreadcrumb(static::$main['noid']);
        $pagenoid = static::$main['noid'];
        $idtypepage = static::$main['noid'];
        $color = (new static)->color;
        $maintable = static::$main['table'];
        $datefilter = [
            'a' => 'All',
            't' => date('M d, Y').' - '.date('M d, Y'),
            'y' => date('M d, Y', strtotime('-1 day')).' - '.date('M d, Y', strtotime('-1 day')),
            'l7d' => date('M d, Y').' - '.date('M d, Y', strtotime('-7 day')),
            'l30d' => date('M d, Y').' - '.date('M d, Y', strtotime('1 month')),
            'tm' => date('M d, Y', strtotime('first day of this month')).' - '.date('M d, Y', strtotime('last day of this month')),
            'lm' => date('M d, Y', strtotime('first day of previous month')).' - '.date('M d, Y', strtotime('last day of previous month')),
        ];
        $datefilterdefault = 'a';
        $filteris = [
            (object)[
                'noid' => 'all',
                'nama' => 'ALL'
            ],
            (object)[
                'noid' => 'isgroupuser',
                'nama' => 'Group User'
            ],
            (object)[
                'noid' => 'isuser',
                'nama' => 'User'
            ],
            (object)[
                'noid' => 'isemployee',
                'nama' => 'Employee'
            ],
            (object)[
                'noid' => 'iscustomer',
                'nama' => 'Customer'
            ],
            (object)[
                'noid' => 'issupplier',
                'nama' => 'Supplier'
            ],
            (object)[
                'noid' => 'is3rparty',
                'nama' => '3rparty'
            ],
        ];
        $genmcompany = MTypeCard::getData(['table'=>'genmcompany','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $genmdepartment = MTypeCard::getData(['table'=>'genmdepartment','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $genmgudang = MTypeCard::getData(['table'=>'genmgudang','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $mtypecard = MTypeCard::getData(['table'=>'mtypecard','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $mlokasiprop = MTypeCard::getData(['table'=>'mlokasiprop','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $mlokasikab = MTypeCard::getData(['table'=>'mlokasikab','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $mlokasikec = MTypeCard::getData(['table'=>'mlokasikec','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $mlokasikel = MTypeCard::getData(['table'=>'mlokasikel','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $genmgender = MTypeCard::getData(['table'=>'genmgender','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $genmreligion = MTypeCard::getData(['table'=>'genmreligion','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $genmmarital = MTypeCard::getData(['table'=>'genmmarital','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $genmpnsstatus = MTypeCard::getData(['table'=>'_genmpnsstatus','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $genmnationality = MTypeCard::getData(['table'=>'genmnationality','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $genmrace = MTypeCard::getData(['table'=>'genmrace','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $genmbloodtype = MTypeCard::getData(['table'=>'genmbloodtype','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $genmlanguage = MTypeCard::getData(['table'=>'genmlanguage','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $genmlokasi = MTypeCard::getData(['table'=>'genmlokasi','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $genmtypeaddress = MTypeCard::getData(['table'=>'genmtypeaddress','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);
        $genmtypecontact = MTypeCard::getData(['table'=>'genmtypecontact','select'=>['noid','kode','nama'],'orderby'=>[['nama','asc']],'isactive'=>true]);

        return view('lam_custom_master.'.static::$main['table'].'.index', compact(
            'tabletitle',
            'breadcrumb',
            'pagenoid',
            'idtypepage',
            'color',
            'maintable',
            'datefilter',
            'datefilterdefault',
            'filteris',
            'genmcompany',
            'genmdepartment',
            'genmgudang',
            'mtypecard',
            'mlokasiprop',
            'mlokasikab',
            'mlokasikec',
            'mlokasikel',
            'genmgender',
            'genmreligion',
            'genmmarital',
            'genmpnsstatus',
            'genmnationality',
            'genmrace',
            'genmbloodtype',
            'genmlanguage',
            'genmlokasi',
            'genmtypeaddress',
            'genmtypecontact'
        ));
    }
    
    public function get(Request $request) {
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->get('search')['value'];
        $orderbycolumn = $request->get('columns')[$request->get('order')[0]['column']]['data'];
        if ($orderbycolumn == 'checkbox' || $orderbycolumn == 'no') {
            $orderbycolumn = 'noid';
        }
        $orderby = [
            'column' => $orderbycolumn,
            'sort' => $request->get('order')[0]['dir']
        ];
        $filter = $request->get('filter');
        $datefilter = $request->get('datefilter');
        $datefilter2 = $request->get('datefilter2');

        $result = MTypeCard::getMaster($start, $length, $search, $orderby, $filter, $datefilter, $datefilter2);
        $data = $result['data'];
        $allnoid = MTypeCard::getAllNoid(static::$main['table']);

        $resulttotal = count(MTypeCard::getMaster(null, null, $search, $orderby, $filter, $datefilter, $datefilter2)['data']);

        $resultsnst = MTypeCard::getMTypeTrancTypeStatus(static::$main['idtypetranc']);
        foreach ($resultsnst as $k => $v) {
            $snst[$v->nourut] = $v;
            if($v->isinternal == 0 && $v->isexternal != 0) {
                $sapcf[] = $v;
            }
        }

        $conditions = Session::get(static::$main['noid'].'-conditions');
        $no = $start+1;
        foreach ($data as $k => $v) {
            $checkbox['checkbox'] = '<input type="checkbox" value="FALSE" class="checkbox-m" id="checkbox-m-'.$v->noid.'" onchange="selectCheckboxM('.$v->noid.')">';
            $_no['no'] = $no;
            $array = (array)$data[$k];
            
            $array['isactive'] = '<input type="checkbox" '.($array['isactive'] == 1 ? 'checked' : '').' onclick="return false;">';
            
            
            $btnview = '<button id="btn-view-data-'.$v->noid.'" onclick="viewData('.$v->noid.')" class="btn btn-info btn-sm flat" data-toggle="tooltip" data-theme="dark" title="View"><i class="fa fa-eye icon-xs"></i> </button>';
            $btnedit = '<button id="btn-edit-data-'.$v->noid.'" onclick="editData('.$v->noid.')" class="btn btn-warning btn-sm flat" data-toggle="tooltip" data-theme="dark" title="Edit"><i class="fa fa-edit icon-xs" style="text-align:center"></i> </button>';
            $btndelete = '<button id="btn-delete-data-'.$v->noid.'" onclick="showModalDelete('.$v->noid.')" class="btn btn-danger btn-sm flat" data-toggle="tooltip" data-theme="dark" title="Delete"><i class="fa fa-trash icon-xs"></i> </button>';

            $allbtn = @$btnprev;
            $allbtn .= @$btnnext;
            $allbtn .= @$btnsapcf;
            $allbtn .= @$btnprint;
            $allbtn .= @$btnlog;
            $allbtn .= @$btnview;
            $allbtn .= @$btnedit;
            $allbtn .= @$btndelete;

            $action = [
                'action' => '<div class="d-flex align-items-center pull-right button-action button-action-'.$k.'">
                    '.$allbtn.'
                </div>'
            ];
            $data[$k] = (object)array_merge($checkbox, $_no, $array, $action);
            $no++;
        }
        
        return response()->json([
            'data' => $data,
            'draw' => intval($request['draw']),
            'recordsFiltered' => $resulttotal,
            'recordsTotal' => $resulttotal,
            'allnoid' => $allnoid,
            'datefilter' => $result['datefilter']
        ]);
    }

    public function save(Request $request) {
        $req = $request->all();

        // DECLARE
        $form = [];

        // FOREACH
        foreach ($req['formmtypecard'] as $k => $v) {
            $form[str_replace('mtypecard_', '', $v['name'])] = $v['value'];
        }

        // CHECKBOX
        $form['isactive'] = (array_key_exists('isactive', $form)) ? 1 : 0;

        // REPLACE

        // CREATE OR UPDATE
        if ($req['statusadd'] == 'true') {
            $form['noid'] = MTypeCard::getNextNoid(static::$main['table']);
            $form['idcreate'] = Session::get('noid');
            $form['docreate'] = date('Y-m-d H:i:s');

            $result = MTypeCard::saveData([
                'form' => $form,
            ]);
        } else {
            $form['idupdate'] = Session::get('noid');
            $form['lastupdate'] = date('Y-m-d H:i:s');

            $result = MTypeCard::updateData([
                'form' => $form, 
                'noid' => $req['noid']
            ]);
        }

        return ['success' => $result['success'], 'message' => $result['message']];
    }

    public function find(Request $request) {
        $noid = $request->get('noid');

        $result = MTypeCard::findData($noid);
        if (!$result) return ['success' => $result['success'], 'message' => $result['message']];

        return [
            'success' => true, 
            'message' => 'Success Find Data', 
            'data' => [
                'find' => $result['data']['find'],
            ]
        ];
    }
    
    public function generate_code(Request $request) {
        $noid = $request->get('noid');
        $nextnoid = MP::getNextNoid(static::$main['table']);

        foreach ($request->get('formheader') as $k => $v) {
            if ($k == 'kodetypetranc') {
                continue;
            } else if ($k == 'idcostcontrol') {
                $formheader[$k] = $v ? $v : 0;
            } else if ($k == 'dostatus' || $k == 'tanggal' || $k == 'tanggaldue') {
                $formheader[$k] = $this->humandateTodbdate($v);
            } else if ($k == 'idstatustranc') {
                $formheader[$k] = @$next_idstatustranc ? $next_idstatustranc : 5011;
            } else {
                $formheader[$k] = $v;
            }
        }
        // $nextnourut = MP::getNextNourut(['table'=>static::$main['table'],'where'=>$formheader['idcostcontrol']]);
        // $formheader['nourut'] = $nextnourut;

        foreach ($request->get('formmodaltanggal') as $k => $v) {
            $formmodaltanggal[$k] = $this->humandateTodbdate($v);
        }
        foreach ($request->get('formfooter') as $k => $v) {
            if ($k == 'subtotal') {
                $formfooter['totalsubtotal'] = filter_var($v, FILTER_SANITIZE_NUMBER_INT);
            } else if ($k == 'total') {
                $formfooter['generaltotal'] = filter_var($v, FILTER_SANITIZE_NUMBER_INT);
            } else {
                $formfooter[$k] = $v;
            }
        }

        $input = [
            'formheader' => $formheader,
            'forminternal' => $request->get('forminternal'),
            'formfooter' => $formfooter,
            'formmodaltanggal' => $formmodaltanggal,
            'formmodalsupplier' => $request->get('formmodalsupplier'),
            'prd' => $request->get('purchasedetail')
        ];

        $table = array_merge(
            [
                'noid' => $nextnoid,
                'idtypetranc' => 0,
                'idcreate' => Session::get('noid'),
                'docreate' => date('Y-m-d H:i:s'),
                'idupdate' => Session::get('noid'),
                'lastupdate' => date('Y-m-d H:i:s'),
            ],
            $input['formheader'],
            $input['forminternal'],
            $input['formmodaltanggal'],
            $input['formmodalsupplier'],
            $input['formfooter']
        );

        $params = [
            'tablemaster' => static::$main['table'],
            'generatecode' => static::$main['generatecode'],
            'idcostcontrol' => $request->get('idcostcontrol'),
            'kodecostcontrol' => $request->get('kodecostcontrol'),
            'data' => $table
        ];
        
        $result = MP::generateCode($params);

        return response()->json([
            'success' => true,
            'message' => 'Find Successfully',
            'data' => $result
        ]);
    }

    public function delete_select_m(Request $request) {
        return MTypeCard::deleteSelectM([
            'selecteds' => $request->get('selecteds'), 
            'selectcheckboxmall' => $request->get('selectcheckboxmall'), 
            'datalog' => null
        ]);
    }

    public function delete(Request $request) {
        return MTypeCard::deleteData(['noid' => $request->get('noid')]);
    }
}