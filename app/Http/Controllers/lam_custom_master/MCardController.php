<?php

namespace App\Http\Controllers\lam_custom_master;

use App\Http\Controllers\Controller;
use App\Model\lam_custom_master\MCard;
use App\Model\RA\RAP as MP2;
use App\Model\Sales\SalesOrder;
use App\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Helpers\MyHelper;
use DB;
use Image;
use Storage;

class MCardController extends Controller {

    public $color = [
        'white' => '#FFFFFF',
        'default' => '#E1E5EC',
        'dark' => '#2F353B',
        'blue' => '#3598DC',
        'blue-madison' => '#578EBE',
        'blue-chambray' => '#2C3E50',
        'blue-ebonyclay' => '#22313F',
        'blue-hoki' => '#67809F',
        'blue-steel' => '#4B77BE',
        'blue-soft' => '#4C87B9',
        'blue-dark' => '#5E738B',
        'blue-sharp' => '#5C9BD1',
        'blue-oleo' => '#94A0B2',
        'green' => '#32C5D2',
        'green-meadow' => '#1BBC9B',
        'green-seagreen' => '#1BA39C',
        'green-torquoise' => '#36D7B7',
        'green-haze' => '#44B6AE',
        'green-jungle' => '#26C281',
        'green-soft' => '#3FABA4',
        'green-dark' => '#4DB3A2',
        'green-sharp' => '#2AB4C0',
        'green-steel' => '#29B4B6',
        'grey' => '#E5E5E5',
        'grey-steel' => '#E9EDEF',
        'grey-cararra' => '#FAFAFA',
        'grey-gallery' => '#555555',
        'grey-cascade' => '#95A5A6',
        'grey-silver' => '#BFBFBF',
        'grey-salsa' => '#ACB5C3',
        'grey-salt' => '#BFCAD1',
        'grey-mint' => '#525E64',
        'red' => '#E7505A',
        'red-pink' => '#E08283',
        'red-sunglo' => '#E26A6A',
        'red-intense' => '#E35B5A',
        'red-thunderbird' => '#D91E18',
        'red-flamingo' => '#EF4836',
        'red-soft' => '#D05454',
        'red-haze' => '#F36A5A',
        'red-mint' => '#E43A45',
        'yellow' => '#C49F47',
        'yellow-gold' => '#E87E04',
        'yellow-casablanca' => '#F2784B',
        'yellow-crusta' => '#F3C200',
        'yellow-lemon' => '#F7CA18',
        'yellow-saffron' => '#F4D03F',
        'yellow-soft' => '#C8D046',
        'yellow-haze' => '#C5BF66',
        'yellow-mint' => '#C5B96B',
        'purple' => '#8E44AD',
        'purple-plum' => '#8775A7',
        'purple-medium' => '#BF55EC',
        'purple-studio' => '#8E44AD',
        'purple-wisteria' => '#9B59B6',
        'purple-seance' => '#9A12B3',
        'purple-intense' => '#8775A7',
        'purple-sharp' => '#796799',
        'purple-soft' => '#8877A9',
    ];
    public static $main = [
        'table' => 'mcard',
        'tablemilestone' => 'prjmrabmilestone',
        'tableheader' => 'prjmrabheader',
        'tabledetail' => 'prjmrabdetail',
        'tablecdf' => 'prjmrabdocument',
        'table-title' => 'Master Card',
        'table2' => 'prjmrap',
        'tabledetail2' => 'prjmrapdetail',
        'table2-title' => 'RAP',
        'generatecode' => 'generatecode/602/1',
        'generatecode2' => 'generatecode/604/1',
        'idtypetranc' => 602,
        'idtypetranc2' => 602,
        'noid' => 99021101,
        'noidnew' => 50011101,
        'printcode' => 'print_purchase_request',
        'condition' => [
            'mttts' => [
                [],
                [
                    'mttts' => 'draft',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => true,
                        'd' => true,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => true,
                        'd' => true,
                    ],
                    'draft' => false,
                    'issued' => true,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => false,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'issued',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => true,
                    'issued' => false,
                    'verified' => true,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => false,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'verified',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => true,
                    'verified' => false,
                    'approved' => true,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => false,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'approved',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'pending',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'canceled',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'finished',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
            ],
        ]
    ];

    public static function index() {
        $tabletitle = static::$main['table-title'];
        $breadcrumb = Menu::getBreadcrumb(\Request::segment(1));
        $pagenoid = \Request::segment(1);
        $idtypepage = \Request::segment(1);
        $color = (new static)->color;
        $maintable = static::$main['table'];
        $datefilter = [
            'a' => 'All',
            't' => date('M d, Y').' - '.date('M d, Y'),
            'y' => date('M d, Y', strtotime('-1 day')).' - '.date('M d, Y', strtotime('-1 day')),
            'l7d' => date('M d, Y').' - '.date('M d, Y', strtotime('-7 day')),
            'l30d' => date('M d, Y').' - '.date('M d, Y', strtotime('1 month')),
            'tm' => date('M d, Y', strtotime('first day of this month')).' - '.date('M d, Y', strtotime('last day of this month')),
            'lm' => date('M d, Y', strtotime('first day of previous month')).' - '.date('M d, Y', strtotime('last day of previous month')),
        ];
        $datefilterdefault = 'a';
        $filteris = [
            (object)[
                'idpage' => '99021101',
                'title' => 'Master Card',
                'noid' => 'all',
                'nama' => 'ALL',
                'active' => false
            ],
            (object)[
                'idpage' => '99021102',
                'title' => 'Master Group User',
                'noid' => 'isgroupuser',
                'nama' => 'Group User',
                'active' => false
            ],
            (object)[
                'idpage' => '99021103',
                'title' => 'Master User',
                'noid' => 'isuser',
                'nama' => 'User',
                'active' => false
            ],
            (object)[
                'idpage' => '99021104',
                'title' => 'Master Employee',
                'noid' => 'isemployee',
                'nama' => 'Employee',
                'active' => false
            ],
            (object)[
                'idpage' => '99021105',
                'idpage2' => '40010302',
                'title' => 'Master Customer',
                'noid' => 'iscustomer',
                'nama' => 'Customer',
                'active' => false
            ],
            (object)[
                'idpage' => '99021106',
                'idpage2' => '40020302',
                'title' => 'Master Supplier',
                'noid' => 'issupplier',
                'nama' => 'Supplier',
                'active' => false
            ],
            (object)[
                'idpage' => '99021107',
                'title' => 'Master 3rdparty',
                'noid' => 'is3rdparty',
                'nama' => '3rparty',
                'active' => false
            ],
        ];
        $idpagenow = \Request::segment(1);
        foreach ($filteris as $k => $v) {
            if ($v->idpage == $idpagenow || 
                (array_key_exists('idpage2', $v) && $v->idpage2 == \Request::segment(1))
            ) {
                $tabletitle = $v->title;
                $filteris[$k]->active = true;
            }
        }
        $defaultprofilepicture = asset('assets/default-person.png');
        $genmcompany = MCard::getData(['table'=>'genmcompany','select'=>['noid','kode','nama'],'orderby'=>[['noid','asc']],'isactive'=>true]);
        $genmdepartment = MCard::getData(['table'=>'genmdepartment','select'=>['noid','kode','nama'],'orderby'=>[['noid','asc']],'isactive'=>true]);
        $genmgudang = MCard::getData(['table'=>'genmgudang','select'=>['noid','kode','nama'],'orderby'=>[['noid','asc']],'isactive'=>true]);
        $mtypecard = MCard::getData(['table'=>'mtypecard','select'=>['noid','kode','nama'],'orderby'=>[['noid','asc']],'isactive'=>true]);
        $genmgender = MCard::getData(['table'=>'genmgender','select'=>['noid','kode','nama'],'orderby'=>[['noid','asc']],'isactive'=>true]);
        $genmreligion = MCard::getData(['table'=>'genmreligion','select'=>['noid','kode','nama'],'orderby'=>[['noid','asc']],'isactive'=>true]);
        $genmmarital = MCard::getData(['table'=>'genmmarital','select'=>['noid','kode','nama'],'orderby'=>[['noid','asc']],'isactive'=>true]);
        $genmpnsstatus = MCard::getData(['table'=>'_genmpnsstatus','select'=>['noid','kode','nama'],'orderby'=>[['noid','asc']],'isactive'=>true]);
        $genmnationality = MCard::getData(['table'=>'genmnationality','select'=>['noid','kode','nama'],'orderby'=>[['noid','asc']],'isactive'=>true]);
        $genmrace = MCard::getData(['table'=>'genmrace','select'=>['noid','kode','nama'],'orderby'=>[['noid','asc']],'isactive'=>true]);
        $genmbloodtype = MCard::getData(['table'=>'genmbloodtype','select'=>['noid','kode','nama'],'orderby'=>[['noid','asc']],'isactive'=>true]);
        $genmlanguage = MCard::getData(['table'=>'genmlanguage','select'=>['noid','kode','nama'],'orderby'=>[['noid','asc']],'isactive'=>true]);
        $genmlokasi = MCard::getData(['table'=>'genmlokasi','select'=>['noid','kode','nama'],'orderby'=>[['noid','asc']],'isactive'=>true]);
        $genmtypeaddress = MCard::getData(['table'=>'genmtypeaddress','select'=>['noid','kode','nama'],'orderby'=>[['noid','asc']],'isactive'=>true]);
        $genmtypecontact = MCard::getData(['table'=>'genmtypecontact','select'=>['noid','kode','nama'],'orderby'=>[['noid','asc']],'isactive'=>true]);
        $mtypecard3rdparty = MCard::getData(['table'=>'mtypecard3rdparty','select'=>['noid','kode','nama'],'orderby'=>[['noid','asc']],'isactive'=>true]);
        $cmsmenu = MCard::getData(['connection'=>'mysql','table'=>'cmsmenu','select'=>['noid','menucaption'],'orderby'=>[['noid','asc']]]);

        return view('lam_custom_master.'.static::$main['table'].'.index', compact(
            'tabletitle',
            'breadcrumb',
            'pagenoid',
            'idtypepage',
            'color',
            'maintable',
            'datefilter',
            'datefilterdefault',
            'filteris',
            'defaultprofilepicture',
            'genmcompany',
            'genmdepartment',
            'genmgudang',
            'mtypecard',
            'genmgender',
            'genmreligion',
            'genmmarital',
            'genmpnsstatus',
            'genmnationality',
            'genmrace',
            'genmbloodtype',
            'genmlanguage',
            'genmlokasi',
            'genmtypeaddress',
            'genmtypecontact',
            'mtypecard3rdparty',
            'cmsmenu'
        ));
    }
    
    public function get(Request $request) {
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->get('search')['value'];
        $orderbycolumn = $request->get('columns')[$request->get('order')[0]['column']]['data'];
        if ($orderbycolumn == 'checkbox' || $orderbycolumn == 'no') {
            $orderbycolumn = 'noid';
        }
        $orderby = [
            'column' => $orderbycolumn,
            'sort' => $request->get('order')[0]['dir']
        ];
        $filter = $request->get('filter');
        $datefilter = $request->get('datefilter');
        $datefilter2 = $request->get('datefilter2');

        $result = MCard::getMaster($start, $length, $search, $orderby, $filter, $datefilter, $datefilter2);
        $data = $result['data'];
        $allnoid = MCard::getAllNoid(static::$main['table']);

        $resulttotal = count(MCard::getMaster(null, null, $search, $orderby, $filter, $datefilter, $datefilter2)['data']);

        $resultsnst = MCard::getMTypeTrancTypeStatus(static::$main['idtypetranc']);
        foreach ($resultsnst as $k => $v) {
            $snst[$v->nourut] = $v;
            if($v->isinternal == 0 && $v->isexternal != 0) {
                $sapcf[] = $v;
            }
        }

        $conditions = Session::get(\Request::segment(1).'-conditions');
        $no = $start+1;
        foreach ($data as $k => $v) {
            $checkbox['checkbox'] = '<input type="checkbox" value="FALSE" class="checkbox-m" id="checkbox-m-'.$v->noid.'" onchange="selectCheckboxM('.$v->noid.')">';
            $_no['no'] = $no;
            $array = (array)$data[$k];
            
            $array['isgroupuser'] = '<input type="checkbox" '.($array['isgroupuser'] == 1 ? 'checked' : '').' onclick="return false;">';
            $array['isuser'] = '<input type="checkbox" '.($array['isuser'] == 1 ? 'checked' : '').' onclick="return false;">';
            $array['isemployee'] = '<input type="checkbox" '.($array['isemployee'] == 1 ? 'checked' : '').' onclick="return false;">';
            $array['iscustomer'] = '<input type="checkbox" '.($array['iscustomer'] == 1 ? 'checked' : '').' onclick="return false;">';
            $array['issupplier'] = '<input type="checkbox" '.($array['issupplier'] == 1 ? 'checked' : '').' onclick="return false;">';
            $array['is3rdparty'] = '<input type="checkbox" '.($array['is3rdparty'] == 1 ? 'checked' : '').' onclick="return false;">';
            $array['isactive'] = '<input type="checkbox" '.($array['isactive'] == 1 ? 'checked' : '').' onclick="return false;">';
            
            
            $btnview = '<button id="btn-view-data-'.$v->noid.'" onclick="viewData('.$v->noid.')" class="btn btn-info btn-sm flat" data-toggle="tooltip" data-theme="dark" title="View"><i class="fa fa-eye icon-xs"></i> </button>';
            $btnedit = '<button id="btn-edit-data-'.$v->noid.'" onclick="editData('.$v->noid.')" class="btn btn-warning btn-sm flat" data-toggle="tooltip" data-theme="dark" title="Edit"><i class="fa fa-edit icon-xs" style="text-align:center"></i> </button>';
            $btndelete = '<button id="btn-delete-data-'.$v->noid.'" onclick="showModalDelete('.$v->noid.')" class="btn btn-danger btn-sm flat" data-toggle="tooltip" data-theme="dark" title="Delete"><i class="fa fa-trash icon-xs"></i> </button>';

            $allbtn = @$btnprev;
            $allbtn .= @$btnnext;
            $allbtn .= @$btnsapcf;
            $allbtn .= @$btnprint;
            $allbtn .= @$btnlog;
            $allbtn .= @$btnview;
            $allbtn .= @$btnedit;
            $allbtn .= @$btndelete;

            $action = [
                'action' => '<div class="d-flex align-items-center pull-right button-action button-action-'.$k.'">
                    '.$allbtn.'
                </div>'
            ];
            $data[$k] = (object)array_merge($checkbox, $_no, $array, $action);
            $no++;
        }
        
        return response()->json([
            'data' => $data,
            'draw' => intval($request['draw']),
            'recordsFiltered' => $resulttotal,
            'recordsTotal' => $resulttotal,
            'allnoid' => $allnoid,
            'datefilter' => $result['datefilter']
        ]);
    }

    public function get_mcardcontact(Request $request) {
        $data = json_decode($request->get('data'));
        $statusdetail = json_decode($request->get('statusdetail'));
        $condition = json_decode($request->get('condition'));
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->get('search')['value'];
        $ordercolumn = $request->get('columns')[$request->get('order')[0]['column']]['data'];
        $orderdir = $request->get('order')[0]['dir'];


        $qtyshow = 0;
        $qtyall = 0;
        $no = 1;
        $index = 0;
        $result = [];
        $pattern = "/".$search."/i";
        // dd($start);

        foreach ($data as $k => $v) {
            if ($v != null) {
                // if ($k >= (int)$start && $qtyshow <= $length) {
                    $insertrecord = false;
                    foreach ($v as $k2 => $v2) {
                        if ($v2->name != 'idinventor') {
                            if (!array_key_exists('value', (array)$v2)) {
                                $v2->value = 0;
                            }
                            if (preg_match($pattern, $v2->value)) {
                                $insertrecord = true;
                            }
                            if ($v2->name == 'subtotal') {
                                $result[$index][$v2->name] = is_numeric($v2->value) ? 'Rp.'.number_format($v2->value) : $v2->value;
                            } else if ($v2->name == 'unitprice' || $v2->name == 'hargatotal') {
                                $result[$index][$v2->name] = filter_var($v2->value, FILTER_SANITIZE_NUMBER_INT);
                            } else {
                                $result[$index][$v2->name] = $v2->value;
                            }
                        }
                    }
                    if (!$insertrecord) {
                        array_pop($result);
                        continue;
                    }
                    $result[$index]['checkbox'] = '<input type="checkbox" id="checkbox-detail-'.$k.'" onchange="selectCheckbox(\'checkbox-detail-'.$k.'\')">';
                    $result[$index]['no'] = $no;

                    $allbtn = $condition->v ? '<a onclick="editContact({id: \''.$k.'\', update: false})"  class="btn btn-success btn-sm flat mr-2"><i class="fa fa-search icon-xs"></i> </a>' : '';
                    $allbtn .= $condition->u ? '<a onclick="editContact({id: \''.$k.'\', update: true})" class="btn btn-warning btn-sm flat mr-1"><i class="fa fa-edit icon-xs" style="text-align:center"></i> </a>': '';
                    $allbtn .= $condition->d ? '<a  onclick="removeContact({id: \''.$k.'\'})" class="btn btn-danger btn-sm flat mr-0"><i class="fa fa-trash icon-xs"></i> </a>' : '';
    
                    $result[$index]['action'] = '<div class="text-right button-action-contact button-action-contact-'.$k.'">
                        '.$allbtn.'
                    </div>';
                    // if (count($result) == $length || count($data) == $k+1) {
                    //     break;
                    // }
                    $index++;
                    // $qtyshow++;
                // }
                $no++;
            }
        }
        // dd($data);

        if ($orderdir == 'asc') {
            $result = collect($result)->sortBy($ordercolumn)->toArray();
        } else {
            $result = collect($result)->sortBy($ordercolumn)->reverse()->toArray();
        }

        $fixresult = [];
        $idx = 0;
        $numb = $start+1;
        foreach ($result as $k => $v) {
            if ($idx >= (int)$start && $qtyshow <= $length) {
                if ($idx < $start) {
                    continue;
                } else {
                }
                if (count($fixresult) == $length) {
                    break;
                }
                $v['no'] = $numb;
                $fixresult[] = $v;
                $qtyshow++;
                $numb++;
            }
            $idx++;
        }

        return response()->json([
            'data' => $fixresult,
            'draw' => intval($request['draw']),
            'recordsFiltered' => $search ? count($result) : count($data),
            'recordsTotal' => $search ? count($result) : count($data),
            'length' => $request->get('length')
        ]);
    }

    public function get_mcardprivilidge(Request $request) {
        $data = json_decode($request->get('data'));
        $statusdetail = json_decode($request->get('statusdetail'));
        $condition = json_decode($request->get('condition'));
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->get('search')['value'];
        $ordercolumn = $request->get('columns')[$request->get('order')[0]['column']]['data'];
        $orderdir = $request->get('order')[0]['dir'];


        $qtyshow = 0;
        $qtyall = 0;
        $no = 1;
        $index = 0;
        $result = [];
        $pattern = "/".$search."/i";
        // dd($start);

        foreach ($data as $k => $v) {
            if ($v != null) {
                // if ($k >= (int)$start && $qtyshow <= $length) {
                    $insertrecord = false;
                    foreach ($v as $k2 => $v2) {
                        if ($v2->name != 'idinventor') {
                            if (!array_key_exists('value', (array)$v2)) {
                                $v2->value = 0;
                            }
                            if (preg_match($pattern, $v2->value)) {
                                $insertrecord = true;
                            }
                            if ($v2->name == 'subtotal') {
                                $result[$index][$v2->name] = is_numeric($v2->value) ? 'Rp.'.number_format($v2->value) : $v2->value;
                            } else if ($v2->name == 'unitprice' || $v2->name == 'hargatotal') {
                                $result[$index][$v2->name] = filter_var($v2->value, FILTER_SANITIZE_NUMBER_INT);
                            } else if ($v2->name == 'idmenu') {
                                $result[$index][$v2->name] = $v2->value;
                            } else {
                                $result[$index][$v2->name] = '<input type="checkbox" '.($v2->value == 1 ? 'checked' : '').' onclick="return false;">';
                            }
                        }
                    }
                    if (!$insertrecord) {
                        array_pop($result);
                        continue;
                    }
                    $result[$index]['checkbox'] = '<input type="checkbox" id="checkbox-detail-'.$k.'" onchange="selectCheckbox(\'checkbox-detail-'.$k.'\')">';
                    $result[$index]['no'] = $no;

                    $allbtn = $condition->v ? '<a onclick="editPrivilidge({id: \''.$k.'\', update: false})"  class="btn btn-success btn-sm flat mr-2"><i class="fa fa-search icon-xs"></i> </a>' : '';
                    $allbtn .= $condition->u ? '<a onclick="editPrivilidge({id: \''.$k.'\', update: true})" class="btn btn-warning btn-sm flat mr-1"><i class="fa fa-edit icon-xs" style="text-align:center"></i> </a>': '';
                    $allbtn .= $condition->d ? '<a  onclick="removePrivilidge({id: \''.$k.'\'})" class="btn btn-danger btn-sm flat mr-0"><i class="fa fa-trash icon-xs"></i> </a>' : '';
    
                    $result[$index]['action'] = '<div class="text-right button-action-privilidge button-action-privilidge-'.$k.'">
                        '.$allbtn.'
                    </div>';
                    // if (count($result) == $length || count($data) == $k+1) {
                    //     break;
                    // }
                    $index++;
                    // $qtyshow++;
                // }
                $no++;
            }
        }
        // dd($data);

        if ($orderdir == 'asc') {
            $result = collect($result)->sortBy($ordercolumn)->toArray();
        } else {
            $result = collect($result)->sortBy($ordercolumn)->reverse()->toArray();
        }

        $fixresult = [];
        $idx = 0;
        $numb = $start+1;
        foreach ($result as $k => $v) {
            if ($idx >= (int)$start && $qtyshow <= $length) {
                if ($idx < $start) {
                    continue;
                } else {
                }
                if (count($fixresult) == $length) {
                    break;
                }
                $v['no'] = $numb;
                $fixresult[] = $v;
                $qtyshow++;
                $numb++;
            }
            $idx++;
        }

        return response()->json([
            'data' => $fixresult,
            'draw' => intval($request['draw']),
            'recordsFiltered' => $search ? count($result) : count($data),
            'recordsTotal' => $search ? count($result) : count($data),
            'length' => $request->get('length')
        ]);
    }

    public function get_select2ajax(Request $request){
        $table = $request->table;
        $idparent = @$request->idparent;
        $fieldparent = @$request->fieldparent;
        $search = $request->search;
        $page = $request->page;
        $limit = 25;
        $offset = ($page-1)*$limit;

        $where = [];
        if ($fieldparent) {
            $where[] = [$fieldparent,'=',$idparent];
        }

        $where[] = ['nama','like',"%$search%"];
        $datas = MCard::getData(['table'=>$table,'select'=>['noid','kode','nama'],'where'=>$where,'orderby'=>[['nourut','asc']],'offset'=>$offset,'limit'=>$limit,'isactive'=>true]);

        $results = [];
        foreach($datas as $k => $v){
            $results[] = array(
                'id' => $v->noid,
                'text' => $v->nama
            );
        }

        $more = count($results) == $limit ? true : false;

        $response = [
            'results' => $results,
            'pagination' => [
                'more' => $more
            ]
        ];
        return response()->json($response);
    }

    public function save(Request $request) {
        $req = $request->all();

        // DECLARE
        $form = [];
        $formgeneral = [];
        $formaddress = [];
        $formcontact = [];

        // FOREACH
        foreach ($req['formmcard'] as $k => $v) {
            $form[$v['name']] = $v['value'];
        }
        foreach ($req['formmcardgeneral'] as $k => $v) {
            $formgeneral[str_replace('mcardgeneral_', '', $v['name'])] = $v['value'];
        }
        $formgeneral['dobirth'] = MyHelper::humandateTodbdate($formgeneral['dobirth']);
        foreach ($req['formmcardaddress'] as $k => $v) {
            $formaddress[str_replace('mcardaddress_', '', $v['name'])] = $v['value'];
        }
        $formaddress['idlokasiprop'] = $formaddress['idlokasiprop_input'];
        unset($formaddress['idlokasiprop_input']);
        $formaddress['idlokasikab'] = $formaddress['idlokasikab_input'];
        unset($formaddress['idlokasikab_input']);
        $formaddress['idlokasikec'] = $formaddress['idlokasikec_input'];
        unset($formaddress['idlokasikec_input']);
        $formaddress['idlokasikel'] = $formaddress['idlokasikel_input'];
        unset($formaddress['idlokasikel_input']);
        if ($req['formmcardcontact'] != 'null') {
            foreach (json_decode($req['formmcardcontact']) as $k => $v) {
                $contact = [];
                foreach ($v as $k2 => $v2) {
                    $contact[$v2->name] = $v2->value;
                }
                unset($contact['idtypecontact_nama']);
                $formcontact[] = $contact;
            }
        }

        // CHECKBOX
        $form['isgroupuser'] = (array_key_exists('isgroupuser', $form)) ? 1 : 0;
        $form['isuser'] = (array_key_exists('isuser', $form)) ? 1 : 0;
        $form['isemployee'] = (array_key_exists('isemployee', $form)) ? 1 : 0;
        $form['iscustomer'] = (array_key_exists('iscustomer', $form)) ? 1 : 0;
        $form['issupplier'] = (array_key_exists('issupplier', $form)) ? 1 : 0;
        $form['is3rdparty'] = (array_key_exists('is3rdparty', $form)) ? 1 : 0;
        $form['isactive'] = (array_key_exists('isactive', $form)) ? 1 : 0;
        $formaddress['ismaincontact'] = (array_key_exists('ismaincontact', $formaddress)) ? 1 : 0;

        // REPLACE
        $formuser = [];
        $formprivilidge = [];
        $formemployee = [];
        $formcustomer = [];
        $formsupplier = [];
        $form3rdparty = [];

        if ($form['isuser']) {
            foreach ($req['formuser'] as $k => $v) {
                if (str_replace('mcarduser_', '', $v['name']) == 'mypassword') {
                    if (!is_null($v['value'])) {
                        $formuser[str_replace('mcarduser_', '', $v['name'])] = $v['value'];
                        $formuser['mypassword'] = sha1($formuser['mypassword']);
                    }
                } else {
                    $formuser[str_replace('mcarduser_', '', $v['name'])] = $v['value'];
                }
            }

            if (array_key_exists('formmcardprivilidge', $req)) {
                foreach (json_decode($req['formmcardprivilidge']) as $k => $v) {
                    $privilidge = [];
                    foreach ($v as $k2 => $v2) {
                        if ($v2->name == 'noid' ||
                            $v2->name == 'noidrecent' ||
                            $v2->name == 'idmenu') {
                            $privilidge[$v2->name] = $v2->value;
                        } else {
                            $privilidge[$v2->name] = $v2->value ? 1 : 0;
                        }
                    }
                    unset($privilidge['idmenu_menucaption']);
                    $formprivilidge[] = $privilidge;
                }
            }
        }
        if ($form['isemployee']) {
            foreach ($req['formemployee'] as $k => $v) {
                $formemployee[str_replace('mce_', '', $v['name'])] = $v['value'];
            }
            $formemployee['idlokasiprop'] = $formemployee['idlokasiprop_input'];
            unset($formemployee['idlokasiprop_input']);
            $formemployee['idlokasikota'] = $formemployee['idlokasikota_input'];
            unset($formemployee['idlokasikota_input']);
            $formemployee['idlokasikec'] = $formemployee['idlokasikec_input'];
            unset($formemployee['idlokasikec_input']);
            $formemployee['idlokasikel'] = $formemployee['idlokasikel_input'];
            unset($formemployee['idlokasikel_input']);
        }
        if ($form['iscustomer']) {
            foreach ($req['formcustomer'] as $k => $v) {
                $formcustomer[str_replace('mcc_', '', $v['name'])] = $v['value'];
            }
            $formcustomer['idlokasiprop'] = $formcustomer['idlokasiprop_input'];
            unset($formcustomer['idlokasiprop_input']);
            $formcustomer['idlokasikota'] = $formcustomer['idlokasikota_input'];
            unset($formcustomer['idlokasikota_input']);
            $formcustomer['idlokasikec'] = $formcustomer['idlokasikec_input'];
            unset($formcustomer['idlokasikec_input']);
            $formcustomer['idlokasikel'] = $formcustomer['idlokasikel_input'];
            unset($formcustomer['idlokasikel_input']);
        }
        if ($form['issupplier']) {
            foreach ($req['formsupplier'] as $k => $v) {
                $formsupplier[str_replace('mcs_', '', $v['name'])] = $v['value'];
            }
            $formsupplier['idlokasiprop'] = $formsupplier['idlokasiprop_input'];
            unset($formsupplier['idlokasiprop_input']);
            $formsupplier['idlokasikota'] = $formsupplier['idlokasikota_input'];
            unset($formsupplier['idlokasikota_input']);
            $formsupplier['idlokasikec'] = $formsupplier['idlokasikec_input'];
            unset($formsupplier['idlokasikec_input']);
            $formsupplier['idlokasikel'] = $formsupplier['idlokasikel_input'];
            unset($formsupplier['idlokasikel_input']);
        }
        if ($form['is3rdparty']) {
            foreach ($req['form3rdparty'] as $k => $v) {
                $form3rdparty[str_replace('mc3_', '', $v['name'])] = $v['value'];
            }
        }

        // CREATE OR UPDATE
        if ($req['statusadd'] == 'true') {
            $form['noid'] = MCard::getNextNoid(static::$main['table']);
            $form['idcreate'] = Session::get('noid');
            $form['docreate'] = date('Y-m-d H:i:s');

            if ($formgeneral) {
                $formgeneral['noid'] = $form['noid'];
                $formgeneral['idcreate'] = Session::get('noid');
                $formgeneral['docreate'] = date('Y-m-d H:i:s');
            }
            if ($formaddress) {
                $formaddress['noid'] = $form['noid'];
                $formaddress['idcreate'] = Session::get('noid');
                $formaddress['docreate'] = date('Y-m-d H:i:s');
            }

            $nextnoidcontact = MCard::getNextNoid('mcardcontact');
            foreach ($formcontact as $k => $v) {
                $formcontact[$k]['noid'] = $nextnoidcontact;
                $formcontact[$k]['idcard'] = $form['noid'];
                $formcontact[$k]['idcreate'] = Session::get('noid');
                $formcontact[$k]['docreate'] = date('Y-m-d H:i:s');
                unset($formcontact[$k]['noidrecent']);
                $nextnoidcontact++;
            }

            if ($formuser) {
                $formuser['noid'] = $form['noid'];
                $formuser['idcreate'] = Session::get('noid');
                $formuser['docreate'] = date('Y-m-d H:i:s');
            }

            $nextnoidprivilidge = MCard::getNextNoid('mcardprivilidge');
            foreach ($formprivilidge as $k => $v) {
                $formprivilidge[$k]['noid'] = $nextnoidprivilidge;
                $formprivilidge[$k]['idcard'] = $form['noid'];
                $formprivilidge[$k]['idcreate'] = Session::get('noid');
                $formprivilidge[$k]['docreate'] = date('Y-m-d H:i:s');
                $nextnoidprivilidge++;
            }

            if ($formemployee) {
                $formemployee['noid'] = $form['noid'];
                $formemployee['idcreate'] = Session::get('noid');
                $formemployee['docreate'] = date('Y-m-d H:i:s');
            }
            if ($formcustomer) {
                $formcustomer['noid'] = $form['noid'];
                $formcustomer['idcreate'] = Session::get('noid');
                $formcustomer['docreate'] = date('Y-m-d H:i:s');
            }
            if ($formsupplier) {
                $formsupplier['noid'] = $form['noid'];
                $formsupplier['idcreate'] = Session::get('noid');
                $formsupplier['docreate'] = date('Y-m-d H:i:s');
            }
            if ($form3rdparty) {
                $form3rdparty['noid'] = $form['noid'];
                $form3rdparty['idcreate'] = Session::get('noid');
                $form3rdparty['docreate'] = date('Y-m-d H:i:s');
            }

            $result = MCard::saveData([
                'form' => $form,
                'formgeneral' => $formgeneral,
                'formaddress' => $formaddress,
                'formcontact' => $formcontact,
                'formuser' => $formuser,
                'formprivilidge' => $formprivilidge,
                'formemployee' => $formemployee,
                'formcustomer' => $formcustomer,
                'formsupplier' => $formsupplier,
                'form3rdparty' => $form3rdparty,
            ]);
        } else {
            $form['idupdate'] = Session::get('noid');
            $form['lastupdate'] = date('Y-m-d H:i:s');

            if ($formgeneral) {
                $formgeneral['noid'] = $req['noid'];
                $formgeneral['idupdate'] = Session::get('noid');
                $formgeneral['lastupdate'] = date('Y-m-d H:i:s');
                $formgeneral['idgender'] = is_null($formgeneral['idgender']) ? 0 : $formgeneral['idgender'];
                $formgeneral['idreligion'] = is_null($formgeneral['idreligion']) ? 0 : $formgeneral['idreligion'];
                $formgeneral['idmarital'] = is_null($formgeneral['idmarital']) ? 0 : $formgeneral['idgender'];
                $formgeneral['idnationality'] = is_null($formgeneral['idnationality']) ? 0 : $formgeneral['idnationality'];
                $formgeneral['idrace'] = is_null($formgeneral['idrace']) ? 0 : $formgeneral['idrace'];
                $formgeneral['idbloodtype'] = is_null($formgeneral['idbloodtype']) ? 0 : $formgeneral['idbloodtype'];
                $formgeneral['idlanguage1'] = is_null($formgeneral['idlanguage1']) ? 0 : $formgeneral['idlanguage1'];
                $formgeneral['idlanguage2'] = is_null($formgeneral['idlanguage2']) ? 0 : $formgeneral['idlanguage2'];
                $formgeneral['idlokasilahir'] = is_null($formgeneral['idlokasilahir']) ? 0 : $formgeneral['idlokasilahir'];
            }
            if ($formaddress) {
                $formaddress['noid'] = $req['noid'];
                $formaddress['idupdate'] = Session::get('noid');
                $formaddress['lastupdate'] = date('Y-m-d H:i:s');
            }

            $fcontact = [];
            $nextnoidcontact = MCard::getNextNoid('mcardcontact');
            foreach ($formcontact as $k => $v) {
                unset($formcontact[$k]['idtypecontact_nama']);
                $formcontact[$k]['idcard'] = $req['noid'];

                if (array_key_exists('noidrecent', $v)) {
                    $formcontact[$k]['noid'] = $v['noidrecent'];
                    unset($formcontact[$k]['noidrecent']);
                    $formcontact[$k]['idupdate'] = Session::get('noid');
                    $formcontact[$k]['lastupdate'] = date('Y-m-d H:i:s');
                    $fcontact['update'][] = $formcontact[$k];
                } else {
                    $formcontact[$k]['noid'] = $nextnoidcontact;
                    $nextnoidcontact++;
                    $formcontact[$k]['idcreate'] = Session::get('noid');
                    $formcontact[$k]['docreate'] = date('Y-m-d H:i:s');
                    $fcontact['insert'][] = $formcontact[$k];
                }
            }

            if ($formuser) {
                $formuser['noid'] = $req['noid'];
                $formuser['idupdate'] = Session::get('noid');
                $formuser['lastupdate'] = date('Y-m-d H:i:s');
            }

            $fprivilidge = [];
            $nextnoidprivilidge = MCard::getNextNoid('mcardprivilidge');
            foreach ($formprivilidge as $k => $v) {
                $formprivilidge[$k]['idcard'] = $req['noid'];

                if (array_key_exists('noidrecent', $v)) {
                    $formprivilidge[$k]['noid'] = $v['noidrecent'];
                    unset($formprivilidge[$k]['noidrecent']);
                    $formprivilidge[$k]['idupdate'] = Session::get('noid');
                    $formprivilidge[$k]['lastupdate'] = date('Y-m-d H:i:s');
                    $fprivilidge['update'][] = $formprivilidge[$k];
                } else {
                    $formprivilidge[$k]['noid'] = $nextnoidprivilidge;
                    $nextnoidprivilidge++;
                    $formprivilidge[$k]['idcreate'] = Session::get('noid');
                    $formprivilidge[$k]['docreate'] = date('Y-m-d H:i:s');
                    $fprivilidge['insert'][] = $formprivilidge[$k];
                }
            }

            if ($formemployee) {
                $formemployee['noid'] = $req['noid'];
                $formemployee['idupdate'] = Session::get('noid');
                $formemployee['lastupdate'] = date('Y-m-d H:i:s');
            }
            if ($formcustomer) {
                $formcustomer['noid'] = $req['noid'];
                $formcustomer['idupdate'] = Session::get('noid');
                $formcustomer['lastupdate'] = date('Y-m-d H:i:s');
            }
            if ($formsupplier) {
                $formsupplier['noid'] = $req['noid'];
                $formsupplier['idupdate'] = Session::get('noid');
                $formsupplier['lastupdate'] = date('Y-m-d H:i:s');
            }
            if ($form3rdparty) {
                $form3rdparty['noid'] = $req['noid'];
                $form3rdparty['idupdate'] = Session::get('noid');
                $form3rdparty['lastupdate'] = date('Y-m-d H:i:s');
            }

            $result = MCard::updateData([
                'form' => $form, 
                'formgeneral' => $formgeneral,
                'formaddress' => $formaddress,
                'formcontact' => $fcontact,
                'formuser' => $formuser,
                'formprivilidge' => $fprivilidge,
                'formemployee' => $formemployee,
                'formcustomer' => $formcustomer,
                'formsupplier' => $formsupplier,
                'form3rdparty' => $form3rdparty,
                'noid' => $req['noid']
            ]);
        }

        return ['success' => $result['success'], 'message' => $result['message']];
    }

    public function find(Request $request) {
        $noid = $request->get('noid');

        // FIND
        $result = MCard::findData($noid);
        if (!$result) return ['success' => $result['success'], 'message' => $result['message']];
        $result['data']['find']->profilepicturepreview = asset('assets/images/profilepicture/'.$result['data']['find']->profilepicture);
        $result['data']['find']->profilepicturefile = $result['data']['find']->profilepicture;

        // CHECK EXIST
        if (is_null($result['data']['find']->profilepicture)) {
            $result['data']['find']->profilepicturepreview = asset('assets/default-person.png');
        } else {
            $fileexist = file_exists('assets/images/profilepicture/'.$result['data']['find']->profilepicture);
            if (!$fileexist) $result['data']['find']->profilepicturepreview = asset('assets/default-person.png');
        }

        // CONVERT DATE
        if ($result['data']['findgeneral']) {
            $result['data']['findgeneral']->dobirth = MyHelper::dbdateTohumandate($result['data']['findgeneral']->dobirth);
        }


        return [
            'success' => true, 
            'message' => 'Success Find Data', 
            'data' => [
                'find' => $result['data']['find'],
                'findgeneral' => $result['data']['findgeneral'],
                'findaddress' => $result['data']['findaddress'],
                'findcontact' => $result['data']['findcontact'],
                'finduser' => $result['data']['finduser'],
                'findprivilidge' => $result['data']['findprivilidge'],
                'findemployee' => $result['data']['findemployee'],
                'findcustomer' => $result['data']['findcustomer'],
                'findsupplier' => $result['data']['findsupplier'],
                'find3rdparty' => $result['data']['find3rdparty'],
            ]
        ];
    }
    
    public function generate_code(Request $request) {
        $noid = $request->get('noid');
        $code = MCard::getNextNoid(static::$main['table']);


        return response()->json([
            'success' => true,
            'message' => 'Find Successfully',
            'data' => $code
        ]);
    }

    public function get_info() {
        return response()->json([
            'success' => true,
            'message' => 'Get Info Successfully',
            'data' => [
                'defaultprofilepicture' => asset('assets/default-person.png')
            ]
        ]);
    }

    public function delete_select_m(Request $request) {
        return MCard::deleteSelectM([
            'selecteds' => $request->get('selecteds'), 
            'selectcheckboxmall' => $request->get('selectcheckboxmall'), 
            'datalog' => null
        ]);
    }

    public function delete(Request $request) {
        // FIND
        $find = MCard::findData($request->get('noid'));
        if (!$find) return ['success' => $find['success'], 'message' => $find['message']];

        // CHECK EXIST
        $filelocation = 'assets/images/profilepicture/';
        $filename = $find['data']['find']->profilepicture;

        if (!is_null($filename)) {
            $fileexist = file_exists($filelocation.$filename);
            
            // DELETE EXIST
            if ($fileexist) {
                $filedelete = unlink($filelocation.$filename);
                if (!$filedelete) {
                    return response()->json(['success' => false, 'message' => 'Cannot delete file!']);
                }
            }
        }

        // DELETE IN DB
        $result = MCard::deleteData(['noid' => $request->get('noid')]);
        if (!$result) return ['success' => $result['success'], 'message' => $result['message']];

        return ['success' => $result['success'], 'message' => $result['message']];
    }

    public function upload_image(Request $request) {
        $file = $request->file('file');
        $noid = $request->get('noid');
        $code = $request->get('code');
        $profilepicture = $request->get('profilepicture');
        $filelocation = 'assets/images/profilepicture/';
        $fileextension = $file->extension();
        $filesize = $file->getSize();
        $datenow = date('YmdHis');
        $filename = "$code-$datenow.$fileextension";
        $fileexist = file_exists($filelocation.$profilepicture);

        // CHECK EXTENTION
        if ($fileextension != 'jpg' &&
            $fileextension != 'jpeg' &&
            $fileextension != 'png') {
                return response()->json(['success' => false, 'message' => 'Extension must be JPG/JPEG/PNG']);
        }

        // CHECK SIZE
        if ($filesize > 2000000) {
            return response()->json(['success' => false, 'message' => 'Size must < 2MB']);
        }
        

        // CHECK NULL
        if (is_null($profilepicture) || !$fileexist) {

            // UPLOAD NEW
            $upload = Image::make($file->path())->save($filelocation.$filename);
            if (!$upload) {
                return response()->json(['success' => false, 'message' => 'Error Upload!']);
            }
        } else {
            // UPLOAD UPDATE EXIST
            $upload = Image::make($file->path())->save($filelocation.$filename);
            if (!$upload) {
                return response()->json(['success' => false, 'message' => 'Error Upload!']);
            }
            
            // DELETE EXIST
            $filedelete = unlink($filelocation.$profilepicture);
            if (!$filedelete) {
                return response()->json(['success' => false, 'message' => 'Cannot delete file!']);
            }
        }

        // SAVE PROFILEPICTURE IN DB
        $save = MCard::saveProfilePicture(['noid' => $noid, 'profilepicture' => $filename]);
        if (!$save) return ['success' => $save['success'], 'message' => $save['message']];

        return response()->json([
            'success' => true, 
            'message' => 'Success Upload.', 
            'data' => [
                'directory' => asset('assets/images/profilepicture/'),
                'file' => $filename
            ]
        ]);
    }
}