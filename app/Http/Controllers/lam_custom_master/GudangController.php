<?php

namespace App\Http\Controllers\lam_custom_master;

use App\Http\Controllers\Controller;
use App\Model\lam_custom_master\Gudang;
use App\Model\RA\RAP as MP2;
use App\Model\Sales\SalesOrder;
use App\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use DB;
use Image;
use Storage;

class GudangController extends Controller {

    public $color = [
        'white' => '#FFFFFF',
        'default' => '#E1E5EC',
        'dark' => '#2F353B',
        'blue' => '#3598DC',
        'blue-madison' => '#578EBE',
        'blue-chambray' => '#2C3E50',
        'blue-ebonyclay' => '#22313F',
        'blue-hoki' => '#67809F',
        'blue-steel' => '#4B77BE',
        'blue-soft' => '#4C87B9',
        'blue-dark' => '#5E738B',
        'blue-sharp' => '#5C9BD1',
        'blue-oleo' => '#94A0B2',
        'green' => '#32C5D2',
        'green-meadow' => '#1BBC9B',
        'green-seagreen' => '#1BA39C',
        'green-torquoise' => '#36D7B7',
        'green-haze' => '#44B6AE',
        'green-jungle' => '#26C281',
        'green-soft' => '#3FABA4',
        'green-dark' => '#4DB3A2',
        'green-sharp' => '#2AB4C0',
        'green-steel' => '#29B4B6',
        'grey' => '#E5E5E5',
        'grey-steel' => '#E9EDEF',
        'grey-cararra' => '#FAFAFA',
        'grey-gallery' => '#555555',
        'grey-cascade' => '#95A5A6',
        'grey-silver' => '#BFBFBF',
        'grey-salsa' => '#ACB5C3',
        'grey-salt' => '#BFCAD1',
        'grey-mint' => '#525E64',
        'red' => '#E7505A',
        'red-pink' => '#E08283',
        'red-sunglo' => '#E26A6A',
        'red-intense' => '#E35B5A',
        'red-thunderbird' => '#D91E18',
        'red-flamingo' => '#EF4836',
        'red-soft' => '#D05454',
        'red-haze' => '#F36A5A',
        'red-mint' => '#E43A45',
        'yellow' => '#C49F47',
        'yellow-gold' => '#E87E04',
        'yellow-casablanca' => '#F2784B',
        'yellow-crusta' => '#F3C200',
        'yellow-lemon' => '#F7CA18',
        'yellow-saffron' => '#F4D03F',
        'yellow-soft' => '#C8D046',
        'yellow-haze' => '#C5BF66',
        'yellow-mint' => '#C5B96B',
        'purple' => '#8E44AD',
        'purple-plum' => '#8775A7',
        'purple-medium' => '#BF55EC',
        'purple-studio' => '#8E44AD',
        'purple-wisteria' => '#9B59B6',
        'purple-seance' => '#9A12B3',
        'purple-intense' => '#8775A7',
        'purple-sharp' => '#796799',
        'purple-soft' => '#8877A9',
    ];
    public static $main = [
        'table' => 'genmgudang',
        'tablemilestone' => 'prjmrabmilestone',
        'tableheader' => 'prjmrabheader',
        'tabledetail' => 'prjmrabdetail',
        'tablecdf' => 'prjmrabdocument',
        'table-title' => 'Master Gudang',
        'table2' => 'prjmrap',
        'tabledetail2' => 'prjmrapdetail',
        'table2-title' => 'RAP',
        'generatecode' => 'generatecode/602/1',
        'generatecode2' => 'generatecode/604/1',
        'idtypetranc' => 602,
        'idtypetranc2' => 602,
        'noid' => 20091104,
        'noidnew' => 50011101,
        'printcode' => 'print_purchase_request',
        'condition' => [
            'mttts' => [
                [],
                [
                    'mttts' => 'draft',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => true,
                        'd' => true,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => true,
                        'd' => true,
                    ],
                    'draft' => false,
                    'issued' => true,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => false,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'issued',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => true,
                    'issued' => false,
                    'verified' => true,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => false,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'verified',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => true,
                    'verified' => false,
                    'approved' => true,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => false,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'approved',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'pending',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'canceled',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'finished',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
            ],
        ]
    ];

    public static function index() {
        $tabletitle = static::$main['table-title'];
        $breadcrumb = Menu::getBreadcrumb(static::$main['noid']);
        $pagenoid = static::$main['noid'];
        $idtypepage = static::$main['noid'];
        $color = (new static)->color;
        $maintable = static::$main['table'];
        $datefilter = [
            'a' => 'All',
            't' => date('M d, Y').' - '.date('M d, Y'),
            'y' => date('M d, Y', strtotime('-1 day')).' - '.date('M d, Y', strtotime('-1 day')),
            'l7d' => date('M d, Y').' - '.date('M d, Y', strtotime('-7 day')),
            'l30d' => date('M d, Y').' - '.date('M d, Y', strtotime('1 month')),
            'tm' => date('M d, Y', strtotime('first day of this month')).' - '.date('M d, Y', strtotime('last day of this month')),
            'lm' => date('M d, Y', strtotime('first day of previous month')).' - '.date('M d, Y', strtotime('last day of previous month')),
        ];
        $datefilterdefault = 'a';
        $genmlokasi = Gudang::getData(['table'=>'genmlokasi','select'=>['noid','kode','nama'],'orderby'=>[['noid','asc']],'isactive'=>true]);
        $mcard = Gudang::getData(['table'=>'mcard','select'=>['noid','kode','nama','isemployee'],'orderby'=>[['noid','asc']],'isactive'=>true]);

        return view('lam_custom_master.'.static::$main['table'].'.index', compact(
            'tabletitle',
            'breadcrumb',
            'pagenoid',
            'idtypepage',
            'color',
            'maintable',
            'datefilter',
            'datefilterdefault',
            'genmlokasi',
            'mcard'
        ));
    }
    
    public function get(Request $request) {
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->get('search')['value'];
        $orderbycolumn = $request->get('columns')[$request->get('order')[0]['column']]['data'];
        if ($orderbycolumn == 'checkbox' || $orderbycolumn == 'no') {
            $orderbycolumn = 'noid';
        }
        $orderby = [
            'column' => $orderbycolumn,
            'sort' => $request->get('order')[0]['dir']
        ];
        $filter = $request->get('filter');
        $datefilter = $request->get('datefilter');
        $datefilter2 = $request->get('datefilter2');

        $result = Gudang::getMaster($start, $length, $search, $orderby, $filter, $datefilter, $datefilter2);
        $data = $result['data'];
        $allnoid = Gudang::getAllNoid(static::$main['table']);

        $resulttotal = count(Gudang::getMaster(null, null, $search, $orderby, $filter, $datefilter, $datefilter2)['data']);

        $resultsnst = Gudang::getMTypeTrancTypeStatus(static::$main['idtypetranc']);
        foreach ($resultsnst as $k => $v) {
            $snst[$v->nourut] = $v;
            if($v->isinternal == 0 && $v->isexternal != 0) {
                $sapcf[] = $v;
            }
        }

        $conditions = Session::get(static::$main['noid'].'-conditions');
        $no = $start+1;
        foreach ($data as $k => $v) {
            $checkbox['checkbox'] = '<input type="checkbox" value="FALSE" class="checkbox-m" id="checkbox-m-'.$v->noid.'" onchange="selectCheckboxM('.$v->noid.')">';
            $_no['no'] = $no;
            $array = (array)$data[$k];

            $array['isactive'] = '<input type="checkbox" '.($array['isactive'] == 1 ? 'checked' : '').' onclick="return false;">';

            $btnview = '<button id="btn-view-data-'.$v->noid.'" onclick="viewData('.$v->noid.')" class="btn btn-info btn-sm flat" data-toggle="tooltip" data-theme="dark" title="View"><i class="fa fa-eye icon-xs"></i> </button>';
            $btnedit = '<button id="btn-edit-data-'.$v->noid.'" onclick="editData('.$v->noid.')" class="btn btn-warning btn-sm flat" data-toggle="tooltip" data-theme="dark" title="Edit"><i class="fa fa-edit icon-xs" style="text-align:center"></i> </button>';
            $btndelete = '<button id="btn-delete-data-'.$v->noid.'" onclick="showModalDelete('.$v->noid.')" class="btn btn-danger btn-sm flat" data-toggle="tooltip" data-theme="dark" title="Delete"><i class="fa fa-trash icon-xs"></i> </button>';

            $allbtn = @$btnprev;
            $allbtn .= @$btnnext;
            $allbtn .= @$btnsapcf;
            $allbtn .= @$btnprint;
            $allbtn .= @$btnlog;
            $allbtn .= @$btnview;
            $allbtn .= @$btnedit;
            $allbtn .= @$btndelete;

            $action = [
                'action' => '<div class="d-flex align-items-center pull-right button-action button-action-'.$k.'">
                    '.$allbtn.'
                </div>'
            ];
            $data[$k] = (object)array_merge($checkbox, $_no, $array, $action);
            $no++;
        }
        
        return response()->json([
            'data' => $data,
            'draw' => intval($request['draw']),
            'recordsFiltered' => $resulttotal,
            'recordsTotal' => $resulttotal,
            'allnoid' => $allnoid,
            'datefilter' => $result['datefilter']
        ]);
    }

   public function get_select2ajax(Request $request){
        $table = $request->table;
        $idparent = @$request->idparent;
        $fieldparent = @$request->fieldparent;
        $search = $request->search;
        $page = $request->page;
        $limit = 25;
        $offset = ($page-1)*$limit;

        $where = [];
        if ($fieldparent) {
            $where[] = [$fieldparent,'=',$idparent];
        }

        // $where[] = ['nama','like',"%$search%"];
        // if (is_null($idparent)) {
        //     $datas = Inventory::getData(['table'=>$table,'select'=>['noid','kode','nama'],'orderby'=>[['nourut','asc']],'offset'=>$offset,'limit'=>$limit,'isactive'=>true]);
        // } else {
            $where[] = ['nama','like',"%$search%"];
            $datas = Gudang::getData(['table'=>$table,'select'=>['noid','kode','nama'],'where'=>$where,'orderby'=>[['nourut','asc']],'offset'=>$offset,'limit'=>$limit,'isactive'=>true]);
        // }

        $results = [];
        foreach($datas as $k => $v){
            $results[] = array(
                'id' => $v->noid,
                'text' => $v->nama
            );
        }

        $more = count($results) == $limit ? true : false;

        $response = [
            'results' => $results,
            'pagination' => [
                'more' => $more
            ]
        ];
        return response()->json($response);
    }

    public function save(Request $request) {
        $req = $request->all();

        $form = [];
        foreach ($req['form'] as $k => $v) {
            $form[$v['name']] = $v['value'];
        }
        $form['idlokasiprop'] = $form['idlokasiprop_input'];
        unset($form['idlokasiprop_input']);
        $form['idlokasikab'] = $form['idlokasikab_input'];
        unset($form['idlokasikab_input']);
        $form['idlokasikec'] = $form['idlokasikec_input'];
        unset($form['idlokasikec_input']);
        $form['idlokasikel'] = $form['idlokasikel_input'];
        unset($form['idlokasikel_input']);

        $form['isactive'] = (array_key_exists('isactive', $form) && $form['isactive'] == 'on') ? 1 : 0;

        if ($req['statusadd'] == 'true') {
            $form['noid'] = Gudang::getNextNoid(static::$main['table']);
            $form['idcreate'] = Session::get('noid');
            $form['docreate'] = date('Y-m-d H:i:s');

            $result = Gudang::saveData(['form' => $form]);
        } else {
            $form['idupdate'] = Session::get('noid');
            $form['lastupdate'] = date('Y-m-d H:i:s');
            $result = Gudang::updateData(['form' => $form, 'noid' => $req['noid']]);
        }

        return ['success' => $result['success'], 'message' => $result['message']];
    }

    public function find(Request $request) {
        $noid = $request->get('noid');

        $result = Gudang::findData($noid);
        if (!$result) return ['success' => $result['success'], 'message' => $result['message']];

        return ['success' => true, 'message' => 'Success Find Data', 'data' => $result['data']];
    }
    
    public function generate_code(Request $request) {
        $noid = $request->get('noid');
        $code = Gudang::getNextNoid(static::$main['table']);


        return response()->json([
            'success' => true,
            'message' => 'Find Successfully',
            'data' => $code
        ]);
    }

    public function delete_select_m(Request $request) {
        return Gudang::deleteSelectM([
            'selecteds' => $request->get('selecteds'), 
            'selectcheckboxmall' => $request->get('selectcheckboxmall'), 
            'datalog' => null
        ]);
    }

    public function delete(Request $request) {
        return Gudang::deleteData(['noid' => $request->get('noid')]);
    }
}