<?php

namespace App\Http\Controllers\lam_project;

use App\Http\Controllers\Controller;
Use DB;
use App\User;
use App\Menu;
use App\Page;
use App\Panel;
use App\Widget;
use App\WidgetGrid;
use App\WidgetGridField;
use App\Departement;
use App\Company;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Schema;
use Carbon;

class RABController extends Controller
{
    public static function RABEntryPage($slug)
    {
      $menu = Menu::where('noid',$slug)->first();
      $page = Page::where('noid',$menu->linkidpage)->first();
      $idhomepagelink = $slug;
      $user = User::where('noid', Session::get('noid'))->first();
      $widget = Widget::where('kode', $slug)->first();
        $public = $menu->ispublic;
        $idhomepage =  $user->idhomepage;
        $noid = Session::get('noid');
        $myusername = Session::get('myusername');
          $panel = Panel::where('idpage',$page->noid)->first();
          $page_title = $page->pagetitle;
          $page_description = $page->pagetitle;

          return view('lam_project.RABEntryPage', compact(
            'noid',
            'public',
            'panel',
            'myusername',
            'idhomepagelink',
            'page',
            'page_title',
            'idhomepage',
            'menu',
            'namatable',
             'page_description'));


    }
}
