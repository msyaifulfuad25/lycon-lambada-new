<?php

namespace App\Http\Controllers;

use App\Http\Controllers\lam_error\ErrorController;
use App\Http\Controllers\purchase\PurchaseRequestController;
Use DB;
use App\User;
use App\Menu;
use App\Page;
use App\Panel;
use App\Widget;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;
use PDF;
use App\Helpers\MyHelper;

// Frontend
use App\Http\Controllers\frontend\ApplicantController;

class HomeController extends Controller {

    public static $main = [
        'direktur' => 'L0106001',
        'finance' => 'L0412004',
        'purchasing' => 'L1709016',
    ];

    public static function home($noid) {
        // APPLICANT
        if ($noid == 'applicant') {
            return view('frontend.applicant.index');
        // } else if ($noid == 'applicant/save') {
        //     return ApplicantController::save();
        }

        if(!Session::get('login')){
            return view('pages.login');
        }

        // if ($noid == 40021102) {
        //     return PurchaseRequestController::index();
        // }

        $menu = Menu::where('noid',$noid)->orWhere('urlmasking', $noid)->first();
        if ($menu == null) {
            return ErrorController::error404($noid);
        }
        $page = Page::where('noid',$menu->linkidpage)->first();
        if ($page == null) {
            return ErrorController::pageNull($noid);
        } else {
            Session::put('idmenu', \Request::segment(1));
            if ($page->idtypepage == 9) {
                // halamancustom
                return CustomController::index($noid);
            } else if ((int)$page->idtypepage == 1) {
                // Halaman System
                // dd('ok');
                return SystemController::index($noid);
            } else if ((int)$page->idtypepage == 2) {
                // Halaman Dashboard
                return DashboardController::index($noid);
            } else if ((int)$page->idtypepage == 3) {
                // Halaman Data Master (Tabel Biasa)
                return MasterTabelController::index($noid);
            } else if ((int)$page->idtypepage == 4) {
                // Halaman Data Master (Tabel Treelist)
                return MasterTreelistController::index($noid);
            } else if ((int)$page->idtypepage == 5) {
                //  Halaman Transaksi Master Detail (1 Tabel Master + 1 Tabel Detail)
                return TransaksiMasterDetailController::index($noid);
            } else if ((int)$page->idtypepage == 6) {
                //   Halaman Transaksi Master Detail (1 Tabel Master + lebih dari 1 Tabel Detail)
                return TransaksiMasterMultiDetailController::index($noid);
            } else if ((int)$page->idtypepage == 7) {
                //   Halaman Reporting Standard
                return ReportingStandardController::index($noid);
            } else if ((int)$page->idtypepage == 8) {
                //  Halaman Reporting Non Standard
                return ReportingNonStandardController::index($noid);
            } else {
                // Other
                return OtherController::index($noid);
            }
        }
    }

    public function print_3($noid, $urlprint) {
        $noidrptmreport = 25101;
        $tabledetail = ['fincashbankdetail', 'fincashdisbursmentdetail'];
        $showdatapertable = 10;
        $urlprint = explode('/', base64_decode($urlprint));
        $template = DB::connection('mysql2')->table('rptmreport')->where('noid', $noidrptmreport)->first();
        
        $tablemaster = DB::table('cmswidget')->select('maintable')->where('kode', $noid)->orderBy('noid', 'ASC')->first()->maintable;
        $resultmaster = DB::connection('mysql2')->table($tablemaster)->where('noid', $urlprint[1])->orderBy('noid', 'ASC')->first();
        $rd = DB::connection('mysql2')->table($tabledetail[0])->where('idmaster', $resultmaster->noid)->orderBy('noid', 'ASC')->get();
        $rd2 = DB::connection('mysql2')->table($tabledetail[1])->where('idmaster', $resultmaster->noid)->orderBy('noid', 'ASC')->get();
        $resultdetail = (count($rd) >= count($rd2)) ? $rd : $rd2;
        $resultdetail2 = (count($rd) >= count($rd2)) ? $rd2 : $rd;
        $resultrptmreportdetail = DB::connection('mysql2')->table('rptmreportdetail')->where('idreport', $noidrptmreport)->orderBy('nourut', 'ASC')->get();        
        $tablemaster_kode = $tablemaster.'_kode';
        $tablemaster_revno = $tablemaster.'_revno';
        $resultheader = DB::connection('mysql2')->select("SELECT $tablemaster.kode AS $tablemaster_kode, $tablemaster.revno AS $tablemaster_revno, genmcompany.nama AS genmcompany_nama FROM $tablemaster JOIN genmcompany ON $tablemaster.idcompany = genmcompany.noid WHERE $tablemaster.noid=$urlprint[1] LIMIT 1")[0];

        $rmrd = [];
        foreach ($resultrptmreportdetail as $k => $v) {
            if ($v->nama == 'First Header') {
                if ($v->isshowfirstpage) {
                    $rmrd['fh'] = '<div class="fh">'.$template->reportheader.'</div>';
                } else {
                    $rmrd['fh'] = '<div class="fh">&nbsp;</div>';
                }
                $rmrd['marginfh'] = '0px 0px 0px 0px';
                $rmrd['heightfh'] = $v->widgetheight;
            } else if ($v->nama == 'Odd Header') {
                if ($v->isshoweverypage) {
                    $rmrd['oh'] = '<div class="oh">'.$template->reportheader.'</div>';
                } else {
                    $rmrd['oh'] = '<div class="oh">&nbsp;</div>';
                }
                $rmrd['marginoh'] = '0px 0px 0px 0px';
                $rmrd['heightoh'] = $v->widgetheight;
            } else if ($v->nama == 'Even Header') {
                if ($v->isshoweverypage) {
                    $rmrd['eh'] = '<div class="eh">'.$template->reportheader.'</div>';
                } else {
                    $rmrd['eh'] = '<div class="eh">&nbsp;</div>';
                }
                $rmrd['margineh'] = '0px 0px 0px 0px';
                $rmrd['heighteh'] = $v->widgetheight;
            } else if ($v->nama == 'Detail') {
                $rmrd['hd'] = $v->widgetheight;
            } else if ($v->nama == 'Odd Footer') {
                if ($v->isshoweverypage) {
                    $rmrd['of'] = '<div class="of">'.$template->reportfooter.'</div>';
                } else {
                    $rmrd['of'] = '<div class="of">&nbsp;</div>';
                }
                $rmrd['marginof'] = '0px 0px 65px 0px';
                $rmrd['heightof'] = $v->widgetheight;
            } else if ($v->nama == 'Even Footer') {
                if ($v->isshoweverypage) {
                    $rmrd['ef'] = '<div class="ef">'.$template->reportfooter.'</div>';
                } else {
                    $rmrd['ef'] = '<div class="ef">&nbsp;</div>';
                }
                $rmrd['marginef'] = '0px 0px 65px 0px';
                $rmrd['heightef'] = $v->widgetheight;
            } else if ($v->nama == 'Last Footer') {
                $rmrd['isshoweverypagelf'] = $v->isshoweverypage;
                $rmrd['marginlf'] = '0px 0px 0px 0px';
                $heightlf = $v->widgetheight;
            }
        }
        
        if ($template->ispotrait) {
            $ispotrait = 'potrait';
        } else {
            $ispotrait = 'landscape';
        }
        
        $html = '<!DOCTYPE html>
                <html lang="">
                    <head>
                        <meta charset="utf-8">
                        <meta http-equiv="X-UA-Compatible" content="IE=edge">
                        <meta name="viewport" content="width=device-width, initial-scale=1">
                        <title>'.$template->nama.'</title>
                        <style>
                            $css
                            
                            #watermark1 {
                                position: fixed;
                                top:   21cm;
                                // left:     1.5cm;
                                width:    8cm;
                                height:   8cm;
                                z-index:  -1000;
                                transform: rotate(300deg); 
                                color: #f9f8f7;
                                font-size: 50px;
                            }

                            #watermark2 {
                                position: fixed;
                                top:   10cm;
                                left:     -5cm;
                                width:    8cm;
                                height:   8cm;
                                z-index:  -1000;
                                transform: rotate(300deg); 
                                color: #f9f8f7;
                                font-size: 50px;
                            }
                        </style>
                    </head>
                    <body>
                        <div id="watermark1"> 
                            <h1>'.$template->reportwatermark.'</h1>
                        </div> 

                        <div id="watermark2"> 
                            <h1>'.$template->reportwatermark.'</h1>
                        </div> 

                        $content
                    </body>
                </html>';

        $body_margin = $template->margintop.' '.$template->marginright.' '.$template->marginbottom.' '.$template->marginleft;

        $css = 'body {
                    font-family: "MS Serif", "New York", sans-serif;
                    font-size: 11px;
                    margin: '.$body_margin.';
                }

                .container {
                    // position: fixed;
                }

                .fh {
                    margin: '.$rmrd['marginfh'].';
                    height: '.$rmrd['heightfh'].'px;
                }

                .oh {
                    margin: '.$rmrd['marginoh'].';
                    height: '.$rmrd['heightoh'].'px;
                }

                .eh {
                    margin: '.$rmrd['margineh'].';
                    height: '.$rmrd['heighteh'].'px;
                }

                .of {
                    margin: '.$rmrd['marginof'].';
                    height: '.$rmrd['heightof'].'px;
                }

                .ef {
                    margin: '.$rmrd['marginef'].';
                    height: '.$rmrd['heightef'].'px;
                }

                .footer {
                    position: fixed;
                    margin: '.$body_margin.';
                }

                .font-size-address {
                    font-size: 10px
                }

                .img-logo {
                    height: 50px;
                    margin: -15px 0px 0px 10px;
                }

                .address {
                    margin: -75px 0px 0px 65px;
                    font-size: 8px;
                }

                .title {
                    position: absolute;
                    z-index: 10;
                    margin: -48px 0px 80px 70px;
                    font-size: 12px;
                }

                .title div {
                    border-style: double;
                    padding: 3px 20px 3px 20px;
                    width: 200px;
                }

                .title b {
                    font-size: 20px;
                }

                .title p {
                    margin: 10px 0px 0px 0px;
                }

                .date {
                    margin: -65px 0px 20px 430px;
                    font-size: 12px;
                }

                .dibayar-kepada {
                    margin: 0px 0px 0px 15px;
                }

                .content-1 {
                    height: '.$rmrd['hd'].'px;
                }
                
                .table-custom-1 {
                    // width: 100%;
                    margin: 0px 0px 0px 0px;
                    border-collapse: collapse;
                }

                .table-custom-1, .table-custom-1 thead tr th, .table-custom-1 tbody tr td {
                    border: 1px solid black;
                    padding: 3px 3px 3px 3px;
                }
                
                .table-custom-1 thead tr th:nth-child(1) {
                    width:0px;
                }

                .table-custom-1 thead tr th:nth-child(2) {
                    width:285px;
                }

                .table-custom-1 thead tr th:nth-child(3) {
                    width: 80px;
                }

                .table-custom-1 thead tr th:nth-child(4) {
                    width: 80px;
                }

                .table-custom-1 thead tr th:nth-child(5) {
                    width: 50px;
                }

                .table-custom-1 thead tr th:nth-child(6) {
                    width: 100px;
                }

                .table-custom-1 tbody tr .giro-cek {
                    border-left: 1px solid white !important;
                    border-bottom: 1px solid white !important;
                }

                .giro-cek b {
                    margin-bottom: 0px !important;
                }



                .table-custom-2 {
                    // width: 100%;
                    border-collapse: collapse;
                }

                .table-custom-2 thead tr th:nth-child(1) {
                    width:144px;
                }
                
                .table-custom-2 thead tr th:nth-child(2) {
                    width:144px;
                }

                .table-custom-2 thead tr th:nth-child(3) {
                    width:144px;
                }

                .table-custom-2 thead tr th:nth-child(4) {
                    width:144px;
                }

                .table-custom-2 thead tr th:nth-child(5) {
                    width:51px;
                }

                .table-custom-2, .table-custom-2 thead tr th, .table-custom-2 tbody tr td {
                    border: 1px solid black;
                    padding: 5px 3px 5px 3px;
                    text-align: center;
                }
                
                .text-right {
                    text-align: right;
                }
                '
        ;
        
        
        $html = str_replace('$css', $css, $html);
        
        /////////
        //1
        if (count($resultdetail) >= $showdatapertable) {
            $qtydatadetail = count($resultdetail)-count($resultdetail)%$showdatapertable;
            $qtydatadetail = $qtydatadetail/$showdatapertable;
            if (count($resultdetail)%$showdatapertable != 0) {
                $qtydatadetail++;
            }
        } else {
            $qtydatadetail = 1;
        }
        //2
        if (count($resultdetail2) >= $showdatapertable) {
            $qtydatadetail2 = count($resultdetail2)-count($resultdetail2)%$showdatapertable;
            $qtydatadetail2 = $qtydatadetail2/$showdatapertable;
            if (count($resultdetail2)%$showdatapertable != 0) {
                $qtydatadetail2++;
            }
        } else {
            $qtydatadetail2 = 1;
        }

        //1
        $start = 0;
        $finish = $showdatapertable-1;
        for ($i=0; $i<$qtydatadetail; $i++) {
            $dataqtydatadetail[] = (Object)[
                'start' => $start,
                'finish' => $finish
            ];
            $start = $finish+1;
            $finish = $start+($showdatapertable-1);
        }
        //2
        $start2 = 0;
        $finish2 = $showdatapertable-1;
        for ($i=0; $i<$qtydatadetail2; $i++) {
            $dataqtydatadetail2[] = (Object)[
                'start' => $start2,
                'finish' => $finish2
            ];
            $start2 = $finish2+1;
            $finish2 = $start2+($showdatapertable-1);
        }

        $content = '';
        $total = [0,0];
        $no = [1,1];
        foreach ($dataqtydatadetail as $k => $v) {
            if ($k == 0) {
                $content .= @$rmrd['fh'];
            }
            foreach ($tabledetail as $k_ => $v_) {
                if ($k_ == 0) {
                    $record = '';
                    $content .= $template->reporttemplate;
                    $qtylasttable = 0;
                    $subtotal = 0;
                    foreach ($resultdetail as $k2 => $v2) {
                        if ($k2 >= $v->start && $k2 <= $v->finish) {
                            if (@$v2->keterangan) {
                                if (strlen($v2->keterangan) >= 50) {
                                    $keterangan = @substr($v2->keterangan, 0, 50).'...';
                                } else {
                                    $keterangan = $v2->keterangan;
                                }
                            } else {
                                $keterangan = '';
                            }
                            $record .= "<tr>
                                            <td>".$no[$k_]."</td>
                                            <td>".@$keterangan."</td>
                                            <td>".@$v2->idso."</td>
                                            <td>".@$v2->idpo."</td>
                                            <td>".@$v2->idpo."</td>
                                            <td style='text-align: right'>".@$v2->nominal."</td>
                                        </tr>";
                            $no[$k_]++;
                            $qtylasttable++;
                            $subtotal += @$v2->nominal;
                        }
                    }

                    $total[0] += $subtotal;
                    $content = str_replace('$datadetail', $record, $content);
                    $content = str_replace('$subtotal_jumlah_datadetail', $subtotal, $content);
                    $content = str_replace('$tax_jumlah_datadetail', 0, $content);
                    $content = str_replace('$total_jumlah_datadetail', $total[0], $content);
                    
                    if (count($dataqtydatadetail) == $k+1) {
                        $marginlf = explode('px ', $rmrd['marginlf']);
                        $marginlf[0] = (count($resultdetail) >= count($resultdetail2)) ? (int)$rmrd['marginlf'][0]+0 : $rmrd['marginlf'][0];
                        $marginlf[2] = (count($resultdetail) >= count($resultdetail2)) ? (int)$rmrd['marginlf'][2]+72 : $rmrd['marginlf'][2];
                        $marginlf = implode('px ', $marginlf);
                        if ($rmrd['isshoweverypagelf']) {
                            $content .= '<div style="margin: '.$marginlf.'; height: '.$heightlf.'px">'.$template->reportfooter.'</div>';
                        } else {
                            $content .= '<div style="margin: '.$marginlf.'; height: '.$heightlf.'px"></div>';
                        }
                        if ($k%2 == 0) {
                            $content .= @$rmrd['oh'];
                        } else {
                            $content .= @$rmrd['eh'];
                        }
                    } else {
                        if ($k%2 == 0) {
                            $content .= @$rmrd['ef'];
                            $content .= @$rmrd['oh'];
                        } else {
                            $content .= @$rmrd['of'];
                            $content .= @$rmrd['eh'];
                        }
                    }
                } else {
                    $record = '';
                    $content .= $template->reporttemplate;
                    $qtylasttable = 0;
                    $subtotal = 0;
                    $anyrecord = false;
                    foreach ($resultdetail2 as $k2 => $v2) {
                        if ($k2 >= $v->start && $k2 <= $v->finish) {
                            if (@$v2->keterangan) {
                                if (strlen($v2->keterangan) >= 50) {
                                    $keterangan = @substr($v2->keterangan, 0, 50).'...';
                                } else {
                                    $keterangan = $v2->keterangan;
                                }
                            } else {
                                $keterangan = '';
                            }
                            $record .= "<tr>
                                            <td>".$no[$k_]."</td>
                                            <td>".@$keterangan."</td>
                                            <td>".@$v2->idso."</td>
                                            <td>".@$v2->idpo."</td>
                                            <td>".@$v2->idpo."</td>
                                            <td style='text-align: right'>".@$v2->nominal."</td>
                                        </tr>";
                            $no[$k_]++;
                            $qtylasttable++;
                            $subtotal += @$v2->nominal;
                            $anyrecord = true;
                        }
                    }

                    $total[1] += $subtotal;
                    $content = str_replace('$datadetail', $record, $content);
                    $content = str_replace('$subtotal_jumlah_datadetail', $subtotal, $content);
                    $content = str_replace('$tax_jumlah_datadetail', 0, $content);
                    $content = str_replace('$total_jumlah_datadetail', $total[1], $content);
                    
                    if ($anyrecord) {
                        if (count($dataqtydatadetail2) == $k+1) {
                            if ($rmrd['isshoweverypagelf']) {
                                $content .= '<div style="margin: '.$rmrd['marginlf'].'; height: '.$heightlf.'px">'.$template->reportfooter.'</div>';
                            } else {
                                $content .= '<div style="margin: '.$rmrd['marginlf'].'; height: '.$heightlf.'px"></div>';
                            }
                        } else {
                            if ($k%2 == 0) {
                                $content .= @$rmrd['ef'];
                                $content .= @$rmrd['oh'];
                            } else {
                                $content .= @$rmrd['of'];
                                $content .= @$rmrd['eh'];
                            }
                        }
                    }
                }
            }
        }
        // dd($content);
        // dd($dataqtydatadetail);
        /////////

        $html = str_replace('$content', $content, $html);
        $html = str_replace('$logo', '<img src="assets/lycon.jpeg" alt="" class="img-logo">', $html);
        $html = str_replace('$genmcompany_nama', $resultheader->genmcompany_nama, $html);
        $html = str_replace('$today', date('d M Y'), $html);
        $html = str_replace('$kode', $resultheader->$tablemaster_kode, $html);
        $html = str_replace('$revdate', $resultheader->$tablemaster_revno.'/'.date('d M Y'), $html);

        $pdf = PDF::loadHTML($html);
        $pdf->setPaper($template->defaultpaper, $ispotrait);
        return $pdf->stream('users.pdf');
    }

    public function print_2($noid, $urlprint) {
        $noidrptmreport = 25101;
        $tabledetail = 'fincashbankdetail';
        $showdatapertable = 10;
        $urlprint = explode('/', base64_decode($urlprint));
        $template = DB::connection('mysql2')->table('rptmreport')->where('noid', $noidrptmreport)->first();
        
        $tablemaster = DB::table('cmswidget')->select('maintable')->where('kode', $noid)->orderBy('noid', 'ASC')->first()->maintable;
        $resultmaster = DB::connection('mysql2')->table($tablemaster)->where('noid', $urlprint[1])->orderBy('noid', 'ASC')->first();
        $resultdetail = DB::connection('mysql2')->table($tabledetail)->where('idmaster', $resultmaster->noid)->orderBy('noid', 'ASC')->get();
        $resultrptmreportdetail = DB::connection('mysql2')->table('rptmreportdetail')->where('idreport', $noidrptmreport)->orderBy('nourut', 'ASC')->get();        
        $tablemaster_kode = $tablemaster.'_kode';
        $tablemaster_revno = $tablemaster.'_revno';
        $resultheader = DB::connection('mysql2')->select("SELECT $tablemaster.kode AS $tablemaster_kode, $tablemaster.revno AS $tablemaster_revno, genmcompany.nama AS genmcompany_nama FROM $tablemaster JOIN genmcompany ON $tablemaster.idcompany = genmcompany.noid WHERE $tablemaster.noid=$urlprint[1] LIMIT 1")[0];

        $rmrd = [];
        foreach ($resultrptmreportdetail as $k => $v) {
            if ($v->nama == 'First Header') {
                if ($v->isshowfirstpage) {
                    $rmrd['fh'] = '<div class="fh">'.$template->reportheader.'</div>';
                } else {
                    $rmrd['fh'] = '<div class="fh">&nbsp;</div>';
                }
                $rmrd['marginfh'] = '0px 0px 0px 0px';
                $rmrd['heightfh'] = $v->widgetheight;
            } else if ($v->nama == 'Odd Header') {
                if ($v->isshoweverypage) {
                    $rmrd['oh'] = '<div class="oh">'.$template->reportheader.'</div>';
                } else {
                    $rmrd['oh'] = '<div class="oh">&nbsp;</div>';
                }
                $rmrd['marginoh'] = '70px 0px 0px 0px';
                $rmrd['heightoh'] = $v->widgetheight;
            } else if ($v->nama == 'Even Header') {
                if ($v->isshoweverypage) {
                    $rmrd['eh'] = '<div class="eh">'.$template->reportheader.'</div>';
                } else {
                    $rmrd['eh'] = '<div class="eh">&nbsp;</div>';
                }
                $rmrd['margineh'] = '0px 0px 0px 0px';
                $rmrd['heighteh'] = $v->widgetheight;
            } else if ($v->nama == 'Detail') {
                $rmrd['hd'] = $v->widgetheight;
            } else if ($v->nama == 'Odd Footer') {
                if ($v->isshoweverypage) {
                    $rmrd['of'] = '<div class="of">'.$template->reportfooter.'</div>';
                } else {
                    $rmrd['of'] = '<div class="of">&nbsp;</div>';
                }
                $rmrd['marginof'] = '0px 0px 50px 0px';
                $rmrd['heightof'] = $v->widgetheight;
            } else if ($v->nama == 'Even Footer') {
                if ($v->isshoweverypage) {
                    $rmrd['ef'] = '<div class="ef">'.$template->reportfooter.'</div>';
                } else {
                    $rmrd['ef'] = '<div class="ef">&nbsp;</div>';
                }
                $rmrd['marginef'] = '0px 0px 0px 0px';
                $rmrd['heightef'] = $v->widgetheight;
            } else if ($v->nama == 'Last Footer') {
                $rmrd['isshoweverypagelf'] = $v->isshoweverypage;
                $rmrd['marginlf'] = '0px 0px 0px 0px';
                $heightlf = $v->widgetheight;
            }
        }
        
        if ($template->ispotrait) {
            $ispotrait = 'potrait';
        } else {
            $ispotrait = 'landscape';
        }
        
        $html = '<!DOCTYPE html>
                <html lang="">
                    <head>
                        <meta charset="utf-8">
                        <meta http-equiv="X-UA-Compatible" content="IE=edge">
                        <meta name="viewport" content="width=device-width, initial-scale=1">
                        <title>'.$template->nama.'</title>
                        <style>
                            $css
                            
                            #watermark1 {
                                position: fixed;
                                top:   21cm;
                                // left:     1.5cm;
                                width:    8cm;
                                height:   8cm;
                                z-index:  -1000;
                                transform: rotate(300deg); 
                                color: #f9f8f7;
                                font-size: 50px;
                            }

                            #watermark2 {
                                position: fixed;
                                top:   10cm;
                                left:     -5cm;
                                width:    8cm;
                                height:   8cm;
                                z-index:  -1000;
                                transform: rotate(300deg); 
                                color: #f9f8f7;
                                font-size: 50px;
                            }
                        </style>
                    </head>
                    <body>
                        <div id="watermark1"> 
                            <h1>'.$template->reportwatermark.'</h1>
                        </div> 

                        <div id="watermark2"> 
                            <h1>'.$template->reportwatermark.'</h1>
                        </div> 

                        $content
                    </body>
                </html>';

        $body_margin = $template->margintop.' '.$template->marginright.' '.$template->marginbottom.' '.$template->marginleft;

        $css = 'body {
                    font-family: "MS Serif", "New York", sans-serif;
                    font-size: 11px;
                    margin: '.$body_margin.';
                }

                .container {
                    // position: fixed;
                }

                .fh {
                    margin: '.$rmrd['marginfh'].';
                    height: '.$rmrd['heightfh'].'px;
                }

                .oh {
                    margin: '.$rmrd['marginoh'].';
                    height: '.$rmrd['heightoh'].'px;
                }

                .eh {
                    margin: '.$rmrd['margineh'].';
                    height: '.$rmrd['heighteh'].'px;
                }

                .of {
                    margin: '.$rmrd['marginof'].';
                    height: '.$rmrd['heightof'].'px;
                }

                .ef {
                    margin: '.$rmrd['marginef'].';
                    height: '.$rmrd['heightef'].'px;
                }

                .footer {
                    position: fixed;
                    margin: '.$body_margin.';
                }

                .font-size-address {
                    font-size: 10px
                }

                .img-logo {
                    height: 50px;
                    margin: -15px 0px 0px 10px;
                }

                .address {
                    margin: -75px 0px 0px 65px;
                    font-size: 8px;
                }

                .title {
                    position: absolute;
                    z-index: 10;
                    margin: -48px 0px 80px 70px;
                    font-size: 12px;
                }

                .title div {
                    border-style: double;
                    padding: 3px 20px 3px 20px;
                    width: 200px;
                }

                .title b {
                    font-size: 20px;
                }

                .title p {
                    margin: 10px 0px 0px 0px;
                }

                .date {
                    margin: -65px 0px 20px 430px;
                    font-size: 12px;
                }

                .dibayar-kepada {
                    margin: 0px 0px 0px 15px;
                }

                .content-1 {
                    height: '.$rmrd['hd'].'px;
                }
                
                .table-custom-1 {
                    // width: 100%;
                    margin: 0px 0px 0px 0px;
                    border-collapse: collapse;
                }

                .table-custom-1, .table-custom-1 thead tr th, .table-custom-1 tbody tr td {
                    border: 1px solid black;
                    padding: 3px 3px 3px 3px;
                }
                
                .table-custom-1 thead tr th:nth-child(1) {
                    width:0px;
                }

                .table-custom-1 thead tr th:nth-child(2) {
                    width:285px;
                }

                .table-custom-1 thead tr th:nth-child(3) {
                    width: 80px;
                }

                .table-custom-1 thead tr th:nth-child(4) {
                    width: 80px;
                }

                .table-custom-1 thead tr th:nth-child(5) {
                    width: 50px;
                }

                .table-custom-1 thead tr th:nth-child(6) {
                    width: 100px;
                }

                .table-custom-1 tbody tr .giro-cek {
                    border-left: 1px solid white !important;
                    border-bottom: 1px solid white !important;
                }

                .giro-cek b {
                    margin-bottom: 0px !important;
                }



                .table-custom-2 {
                    // width: 100%;
                    border-collapse: collapse;
                }

                .table-custom-2 thead tr th:nth-child(1) {
                    width:144px;
                }
                
                .table-custom-2 thead tr th:nth-child(2) {
                    width:144px;
                }

                .table-custom-2 thead tr th:nth-child(3) {
                    width:144px;
                }

                .table-custom-2 thead tr th:nth-child(4) {
                    width:144px;
                }

                .table-custom-2 thead tr th:nth-child(5) {
                    width:51px;
                }

                .table-custom-2, .table-custom-2 thead tr th, .table-custom-2 tbody tr td {
                    border: 1px solid black;
                    padding: 5px 3px 5px 3px;
                    text-align: center;
                }
                
                .text-right {
                    text-align: right;
                }
                '
        ;
        
        
        $html = str_replace('$css', $css, $html);
        
        /////////
        if (count($resultdetail) >= $showdatapertable) {
            $qtydatadetail = count($resultdetail)-count($resultdetail)%$showdatapertable;
            $qtydatadetail = $qtydatadetail/$showdatapertable;
            if (count($resultdetail)%$showdatapertable != 0) {
                $qtydatadetail++;
            }
        } else {
            $qtydatadetail = 1;
        }

        $start = 0;
        $finish = $showdatapertable-1;
        for ($i=0; $i<$qtydatadetail; $i++) {
            $dataqtydatadetail[] = (Object)[
                'start' => $start,
                'finish' => $finish
            ];
            $start = $finish+1;
            $finish = $start+($showdatapertable-1);
        }

        $content = '';
        $total = 0;
        $no = 1;
        foreach ($dataqtydatadetail as $k => $v) {
            if ($k == 0) {
                $content .= @$rmrd['fh'];
            }

            $record = '';
            $content .= $template->reporttemplate;
            $qtylasttable = 0;
            $subtotal = 0;
            foreach ($resultdetail as $k2 => $v2) {
                if ($k2 >= $v->start && $k2 <= $v->finish) {
                    if (@$v2->keterangan) {
                        if (strlen($v2->keterangan) >= 50) {
                            $keterangan = @substr($v2->keterangan, 0, 50).'...';
                        } else {
                            $keterangan = $v2->keterangan;
                        }
                    } else {
                        $keterangan = '';
                    }
                    $record .= "<tr>
                                    <td>".$no."</td>
                                    <td>".@$keterangan."</td>
                                    <td>".@$v2->idso."</td>
                                    <td>".@$v2->idpo."</td>
                                    <td>".@$v2->idpo."</td>
                                    <td style='text-align: right'>".@$v2->nominal."</td>
                                </tr>";
                    $no++;
                    $qtylasttable++;
                    $subtotal += @$v2->nominal;
                }
            }

            $total += $subtotal;
            $content = str_replace('$datadetail', $record, $content);
            $content = str_replace('$subtotal_jumlah_datadetail', $subtotal, $content);
            $content = str_replace('$tax_jumlah_datadetail', 0, $content);
            $content = str_replace('$total_jumlah_datadetail', $total, $content);
            
            if (count($dataqtydatadetail) == $k+1) {
                if ($rmrd['isshoweverypagelf']) {
                    $content .= '<div style="margin: '.$rmrd['marginlf'].'; height: '.$heightlf.'px">'.$template->reportfooter.'</div>';
                } else {
                    $content .= '<div style="margin: '.$rmrd['marginlf'].'; height: '.$heightlf.'px"></div>';
                }
            } else {
                if ($k%2 == 0) {
                    $content .= @$rmrd['ef'];
                    $content .= @$rmrd['oh'];
                } else {
                    $content .= @$rmrd['of'];
                    $content .= @$rmrd['eh'];
                }
            }
        }
        // dd($content);
        // dd($dataqtydatadetail);
        /////////

        $html = str_replace('$content', $content, $html);
        $html = str_replace('$logo', '<img src="assets/lycon.jpeg" alt="" class="img-logo">', $html);
        $html = str_replace('$genmcompany_nama', $resultheader->genmcompany_nama, $html);
        $html = str_replace('$today', date('d M Y'), $html);
        $html = str_replace('$kode', $resultheader->$tablemaster_kode, $html);
        $html = str_replace('$revdate', $resultheader->$tablemaster_revno.'/'.date('d M Y'), $html);

        $pdf = PDF::loadHTML($html);
        $pdf->setPaper($template->defaultpaper, $ispotrait);
        return $pdf->stream('users.pdf');
    }


    public function print_bl0x($noid, $urlprint) {
        $noidrptmreport = 25101;                            //noid rptmreport
        $tabledetail = 'fincashbankdetail';                 //table detail
        $urlprint = explode('/', base64_decode($urlprint));
        $template = DB::connection('mysql2')->table('rptmreport')->where('noid', $noidrptmreport)->first();
        
        if ($template->defaultpaper == 'A4') {
            if ($template->ispotrait) {
                $ispotrait = 'potrait';
                $showdatapertable = 38;                     //show data pertable default
                $sdpt0 = 38;                                //show data pertable pertama
                $sdptoh = 38;                               //show data pertable odd header
                $sdpteh = 38;                               //show data pertable even header
                $sdptof = 38;                               //show data pertable odd footer
                $sdptef = 38;                               //show data pertable even footer
                $content1width = [20,298,80,80,70,100];     //width column table content
                $charketerangan = 50;                       //slice character keterangan
                $headermiddleml = 93;                       //margin-left middle header
                $headerlastml = 480;                        //margin-left last header
                $content2width = [150,150,150,150,55];      //width column table footer
                $heightdetail = 875;                        //height table content
            } else {
                $ispotrait = 'landscape';
                $showdatapertable = 22;
                $sdpt0 = 22;
                $sdptoh = 22;
                $sdpteh = 22;
                $sdptof = 23;
                $sdptef = 23;
                $content1width = [20,640,80,80,50,100];
                $charketerangan = 110;
                $headermiddleml = 240;
                $headerlastml = 790;
                $content2width = [215,215,215,215,129];
                $heightdetail = 545;
            }
        } else if ($template->defaultpaper == 'A3') {
            if ($template->ispotrait) {
                $ispotrait = 'potrait';
                $showdatapertable = 60;
                $sdpt0 = 60;
                $sdptoh = 35;
                $sdpteh = 35;
                $sdptof = 62;
                $sdptef = 62;
                $content1width = [0,615,80,80,50,100];
                $charketerangan = 115;
                $headermiddleml = 230;
                $headerlastml = 760;
                $content2width = [220,220,220,220,79];
                $heightdetail = 1290;
            } else {
                $ispotrait = 'landscape';
                $showdatapertable = 35;
                $sdpt0 = 60;
                $sdptoh = 35;
                $sdpteh = 35;
                $sdptof = 39;
                $sdptef = 39;
                $content1width = [0,1080,80,80,50,100];
                $charketerangan = 210;
                $headermiddleml = 430;
                $headerlastml = 1200;
                $content2width = [330,330,330,330,104];
                $heightdetail = 830;
            }
        } else if ($template->defaultpaper == 'A5') {
            if ($template->ispotrait) {
                $ispotrait = 'potrait';
                $showdatapertable = 20;
                $sdpt0 = 20;
                $sdptoh = 20;
                $sdpteh = 20;
                $sdptof = 11;
                $sdptef = 11;
                $content1width = [20,10,80,80,50,100];
                $charketerangan = 10;
                $headermiddleml = 10;
                $headerlastml = 170;
                $content2width = [145,145,145,145,78];
                $heightdetail = 300;
            } else {
                $ispotrait = 'landscape';
                $showdatapertable = 10;
                $sdpt0 = 10;
                $sdptoh = 10;
                $sdpteh = 10;
                $sdptof = 11;
                $sdptef = 11;
                $content1width = [20,310,80,80,50,100];
                $charketerangan = 10;
                $headermiddleml = 100;
                $headerlastml = 470;
                $content2width = [145,145,145,145,78];
                $heightdetail = 1000;
            }
        }
        
        $tablemaster = DB::table('cmswidget')->select('maintable')->where('kode', $noid)->orderBy('noid', 'ASC')->first()->maintable;
        $resultmaster = DB::connection('mysql2')->table($tablemaster)->where('noid', $urlprint[1])->orderBy('noid', 'ASC')->first();
        $resultdetail = DB::connection('mysql2')->table($tabledetail)->where('idmaster', $resultmaster->noid)->orderBy('noid', 'ASC')->get();
        $resultrptmreportdetail = DB::connection('mysql2')->table('rptmreportdetail')->where('idreport', $noidrptmreport)->orderBy('nourut', 'ASC')->get();        
        $tablemaster_kode = $tablemaster.'_kode';
        $tablemaster_revno = $tablemaster.'_revno';
        $resultheader = DB::connection('mysql2')->select("SELECT $tablemaster.kode AS $tablemaster_kode, $tablemaster.revno AS $tablemaster_revno, genmcompany.nama AS genmcompany_nama FROM $tablemaster JOIN genmcompany ON $tablemaster.idcompany = genmcompany.noid WHERE $tablemaster.noid=$urlprint[1] LIMIT 1")[0];

        $rmrd = [];
        foreach ($resultrptmreportdetail as $k => $v) {
            if ($v->nama == 'First Header') {
                if ($v->isshowfirstpage) {
                    $rmrd['fh'] = '<div class="fh">'.$template->reportheader.'</div>';
                    $rmrd['sdpteh'] = $showdatapertable;
                    $rmrd['sdpt0'] = $sdpt0;
                    $rmrd['showfh'] = true;
                } else {
                    $rmrd['fh'] = '<div class="fh">&nbsp;</div>';
                    $rmrd['sdpteh'] = $sdpteh;
                    $rmrd['sdpt0'] = $sdpt0+4;
                    $rmrd['showfh'] = false;
                }
                $rmrd['marginfh'] = '0px 0px 0px 0px';
                $rmrd['heightfh'] = $v->widgetheight;
            } else if ($v->nama == 'Odd Header') {
                if ($v->isshoweverypage) {
                    $rmrd['oh'] = '<div class="oh">'.$template->reportheader.'</div>';
                    $rmrd['sdptoh'] = $showdatapertable;
                    $rmrd['showoh'] = true;
                } else {
                    $rmrd['oh'] = '<div class="oh">&nbsp;</div>';
                    $rmrd['sdptoh'] = $sdptoh;
                    $rmrd['showoh'] = false;
                }
                $rmrd['marginoh'] = '0px 0px 0px 0px';
                $rmrd['heightoh'] = $v->widgetheight;
            } else if ($v->nama == 'Even Header') {
                if ($v->isshoweverypage) {
                    $rmrd['eh'] = '<div class="eh">'.$template->reportheader.'</div>';
                    $rmrd['showeh'] = true;
                } else {
                    $rmrd['eh'] = '<div class="eh">&nbsp;</div>';
                    $rmrd['showeh'] = false;
                }
                $rmrd['margineh'] = '0px 0px 0px 0px';
                $rmrd['heighteh'] = $v->widgetheight;
            } else if ($v->nama == 'Detail') {
                // $rmrd['hd'] = $v->widgetheight;
                $rmrd['hd'] = $heightdetail;
            } else if ($v->nama == 'Odd Footer') {
                if ($v->isshoweverypage) {
                    $rmrd['of'] = '<div class="of">'.$template->reportfooter.'</div>';
                    $rmrd['sdptof'] = $showdatapertable;
                    $rmrd['showof'] = true;
                } else {
                    $rmrd['of'] = '<div class="of">&nbsp;</div>';
                    $rmrd['sdptof'] = $sdptof;
                    $rmrd['showof'] = false;
                }
                $rmrd['marginof'] = '0px 0px 0px 0px';
                $rmrd['heightof'] = $v->widgetheight;
            } else if ($v->nama == 'Even Footer') {
                if ($v->isshoweverypage) {
                    $rmrd['ef'] = '<div class="ef">'.$template->reportfooter.'</div>';
                    $rmrd['sdptef'] = $showdatapertable;
                    $rmrd['showef'] = true;
                    $rmrd['ef0'] = '<div class="ef0">'.$template->reportfooter.'</div>';
                    $rmrd['ef0_'] = '<div class="ef0_">'.$template->reportfooter.'</div>';
                    $rmrd['hfef0'] = $v->widgetheight;
                } else {
                    $rmrd['ef0'] = '<div class="ef0">&nbsp;</div>';
                    // $rmrd['ef0_'] = '<div class="ef0_">&nbsp;</div>';
                    $rmrd['hfef0'] = 0;
                    $rmrd['sdpt0'] += 6;
                    $rmrd['ef'] = '<div class="ef">&nbsp;</div>';
                    $rmrd['sdptef'] = $sdptef;
                    $rmrd['showef'] = false;
                }
                $rmrd['hfef0_'] = $v->widgetheight;
                // dd($rmrd['showfh'].'-'.$rmrd['showef'].'/'.$rmrd['sdpt0']);
                $rmrd['marginef'] = '0px 0px 0px 0px';
                $rmrd['heightef'] = $v->widgetheight;
            } else if ($v->nama == 'Last Footer') {
                $rmrd['isshoweverypagelf'] = $v->isshoweverypage;
                $rmrd['marginlf'] = '0px 0px 0px 0px';
                $heightlf = $v->widgetheight;
                if ($v->isshoweverypage) {
                    $rmrd['showlf'] = true;
                } else {
                    $rmrd['showlf'] = false;
                }
            }
        }
        // dd($rmrd['ef0']);

        // dd(1);
        if ($template->defaultpaper == 'A4') {
            if ($template->ispotrait) {
                if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] = $rmrd['sdpt0'];
                    $rmrd['sdptef'] += 0;
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if (!$rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] = $sdpt0+2;
                    $rmrd['sdptef'] += 0;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail+60;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 50;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail;
                    $rmrd['hfehef'] = 50;
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = 9;
                    $rmrd['hdohof'] = $heightdetail+65;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhehef'] = 9;
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 3;
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptof'] += 3;
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = 0;
                    $rmrd['sdptef'] += 3;
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] = $rmrd['sdpt0'];
                    $rmrd['sdptef'] += 0;
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if (!$rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 1;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail+60;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 0;
                    $rmrd['hhohof'] = 9;
                    $rmrd['hdohof'] = $heightdetail+65;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if (!$rmrd['showfh'] && $rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 1;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail+60;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 3;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhehef'] = 10;
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = 50;
                    $rmrd['sdptef'] += 0;
                } else if (!$rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 1;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail+60;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptof'] += 3;
                } else if (!$rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 4;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail+40;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = 0;
                    $rmrd['sdptef'] += 3;
                } else if (!$rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 1;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail+60;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 0;
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = 9;
                    $rmrd['hdohof'] = $heightdetail+65;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhehef'] = 9;
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 3;
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = 10;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['sdptof'] += 6;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 0;
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail+65;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = 0;
                    $rmrd['sdptef'] += 3;
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = 9;
                    $rmrd['hdohof'] = $heightdetail+65;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 0;
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = 20;
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 3;
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhehef'] = 10;
                    $rmrd['hdehef'] = $heightdetail+115;
                    $rmrd['hfehef'] = 0;
                    $rmrd['sdptef'] += 6;
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhehef'] = 9;
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 3;
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 10;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = 0;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 3;
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 0;
                    $rmrd['sdptof'] += 3;
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+50;
                    $rmrd['hfehef'] = 0;
                    $rmrd['sdptef'] += 3;
                } else if (!$rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {    
                    $rmrd['sdpt0'] -= 1;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail+40;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 10;
                    $rmrd['hdohof'] = $heightdetail+65;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhehef'] = 10;
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 3;
                } else if (!$rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {    
                    $rmrd['sdpt0'] -= 1;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail+40;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 10;
                    $rmrd['hdohof'] = $heightdetail+65;
                    $rmrd['hfohof'] = 0;
                    $rmrd['sdptof'] += 6;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 0;
                } else if (!$rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {    
                    $rmrd['sdpt0'] -= 4;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail+40;
                    $rmrd['hffhef'] = 10;
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = 0;
                    $rmrd['sdptef'] += 3;
                } else if (!$rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && !$rmrd['showlf']) {    
                    $rmrd['sdpt0'] -= 2;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail+65;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = 30;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 0;
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {   //YNNNNY
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 10;
                    $rmrd['sdptof'] += 6;
                    $rmrd['hhehef'] = 9;
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 6;
                } else {
                    if ($rmrd['showfh'] && $rmrd['showef']) {
                        $rmrd['hhfhef'] = $rmrd['heightfh'];
                        $rmrd['hdfhef'] = $heightdetail;
                        $rmrd['hffhef'] = $rmrd['heightef'];
                    } else if (!$rmrd['showfh'] && $rmrd['showef']) {
                        $rmrd['hhfhef'] = 0;
                        $rmrd['hdfhef'] = $heightdetail+60;
                        $rmrd['hffhef'] = $rmrd['heightef'];
                        $rmrd['sdptef'] += 3;
                    } else if ($rmrd['showfh'] && !$rmrd['showef']) {
                        $rmrd['hhfhef'] = $rmrd['heightfh'];
                        $rmrd['hdfhef'] = $heightdetail;
                        // if (!$rmrd['showef'] && !$rmrd['showof']) {
                        //     $rmrd['sdpt0'] -= 6;
                        //     $rmrd['hffhef'] = $rmrd['hfef0_'];
                        // } else {
                            $rmrd['sdpt0'] -= 3;
                            $rmrd['hffhef'] = 0;
                        // }
                    } else if (!$rmrd['showfh'] && !$rmrd['showef']) {
                        $rmrd['hhfhef'] = 0;
                        $rmrd['hdfhef'] = $heightdetail+40;
                        $rmrd['hffhef'] = 0;
                    }
                    
                    //odd
                    if ($rmrd['showoh'] && $rmrd['showof']) {
                        $rmrd['hhohof'] = $rmrd['heightoh'];
                        $rmrd['hdohof'] = $heightdetail;
                        $rmrd['hfohof'] = $rmrd['heightof'];
                        $rmrd['sdptof'] += 0;
                    } else if (!$rmrd['showoh'] && $rmrd['showof']) {
                        $rmrd['hhohof'] = 9;
                        $rmrd['hdohof'] = $heightdetail+65;
                        $rmrd['hfohof'] = $rmrd['heightof'];
                        $rmrd['sdptof'] += 3;
                    } else if ($rmrd['showoh'] && !$rmrd['showof']) {
                        $rmrd['hhohof'] = $rmrd['heightoh'];
                        $rmrd['hdohof'] = $heightdetail;
                        $rmrd['hfohof'] = 0;
                    } else if (!$rmrd['showoh'] && !$rmrd['showof']) {
                        $rmrd['hhohof'] = 24;
                        $rmrd['hdohof'] = $heightdetail;
                        $rmrd['hfohof'] = 0;
                        $rmrd['sdptof'] += 3;
                    }
            
                    if ($rmrd['showeh'] && $rmrd['showef']) {
                        $rmrd['hhehef'] = $rmrd['heighteh'];
                        $rmrd['hdehef'] = $heightdetail+7;
                        $rmrd['hfehef'] = $rmrd['heightef'];
                        $rmrd['sdptef'] += 0;
                    } else if (!$rmrd['showeh'] && $rmrd['showef']) {
                        $rmrd['hhehef'] = 9;
                        $rmrd['hdehef'] = $heightdetail+65;
                        $rmrd['hfehef'] = $rmrd['heightef'];
                        $rmrd['sdptef'] += 3;
                    } else if ($rmrd['showeh'] && !$rmrd['showef']) {
                        $rmrd['hhehef'] = $rmrd['heighteh'];
                        $rmrd['hdehef'] = $heightdetail+75;
                        $rmrd['hfehef'] = 0;
                        $rmrd['sdptef'] += 0;
                    } else if (!$rmrd['showeh'] && !$rmrd['showef']) {
                        $rmrd['hhehef'] = 24;
                        $rmrd['hdehef'] = $heightdetail+120;
                        $rmrd['hfehef'] = 0;
                        $rmrd['sdptef'] += 3;
                    }
                }
            } else {
                if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] = $rmrd['sdpt0'];
                    $rmrd['sdptef'] += 0;
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = 15;
                    $rmrd['hdohof'] = $heightdetail+65;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhehef'] = 15;
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 3;
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {   //YNNNNY
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 10;
                    $rmrd['sdptof'] += 5;
                    $rmrd['hhehef'] = 9;
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 5;
                } else {
                    if ($rmrd['showfh'] && $rmrd['showef']) {
                        $rmrd['hhfhef'] = $rmrd['heightfh'];
                        $rmrd['hdfhef'] = $heightdetail;
                        $rmrd['hffhef'] = $rmrd['heightef'];
                    } else if (!$rmrd['showfh'] && $rmrd['showef']) {
                        $rmrd['hhfhef'] = 0;
                        $rmrd['hdfhef'] = $heightdetail+60;
                        $rmrd['hffhef'] = $rmrd['heightef'];
                        $rmrd['sdptef'] += 3;
                    } else if ($rmrd['showfh'] && !$rmrd['showef']) {
                        $rmrd['hhfhef'] = $rmrd['heightfh'];
                        $rmrd['hdfhef'] = $heightdetail;
                        $rmrd['hffhef'] = 0;
                        $rmrd['sdpt0'] -= 3;
                    } else if (!$rmrd['showfh'] && !$rmrd['showef']) {
                        $rmrd['hhfhef'] = 0;
                        $rmrd['hdfhef'] = $heightdetail+40;
                        $rmrd['hffhef'] = 0;
                    }
                    
                    //odd
                    if ($rmrd['showoh'] && $rmrd['showof']) {
                        $rmrd['hhohof'] = $rmrd['heightoh'];
                        $rmrd['hdohof'] = $heightdetail;
                        $rmrd['hfohof'] = $rmrd['heightof'];
                        $rmrd['sdptof'] += 0;
                    } else if (!$rmrd['showoh'] && $rmrd['showof']) {
                        $rmrd['hhohof'] = 9;
                        $rmrd['hdohof'] = $heightdetail+65;
                        $rmrd['hfohof'] = $rmrd['heightof'];
                        $rmrd['sdptof'] += 3;
                    } else if ($rmrd['showoh'] && !$rmrd['showof']) {
                        $rmrd['hhohof'] = $rmrd['heightoh'];
                        $rmrd['hdohof'] = $heightdetail;
                        $rmrd['hfohof'] = 0;
                        $rmrd['sdptof'] += 2;
                    } else if (!$rmrd['showoh'] && !$rmrd['showof']) {
                        $rmrd['hhohof'] = 24;
                        $rmrd['hdohof'] = $heightdetail;
                        $rmrd['hfohof'] = 0;
                        $rmrd['sdptof'] += 3;
                    }
            
                    if ($rmrd['showeh'] && $rmrd['showef']) {
                        $rmrd['hhehef'] = $rmrd['heighteh'];
                        $rmrd['hdehef'] = $heightdetail+7;
                        $rmrd['hfehef'] = $rmrd['heightef'];
                        $rmrd['sdptef'] += 0;
                    } else if (!$rmrd['showeh'] && $rmrd['showef']) {
                        $rmrd['hhehef'] = 9;
                        $rmrd['hdehef'] = $heightdetail+70;
                        $rmrd['hfehef'] = $rmrd['heightef'];
                        $rmrd['sdptef'] += 3;
                    } else if ($rmrd['showeh'] && !$rmrd['showef']) {
                        $rmrd['hhehef'] = $rmrd['heighteh'];
                        $rmrd['hdehef'] = $heightdetail+75;
                        $rmrd['hfehef'] = 0;
                        $rmrd['sdptef'] += 2;
                    } else if (!$rmrd['showeh'] && !$rmrd['showef']) {
                        $rmrd['hhehef'] = 24;
                        $rmrd['hdehef'] = $heightdetail+120;
                        $rmrd['hfehef'] = 0;
                        $rmrd['sdptef'] += 3;
                    }
                }
            }
        } else if ($template->defaultpaper == 'A3') {
            if ($template->ispotrait) {
                if ($rmrd['showfh'] && $rmrd['showef']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                } else if (!$rmrd['showfh'] && $rmrd['showef']) {
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail+60;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 3;
                } else if ($rmrd['showfh'] && !$rmrd['showef']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    // if (!$rmrd['showef'] && !$rmrd['showof']) {
                    //     $rmrd['sdpt0'] -= 6;
                    //     $rmrd['hffhef'] = $rmrd['hfef0_'];
                    // } else {
                        $rmrd['sdpt0'] -= 3;
                        $rmrd['hffhef'] = 0;
                    // }
                } else if (!$rmrd['showfh'] && !$rmrd['showef']) {
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail+40;
                    $rmrd['hffhef'] = 0;
                }
                
                //odd
                if ($rmrd['showoh'] && $rmrd['showof']) {
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 0;
                } else if (!$rmrd['showoh'] && $rmrd['showof']) {
                    $rmrd['hhohof'] = 9;
                    $rmrd['hdohof'] = $heightdetail+65;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 3;
                } else if ($rmrd['showoh'] && !$rmrd['showof']) {
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                } else if (!$rmrd['showoh'] && !$rmrd['showof']) {
                    $rmrd['hhohof'] = 24;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['sdptof'] += 3;
                }
        
                if ($rmrd['showeh'] && $rmrd['showef']) {
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 0;
                } else if (!$rmrd['showeh'] && $rmrd['showef']) {
                    $rmrd['hhehef'] = 9;
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 3;
                } else if ($rmrd['showeh'] && !$rmrd['showef']) {
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+75;
                    $rmrd['hfehef'] = 0;
                    $rmrd['sdptef'] += 0;
                } else if (!$rmrd['showeh'] && !$rmrd['showef']) {
                    $rmrd['hhehef'] = 24;
                    $rmrd['hdehef'] = $heightdetail+120;
                    $rmrd['hfehef'] = 0;
                    $rmrd['sdptef'] += 3;
                }
            }
        } else if ($template->defaultpaper == 'A5') {
            if ($template->ispotrait) {
                if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] = $rmrd['sdpt0'];
                    $rmrd['sdptef'] += 0;
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 10;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = 0;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 3;
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail+75;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhehef'] = 20;
                    $rmrd['hdehef'] = $heightdetail+75;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 3;
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {   //YNNNNY
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['sdpt0'] -= 2;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 9;
                    $rmrd['hdohof'] = $heightdetail+65;
                    $rmrd['hfohof'] = 9;
                    $rmrd['sdptof'] += 6;
                    $rmrd['hhehef'] = 9;
                    $rmrd['hdehef'] = $heightdetail+135;
                    $rmrd['hfehef'] = 9;
                    $rmrd['sdptef'] += 5;
                } else {
                    if ($rmrd['showfh'] && $rmrd['showef']) {
                        $rmrd['hhfhef'] = $rmrd['heightfh'];
                        $rmrd['hdfhef'] = $heightdetail;
                        $rmrd['hffhef'] = $rmrd['heightef'];
                    } else if (!$rmrd['showfh'] && $rmrd['showef']) {
                        $rmrd['hhfhef'] = 0;
                        $rmrd['hdfhef'] = $heightdetail+60;
                        $rmrd['hffhef'] = $rmrd['heightef'];
                        $rmrd['sdptef'] += 3;
                    } else if ($rmrd['showfh'] && !$rmrd['showef']) {
                        $rmrd['hhfhef'] = $rmrd['heightfh'];
                        $rmrd['hdfhef'] = $heightdetail;
                        // if (!$rmrd['showef'] && !$rmrd['showof']) {
                        //     $rmrd['sdpt0'] -= 6;
                        //     $rmrd['hffhef'] = $rmrd['hfef0_'];
                        // } else {
                            $rmrd['sdpt0'] -= 3;
                            $rmrd['hffhef'] = 0;
                        // }
                    } else if (!$rmrd['showfh'] && !$rmrd['showef']) {
                        $rmrd['hhfhef'] = 0;
                        $rmrd['hdfhef'] = $heightdetail+40;
                        $rmrd['hffhef'] = 0;
                    }
                    
                    //odd
                    if ($rmrd['showoh'] && $rmrd['showof']) {
                        $rmrd['hhohof'] = $rmrd['heightoh'];
                        $rmrd['hdohof'] = $heightdetail;
                        $rmrd['hfohof'] = $rmrd['heightof'];
                        $rmrd['sdptof'] += 0;
                    } else if (!$rmrd['showoh'] && $rmrd['showof']) {
                        $rmrd['hhohof'] = 9;
                        $rmrd['hdohof'] = $heightdetail+65;
                        $rmrd['hfohof'] = $rmrd['heightof'];
                        $rmrd['sdptof'] += 3;
                    } else if ($rmrd['showoh'] && !$rmrd['showof']) {
                        $rmrd['hhohof'] = $rmrd['heightoh'];
                        $rmrd['hdohof'] = $heightdetail;
                        $rmrd['hfohof'] = 0;
                    } else if (!$rmrd['showoh'] && !$rmrd['showof']) {
                        $rmrd['hhohof'] = 24;
                        $rmrd['hdohof'] = $heightdetail;
                        $rmrd['hfohof'] = 0;
                        $rmrd['sdptof'] += 3;
                    }
            
                    if ($rmrd['showeh'] && $rmrd['showef']) {
                        $rmrd['hhehef'] = $rmrd['heighteh'];
                        $rmrd['hdehef'] = $heightdetail+7;
                        $rmrd['hfehef'] = $rmrd['heightef'];
                        $rmrd['sdptef'] += 0;
                    } else if (!$rmrd['showeh'] && $rmrd['showef']) {
                        $rmrd['hhehef'] = 9;
                        $rmrd['hdehef'] = $heightdetail+65;
                        $rmrd['hfehef'] = $rmrd['heightef'];
                        $rmrd['sdptef'] += 3;
                    } else if ($rmrd['showeh'] && !$rmrd['showef']) {
                        $rmrd['hhehef'] = $rmrd['heighteh'];
                        $rmrd['hdehef'] = $heightdetail+75;
                        $rmrd['hfehef'] = 0;
                        $rmrd['sdptef'] += 0;
                    } else if (!$rmrd['showeh'] && !$rmrd['showef']) {
                        $rmrd['hhehef'] = 24;
                        $rmrd['hdehef'] = $heightdetail+120;
                        $rmrd['hfehef'] = 0;
                        $rmrd['sdptef'] += 3;
                    }
                }
            } else {
                if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] = $rmrd['sdpt0'];
                    $rmrd['sdptef'] += 0;
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 10;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = 0;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 3;
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail+75;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhehef'] = 20;
                    $rmrd['hdehef'] = $heightdetail+75;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 3;
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {   //YNNNNY
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['sdpt0'] -= 2;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 9;
                    $rmrd['hdohof'] = $heightdetail+65;
                    $rmrd['hfohof'] = 9;
                    $rmrd['sdptof'] += 6;
                    $rmrd['hhehef'] = 9;
                    $rmrd['hdehef'] = $heightdetail+135;
                    $rmrd['hfehef'] = 9;
                    $rmrd['sdptef'] += 5;
                } else {
                    if ($rmrd['showfh'] && $rmrd['showef']) {
                        $rmrd['hhfhef'] = $rmrd['heightfh'];
                        $rmrd['hdfhef'] = $heightdetail;
                        $rmrd['hffhef'] = $rmrd['heightef'];
                    } else if (!$rmrd['showfh'] && $rmrd['showef']) {
                        $rmrd['hhfhef'] = 0;
                        $rmrd['hdfhef'] = $heightdetail+60;
                        $rmrd['hffhef'] = $rmrd['heightef'];
                        $rmrd['sdptef'] += 3;
                    } else if ($rmrd['showfh'] && !$rmrd['showef']) {
                        $rmrd['hhfhef'] = $rmrd['heightfh'];
                        $rmrd['hdfhef'] = $heightdetail;
                        // if (!$rmrd['showef'] && !$rmrd['showof']) {
                        //     $rmrd['sdpt0'] -= 6;
                        //     $rmrd['hffhef'] = $rmrd['hfef0_'];
                        // } else {
                            $rmrd['sdpt0'] -= 3;
                            $rmrd['hffhef'] = 0;
                        // }
                    } else if (!$rmrd['showfh'] && !$rmrd['showef']) {
                        $rmrd['hhfhef'] = 0;
                        $rmrd['hdfhef'] = $heightdetail+40;
                        $rmrd['hffhef'] = 0;
                    }
                    
                    //odd
                    if ($rmrd['showoh'] && $rmrd['showof']) {
                        $rmrd['hhohof'] = $rmrd['heightoh'];
                        $rmrd['hdohof'] = $heightdetail;
                        $rmrd['hfohof'] = $rmrd['heightof'];
                        $rmrd['sdptof'] += 0;
                    } else if (!$rmrd['showoh'] && $rmrd['showof']) {
                        $rmrd['hhohof'] = 9;
                        $rmrd['hdohof'] = $heightdetail+65;
                        $rmrd['hfohof'] = $rmrd['heightof'];
                        $rmrd['sdptof'] += 3;
                    } else if ($rmrd['showoh'] && !$rmrd['showof']) {
                        $rmrd['hhohof'] = $rmrd['heightoh'];
                        $rmrd['hdohof'] = $heightdetail;
                        $rmrd['hfohof'] = 0;
                    } else if (!$rmrd['showoh'] && !$rmrd['showof']) {
                        $rmrd['hhohof'] = 24;
                        $rmrd['hdohof'] = $heightdetail;
                        $rmrd['hfohof'] = 0;
                        $rmrd['sdptof'] += 3;
                    }
            
                    if ($rmrd['showeh'] && $rmrd['showef']) {
                        $rmrd['hhehef'] = $rmrd['heighteh'];
                        $rmrd['hdehef'] = $heightdetail+7;
                        $rmrd['hfehef'] = $rmrd['heightef'];
                        $rmrd['sdptef'] += 0;
                    } else if (!$rmrd['showeh'] && $rmrd['showef']) {
                        $rmrd['hhehef'] = 9;
                        $rmrd['hdehef'] = $heightdetail+65;
                        $rmrd['hfehef'] = $rmrd['heightef'];
                        $rmrd['sdptef'] += 3;
                    } else if ($rmrd['showeh'] && !$rmrd['showef']) {
                        $rmrd['hhehef'] = $rmrd['heighteh'];
                        $rmrd['hdehef'] = $heightdetail+75;
                        $rmrd['hfehef'] = 0;
                        $rmrd['sdptef'] += 0;
                    } else if (!$rmrd['showeh'] && !$rmrd['showef']) {
                        $rmrd['hhehef'] = 24;
                        $rmrd['hdehef'] = $heightdetail+120;
                        $rmrd['hfehef'] = 0;
                        $rmrd['sdptef'] += 3;
                    }
                }
            }
        }
        // dd($rmrd['hffhef']);
        
        $html = '<!DOCTYPE html>
                <html lang="">
                    <head>
                        <meta charset="utf-8">
                        <meta http-equiv="X-UA-Compatible" content="IE=edge">
                        <meta name="viewport" content="width=device-width, initial-scale=1">
                        <title>'.$template->nama.'</title>
                        <style>
                            $css
                            
                            #watermark1 {
                                position: fixed;
                                top:   21cm;
                                // left:     1.5cm;
                                width:    8cm;
                                height:   8cm;
                                z-index:  -1000;
                                transform: rotate(300deg); 
                                color: #f9f8f7;
                                font-size: 50px;
                            }

                            #watermark2 {
                                position: fixed;
                                top:   10cm;
                                left:     -5cm;
                                width:    8cm;
                                height:   8cm;
                                z-index:  -1000;
                                transform: rotate(300deg); 
                                color: #f9f8f7;
                                font-size: 50px;
                            }
                        </style>
                    </head>
                    <body>
                        <div id="watermark1"> 
                            <h1>'.$template->reportwatermark.'</h1>
                        </div> 

                        <div id="watermark2"> 
                            <h1>'.$template->reportwatermark.'</h1>
                        </div> 

                        $content
                    </body>
                </html>';

        $body_margin = $template->margintop.' '.$template->marginright.' '.$template->marginbottom.' '.$template->marginleft;

        $css = 'body {
                    font-family: "MS Serif", "New York", sans-serif;
                    font-size: 11px;
                    margin: '.$body_margin.';
                }

                .container {
                    // position: fixed;
                }

                .fh {
                    margin: '.$rmrd['marginfh'].';
                    height: '.$rmrd['hhfhef'].'px;
                    position: absolute;
                }

                .oh {
                    margin: '.$rmrd['marginoh'].';
                    height: '.$rmrd['hhohof'].'px;
                }

                .eh {
                    margin: '.$rmrd['margineh'].';
                    height: '.$rmrd['hhehef'].'px;
                }

                .of {
                    margin: '.$rmrd['marginof'].';
                    height: '.$rmrd['hfohof'].'px;
                }

                .ef0 {
                    margin: '.$rmrd['marginef'].';
                    height: '.$rmrd['hfef0'].'px;
                }

                .ef0_ {
                    margin: '.$rmrd['marginef'].';
                    height: '.$rmrd['hfef0_'].'px;
                }

                .ef {
                    margin: '.$rmrd['marginef'].';
                    height: '.$rmrd['hfehef'].'px;
                }

                .footer {
                    position: fixed;
                    margin: '.$body_margin.';
                }

                .font-size-address {
                    font-size: 10px
                }

                .img-logo {
                    height: 50px;
                    margin: 0px 0px 0px 10px;
                }

                .address {
                    margin: -60px 0px 0px 65px;
                    font-size: 8px;
                }

                .title {
                    position: absolute;
                    z-index: 10;
                    margin: -48px 0px 80px '.$headermiddleml.'px;
                    font-size: 12px;
                }

                .title div {
                    border-style: double;
                    padding: 3px 20px 3px 20px;
                    width: 200px;
                }

                .title b {
                    font-size: 20px;
                }

                .title p {
                    margin: 10px 0px 0px 0px;
                }

                .date {
                    margin: -65px 0px 20px '.$headerlastml.'px;
                    font-size: 12px;
                }

                .dibayar-kepada {
                    margin: 0px 0px 0px 15px;
                }

                /////////////////////

                .content-1-even-ef0 {
                    height: '.$rmrd['hdfhef'].'px;
                }

                .content-1-even-eh-ef {
                    height: '.$rmrd['hd'].'px;
                }

                .content-1-even-eh {
                    height: '.$rmrd['hd'].'px;
                }

                .content-1-even-ef {
                    height: '.$rmrd['hdehef'].'px;
                }

                .content-1-even {
                    height: '.$rmrd['hd'].'px;
                }

                .content-1-odd-oh-of {
                    height: '.$rmrd['hd'].'px;
                }

                .content-1-odd-oh {
                    height: '.$rmrd['hd'].'px;
                }

                .content-1-odd-of {
                    height: '.$rmrd['hdohof'].'px;
                }

                .content-1-odd {
                    height: '.$rmrd['hd'].'px;
                }

                ///////////////

                .content-1 {
                    height: '.$rmrd['hd'].'px;
                }
                
                .table-custom-1 {
                    // width: 100%;
                    margin: 0px 0px 0px 0px;
                    border-collapse: collapse;
                }

                .table-custom-1, .table-custom-1 thead tr th, .table-custom-1 tbody tr td {
                    border: 1px solid black;
                    padding: 3px 3px 3px 3px;
                }
                
                .table-custom-1 thead tr th:nth-child(1) {
                    width: '.$content1width[0].'px;
                }

                .table-custom-1 thead tr th:nth-child(2) {
                    width: '.$content1width[1].'px;
                }

                .table-custom-1 thead tr th:nth-child(3) {
                    width: '.$content1width[2].'px;
                }

                .table-custom-1 thead tr th:nth-child(4) {
                    width: '.$content1width[3].'px;
                }

                .table-custom-1 thead tr th:nth-child(5) {
                    width: '.$content1width[4].'px;
                }

                .table-custom-1 thead tr th:nth-child(6) {
                    width: '.$content1width[5].'px;
                }

                .table-custom-1 tbody tr .giro-cek {
                    border-left: 1px solid white !important;
                    border-bottom: 1px solid white !important;
                }

                .giro-cek b {
                    margin-bottom: 0px !important;
                }



                .table-custom-2 {
                    // width: 100%;
                    border-collapse: collapse;
                }

                .table-custom-2 thead tr th:nth-child(1) {
                    width: '.$content2width[0].'px;
                }
                
                .table-custom-2 thead tr th:nth-child(2) {
                    width: '.$content2width[1].'px;
                }

                .table-custom-2 thead tr th:nth-child(3) {
                    width: '.$content2width[2].'px;
                }

                .table-custom-2 thead tr th:nth-child(4) {
                    width: '.$content2width[3].'px;
                }

                .table-custom-2 thead tr th:nth-child(5) {
                    width: '.$content2width[4].'px;
                }

                .table-custom-2, .table-custom-2 thead tr th, .table-custom-2 tbody tr td {
                    border: 1px solid black;
                    padding: 5px 3px 5px 3px;
                    text-align: center;
                }
                
                .text-right {
                    text-align: right;
                }
                '
        ;
        
        
        $html = str_replace('$css', $css, $html);
        
        /////////
        if (count($resultdetail) >= $showdatapertable) {
            $qtydatadetail = count($resultdetail)-count($resultdetail)%$showdatapertable;
            $qtydatadetail = $qtydatadetail/$showdatapertable;
            if (count($resultdetail)%$showdatapertable != 0) {
                $qtydatadetail++;
            }
        } else {
            $qtydatadetail = 1;
        }

        $start = 0;
        $finish = $showdatapertable-1;
        for ($i=0; $i<$qtydatadetail; $i++) {
            if ($i == 0) {
                if ($rmrd['sdpt0'] != $showdatapertable) {
                    $plusfinish = $rmrd['sdpt0'];
                } else {
                    $plusfinish = $showdatapertable;
                }
            } else {
                if ($i%2 == 0) {
                    if ($rmrd['sdpteh'] != $showdatapertable) {
                        $plusfinish = $rmrd['sdpteh'];
                    } else {
                        $plusfinish = $showdatapertable;
                    }
                    if ($rmrd['sdptef'] != $showdatapertable) {
                        $plusfinish = $rmrd['sdptef'];
                    } else {
                        $plusfinish = $showdatapertable;
                    }
                } else {
                    if ($rmrd['sdptoh'] != $showdatapertable) {
                        $plusfinish = $rmrd['sdptoh'];
                    } else {
                        $plusfinish = $showdatapertable;
                    }
                    if ($rmrd['sdptof'] != $showdatapertable) {
                        $plusfinish = $rmrd['sdptof'];
                    } else {
                        $plusfinish = $showdatapertable;
                    }
                }
            }
            $finish = $start+$plusfinish-1;
            $dataqtydatadetail[] = (Object)[
                'start' => $start,
                'finish' => $finish
            ];
            $start = $finish+1;
            $finish = $start+($plusfinish-1);
        }
        // dd($dataqtydatadetail);

        $qlr_ = [];
        foreach ($dataqtydatadetail as $k => $v) {
            $qlr_[$k] = 0;
            foreach ($resultdetail as $k2 => $v2) {
                if ($k2 >= $v->start && $k2 <= $v->finish) {
                    $qlr_[$k]++;
                }
            }
        }

        for ($i=count($qlr_)-1; $i>0; $i--) {
            if ($qlr_[$i] != 0) {
                break;
            }
            array_pop($qlr_);
            array_pop($dataqtydatadetail);
        }
        // die;
        // dd($qlr_[array_key_last($qlr_)]);

        if ($template->defaultpaper == 'A4') {
            if ($template->ispotrait) {
                $rowenterrow = 21;
            } else {
                $rowenterrow = 21;
            }
        } else if ($template->defaultpaper == 'A5') {
            if ($template->ispotrait) {
                $rowenterrow = 10;
            } else {
                $rowenterrow = 10;
            }
        }

        $isenterrow = false;
        if ($rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef']) {
            if ($qlr_[array_key_last($qlr_)] > $rowenterrow) {
                array_push($qlr_, $qlr_[array_key_last($qlr_)]-$rowenterrow);
                $qlr_[count($qlr_)-2] -= $qlr_[array_key_last($qlr_)];
                $dataqtydatadetail[array_key_last($dataqtydatadetail)]->finish = $dataqtydatadetail[array_key_last($dataqtydatadetail)]->start+$qlr_[array_key_last($dataqtydatadetail)]-1;
                // dd(count($resultdetail)-1);
                if ($dataqtydatadetail[array_key_last($dataqtydatadetail)]->finish <= count($resultdetail)-1) {
                    array_push($dataqtydatadetail, (object)[
                        'start' => $dataqtydatadetail[array_key_last($dataqtydatadetail)]->finish+1,
                        'finish' => $dataqtydatadetail[array_key_last($dataqtydatadetail)]->finish+1+($showdatapertable*2)
                    ]);
                }
                // dd($dataqtydatadetail);
                $isenterrow = true;
            }
        }
        
        // dd($qlr_);
        // dd($dataqtydatadetail);

        $content = '';
        $total = 0;
        $no = 1;
        $qtylastrecord = [];
        foreach ($dataqtydatadetail as $k => $v) {
            if ($k == 0) {
                $content .= @$rmrd['fh'];
                $content .= '<div style="position: absolute; margin-top: 490px">'.$template->reportheader.'</div>';
            }

            $record = '';
            $qtylasttable = 0;
            $subtotal = 0;
            $isappend = false;
            $qtylastrecord[$k] = 0;
            foreach ($resultdetail as $k2 => $v2) {
                if ($k2 >= $v->start && $k2 <= $v->finish) {
                    $isappend = true;
                    if (@$v2->keterangan) {
                        if (strlen($v2->keterangan) >= $charketerangan) {
                            $keterangan = @substr($v2->keterangan, 0, $charketerangan).'...';
                        } else {
                            $keterangan = $v2->keterangan;
                        }
                    } else {
                        $keterangan = '';
                    }
                    $record .= "<tr>
                                    <td>".$no."</td>
                                    <td>".@$keterangan."</td>
                                    <td>".@$v2->idso."</td>
                                    <td>".@$v2->idpo."</td>
                                    <td>".@$v2->idpo."</td>
                                    <td style='text-align: right'>".@$v2->nominal."</td>
                                </tr>";
                    $no++;
                    $qtylasttable++;
                    $subtotal += @$v2->nominal;
                    // if (count($dataqtydatadetail) == $k+1) {
                        $qtylastrecord[$k]++;
                    // }
                }
            }
            // if (count($dataqtydatadetail) == $k+1) {
            //     dd($qtylastrecord);
            // }
            // die;

            if ($isappend) {
                if ($k == 0) {
                    $content .= '<div class="content-1-even-ef0">'.$template->reporttemplate.'</div>';
                } else {
                    if ($k%2 == 0) {
                        $content .= '<div class="content-1-even-ef">'.$template->reporttemplate.'</div>';
                    } else {
                        $content .= '<div class="content-1-even-of">'.$template->reporttemplate.'</div>';
                    }
                }
                $total += $subtotal;
                $content = str_replace('$datadetail', $record, $content);
                $content = str_replace('$subtotal_jumlah_datadetail', $subtotal, $content);
                $content = str_replace('$tax_jumlah_datadetail', 0, $content);
                $content = str_replace('$total_jumlah_datadetail', $total, $content);
            }
            
            if (count($dataqtydatadetail) == $k+1) {
                $showlf = $rmrd['showlf'];
                if ($template->defaultpaper == 'A4') {
                    if ($template->ispotrait) {
                        if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                            if (count($qtylastrecord)%2 == 1 && $qtylastrecord[count($dataqtydatadetail)-1] == 0) {
                                $showlf = false;
                            }
                        }
                    }
                } else if ($template->defaultpaper == 'A3') {
                    if ($template->ispotrait) {
                        if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                            if (count($qtylastrecord)%2 == 1 && $qtylastrecord[count($dataqtydatadetail)-1] == 0) {
                                $showlf = false;
                            }
                        }
                    }
                }

                if ($rmrd['isshoweverypagelf']) {
                    // if ($qtylastrecord == 2) {
                    //     $rmrd['marginlf'] = '62px 0px 0px 0px';
                    // } else if ($qtylastrecord == 3) {
                    //     $rmrd['marginlf'] = '62px 0px 0px 0px';
                    // } else if ($qtylastrecord == 4) {
                        // if ($k%2 == 1) {
                            $qlr = 0;
                            for ($i=count($qtylastrecord)-1; $i>=0; $i--) {
                                if ($qtylastrecord[$i] != 0) {
                                    $qlr = $qtylastrecord[$i];
                                    break;
                                }
                            }

                            if ($template->defaultpaper == 'A4') {
                                if ($template->ispotrait) {
                                    if ($rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 760;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else if (!$rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 830;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else if ($rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 760;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else if (!$rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 830;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else {
                                        $mtlf = 0;
                                    }
                                } else {
                                    if (!$rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 500;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else if ($rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 450;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else {
                                        // dd($qlr);
                                        if ($rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef']) {
                                            if ($k%2 == 0 && $k != 0) {
                                                $mtlf = 0;
                                            } else {
                                                $mtlf = 440;
                                                for ($i=0; $i<$qlr; $i++) {
                                                    $mtlf -= 20;
                                                }
                                            }
                                        } else {
                                            // dd($qlr);
                                            // if ($k%2 == 0) {
                                                $mtlf = 0;
                                            // } else {
                                            //     $mtlf = 440;
                                            //     for ($i=0; $i<$qlr; $i++) {
                                            //         $mtlf -= 20;
                                            //     }
                                            // }
                                        }
                                    }
                                }
                            } else if ($template->defaultpaper == 'A3') {
                                if ($template->ispotrait) {
                                    if ($rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 760;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else if (!$rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 830;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else if ($rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 760;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else if (!$rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 830;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else {
                                        $mtlf = 0;
                                    }
                                } else {
                                    if (!$rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 500;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else if ($rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 450;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else {
                                        // dd($qlr);
                                        if ($rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef']) {
                                            if ($k%2 == 0 && $k != 0) {
                                                $mtlf = 0;
                                            } else {
                                                $mtlf = 440;
                                                for ($i=0; $i<$qlr; $i++) {
                                                    $mtlf -= 20;
                                                }
                                            }
                                        } else {
                                            // dd($qlr);
                                            // if ($k%2 == 0) {
                                                $mtlf = 0;
                                            // } else {
                                            //     $mtlf = 440;
                                            //     for ($i=0; $i<$qlr; $i++) {
                                            //         $mtlf -= 20;
                                            //     }
                                            // }
                                        }
                                    }
                                }
                            } else if ($template->defaultpaper == 'A5') {
                                if ($template->ispotrait) {
                                    if ($rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 760;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else if (!$rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 830;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else if ($rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 60;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else if (!$rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 830;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else {
                                        $mtlf = 0;
                                    }
                                } else {
                                    if ($rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 210;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else if (!$rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 500;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else if ($rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = -50;
                                        } else {
                                            $mtlf = 210;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else {
                                        // dd($qlr);
                                        if ($rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef']) {
                                            if ($k%2 == 0 && $k != 0) {
                                                $mtlf = 0;
                                            } else {
                                                $mtlf = 440;
                                                for ($i=0; $i<$qlr; $i++) {
                                                    $mtlf -= 20;
                                                }
                                            }
                                        } else {
                                            // dd($qlr);
                                            // if ($k%2 == 0) {
                                                $mtlf = 0;
                                            // } else {
                                            //     $mtlf = 440;
                                            //     for ($i=0; $i<$qlr; $i++) {
                                            //         $mtlf -= 20;
                                            //     }
                                            // }
                                        }
                                    }
                                }
                            }
                            $rmrd['marginlf'] = $mtlf.'px 0px 0px 0px';
                        // }
                    // }
                    // echo $qlr.'<br>';die;
                    if ($showlf) {
                        $content .= '<div style="margin: '.$rmrd['marginlf'].'; height: '.$heightlf.'px">'.$template->reportfooter.'</div>';
                    }
                } else {
                    $content .= '<div style="margin: '.$rmrd['marginlf'].'; height: '.$heightlf.'px"></div>';
                }
            } else if (count($dataqtydatadetail) == $k+2 && $isenterrow) {
                if ($template->defaultpaper == 'A4') {
                    if (count($dataqtydatadetail)%2 == 0) {
                        // $content .= '<div style="margin-top: 20px">&nbsp;</div>';
                    } else {
                        // $content .= '<div style="margin-top: 20px">&nbsp;</div>';
                    }
                } else if ($template->defaultpaper == 'A5') {
                    if (count($dataqtydatadetail)%2 == 0) {
                        $content .= '<div style="margin-top: 20px">&nbsp;</div>';
                    } else {
                        $content .= '<div style="margin-top: 40px">&nbsp;</div>';
                    }
                }
                $content .= @$rmrd['oh'];
            } else {
                // if ($isappend) {
                    if ($k == 0) {
                        // $content .= @$rmrd['ef0'];
                        // $content .= @$rmrd['oh'];
                    } else {
                        // dd($rmrd);
                        if ($k%2 == 0) {
                            $content .= @$rmrd['ef'];
                            if (count($dataqtydatadetail) > $k+2) {
                                $content .= @$rmrd['oh'];
                            } else {
                                // if ($rmrd['showof']) {
                                    $content .= @$rmrd['oh'];
                                // } else {
                                //     $content .= '<div>&nbsp;</div>';
                                // }
                            }
                        } else {
                            $content .= @$rmrd['of'];
                            if (count($dataqtydatadetail) > $k+2) {
                                $content .= @$rmrd['eh'];
                            } else {
                                // if ($rmrd['showef']) {
                                    $content .= @$rmrd['eh'];
                                // } else {
                                //     $content .= '<div>&nbsp;</div>';
                                // }
                            }
                        }
                    }
                // }
            }
        }

        // dd($dataqtydatadetail);
        /////////

        $html = str_replace('$content', $content, $html);
        $html = str_replace('$logo', '<img src="assets/lycon.jpeg" alt="" class="img-logo">', $html);
        $html = str_replace('$genmcompany_nama', $resultheader->genmcompany_nama, $html);
        $html = str_replace('$today', date('d M Y'), $html);
        $html = str_replace('$kode', $resultheader->$tablemaster_kode, $html);
        $html = str_replace('$revdate', $resultheader->$tablemaster_revno.'/'.date('d M Y'), $html);

        $pdf = PDF::loadHTML($html);
        $pdf->setPaper($template->defaultpaper, $ispotrait);
        return $pdf->stream('users.pdf');
    }

    public function print_purchase_request($noid, $urlprint) {
        $pdf = PDF::loadHTML('
        <!DOCTYPE html>
                        <html lang="">
                            <head>
                                <meta charset="utf-8">
                                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                                <meta name="viewport" content="width=device-width, initial-scale=1">
                                <title>CASH IN LIST REPORT</title>
                                <style>
                                    body {
                            font-family: "MS Serif", "New York", sans-serif;
                            font-size: 11px;
                            margin: 0 0 0 0;
                        }
        
                        .container {
                            // position: fixed;
                        }
        
                        .footer {
                            position: fixed;
                            margin: 0 0 0 0;
                        }
        
                        .font-size-address {
                            font-size: 10px
                        }
        
                        .img-logo {
                            height: 50px;
                            margin: 0px 0px 0px 10px;
                        }
        
                        .address {
                            margin: -60px 0px 0px 65px;
                            font-size: 8px;
                        }
        
                        .title {
                            position: absolute;
                            z-index: 10;
                            margin: -48px 0px 80px 100px;
                            font-size: 12px;
                        }
        
                        .title div {
                            border-style: double;
                            padding: 3px 20px 3px 20px;
                            width: 200px;
                        }
        
                        .title b {
                            font-size: 20px;
                        }
        
                        .title p {
                            margin: 10px 0px 0px 0px;
                        }
        
                        .date {
                            margin: -65px 0px 20px 480px;
                            font-size: 12px;
                        }
        
                        .dibayar-kepada {
                            margin: 0px 0px 0px 15px;
                        }
        
                        .content-1 {
                            height: 300px;
                        }
                        
                        .table-custom-1 {
                            width: 100%;
                                            table-layout: fixed;
                            margin: 0px 0px 0px 0px;
                            border-collapse: collapse;
                        }
        
                        .table-custom-1, .table-custom-1 thead tr th, .table-custom-1 tbody tr td {
                            border: 1px solid black;
                            padding: 3px 3px 3px 3px;
                        }
                        
                        .table-custom-1 thead tr th:nth-child(1) {
                            width: 4px;
                        }
        
                        .table-custom-1 thead tr th:nth-child(2) {
                            width: 46px;
                        }
        
                        .table-custom-1 thead tr th:nth-child(3) {
                            width: 12px;
                        }
        
                        .table-custom-1 thead tr th:nth-child(4) {
                            width: 12px;
                        }
        
                        .table-custom-1 thead tr th:nth-child(5) {
                            width: 12px;
                        }
        
                        .table-custom-1 thead tr th:nth-child(6) {
                            width: 14px;
                        }
        
                        .table-custom-1 tbody tr .giro-cek {
                            border-left: 1px solid white !important;
                            border-bottom: 1px solid white !important;
                        }
        
                        .giro-cek b {
                            margin-bottom: 0px !important;
                        }
        
        
        
                        .table-custom-2 {
                            // width: 100%;
                            border-collapse: collapse;
                        }
        
                        .table-custom-2 thead tr th:nth-child(1) {
                            width: 145px;
                        }
                        
                        .table-custom-2 thead tr th:nth-child(2) {
                            width: 145px;
                        }
        
                        .table-custom-2 thead tr th:nth-child(3) {
                            width: 145px;
                        }
        
                        .table-custom-2 thead tr th:nth-child(4) {
                            width: 145px;
                        }
        
                        .table-custom-2 thead tr th:nth-child(5) {
                            width: 80px;
                        }
        
                        .table-custom-2, .table-custom-2 thead tr th, .table-custom-2 tbody tr td {
                            border: 1px solid black;
                            padding: 5px 3px 5px 3px;
                            text-align: center;
                        }
                        
                        .text-right {
                            text-align: right;
                        }
        
                        .page-break {
                            page-break-after: always;
                        }
                        
                                    
                                    #watermark1 {
                                        position: fixed;
                                        top:   21cm;
                                        // left:     1.5cm;
                                        width:    8cm;
                                        height:   8cm;
                                        z-index:  -1000;
                                        transform: rotate(300deg); 
                                        color: #f9f8f7;
                                        font-size: 50px;
                                    }
        
                                    #watermark2 {
                                        position: fixed;
                                        top:   10cm;
                                        left:     -5cm;
                                        width:    8cm;
                                        height:   8cm;
                                        z-index:  -1000;
                                        transform: rotate(300deg); 
                                        color: #f9f8f7;
                                        font-size: 50px;
                                    }
                                </style>
                            </head>
                            <body>
                                <div id="watermark1"> 
                                    <h1>PT.&nbsp;LYCON&nbsp;ASIA&nbsp;MANDIRI</h1>
                                </div> 
        
                                <div id="watermark2"> 
                                    <h1>PT.&nbsp;LYCON&nbsp;ASIA&nbsp;MANDIRI</h1>
                                </div> 
        
                                <div style="height: 50"><span style="color: orange"><b>FH</b></span>
                            <img src="assets/lycon.jpeg" alt="" class="img-logo">
                            <div class="address">
                                <b>-</b><br>
                                Manyar Megah Indah Plaza Block E 22.<br>
                                Surabaya - East Java<br>
                                Indonesia 60174<br>
                                Phone &nbsp;&nbsp;: +62 31504116; 315034346<br>
                                Fax &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: +62 31 5020116
                                <div class="title">
                                    <div>
                                        <b>BUKTI KAS KELUAR</b><br>
                                    </div>
                                    <p>No.   : CASHBANKKKB-00021/03/2021 </p>
                                </div>
                                <div class="date">
                                    <p>Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 02 Apr 2021</p>
                                    <p>Rev./Date &nbsp;: /02 Apr 2021</p>
                                </div>
                            </div></div><div style="height: 307px"><div class="content-1">
                            <b class="dibayar-kepada">DIBAYAR KEPADA :  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ </b>
                            <table class="table-custom-1">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th><span style="color: blue"><b>FD</b></span> Keterangan</th>
                                        <th>No. SO</th>
                                        <th>No. PO</th>
                                        <th>KODE ACC.</th>
                                        <th>JUMLAH</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr><td style="width: 4%; text-align: left;">1</td><td style="width: 46%; text-align: left;">TEST&nbsp;LAGI&nbsp;LAH</td><td style="width: 12%; text-align: right;">0</td><td style="width: 12%; text-align: right;">0</td><td style="width: 12%; text-align: right;">0</td><td style="width: 14%; text-align: right;">9999</td></tr>
                                    <tr>
                                        <td colspan="4" class="giro-cek">
                                        </td>
                                        <td class="text-right"><b>SUBTOTAL</b></td>
                                        <td class="text-right">9999</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="giro-cek">
                                        </td>
                                        <td class="text-right"><b>TAX</b></td>
                                        <td class="text-right">0</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="giro-cek">
                                            <b>GIRO/CEK No : ______________________________________________________</b>
                                        </td>
                                        <td class="text-right"><b>TOTAL</b></td>
                                        <td class="text-right">9999</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div></div><div style="height: 60"><table class="table-custom-2 table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center"><span style="color: red">FF</span> Issued by</th>
                                        <th class="text-center">Verification by</th>
                                        <th class="text-center">Approved by</th>
                                        <th class="text-center">Received by</th>
                                        <th class="text-center">Date :</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    <tr>
                                        <td>
                                            <br>
                                            <br>
                                            Finance Division
                                        </td>
                                        <td>
                                            <br>
                                            <br>
                                            Accounting Division
                                        </td>
                                        <td>
                                            <br>
                                            <br>
                                            Director/ Marketing Manager
                                        </td>
                                        <td>
                                            <br>
                                            <br>
                                            ...........................
                                        </td>
                                        <td>
                                            <br>
                                            <br>
                                            <br>
                                        </td>
                                    </tr>
                                </tbody>
                            </table></div>
                            </body>
                        </html>');
        $pdf->setPaper('A5', 'landscape');
        // print_r($html);die;
        return $pdf->stream('users.pdf');
    }

    public function getRMRD($params) {
        $showdatapertable = $params['showdatapertable'];
        $resultrptmreport = $params['resultrptmreport'];
        $resultrptmreportdetail = $params['resultrptmreportdetail'];
        
        $rmrd = [];
        foreach ($resultrptmreportdetail as $k => $v) {
            if ($v->nama == 'First Header') {
                if ($v->isshowfirstpage) {
                    $rmrd['showfh'] = true;
                } else {
                    $rmrd['showfh'] = false;
                }
                $rmrd['heightfh'] = $v->widgetheight;
                if ($v->isshowfirstpage) {
                    $rmrd['showfhfp'] = true;
                } else {
                    $rmrd['showfhfp'] = false;
                }
                if ($v->isshoweverypage) {
                    $rmrd['showfhep'] = true;
                } else {
                    $rmrd['showfhep'] = false;
                }
                if ($v->isshowlastpage) {
                    $rmrd['showfhlp'] = true;
                } else {
                    $rmrd['showfhlp'] = false;
                }
            } else if ($v->nama == 'First Detail') {
                if ($v->isshowfirstpage) {
                    $rmrd['showfd'] = true;
                } else {
                    $rmrd['showfd'] = false;
                }
                $rmrd['heightfd'] = $v->widgetheight;
                if ($v->isshowfirstpage) {
                    $rmrd['showfdfp'] = true;
                } else {
                    $rmrd['showfdfp'] = false;
                }
                if ($v->isshoweverypage) {
                    $rmrd['showfdep'] = true;
                } else {
                    $rmrd['showfdep'] = false;
                }
                if ($v->isshowlastpage) {
                    $rmrd['showfdlp'] = true;
                } else {
                    $rmrd['showfdlp'] = false;
                }
            } else if ($v->nama == 'First Footer') {
                if ($v->isshowfirstpage) {
                    $rmrd['showff'] = true;
                } else {
                    $rmrd['showff'] = false;
                }
                $rmrd['heightff'] = $v->widgetheight;
                if ($v->isshowfirstpage) {
                    $rmrd['showfffp'] = true;
                } else {
                    $rmrd['showfffp'] = false;
                }
                if ($v->isshoweverypage) {
                    $rmrd['showffep'] = true;
                } else {
                    $rmrd['showffep'] = false;
                }
                if ($v->isshowlastpage) {
                    $rmrd['showfflp'] = true;
                } else {
                    $rmrd['showfflp'] = false;
                }
            } else if ($v->nama == 'Other Header') {
                if ($v->isshoweverypage) {
                    $rmrd['showothh'] = true;
                } else {
                    $rmrd['showothh'] = false;
                }
                $rmrd['heightothf'] = $v->widgetheight;
                if ($v->isshowfirstpage) {
                    $rmrd['showothhfp'] = true;
                } else {
                    $rmrd['showothhfp'] = false;
                }
                if ($v->isshoweverypage) {
                    $rmrd['showothhep'] = true;
                } else {
                    $rmrd['showothhep'] = false;
                }
                if ($v->isshowlastpage) {
                    $rmrd['showothhlp'] = true;
                } else {
                    $rmrd['showothhlp'] = false;
                }
            } else if ($v->nama == 'Other Detail') {
                if ($v->isshoweverypage) {
                    $rmrd['showothd'] = true;
                } else {
                    $rmrd['showothd'] = false;
                }
                $rmrd['heightothd'] = $v->widgetheight;
                if ($v->isshowfirstpage) {
                    $rmrd['showothdfp'] = true;
                } else {
                    $rmrd['showothdfp'] = false;
                }
                if ($v->isshoweverypage) {
                    $rmrd['showothdep'] = true;
                } else {
                    $rmrd['showothdep'] = false;
                }
                if ($v->isshowlastpage) {
                    $rmrd['showothdlp'] = true;
                } else {
                    $rmrd['showothdlp'] = false;
                }
            } else if ($v->nama == 'Other Footer') {
                if ($v->isshoweverypage) {
                    $rmrd['showothf'] = true;
                } else {
                    $rmrd['showothf'] = false;
                }
                $rmrd['heightothf'] = $v->widgetheight;
                if ($v->isshowfirstpage) {
                    $rmrd['showothffp'] = true;
                } else {
                    $rmrd['showothffp'] = false;
                }
                if ($v->isshoweverypage) {
                    $rmrd['showothfep'] = true;
                } else {
                    $rmrd['showothfep'] = false;
                }
                if ($v->isshowlastpage) {
                    $rmrd['showothflp'] = true;
                } else {
                    $rmrd['showothflp'] = false;
                }
            } else if ($v->nama == 'Last Header') {
                if ($v->isshowlastpage) {
                    $rmrd['showlh'] = true;
                } else {
                    $rmrd['showlh'] = false;
                }
                $rmrd['heightlh'] = $v->widgetheight;
                if ($v->isshowfirstpage) {
                    $rmrd['showlhfp'] = true;
                } else {
                    $rmrd['showlhfp'] = false;
                }
                if ($v->isshoweverypage) {
                    $rmrd['showlhep'] = true;
                } else {
                    $rmrd['showlhep'] = false;
                }
                if ($v->isshowlastpage) {
                    $rmrd['showlhlp'] = true;
                } else {
                    $rmrd['showlhlp'] = false;
                }
            } else if ($v->nama == 'Last Detail') {
                if ($v->isshowlastpage) {
                    $rmrd['showld'] = true;
                } else {
                    $rmrd['showld'] = false;
                }
                $rmrd['heightld'] = $v->widgetheight;
                if ($v->isshowfirstpage) {
                    $rmrd['showldfp'] = true;
                } else {
                    $rmrd['showldfp'] = false;
                }
                if ($v->isshoweverypage) {
                    $rmrd['showldep'] = true;
                } else {
                    $rmrd['showldep'] = false;
                }
                if ($v->isshowlastpage) {
                    $rmrd['showldlp'] = true;
                } else {
                    $rmrd['showldlp'] = false;
                }
            } else if ($v->nama == 'Last Footer') {
                if ($v->isshowlastpage) {
                    $rmrd['showlf'] = true;
                } else {
                    $rmrd['showlf'] = false;
                }
                $rmrd['heightlf'] = $v->widgetheight;
                if ($v->isshowfirstpage) {
                    $rmrd['showlffp'] = true;
                } else {
                    $rmrd['showlffp'] = false;
                }
                if ($v->isshoweverypage) {
                    $rmrd['showlfep'] = true;
                } else {
                    $rmrd['showlfep'] = false;
                }
                if ($v->isshowlastpage) {
                    $rmrd['showlflp'] = true;
                } else {
                    $rmrd['showlflp'] = false;
                }
            } else if ($v->nama == 'Odd Header') {
                if ($v->isshoweverypage) {
                    $rmrd['showoh'] = true;
                } else {
                    $rmrd['showoh'] = false;
                }
                $rmrd['heightoh'] = $v->widgetheight;
                if ($v->isshowfirstpage) {
                    $rmrd['showohfp'] = true;
                } else {
                    $rmrd['showohfp'] = false;
                }
                if ($v->isshoweverypage) {
                    $rmrd['showohep'] = true;
                } else {
                    $rmrd['showohep'] = false;
                }
                if ($v->isshowlastpage) {
                    $rmrd['showohlp'] = true;
                } else {
                    $rmrd['showohlp'] = false;
                }
            } else if ($v->nama == 'Even Header') {
                if ($v->isshoweverypage) {
                    $rmrd['showeh'] = true;
                } else {
                    $rmrd['showeh'] = false;
                }
                $rmrd['heighteh'] = $v->widgetheight;
                if ($v->isshowfirstpage) {
                    $rmrd['showehfp'] = true;
                } else {
                    $rmrd['showehfp'] = false;
                }
                if ($v->isshoweverypage) {
                    $rmrd['showehep'] = true;
                } else {
                    $rmrd['showehep'] = false;
                }
                if ($v->isshowlastpage) {
                    $rmrd['showehlp'] = true;
                } else {
                    $rmrd['showehlp'] = false;
                }
            } else if ($v->nama == 'Odd Detail') {
                if ($v->isshoweverypage) {
                    $rmrd['showod'] = true;
                } else {
                    $rmrd['showod'] = false;
                }
                $rmrd['heightod'] = $v->widgetheight;
                if ($v->isshowfirstpage) {
                    $rmrd['showodfp'] = true;
                } else {
                    $rmrd['showodfp'] = false;
                }
                if ($v->isshoweverypage) {
                    $rmrd['showodep'] = true;
                } else {
                    $rmrd['showodep'] = false;
                }
                if ($v->isshowlastpage) {
                    $rmrd['showodlp'] = true;
                } else {
                    $rmrd['showodlp'] = false;
                }
            } else if ($v->nama == 'Even Detail') {
                if ($v->isshoweverypage) {
                    $rmrd['showed'] = true;
                } else {
                    $rmrd['showed'] = false;
                }
                $rmrd['heighted'] = $v->widgetheight;
                if ($v->isshowfirstpage) {
                    $rmrd['showedfp'] = true;
                } else {
                    $rmrd['showedfp'] = false;
                }
                if ($v->isshoweverypage) {
                    $rmrd['showedep'] = true;
                } else {
                    $rmrd['showedep'] = false;
                }
                if ($v->isshowlastpage) {
                    $rmrd['showedlp'] = true;
                } else {
                    $rmrd['showedlp'] = false;
                }
            } else if ($v->nama == 'Odd Footer') {
                if ($v->isshoweverypage) {
                    $rmrd['showof'] = true;
                } else {
                    $rmrd['showof'] = false;
                }
                $rmrd['heightof'] = $v->widgetheight;
                if ($v->isshowfirstpage) {
                    $rmrd['showoffp'] = true;
                } else {
                    $rmrd['showoffp'] = false;
                }
                if ($v->isshoweverypage) {
                    $rmrd['showofep'] = true;
                } else {
                    $rmrd['showofep'] = false;
                }
                if ($v->isshowlastpage) {
                    $rmrd['showoflp'] = true;
                } else {
                    $rmrd['showoflp'] = false;
                }
            } else if ($v->nama == 'Even Footer') {
                if ($v->isshoweverypage) {
                    $rmrd['showef'] = true;
                } else {
                    $rmrd['showef'] = false;
                }
                $rmrd['heightef'] = $v->widgetheight;
                if ($v->isshowfirstpage) {
                    $rmrd['showeffp'] = true;
                } else {
                    $rmrd['showeffp'] = false;
                }
                if ($v->isshoweverypage) {
                    $rmrd['showefep'] = true;
                } else {
                    $rmrd['showefep'] = false;
                }
                if ($v->isshowlastpage) {
                    $rmrd['showeflp'] = true;
                } else {
                    $rmrd['showeflp'] = false;
                }
            }
        }


        $rmrd['_heightfh'] = $rmrd['heightfh'];
        if ($rmrd['showfh']) {
            $rmrd['fh'] = $resultrptmreport->reportheader;
        } else {
            if ($rmrd['showof']) {
                $rmrd['fh'] = $resultrptmreport->reportheader;
            } else {
            }
        };

        if ($rmrd['showff']) { $rmrd['heightff'] -= 0; } else { $rmrd['heightff'] -= $rmrd['heightff']; };
        $rmrd['_heightoh'] = $rmrd['heightoh'];

        if ($rmrd['showof'] || $rmrd['showothf']) { $rmrd['heightof'] -= 0; $rmrd['of'] = $resultrptmreport->reportfooter; } else { $rmrd['heightof'] -= $rmrd['heightof']; };
        $rmrd['_heighteh'] = $rmrd['heighteh'];

        if ($rmrd['showef'] || $rmrd['showothf']) { $rmrd['heightef'] -= 0; $rmrd['ef'] = $resultrptmreport->reportfooter; } else { $rmrd['heightef'] -= $rmrd['heightef']; };
        if ($rmrd['showlf']) { $rmrd['heightlf'] -= 0; } else { $rmrd['heightlf'] -= $rmrd['heightlf']; };
        $rmrd['sdptf'] = $showdatapertable;

        if ($rmrd['showfhfp'] || $rmrd['showothhfp'] || $rmrd['showlhfp'] || $rmrd['showohfp'] || $rmrd['showehfp']) {
        } else {
            $rmrd['sdptf'] += 3;
            $rmrd['heightfd'] += $rmrd['heightfh']+16;
        }

        if ($rmrd['showfdfp'] || $rmrd['showothdfp'] || $rmrd['showldfp'] || $rmrd['showodfp'] || $rmrd['showedfp']) {
            $rmrd['sdptf'] = $showdatapertable;
        }

        if ($rmrd['showfffp'] || $rmrd['showothffp'] || $rmrd['showlffp'] || $rmrd['showoffp'] || $rmrd['showeffp']) {
        } else {
            $rmrd['sdptf'] += 3;
        }

        if ($rmrd['showfhep'] || $rmrd['showothhep'] || $rmrd['showlhep']) {
            $rmrd['sdpte'] = $showdatapertable;
            $rmrd['sdpto'] = $showdatapertable;
            if ($rmrd['showffep'] || $rmrd['showothfep'] || $rmrd['showlfep']) {
            } else if ($rmrd['showofep'] && $rmrd['showefep']) {
                $rmrd['sdpte'] = $showdatapertable;
                $rmrd['sdpto'] = $showdatapertable;
            } else if (!$rmrd['showofep'] && $rmrd['showefep']) {
                $rmrd['sdpte'] = $showdatapertable;
                $rmrd['sdpto'] = $showdatapertable+3;
            } else if ($rmrd['showofep'] && !$rmrd['showefep']) {
                $rmrd['sdpte'] = $showdatapertable+3;
                $rmrd['sdpto'] = $showdatapertable;
            } else {
                $rmrd['sdpte'] = $showdatapertable+3;
                $rmrd['sdpto'] = $showdatapertable+3;
            }
        } else if ($rmrd['showohep'] && $rmrd['showehep']) {
            $rmrd['sdpte'] = $showdatapertable;
            $rmrd['sdpto'] = $showdatapertable;
            if ($rmrd['showffep'] || $rmrd['showothfep'] || $rmrd['showlfep']) {
            } else if ($rmrd['showofep'] && $rmrd['showefep']) {
                $rmrd['sdpte'] += 0;
                $rmrd['sdpto'] += 0;
            } else if (!$rmrd['showofep'] && $rmrd['showefep']) {
                $rmrd['sdpte'] += 0;
                $rmrd['sdpto'] += 3;
            } else if ($rmrd['showofep'] && !$rmrd['showefep']) {
                $rmrd['sdpte'] += 3;
                $rmrd['sdpto'] += 0;
            } else {
                $rmrd['sdpte'] = $showdatapertable+3;
                $rmrd['sdpto'] = $showdatapertable+3;
            }
        } else if (!$rmrd['showohep'] && $rmrd['showehep']) {
            $rmrd['sdpte'] = $showdatapertable;
            $rmrd['sdpto'] = $showdatapertable+3;
            $rmrd['heightod'] += $rmrd['heightoh']+16;
            if ($rmrd['showffep'] || $rmrd['showothfep'] || $rmrd['showlfep']) {
            } else if ($rmrd['showofep'] && $rmrd['showefep']) {
                $rmrd['sdpte'] += 0;
                $rmrd['sdpto'] += 0;
            } else if (!$rmrd['showofep'] && $rmrd['showefep']) {
                $rmrd['sdpte'] += 0;
                $rmrd['sdpto'] += 3;
            } else if ($rmrd['showofep'] && !$rmrd['showefep']) {
                $rmrd['sdpte'] += 3;
                $rmrd['sdpto'] += 0;
            } else {
                $rmrd['sdpte'] += 3;
                $rmrd['sdpto'] += 3;
            }
        } else if ($rmrd['showohep'] && !$rmrd['showehep']) {
            $rmrd['sdpte'] = $showdatapertable+3;
            $rmrd['sdpto'] = $showdatapertable;
            $rmrd['heightod'] += $rmrd['heightoh']+16;
            if ($rmrd['showffep'] || $rmrd['showothfep'] || $rmrd['showlfep']) {
            } else if ($rmrd['showofep'] && $rmrd['showefep']) {
                $rmrd['sdpte'] += 0;
                $rmrd['sdpto'] += 0;
            } else if (!$rmrd['showofep'] && $rmrd['showefep']) {
                $rmrd['sdpte'] += 0;
                $rmrd['sdpto'] += 3;
            } else if ($rmrd['showofep'] && !$rmrd['showefep']) {
                $rmrd['sdpte'] += 3;
                $rmrd['sdpto'] += 0;
            } else {
                $rmrd['sdpte'] += 3;
                $rmrd['sdpto'] += 3;
            }
        } else if (!$rmrd['showohep'] && !$rmrd['showehep']) {
            $rmrd['sdpte'] = $showdatapertable+3;
            $rmrd['sdpto'] = $showdatapertable+3;
            if ($rmrd['showffep'] || $rmrd['showothfep'] || $rmrd['showlfep']) {
            } else if ($rmrd['showofep'] && $rmrd['showefep']) {
                $rmrd['sdpte'] += 0;
                $rmrd['sdpto'] += 0;
            } else if (!$rmrd['showofep'] && $rmrd['showefep']) {
                $rmrd['sdpte'] += 0;
                $rmrd['sdpto'] += 3;
            } else if ($rmrd['showofep'] && !$rmrd['showefep']) {
                $rmrd['sdpte'] += 3;
                $rmrd['sdpto'] += 0;
            } else {
                $rmrd['sdpte'] += 3;
                $rmrd['sdpto'] += 3;
            }
        } else {
            if ($rmrd['showohep']){
                $rmrd['sdpto'] = $showdatapertable;
            } else {
                $rmrd['sdpto'] = $showdatapertable+3;
            }
            if ($rmrd['showehep']){
                $rmrd['sdpte'] = $showdatapertable;
            } else {
                $rmrd['sdpte'] = $showdatapertable+3;
            }
        }

        if ($rmrd['showffep'] || $rmrd['showothfep'] || $rmrd['showlfep']) {
        } else {
        }
        $rmrd['sdptl'] = $showdatapertable;

        if ($rmrd['showfhlp'] || $rmrd['showothhlp'] || $rmrd['showlhlp'] || $rmrd['showohlp'] || $rmrd['showehlp']) {
        } else {
            $rmrd['sdptl'] += 3;
            $rmrd['heightld'] += $rmrd['heightlh']+16;
        }

        if ($rmrd['showfflp'] || $rmrd['showothflp'] || $rmrd['showlflp'] || $rmrd['showoflp'] || $rmrd['showeflp']) {
        } else {
            $rmrd['sdptl'] += 3;
        }

        $rmrd['sdpt0'] = $rmrd['sdptf'];
        $rmrd['sdptoh'] = $rmrd['sdpto'];
        $rmrd['sdptof'] = $rmrd['sdpto'];
        $rmrd['sdpteh'] = $rmrd['sdpte'];
        $rmrd['sdptef'] = $rmrd['sdpte'];

        return $rmrd;
    }

    public function getHTML($params) {
        $rptmreport = $params['rptmreport'];
        $datarec = $params['datarec'];
        $headermiddleml = $params['headermiddleml'];
        $headerlastml = $params['headerlastml'];
        $heightdetail = $params['heightdetail'];
        $content1width = $params['content1width'];
        $content2width = $params['content2width'];

        $html = '<!DOCTYPE html>
                <html lang="">
                    <head>
                        <meta charset="utf-8">
                        <meta http-equiv="X-UA-Compatible" content="IE=edge">
                        <meta name="viewport" content="width=device-width, initial-scale=1">
                        <title>'.$rptmreport->nama.'</title>
                        <style>
                            $css
                            
                            #watermark1 {
                                position: fixed;
                                top:   21cm;
                                // left:     1.5cm;
                                width:    8cm;
                                height:   8cm;
                                z-index:  -1000;
                                transform: rotate(300deg); 
                                color: #f9f8f7;
                                font-size: 50px;
                            }

                            #watermark2 {
                                position: fixed;
                                top:   10cm;
                                left:     -5cm;
                                width:    8cm;
                                height:   8cm;
                                z-index:  -1000;
                                transform: rotate(300deg); 
                                color: #f9f8f7;
                                font-size: 50px;
                            }
                        </style>
                    </head>
                    <body>
                        <div id="watermark1"> 
                            <h1>'.$rptmreport->reportwatermark.'</h1>
                        </div> 

                        <div id="watermark2"> 
                            <h1>'.$rptmreport->reportwatermark.'</h1>
                        </div> 

                        $content
                    </body>
                </html>';

        $body_margin = $rptmreport->margintop.' '.$rptmreport->marginright.' '.$rptmreport->marginbottom.' '.$rptmreport->marginleft;

        foreach ($datarec as $k => $v) {
            if ($v->fixed) {
                $isfixedcontent1 = '';
                break;
            } else {
                $isfixedcontent1 = 'width: 100%;
                                    table-layout: fixed;';
            }
        }
        $css = 'body {
                    font-family: "MS Serif", "New York", sans-serif;
                    font-size: 11px;
                    margin: '.$body_margin.';
                }

                .container {
                    // position: fixed;
                }

                .footer {
                    position: fixed;
                    margin: '.$body_margin.';
                }

                .font-size-address {
                    font-size: 10px
                }

                .img-logo {
                    height: 50px;
                    margin: 0px 0px 0px 10px;
                }

                .address {
                    margin: -60px 0px 0px 65px;
                    font-size: 8px;
                }

                .title {
                    position: absolute;
                    z-index: 10;
                    margin: -48px 0px 80px '.$headermiddleml.'px;
                    font-size: 12px;
                }

                .title div {
                    border-style: double;
                    padding: 3px 20px 3px 20px;
                    width: 230px;
                }

                .title b {
                    font-size: 20px;
                }

                .title p {
                    margin: 10px 0px 0px 0px;
                }

                .date {
                    margin: -65px 0px 20px '.$headerlastml.'px;
                    font-size: 12px;
                }

                .dibayar-kepada {
                    margin: 0px 0px 0px 15px;
                }

                .content-1 {
                    height: '.$heightdetail.'px;
                }
                
                .table-custom-1 {
                    '.$isfixedcontent1.'
                    margin: 0px 0px 0px 0px;
                    border-collapse: collapse;
                }

                .table-custom-1, .table-custom-1 thead tr th, .table-custom-1 tbody tr td {
                    border: 1px solid black;
                    padding: 3px 3px 3px 3px;
                }
                
                .table-custom-1 thead tr th:nth-child(1) {
                    width: '.$content1width[0].'px;
                }

                .table-custom-1 thead tr th:nth-child(2) {
                    width: '.$content1width[1].'px;
                }

                .table-custom-1 thead tr th:nth-child(3) {
                    width: '.$content1width[2].'px;
                }

                .table-custom-1 thead tr th:nth-child(4) {
                    width: '.$content1width[3].'px;
                }

                .table-custom-1 thead tr th:nth-child(5) {
                    width: '.$content1width[4].'px;
                }

                .table-custom-1 thead tr th:nth-child(6) {
                    width: '.@$content1width[5].'px;
                }

                .table-custom-1 tbody tr .giro-cek {
                    border-left: 1px solid white !important;
                    border-bottom: 1px solid white !important;
                }

                .giro-cek b {
                    margin-bottom: 0px !important;
                }



                .table-custom-2 {
                    // width: 100%;
                    border-collapse: collapse;
                }

                .table-custom-2 thead tr th:nth-child(1) {
                    width: '.$content2width[0].'px;
                }
                
                .table-custom-2 thead tr th:nth-child(2) {
                    width: '.$content2width[1].'px;
                }

                .table-custom-2 thead tr th:nth-child(3) {
                    width: '.$content2width[2].'px;
                }

                .table-custom-2 thead tr th:nth-child(4) {
                    width: '.$content2width[3].'px;
                }

                .table-custom-2 thead tr th:nth-child(5) {
                    width: '.$content2width[4].'px;
                }

                .table-custom-2, .table-custom-2 thead tr th, .table-custom-2 tbody tr td {
                    border: 1px solid black;
                    padding: 5px 3px 5px 3px;
                    text-align: center;
                }
                
                .text-right {
                    text-align: right;
                }

                .page-break {
                    page-break-after: always;
                }
                '
        ;
        
        
        return $html = str_replace('$css', $css, $html);
    }
    
    public function print_purchase_offer($params) {
        $page = $params['page'];
        $noid = $params['noid'];
        $tablemaster = 'purchaseoffer';
        $tabledetail = 'purchaseofferdetail';
        $currency = ['unitprice','subtotal'];


        // FIND IDREPORTDAFTAR
        $resultcmswidgetgrid = DB::table('cmswidgetgrid')
            ->select('idreportdaftar')
            ->where('noid', $page.'01')
            ->first();
        if (!$resultcmswidgetgrid) return ['success'=>false,'message'=>'Error get ID Report Daftar'];
        $idreportdaftar = $resultcmswidgetgrid->idreportdaftar;

        
        // FIND REPORT DETAIL
        $resultrptmreportdetail = DB::connection('mysql2')
            ->table('rptmreportdetail')
            ->select('rptmreportdetail.idreportwidget', 'rptmreportdetail.nama', 'rptmreportwidget.noid', 'rptmreportwidget.phpsource')
            ->join('rptmreportwidget', 'rptmreportwidget.noid', '=', 'rptmreportdetail.idreportwidget')
            ->where('idreport', $idreportdaftar)
            ->get();
        if (!$resultrptmreportdetail) return ['success'=>false,'message'=>'Error get rptmreportdetail'];


        // SET REPORT DETAIL
        $templatedetail = [];
        foreach ($resultrptmreportdetail as $k => $v) {
            $templatedetail[$v->nama] = $v;
        }


        // FIND TEMPLATE
        $resultrptmreport = DB::connection('mysql2')
            ->table('rptmreport')
            ->where('noid', $idreportdaftar)
            ->first();
        if (!$resultrptmreport) return ['success'=>false,'message'=>'Error get rptmreport'];
        $ispotrait = $resultrptmreport->ispotrait ? 'potrait' : 'landscape';
        $jsonrules = json_decode($resultrptmreport->reportconfig);
        $tabledetail = $jsonrules->tableinfo->tabledetail;
        $showdatapertable = $jsonrules->showdatapertable;


        // SETINGG
        $sdpt0 = $showdatapertable;
        $sdptoh = $showdatapertable;
        $sdpteh = $showdatapertable;
        $sdptof = $showdatapertable+1;
        $sdptef = $showdatapertable+1;
        $headermiddleml = $jsonrules->headermiddleml;
        $headerlastml = $jsonrules->headerlastml;
        $heightdetail = $jsonrules->heightdetail;
        $content1width = [];
        $content2width = [];

        foreach ($jsonrules->datarec as $k => $v) {
            $content1width[$k] = $v->width;
        }

        foreach ($jsonrules->footerrow as $k => $v) {
            $content2width[$k] = $v->width;
        }


        // RESULT MASTER
        $resultmaster = DB::connection('mysql2')
            ->table($tablemaster.' AS tm')
            ->leftJoin('mtypecostcontrol AS mtcc', 'tm.idtypecostcontrol', '=', 'mtcc.noid')
            ->leftJoin('genmdepartment AS gmd', 'tm.iddepartment', '=', 'gmd.noid')
            ->leftJoin('genmgudang AS gmg', 'tm.idgudang', '=', 'gmg.noid')
            ->leftJoin('mcardsupplier AS mcs', 'tm.idcardsupplier', '=', 'mcs.noid')
            ->leftJoin('mlokasiprop AS mlprop', 'mcs.idlokasiprop', '=', 'mlprop.noid')
            ->leftJoin('mlokasikab AS mlkab', 'mcs.idlokasikota', '=', 'mlkab.noid')
            ->leftJoin('mlokasikec AS mlkec', 'mcs.idlokasikec', '=', 'mlkec.noid')
            ->leftJoin('mlokasikel AS mlkel', 'mcs.idlokasikel', '=', 'mlkel.noid')
            ->leftJoin('mcarduser AS mcu', 'tm.idcreate', '=', 'mcu.noid')
            ->leftJoin('mcarduser AS mcu2', 'tm.idcardrequest', '=', 'mcu2.noid')
            ->leftJoin('salesorder AS so', 'tm.idcostcontrol', '=', 'so.noid')
            ->leftJoin('mcard AS mc2', 'mcu2.noid', '=', 'mc2.noid')
            ->leftJoin('mcard AS mc3', 'so.idcardkoor', '=', 'mc3.noid')
            ->leftJoin('mcard AS mc4', 'so.idcardpj', '=', 'mc4.noid')
            ->select([
                'tm.*', 
                'mcs.nama AS idcardsupplier_nama', 
                'mcs.contactperson AS idcardsupplier_contactperson', 
                'mcs.alamat AS idcardsupplier_alamat', 
                'mcs.alamatjl AS idcardsupplier_alamatjl', 
                'mcs.nomobile AS idcardsupplier_nomobile', 
                'mcs.nophone AS idcardsupplier_nophone', 
                'mlprop.nama AS idlokasiprop_nama', 
                'mlkab.nama AS idlokasikota_nama', 
                'mlkec.nama AS idlokasikec_nama', 
                'mlkel.nama AS idlokasikel_nama', 
                'mtcc.nama AS mtcc_nama',
                'gmd.nama AS gmd_nama',
                'so.kode AS so_kode',
                'so.projectname AS so_projectname',
                'so.projectlocation AS so_projectlocation',
                'gmg.nama AS gmg_nama',
                'mc2.nama AS mc2_nama',
                'mcu.myusername AS mcu_myusername',
                'mc2.noid AS mc2_noid',
                'mc2.nama AS mc2_nama',
                'mc2.kode AS mc2_kode',
                'mc3.noid AS mc3_noid',
                'mc3.nama AS mc3_nama',
                'mc3.kode AS mc3_kode',
                'mc4.noid AS mc4_noid',
                'mc4.nama AS mc4_nama',
                'mc4.kode AS mc4_kode'
            ])
            ->where('tm.noid', $noid)
            ->orderBy('tm.noid', 'ASC')
            ->first();
        if (!$resultmaster) return ['success'=>false,'message'=>'Error get master'];
        $isttdpurcahasing = $resultmaster->idstatustranc == 5024 ? true : false;


        // FIND PURCHASING
        $resultpurchasing = [];
        if ($isttdpurcahasing) {
            $resultpurchasing = DB::connection('mysql2')
                ->table('mcard')
                ->select(['noid','kode','nama'])
                ->where('kode',static::$main['purchasing'])
                ->first();
        }

        
        // FIND DIREKTUR
        $resultdirektur = DB::connection('mysql2')
            ->table('mcard')
            ->select(['noid','kode','nama'])
            ->where('kode',static::$main['direktur'])
            ->first();
        if (!$resultdirektur) return ['success'=>false,'message'=>'Error get Direktur'];


        // FIND FINANCE
        $resultfinance = DB::connection('mysql2')
            ->table('mcard')
            ->select(['noid','kode','nama'])
            ->where('kode',static::$main['finance'])
            ->first();
        if (!$resultfinance) return ['success'=>false,'message'=>'Error get Finance'];

            
        // RESULT DETAIL
        $resultdetail = DB::connection('mysql2')
            ->table($tabledetail)
            ->leftJoin($tablemaster, $tabledetail.'.idmaster', '=', $tablemaster.'.noid')
            ->leftJoin('accmcurrency', $tablemaster.'.idcurrency', '=', 'accmcurrency.noid')
            ->leftJoin('invminventory', $tabledetail.'.idinventor', '=', 'invminventory.noid')
            ->leftJoin('invmsatuan', $tabledetail.'.idsatuan', '=', 'invmsatuan.noid')
            ->leftJoin('mtypepackage', $tabledetail.'.idtypepackage', '=', 'mtypepackage.noid')
            ->leftJoin('accmpajak', $tabledetail.'.idtax', '=', 'accmpajak.noid')
            ->leftJoin('mcardsupplier', $tabledetail.'.idcardsupplier', '=', 'mcardsupplier.noid')
            ->select([
                $tabledetail.'.noid AS noid',
                $tabledetail.'.idinventor AS idinventor',
                'invminventory.kode AS kode',
                'invminventory.nama AS namainventor',
                'invminventory.purchaseprice AS purchaseprice',
                $tabledetail.'.namainventor AS namainventor2',
                $tabledetail.'.keterangan AS keterangan',
                $tabledetail.'.unitqty AS unitqty',
                $tabledetail.'.unitqtysisa AS unitqtysisa',
                'invmsatuan.nama AS namasatuan',
                $tabledetail.'.unitprice AS unitprice',
                $tabledetail.'.subtotal AS subtotal',
                'accmpajak.nama AS idtax_nama',
                $tabledetail.'.hargatotal AS hargatotal',
                $tabledetail.'.idcardsupplier AS idcardsupplier',
                'mcardsupplier.nama AS idcardsupplier_nama',
                'accmcurrency.nama AS currency',
                'mtypepackage.nama AS mtypepackage_nama',
            ])
            ->where('idmaster', $resultmaster->noid)
            ->orderBy($tabledetail.'.idinventor', 'ASC')
            ->orderBy($tabledetail.'.idcardsupplier', 'ASC')
            ->get();
        if (!$resultdetail) return ['success'=>false,'message'=>'Error get Detail'];

        $resultsupplier = DB::connection('mysql2')
            ->table($tabledetail.' AS td')
            ->leftJoin('mcardsupplier AS mcs', 'mcs.noid', '=', 'td.idcardsupplier')
            ->select([
                'td.idcardsupplier',
                'mcs.nama AS idcardsupplier_nama'
            ])
            ->where('td.idmaster', $resultmaster->noid)
            ->groupBy('td.idcardsupplier')
            ->get();
        if (!$resultsupplier) return ['success'=>false,'message'=>'Error get Supplier'];

        $unitpricesupplier = [];
        $datasupplier = '';
        foreach ($resultsupplier as $k => $v) {
            $unitpricesupplier[$v->idcardsupplier] = 0;
            $datasupplier .= '<th>'.substr($v->idcardsupplier_nama,0,8).'</th>';
        }

        $fixresultdetail = [];
        foreach ($resultdetail as $k => $v) {
            if (!array_key_exists($v->idinventor, $fixresultdetail)) {
                $fixresultdetail[$v->idinventor] = [
                    'idinventor' => $v->idinventor,
                    'kode' => $v->kode,
                    'namainventor' => $v->namainventor,
                    'namainventor2' => $v->namainventor2,
                    'unitqty' => $v->unitqty,
                    'namasatuan' => $v->namasatuan,
                    'unitprice' => $unitpricesupplier
                ];
            }
            $fixresultdetail[$v->idinventor]['unitprice'][$v->idcardsupplier] = $v->unitprice;
        }

        $resultdetail = [];
        $idx = 0;
        foreach ($fixresultdetail as $k => $v) {
            $resultdetail[$idx] = $v;
            $idx++;
        }

        $resultrptmreportdetail = DB::connection('mysql2')
            ->table('rptmreportdetail')
            ->where('idreport', $idreportdaftar)
            ->orderBy('nourut', 'ASC')
            ->get();
        if (!$resultrptmreportdetail) return ['success'=>false,'message'=>'Error get resultrptmreportdetail'];
        
        $field = ['kode','kodereff'];      //fincekgiro
        $tablemaster_kode = $tablemaster.'_'.$field[0];
        $tablemaster_revno = $tablemaster.'_'.$field[1];
        $resultheader = DB::connection('mysql2')->select("SELECT $tablemaster.$field[0] AS $tablemaster_kode, $tablemaster.$field[1] AS $tablemaster_revno, genmcompany.nama AS genmcompany_nama FROM $tablemaster JOIN genmcompany ON $tablemaster.idcompany = genmcompany.noid WHERE $tablemaster.noid=$noid LIMIT 1")[0];
        

        // GET RMRD
        $rmrd = $this->getRMRD([
            'showdatapertable' => $showdatapertable,
            'resultrptmreport' => $resultrptmreport,
            'resultrptmreportdetail' => $resultrptmreportdetail
        ]);


        // GET HTML
        $html = $this->getHTML([
            'rptmreport' => $resultrptmreport,
            'datarec' => $jsonrules->datarec,
            'headermiddleml' => $headermiddleml,
            'headerlastml' => $headerlastml,
            'heightdetail' => $heightdetail,
            'content1width' => $content1width,
            'content2width' => $content2width,
        ]);


        // QTY DETAIL
        if (count($resultdetail) >= $showdatapertable) {
            $qtydatadetail = count($resultdetail)-count($resultdetail)%$showdatapertable;
            $qtydatadetail = $qtydatadetail/$showdatapertable;
            if (count($resultdetail)%$showdatapertable != 0) {
                $qtydatadetail++;
            }
        } else {
            $qtydatadetail = 1;
        }


        // FIND START & FINISH
        $start = 0;
        $finish = $showdatapertable-1;
        for ($i=0; $i<$qtydatadetail; $i++) {
            if ($i == 0) {
                if ($rmrd['sdpt0'] != $showdatapertable) {
                    $plusfinish = $rmrd['sdpt0'];
                } else {
                    $plusfinish = $showdatapertable;
                }
            } else {
                if ($i%2 == 0) {
                    $plusfinish = $rmrd['sdpto'];
                } else {
                    $plusfinish = $rmrd['sdpte'];
                }
            }
            $finish = $start+@($plusfinish-1);
            $dataqtydatadetail[] = (Object)[
                'start' => $start,
                'finish' => $finish
            ];
            $start = $finish+1;
            $finish = $start+@($plusfinish-1);
        }


        // CHECK ISENTERROW
        $qlr_ = [];
        foreach ($dataqtydatadetail as $k => $v) {
            $qlr_[$k] = 0;
            foreach ($resultdetail as $k2 => $v2) {
                if ($k2 >= $v->start && $k2 <= $v->finish) {
                    $qlr_[$k]++;
                }
            }
        }

        for ($i=count($qlr_)-1; $i>0; $i--) {
            if ($qlr_[$i] != 0) {
                break;
            }
            array_pop($qlr_);
            array_pop($dataqtydatadetail);
        }
        $rowenterrow = $jsonrules->showdatapertable;

        $isenterrow = false;
        if ($qlr_[array_key_last($qlr_)] > $rowenterrow) {
            array_push($qlr_, $qlr_[array_key_last($qlr_)]-$rowenterrow);
            $qlr_[count($qlr_)-2] -= $qlr_[array_key_last($qlr_)];
            $dataqtydatadetail[array_key_last($dataqtydatadetail)]->finish = $dataqtydatadetail[array_key_last($dataqtydatadetail)]->start+$qlr_[array_key_last($dataqtydatadetail)]-1;
            if ($dataqtydatadetail[array_key_last($dataqtydatadetail)]->finish <= count($resultdetail)-1) {
                array_push($dataqtydatadetail, (object)[
                    'start' => $dataqtydatadetail[array_key_last($dataqtydatadetail)]->finish+1,
                    'finish' => $dataqtydatadetail[array_key_last($dataqtydatadetail)]->finish+1+($showdatapertable*2)
                ]);
            }
            $isenterrow = true;
        }

        
        // QTY LAST RECORD
        $content = '';
        $total = 0;
        $no = 1;
        $qtylastrecord = [];
        foreach ($dataqtydatadetail as $k => $v) {
            if ($k == 0) {
                if ($rmrd['showfhfp'] || $rmrd['showothhfp'] || $rmrd['showlhfp'] || $rmrd['showohfp'] || $rmrd['showehfp']) {
                    $content .= '<div style="height: '.$rmrd['heightfh'].'">'.$templatedetail['First Header']->phpsource.'</div>';
                }
            } else if ($k > 0 && $k < count($dataqtydatadetail)-1) {
                if ($rmrd['showfhep'] || $rmrd['showothhep'] || $rmrd['showlhep']) {
                    if ($k%2 == 0) {
                        $content .= '<div style="height: '.$rmrd['heightoh'].'">'.$templatedetail['Other Header']->phpsource.'</div>';
                    } else {
                        $content .= '<div style="height: '.$rmrd['heighteh'].'">'.$templatedetail['Other Header']->phpsource.'</div>';
                    }
                } else {
                    if ($k%2 == 0) {
                        if ($rmrd['showohep']) {
                            $content .= '<div style="height: '.$rmrd['heightoh'].'">'.$templatedetail['Odd Header']->phpsource.'</div>';
                        }
                    }
                    if ($k%2 == 1) {
                        if ($rmrd['showehep']) {
                            $content .= '<div style="height: '.$rmrd['heighteh'].'">'.$templatedetail['Even Header']->phpsource.'</div>';
                        }
                    }
                }
            } else if ($k == count($dataqtydatadetail)-1) {
                if ($rmrd['showfhlp'] || $rmrd['showothhlp'] || $rmrd['showlhlp'] || $rmrd['showohlp'] || $rmrd['showehlp']) {
                    $content .= '<div style="height: '.$rmrd['heightlh'].'">'.$templatedetail['Last Header']->phpsource.'</div>';
                }
            }

            $record = '';
            $qtylasttable = 0;
            $subtotal = 0;
            $isappend = false;
            $qtylastrecord[$k] = 0;
            $qtyrow = 42;


            $totalprices = 0;
            foreach ($resultdetail as $k2 => $v2) {
                $v2 = (object)$v2;
                if ($k2 >= $v->start && $k2 <= $v->finish) {
                    $isappend = true;
                    $record .= "<tr>";
                    foreach ($jsonrules->datarec as $k3 => $v3) {
                        $field = $v3->field;
                        if ($field == 'subtotal') {
                            $value = @$v2->$field;
                        } else if ($field == 'nopo') {
                            $value = $resultmaster->kodereff;
                        } else if ($field == 'noinvoice') {
                            $value = $resultmaster->kode;
                        }

                        if (in_array($field, $currency)) {
                            $v2->$field = number_format((int)@$v2->$field);
                        }

                        if ($field == 'no') {
                            $value = $no;
                        } else if ($field == 'namainventor2'){
                            $maxchar = 55;
                            $length = strlen($v2->$field);
                            $rest = fmod($length, $maxchar);
                            $loop = ($length-$rest)/$maxchar;

                            if ($loop > 0) {
                                for ($i=0; $i<=$loop; $i++) {
                                    if ($i == 0) {
                                        $cutstring = substr($v2->$field, 0, $maxchar);
                                        $first = 0;
                                        $last = strrpos($cutstring, ' ');
                                        $string = substr($cutstring, $first, $last);
                                    } else {
                                        $cutstring = $string.'<br>'.substr($v2->$field, $last+1, $maxchar);
                                        $first = $last;
                                        $last = strrpos($cutstring, ' ');
                                        $string .= substr($cutstring, $first, $last);
                                        $qtyrow--;
                                    }
                                }
                                $value = $string;
                            } else {
                                $value = $v2->$field;
                            }

                        } else if ($field == 'idcardsupplier'){
                            foreach ($v2->unitprice as $k4 => $v4) {
                                $record .= "<td style='width: 10%; background: #06e524; text-align: ".$v3->textalign.";'>".'Rp. '.number_format((int)$v4)."</td>";
                            }
                            continue;
                        } else if ($field == 'subtotal'){
                            $value = 'Rp. '.number_format((int)$value);
                            $totalprices += filter_var($value, FILTER_SANITIZE_NUMBER_INT);
                        } else {
                            if ($v3->maxchar != 0) {
                                if (strlen($v2->$field) >= $v3->maxchar) {
                                    $value = @substr($v2->$field, 0, $v3->maxchar).'...';
                                } else {
                                    $value = $v2->$field;
                                }
                            } else if ($field == 'nopo' || $field == 'noinvoice'){
                            } else {
                                $value = @$v2->$field;
                            }
                        }

                        $wordwrap = $v3->wordwrap ? 'break-all' : 'keep-all';
                        $value = !$v3->wordwrap ? str_replace(' ', '&nbsp;', $value) : '';
                        $fixed = $v3->fixed ? 'px' : '%';
                        if ($v3->fixed) {
                            $fixed = 'px';
                        } else {
                            $fixed = '%';
                        }
                        
                        $record .= "<td style='width: ".@$content1width[$k3]."%; text-align: ".$v3->textalign.";'>".$value."</td>";
                    }
                    $record .= "</tr>";
                    $no++;
                    $qtylasttable++;
                    $subtotal += @filter_var(((array)$v2)['subtotal'], FILTER_SANITIZE_NUMBER_INT);
                    $qtylastrecord[$k]++;
                }
            }

            $othertd = '';
            $colsupplier = '';
            if (@$unitpricesupplier) {
                foreach ($unitpricesupplier as $k2 => $v2) {
                    $colsupplier .= '<td style="background: #06e524;"></td>';
                }
            }

            $qtyrowempty = $qtyrow-count($resultdetail);
            for ($i=1;$i<=$qtyrowempty;$i++) {
                if ($i == $qtyrowempty) {
                    $totalpricestitle = 'Total Prices';
                    $totalpricesvalue = 'Rp. '.number_format($totalprices);
                } else {
                    $totalpricestitle = '';
                    $totalpricesvalue = '';
                }
                $record .= '<tr><td>&nbsp;</td><th style="text-align: right">'.$totalpricestitle.'</th><td></td><td></td>'.$othertd.$colsupplier.'<th style="text-align: right">'.$totalpricesvalue.'</th></tr>';
            }

            $nextprint = true;
            if ($k == 0) {
                if ($rmrd['showfdfp'] || $rmrd['showothdfp'] || $rmrd['showldfp'] || $rmrd['showodfp'] || $rmrd['showedfp']) {
                    
                } else {
                    $content .= '<div style="height: '.$rmrd['heightfd'].'px"></div>';
                    $nextprint = false;
                }
            } else if ($k > 0 && $k < count($dataqtydatadetail)-1) {
                if ($rmrd['showfdep'] || $rmrd['showothdep'] || $rmrd['showldep']) {
                } else if ($rmrd['showodep'] && $rmrd['showedep']) {
                } else if (!$rmrd['showodep'] && $rmrd['showedep']) {
                    if ($k%2 == 0) {
                        $content .= '<div style="height: '.$rmrd['heighted'].'px"></div>';
                        $nextprint = false;
                    }
                } else if ($rmrd['showodep'] && !$rmrd['showedep']) {
                    if ($k%2 == 1) {
                        $content .= '<div style="height: '.$rmrd['heighted'].'px"></div>';
                        $nextprint = false;
                    }
                } else if (!$rmrd['showodep'] && !$rmrd['showedep']) {
                    $content .= '<div style="height: '.$rmrd['heighted'].'px"></div>';
                    $nextprint = false;
                } else {
                    if ($k%2 == 0) {
                        if (!$rmrd['showed'] && !$rmrd['showothd']) {
                            $content .= '<div style="height: '.$rmrd['heighted'].'px"></div>';
                            $nextprint = false;
                        } else {
                        }
                    } else {
                        if (!$rmrd['showod'] && !$rmrd['showothd']) {
                            $content .= '<div style="height: '.$rmrd['heightod'].'px"></div>';
                            $nextprint = false;
                        } else {
                        }
                    }
                }
            } else if (count($dataqtydatadetail) == $k+1) {
                if ($rmrd['showfdlp'] || $rmrd['showothdlp'] || $rmrd['showldlp'] || $rmrd['showodlp'] || $rmrd['showedlp']) {
                    
                } else {
                    $content .= '<div style="height: '.$rmrd['heightfd'].'px"></div>';
                    $nextprint = false;
                }
            }
            
            if ($nextprint) {
                if ($isappend) {
                    if ($k == 0) {
                        $content .= '<div style="height: '.$rmrd['heightfd'].'px">'.$templatedetail['First Detail']->phpsource.'</div>';
                    } else {
                        if (count($dataqtydatadetail) == $k+1) {
                            if (count($dataqtydatadetail)%2 == 0) {
                                if ($isenterrow) {
                                    if ($rmrd['showeh'] && $rmrd['showef']) {
                                        $hdlast = $heightdetail;
                                    } else if (!$rmrd['showeh'] && $rmrd['showef']) {
                                        $hdlast = $heightdetail;
                                    } else if ($rmrd['showeh'] && !$rmrd['showef']) {
                                        $hdlast = $heightdetail;
                                    } else if (!$rmrd['showeh'] && !$rmrd['showef']) {
                                        $hdlast = $heightdetail;
                                    }
                                } else {
                                    if ($rmrd['showoh'] && $rmrd['showof']) {
                                        $hdlast = $heightdetail;
                                    } else if (!$rmrd['showoh'] && $rmrd['showof']) {
                                        $hdlast = $heightdetail;
                                    } else if ($rmrd['showoh'] && !$rmrd['showof']) {
                                        $hdlast = $heightdetail;
                                    } else if (!$rmrd['showoh'] && !$rmrd['showof']) {
                                        $hdlast = $heightdetail;
                                    }
                                }
                            } else {
                                if ($isenterrow) {
                                    if ($rmrd['showoh'] && $rmrd['showof']) {
                                        $hdlast = $heightdetail;
                                    } else if (!$rmrd['showoh'] && $rmrd['showof']) {
                                        $hdlast = $heightdetail;
                                    } else if ($rmrd['showoh'] && !$rmrd['showof']) {
                                        $hdlast = $heightdetail;
                                    } else if (!$rmrd['showoh'] && !$rmrd['showof']) {
                                        $hdlast = $heightdetail;
                                    }
                                } else {
                                    if ($rmrd['showeh'] && $rmrd['showef']) {
                                        $hdlast = $heightdetail;
                                    } else if (!$rmrd['showeh'] && $rmrd['showef']) {
                                        $hdlast = $heightdetail;
                                    } else if ($rmrd['showeh'] && !$rmrd['showef']) {
                                        $hdlast = $heightdetail;
                                    } else if (!$rmrd['showeh'] && !$rmrd['showef']) {
                                        $hdlast = $heightdetail;
                                    }
                                }
                            }
                            $hdlast = $heightdetail+7;
                            $content .= '<div style="height: '.$rmrd['heightld'].'px">'.$templatedetail['Last Detail']->phpsource.'</div>';
                        } else {
                            if ($rmrd['showfhep'] || $rmrd['showothhep'] || $rmrd['showlhep']) {
                                if ($k%2 == 0) {
                                    $content .= '<div style="height: '.$rmrd['heightod'].'px">'.$templatedetail['Other Detail']->phpsource.'</div>';
                                } else {
                                    $content .= '<div style="height: '.$rmrd['heighted'].'px">'.$templatedetail['Other Detail']->phpsource.'</div>';
                                }
                            } else if ($rmrd['showohep'] && $rmrd['showehep']) {
                                if ($k%2 == 0) {
                                    $content .= '<div style="height: '.$rmrd['heightod'].'px">'.$templatedetail['Odd Detail']->phpsource.'</div>';
                                } else {
                                    $content .= '<div style="height: '.$rmrd['heighted'].'px">'.$templatedetail['Even Detail']->phpsource.'</div>';
                                }
                            } else if (!$rmrd['showohep'] && $rmrd['showehep']) {
                                if ($k%2 == 0) {
                                    $content .= '<div style="height: '.$rmrd['heightod'].'px">'.$templatedetail['Odd Detail']->phpsource.'</div>';
                                } else {
                                    $content .= '<div style="height: '.$rmrd['heighted'].'px">'.$templatedetail['Even Detail']->phpsource.'</div>';
                                }
                            } else if ($rmrd['showohep'] && !$rmrd['showehep']) {
                                if ($k%2 == 0) {
                                    $content .= '<div style="height: '.$rmrd['heighted'].'px">'.$templatedetail['Even Detail']->phpsource.'</div>';
                                } else {
                                    $content .= '<div style="height: '.$rmrd['heightod'].'px">'.$templatedetail['Odd Detail']->phpsource.'</div>';
                                }
                            } else {
                                if ($k%2 == 0) {
                                    if (!$rmrd['showoh'] && !$rmrd['showothh']) {
                                        $rmrd['heightod'] += $rmrd['_heightoh']+16;
                                    }
                                    $content .= '<div style="height: '.$rmrd['heightod'].'px">'.$templatedetail['Other Detail']->phpsource.'</div>';
                                } else {
                                    if (!$rmrd['showeh'] && !$rmrd['showothh']) {
                                        $rmrd['heighted'] += $rmrd['_heighteh']+16;
                                    }
                                    $content .= '<div style="height: '.$rmrd['heighted'].'px">'.$templatedetail['Other Detail']->phpsource.'</div>';
                                }
                            }
                        }
                    }
                    $total += $subtotal;
                    $content = str_replace('$datadetail', $record, $content);
                    $content = str_replace('$subtotal_jumlah_datadetail', 'Rp. '.number_format($subtotal), $content);
                }
            }

            if ($k == 0) {
                if (count($dataqtydatadetail) == 1) {
                    if ($rmrd['showfffp'] || $rmrd['showothffp'] || $rmrd['showlffp'] || $rmrd['showoffp'] || $rmrd['showeffp']) {
                        $content .= @'<div style="height: '.$rmrd['heightff'].'">'.$templatedetail['First Footer']->phpsource.'</div>';
                    }
                } else {
                    if ($rmrd['showfffp'] || $rmrd['showothffp'] || $rmrd['showlffp'] || $rmrd['showoffp'] || $rmrd['showeffp']) {
                        $content .= @'<div style="height: '.$rmrd['heightff'].'">'.$templatedetail['First Footer']->phpsource.'</div>';
                    }
                    $content .= '<div class="page-break"></div>';
                }
            } else if ($k > 0 && $k < count($dataqtydatadetail)-1) {
                if ($rmrd['showffep'] || $rmrd['showothfep'] || $rmrd['showlfep']) {
                    $content .= @'<div style="height: '.$rmrd['heightff'].'">'.$templatedetail['Other Footer']->phpsource.'</div>';
                } else if ($rmrd['showofep'] && $rmrd['showefep']) {
                    if ($k%2 == 0) {
                        $content .= '<div style="height: '.$rmrd['heightof'].'">'.$templatedetail['Odd Footer']->phpsource.'</div>';
                    } else {
                        $content .= '<div style="height: '.$rmrd['heightef'].'">'.$templatedetail['Even Footer']->phpsource.'</div>';
                    }
                } else if (!$rmrd['showofep'] && $rmrd['showefep']) {
                    if ($k%2 == 1) {
                        $content .= '<div style="height: '.$rmrd['heightef'].'">'.$templatedetail['Even Footer']->phpsource.'</div>';   
                    }
                } else if ($rmrd['showofep'] && !$rmrd['showefep']) {
                    if ($k%2 == 0) {
                        $content .= '<div style="height: '.$rmrd['heightof'].'">'.$templatedetail['Odd Footer']->phpsource.'</div>';   
                    }
                } else if ($rmrd['showefep']) {
                    if ($k%2 == 1) {
                        $content .= '<div style="height: '.$rmrd['heightof'].'">'.$templatedetail['Even Footer']->phpsource.'</div>';
                    }
                }
                $content .= '<div class="page-break"></div>';
            } else if ($k == count($dataqtydatadetail)-1) {
                if ($rmrd['showfflp'] || $rmrd['showothflp'] || $rmrd['showlflp'] || $rmrd['showoflp'] || $rmrd['showeflp']) {
                    $content .= @'<div style="height: '.$rmrd['heightff'].'">'.$templatedetail['Last Footer']->phpsource.'</div>';
                }
            }
            
            if (count($dataqtydatadetail) == $k+1) {
                if ($rmrd['showlf']) {
                    $qlr = 0;
                    for ($i=count($qtylastrecord)-1; $i>=0; $i--) {
                        if ($qtylastrecord[$i] != 0) {
                            $qlr = $qtylastrecord[$i];
                            break;
                        }
                    }
                    
                    $mtlf = 0;
                    $rmrd['marginlf'] = $mtlf.'px 0px 0px 0px';
                    if ($rmrd['showlf']) {
                    }
                } else {
                }
            } else if (count($dataqtydatadetail) == $k+2 && $isenterrow) {
            } else {
                if ($k == 0) {
                } else {
                    if ($k%2 == 0) {
                        if (count($dataqtydatadetail) > $k+2) {
                        } else {
                        }
                    } else {
                        if (count($dataqtydatadetail) > $k+2) {
                        } else {
                        }
                    }
                }
            }
        }

        // TTD
        $_colspandescription = 0;
        $_ttd1 = '';
        $_ttd2 = '';
        $_ttd3 = '';
        $_ttd4 = '';
        $_nama1 = '-';
        $_nama2 = '-';
        $_nama3 = '-';
        $_nama4 = '-';
        $_randomttd = rand(1,6);
            
        $_colspandescription = count(@$unitpricesupplier ? $unitpricesupplier : [])+count($jsonrules->datarec)-1;
        $_ttd1 = '<img src="filemanager/original/'.$resultmaster->mc2_kode.'/'.$resultmaster->mc2_kode.'-ttd-'.$_randomttd.'.png" height="50px">';
        $_nama1 = $resultmaster->mc2_nama;
        $_ttd2 = '<img src="filemanager/original/'.$resultmaster->mc3_kode.'/'.$resultmaster->mc3_kode.'-ttd-'.$_randomttd.'.png" height="50px">';
        $_nama2 = $resultmaster->mc3_nama;
        $_ttd3 = '<img src="filemanager/original/'.$resultmaster->mc4_kode.'/'.$resultmaster->mc4_kode.'-ttd-'.$_randomttd.'.png" height="50px">';
        $_nama3 = $resultmaster->mc4_nama;

        $content = str_replace('$qtydatasupplier', count(@$unitpricesupplier ? $unitpricesupplier : []), $content);
        $content = str_replace('$datasupplier', $datasupplier, $content);
        $content = str_replace('$ttd1', $_ttd1, $content);
        $content = str_replace('$ttd2', $_ttd2, $content);
        $content = str_replace('$ttd3', $_ttd3, $content);
        $content = str_replace('$ttd4', $_ttd4, $content);
        $content = str_replace('$preparedby', $_nama1, $content);
        $content = str_replace('$reviewedby', $_nama2, $content);
        $content = str_replace('$approvedby', $_nama3, $content);
        $content = str_replace('$completedby', $_nama4, $content);

        $content = str_replace('$requestedby', $resultmaster->mc2_nama, $content);
        $content = str_replace('$department', ucfirst(strtolower($resultmaster->mtcc_nama)).' / '.$resultmaster->so_projectname.' / '.$resultmaster->so_projectlocation, $content);
        $content = str_replace('$date', date('F d, Y', strtotime($resultmaster->dostatus)), $content);
        $content = str_replace('$colspandescription', $_colspandescription, $content);
        $content = str_replace('$colspanunitprice', count($unitpricesupplier), $content);

        if ($isttdpurcahasing) {
            $_ttd4 = '<img src="filemanager/original/'.$resultpurchasing->kode.'/'.$resultpurchasing->kode.'-ttd-'.$_randomttd.'.png" height="50px">';
            $_nama4 = $resultpurchasing->nama;
        }

        $html = str_replace('$content', $content, $html);
        $html = str_replace('$logo', '<img src="assets/lycon.jpeg" alt="" class="img-logo">', $html);
        $html = str_replace('$genmcompany_nama', $resultheader->genmcompany_nama, $html);
        $html = str_replace('$today', date('d M Y'), $html);
        $html = str_replace('$jobno', $resultmaster->so_kode, $html);
        $html = str_replace('$prno', $resultheader->$tablemaster_kode, $html);
        $html = str_replace('$revdate', $resultheader->$tablemaster_revno.'/'.date('d M Y'), $html);
        $html = str_replace('$deliverytime', date('F d, Y', strtotime($resultmaster->tanggaldue)), $html);
        $html = str_replace('$psbib', date('F d, Y', strtotime($resultmaster->tanggaldue)), $html);
        $html = str_replace('$notes', $resultmaster->keterangan, $html);
        $html = str_replace('$shipto', $resultmaster->gmg_nama, $html);
        $html = str_replace('$deliverypoint', $resultmaster->deliverypoint, $html);
        $html = str_replace('$preparedby', $resultmaster->mc2_nama, $html);

        return [
            'html' => $html,
            'defaultpaper' => $resultrptmreport->defaultpaper,
            'ispotrait' => $ispotrait,
        ];
    }

    public function print($noid, $urlprint) {
        // CONVERT URLPRINT
        $urlprint = explode('/', base64_decode($urlprint));
        $custom = ['status' => false];

        if ($urlprint[0] == 'print_purchase_request') {
            $custom['status'] = true;
            $custom['ispotrait'] = 'potrait';
            $custom['tablemaster'] = 'purchaserequest';
            $custom['subtotal'] = 'subtotal';
            $custom['currency'] = ['unitprice','subtotal'];
        } else if ($urlprint[0] == 'print_purchase_offer') {
            $data = (new static)->print_purchase_offer([
                'page' => $noid,
                'noid' => $urlprint[1],
            ]);
            $pdf = PDF::loadHTML($data['html']);
            $pdf->setPaper($data['defaultpaper'], $data['ispotrait']);
            return $pdf->stream('users.pdf');
        } else if ($urlprint[0] == 'print_purchase_order') {
            $custom['status'] = true;
            $custom['ispotrait'] = 'potrait';
            $custom['tablemaster'] = 'purchaseorder';
            $custom['subtotal'] = 'subtotal';
            $custom['currency'] = ['unitprice','subtotal'];
        } else if ($urlprint[0] == 'print_purchase_delivery') {
            $custom['status'] = true;
            $custom['ispotrait'] = 'landscape';
            $custom['tablemaster'] = 'purchasedelivery';
            $custom['subtotal'] = 'subtotal';
            $custom['currency'] = ['unitprice','subtotal'];
        } else if ($urlprint[0] == 'print_purchase_invoice') {
            $custom['status'] = true;
            $custom['ispotrait'] = 'landscape';
            $custom['tablemaster'] = 'purchaseinvoice';
            $custom['subtotal'] = 'subtotal';
            $custom['currency'] = ['unitprice','subtotal'];
        } else if ($urlprint[0] == 'print_kas_advance') {
            $custom['status'] = true;
            $custom['ispotrait'] = 'landscape';
            $custom['tablemaster'] = 'fincashbankadvance';
            $custom['subtotal'] = 'subtotal';
            $custom['currency'] = ['unitprice','subtotal'];
        }

        $noidrptmreport = DB::table('cmswidgetgrid')
            ->select('idreportdaftar')
            ->where('noid', $noid.'01')
            ->first()->idreportdaftar;
        $template = DB::connection('mysql2')
            ->table('rptmreport')
            ->where('noid', $noidrptmreport)
            ->first();
        $resulttemplatedetail = DB::connection('mysql2')
            ->table('rptmreportdetail')
            ->select('rptmreportdetail.idreportwidget', 'rptmreportdetail.nama', 'rptmreportwidget.noid', 'rptmreportwidget.phpsource')
            ->join('rptmreportwidget', 'rptmreportwidget.noid', '=', 'rptmreportdetail.idreportwidget')
            ->where('idreport', $noidrptmreport)
            ->get();
        $templatedetail = [];
        foreach ($resulttemplatedetail as $k => $v) {
            $templatedetail[$v->nama] = $v;
        }
        // dd($templatedetail);
        // dd(json_decode($template->reportconfig));

        if (is_null($template)) return ErrorController::underConstruction();
        $jsonrules = json_decode($template->reportconfig);
        
        if ($custom['status']) {
            $ispotrait = $custom['ispotrait'];
        } else {
            if ($template->ispotrait) {
                $ispotrait = 'potrait';
            } else {
                $ispotrait = 'landscape';
            }
        }

        // $noidrptmreport = $jsonrules->noidrptmreport;                            //noid rptmreport
        if ($custom['status']) {
            // if ($custom['tablemaster'] == 'purchaseoffer') {
            //     $tabledetail = 'purchaserequestdetail';                 //table detail
            // } else {
                $tabledetail = $jsonrules->tableinfo->tabledetail;                 //table detail
            // }
        } else {
            $tabledetail = $jsonrules->tableinfo->tabledetail;                 //table detail
        }

        $showdatapertable = $jsonrules->showdatapertable;
        $sdpt0 = $showdatapertable;
        $sdptoh = $showdatapertable;
        $sdpteh = $showdatapertable;
        $sdptof = $showdatapertable+1;
        $sdptef = $showdatapertable+1;
        foreach ($jsonrules->datarec as $k => $v) {
            $content1width[$k] = $v->width;
        }
        // $content1width = $jsonrules->content1width;
        // $charketerangan = $jsonrules->charketerangan;
        $headermiddleml = $jsonrules->headermiddleml;
        $headerlastml = $jsonrules->headerlastml;
        foreach ($jsonrules->footerrow as $k => $v) {
            $content2width[$k] = $v->width;
        }
        // $content2width = $jsonrules->content2width;
        $heightdetail = $jsonrules->heightdetail;
        
        if ($custom['status']) {
            // if ($custom['tablemaster'] == 'purchaseoffer') {
            //     $tablemaster = 'purchaserequest';
            // } else {
                $tablemaster = $custom['tablemaster'];
            // }
        } else {
            $tablemaster = DB::table('cmswidget')->select('maintable')->where('kode', $noid)->orderBy('noid', 'ASC')->first()->maintable;
        }
        // resultmasterasli

        if ($tablemaster == 'purchaserequest' ||
            $tablemaster == 'purchaseoffer' ||
            $tablemaster == 'purchaseorder' ||
            $tablemaster == 'purchasedelivery' ||
            $tablemaster == 'purchaseinvoice')
        {
            $resultmaster = DB::connection('mysql2')
                ->table($tablemaster.' AS tm')
                ->leftJoin('mtypecostcontrol AS mtcc', 'tm.idtypecostcontrol', '=', 'mtcc.noid')
                ->leftJoin('genmdepartment AS gmd', 'tm.iddepartment', '=', 'gmd.noid')
                ->leftJoin('genmgudang AS gmg', 'tm.idgudang', '=', 'gmg.noid')
                ->leftJoin('mcardsupplier AS mcs', 'tm.idcardsupplier', '=', 'mcs.noid')
                ->leftJoin('mlokasiprop AS mlprop', 'mcs.idlokasiprop', '=', 'mlprop.noid')
                ->leftJoin('mlokasikab AS mlkab', 'mcs.idlokasikota', '=', 'mlkab.noid')
                ->leftJoin('mlokasikec AS mlkec', 'mcs.idlokasikec', '=', 'mlkec.noid')
                ->leftJoin('mlokasikel AS mlkel', 'mcs.idlokasikel', '=', 'mlkel.noid')
                ->leftJoin('mcarduser AS mcu', 'tm.idcreate', '=', 'mcu.noid')
                ->leftJoin('mcarduser AS mcu2', 'tm.idcardrequest', '=', 'mcu2.noid')
                ->leftJoin('salesorder AS so', 'tm.idcostcontrol', '=', 'so.noid')
                ->leftJoin('mcard AS mc2', 'mcu2.noid', '=', 'mc2.noid')
                ->leftJoin('mcard AS mc3', 'so.idcardkoor', '=', 'mc3.noid')
                ->leftJoin('mcard AS mc4', 'so.idcardpj', '=', 'mc4.noid')
                ->select([
                    'tm.*', 
                    'mcs.nama AS idcardsupplier_nama', 
                    'mcs.contactperson AS idcardsupplier_contactperson', 
                    'mcs.alamat AS idcardsupplier_alamat', 
                    'mcs.alamatjl AS idcardsupplier_alamatjl', 
                    'mcs.nomobile AS idcardsupplier_nomobile', 
                    'mcs.nophone AS idcardsupplier_nophone', 
                    'mlprop.nama AS idlokasiprop_nama', 
                    'mlkab.nama AS idlokasikota_nama', 
                    'mlkec.nama AS idlokasikec_nama', 
                    'mlkel.nama AS idlokasikel_nama', 
                    'mtcc.nama AS mtcc_nama',
                    'gmd.nama AS gmd_nama',
                    'so.kode AS so_kode',
                    'so.projectname AS so_projectname',
                    'so.projectlocation AS so_projectlocation',
                    'gmg.nama AS gmg_nama',
                    'mc2.nama AS mc2_nama',
                    'mcu.myusername AS mcu_myusername',
                    'mc2.noid AS mc2_noid',
                    'mc2.nama AS mc2_nama',
                    'mc2.kode AS mc2_kode',
                    'mc3.noid AS mc3_noid',
                    'mc3.nama AS mc3_nama',
                    'mc3.kode AS mc3_kode',
                    'mc4.noid AS mc4_noid',
                    'mc4.nama AS mc4_nama',
                    'mc4.kode AS mc4_kode'
                ])
                ->where('tm.noid', $urlprint[1])
                ->orderBy('tm.noid', 'ASC')
                ->first();
        } else if ($tablemaster == 'fincashbankadvance'){
            $resultmaster = DB::connection('mysql2')
                ->table($tablemaster.' AS tm')
                ->leftJoin('genmdepartment AS gmd', 'tm.iddepartment', '=', 'gmd.noid')
                ->leftJoin('mcarduser AS mcu', 'tm.idcreate', '=', 'mcu.noid')
                ->leftJoin('mcarduser AS mcu2', 'tm.idcardrequest', '=', 'mcu2.noid')
                ->leftJoin('mcard AS mc2', 'mcu2.noid', '=', 'mc2.noid')
                ->leftJoin('salesorder AS so', 'tm.idso', '=', 'so.noid')
                ->select([
                    'tm.*', 
                    'gmd.nama AS gmd_nama',
                    'mc2.nama AS mc2_nama',
                    'mcu.myusername AS mcu_myusername',
                    'mc2.noid AS mc2_noid',
                    'mc2.nama AS mc2_nama',
                    'mc2.kode AS mc2_kode',
                    'so.kode AS so_kode',
                ])
                ->where('tm.noid', $urlprint[1])
                ->orderBy('tm.noid', 'ASC')
                ->first();
        }

        $isttdpurcahasing = false;
        if ($custom['tablemaster'] == 'purchaserequest') {
            $resultpof = DB::connection('mysql2')
                ->table('purchaseoffer')
                ->where('kodereff',$resultmaster->kode)
                ->get();

            foreach ($resultpof as $k => $v) {
                if ($v->idstatustranc == 5024) {
                    $isttdpurcahasing = true;
                    break;
                }
            }
        } else if ($custom['tablemaster'] == 'purchaseoffer') {
            $isttdpurcahasing = $resultmaster->idstatustranc == 5024
                ? true
                : false;
        } else if ($custom['tablemaster'] == 'purchaseorder') {
            $isttdpurcahasing = true;
        } else if ($custom['tablemaster'] == 'purchaseinvoice') {
            $isttdpurcahasing = true;
        }

        // FIND PURCHASING
        $resultpurchasing = [];
        if ($isttdpurcahasing) {
            $resultpurchasing = DB::connection('mysql2')
                ->table('mcard')
                ->select(['noid','kode','nama'])
                ->where('kode',static::$main['purchasing'])
                ->first();
        }

        // FIND DIREKTUR
        $resultdirektur = DB::connection('mysql2')
            ->table('mcard')
            ->select(['noid','kode','nama'])
            ->where('kode',static::$main['direktur'])
            ->first();

        // FIND FINANCE
        $resultfinance = DB::connection('mysql2')
            ->table('mcard')
            ->select(['noid','kode','nama'])
            ->where('kode',static::$main['finance'])
            ->first();

            
        // dd($isttdpurcahasing);
        $resultdetail = DB::connection('mysql2')
            ->table($tabledetail)
            ->leftJoin($tablemaster, $tabledetail.'.idmaster', '=', $tablemaster.'.noid')
            ->leftJoin('accmcurrency', $tablemaster.'.idcurrency', '=', 'accmcurrency.noid')
            ->leftJoin('invminventory', $tabledetail.'.idinventor', '=', 'invminventory.noid')
            ->leftJoin('invmsatuan', $tabledetail.'.idsatuan', '=', 'invmsatuan.noid')
            ->leftJoin('mtypepackage', $tabledetail.'.idtypepackage', '=', 'mtypepackage.noid')
            ->leftJoin('accmpajak', $tabledetail.'.idtax', '=', 'accmpajak.noid')
            ->select([
                $tabledetail.'.noid AS noid',
                $tabledetail.'.idinventor AS idinventor',
                'invminventory.kode AS kode',
                'invminventory.nama AS namainventor',
                'invminventory.purchaseprice AS purchaseprice',
                $tabledetail.'.namainventor AS namainventor2',
                $tabledetail.'.keterangan AS keterangan',
                $tabledetail.'.unitqty AS unitqty',
                $tabledetail.'.unitqtysisa AS unitqtysisa',
                'invmsatuan.nama AS namasatuan',
                $tabledetail.'.unitprice AS unitprice',
                $tabledetail.'.subtotal AS subtotal',
                'accmpajak.nama AS idtax_nama',
                $tabledetail.'.hargatotal AS hargatotal',
                'accmcurrency.nama AS currency',
                'mtypepackage.nama AS mtypepackage_nama',
            ])
            ->where('idmaster', $resultmaster->noid)
            ->orderBy($tabledetail.'.noid', 'ASC')
            ->get();
        $resultrptmreportdetail = DB::connection('mysql2')->table('rptmreportdetail')->where('idreport', $noidrptmreport)->orderBy('nourut', 'ASC')->get(); 
        // $field = ['kode','revno'];      //fincashbank
        $field = ['kode','kodereff'];      //fincekgiro
        $tablemaster_kode = $tablemaster.'_'.$field[0];
        $tablemaster_revno = $tablemaster.'_'.$field[1];
        // dd($tablemaster_revno);
        //fincashbank
        // $resultheader = DB::connection('mysql2')->select("SELECT $tablemaster.$field[0] AS $tablemaster_kode, $tablemaster.$field[1] AS $tablemaster_revno, genmcompany.nama AS genmcompany_nama FROM $tablemaster JOIN genmcompany ON $tablemaster.idcompany = genmcompany.noid WHERE $tablemaster.noid=$urlprint[1] LIMIT 1")[0];
        //fincekgiro
        $resultheader = DB::connection('mysql2')->select("SELECT $tablemaster.$field[0] AS $tablemaster_kode, $tablemaster.$field[1] AS $tablemaster_revno, genmcompany.nama AS genmcompany_nama FROM $tablemaster JOIN genmcompany ON $tablemaster.idcompany = genmcompany.noid WHERE $tablemaster.noid=$urlprint[1] LIMIT 1")[0];
        
        $rmrd = [];
        foreach ($resultrptmreportdetail as $k => $v) {
            if ($v->nama == 'First Header') {
                if ($v->isshowfirstpage) {
                    // $rmrd['fh'] = '<div class="fh">'.$template->reportheader.'</div>';
                    // $rmrd['sdpteh'] = $showdatapertable;
                    // $rmrd['sdpt0'] = $sdpt0;
                    $rmrd['showfh'] = true;
                } else {
                    // $rmrd['fh'] = '<div class="fh">&nbsp;</div>';
                    // $rmrd['sdpteh'] = $sdpteh;
                    // $rmrd['sdpt0'] = $sdpt0+4;
                    $rmrd['showfh'] = false;
                }
                // $rmrd['marginfh'] = '0px 0px 0px 0px';
                $rmrd['heightfh'] = $v->widgetheight;
                /////
                if ($v->isshowfirstpage) {
                    $rmrd['showfhfp'] = true;
                } else {
                    $rmrd['showfhfp'] = false;
                }
                if ($v->isshoweverypage) {
                    $rmrd['showfhep'] = true;
                } else {
                    $rmrd['showfhep'] = false;
                }
                if ($v->isshowlastpage) {
                    $rmrd['showfhlp'] = true;
                } else {
                    $rmrd['showfhlp'] = false;
                }
            } else if ($v->nama == 'First Detail') {
                if ($v->isshowfirstpage) {
                    $rmrd['showfd'] = true;
                } else {
                    $rmrd['showfd'] = false;
                }
                $rmrd['heightfd'] = $v->widgetheight;
                /////
                if ($v->isshowfirstpage) {
                    $rmrd['showfdfp'] = true;
                } else {
                    $rmrd['showfdfp'] = false;
                }
                if ($v->isshoweverypage) {
                    $rmrd['showfdep'] = true;
                } else {
                    $rmrd['showfdep'] = false;
                }
                if ($v->isshowlastpage) {
                    $rmrd['showfdlp'] = true;
                } else {
                    $rmrd['showfdlp'] = false;
                }
            } else if ($v->nama == 'First Footer') {
                if ($v->isshowfirstpage) {
                    $rmrd['showff'] = true;
                } else {
                    $rmrd['showff'] = false;
                }
                $rmrd['heightff'] = $v->widgetheight;
                /////
                if ($v->isshowfirstpage) {
                    $rmrd['showfffp'] = true;
                } else {
                    $rmrd['showfffp'] = false;
                }
                if ($v->isshoweverypage) {
                    $rmrd['showffep'] = true;
                } else {
                    $rmrd['showffep'] = false;
                }
                if ($v->isshowlastpage) {
                    $rmrd['showfflp'] = true;
                } else {
                    $rmrd['showfflp'] = false;
                }
            } else if ($v->nama == 'Other Header') {
                if ($v->isshoweverypage) {
                    $rmrd['showothh'] = true;
                } else {
                    $rmrd['showothh'] = false;
                }
                $rmrd['heightothf'] = $v->widgetheight;
                /////
                if ($v->isshowfirstpage) {
                    $rmrd['showothhfp'] = true;
                } else {
                    $rmrd['showothhfp'] = false;
                }
                if ($v->isshoweverypage) {
                    $rmrd['showothhep'] = true;
                } else {
                    $rmrd['showothhep'] = false;
                }
                if ($v->isshowlastpage) {
                    $rmrd['showothhlp'] = true;
                } else {
                    $rmrd['showothhlp'] = false;
                }
            } else if ($v->nama == 'Other Detail') {
                if ($v->isshoweverypage) {
                    $rmrd['showothd'] = true;
                } else {
                    $rmrd['showothd'] = false;
                }
                $rmrd['heightothd'] = $v->widgetheight;
                /////
                if ($v->isshowfirstpage) {
                    $rmrd['showothdfp'] = true;
                } else {
                    $rmrd['showothdfp'] = false;
                }
                if ($v->isshoweverypage) {
                    $rmrd['showothdep'] = true;
                } else {
                    $rmrd['showothdep'] = false;
                }
                if ($v->isshowlastpage) {
                    $rmrd['showothdlp'] = true;
                } else {
                    $rmrd['showothdlp'] = false;
                }
            } else if ($v->nama == 'Other Footer') {
                if ($v->isshoweverypage) {
                    $rmrd['showothf'] = true;
                } else {
                    $rmrd['showothf'] = false;
                }
                $rmrd['heightothf'] = $v->widgetheight;
                /////
                if ($v->isshowfirstpage) {
                    $rmrd['showothffp'] = true;
                } else {
                    $rmrd['showothffp'] = false;
                }
                if ($v->isshoweverypage) {
                    $rmrd['showothfep'] = true;
                } else {
                    $rmrd['showothfep'] = false;
                }
                if ($v->isshowlastpage) {
                    $rmrd['showothflp'] = true;
                } else {
                    $rmrd['showothflp'] = false;
                }
            } else if ($v->nama == 'Last Header') {
                if ($v->isshowlastpage) {
                    $rmrd['showlh'] = true;
                } else {
                    $rmrd['showlh'] = false;
                }
                $rmrd['heightlh'] = $v->widgetheight;
                /////
                if ($v->isshowfirstpage) {
                    $rmrd['showlhfp'] = true;
                } else {
                    $rmrd['showlhfp'] = false;
                }
                if ($v->isshoweverypage) {
                    $rmrd['showlhep'] = true;
                } else {
                    $rmrd['showlhep'] = false;
                }
                if ($v->isshowlastpage) {
                    $rmrd['showlhlp'] = true;
                } else {
                    $rmrd['showlhlp'] = false;
                }
            } else if ($v->nama == 'Last Detail') {
                if ($v->isshowlastpage) {
                    $rmrd['showld'] = true;
                } else {
                    $rmrd['showld'] = false;
                }
                $rmrd['heightld'] = $v->widgetheight;
                /////
                if ($v->isshowfirstpage) {
                    $rmrd['showldfp'] = true;
                } else {
                    $rmrd['showldfp'] = false;
                }
                if ($v->isshoweverypage) {
                    $rmrd['showldep'] = true;
                } else {
                    $rmrd['showldep'] = false;
                }
                if ($v->isshowlastpage) {
                    $rmrd['showldlp'] = true;
                } else {
                    $rmrd['showldlp'] = false;
                }
            } else if ($v->nama == 'Last Footer') {
                if ($v->isshowlastpage) {
                    $rmrd['showlf'] = true;
                } else {
                    $rmrd['showlf'] = false;
                }
                $rmrd['heightlf'] = $v->widgetheight;
                /////
                if ($v->isshowfirstpage) {
                    $rmrd['showlffp'] = true;
                } else {
                    $rmrd['showlffp'] = false;
                }
                if ($v->isshoweverypage) {
                    $rmrd['showlfep'] = true;
                } else {
                    $rmrd['showlfep'] = false;
                }
                if ($v->isshowlastpage) {
                    $rmrd['showlflp'] = true;
                } else {
                    $rmrd['showlflp'] = false;
                }
            } else if ($v->nama == 'Odd Header') {
                if ($v->isshoweverypage) {
                    // $rmrd['oh'] = '<div class="oh">'.$template->reportheader.'</div>';
                    // $rmrd['sdptoh'] = $showdatapertable;
                    $rmrd['showoh'] = true;
                } else {
                    // $rmrd['oh'] = '<div class="oh">&nbsp;</div>';
                    // $rmrd['sdptoh'] = $sdptoh;
                    $rmrd['showoh'] = false;
                }
                // $rmrd['marginoh'] = '0px 0px 0px 0px';
                $rmrd['heightoh'] = $v->widgetheight;
                /////
                if ($v->isshowfirstpage) {
                    $rmrd['showohfp'] = true;
                } else {
                    $rmrd['showohfp'] = false;
                }
                if ($v->isshoweverypage) {
                    $rmrd['showohep'] = true;
                } else {
                    $rmrd['showohep'] = false;
                }
                if ($v->isshowlastpage) {
                    $rmrd['showohlp'] = true;
                } else {
                    $rmrd['showohlp'] = false;
                }
            } else if ($v->nama == 'Even Header') {
                if ($v->isshoweverypage) {
                    // $rmrd['eh'] = '<div class="eh">'.$template->reportheader.'</div>';
                    $rmrd['showeh'] = true;
                } else {
                    // $rmrd['eh'] = '<div class="eh">&nbsp;</div>';
                    $rmrd['showeh'] = false;
                }
                // $rmrd['margineh'] = '0px 0px 0px 0px';
                $rmrd['heighteh'] = $v->widgetheight;
                /////
                if ($v->isshowfirstpage) {
                    $rmrd['showehfp'] = true;
                } else {
                    $rmrd['showehfp'] = false;
                }
                if ($v->isshoweverypage) {
                    $rmrd['showehep'] = true;
                } else {
                    $rmrd['showehep'] = false;
                }
                if ($v->isshowlastpage) {
                    $rmrd['showehlp'] = true;
                } else {
                    $rmrd['showehlp'] = false;
                }
            } else if ($v->nama == 'Odd Detail') {
                if ($v->isshoweverypage) {
                    $rmrd['showod'] = true;
                } else {
                    $rmrd['showod'] = false;
                }
                $rmrd['heightod'] = $v->widgetheight;
                /////
                if ($v->isshowfirstpage) {
                    $rmrd['showodfp'] = true;
                } else {
                    $rmrd['showodfp'] = false;
                }
                if ($v->isshoweverypage) {
                    $rmrd['showodep'] = true;
                } else {
                    $rmrd['showodep'] = false;
                }
                if ($v->isshowlastpage) {
                    $rmrd['showodlp'] = true;
                } else {
                    $rmrd['showodlp'] = false;
                }
            } else if ($v->nama == 'Even Detail') {
                if ($v->isshoweverypage) {
                    $rmrd['showed'] = true;
                } else {
                    $rmrd['showed'] = false;
                }
                $rmrd['heighted'] = $v->widgetheight;
                /////
                if ($v->isshowfirstpage) {
                    $rmrd['showedfp'] = true;
                } else {
                    $rmrd['showedfp'] = false;
                }
                if ($v->isshoweverypage) {
                    $rmrd['showedep'] = true;
                } else {
                    $rmrd['showedep'] = false;
                }
                if ($v->isshowlastpage) {
                    $rmrd['showedlp'] = true;
                } else {
                    $rmrd['showedlp'] = false;
                }
            } else if ($v->nama == 'Odd Footer') {
                if ($v->isshoweverypage) {
                    // $rmrd['of'] = '<div class="of">'.$template->reportfooter.'</div>';
                    // $rmrd['sdptof'] = $showdatapertable;
                    $rmrd['showof'] = true;
                } else {
                    // $rmrd['of'] = '<div class="of">&nbsp;</div>';
                    // $rmrd['sdptof'] = $sdptof;
                    $rmrd['showof'] = false;
                }
                // $rmrd['marginof'] = '0px 0px 0px 0px';
                $rmrd['heightof'] = $v->widgetheight;
                /////
                if ($v->isshowfirstpage) {
                    $rmrd['showoffp'] = true;
                } else {
                    $rmrd['showoffp'] = false;
                }
                if ($v->isshoweverypage) {
                    $rmrd['showofep'] = true;
                } else {
                    $rmrd['showofep'] = false;
                }
                if ($v->isshowlastpage) {
                    $rmrd['showoflp'] = true;
                } else {
                    $rmrd['showoflp'] = false;
                }
            } else if ($v->nama == 'Even Footer') {
                if ($v->isshoweverypage) {
                    // $rmrd['ef'] = '<div class="ef">'.$template->reportfooter.'</div>';
                    // $rmrd['sdptef'] = $showdatapertable;
                    $rmrd['showef'] = true;
                    // $rmrd['ef0'] = '<div class="ef0">'.$template->reportfooter.'</div>';
                    // $rmrd['ef0_'] = '<div class="ef0_">'.$template->reportfooter.'</div>';
                    // $rmrd['hfef0'] = $v->widgetheight;
                } else {
                    // $rmrd['ef0'] = '<div class="ef0">&nbsp;</div>';
                    // // $rmrd['ef0_'] = '<div class="ef0_">&nbsp;</div>';
                    // $rmrd['hfef0'] = 0;
                    // $rmrd['sdpt0'] += 6;
                    // $rmrd['ef'] = '<div class="ef">&nbsp;</div>';
                    // $rmrd['sdptef'] = $sdptef;
                    $rmrd['showef'] = false;
                }
                // $rmrd['hfef0_'] = $v->widgetheight;
                // // dd($rmrd['showfh'].'-'.$rmrd['showef'].'/'.$rmrd['sdpt0']);
                // $rmrd['marginef'] = '0px 0px 0px 0px';
                $rmrd['heightef'] = $v->widgetheight;
            // } else if ($v->nama == 'Last Footer') {
            //     $rmrd['isshoweverypagelf'] = $v->isshoweverypage;
            //     $rmrd['marginlf'] = '0px 0px 0px 0px';
            //     $heightlf = $v->widgetheight;
            //     if ($v->isshoweverypage) {
            //         $rmrd['showlf'] = true;
            //     } else {
            //         $rmrd['showlf'] = false;
            //     }
                /////
                if ($v->isshowfirstpage) {
                    $rmrd['showeffp'] = true;
                } else {
                    $rmrd['showeffp'] = false;
                }
                if ($v->isshoweverypage) {
                    $rmrd['showefep'] = true;
                } else {
                    $rmrd['showefep'] = false;
                }
                if ($v->isshowlastpage) {
                    $rmrd['showeflp'] = true;
                } else {
                    $rmrd['showeflp'] = false;
                }
            }
        }

        //SHOW(FH,FD,FF)
        // if ($rmrd['showfh'] || $rmrd['showohfp']) {
        //     $rmrd['showfh'] = true;
        // }
        // if ($rmrd['showfd'] || $rmrd['showod']) {
        //     $rmrd['showfd'] = true;
        // }
        // if ($rmrd['showff'] || $rmrd['showof']) {
        //     $rmrd['showff'] = true;
        // }

        //SDPTF
        // if ($rmrd['showfd'] && $rmrd['showod']) {
        //     $rmrd['sdptf'] = $showdatapertable;
        // } else if (!$rmrd['showfd'] && $rmrd['showod']) {
        //     $rmrd['sdptf'] = $showdatapertable;
        // } else if ($rmrd['showfd'] && !$rmrd['showod']) {
        //     $rmrd['sdptf'] = $showdatapertable;
        // } else if (!$rmrd['showfd'] && !$rmrd['showod']) {
        //     $rmrd['sdptf'] = 0;
        // }

        $rmrd['_heightfh'] = $rmrd['heightfh'];
        if ($rmrd['showfh']) {
            // $rmrd['heightfh'] -= 0;
            $rmrd['fh'] = $template->reportheader;
        } else {
            if ($rmrd['showof']) {
                $rmrd['fh'] = $template->reportheader;
            } else {
                // $rmrd['heightfh'] -= $rmrd['heightfh'];
                // $rmrd['heightfd'] += $rmrd['heightff'];
            }
        };
        // if ($rmrd['showfd']) { $rmrd['heightfd'] -= 0; } else { $rmrd['heightfd'] -= $rmrd['heightfd']; };
        if ($rmrd['showff']) { $rmrd['heightff'] -= 0; } else { $rmrd['heightff'] -= $rmrd['heightff']; };

        //SDPTO
        // if ($rmrd['showod'] && $rmrd['showothd']) {
        //     $rmrd['sdpto'] = $showdatapertable;
        // } else if (!$rmrd['showod'] && $rmrd['showothd']) {
        //     $rmrd['sdpto'] = $showdatapertable;
        // } else if ($rmrd['showod'] && !$rmrd['showothd']) {
        //     $rmrd['sdpto'] = $showdatapertable;
        // } else if (!$rmrd['showod'] && !$rmrd['showothd']) {
        //     $rmrd['sdpto'] = 0;
        // }

        $rmrd['_heightoh'] = $rmrd['heightoh'];
        // if ($rmrd['showoh'] || $rmrd['showothh']) { $rmrd['heightoh'] -= 0; $rmrd['oh'] = $template->reportheader; } else { $rmrd['heightoh'] -= $rmrd['heightoh']; };
        // if ($rmrd['showod']) { $rmrd['heightod'] -= 0; } else { $rmrd['heightod'] -= $rmrd['heightod']; };
        if ($rmrd['showof'] || $rmrd['showothf']) { $rmrd['heightof'] -= 0; $rmrd['of'] = $template->reportfooter; } else { $rmrd['heightof'] -= $rmrd['heightof']; };

        //SDPTE
        // if ($rmrd['showed'] && $rmrd['showothd']) {
        //     $rmrd['sdpte'] = $showdatapertable;
        // } else if (!$rmrd['showed'] && $rmrd['showothd']) {
        //     $rmrd['sdpte'] = $showdatapertable;
        // } else if ($rmrd['showed'] && !$rmrd['showothd']) {
        //     $rmrd['sdpte'] = $showdatapertable;
        // } else if (!$rmrd['showed'] && !$rmrd['showothd']) {
        //     $rmrd['sdpte'] = 0;
        // }

        $rmrd['_heighteh'] = $rmrd['heighteh'];
        // if ($rmrd['showeh'] || $rmrd['showothh']) { $rmrd['heighteh'] -= 0; $rmrd['eh'] = $template->reportheader; } else { $rmrd['heighteh'] -= $rmrd['heighteh']; };
        // if ($rmrd['showed']) { $rmrd['heighted'] -= 0; } else { $rmrd['heighted'] -= $rmrd['heighted']; };
        if ($rmrd['showef'] || $rmrd['showothf']) { $rmrd['heightef'] -= 0; $rmrd['ef'] = $template->reportfooter; } else { $rmrd['heightef'] -= $rmrd['heightef']; };


        //_SDPTF
        // $rmrd['_sdptf'] = $rmrd['sdptf'];
        // if ($rmrd['showfh'] && $rmrd['showoh']) {
        //     $rmrd['_sdptf'] += 0;
        // } else if (!$rmrd['showfh'] && $rmrd['showoh']) {
        //     $rmrd['_sdptf'] += 0;
        //     $rmrd['heightfd'] += $rmrd['heightfh']+16;
        // } else if ($rmrd['showfh'] && !$rmrd['showoh']) {
        //     $rmrd['_sdptf'] += 0;
        //     $rmrd['heightfd'] += 0;
        // } else if (!$rmrd['showfh'] && !$rmrd['showoh']) {
        //     $rmrd['_sdptf'] += 3;
        //     $rmrd['heightfd'] += $rmrd['heightfh']+16;
        // }
        // $rmrd['sdptf'] = $rmrd['_sdptf'];

        // dd($rmrd['sdpto']);
        //_SDPTO
        // if ($rmrd['sdpto'] != 0) {
        //     $rmrd['_sdpto'] = $rmrd['sdpto'];
        //     if ($rmrd['showeh'] && $rmrd['showothh']) {
        //         $rmrd['_sdpto'] += 0;
        //     } else if (!$rmrd['showeh'] && $rmrd['showothh']) {
        //         $rmrd['_sdpto'] += 0;
        //         // $rmrd['heightod'] += $rmrd['_heightoh']+16;
        //         // $rmrd['heightod'] += $rmrd['heightoh']+16;
        //     } else if ($rmrd['showeh'] && !$rmrd['showothh']) {
        //         $rmrd['_sdpto'] += 0;
        //         // $rmrd['heightod'] += $rmrd['_heightoh']+16;
        //         // $rmrd['heightod'] += $rmrd['heightoh']+16;
        //     } else if (!$rmrd['showeh'] && !$rmrd['showothh']) {
        //         $rmrd['_sdpto'] += 3;
        //         // $rmrd['heightod'] += $rmrd['_heightoh']+16;
        //         // $rmrd['heightod'] += $rmrd['heightoh']+16;
        //     }

        //     // if ($rmrd['showothh']) {
        //     //     $rmrd['heightod'] -= $rmrd['_heightoh']+16;
        //     // }

        //     if ($rmrd['showof'] && $rmrd['showothf']) {
        //         $rmrd['_sdpto'] += 0;
        //     } else if (!$rmrd['showof'] && $rmrd['showothf']) {
        //         $rmrd['_sdpto'] += 0;
        //     } else if ($rmrd['showof'] && !$rmrd['showothf']) {
        //         $rmrd['_sdpto'] += 0;
        //     } else if (!$rmrd['showof'] && !$rmrd['showothf']) {
        //         $rmrd['_sdpto'] += 3;
        //     }
        //     $rmrd['sdpto'] = $rmrd['_sdpto'];
        // }
        

        //_SDPTE
        // if ($rmrd['sdpte'] != 0) {
        //     $rmrd['_sdpte'] = $rmrd['sdpte'];
        //     if ($rmrd['showoh'] && $rmrd['showothh']) {
        //         $rmrd['_sdpte'] += 0;
        //     } else if (!$rmrd['showoh'] && $rmrd['showothh']) {
        //         $rmrd['_sdpte'] += 0;
        //         // $rmrd['heighted'] += $rmrd['_heighteh']+16;
        //     } else if ($rmrd['showoh'] && !$rmrd['showothh']) {
        //         $rmrd['_sdpte'] += 0;
        //         // $rmrd['heighted'] += $rmrd['_heighteh']+16;
        //     } else if (!$rmrd['showoh'] && !$rmrd['showothh']) {
        //         $rmrd['_sdpte'] += 3;
        //         // $rmrd['heighted'] += $rmrd['_heighteh']+16;
        //     }

        //     // if ($rmrd['showothh']) {
        //     //     $rmrd['heighted'] -= $rmrd['_heighteh']+16;
        //     // }

        //     if ($rmrd['showef'] && $rmrd['showothf']) {
        //         $rmrd['_sdpte'] += 0;
        //     } else if (!$rmrd['showef'] && $rmrd['showothf']) {
        //         $rmrd['_sdpte'] += 0;
        //     } else if ($rmrd['showef'] && !$rmrd['showothf']) {
        //         $rmrd['_sdpte'] += 0;
        //     } else if (!$rmrd['showef'] && !$rmrd['showothf']) {
        //         $rmrd['_sdpte'] += 3;
        //     }
        //     $rmrd['sdpte'] = $rmrd['_sdpte'];
        // }
        // dd($rmrd['sdpte']);
        // dd($rmrd['heighted']);
        // dd($rmrd['showefep']);

        //SDPTL
        // if ($rmrd['showlh'] && $rmrd['showld'] && $rmrd['showlf']) {
        //     $rmrd['sdptl'] = $showdatapertable;
        // } else if (!$rmrd['showlh'] && $rmrd['showld'] && $rmrd['showlf']) {
        //     $rmrd['sdptl'] = $showdatapertable+3;
        // } else if ($rmrd['showlh'] && !$rmrd['showld'] && $rmrd['showlf']) {
        //     $rmrd['sdptl'] = 0;
        // } else if ($rmrd['showlh'] && $rmrd['showld'] && !$rmrd['showlf']) {
        //     $rmrd['sdptl'] = $showdatapertable+3;
        // } else if (!$rmrd['showlh'] && !$rmrd['showld'] && $rmrd['showlf']) {
        //     $rmrd['sdptl'] = 0;
        // } else if (!$rmrd['showlh'] && $rmrd['showld'] && !$rmrd['showlf']) {
        //     $rmrd['sdptl'] = $showdatapertable+6;
        // } else if ($rmrd['showlh'] && !$rmrd['showld'] && !$rmrd['showlf']) {
        //     $rmrd['sdptl'] = 0;
        // } else if (!$rmrd['showlh'] && !$rmrd['showld'] && !$rmrd['showlf']) {
        //     $rmrd['sdptl'] = 0;
        // }

        // if ($rmrd['showlh']) { $rmrd['heightlh'] -= 0; $rmrd['lh'] = $template->reportheader; } else { $rmrd['heightlh'] -= $rmrd['heightlh']; };
        // if ($rmrd['showld']) { $rmrd['heightld'] -= 0; } else { $rmrd['heightld'] -= $rmrd['heightld']; };
        if ($rmrd['showlf']) { $rmrd['heightlf'] -= 0; } else { $rmrd['heightlf'] -= $rmrd['heightlf']; };

        //FP
        $rmrd['sdptf'] = $showdatapertable;
        if ($rmrd['showfhfp'] || $rmrd['showothhfp'] || $rmrd['showlhfp'] || $rmrd['showohfp'] || $rmrd['showehfp']) {
        } else {
            $rmrd['sdptf'] += 3;
            $rmrd['heightfd'] += $rmrd['heightfh']+16;
        }
        if ($rmrd['showfdfp'] || $rmrd['showothdfp'] || $rmrd['showldfp'] || $rmrd['showodfp'] || $rmrd['showedfp']) {
            $rmrd['sdptf'] = $showdatapertable;
        }
        // dd($rmrd['sdptf']);
        if ($rmrd['showfffp'] || $rmrd['showothffp'] || $rmrd['showlffp'] || $rmrd['showoffp'] || $rmrd['showeffp']) {
        } else {
            $rmrd['sdptf'] += 3;
        }
        // dd($rmrd['heightfh']);
        //EP
        if ($rmrd['showfhep'] || $rmrd['showothhep'] || $rmrd['showlhep']) {
            $rmrd['sdpte'] = $showdatapertable;
            $rmrd['sdpto'] = $showdatapertable;
            if ($rmrd['showffep'] || $rmrd['showothfep'] || $rmrd['showlfep']) {
            } else if ($rmrd['showofep'] && $rmrd['showefep']) {
                $rmrd['sdpte'] = $showdatapertable;
                $rmrd['sdpto'] = $showdatapertable;
            } else if (!$rmrd['showofep'] && $rmrd['showefep']) {
                $rmrd['sdpte'] = $showdatapertable;
                $rmrd['sdpto'] = $showdatapertable+3;
            } else if ($rmrd['showofep'] && !$rmrd['showefep']) {
                $rmrd['sdpte'] = $showdatapertable+3;
                $rmrd['sdpto'] = $showdatapertable;
            } else {
                $rmrd['sdpte'] = $showdatapertable+3;
                $rmrd['sdpto'] = $showdatapertable+3;
            }
            // dd($rmrd['heighted']);
        } else if ($rmrd['showohep'] && $rmrd['showehep']) {
            $rmrd['sdpte'] = $showdatapertable;
            $rmrd['sdpto'] = $showdatapertable;
            if ($rmrd['showffep'] || $rmrd['showothfep'] || $rmrd['showlfep']) {
            } else if ($rmrd['showofep'] && $rmrd['showefep']) {
                $rmrd['sdpte'] += 0;
                $rmrd['sdpto'] += 0;
            } else if (!$rmrd['showofep'] && $rmrd['showefep']) {
                $rmrd['sdpte'] += 0;
                $rmrd['sdpto'] += 3;
            } else if ($rmrd['showofep'] && !$rmrd['showefep']) {
                $rmrd['sdpte'] += 3;
                $rmrd['sdpto'] += 0;
            } else {
                $rmrd['sdpte'] = $showdatapertable+3;
                $rmrd['sdpto'] = $showdatapertable+3;
            }
        } else if (!$rmrd['showohep'] && $rmrd['showehep']) {
            $rmrd['sdpte'] = $showdatapertable;
            $rmrd['sdpto'] = $showdatapertable+3;
            $rmrd['heightod'] += $rmrd['heightoh']+16;
            if ($rmrd['showffep'] || $rmrd['showothfep'] || $rmrd['showlfep']) {
            } else if ($rmrd['showofep'] && $rmrd['showefep']) {
                $rmrd['sdpte'] += 0;
                $rmrd['sdpto'] += 0;
            } else if (!$rmrd['showofep'] && $rmrd['showefep']) {
                $rmrd['sdpte'] += 0;
                $rmrd['sdpto'] += 3;
            } else if ($rmrd['showofep'] && !$rmrd['showefep']) {
                $rmrd['sdpte'] += 3;
                $rmrd['sdpto'] += 0;
            } else {
                $rmrd['sdpte'] += 3;
                $rmrd['sdpto'] += 3;
            }
        } else if ($rmrd['showohep'] && !$rmrd['showehep']) {
            $rmrd['sdpte'] = $showdatapertable+3;
            $rmrd['sdpto'] = $showdatapertable;
            $rmrd['heightod'] += $rmrd['heightoh']+16;
            if ($rmrd['showffep'] || $rmrd['showothfep'] || $rmrd['showlfep']) {
            } else if ($rmrd['showofep'] && $rmrd['showefep']) {
                $rmrd['sdpte'] += 0;
                $rmrd['sdpto'] += 0;
            } else if (!$rmrd['showofep'] && $rmrd['showefep']) {
                $rmrd['sdpte'] += 0;
                $rmrd['sdpto'] += 3;
            } else if ($rmrd['showofep'] && !$rmrd['showefep']) {
                $rmrd['sdpte'] += 3;
                $rmrd['sdpto'] += 0;
            } else {
                $rmrd['sdpte'] += 3;
                $rmrd['sdpto'] += 3;
            }
        } else if (!$rmrd['showohep'] && !$rmrd['showehep']) {
            $rmrd['sdpte'] = $showdatapertable+3;
            $rmrd['sdpto'] = $showdatapertable+3;
            if ($rmrd['showffep'] || $rmrd['showothfep'] || $rmrd['showlfep']) {
            } else if ($rmrd['showofep'] && $rmrd['showefep']) {
                $rmrd['sdpte'] += 0;
                $rmrd['sdpto'] += 0;
            } else if (!$rmrd['showofep'] && $rmrd['showefep']) {
                $rmrd['sdpte'] += 0;
                $rmrd['sdpto'] += 3;
            } else if ($rmrd['showofep'] && !$rmrd['showefep']) {
                $rmrd['sdpte'] += 3;
                $rmrd['sdpto'] += 0;
            } else {
                $rmrd['sdpte'] += 3;
                $rmrd['sdpto'] += 3;
            }
        } else {
            if ($rmrd['showohep']){
                $rmrd['sdpto'] = $showdatapertable;
                // $rmrd['heightod'] += $rmrd['heightoh']+16;
            } else {
                $rmrd['sdpto'] = $showdatapertable+3;
                // $rmrd['heighted'] += $rmrd['heighteh']+16;
            }
            if ($rmrd['showehep']){
                $rmrd['sdpte'] = $showdatapertable;
                // $rmrd['heighted'] += $rmrd['heighteh']+16;
            } else {
                $rmrd['sdpte'] = $showdatapertable+3;
                // $rmrd['heightod'] += $rmrd['heightoh']+16;
            }
        }
        // dd($rmrd['heightod']);
        if ($rmrd['showffep'] || $rmrd['showothfep'] || $rmrd['showlfep']) {
            // $rmrd['sdpto'] = $showdatapertable;
            // $rmrd['sdpte'] = $showdatapertable;
        // } else if ($rmrd['showofep']){
            // if ($rmrd['showehep']) {
            //     $rmrd['sdpte'] += 3;
            // }
            // $rmrd['sdpto'] = $showdatapertable;
        // } else if ($rmrd['showefep']){
            // $rmrd['sdpto'] = $showdatapertable;
        } else {
            // $rmrd['sdpto'] = $showdatapertable+3;
            // $rmrd['sdpte'] = $showdatapertable+3;
        }
        //LP
        $rmrd['sdptl'] = $showdatapertable;
        if ($rmrd['showfhlp'] || $rmrd['showothhlp'] || $rmrd['showlhlp'] || $rmrd['showohlp'] || $rmrd['showehlp']) {
        } else {
            $rmrd['sdptl'] += 3;
            $rmrd['heightld'] += $rmrd['heightlh']+16;
        }
        if ($rmrd['showfflp'] || $rmrd['showothflp'] || $rmrd['showlflp'] || $rmrd['showoflp'] || $rmrd['showeflp']) {
        } else {
            $rmrd['sdptl'] += 3;
        }
        // dd($rmrd['heightld']);

        $rmrd['sdpt0'] = $rmrd['sdptf'];
        $rmrd['sdptoh'] = $rmrd['sdpto'];
        $rmrd['sdptof'] = $rmrd['sdpto'];
        $rmrd['sdpteh'] = $rmrd['sdpte'];
        $rmrd['sdptef'] = $rmrd['sdpte'];
        
        $html = '<!DOCTYPE html>
                <html lang="">
                    <head>
                        <meta charset="utf-8">
                        <meta http-equiv="X-UA-Compatible" content="IE=edge">
                        <meta name="viewport" content="width=device-width, initial-scale=1">
                        <title>'.$template->nama.'</title>
                        <style>
                            $css
                            
                            #watermark1 {
                                position: fixed;
                                top:   21cm;
                                // left:     1.5cm;
                                width:    8cm;
                                height:   8cm;
                                z-index:  -1000;
                                transform: rotate(300deg); 
                                color: #f9f8f7;
                                font-size: 50px;
                            }

                            #watermark2 {
                                position: fixed;
                                top:   10cm;
                                left:     -5cm;
                                width:    8cm;
                                height:   8cm;
                                z-index:  -1000;
                                transform: rotate(300deg); 
                                color: #f9f8f7;
                                font-size: 50px;
                            }
                        </style>
                    </head>
                    <body>
                        <div id="watermark1"> 
                            <h1>'.$template->reportwatermark.'</h1>
                        </div> 

                        <div id="watermark2"> 
                            <h1>'.$template->reportwatermark.'</h1>
                        </div> 

                        $content
                    </body>
                </html>';

        $body_margin = $template->margintop.' '.$template->marginright.' '.$template->marginbottom.' '.$template->marginleft;

        foreach ($jsonrules->datarec as $k => $v) {
            if ($v->fixed) {
                $isfixedcontent1 = '';
                break;
            } else {
                $isfixedcontent1 = 'width: 100%;
                                    table-layout: fixed;';
            }
        }
        $css = 'body {
                    font-family: "MS Serif", "New York", sans-serif;
                    font-size: 11px;
                    margin: '.$body_margin.';
                }

                .container {
                    // position: fixed;
                }

                .footer {
                    position: fixed;
                    margin: '.$body_margin.';
                }

                .font-size-address {
                    font-size: 10px
                }

                .img-logo {
                    height: 50px;
                    margin: 0px 0px 0px 10px;
                }

                .address {
                    margin: -60px 0px 0px 65px;
                    font-size: 8px;
                }

                .title {
                    position: absolute;
                    z-index: 10;
                    margin: -48px 0px 80px '.$headermiddleml.'px;
                    font-size: 12px;
                }

                .title div {
                    border-style: double;
                    padding: 3px 20px 3px 20px;
                    width: 230px;
                }

                .title b {
                    font-size: 20px;
                }

                .title p {
                    margin: 10px 0px 0px 0px;
                }

                .date {
                    margin: -65px 0px 20px '.$headerlastml.'px;
                    font-size: 12px;
                }

                .dibayar-kepada {
                    margin: 0px 0px 0px 15px;
                }

                .content-1 {
                    height: '.$heightdetail.'px;
                }
                
                .table-custom-1 {
                    '.$isfixedcontent1.'
                    margin: 0px 0px 0px 0px;
                    border-collapse: collapse;
                }

                .table-custom-1, .table-custom-1 thead tr th, .table-custom-1 tbody tr td {
                    border: 1px solid black;
                    padding: 3px 3px 3px 3px;
                }
                
                .table-custom-1 thead tr th:nth-child(1) {
                    width: '.$content1width[0].'px;
                }

                .table-custom-1 thead tr th:nth-child(2) {
                    width: '.$content1width[1].'px;
                }

                .table-custom-1 thead tr th:nth-child(3) {
                    width: '.$content1width[2].'px;
                }

                .table-custom-1 thead tr th:nth-child(4) {
                    width: '.$content1width[3].'px;
                }

                .table-custom-1 thead tr th:nth-child(5) {
                    width: '.$content1width[4].'px;
                }

                .table-custom-1 thead tr th:nth-child(6) {
                    width: '.@$content1width[5].'px;
                }

                .table-custom-1 tbody tr .giro-cek {
                    border-left: 1px solid white !important;
                    border-bottom: 1px solid white !important;
                }

                .giro-cek b {
                    margin-bottom: 0px !important;
                }



                .table-custom-2 {
                    // width: 100%;
                    border-collapse: collapse;
                }

                .table-custom-2 thead tr th:nth-child(1) {
                    width: '.$content2width[0].'px;
                }
                
                .table-custom-2 thead tr th:nth-child(2) {
                    width: '.$content2width[1].'px;
                }

                .table-custom-2 thead tr th:nth-child(3) {
                    width: '.$content2width[2].'px;
                }

                .table-custom-2 thead tr th:nth-child(4) {
                    width: '.$content2width[3].'px;
                }

                .table-custom-2 thead tr th:nth-child(5) {
                    width: '.$content2width[4].'px;
                }

                .table-custom-2, .table-custom-2 thead tr th, .table-custom-2 tbody tr td {
                    border: 1px solid black;
                    padding: 5px 3px 5px 3px;
                    text-align: center;
                }
                
                .text-right {
                    text-align: right;
                }

                .page-break {
                    page-break-after: always;
                }
                '
        ;
        
        
        $html = str_replace('$css', $css, $html);
        
        /////////
        if (count($resultdetail) >= $showdatapertable) {
            $qtydatadetail = count($resultdetail)-count($resultdetail)%$showdatapertable;
            $qtydatadetail = $qtydatadetail/$showdatapertable;
            if (count($resultdetail)%$showdatapertable != 0) {
                $qtydatadetail++;
            }
        } else {
            $qtydatadetail = 1;
        }

        $start = 0;
        $finish = $showdatapertable-1;
        for ($i=0; $i<$qtydatadetail; $i++) {
            // $plusfinish = 0;
            if ($i == 0) {
                if ($rmrd['sdpt0'] != $showdatapertable) {
                    $plusfinish = $rmrd['sdpt0'];
                } else {
                    $plusfinish = $showdatapertable;
                }
            } else {
                if ($i%2 == 0) {
                    // if ($rmrd['sdpteh'] != $showdatapertable) {
                    //     // $plusfinish = $rmrd['sdpteh'];
                    //     $plusfinish = $showdatapertable;
                    // } else {
                    //     $plusfinish = $showdatapertable;
                    // }
                    // if ($rmrd['sdptef'] != $showdatapertable) {
                    //     // $plusfinish = $rmrd['sdptef'];
                    //     $plusfinish = $showdatapertable;
                    // } else {
                    //     $plusfinish = $showdatapertable;
                    // }
                    // if ($rmrd['sdpte'] > $showdatapertable) {
                        $plusfinish = $rmrd['sdpto'];
                    // }
                } else {
                    // if ($rmrd['sdptoh'] != $showdatapertable) {
                    //     // $plusfinish = $rmrd['sdptoh'];
                    //     $plusfinish = $showdatapertable;
                    // } else {
                    //     $plusfinish = $showdatapertable;
                    // }
                    // if ($rmrd['sdptof'] != $showdatapertable) {
                    //     // $plusfinish = $rmrd['sdptof'];
                    //     $plusfinish = $showdatapertable;
                    // } else {
                    //     $plusfinish = $showdatapertable;
                    // }
                    // if ($rmrd['sdpto'] > $showdatapertable) {
                        $plusfinish = $rmrd['sdpte'];
                    // }
                }
            }
            // if ($i == 1) {
            //     dd($rmrd['sdpte']);
            // }
            $finish = $start+@($plusfinish-1);
            $dataqtydatadetail[] = (Object)[
                'start' => $start,
                'finish' => $finish
            ];
            $start = $finish+1;
            $finish = $start+@($plusfinish-1);
        }
        // dd($dataqtydatadetail);
        // dd($dataqtydatadetail);

        $qlr_ = [];
        foreach ($dataqtydatadetail as $k => $v) {
            $qlr_[$k] = 0;
            foreach ($resultdetail as $k2 => $v2) {
                if ($k2 >= $v->start && $k2 <= $v->finish) {
                    $qlr_[$k]++;
                }
            }
        }

        for ($i=count($qlr_)-1; $i>0; $i--) {
            if ($qlr_[$i] != 0) {
                break;
            }
            array_pop($qlr_);
            array_pop($dataqtydatadetail);
        }
        // die;
        // dd($qlr_);
        // dd($qlr_[array_key_last($qlr_)]);
        // dd($rmrd['sdpte']);

        $rowenterrow = $jsonrules->showdatapertable;
        // dd($qlr_);

        $isenterrow = false;
        // if ($rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] ||
        //     $rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] ||
        //     $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] ||
        //     !$rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] ||
        //     !$rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] ||
        //     !$rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] ||
        //     !$rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] ||
        //     $rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] ||
        //     $rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef']) {
            if ($qlr_[array_key_last($qlr_)] > $rowenterrow) {
                array_push($qlr_, $qlr_[array_key_last($qlr_)]-$rowenterrow);
                $qlr_[count($qlr_)-2] -= $qlr_[array_key_last($qlr_)];
                $dataqtydatadetail[array_key_last($dataqtydatadetail)]->finish = $dataqtydatadetail[array_key_last($dataqtydatadetail)]->start+$qlr_[array_key_last($dataqtydatadetail)]-1;
                // dd(count($resultdetail)-1);
                if ($dataqtydatadetail[array_key_last($dataqtydatadetail)]->finish <= count($resultdetail)-1) {
                    array_push($dataqtydatadetail, (object)[
                        'start' => $dataqtydatadetail[array_key_last($dataqtydatadetail)]->finish+1,
                        'finish' => $dataqtydatadetail[array_key_last($dataqtydatadetail)]->finish+1+($showdatapertable*2)
                    ]);
                }
                // dd($dataqtydatadetail);
                $isenterrow = true;
            }
            // dd('asd');
        // }
        
        // dd($rowenterrow);
        // dd($dataqtydatadetail);
        // dd($rowenterrow);

        if ($custom['status'] && ($custom['tablemaster'] == 'purchaserequest' || $custom['tablemaster'] == 'purchaseoffer')) {
            $pof = DB::connection('mysql2')
                    ->table('purchaseoffer AS pof')
                    ->leftJoin('purchaseofferdetail AS pofd', 'pof.noid', '=', 'pofd.idmaster')
                    ->leftJoin('mcardsupplier AS mcs', 'pof.idcardsupplier', '=', 'mcs.noid')
                    ->select([
                        'pof.kode AS pof_noid',
                        'pof.idcardsupplier AS pof_idcardsupplier',
                        'pofd.noid AS pofd_noid',
                        'pofd.idmaster AS pofd_idmaster',
                        'pofd.idinventor AS pofd_idinventor',
                        'pofd.namainventor AS pofd_namainventor',
                        'pofd.hargatotal AS pofd_hargatotal',
                        'pofd.idtrancprevd AS pofd_idtrancprevd',
                        'mcs.noid AS mcs_noid',
                        'mcs.nama AS mcs_nama'
                    ])
                    ->where('pof.kodereff', $resultmaster->kodereff)
                    ->orderBy('pof.noid', 'asc')
                    ->get();
            // $supplier = DB::connection('mysql2')
            //                 ->table('mcard')

            $supplier = [];
            $datapof = [];
            $datasupplier = '';
            if ($custom['tablemaster'] == 'purchaseoffer') {
                foreach($pof as $k => $v) {
                    if (!array_key_exists('supplier-'.$v->mcs_noid, $supplier)) {
                        $supplier['supplier-'.$v->mcs_noid] = $v->mcs_nama;
                    }
                    $datapof[$v->pofd_idinventor]['supplier-'.$v->mcs_noid] = (object)[
                        'hargatotal' => $v->pofd_hargatotal
                    ];
                }
                if ($custom['tablemaster'] == 'purchaseoffer') {
                    foreach ($supplier as $k => $v) {
                        $datasupplier .= '<th>'.substr($v,0,8).'</th>';
                    }
                }
            }

            $temprd = $resultdetail;
            foreach ($temprd as $k => $v) {
                $other = [];
                foreach ($supplier as $k2 => $v2) {
                    $other[$k2] = $datapof[$v->idinventor][$k2]->hargatotal;
                }
                $resultdetail[$k] = (object)array_merge((array)$resultdetail[$k], $other);
            }
        }

        $content = '';
        $total = 0;
        $no = 1;
        $qtylastrecord = [];
        foreach ($dataqtydatadetail as $k => $v) {
            if ($k == 0) {
                if ($rmrd['showfhfp'] || $rmrd['showothhfp'] || $rmrd['showlhfp'] || $rmrd['showohfp'] || $rmrd['showehfp']) {
                    $content .= '<div style="height: '.$rmrd['heightfh'].'">'.$templatedetail['First Header']->phpsource.'</div>';
                }
            } else if ($k > 0 && $k < count($dataqtydatadetail)-1) {
                if ($rmrd['showfhep'] || $rmrd['showothhep'] || $rmrd['showlhep']) {
                    // dd($rmrd['heighteh']);
                    if ($k%2 == 0) {
                        $content .= '<div style="height: '.$rmrd['heightoh'].'">'.$templatedetail['Other Header']->phpsource.'</div>';
                    } else {
                        $content .= '<div style="height: '.$rmrd['heighteh'].'">'.$templatedetail['Other Header']->phpsource.'</div>';
                    }
                } else {
                    if ($k%2 == 0) {
                        if ($rmrd['showohep']) {
                            $content .= '<div style="height: '.$rmrd['heightoh'].'">'.$templatedetail['Odd Header']->phpsource.'</div>';
                        }
                    }
                // } else if ($rmrd['showehep']){
                    if ($k%2 == 1) {
                        if ($rmrd['showehep']) {
                            $content .= '<div style="height: '.$rmrd['heighteh'].'">'.$templatedetail['Even Header']->phpsource.'</div>';
                        }
                    }
                // } else {
                    // if ($k%2 == 0) {
                    //     $content .= '<div style="height: '.$rmrd['heightoh'].'">'.@$rmrd['oh'].'</div>';
                    // } else {
                    //     $content .= '<div style="height: '.$rmrd['heighteh'].'">'.@$rmrd['eh'].'</div>';
                    // }
                }
            } else if ($k == count($dataqtydatadetail)-1) {
                if ($rmrd['showfhlp'] || $rmrd['showothhlp'] || $rmrd['showlhlp'] || $rmrd['showohlp'] || $rmrd['showehlp']) {
                    $content .= '<div style="height: '.$rmrd['heightlh'].'">'.$templatedetail['Last Header']->phpsource.'</div>';
                }
            }

            $record = '';
            $qtylasttable = 0;
            $subtotal = 0;
            $isappend = false;
            $qtylastrecord[$k] = 0;
            

            $qtyrow = 0;
            if ($custom['tablemaster'] == 'purchaserequest') {
                // $qtyrow = 47;
                $qtyrow = 43;
            } else if ($custom['tablemaster'] == 'purchaseoffer') {
                $qtyrow = 42;
            } else if ($custom['tablemaster'] == 'purchaseorder') {
                $qtyrow = 38;
            }

            $totalprices = 0;
            foreach ($resultdetail as $k2 => $v2) {
                // $heighttd = 20;
                if ($k2 >= $v->start && $k2 <= $v->finish) {
                    $isappend = true;
                    $record .= "<tr>";
                    foreach ($jsonrules->datarec as $k3 => $v3) {
                        $field = $v3->field;
                        if ($field == 'subtotal') {
                            $value = $v2->$field;
                        } else if ($field == 'nopo') {
                            $value = $resultmaster->kodereff;
                        } else if ($field == 'noinvoice') {
                            $value = $resultmaster->kode;
                        }

                        if ($custom['status']) {
                            if (in_array($field, $custom['currency'])) {
                                $v2->$field = 'Rp. '.number_format((int)$v2->$field);
                            }
                        }

                        if ($field == 'no') {
                            $value = $no;
                        } else if ($field == 'namainventor2'){
                            $maxchar = 55;
                            $length = strlen($v2->$field);
                            $rest = fmod($length, $maxchar);
                            $loop = ($length-$rest)/$maxchar;

                            if ($loop > 0) {
                                for ($i=0; $i<=$loop; $i++) {
                                    if ($i == 0) {
                                        $cutstring = substr($v2->$field, 0, $maxchar);
                                        $first = 0;
                                        $last = strrpos($cutstring, ' ');
                                        $string = substr($cutstring, $first, $last);
                                    } else {
                                        $cutstring = $string.'<br>'.substr($v2->$field, $last+1, $maxchar);
                                        $first = $last;
                                        $last = strrpos($cutstring, ' ');
                                        $string .= substr($cutstring, $first, $last);
                                        $qtyrow--;
                                        // $heighttd += 15;
                                    }
                                }
                                $value = $string;
                            } else {
                                $value = $v2->$field;
                            }

                            // dd($string);
                            
                        } else if ($field == 'idcardsupplier'){
                            $tempv2 = (array)$v2;
                            foreach ($supplier as $k4 => $v4) {
                                $record .= "<td style='width: 10%; background: #06e524; text-align: ".$v3->textalign.";'>".number_format($tempv2[$k4])."</td>";
                            }
                            continue;
                        } else if ($field == 'subtotal'){
                            $value = 'Rp. '.number_format((int)$value);
                            $totalprices += filter_var($value, FILTER_SANITIZE_NUMBER_INT);
                        } else {
                            if ($v3->maxchar != 0) {
                                if (strlen($v2->$field) >= $v3->maxchar) {
                                    $value = @substr($v2->$field, 0, $v3->maxchar).'...';
                                } else {
                                    $value = $v2->$field;
                                }
                            } else if ($field == 'nopo' || $field == 'noinvoice'){
                            } else {
                                $value = $v2->$field;
                            }
                        }

                        if ($v3->wordwrap) {
                            $wordwrap = 'break-all';
                        } else {
                            $wordwrap = 'keep-all';
                        }
                        // if ($k3 == 0) {
                        //     $content1width[$k3] = 4;
                        // } else if ($k3 == 1) {
                        //     $content1width[$k3] = 46;
                        // } else if ($k3 == 2) {
                        //     $content1width[$k3] = 12;
                        // } else if ($k3 == 3) {
                        //     $content1width[$k3] = 12;
                        // } else if ($k3 == 4) {
                        //     $content1width[$k3] = 12;
                        // } else if ($k3 == 5) {
                        //     $content1width[$k3] = 14;
                        // }

                        if (!$v3->wordwrap) {
                            $value = str_replace(' ', '&nbsp;', $value);
                        }
                        if ($v3->fixed) {
                            $fixed = 'px';
                        } else {
                            $fixed = '%';
                        }
                        
                        $record .= "<td style='width: ".@$content1width[$k3]."%; text-align: ".$v3->textalign.";'>".$value."</td>";
                    }
                    $record .= "</tr>";
                    $no++;
                    $qtylasttable++;
                    if ($custom['status']) {
                        $subtotal += @filter_var(((array)$v2)[$custom['subtotal']], FILTER_SANITIZE_NUMBER_INT);
                    } else {
                        $subtotal += @$v2->nominal;
                    }
                    // if (count($dataqtydatadetail) == $k+1) {
                        $qtylastrecord[$k]++;
                    // }
                }
            }
            // dd($record);

            if ($custom['status'] && ($custom['tablemaster'] == 'purchaserequest' || 
                $custom['tablemaster'] == 'purchaseoffer' ||
                $custom['tablemaster'] == 'purchaseorder')) {
                $othertd = '';
                if ($custom['tablemaster'] == 'purchaserequest') {
                    $othertd .= '<td></td>';
                }
                $colsupplier = '';
                if (@$supplier) {
                    foreach ($supplier as $k2 => $v2) {
                        $colsupplier .= '<td style="background: #06e524;"></td>';
                    }
                }

                $qtyrowempty = $qtyrow-count($resultdetail);
                for ($i=1;$i<=$qtyrowempty;$i++) {
                    if ($i == $qtyrowempty) {
                        if ($custom['tablemaster'] == 'purchaserequest') {
                            $totalpricestitle = '';
                            $totalpricesvalue = '';
                        } else if ($custom['tablemaster'] == 'purchaseoffer') {
                            $totalpricestitle = 'Total Prices';
                            $totalpricesvalue = 'Rp. '.number_format($totalprices);
                        } else if ($custom['tablemaster'] == 'purchaseorder') {
                            $totalpricestitle = 'Total Prices';
                            $totalpricesvalue = 'Rp. '.number_format($totalprices);
                        }
                    } else {
                        $totalpricestitle = '';
                        $totalpricesvalue = '';
                    }
                    $record .= '<tr><td>&nbsp;</td><th style="text-align: right">'.$totalpricestitle.'</th><td></td><td></td>'.$othertd.$colsupplier.'<th style="text-align: right">'.$totalpricesvalue.'</th></tr>';
                }
            }
            // dd($resultdetail);
            // dd($qtyrowempty);
            // if (count($dataqtydatadetail) == $k+1) {
            //     dd($qtylastrecord);
            // }
            // die;

            $nextprint = true;
            if ($k == 0) {
                if ($rmrd['showfdfp'] || $rmrd['showothdfp'] || $rmrd['showldfp'] || $rmrd['showodfp'] || $rmrd['showedfp']) {
                    
                } else {
                    $content .= '<div style="height: '.$rmrd['heightfd'].'px"></div>';
                    $nextprint = false;
                }
                // if (!$rmrd['showfd'] && !$rmrd['showod']) {
                //     $content .= '<div style="height: '.$rmrd['heightfd'].'px"></div>';
                //     $nextprint = false;
                // }
            } else if ($k > 0 && $k < count($dataqtydatadetail)-1) {
                if ($rmrd['showfdep'] || $rmrd['showothdep'] || $rmrd['showldep']) {
                } else if ($rmrd['showodep'] && $rmrd['showedep']) {
                } else if (!$rmrd['showodep'] && $rmrd['showedep']) {
                    if ($k%2 == 0) {
                        $content .= '<div style="height: '.$rmrd['heighted'].'px"></div>';
                        $nextprint = false;
                    }
                } else if ($rmrd['showodep'] && !$rmrd['showedep']) {
                    if ($k%2 == 1) {
                        $content .= '<div style="height: '.$rmrd['heighted'].'px"></div>';
                        $nextprint = false;
                    }
                } else if (!$rmrd['showodep'] && !$rmrd['showedep']) {
                    $content .= '<div style="height: '.$rmrd['heighted'].'px"></div>';
                    $nextprint = false;
                } else {
                    if ($k%2 == 0) {
                        if (!$rmrd['showed'] && !$rmrd['showothd']) {
                            $content .= '<div style="height: '.$rmrd['heighted'].'px"></div>';
                            $nextprint = false;
                        } else {
                            // $rmrd['sdpto'] += $rmrd['_sdpto'];
                        }
                    } else {
                        if (!$rmrd['showod'] && !$rmrd['showothd']) {
                            $content .= '<div style="height: '.$rmrd['heightod'].'px"></div>';
                            $nextprint = false;
                        } else {
                            // $rmrd['sdpte'] += $rmrd['_sdpte'];
                        }
                    }
                }
            } else if (count($dataqtydatadetail) == $k+1) {
                if ($rmrd['showfdlp'] || $rmrd['showothdlp'] || $rmrd['showldlp'] || $rmrd['showodlp'] || $rmrd['showedlp']) {
                    
                } else {
                    $content .= '<div style="height: '.$rmrd['heightfd'].'px"></div>';
                    $nextprint = false;
                }
                // if ($k+1%2 == 0) {
                //     if (!$rmrd['showld'] && !$rmrd['showod']) {
                //         $content .= '<div style="height: '.$heightdetail.'px"></div>';
                //         $nextprint = false;
                //     }
                // } else {
                //     if (!$rmrd['showld'] && !$rmrd['showed']) {
                //         $content .= '<div style="height: '.$heightdetail.'px"></div>';
                //         $nextprint = false;
                //     }
                // }
            }
            // if ($k == 1) {
                // dd(count($dataqtydatadetail));
            // }
            // dd($nextprint);
            
            if ($nextprint) {
                if ($isappend) {
                    if ($k == 0) {
                        $content .= '<div style="height: '.$rmrd['heightfd'].'px">'.$templatedetail['First Detail']->phpsource.'</div>';
                    } else {
                        if (count($dataqtydatadetail) == $k+1) {
                            if (count($dataqtydatadetail)%2 == 0) {
                                if ($isenterrow) {
                                    if ($rmrd['showeh'] && $rmrd['showef']) {
                                        $hdlast = $heightdetail;
                                    } else if (!$rmrd['showeh'] && $rmrd['showef']) {
                                        $hdlast = $heightdetail;
                                    } else if ($rmrd['showeh'] && !$rmrd['showef']) {
                                        $hdlast = $heightdetail;
                                    } else if (!$rmrd['showeh'] && !$rmrd['showef']) {
                                        $hdlast = $heightdetail;
                                    }
                                } else {
                                    if ($rmrd['showoh'] && $rmrd['showof']) {
                                        $hdlast = $heightdetail;
                                    } else if (!$rmrd['showoh'] && $rmrd['showof']) {
                                        $hdlast = $heightdetail;
                                    } else if ($rmrd['showoh'] && !$rmrd['showof']) {
                                        $hdlast = $heightdetail;
                                    } else if (!$rmrd['showoh'] && !$rmrd['showof']) {
                                        $hdlast = $heightdetail;
                                    }
                                }
                                // dd($isenterrow);
                            } else {
                                if ($isenterrow) {
                                    if ($rmrd['showoh'] && $rmrd['showof']) {
                                        $hdlast = $heightdetail;
                                    } else if (!$rmrd['showoh'] && $rmrd['showof']) {
                                        $hdlast = $heightdetail;
                                    } else if ($rmrd['showoh'] && !$rmrd['showof']) {
                                        $hdlast = $heightdetail;
                                    } else if (!$rmrd['showoh'] && !$rmrd['showof']) {
                                        $hdlast = $heightdetail;
                                    }
                                } else {
                                    if ($rmrd['showeh'] && $rmrd['showef']) {
                                        $hdlast = $heightdetail;
                                    } else if (!$rmrd['showeh'] && $rmrd['showef']) {
                                        $hdlast = $heightdetail;
                                    } else if ($rmrd['showeh'] && !$rmrd['showef']) {
                                        $hdlast = $heightdetail;
                                    } else if (!$rmrd['showeh'] && !$rmrd['showef']) {
                                        $hdlast = $heightdetail;
                                    }
                                }
                                // dd($hdlast);
                            }
                            // dd($hdlast);
                            // $hdlast -= $hdlast;
                            $hdlast = $heightdetail+7;
                            $content .= '<div style="height: '.$rmrd['heightld'].'px">'.$templatedetail['Last Detail']->phpsource.'</div>';
                        } else {
                            if ($rmrd['showfhep'] || $rmrd['showothhep'] || $rmrd['showlhep']) {
                                if ($k%2 == 0) {
                                    $content .= '<div style="height: '.$rmrd['heightod'].'px">'.$templatedetail['Other Detail']->phpsource.'</div>';
                                } else {
                                    $content .= '<div style="height: '.$rmrd['heighted'].'px">'.$templatedetail['Other Detail']->phpsource.'</div>';
                                }
                            } else if ($rmrd['showohep'] && $rmrd['showehep']) {
                                if ($k%2 == 0) {
                                    $content .= '<div style="height: '.$rmrd['heightod'].'px">'.$templatedetail['Odd Detail']->phpsource.'</div>';
                                } else {
                                    $content .= '<div style="height: '.$rmrd['heighted'].'px">'.$templatedetail['Even Detail']->phpsource.'</div>';
                                }
                            } else if (!$rmrd['showohep'] && $rmrd['showehep']) {
                                if ($k%2 == 0) {
                                    $content .= '<div style="height: '.$rmrd['heightod'].'px">'.$templatedetail['Odd Detail']->phpsource.'</div>';
                                } else {
                                    $content .= '<div style="height: '.$rmrd['heighted'].'px">'.$templatedetail['Even Detail']->phpsource.'</div>';
                                }
                            } else if ($rmrd['showohep'] && !$rmrd['showehep']) {
                                if ($k%2 == 0) {
                                    // dd($rmrd['heighted']);
                                    $content .= '<div style="height: '.$rmrd['heighted'].'px">'.$templatedetail['Even Detail']->phpsource.'</div>';
                                } else {
                                    $content .= '<div style="height: '.$rmrd['heightod'].'px">'.$templatedetail['Odd Detail']->phpsource.'</div>';
                                }
                            // } else if ($rmrd['showehep']) {
                            //     $content .= '<div style="height: '.$rmrd['heighted'].'px">'.$template->reporttemplate.'</div>';
                            } else {
                                if ($k%2 == 0) {
                                    if (!$rmrd['showoh'] && !$rmrd['showothh']) {
                                        $rmrd['heightod'] += $rmrd['_heightoh']+16;
                                    }
                                    $content .= '<div style="height: '.$rmrd['heightod'].'px">'.$templatedetail['Other Detail']->phpsource.'</div>';
                                } else {
                                    if (!$rmrd['showeh'] && !$rmrd['showothh']) {
                                        $rmrd['heighted'] += $rmrd['_heighteh']+16;
                                    }
                                    $content .= '<div style="height: '.$rmrd['heighted'].'px">'.$templatedetail['Other Detail']->phpsource.'</div>';
                                }
                            }
                        }
                    }
                    $total += $subtotal;
                    $content = str_replace('$datadetail', $record, $content);
                    $content = str_replace('$subtotal_jumlah_datadetail', 'Rp. '.number_format($subtotal), $content);
                    
                    if ($custom['tablemaster'] == 'purchaseinvoice') {
                        // $accmpajak = DB::connection('mysql2')
                        //     ->table('accmpajak')
                        //     ->where('noid', $resultmaster->idaccmpajak)
                        //     ->first();

                        // $sumtax = (int)$total*$accmpajak->prosentax/100;
                        $generaltotal = (int)$total+@$sumtax;
                        
                        // $content = str_replace('$tax', '<tr>
                        //     <td colspan="3" class="giro-cek">
                        //     </td>
                        //     <td class="text-right"><b>'.$accmpajak->nama.'</b></td>
                        //     <td colspan="1" class="text-right">'.'Rp. '.number_format($sumtax).'</td>
                        // </tr>', $content);

                        $content = str_replace('$total_jumlah_datadetail', 'Rp. '.number_format($generaltotal), $content);
                    }
                }
            }

            if ($k == 0) {
                // dd($dataqtydatadetail);
                if (count($dataqtydatadetail) == 1) {
                    if ($rmrd['showfffp'] || $rmrd['showothffp'] || $rmrd['showlffp'] || $rmrd['showoffp'] || $rmrd['showeffp']) {
                        $content .= @'<div style="height: '.$rmrd['heightff'].'">'.$templatedetail['First Footer']->phpsource.'</div>';
                    }
                } else {
                    if ($rmrd['showfffp'] || $rmrd['showothffp'] || $rmrd['showlffp'] || $rmrd['showoffp'] || $rmrd['showeffp']) {
                        $content .= @'<div style="height: '.$rmrd['heightff'].'">'.$templatedetail['First Footer']->phpsource.'</div>';
                    }
                    // if ($rmrd['showff']) {
                    //     $content .= @'<div style="height: '.$rmrd['heightff'].'">'.$template->reportfooter.'</div>';
                    // } else {
                    //     if ($rmrd['showof']) {
                    //         $content .= @'<div style="height: '.$rmrd['heightff'].'">'.$template->reportfooter.'</div>';
                    //     }
                    // }
                    $content .= '<div class="page-break"></div>';
                }
                // dd($rmrd['showff']);
            } else if ($k > 0 && $k < count($dataqtydatadetail)-1) {
                if ($rmrd['showffep'] || $rmrd['showothfep'] || $rmrd['showlfep']) {
                    $content .= @'<div style="height: '.$rmrd['heightff'].'">'.$templatedetail['Other Footer']->phpsource.'</div>';
                } else if ($rmrd['showofep'] && $rmrd['showefep']) {
                    if ($k%2 == 0) {
                        $content .= '<div style="height: '.$rmrd['heightof'].'">'.$templatedetail['Odd Footer']->phpsource.'</div>';
                    } else {
                        $content .= '<div style="height: '.$rmrd['heightef'].'">'.$templatedetail['Even Footer']->phpsource.'</div>';
                    }
                } else if (!$rmrd['showofep'] && $rmrd['showefep']) {
                    if ($k%2 == 1) {
                        $content .= '<div style="height: '.$rmrd['heightef'].'">'.$templatedetail['Even Footer']->phpsource.'</div>';   
                    }
                } else if ($rmrd['showofep'] && !$rmrd['showefep']) {
                    if ($k%2 == 0) {
                        $content .= '<div style="height: '.$rmrd['heightof'].'">'.$templatedetail['Odd Footer']->phpsource.'</div>';   
                    }
                } else if ($rmrd['showefep']) {
                    if ($k%2 == 1) {
                        $content .= '<div style="height: '.$rmrd['heightof'].'">'.$templatedetail['Even Footer']->phpsource.'</div>';
                    }
                }
                $content .= '<div class="page-break"></div>';
            } else if ($k == count($dataqtydatadetail)-1) {
                // if ($rmrd['showlf']) {
                //     $content .= @'<div style="height: '.$rmrd['heightlf'].'">'.$template->reportfooter.'</div>';
                // }
                // dd($rmrd['showlflp']);
                if ($rmrd['showfflp'] || $rmrd['showothflp'] || $rmrd['showlflp'] || $rmrd['showoflp'] || $rmrd['showeflp']) {
                    $content .= @'<div style="height: '.$rmrd['heightff'].'">'.$templatedetail['Last Footer']->phpsource.'</div>';
                }
            }
            // dd($content);
            
            if (count($dataqtydatadetail) == $k+1) {
                if ($rmrd['showlf']) {
                    // if ($qtylastrecord == 2) {
                    //     $rmrd['marginlf'] = '62px 0px 0px 0px';
                    // } else if ($qtylastrecord == 3) {
                    //     $rmrd['marginlf'] = '62px 0px 0px 0px';
                    // } else if ($qtylastrecord == 4) {
                        // if ($k%2 == 1) {
                            $qlr = 0;
                            for ($i=count($qtylastrecord)-1; $i>=0; $i--) {
                                if ($qtylastrecord[$i] != 0) {
                                    $qlr = $qtylastrecord[$i];
                                    break;
                                }
                            }
                            
                            $mtlf = 0;
                            $rmrd['marginlf'] = $mtlf.'px 0px 0px 0px';
                        // }
                    // }
                    // echo $qlr.'<br>';die;
                    if ($rmrd['showlf']) {
                        // $content .= '<div style="margin: '.$rmrd['marginlf'].'; height: '.$rmrd['heightlf'].'px">'.$template->reportfooter.'</div>';
                    }
                } else {
                    // $content .= '<div style="margin: '.$rmrd['marginlf'].'; height: '.$rmrd['heightlf'].'px"></div>';
                }
            } else if (count($dataqtydatadetail) == $k+2 && $isenterrow) {
                // $content .= '<div class="page-break"></div>';
                // $content .= @$rmrd['oh'];
            } else {
                // if ($isappend) {
                    if ($k == 0) {
                        // if ($rmrd['showef']) {
                            // $content .= @$rmrd['ef0'];
                        // }
                        // $content .= '<div class="page-break"></div>';
                        // if ($rmrd['showfh']) {
                            // $content .= @$rmrd['oh'];   
                        // }
                    } else {
                        // dd($rmrd);
                        if ($k%2 == 0) {
                            // $content .= @$rmrd['ef'];
                            // $content .= '<div class="page-break"></div>';
                            if (count($dataqtydatadetail) > $k+2) {
                                // $content .= @$rmrd['oh'];
                            } else {
                                // if ($rmrd['showof']) {
                                    // $content .= @$rmrd['oh'];
                                // } else {
                                //     $content .= '<div>&nbsp;</div>';
                                // }
                            }
                        } else {
                            // $content .= @$rmrd['of'];
                            // $content .= '<div class="page-break"></div>';
                            if (count($dataqtydatadetail) > $k+2) {
                                // $content .= @$rmrd['eh'];
                            } else {
                                // if ($rmrd['showef']) {
                                    // $content .= @$rmrd['eh'];
                                // } else {
                                //     $content .= '<div>&nbsp;</div>';
                                // }
                            }
                        }
                    }
                // }
            }
        }

        // TTD
        if ($custom['status'] && 
            (
                $custom['tablemaster'] == 'purchaserequest' || 
                $custom['tablemaster'] == 'purchaseoffer' || 
                $custom['tablemaster'] == 'purchaseorder' || 
                $custom['tablemaster'] == 'purchaseinvoice'|| 
                $custom['tablemaster'] == 'fincashbankadvance'
            )
        ) {
            $_colspandescription = 0;
            $_ttd1 = '';
            $_ttd2 = '';
            $_ttd3 = '';
            $_ttd4 = '';
            $_nama1 = '-';
            $_nama2 = '-';
            $_nama3 = '-';
            $_nama4 = '-';
            $_randomttd = rand(1,6);
            if ($custom['tablemaster'] == 'purchaserequest') {
                $_colspandescription = count($supplier)+count($jsonrules->datarec);
                if ($resultmaster->idstatustranc >= 5011) {
                    $_ttd1 = '<img src="filemanager/original/'.$resultmaster->mc2_kode.'/'.$resultmaster->mc2_kode.'-ttd-'.$_randomttd.'.png" height="100px">';
                    $_nama1 = $resultmaster->mc2_nama;
                }
                if ($resultmaster->idstatustranc >= 5013) {
                    $_ttd2 = '<img src="filemanager/original/'.$resultmaster->mc3_kode.'/'.$resultmaster->mc3_kode.'-ttd-'.$_randomttd.'.png" height="100px">';
                    $_nama2 = $resultmaster->mc3_nama;
                }
                if ($resultmaster->idstatustranc >= 5014) {
                    $_ttd3 = '<img src="filemanager/original/'.$resultmaster->mc4_kode.'/'.$resultmaster->mc4_kode.'-ttd-'.$_randomttd.'.png" height="100px">';
                    $_nama3 = $resultmaster->mc4_nama;
                }

                $content = str_replace('$qtydatasupplier', count($supplier), $content);
                $content = str_replace('$datasupplier', $datasupplier, $content);
                $content = str_replace('$ttd1', $_ttd1, $content);
                $content = str_replace('$ttd2', $_ttd2, $content);
                $content = str_replace('$ttd3', $_ttd3, $content);
                $content = str_replace('$ttd4', $_ttd4, $content);
                $content = str_replace('$preparedby', $_nama1, $content);
                $content = str_replace('$reviewedby', $_nama2, $content);
                $content = str_replace('$approvedby', $_nama3, $content);
                $content = str_replace('$completedby', $_nama4, $content);

                $content = str_replace('$requestedby', $resultmaster->mc2_nama, $content);
                $content = str_replace('$department', ucfirst(strtolower($resultmaster->mtcc_nama)).' / '.$resultmaster->so_projectname.' / '.$resultmaster->so_projectlocation, $content);
                $content = str_replace('$date', date('F d, Y', strtotime($resultmaster->dostatus)), $content);
                $content = str_replace('$colspandescription', $_colspandescription, $content);
            } else if ($custom['tablemaster'] == 'purchaseoffer') {
                $_colspandescription = count($supplier)+count($jsonrules->datarec)-1;
                $_ttd1 = '<img src="filemanager/original/'.$resultmaster->mc2_kode.'/'.$resultmaster->mc2_kode.'-ttd-'.$_randomttd.'.png" height="50px">';
                $_nama1 = $resultmaster->mc2_nama;
                $_ttd2 = '<img src="filemanager/original/'.$resultmaster->mc3_kode.'/'.$resultmaster->mc3_kode.'-ttd-'.$_randomttd.'.png" height="50px">';
                $_nama2 = $resultmaster->mc3_nama;
                $_ttd3 = '<img src="filemanager/original/'.$resultmaster->mc4_kode.'/'.$resultmaster->mc4_kode.'-ttd-'.$_randomttd.'.png" height="50px">';
                $_nama3 = $resultmaster->mc4_nama;

                $content = str_replace('$qtydatasupplier', count($supplier), $content);
                $content = str_replace('$datasupplier', $datasupplier, $content);
                $content = str_replace('$ttd1', $_ttd1, $content);
                $content = str_replace('$ttd2', $_ttd2, $content);
                $content = str_replace('$ttd3', $_ttd3, $content);
                $content = str_replace('$ttd4', $_ttd4, $content);
                $content = str_replace('$preparedby', $_nama1, $content);
                $content = str_replace('$reviewedby', $_nama2, $content);
                $content = str_replace('$approvedby', $_nama3, $content);
                $content = str_replace('$completedby', $_nama4, $content);

                $content = str_replace('$requestedby', $resultmaster->mc2_nama, $content);
                $content = str_replace('$department', ucfirst(strtolower($resultmaster->mtcc_nama)).' / '.$resultmaster->so_projectname.' / '.$resultmaster->so_projectlocation, $content);
                $content = str_replace('$date', date('F d, Y', strtotime($resultmaster->dostatus)), $content);
                $content = str_replace('$colspandescription', $_colspandescription, $content);
            } else if ($custom['tablemaster'] == 'purchaseorder') {
                if ($resultmaster->idstatustranc >= 5031) {
                    $_ttd4 = '<img src="filemanager/original/'.$resultpurchasing->kode.'/'.$resultpurchasing->kode.'-ttd-'.$_randomttd.'.png" height="50px">';
                    $_nama4 = $resultpurchasing->nama;
                }
                if ($resultmaster->idstatustranc >= 5033) {
                    $_ttd2 = '<img src="filemanager/original/'.$resultmaster->mc3_kode.'/'.$resultmaster->mc3_kode.'-ttd-'.$_randomttd.'.png" height="50px">';
                    $_nama2 = $resultmaster->mc3_nama;
                }
                if ($resultmaster->idstatustranc >= 5034) {
                    $_ttd3 = '<img src="filemanager/original/'.$resultmaster->mc4_kode.'/'.$resultmaster->mc4_kode.'-ttd-'.$_randomttd.'.png" height="50px">';
                    $_nama3 = $resultmaster->mc4_nama;
                }

                $content = str_replace('$ttd4', $_ttd4, $content);
                $content = str_replace('$ttd2', $_ttd2, $content);
                $content = str_replace('$ttd3', $_ttd3, $content);
                $content = str_replace('$purchasingname', $_nama4, $content);
                $content = str_replace('$koorname', $_nama2, $content);
                $content = str_replace('$managername', $_nama3, $content);

                $content = str_replace('$requestedby', $resultmaster->mc2_nama, $content);
                $content = str_replace('$department', ucfirst(strtolower($resultmaster->mtcc_nama)).' / '.$resultmaster->so_projectname.' / '.$resultmaster->so_projectlocation, $content);
                $content = str_replace('$date', date('F d, Y', strtotime($resultmaster->dostatus)), $content);
                $content = str_replace('$colspandescription', $_colspandescription, $content);
            } else if ($custom['tablemaster'] == 'purchaseinvoice') {
                if ($resultmaster->idstatustranc >= 5051) {
                    $_ttd4 = '<img src="filemanager/original/'.$resultpurchasing->kode.'/'.$resultpurchasing->kode.'-ttd-'.$_randomttd.'.png" height="50px">';
                    $_nama4 = $resultpurchasing->nama;
                }

                if ($resultmaster->kode == null) {
                    if ($resultmaster->idstatustranc >= 5053) {
                        $_ttd2 = '<img src="filemanager/original/'.$resultfinance->kode.'/'.$resultfinance->kode.'-ttd-'.$_randomttd.'.png" height="50px">';
                        $_nama2 = $resultfinance->nama;
                    }
                    if ($resultmaster->idstatustranc >= 5054) {
                        $_ttd3 = '<img src="filemanager/original/'.$resultfinance->kode.'/'.$resultfinance->kode.'-ttd-'.$_randomttd.'.png" height="50px">';
                        $_nama3 = $resultfinance->nama;
                    }
                } else {
                    if ($resultmaster->idstatustranc >= 5053) {
                        $_ttd2 = '<img src="filemanager/original/'.$resultmaster->mc3_kode.'/'.$resultmaster->mc3_kode.'-ttd-'.$_randomttd.'.png" height="50px">';
                        $_nama2 = $resultmaster->mc3_nama;
                    }
                    if ($resultmaster->idstatustranc >= 5054) {
                        $_ttd3 = '<img src="filemanager/original/'.$resultmaster->mc4_kode.'/'.$resultmaster->mc4_kode.'-ttd-'.$_randomttd.'.png" height="50px">';
                        $_nama3 = $resultmaster->mc4_nama;
                    }
                }

                $content = str_replace('$ttd4', $_ttd4, $content);
                $content = str_replace('$ttd2', $_ttd2, $content);
                $content = str_replace('$ttd3', $_ttd3, $content);
                $content = str_replace('$purchasingname', $_nama4, $content);
                $content = str_replace('$koorname', $_nama2, $content);
                $content = str_replace('$managername', $_nama3, $content);

                $content = str_replace('$requestedby', $resultmaster->mc2_nama, $content);
                $content = str_replace('$department', ucfirst(strtolower($resultmaster->mtcc_nama)).' / '.$resultmaster->so_projectname.' / '.$resultmaster->so_projectlocation, $content);
                $content = str_replace('$date', date('F d, Y', strtotime($resultmaster->dostatus)), $content);
                $content = str_replace('$colspandescription', $_colspandescription, $content);
            } else if ($custom['tablemaster'] == 'fincashbankadvance') {
                if ($resultmaster->idstatustranc >= 2541) {
                    $_ttd1 = '<img src="filemanager/original/'.$resultmaster->mc2_kode.'/'.$resultmaster->mc2_kode.'-ttd-'.$_randomttd.'.png" height="50px">';
                    $_nama1 = $resultmaster->mc2_nama;
                }
                if ($resultmaster->idstatustranc >= 2542) {
                    $_ttd2 = '<img src="filemanager/original/'.$resultdirektur->kode.'/'.$resultdirektur->kode.'-ttd-'.$_randomttd.'.png" height="50px">';
                    $_nama2 = $resultdirektur->nama;
                }
                if ($resultmaster->idstatustranc >= 2543) {
                    $_ttd3 = '<img src="filemanager/original/'.$resultfinance->kode.'/'.$resultfinance->kode.'-ttd-'.$_randomttd.'.png" height="50px">';
                    $_nama3 = $resultfinance->nama;
                }
                if ($resultmaster->idstatustranc >= 2544) {
                    $_ttd4 = '<img src="filemanager/original/'.$resultmaster->mc4_kode.'/'.$resultmaster->mc4_kode.'-ttd-'.$_randomttd.'.png" height="50px">';
                    $_nama4 = $resultmaster->mc4_nama;
                }

                $content = str_replace('$ttd1', $_ttd1, $content);
                $content = str_replace('$ttd2', $_ttd2, $content);
                $content = str_replace('$ttd3', $_ttd3, $content);
                $content = str_replace('$ttd4', $_ttd4, $content);
                $content = str_replace('$name1', $_nama1, $content);
                $content = str_replace('$name2', $_nama2, $content);
                $content = str_replace('$name3', $_nama3, $content);
                $content = str_replace('$name4', $_nama4, $content);
            }
            // dd($resultmaster->idstatustranc);

            if ($isttdpurcahasing) {
                $_ttd4 = '<img src="filemanager/original/'.$resultpurchasing->kode.'/'.$resultpurchasing->kode.'-ttd-'.$_randomttd.'.png" height="50px">';
                $_nama4 = $resultpurchasing->nama;
            }
        }

        $html = str_replace('$content', $content, $html);

        if ($custom['tablemaster'] == 'purchaserequest' || $custom['tablemaster'] == 'purchaseoffer') {
            $html = str_replace('$logo', '<img src="assets/lycon.jpeg" alt="" class="img-logo">', $html);
            $html = str_replace('$genmcompany_nama', $resultheader->genmcompany_nama, $html);
            $html = str_replace('$today', date('d M Y'), $html);
            $html = str_replace('$jobno', $resultmaster->so_kode, $html);
            $html = str_replace('$prno', $resultheader->$tablemaster_kode, $html);
            $html = str_replace('$revdate', $resultheader->$tablemaster_revno.'/'.date('d M Y'), $html);
            $html = str_replace('$deliverytime', date('F d, Y', strtotime($resultmaster->tanggaldue)), $html);
            $html = str_replace('$psbib', date('F d, Y', strtotime($resultmaster->tanggaldue)), $html);
            $html = str_replace('$notes', $resultmaster->keterangan, $html);
            $html = str_replace('$shipto', $resultmaster->gmg_nama, $html);
            $html = str_replace('$deliverypoint', $resultmaster->deliverypoint, $html);
            $html = str_replace('$preparedby', $resultmaster->mc2_nama, $html);
        } else if ($custom['tablemaster'] == 'purchaseorder') {
            $pr = DB::connection('mysql2')
                ->table('purchaseoffer')
                ->where('kode', $resultmaster->kodereff)
                ->first();

            $html = str_replace('$logo', '<img src="assets/lycon.jpeg" alt="" style="height: 50px">', $html);
            $html = str_replace('$pono', $resultheader->$tablemaster_kode, $html);
            $html = str_replace('$today', date('d M Y'), $html);
            $html = str_replace('$jobno', $resultmaster->so_kode, $html);
            $html = str_replace('$prno', $pr->kodereff, $html);
            $html = str_replace('$termofpayment', $resultmaster->termofpayment, $html);
            $html = str_replace('$deliverytime', $resultmaster->deliverytime, $html);
            $html = str_replace('$deliveryaddress', $resultmaster->gmg_nama, $html);
            $html = str_replace('$idcardsupplier_nama', $resultmaster->idcardsupplier_nama, $html);
            $html = str_replace('$idcardsupplier_contactperson', $resultmaster->idcardsupplier_contactperson, $html);
            // $html = str_replace('$idcardsupplier_alamat', $resultmaster->idcardsupplier_alamat, $html);
            $html = str_replace('$idcardsupplier_alamatjl', $resultmaster->idcardsupplier_alamatjl, $html);
            $html = str_replace('$idlokasikota_nama', $resultmaster->idlokasikota_nama, $html);
            $html = str_replace('$idcardsupplier_nomobile', $resultmaster->idcardsupplier_nomobile, $html);
            $html = str_replace('$idcardsupplier_nophone', (is_null($resultmaster->idcardsupplier_nophone) ? '' : ', ').$resultmaster->idcardsupplier_nophone, $html);
        } else if ($custom['tablemaster'] == 'purchaseinvoice') {
            $html = str_replace('$logo', '<img src="assets/lycon.jpeg" alt="" style="height: 50px;">', $html);
            $html = str_replace('$genmcompany_nama', $resultheader->genmcompany_nama, $html);
            $html = str_replace('$pono', $resultheader->$tablemaster_kode, $html);
            $html = str_replace('$noso', $resultmaster->so_kode, $html);
            $html = str_replace('$nopo', $resultmaster->kodereff, $html);
            $html = str_replace('$noinvoice', $resultmaster->paymentnumber, $html);
            $html = str_replace('$today', date('d M Y'), $html);
            $html = str_replace('$jatuhtempo', MyHelper::dbdateTohumandate($resultmaster->tanggaldue), $html);
            $html = str_replace('$jobno', $resultmaster->so_kode, $html);
            $html = str_replace('$prno', $resultmaster->kode, $html);
            $html = str_replace('$keterangan', $resultmaster->keterangan, $html);
            $html = str_replace('$nopo', $resultmaster->kodereff, $html);
            $html = str_replace('$noinvoice', $resultmaster->kode, $html);
            $html = str_replace('$kodeacc', '-', $html);            
        } else if ($custom['tablemaster'] == 'fincashbankadvance') {
            $html = str_replace('$logo', '<img src="assets/lycon.jpeg" alt="" style="height: 50px;">', $html);
            $html = str_replace('$genmcompany_nama', $resultheader->genmcompany_nama, $html);
            $html = str_replace('$date', date('d M Y'), $html);
            $html = str_replace('$revdate', date('d M Y'), $html);
            $html = str_replace('$prno', $resultmaster->kode, $html);
            $html = str_replace('$nama', $resultmaster->mc2_nama, $html);
            $html = str_replace('$departemen', $resultmaster->gmd_nama, $html);
            $html = str_replace('$jumlahpengajuan', 'Rp. '.number_format($resultmaster->totalsubmission), $html);

            if (!is_null($resultmaster->idso)) {
                $number = $resultmaster->so_kode;
            } else if (!is_null($resultmaster->nosov)) {
                $number = $resultmaster->nosov;
            } else if (!is_null($resultmaster->noppb)) {
                $number = $resultmaster->noppb;
            }

            $html = str_replace('$number', $number, $html);
            $html = str_replace('$keperluan', $resultmaster->necessity, $html);
            $html = str_replace('$jabatan', $resultmaster->position, $html);
            $html = str_replace('$jumlahkembali', 'Rp. '.number_format($resultmaster->totalchangeover), $html);
            $html = str_replace('$tanggalpenyelesaian', date('F d, Y', strtotime($resultmaster->completiondate)), $html);
        }

        $pdf = PDF::loadHTML($html);
        $pdf->setPaper($template->defaultpaper, $ispotrait);
        // $pdf->setPaper('A4', 'potrait');
        // print_r($html);die;
        return $pdf->stream('users.pdf');
    }

    public function print_6_jan_2021($noid, $urlprint) {
        $jsonrules = [
            'noidrptmreport' => 25101,
            'tabledetail' => 'fincashbankdetail',
            'showdatapertable' => 10,
            'content1width' => [20,310,80,80,50,100],
            'charketerangan' => 60,
            'headermiddleml' => 100,
            'headerlastml' => 470,
            'content2width' => [145,145,145,145,78],
            'heightdetail' => 300
        ];

        $jsonrules = json_encode($jsonrules);
        $jsonrules = json_decode($jsonrules);

        $noidrptmreport = DB::table('cmswidgetgrid')->select('idreportdaftar')->where('noid', $noid.'01')->first()->idreportdaftar;
        // $noidrptmreport = $jsonrules->noidrptmreport;                            //noid rptmreport
        $tabledetail = $jsonrules->tabledetail;                 //table detail
        $urlprint = explode('/', base64_decode($urlprint));
        $template = DB::connection('mysql2')->table('rptmreport')->where('noid', $noidrptmreport)->first();
        
        if ($template->defaultpaper == 'A4') {
            if ($template->ispotrait) {
                $ispotrait = 'potrait';
                $showdatapertable = 38;                     //show data pertable default
                $sdpt0 = 38;                                //show data pertable pertama
                $sdptoh = 38;                               //show data pertable odd header
                $sdpteh = 38;                               //show data pertable even header
                $sdptof = 38;                               //show data pertable odd footer
                $sdptef = 38;                               //show data pertable even footer
                $content1width = [20,298,80,80,70,100];     //width column table content
                $charketerangan = 50;                       //slice character keterangan
                $headermiddleml = 93;                       //margin-left middle header
                $headerlastml = 480;                        //margin-left last header
                $content2width = [150,150,150,150,55];      //width column table footer
                $heightdetail = 875;                        //height table content
            } else {
                $ispotrait = 'landscape';
                $showdatapertable = 22;
                $sdpt0 = 22;
                $sdptoh = 22;
                $sdpteh = 22;
                $sdptof = 23;
                $sdptef = 23;
                $content1width = [20,640,80,80,50,100];
                $charketerangan = 110;
                $headermiddleml = 240;
                $headerlastml = 790;
                $content2width = [215,215,215,215,129];
                $heightdetail = 545;
            }
        } else if ($template->defaultpaper == 'A3') {
            if ($template->ispotrait) {
                $ispotrait = 'potrait';
                $showdatapertable = 60;
                $sdpt0 = 60;
                $sdptoh = 35;
                $sdpteh = 35;
                $sdptof = 62;
                $sdptef = 62;
                $content1width = [0,615,80,80,50,100];
                $charketerangan = 115;
                $headermiddleml = 230;
                $headerlastml = 760;
                $content2width = [220,220,220,220,79];
                $heightdetail = 1290;
            } else {
                $ispotrait = 'landscape';
                $showdatapertable = 35;
                $sdpt0 = 60;
                $sdptoh = 35;
                $sdpteh = 35;
                $sdptof = 39;
                $sdptef = 39;
                $content1width = [0,1080,80,80,50,100];
                $charketerangan = 210;
                $headermiddleml = 430;
                $headerlastml = 1200;
                $content2width = [330,330,330,330,104];
                $heightdetail = 830;
            }
        } else if ($template->defaultpaper == 'A5') {
            if ($template->ispotrait) {
                $ispotrait = 'potrait';
                $showdatapertable = 20;
                $sdpt0 = 20;
                $sdptoh = 20;
                $sdpteh = 20;
                $sdptof = 20;
                $sdptef = 20;
                $content1width = [10,10,80,80,50,50];
                $charketerangan = 10;
                $headermiddleml = 0;
                $headerlastml = 250;
                $content2width = [88,100,100,100,38];
                $heightdetail = 540;
            } else {
                $ispotrait = 'landscape';
                $showdatapertable = $jsonrules->showdatapertable;
                $sdpt0 = $showdatapertable;
                $sdptoh = $showdatapertable;
                $sdpteh = $showdatapertable;
                $sdptof = $showdatapertable+1;
                $sdptef = $showdatapertable+1;
                $content1width = $jsonrules->content1width;
                $charketerangan = $jsonrules->charketerangan;
                $headermiddleml = $jsonrules->headermiddleml;
                $headerlastml = $jsonrules->headerlastml;
                $content2width = $jsonrules->content2width;
                $heightdetail = $jsonrules->heightdetail;
            }
        }
        
        $tablemaster = DB::table('cmswidget')->select('maintable')->where('kode', $noid)->orderBy('noid', 'ASC')->first()->maintable;
        $resultmaster = DB::connection('mysql2')->table($tablemaster)->where('noid', $urlprint[1])->orderBy('noid', 'ASC')->first();
        $resultdetail = DB::connection('mysql2')->table($tabledetail)->where('idmaster', $resultmaster->noid)->orderBy('noid', 'ASC')->get();
        $resultrptmreportdetail = DB::connection('mysql2')->table('rptmreportdetail')->where('idreport', $noidrptmreport)->orderBy('nourut', 'ASC')->get();        
        $tablemaster_kode = $tablemaster.'_kode';
        $tablemaster_revno = $tablemaster.'_revno';
        $resultheader = DB::connection('mysql2')->select("SELECT $tablemaster.kode AS $tablemaster_kode, $tablemaster.revno AS $tablemaster_revno, genmcompany.nama AS genmcompany_nama FROM $tablemaster JOIN genmcompany ON $tablemaster.idcompany = genmcompany.noid WHERE $tablemaster.noid=$urlprint[1] LIMIT 1")[0];

        $rmrd = [];
        foreach ($resultrptmreportdetail as $k => $v) {
            if ($v->nama == 'First Header') {
                if ($v->isshowfirstpage) {
                    $rmrd['fh'] = '<div class="fh">'.$template->reportheader.'</div>';
                    $rmrd['sdpteh'] = $showdatapertable;
                    $rmrd['sdpt0'] = $sdpt0;
                    $rmrd['showfh'] = true;
                } else {
                    $rmrd['fh'] = '<div class="fh">&nbsp;</div>';
                    $rmrd['sdpteh'] = $sdpteh;
                    $rmrd['sdpt0'] = $sdpt0+4;
                    $rmrd['showfh'] = false;
                }
                $rmrd['marginfh'] = '0px 0px 0px 0px';
                $rmrd['heightfh'] = $v->widgetheight;
            } else if ($v->nama == 'Odd Header') {
                if ($v->isshoweverypage) {
                    $rmrd['oh'] = '<div class="oh">'.$template->reportheader.'</div>';
                    $rmrd['sdptoh'] = $showdatapertable;
                    $rmrd['showoh'] = true;
                } else {
                    $rmrd['oh'] = '<div class="oh">&nbsp;</div>';
                    $rmrd['sdptoh'] = $sdptoh;
                    $rmrd['showoh'] = false;
                }
                $rmrd['marginoh'] = '0px 0px 0px 0px';
                $rmrd['heightoh'] = $v->widgetheight;
            } else if ($v->nama == 'Even Header') {
                if ($v->isshoweverypage) {
                    $rmrd['eh'] = '<div class="eh">'.$template->reportheader.'</div>';
                    $rmrd['showeh'] = true;
                } else {
                    $rmrd['eh'] = '<div class="eh">&nbsp;</div>';
                    $rmrd['showeh'] = false;
                }
                $rmrd['margineh'] = '0px 0px 0px 0px';
                $rmrd['heighteh'] = $v->widgetheight;
            } else if ($v->nama == 'Detail') {
                // $rmrd['hd'] = $v->widgetheight;
                $rmrd['hd'] = $heightdetail;
            } else if ($v->nama == 'Odd Footer') {
                if ($v->isshoweverypage) {
                    $rmrd['of'] = '<div class="of">'.$template->reportfooter.'</div>';
                    $rmrd['sdptof'] = $showdatapertable;
                    $rmrd['showof'] = true;
                } else {
                    $rmrd['of'] = '<div class="of">&nbsp;</div>';
                    $rmrd['sdptof'] = $sdptof;
                    $rmrd['showof'] = false;
                }
                $rmrd['marginof'] = '0px 0px 0px 0px';
                $rmrd['heightof'] = $v->widgetheight;
            } else if ($v->nama == 'Even Footer') {
                if ($v->isshoweverypage) {
                    $rmrd['ef'] = '<div class="ef">'.$template->reportfooter.'</div>';
                    $rmrd['sdptef'] = $showdatapertable;
                    $rmrd['showef'] = true;
                    $rmrd['ef0'] = '<div class="ef0">'.$template->reportfooter.'</div>';
                    $rmrd['ef0_'] = '<div class="ef0_">'.$template->reportfooter.'</div>';
                    $rmrd['hfef0'] = $v->widgetheight;
                } else {
                    $rmrd['ef0'] = '<div class="ef0">&nbsp;</div>';
                    // $rmrd['ef0_'] = '<div class="ef0_">&nbsp;</div>';
                    $rmrd['hfef0'] = 0;
                    $rmrd['sdpt0'] += 6;
                    $rmrd['ef'] = '<div class="ef">&nbsp;</div>';
                    $rmrd['sdptef'] = $sdptef;
                    $rmrd['showef'] = false;
                }
                $rmrd['hfef0_'] = $v->widgetheight;
                // dd($rmrd['showfh'].'-'.$rmrd['showef'].'/'.$rmrd['sdpt0']);
                $rmrd['marginef'] = '0px 0px 0px 0px';
                $rmrd['heightef'] = $v->widgetheight;
            } else if ($v->nama == 'Last Footer') {
                $rmrd['isshoweverypagelf'] = $v->isshoweverypage;
                $rmrd['marginlf'] = '0px 0px 0px 0px';
                $heightlf = $v->widgetheight;
                if ($v->isshoweverypage) {
                    $rmrd['showlf'] = true;
                } else {
                    $rmrd['showlf'] = false;
                }
            }
        }
        // dd($rmrd['ef0']);

        // dd(1);
        if ($template->defaultpaper == 'A4') {
            if ($template->ispotrait) {
                if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] = $rmrd['sdpt0'];
                    $rmrd['sdptef'] += 0;
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {   //YNNNNY
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 10;
                    $rmrd['sdptof'] += 6;
                    $rmrd['hhehef'] = 9;
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 6;
                } else if (!$rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] = $sdpt0+2;
                    $rmrd['sdptef'] += 0;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail+60;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 50;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail;
                    $rmrd['hfehef'] = 50;
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = 9;
                    $rmrd['hdohof'] = $heightdetail+65;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhehef'] = 9;
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 3;
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptof'] += 3;
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = 0;
                    $rmrd['sdptef'] += 3;
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] = $rmrd['sdpt0'];
                    $rmrd['sdptef'] += 0;
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if (!$rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 1;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail+60;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 0;
                    $rmrd['hhohof'] = 9;
                    $rmrd['hdohof'] = $heightdetail+65;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if (!$rmrd['showfh'] && $rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 1;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail+60;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 3;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhehef'] = 10;
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = 50;
                    $rmrd['sdptef'] += 0;
                } else if (!$rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 1;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail+60;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptof'] += 3;
                } else if (!$rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 4;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail+40;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = 0;
                    $rmrd['sdptef'] += 3;
                } else if (!$rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 1;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail+60;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 0;
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = 9;
                    $rmrd['hdohof'] = $heightdetail+65;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhehef'] = 9;
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 3;
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = 10;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['sdptof'] += 6;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 0;
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail+65;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = 0;
                    $rmrd['sdptef'] += 3;
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = 9;
                    $rmrd['hdohof'] = $heightdetail+65;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 0;
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = 20;
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 3;
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhehef'] = 10;
                    $rmrd['hdehef'] = $heightdetail+115;
                    $rmrd['hfehef'] = 0;
                    $rmrd['sdptef'] += 6;
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhehef'] = 9;
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 3;
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 10;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = 0;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 3;
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 0;
                    $rmrd['sdptof'] += 3;
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+50;
                    $rmrd['hfehef'] = 0;
                    $rmrd['sdptef'] += 3;
                } else if (!$rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {    
                    $rmrd['sdpt0'] -= 1;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail+40;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 10;
                    $rmrd['hdohof'] = $heightdetail+65;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhehef'] = 10;
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 3;
                } else if (!$rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {    
                    $rmrd['sdpt0'] -= 1;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail+40;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 10;
                    $rmrd['hdohof'] = $heightdetail+65;
                    $rmrd['hfohof'] = 0;
                    $rmrd['sdptof'] += 6;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 0;
                } else if (!$rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {    
                    $rmrd['sdpt0'] -= 4;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail+40;
                    $rmrd['hffhef'] = 10;
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = 0;
                    $rmrd['sdptef'] += 3;
                } else if (!$rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && !$rmrd['showlf']) {    
                    $rmrd['sdpt0'] -= 2;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail+65;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = 30;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 0;
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 0;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 6;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 10;
                    $rmrd['hhehef'] = 9;
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['sdptef'] += 6;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = 9;
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 0;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = 9;
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 6;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = 0;
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 0;
                    $rmrd['sdptef'] += 0;
                    $rmrd['sdptof'] += 6;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['sdptef'] += 6;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = 20;
                    $rmrd['hdehef'] = $heightdetail+70;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 0;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = 20;
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['sdptef'] += 6;
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = 20;
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else {
                    if ($rmrd['showfh'] && $rmrd['showef']) {
                        $rmrd['hhfhef'] = $rmrd['heightfh'];
                        $rmrd['hdfhef'] = $heightdetail;
                        $rmrd['hffhef'] = $rmrd['heightef'];
                    } else if (!$rmrd['showfh'] && $rmrd['showef']) {
                        $rmrd['hhfhef'] = 0;
                        $rmrd['hdfhef'] = $heightdetail+60;
                        $rmrd['hffhef'] = $rmrd['heightef'];
                        $rmrd['sdptef'] += 3;
                    } else if ($rmrd['showfh'] && !$rmrd['showef']) {
                        $rmrd['hhfhef'] = $rmrd['heightfh'];
                        $rmrd['hdfhef'] = $heightdetail;
                        // if (!$rmrd['showef'] && !$rmrd['showof']) {
                        //     $rmrd['sdpt0'] -= 6;
                        //     $rmrd['hffhef'] = $rmrd['hfef0_'];
                        // } else {
                            $rmrd['sdpt0'] -= 3;
                            $rmrd['hffhef'] = 0;
                        // }
                    } else if (!$rmrd['showfh'] && !$rmrd['showef']) {
                        $rmrd['hhfhef'] = 0;
                        $rmrd['hdfhef'] = $heightdetail+40;
                        $rmrd['hffhef'] = 0;
                    }
                    
                    //odd
                    if ($rmrd['showoh'] && $rmrd['showof']) {
                        $rmrd['hhohof'] = $rmrd['heightoh'];
                        $rmrd['hdohof'] = $heightdetail;
                        $rmrd['hfohof'] = $rmrd['heightof'];
                        $rmrd['sdptof'] += 0;
                    } else if (!$rmrd['showoh'] && $rmrd['showof']) {
                        $rmrd['hhohof'] = 9;
                        $rmrd['hdohof'] = $heightdetail+65;
                        $rmrd['hfohof'] = $rmrd['heightof'];
                        $rmrd['sdptof'] += 3;
                    } else if ($rmrd['showoh'] && !$rmrd['showof']) {
                        $rmrd['hhohof'] = $rmrd['heightoh'];
                        $rmrd['hdohof'] = $heightdetail;
                        $rmrd['hfohof'] = 0;
                    } else if (!$rmrd['showoh'] && !$rmrd['showof']) {
                        $rmrd['hhohof'] = 24;
                        $rmrd['hdohof'] = $heightdetail;
                        $rmrd['hfohof'] = 0;
                        $rmrd['sdptof'] += 3;
                    }
            
                    if ($rmrd['showeh'] && $rmrd['showef']) {
                        $rmrd['hhehef'] = $rmrd['heighteh'];
                        $rmrd['hdehef'] = $heightdetail+7;
                        $rmrd['hfehef'] = $rmrd['heightef'];
                        $rmrd['sdptef'] += 0;
                    } else if (!$rmrd['showeh'] && $rmrd['showef']) {
                        $rmrd['hhehef'] = 9;
                        $rmrd['hdehef'] = $heightdetail+65;
                        $rmrd['hfehef'] = $rmrd['heightef'];
                        $rmrd['sdptef'] += 3;
                    } else if ($rmrd['showeh'] && !$rmrd['showef']) {
                        $rmrd['hhehef'] = $rmrd['heighteh'];
                        $rmrd['hdehef'] = $heightdetail+75;
                        $rmrd['hfehef'] = 0;
                        $rmrd['sdptef'] += 0;
                    } else if (!$rmrd['showeh'] && !$rmrd['showef']) {
                        $rmrd['hhehef'] = 24;
                        $rmrd['hdehef'] = $heightdetail+120;
                        $rmrd['hfehef'] = 0;
                        $rmrd['sdptef'] += 3;
                    }
                }
            } else {
                if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] = $rmrd['sdpt0'];
                    $rmrd['sdptef'] += 0;
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = 15;
                    $rmrd['hdohof'] = $heightdetail+65;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhehef'] = 15;
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 3;
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {   //YNNNNY
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 10;
                    $rmrd['sdptof'] += 5;
                    $rmrd['hhehef'] = 9;
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 5;
                } else {
                    if ($rmrd['showfh'] && $rmrd['showef']) {
                        $rmrd['hhfhef'] = $rmrd['heightfh'];
                        $rmrd['hdfhef'] = $heightdetail;
                        $rmrd['hffhef'] = $rmrd['heightef'];
                    } else if (!$rmrd['showfh'] && $rmrd['showef']) {
                        $rmrd['hhfhef'] = 0;
                        $rmrd['hdfhef'] = $heightdetail+60;
                        $rmrd['hffhef'] = $rmrd['heightef'];
                        $rmrd['sdptef'] += 3;
                    } else if ($rmrd['showfh'] && !$rmrd['showef']) {
                        $rmrd['hhfhef'] = $rmrd['heightfh'];
                        $rmrd['hdfhef'] = $heightdetail;
                        $rmrd['hffhef'] = 0;
                        $rmrd['sdpt0'] -= 3;
                    } else if (!$rmrd['showfh'] && !$rmrd['showef']) {
                        $rmrd['hhfhef'] = 0;
                        $rmrd['hdfhef'] = $heightdetail+40;
                        $rmrd['hffhef'] = 0;
                    }
                    
                    //odd
                    if ($rmrd['showoh'] && $rmrd['showof']) {
                        $rmrd['hhohof'] = $rmrd['heightoh'];
                        $rmrd['hdohof'] = $heightdetail;
                        $rmrd['hfohof'] = $rmrd['heightof'];
                        $rmrd['sdptof'] += 0;
                    } else if (!$rmrd['showoh'] && $rmrd['showof']) {
                        $rmrd['hhohof'] = 9;
                        $rmrd['hdohof'] = $heightdetail+65;
                        $rmrd['hfohof'] = $rmrd['heightof'];
                        $rmrd['sdptof'] += 3;
                    } else if ($rmrd['showoh'] && !$rmrd['showof']) {
                        $rmrd['hhohof'] = $rmrd['heightoh'];
                        $rmrd['hdohof'] = $heightdetail;
                        $rmrd['hfohof'] = 0;
                        $rmrd['sdptof'] += 2;
                    } else if (!$rmrd['showoh'] && !$rmrd['showof']) {
                        $rmrd['hhohof'] = 24;
                        $rmrd['hdohof'] = $heightdetail;
                        $rmrd['hfohof'] = 0;
                        $rmrd['sdptof'] += 3;
                    }
            
                    if ($rmrd['showeh'] && $rmrd['showef']) {
                        $rmrd['hhehef'] = $rmrd['heighteh'];
                        $rmrd['hdehef'] = $heightdetail+7;
                        $rmrd['hfehef'] = $rmrd['heightef'];
                        $rmrd['sdptef'] += 0;
                    } else if (!$rmrd['showeh'] && $rmrd['showef']) {
                        $rmrd['hhehef'] = 9;
                        $rmrd['hdehef'] = $heightdetail+70;
                        $rmrd['hfehef'] = $rmrd['heightef'];
                        $rmrd['sdptef'] += 3;
                    } else if ($rmrd['showeh'] && !$rmrd['showef']) {
                        $rmrd['hhehef'] = $rmrd['heighteh'];
                        $rmrd['hdehef'] = $heightdetail+75;
                        $rmrd['hfehef'] = 0;
                        $rmrd['sdptef'] += 2;
                    } else if (!$rmrd['showeh'] && !$rmrd['showef']) {
                        $rmrd['hhehef'] = 24;
                        $rmrd['hdehef'] = $heightdetail+120;
                        $rmrd['hfehef'] = 0;
                        $rmrd['sdptef'] += 3;
                    }
                }
            }
        } else if ($template->defaultpaper == 'A3') {
            if ($template->ispotrait) {
                if ($rmrd['showfh'] && $rmrd['showef']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                } else if (!$rmrd['showfh'] && $rmrd['showef']) {
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail+60;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 3;
                } else if ($rmrd['showfh'] && !$rmrd['showef']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    // if (!$rmrd['showef'] && !$rmrd['showof']) {
                    //     $rmrd['sdpt0'] -= 6;
                    //     $rmrd['hffhef'] = $rmrd['hfef0_'];
                    // } else {
                        $rmrd['sdpt0'] -= 3;
                        $rmrd['hffhef'] = 0;
                    // }
                } else if (!$rmrd['showfh'] && !$rmrd['showef']) {
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail+40;
                    $rmrd['hffhef'] = 0;
                }
                
                //odd
                if ($rmrd['showoh'] && $rmrd['showof']) {
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 0;
                } else if (!$rmrd['showoh'] && $rmrd['showof']) {
                    $rmrd['hhohof'] = 9;
                    $rmrd['hdohof'] = $heightdetail+65;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 3;
                } else if ($rmrd['showoh'] && !$rmrd['showof']) {
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                } else if (!$rmrd['showoh'] && !$rmrd['showof']) {
                    $rmrd['hhohof'] = 24;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['sdptof'] += 3;
                }
        
                if ($rmrd['showeh'] && $rmrd['showef']) {
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 0;
                } else if (!$rmrd['showeh'] && $rmrd['showef']) {
                    $rmrd['hhehef'] = 9;
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 3;
                } else if ($rmrd['showeh'] && !$rmrd['showef']) {
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+75;
                    $rmrd['hfehef'] = 0;
                    $rmrd['sdptef'] += 0;
                } else if (!$rmrd['showeh'] && !$rmrd['showef']) {
                    $rmrd['hhehef'] = 24;
                    $rmrd['hdehef'] = $heightdetail+120;
                    $rmrd['hfehef'] = 0;
                    $rmrd['sdptef'] += 3;
                }
            }
        } else if ($template->defaultpaper == 'A5') {
            if ($template->ispotrait) {
                if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] = $rmrd['sdpt0'];
                    $rmrd['sdptef'] += 0;
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 2;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 4;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 10;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+300;
                    $rmrd['hfehef'] = 0;
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail+75;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhehef'] = 20;
                    $rmrd['hdehef'] = $heightdetail+75;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                    $rmrd['sdptef'] += 3;
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {   //YNNNNY
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['sdpt0'] -= 2;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 9;
                    $rmrd['hdohof'] = $heightdetail+65;
                    $rmrd['hfohof'] = 9;
                    $rmrd['sdptof'] += 6;
                    $rmrd['hhehef'] = 9;
                    $rmrd['hdehef'] = $heightdetail+135;
                    $rmrd['hfehef'] = 9;
                    $rmrd['sdptef'] += 5;
                } else {
                    if ($rmrd['showfh'] && $rmrd['showef']) {
                        $rmrd['hhfhef'] = $rmrd['heightfh'];
                        $rmrd['hdfhef'] = $heightdetail;
                        $rmrd['hffhef'] = $rmrd['heightef'];
                    } else if (!$rmrd['showfh'] && $rmrd['showef']) {
                        $rmrd['hhfhef'] = 0;
                        $rmrd['hdfhef'] = $heightdetail+60;
                        $rmrd['hffhef'] = $rmrd['heightef'];
                        $rmrd['sdptef'] += 3;
                    } else if ($rmrd['showfh'] && !$rmrd['showef']) {
                        $rmrd['hhfhef'] = $rmrd['heightfh'];
                        $rmrd['hdfhef'] = $heightdetail;
                        // if (!$rmrd['showef'] && !$rmrd['showof']) {
                        //     $rmrd['sdpt0'] -= 6;
                        //     $rmrd['hffhef'] = $rmrd['hfef0_'];
                        // } else {
                            $rmrd['sdpt0'] -= 3;
                            $rmrd['hffhef'] = 0;
                        // }
                    } else if (!$rmrd['showfh'] && !$rmrd['showef']) {
                        $rmrd['hhfhef'] = 0;
                        $rmrd['hdfhef'] = $heightdetail+40;
                        $rmrd['hffhef'] = 0;
                    }
                    
                    //odd
                    if ($rmrd['showoh'] && $rmrd['showof']) {
                        $rmrd['hhohof'] = $rmrd['heightoh'];
                        $rmrd['hdohof'] = $heightdetail;
                        $rmrd['hfohof'] = $rmrd['heightof'];
                        $rmrd['sdptof'] += 0;
                    } else if (!$rmrd['showoh'] && $rmrd['showof']) {
                        $rmrd['hhohof'] = 9;
                        $rmrd['hdohof'] = $heightdetail+65;
                        $rmrd['hfohof'] = $rmrd['heightof'];
                        $rmrd['sdptof'] += 3;
                    } else if ($rmrd['showoh'] && !$rmrd['showof']) {
                        $rmrd['hhohof'] = $rmrd['heightoh'];
                        $rmrd['hdohof'] = $heightdetail;
                        $rmrd['hfohof'] = 0;
                    } else if (!$rmrd['showoh'] && !$rmrd['showof']) {
                        $rmrd['hhohof'] = 24;
                        $rmrd['hdohof'] = $heightdetail;
                        $rmrd['hfohof'] = 0;
                        $rmrd['sdptof'] += 3;
                    }
            
                    if ($rmrd['showeh'] && $rmrd['showef']) {
                        $rmrd['hhehef'] = $rmrd['heighteh'];
                        $rmrd['hdehef'] = $heightdetail+7;
                        $rmrd['hfehef'] = $rmrd['heightef'];
                        $rmrd['sdptef'] += 0;
                    } else if (!$rmrd['showeh'] && $rmrd['showef']) {
                        $rmrd['hhehef'] = 9;
                        $rmrd['hdehef'] = $heightdetail+65;
                        $rmrd['hfehef'] = $rmrd['heightef'];
                        $rmrd['sdptef'] += 3;
                    } else if ($rmrd['showeh'] && !$rmrd['showef']) {
                        $rmrd['hhehef'] = $rmrd['heighteh'];
                        $rmrd['hdehef'] = $heightdetail+75;
                        $rmrd['hfehef'] = 0;
                        $rmrd['sdptef'] += 0;
                    } else if (!$rmrd['showeh'] && !$rmrd['showef']) {
                        $rmrd['hhehef'] = 24;
                        $rmrd['hdehef'] = $heightdetail+120;
                        $rmrd['hfehef'] = 0;
                        $rmrd['sdptef'] += 3;
                    }
                }
            } else {
                $rmrd['_sdpt0'] = $rmrd['sdpt0'];
                $rmrd['_sdptef'] = $rmrd['sdptef'];
                $rmrd['_sdptof'] = $rmrd['sdptof'];

                if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 0;
                    $rmrd['sdptef'] += 0;
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if (!$rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 1;
                    $rmrd['sdptef'] += 0;
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 0;
                    $rmrd['sdptef'] += 0;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 0;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = 20;
                    $rmrd['hdehef'] = $heightdetail+70;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 0;
                    $rmrd['sdptef'] += 0;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 0;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+70;
                    $rmrd['hfehef'] = 0;
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 0;
                    $rmrd['sdptef'] += 0;
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if (!$rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 1;
                    $rmrd['sdptef'] += 0;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if (!$rmrd['showfh'] && $rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 1;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = 20;
                    $rmrd['hdehef'] = $heightdetail+70;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if (!$rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 1;
                    $rmrd['sdptef'] += 0;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if (!$rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if (!$rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 1;
                    $rmrd['sdptef'] += 0;
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 0;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail+75;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = 20;
                    $rmrd['hdehef'] = $heightdetail+70;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 0;
                    $rmrd['sdptef'] += 0;
                    $rmrd['sdptof'] += 6;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail+75;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 2;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = 10;
                    $rmrd['hdohof'] = $heightdetail+75;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+75;
                    $rmrd['hfehef'] = 20;
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 0;
                    $rmrd['sdptef'] += 0;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail+75;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 0;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail+75;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = 10;
                    $rmrd['hdehef'] = $heightdetail+70;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 2;
                    $rmrd['sdptef'] += 6;
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail+75;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = 20;
                    $rmrd['hdehef'] = $heightdetail+70;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 0;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail+75;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = 20;
                    $rmrd['hdehef'] = $heightdetail+70;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 2;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 10;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+65;
                    $rmrd['hfehef'] = 0;
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 0;
                    $rmrd['sdptef'] += 0;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 10;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 2;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if (!$rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 1;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = 20;
                    $rmrd['hdehef'] = $heightdetail+75;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if (!$rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 1;
                    $rmrd['sdptef'] += 0;
                    $rmrd['sdptof'] += 6;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if (!$rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 10;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef']+10;
                } else if (!$rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 1;
                    $rmrd['sdptef'] += 0;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef']+10;
                } else if (!$rmrd['showfh'] && $rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 1;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = 10;
                    $rmrd['hdehef'] = $heightdetail+75;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if (!$rmrd['showfh'] && $rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['sdptef'] += 6;
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = 20;
                    $rmrd['hdehef'] = $heightdetail+75;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if (!$rmrd['showfh'] && $rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 1;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = 20;
                    $rmrd['hdehef'] = $heightdetail+70;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if (!$rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+70;
                    $rmrd['hfehef'] = 0;
                } else if (!$rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 1;
                    $rmrd['sdptef'] += 0;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if (!$rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 0;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 6;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = 10;
                    $rmrd['hdehef'] = $heightdetail+70;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 2;
                    $rmrd['sdptef'] += 6;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 10;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = 20;
                    $rmrd['hdehef'] = $heightdetail+70;
                    $rmrd['hfehef'] = $rmrd['heightef']+10;
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 0;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = 20;
                    $rmrd['hdehef'] = $heightdetail+70;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 2;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 6;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 10;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+70;
                    $rmrd['hfehef'] = 20;
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 0;
                    $rmrd['sdptef'] += 0;
                    $rmrd['sdptof'] += 6;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = $rmrd['heightef'];
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 2;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 10;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+75;
                    $rmrd['hfehef'] = 20;
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 2;
                    $rmrd['sdptef'] += 6;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = 10;
                    $rmrd['hdehef'] = $heightdetail+155;
                    $rmrd['hfehef'] = 0;
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 0;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = 10;
                    $rmrd['hdehef'] = $heightdetail+75;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 2;
                    $rmrd['sdptef'] += 6;
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];;
                    $rmrd['hhehef'] = 20;
                    $rmrd['hdehef'] = $heightdetail+75;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 2;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+75;
                    $rmrd['hfehef'] = 0;
                } else if (!$rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 1;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 6;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = 10;
                    $rmrd['hdehef'] = $heightdetail+75;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if (!$rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['sdptef'] += 6;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 10;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = 20;
                    $rmrd['hdehef'] = $heightdetail+75;
                    $rmrd['hfehef'] = $rmrd['heightef']+10;
                } else if (!$rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 1;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = 20;
                    $rmrd['hdehef'] = $heightdetail+75;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if (!$rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 6;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 10;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+75;
                    $rmrd['hfehef'] = 20;
                } else if (!$rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 1;
                    $rmrd['sdptef'] += 0;
                    $rmrd['sdptof'] += 6;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if (!$rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 10;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+7;
                    $rmrd['hfehef'] = $rmrd['heightef']+10;
                } else if (!$rmrd['showfh'] && $rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['sdptef'] += 6;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = 10;
                    $rmrd['hdehef'] = $heightdetail+75;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if (!$rmrd['showfh'] && $rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 1;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = 10;
                    $rmrd['hdehef'] = $heightdetail+70;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if (!$rmrd['showfh'] && $rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['sdptef'] += 6;
                    $rmrd['sdptof'] += 0;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = 20;
                    $rmrd['hdehef'] = $heightdetail+70;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if (!$rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+70;
                    $rmrd['hfehef'] = 0;
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {   //YNNNNY
                    $rmrd['sdpt0'] -= 2;
                    $rmrd['sdptef'] += 5;
                    $rmrd['sdptof'] += 6;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 9;
                    $rmrd['hdohof'] = $heightdetail+65;
                    $rmrd['hfohof'] = 9;
                    $rmrd['hhehef'] = 9;
                    $rmrd['hdehef'] = $heightdetail+135;
                    $rmrd['hfehef'] = 9;
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 0;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 6;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = 10;
                    $rmrd['hdehef'] = $heightdetail+70;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 2;
                    $rmrd['sdptef'] += 6;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 10;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = 20;
                    $rmrd['hdehef'] = $heightdetail+70;
                    $rmrd['hfehef'] = $rmrd['heightef']+10;
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 2;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 6;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 10;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+70;
                    $rmrd['hfehef'] = 20;
                } else if ($rmrd['showfh'] && $rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 2;
                    $rmrd['sdptef'] += 6;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heighteh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = 10;
                    $rmrd['hdehef'] = $heightdetail+135;
                    $rmrd['hfehef'] = 0;
                } else if (!$rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['sdptef'] += 6;
                    $rmrd['sdptof'] += 6;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 10;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = 10;
                    $rmrd['hdehef'] = $heightdetail+135;
                    $rmrd['hfehef'] = 20;
                } else if (!$rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 1;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 6;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 20;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = 10;
                    $rmrd['hdehef'] = $heightdetail+70;
                    $rmrd['hfehef'] = $rmrd['heightef'];
                } else if (!$rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['sdptef'] += 6;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 10;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = $rmrd['heightof'];
                    $rmrd['hhehef'] = 20;
                    $rmrd['hdehef'] = $heightdetail+70;
                    $rmrd['hfehef'] = $rmrd['heightef']+10;
                } else if (!$rmrd['showfh'] && !$rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['sdptef'] += 3;
                    $rmrd['sdptof'] += 6;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 10;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = $rmrd['heighteh'];
                    $rmrd['hdehef'] = $heightdetail+75;
                    $rmrd['hfehef'] = 20;
                } else if (!$rmrd['showfh'] && $rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['sdptef'] += 6;
                    $rmrd['sdptof'] += 3;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = $rmrd['heightoh'];
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = 10;
                    $rmrd['hdehef'] = $heightdetail+135;
                    $rmrd['hfehef'] = 0;
                } else if ($rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 2;
                    $rmrd['sdptef'] += 6;
                    $rmrd['sdptof'] += 6;
                    $rmrd['hhfhef'] = $rmrd['heightfh'];
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 10;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = 10;
                    $rmrd['hdehef'] = $heightdetail+135;
                    $rmrd['hfehef'] = 20;
                } else if (!$rmrd['showfh'] && !$rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] && !$rmrd['showlf']) {
                    $rmrd['sdpt0'] -= 3;
                    $rmrd['sdptef'] += 6;
                    $rmrd['sdptof'] += 6;
                    $rmrd['hhfhef'] = 0;
                    $rmrd['hdfhef'] = $heightdetail;
                    $rmrd['hffhef'] = 0;
                    $rmrd['hhohof'] = 10;
                    $rmrd['hdohof'] = $heightdetail;
                    $rmrd['hfohof'] = 0;
                    $rmrd['hhehef'] = 10;
                    $rmrd['hdehef'] = $heightdetail+135;
                    $rmrd['hfehef'] = 20;
                } else {
                    if ($rmrd['showfh'] && $rmrd['showef']) {
                        $rmrd['hhfhef'] = $rmrd['heightfh'];
                        $rmrd['hdfhef'] = $heightdetail;
                        $rmrd['hffhef'] = $rmrd['heightef'];
                    } else if (!$rmrd['showfh'] && $rmrd['showef']) {
                        $rmrd['hhfhef'] = 0;
                        $rmrd['hdfhef'] = $heightdetail+60;
                        $rmrd['hffhef'] = $rmrd['heightef'];
                        $rmrd['sdptef'] += 3;
                    } else if ($rmrd['showfh'] && !$rmrd['showef']) {
                        $rmrd['hhfhef'] = $rmrd['heightfh'];
                        $rmrd['hdfhef'] = $heightdetail;
                        // if (!$rmrd['showef'] && !$rmrd['showof']) {
                        //     $rmrd['sdpt0'] -= 6;
                        //     $rmrd['hffhef'] = $rmrd['hfef0_'];
                        // } else {
                            $rmrd['sdpt0'] -= 3;
                            $rmrd['hffhef'] = 0;
                        // }
                    } else if (!$rmrd['showfh'] && !$rmrd['showef']) {
                        $rmrd['hhfhef'] = 0;
                        $rmrd['hdfhef'] = $heightdetail+40;
                        $rmrd['hffhef'] = 0;
                    }
                    
                    //odd
                    if ($rmrd['showoh'] && $rmrd['showof']) {
                        $rmrd['hhohof'] = $rmrd['heightoh'];
                        $rmrd['hdohof'] = $heightdetail;
                        $rmrd['hfohof'] = $rmrd['heightof'];
                        $rmrd['sdptof'] += 0;
                    } else if (!$rmrd['showoh'] && $rmrd['showof']) {
                        $rmrd['hhohof'] = 9;
                        $rmrd['hdohof'] = $heightdetail+65;
                        $rmrd['hfohof'] = $rmrd['heightof'];
                        $rmrd['sdptof'] += 3;
                    } else if ($rmrd['showoh'] && !$rmrd['showof']) {
                        $rmrd['hhohof'] = $rmrd['heightoh'];
                        $rmrd['hdohof'] = $heightdetail;
                        $rmrd['hfohof'] = 0;
                    } else if (!$rmrd['showoh'] && !$rmrd['showof']) {
                        $rmrd['hhohof'] = 24;
                        $rmrd['hdohof'] = $heightdetail;
                        $rmrd['hfohof'] = 0;
                        $rmrd['sdptof'] += 3;
                    }
            
                    if ($rmrd['showeh'] && $rmrd['showef']) {
                        $rmrd['hhehef'] = $rmrd['heighteh'];
                        $rmrd['hdehef'] = $heightdetail+7;
                        $rmrd['hfehef'] = $rmrd['heightef'];
                        $rmrd['sdptef'] += 0;
                    } else if (!$rmrd['showeh'] && $rmrd['showef']) {
                        $rmrd['hhehef'] = 9;
                        $rmrd['hdehef'] = $heightdetail+65;
                        $rmrd['hfehef'] = $rmrd['heightef'];
                        $rmrd['sdptef'] += 3;
                    } else if ($rmrd['showeh'] && !$rmrd['showef']) {
                        $rmrd['hhehef'] = $rmrd['heighteh'];
                        $rmrd['hdehef'] = $heightdetail+75;
                        $rmrd['hfehef'] = 0;
                        $rmrd['sdptef'] += 0;
                    } else if (!$rmrd['showeh'] && !$rmrd['showef']) {
                        $rmrd['hhehef'] = 24;
                        $rmrd['hdehef'] = $heightdetail+120;
                        $rmrd['hfehef'] = 0;
                        $rmrd['sdptef'] += 3;
                    }
                }

                if ($rmrd['showfh'] && $rmrd['showef']) {
                    $rmrd['sdpt0'] = $rmrd['_sdpt0']-0;
                } else if (!$rmrd['showfh'] && $rmrd['showef']) {
                    $rmrd['sdpt0'] = $rmrd['_sdpt0']-1;
                } else if ($rmrd['showfh'] && !$rmrd['showef']) {
                    $rmrd['sdpt0'] = $rmrd['_sdpt0']-2;
                } else if (!$rmrd['showfh'] && !$rmrd['showef']) {
                    $rmrd['sdpt0'] = $rmrd['_sdpt0']-3;
                }
                
                if ($rmrd['showeh'] && $rmrd['showef']) {
                    $rmrd['sdptef'] = $rmrd['_sdptef']+0;
                } else if (!$rmrd['showeh'] && $rmrd['showef']) {
                    $rmrd['sdptef'] = $rmrd['_sdptef']+3;
                } else if ($rmrd['showeh'] && !$rmrd['showef']) {
                    $rmrd['sdptef'] = $rmrd['_sdptef']+3;
                } else if (!$rmrd['showeh'] && !$rmrd['showef']) {
                    $rmrd['sdptef'] = $rmrd['_sdptef']+6;
                }

                if ($rmrd['showoh'] && $rmrd['showof']) {
                    $rmrd['sdptof'] = $rmrd['_sdptof']+0;
                } else if (!$rmrd['showoh'] && $rmrd['showof']) {
                    $rmrd['sdptof'] = $rmrd['_sdptof']+3;
                } else if ($rmrd['showoh'] && !$rmrd['showof']) {
                    $rmrd['sdptof'] = $rmrd['_sdptof']+3;
                } else if (!$rmrd['showoh'] && !$rmrd['showof']) {
                    $rmrd['sdptof'] = $rmrd['_sdptof']+6;
                }
            }
        }
        // dd($rmrd['hffhef']);
        
        $html = '<!DOCTYPE html>
                <html lang="">
                    <head>
                        <meta charset="utf-8">
                        <meta http-equiv="X-UA-Compatible" content="IE=edge">
                        <meta name="viewport" content="width=device-width, initial-scale=1">
                        <title>'.$template->nama.'</title>
                        <style>
                            $css
                            
                            #watermark1 {
                                position: fixed;
                                top:   21cm;
                                // left:     1.5cm;
                                width:    8cm;
                                height:   8cm;
                                z-index:  -1000;
                                transform: rotate(300deg); 
                                color: #f9f8f7;
                                font-size: 50px;
                            }

                            #watermark2 {
                                position: fixed;
                                top:   10cm;
                                left:     -5cm;
                                width:    8cm;
                                height:   8cm;
                                z-index:  -1000;
                                transform: rotate(300deg); 
                                color: #f9f8f7;
                                font-size: 50px;
                            }
                        </style>
                    </head>
                    <body>
                        <div id="watermark1"> 
                            <h1>'.$template->reportwatermark.'</h1>
                        </div> 

                        <div id="watermark2"> 
                            <h1>'.$template->reportwatermark.'</h1>
                        </div> 

                        $content
                    </body>
                </html>';

        $body_margin = $template->margintop.' '.$template->marginright.' '.$template->marginbottom.' '.$template->marginleft;

        $css = 'body {
                    font-family: "MS Serif", "New York", sans-serif;
                    font-size: 11px;
                    margin: '.$body_margin.';
                }

                .container {
                    // position: fixed;
                }

                .fh {
                    margin: '.$rmrd['marginfh'].';
                    height: '.$rmrd['hhfhef'].'px;
                }

                .oh {
                    margin: '.$rmrd['marginoh'].';
                    height: '.$rmrd['hhohof'].'px;
                }

                .eh {
                    margin: '.$rmrd['margineh'].';
                    height: '.$rmrd['hhehef'].'px;
                }

                .of {
                    margin: '.$rmrd['marginof'].';
                    height: '.$rmrd['hfohof'].'px;
                }

                .ef0 {
                    margin: '.$rmrd['marginef'].';
                    height: '.$rmrd['hfef0'].'px;
                }

                .ef0_ {
                    margin: '.$rmrd['marginef'].';
                    height: '.$rmrd['hfef0_'].'px;
                }

                .ef {
                    margin: '.$rmrd['marginef'].';
                    height: '.$rmrd['hfehef'].'px;
                }

                .footer {
                    position: fixed;
                    margin: '.$body_margin.';
                }

                .font-size-address {
                    font-size: 10px
                }

                .img-logo {
                    height: 50px;
                    margin: 0px 0px 0px 10px;
                }

                .address {
                    margin: -60px 0px 0px 65px;
                    font-size: 8px;
                }

                .title {
                    position: absolute;
                    z-index: 10;
                    margin: -48px 0px 80px '.$headermiddleml.'px;
                    font-size: 12px;
                }

                .title div {
                    border-style: double;
                    padding: 3px 20px 3px 20px;
                    width: 200px;
                }

                .title b {
                    font-size: 20px;
                }

                .title p {
                    margin: 10px 0px 0px 0px;
                }

                .date {
                    margin: -65px 0px 20px '.$headerlastml.'px;
                    font-size: 12px;
                }

                .dibayar-kepada {
                    margin: 0px 0px 0px 15px;
                }

                /////////////////////

                .content-1-even-ef0 {
                    height: '.$rmrd['hdfhef'].'px;
                }

                .content-1-even-eh-ef {
                    height: '.$rmrd['hd'].'px;
                }

                .content-1-even-eh {
                    height: '.$rmrd['hd'].'px;
                }

                .content-1-even-ef {
                    height: '.$rmrd['hdehef'].'px;
                }

                .content-1-even {
                    height: '.$rmrd['hd'].'px;
                }

                .content-1-odd-oh-of {
                    height: '.$rmrd['hd'].'px;
                }

                .content-1-odd-oh {
                    height: '.$rmrd['hd'].'px;
                }

                .content-1-odd-of {
                    height: '.$rmrd['hdohof'].'px;
                }

                .content-1-odd {
                    height: '.$rmrd['hd'].'px;
                }

                ///////////////

                .content-1 {
                    height: '.$rmrd['hd'].'px;
                }
                
                .table-custom-1 {
                    // width: 100%;
                    margin: 0px 0px 0px 0px;
                    border-collapse: collapse;
                }

                .table-custom-1, .table-custom-1 thead tr th, .table-custom-1 tbody tr td {
                    border: 1px solid black;
                    padding: 3px 3px 3px 3px;
                }
                
                .table-custom-1 thead tr th:nth-child(1) {
                    width: '.$content1width[0].'px;
                }

                .table-custom-1 thead tr th:nth-child(2) {
                    width: '.$content1width[1].'px;
                }

                .table-custom-1 thead tr th:nth-child(3) {
                    width: '.$content1width[2].'px;
                }

                .table-custom-1 thead tr th:nth-child(4) {
                    width: '.$content1width[3].'px;
                }

                .table-custom-1 thead tr th:nth-child(5) {
                    width: '.$content1width[4].'px;
                }

                .table-custom-1 thead tr th:nth-child(6) {
                    width: '.$content1width[5].'px;
                }

                .table-custom-1 tbody tr .giro-cek {
                    border-left: 1px solid white !important;
                    border-bottom: 1px solid white !important;
                }

                .giro-cek b {
                    margin-bottom: 0px !important;
                }



                .table-custom-2 {
                    // width: 100%;
                    border-collapse: collapse;
                }

                .table-custom-2 thead tr th:nth-child(1) {
                    width: '.$content2width[0].'px;
                }
                
                .table-custom-2 thead tr th:nth-child(2) {
                    width: '.$content2width[1].'px;
                }

                .table-custom-2 thead tr th:nth-child(3) {
                    width: '.$content2width[2].'px;
                }

                .table-custom-2 thead tr th:nth-child(4) {
                    width: '.$content2width[3].'px;
                }

                .table-custom-2 thead tr th:nth-child(5) {
                    width: '.$content2width[4].'px;
                }

                .table-custom-2, .table-custom-2 thead tr th, .table-custom-2 tbody tr td {
                    border: 1px solid black;
                    padding: 5px 3px 5px 3px;
                    text-align: center;
                }
                
                .text-right {
                    text-align: right;
                }
                '
        ;
        
        
        $html = str_replace('$css', $css, $html);
        
        /////////
        if (count($resultdetail) >= $showdatapertable) {
            $qtydatadetail = count($resultdetail)-count($resultdetail)%$showdatapertable;
            $qtydatadetail = $qtydatadetail/$showdatapertable;
            if (count($resultdetail)%$showdatapertable != 0) {
                $qtydatadetail++;
            }
        } else {
            $qtydatadetail = 1;
        }

        $start = 0;
        $finish = $showdatapertable-1;
        for ($i=0; $i<$qtydatadetail; $i++) {
            if ($i == 0) {
                if ($rmrd['sdpt0'] != $showdatapertable) {
                    $plusfinish = $rmrd['sdpt0'];
                } else {
                    $plusfinish = $showdatapertable;
                }
            } else {
                if ($i%2 == 0) {
                    if ($rmrd['sdpteh'] != $showdatapertable) {
                        $plusfinish = $rmrd['sdpteh'];
                    } else {
                        $plusfinish = $showdatapertable;
                    }
                    if ($rmrd['sdptef'] != $showdatapertable) {
                        $plusfinish = $rmrd['sdptef'];
                    } else {
                        $plusfinish = $showdatapertable;
                    }
                } else {
                    if ($rmrd['sdptoh'] != $showdatapertable) {
                        $plusfinish = $rmrd['sdptoh'];
                    } else {
                        $plusfinish = $showdatapertable;
                    }
                    if ($rmrd['sdptof'] != $showdatapertable) {
                        $plusfinish = $rmrd['sdptof'];
                    } else {
                        $plusfinish = $showdatapertable;
                    }
                }
            }
            $finish = $start+$plusfinish-1;
            $dataqtydatadetail[] = (Object)[
                'start' => $start,
                'finish' => $finish
            ];
            $start = $finish+1;
            $finish = $start+($plusfinish-1);
        }
        // dd($dataqtydatadetail);

        $qlr_ = [];
        foreach ($dataqtydatadetail as $k => $v) {
            $qlr_[$k] = 0;
            foreach ($resultdetail as $k2 => $v2) {
                if ($k2 >= $v->start && $k2 <= $v->finish) {
                    $qlr_[$k]++;
                }
            }
        }

        for ($i=count($qlr_)-1; $i>0; $i--) {
            if ($qlr_[$i] != 0) {
                break;
            }
            array_pop($qlr_);
            array_pop($dataqtydatadetail);
        }
        // die;
        // dd($qlr_);
        // dd($qlr_[array_key_last($qlr_)]);

        if ($template->defaultpaper == 'A4') {
            if ($template->ispotrait) {
                $rowenterrow = 21;
            } else {
                $rowenterrow = 21;
            }
        } else if ($template->defaultpaper == 'A5') {
            if ($template->ispotrait) {
                $rowenterrow = 10;
            } else {
                $rowenterrow = $jsonrules->showdatapertable;
            }
        }
        // dd($qlr_);

        $isenterrow = false;
        if ($rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef'] ||
            $rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef'] ||
            $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef']) {
            if ($qlr_[array_key_last($qlr_)] > $rowenterrow) {
                array_push($qlr_, $qlr_[array_key_last($qlr_)]-$rowenterrow);
                $qlr_[count($qlr_)-2] -= $qlr_[array_key_last($qlr_)];
                $dataqtydatadetail[array_key_last($dataqtydatadetail)]->finish = $dataqtydatadetail[array_key_last($dataqtydatadetail)]->start+$qlr_[array_key_last($dataqtydatadetail)]-1;
                // dd(count($resultdetail)-1);
                if ($dataqtydatadetail[array_key_last($dataqtydatadetail)]->finish <= count($resultdetail)-1) {
                    array_push($dataqtydatadetail, (object)[
                        'start' => $dataqtydatadetail[array_key_last($dataqtydatadetail)]->finish+1,
                        'finish' => $dataqtydatadetail[array_key_last($dataqtydatadetail)]->finish+1+($showdatapertable*2)
                    ]);
                }
                // dd($dataqtydatadetail);
                $isenterrow = true;
            }
        }
        
        // dd($qlr_);
        // dd($dataqtydatadetail);

        $content = '';
        $total = 0;
        $no = 1;
        $qtylastrecord = [];
        foreach ($dataqtydatadetail as $k => $v) {
            if ($k == 0) {
                $content .= @$rmrd['fh'];
            }

            $record = '';
            $qtylasttable = 0;
            $subtotal = 0;
            $isappend = false;
            $qtylastrecord[$k] = 0;
            foreach ($resultdetail as $k2 => $v2) {
                if ($k2 >= $v->start && $k2 <= $v->finish) {
                    $isappend = true;
                    if (@$v2->keterangan) {
                        if (strlen($v2->keterangan) >= $charketerangan) {
                            $keterangan = @substr($v2->keterangan, 0, $charketerangan).'...';
                        } else {
                            $keterangan = $v2->keterangan;
                        }
                    } else {
                        $keterangan = '';
                    }
                    $record .= "<tr>
                                    <td>".$no."</td>
                                    <td>".@$keterangan."</td>
                                    <td>".@$v2->idso."</td>
                                    <td>".@$v2->idpo."</td>
                                    <td>".@$v2->idpo."</td>
                                    <td style='text-align: right'>".@$v2->nominal."</td>
                                </tr>";
                    $no++;
                    $qtylasttable++;
                    $subtotal += @$v2->nominal;
                    // if (count($dataqtydatadetail) == $k+1) {
                        $qtylastrecord[$k]++;
                    // }
                }
            }
            // if (count($dataqtydatadetail) == $k+1) {
            //     dd($qtylastrecord);
            // }
            // die;

            if ($isappend) {
                if ($k == 0) {
                    $content .= '<div class="content-1-even-ef0">'.$template->reporttemplate.'</div>';
                } else {
                    if ($k%2 == 0) {
                        $content .= '<div class="content-1-even-ef">'.$template->reporttemplate.'</div>';
                    } else {
                        $content .= '<div class="content-1-even-of">'.$template->reporttemplate.'</div>';
                    }
                }
                $total += $subtotal;
                $content = str_replace('$datadetail', $record, $content);
                $content = str_replace('$subtotal_jumlah_datadetail', $subtotal, $content);
                $content = str_replace('$tax_jumlah_datadetail', 0, $content);
                $content = str_replace('$total_jumlah_datadetail', $total, $content);
            }
            
            if (count($dataqtydatadetail) == $k+1) {
                $showlf = $rmrd['showlf'];
                if ($template->defaultpaper == 'A4') {
                    if ($template->ispotrait) {
                        if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                            if (count($qtylastrecord)%2 == 1 && $qtylastrecord[count($dataqtydatadetail)-1] == 0) {
                                $showlf = false;
                            }
                        }
                    }
                } else if ($template->defaultpaper == 'A3') {
                    if ($template->ispotrait) {
                        if ($rmrd['showfh'] && $rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef'] && $rmrd['showlf']) {
                            if (count($qtylastrecord)%2 == 1 && $qtylastrecord[count($dataqtydatadetail)-1] == 0) {
                                $showlf = false;
                            }
                        }
                    }
                }

                if ($rmrd['isshoweverypagelf']) {
                    // if ($qtylastrecord == 2) {
                    //     $rmrd['marginlf'] = '62px 0px 0px 0px';
                    // } else if ($qtylastrecord == 3) {
                    //     $rmrd['marginlf'] = '62px 0px 0px 0px';
                    // } else if ($qtylastrecord == 4) {
                        // if ($k%2 == 1) {
                            $qlr = 0;
                            for ($i=count($qtylastrecord)-1; $i>=0; $i--) {
                                if ($qtylastrecord[$i] != 0) {
                                    $qlr = $qtylastrecord[$i];
                                    break;
                                }
                            }

                            if ($template->defaultpaper == 'A4') {
                                if ($template->ispotrait) {
                                    if ($rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 760;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else if (!$rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 830;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else if ($rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 760;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else if (!$rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 830;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else {
                                        $mtlf = 0;
                                    }
                                } else {
                                    if (!$rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 500;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else if ($rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 450;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else {
                                        // dd($qlr);
                                        if ($rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef']) {
                                            if ($k%2 == 0 && $k != 0) {
                                                $mtlf = 0;
                                            } else {
                                                $mtlf = 440;
                                                for ($i=0; $i<$qlr; $i++) {
                                                    $mtlf -= 20;
                                                }
                                            }
                                        } else {
                                            // dd($qlr);
                                            // if ($k%2 == 0) {
                                                $mtlf = 0;
                                            // } else {
                                            //     $mtlf = 440;
                                            //     for ($i=0; $i<$qlr; $i++) {
                                            //         $mtlf -= 20;
                                            //     }
                                            // }
                                        }
                                    }
                                }
                            } else if ($template->defaultpaper == 'A3') {
                                if ($template->ispotrait) {
                                    if ($rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 760;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else if (!$rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 830;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else if ($rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 760;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else if (!$rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 830;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else {
                                        $mtlf = 0;
                                    }
                                } else {
                                    if (!$rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 500;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else if ($rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 450;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else {
                                        // dd($qlr);
                                        if ($rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef']) {
                                            if ($k%2 == 0 && $k != 0) {
                                                $mtlf = 0;
                                            } else {
                                                $mtlf = 440;
                                                for ($i=0; $i<$qlr; $i++) {
                                                    $mtlf -= 20;
                                                }
                                            }
                                        } else {
                                            // dd($qlr);
                                            // if ($k%2 == 0) {
                                                $mtlf = 0;
                                            // } else {
                                            //     $mtlf = 440;
                                            //     for ($i=0; $i<$qlr; $i++) {
                                            //         $mtlf -= 20;
                                            //     }
                                            // }
                                        }
                                    }
                                }
                            } else if ($template->defaultpaper == 'A5') {
                                if ($template->ispotrait) {
                                    if ($rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 400;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else if (!$rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 830;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else if ($rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 60;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else if (!$rmrd['showoh'] && !$rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 830;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else {
                                        $mtlf = 0;
                                    }
                                } else {
                                    if ($rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 210;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else if ($rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && $rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 270;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else if ($rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && !$rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 210;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else if (!$rmrd['showoh'] && !$rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = 0;
                                        } else {
                                            $mtlf = 270;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else if ($rmrd['showoh'] && $rmrd['showeh'] && !$rmrd['showof'] && !$rmrd['showef']) {
                                        if ($k%2 == 0 && $k != 0) {
                                            $mtlf = -50;
                                        } else {
                                            $mtlf = 210;
                                            for ($i=0; $i<$qlr; $i++) {
                                                $mtlf -= 20;
                                            }
                                        }
                                    } else {
                                        // dd($qlr);
                                        if ($rmrd['showoh'] && $rmrd['showeh'] && $rmrd['showof'] && $rmrd['showef']) {
                                            if ($k%2 == 0 && $k != 0) {
                                                $mtlf = 0;
                                            } else {
                                                $mtlf = 440;
                                                for ($i=0; $i<$qlr; $i++) {
                                                    $mtlf -= 20;
                                                }
                                            }
                                        } else {
                                            // dd($qlr);
                                            // if ($k%2 == 0) {
                                                $mtlf = 0;
                                            // } else {
                                            //     $mtlf = 440;
                                            //     for ($i=0; $i<$qlr; $i++) {
                                            //         $mtlf -= 20;
                                            //     }
                                            // }
                                        }
                                    }
                                }
                            }
                            $rmrd['marginlf'] = $mtlf.'px 0px 0px 0px';
                        // }
                    // }
                    // echo $qlr.'<br>';die;
                    if ($showlf) {
                        $content .= '<div style="margin: '.$rmrd['marginlf'].'; height: '.$heightlf.'px">'.$template->reportfooter.'</div>';
                    }
                } else {
                    $content .= '<div style="margin: '.$rmrd['marginlf'].'; height: '.$heightlf.'px"></div>';
                }
            } else if (count($dataqtydatadetail) == $k+2 && $isenterrow) {
                if ($template->defaultpaper == 'A4') {
                    if (count($dataqtydatadetail)%2 == 0) {
                        // $content .= '<div style="margin-top: 20px">&nbsp;</div>';
                    } else {
                        // $content .= '<div style="margin-top: 20px">&nbsp;</div>';
                    }
                } else if ($template->defaultpaper == 'A5') {
                    if (count($dataqtydatadetail)%2 == 0) {
                        $content .= '<div style="margin-top: 20px">&nbsp;</div>';
                    } else {
                        $content .= '<div style="margin-top: 40px">&nbsp;</div>';
                    }
                }
                $content .= @$rmrd['oh'];
            } else {
                // if ($isappend) {
                    if ($k == 0) {
                        $content .= @$rmrd['ef0'];
                        $content .= @$rmrd['oh'];
                    } else {
                        // dd($rmrd);
                        if ($k%2 == 0) {
                            $content .= @$rmrd['ef'];
                            if (count($dataqtydatadetail) > $k+2) {
                                $content .= @$rmrd['oh'];
                            } else {
                                // if ($rmrd['showof']) {
                                    $content .= @$rmrd['oh'];
                                // } else {
                                //     $content .= '<div>&nbsp;</div>';
                                // }
                            }
                        } else {
                            $content .= @$rmrd['of'];
                            if (count($dataqtydatadetail) > $k+2) {
                                $content .= @$rmrd['eh'];
                            } else {
                                // if ($rmrd['showef']) {
                                    $content .= @$rmrd['eh'];
                                // } else {
                                //     $content .= '<div>&nbsp;</div>';
                                // }
                            }
                        }
                    }
                // }
            }
        }

        // dd($dataqtydatadetail);
        /////////

        $html = str_replace('$content', $content, $html);
        $html = str_replace('$logo', '<img src="assets/lycon.jpeg" alt="" class="img-logo">', $html);
        $html = str_replace('$genmcompany_nama', $resultheader->genmcompany_nama, $html);
        $html = str_replace('$today', date('d M Y'), $html);
        $html = str_replace('$kode', $resultheader->$tablemaster_kode, $html);
        $html = str_replace('$revdate', $resultheader->$tablemaster_revno.'/'.date('d M Y'), $html);

        $pdf = PDF::loadHTML($html);
        $pdf->setPaper($template->defaultpaper, $ispotrait);
        // print_r($html);die;
        return $pdf->stream('users.pdf');
    }

    public function print_($noid, $urlprint) {
        $noidrptmreport = 25101;
        $tabledetail = 'fincashbankdetail';
        $urlprint = explode('/', base64_decode($urlprint));
        $template = DB::connection('mysql2')->table('rptmreport')->where('noid', $noidrptmreport)->first();
        
        if ($template->defaultpaper == 'A4') {
            if ($template->ispotrait) {
                $ispotrait = 'potrait';
                $showdatapertable = 38;
                $sdpt0 = 38;
                $sdptoh = 35;
                $sdpteh = 35;
                $sdptof = 41;
                $sdptef = 41;
                $content1width = [20,298,80,80,70,100];
                $charketerangan = 50;
                $headermiddleml = 93;
                $headerlastml = 480;
                $content2width = [150,150,150,150,55];
                $heightdetail = 875;
            } else {
                $ispotrait = 'landscape';
                $showdatapertable = 22;
                $sdpt0 = 22;
                $sdptoh = 22;
                $sdpteh = 22;
                $sdptof = 23;
                $sdptef = 23;
                $content1width = [20,640,80,80,50,100];
                $charketerangan = 110;
                $headermiddleml = 240;
                $headerlastml = 790;
                $content2width = [215,215,215,215,129];
                $heightdetail = 545;
            }
        } else if ($template->defaultpaper == 'A3') {
            if ($template->ispotrait) {
                $ispotrait = 'potrait';
                $showdatapertable = 58;
                $sdptoh = 35;
                $sdpteh = 35;
                $sdptof = 62;
                $sdptef = 62;
                $content1width = [0,615,80,80,50,100];
                $charketerangan = 115;
                $headermiddleml = 230;
                $headerlastml = 760;
                $content2width = [220,220,220,220,79];
                $heightdetail = 1290;
            } else {
                $ispotrait = 'landscape';
                $showdatapertable = 35;
                $sdptoh = 35;
                $sdpteh = 35;
                $sdptof = 39;
                $sdptef = 39;
                $content1width = [0,1080,80,80,50,100];
                $charketerangan = 210;
                $headermiddleml = 430;
                $headerlastml = 1200;
                $content2width = [330,330,330,330,104];
                $heightdetail = 830;
            }
        }
        
        $tablemaster = DB::table('cmswidget')->select('maintable')->where('kode', $noid)->orderBy('noid', 'ASC')->first()->maintable;
        $resultmaster = DB::connection('mysql2')->table($tablemaster)->where('noid', $urlprint[1])->orderBy('noid', 'ASC')->first();
        $resultdetail = DB::connection('mysql2')->table($tabledetail)->where('idmaster', $resultmaster->noid)->orderBy('noid', 'ASC')->get();
        $resultrptmreportdetail = DB::connection('mysql2')->table('rptmreportdetail')->where('idreport', $noidrptmreport)->orderBy('nourut', 'ASC')->get();        
        $tablemaster_kode = $tablemaster.'_kode';
        $tablemaster_revno = $tablemaster.'_revno';
        $resultheader = DB::connection('mysql2')->select("SELECT $tablemaster.kode AS $tablemaster_kode, $tablemaster.revno AS $tablemaster_revno, genmcompany.nama AS genmcompany_nama FROM $tablemaster JOIN genmcompany ON $tablemaster.idcompany = genmcompany.noid WHERE $tablemaster.noid=$urlprint[1] LIMIT 1")[0];

        $rmrd = [];
        foreach ($resultrptmreportdetail as $k => $v) {
            if ($v->nama == 'First Header') {
                if ($v->isshowfirstpage) {
                    $rmrd['fh'] = '<div class="fh">'.$template->reportheader.'</div>';
                    $rmrd['sdpteh'] = $showdatapertable;
                    $rmrd['sdpt0'] = $sdpt0;
                    $rmrd['showfh'] = true;
                } else {
                    $rmrd['fh'] = '<div class="fh">&nbsp;</div>';
                    $rmrd['sdpteh'] = $sdpteh;
                    $rmrd['sdpt0'] = $sdpt0+4;
                    $rmrd['showfh'] = false;
                }
                $rmrd['marginfh'] = '0px 0px 0px 0px';
                $rmrd['heightfh'] = $v->widgetheight;
            } else if ($v->nama == 'Odd Header') {
                if ($v->isshoweverypage) {
                    $rmrd['oh'] = '<div class="oh">'.$template->reportheader.'</div>';
                    $rmrd['sdptoh'] = $showdatapertable;
                    $rmrd['showoh'] = true;
                } else {
                    $rmrd['oh'] = '<div class="oh">&nbsp;</div>';
                    $rmrd['sdptoh'] = $sdptoh;
                    $rmrd['showoh'] = false;
                }
                $rmrd['marginoh'] = '0px 0px 0px 0px';
                $rmrd['heightoh'] = $v->widgetheight;
            } else if ($v->nama == 'Even Header') {
                if ($v->isshoweverypage) {
                    $rmrd['eh'] = '<div class="eh">'.$template->reportheader.'</div>';
                    $rmrd['showeh'] = true;
                } else {
                    $rmrd['eh'] = '<div class="eh">&nbsp;</div>';
                    $rmrd['showeh'] = false;
                }
                $rmrd['margineh'] = '0px 0px 0px 0px';
                $rmrd['heighteh'] = $v->widgetheight;
            } else if ($v->nama == 'Detail') {
                // $rmrd['hd'] = $v->widgetheight;
                $rmrd['hd'] = $heightdetail;
            } else if ($v->nama == 'Odd Footer') {
                if ($v->isshoweverypage) {
                    $rmrd['of'] = '<div class="of">'.$template->reportfooter.'</div>';
                    $rmrd['sdptof'] = $showdatapertable;
                    $rmrd['showof'] = true;
                } else {
                    $rmrd['of'] = '<div class="of">&nbsp;</div>';
                    $rmrd['sdptof'] = $sdptof;
                    $rmrd['showof'] = false;
                }
                $rmrd['marginof'] = '0px 0px 0px 0px';
                $rmrd['heightof'] = $v->widgetheight;
            } else if ($v->nama == 'Even Footer') {
                if ($v->isshoweverypage) {
                    $rmrd['ef'] = '<div class="ef">'.$template->reportfooter.'</div>';
                    $rmrd['sdptef'] = $showdatapertable;
                    $rmrd['showef'] = true;
                    $rmrd['ef0'] = '<div class="ef0">'.$template->reportfooter.'</div>';
                    $rmrd['hfef0'] = $v->widgetheight;
                } else {
                    $rmrd['ef0'] = '<div class="ef0">&nbsp;</div>';
                    $rmrd['hfef0'] = 0;
                    $rmrd['sdpt0'] += 6;
                    $rmrd['ef'] = '<div class="ef">&nbsp;</div>';
                    $rmrd['sdptef'] = $sdptef;
                    $rmrd['showef'] = false;
                }
                // dd($rmrd['showfh'].'-'.$rmrd['showef'].'/'.$rmrd['sdpt0']);
                $rmrd['marginef'] = '0px 0px 0px 0px';
                $rmrd['heightef'] = $v->widgetheight;
            } else if ($v->nama == 'Last Footer') {
                $rmrd['isshoweverypagelf'] = $v->isshoweverypage;
                $rmrd['marginlf'] = '0px 0px 0px 0px';
                $heightlf = $v->widgetheight;
                $rmrd['showlf'] = true;
            }
        }
        // dd($rmrd['ef0']);

        if ($rmrd['showfh'] && $rmrd['showef']) {
            $rmrd['hhfhef'] = $rmrd['heightfh'];
            $rmrd['hdfhef'] = $heightdetail;
            $rmrd['hffhef'] = $rmrd['heightef'];
        } else if (!$rmrd['showfh'] && $rmrd['showef']) {
            $rmrd['hhfhef'] = 0;
            $rmrd['hdfhef'] = $heightdetail+60;
            $rmrd['hffhef'] = $rmrd['heightef'];
            $rmrd['sdptef'] += 3;
        } else if ($rmrd['showfh'] && !$rmrd['showef']) {
            $rmrd['hhfhef'] = $rmrd['heightfh'];
            $rmrd['hdfhef'] = $heightdetail;
            $rmrd['hffhef'] = 0;
            $rmrd['sdpt0'] -= 3;
        } else if (!$rmrd['showfh'] && !$rmrd['showef']) {
            $rmrd['hhfhef'] = 0;
            $rmrd['hdfhef'] = $heightdetail+40;
            $rmrd['hffhef'] = 0;
        }
        
        //odd
        if ($rmrd['showoh'] && $rmrd['showof']) {
            $rmrd['hhohof'] = $rmrd['heightoh'];
            $rmrd['hdohof'] = $heightdetail;
            $rmrd['hfohof'] = $rmrd['heightof'];
            $rmrd['sdptof'] += 0;
        } else if (!$rmrd['showoh'] && $rmrd['showof']) {
            $rmrd['hhohof'] = 9;
            $rmrd['hdohof'] = $heightdetail+65;
            $rmrd['hfohof'] = $rmrd['heightof'];
            $rmrd['sdptof'] += 3;
        } else if ($rmrd['showoh'] && !$rmrd['showof']) {
            $rmrd['hhohof'] = $rmrd['heightoh'];
            $rmrd['hdohof'] = $heightdetail;
            $rmrd['hfohof'] = 0;
        } else if (!$rmrd['showoh'] && !$rmrd['showof']) {
            $rmrd['hhohof'] = 24;
            $rmrd['hdohof'] = $heightdetail;
            $rmrd['hfohof'] = 0;
            $rmrd['sdptof'] += 3;
        }

        if ($rmrd['showeh'] && $rmrd['showef']) {
            $rmrd['hhehef'] = $rmrd['heighteh'];
            $rmrd['hdehef'] = $heightdetail+7;
            $rmrd['hfehef'] = $rmrd['heightef'];
            $rmrd['sdptef'] += 0;
        } else if (!$rmrd['showeh'] && $rmrd['showef']) {
            $rmrd['hhehef'] = 9;
            $rmrd['hdehef'] = $heightdetail+65;
            $rmrd['hfehef'] = $rmrd['heightef'];
            $rmrd['sdptef'] += 3;
        } else if ($rmrd['showeh'] && !$rmrd['showef']) {
            $rmrd['hhehef'] = $rmrd['heighteh'];
            $rmrd['hdehef'] = $heightdetail+75;
            $rmrd['hfehef'] = 0;
            $rmrd['sdptef'] += 0;
        } else if (!$rmrd['showeh'] && !$rmrd['showef']) {
            $rmrd['hhehef'] = 24;
            $rmrd['hdehef'] = $heightdetail+120;
            $rmrd['hfehef'] = 0;
            $rmrd['sdptef'] += 3;
        }
        // dd($rmrd['hffhef']);
        
        $html = '<!DOCTYPE html>
                <html lang="">
                    <head>
                        <meta charset="utf-8">
                        <meta http-equiv="X-UA-Compatible" content="IE=edge">
                        <meta name="viewport" content="width=device-width, initial-scale=1">
                        <title>'.$template->nama.'</title>
                        <style>
                            $css
                            
                            #watermark1 {
                                position: fixed;
                                top:   21cm;
                                // left:     1.5cm;
                                width:    8cm;
                                height:   8cm;
                                z-index:  -1000;
                                transform: rotate(300deg); 
                                color: #f9f8f7;
                                font-size: 50px;
                            }

                            #watermark2 {
                                position: fixed;
                                top:   10cm;
                                left:     -5cm;
                                width:    8cm;
                                height:   8cm;
                                z-index:  -1000;
                                transform: rotate(300deg); 
                                color: #f9f8f7;
                                font-size: 50px;
                            }
                        </style>
                    </head>
                    <body>
                        <div id="watermark1"> 
                            <h1>'.$template->reportwatermark.'</h1>
                        </div> 

                        <div id="watermark2"> 
                            <h1>'.$template->reportwatermark.'</h1>
                        </div> 

                        $content
                    </body>
                </html>';

        $body_margin = $template->margintop.' '.$template->marginright.' '.$template->marginbottom.' '.$template->marginleft;

        $css = 'body {
                    font-family: "MS Serif", "New York", sans-serif;
                    font-size: 11px;
                    margin: '.$body_margin.';
                }

                .container {
                    // position: fixed;
                }

                .fh {
                    margin: '.$rmrd['marginfh'].';
                    height: '.$rmrd['hhfhef'].'px;
                }

                .oh {
                    margin: '.$rmrd['marginoh'].';
                    height: '.$rmrd['hhohof'].'px;
                }

                .eh {
                    margin: '.$rmrd['margineh'].';
                    height: '.$rmrd['hhehef'].'px;
                }

                .of {
                    margin: '.$rmrd['marginof'].';
                    height: '.$rmrd['hfohof'].'px;
                }

                .ef0 {
                    margin: '.$rmrd['marginef'].';
                    height: '.$rmrd['hfef0'].'px;
                }

                .ef {
                    margin: '.$rmrd['marginef'].';
                    height: '.$rmrd['hfehef'].'px;
                }

                .footer {
                    position: fixed;
                    margin: '.$body_margin.';
                }

                .font-size-address {
                    font-size: 10px
                }

                .img-logo {
                    height: 50px;
                    margin: 0px 0px 0px 10px;
                }

                .address {
                    margin: -60px 0px 0px 65px;
                    font-size: 8px;
                }

                .title {
                    position: absolute;
                    z-index: 10;
                    margin: -48px 0px 80px '.$headermiddleml.'px;
                    font-size: 12px;
                }

                .title div {
                    border-style: double;
                    padding: 3px 20px 3px 20px;
                    width: 200px;
                }

                .title b {
                    font-size: 20px;
                }

                .title p {
                    margin: 10px 0px 0px 0px;
                }

                .date {
                    margin: -65px 0px 20px '.$headerlastml.'px;
                    font-size: 12px;
                }

                .dibayar-kepada {
                    margin: 0px 0px 0px 15px;
                }

                /////////////////////

                .content-1-even-ef0 {
                    height: '.$rmrd['hdfhef'].'px;
                }

                .content-1-even-eh-ef {
                    height: '.$rmrd['hd'].'px;
                }

                .content-1-even-eh {
                    height: '.$rmrd['hd'].'px;
                }

                .content-1-even-ef {
                    height: '.$rmrd['hdehef'].'px;
                }

                .content-1-even {
                    height: '.$rmrd['hd'].'px;
                }

                .content-1-odd-oh-of {
                    height: '.$rmrd['hd'].'px;
                }

                .content-1-odd-oh {
                    height: '.$rmrd['hd'].'px;
                }

                .content-1-odd-of {
                    height: '.$rmrd['hdohof'].'px;
                }

                .content-1-odd {
                    height: '.$rmrd['hd'].'px;
                }

                ///////////////

                .content-1 {
                    height: '.$rmrd['hd'].'px;
                }
                
                .table-custom-1 {
                    // width: 100%;
                    margin: 0px 0px 0px 0px;
                    border-collapse: collapse;
                }

                .table-custom-1, .table-custom-1 thead tr th, .table-custom-1 tbody tr td {
                    border: 1px solid black;
                    padding: 3px 3px 3px 3px;
                }
                
                .table-custom-1 thead tr th:nth-child(1) {
                    width: '.$content1width[0].'px;
                }

                .table-custom-1 thead tr th:nth-child(2) {
                    width: '.$content1width[1].'px;
                }

                .table-custom-1 thead tr th:nth-child(3) {
                    width: '.$content1width[2].'px;
                }

                .table-custom-1 thead tr th:nth-child(4) {
                    width: '.$content1width[3].'px;
                }

                .table-custom-1 thead tr th:nth-child(5) {
                    width: '.$content1width[4].'px;
                }

                .table-custom-1 thead tr th:nth-child(6) {
                    width: '.$content1width[5].'px;
                }

                .table-custom-1 tbody tr .giro-cek {
                    border-left: 1px solid white !important;
                    border-bottom: 1px solid white !important;
                }

                .giro-cek b {
                    margin-bottom: 0px !important;
                }



                .table-custom-2 {
                    // width: 100%;
                    border-collapse: collapse;
                }

                .table-custom-2 thead tr th:nth-child(1) {
                    width: '.$content2width[0].'px;
                }
                
                .table-custom-2 thead tr th:nth-child(2) {
                    width: '.$content2width[1].'px;
                }

                .table-custom-2 thead tr th:nth-child(3) {
                    width: '.$content2width[2].'px;
                }

                .table-custom-2 thead tr th:nth-child(4) {
                    width: '.$content2width[3].'px;
                }

                .table-custom-2 thead tr th:nth-child(5) {
                    width: '.$content2width[4].'px;
                }

                .table-custom-2, .table-custom-2 thead tr th, .table-custom-2 tbody tr td {
                    border: 1px solid black;
                    padding: 5px 3px 5px 3px;
                    text-align: center;
                }
                
                .text-right {
                    text-align: right;
                }
                '
        ;
        
        
        $html = str_replace('$css', $css, $html);
        
        /////////
        if (count($resultdetail) >= $showdatapertable) {
            $qtydatadetail = count($resultdetail)-count($resultdetail)%$showdatapertable;
            $qtydatadetail = $qtydatadetail/$showdatapertable;
            if (count($resultdetail)%$showdatapertable != 0) {
                $qtydatadetail++;
            }
        } else {
            $qtydatadetail = 1;
        }

        $start = 0;
        $finish = $showdatapertable-1;
        for ($i=0; $i<$qtydatadetail; $i++) {
            if ($i == 0) {
                if ($rmrd['sdpt0'] != $showdatapertable) {
                    $plusfinish = $rmrd['sdpt0'];
                } else {
                    $plusfinish = $showdatapertable;
                }
            } else {
                if ($i%2 == 0) {
                    if ($rmrd['sdpteh'] != $showdatapertable) {
                        $plusfinish = $rmrd['sdpteh'];
                    } else {
                        $plusfinish = $showdatapertable;
                    }
                    if ($rmrd['sdptef'] != $showdatapertable) {
                        $plusfinish = $rmrd['sdptef'];
                    } else {
                        $plusfinish = $showdatapertable;
                    }
                } else {
                    if ($rmrd['sdptoh'] != $showdatapertable) {
                        $plusfinish = $rmrd['sdptoh'];
                    } else {
                        $plusfinish = $showdatapertable;
                    }
                    if ($rmrd['sdptof'] != $showdatapertable) {
                        $plusfinish = $rmrd['sdptof'];
                    } else {
                        $plusfinish = $showdatapertable;
                    }
                }
            }
            $finish = $start+$plusfinish-1;
            $dataqtydatadetail[] = (Object)[
                'start' => $start,
                'finish' => $finish
            ];
            $start = $finish+1;
            $finish = $start+($plusfinish-1);
        }
        // dd($dataqtydatadetail);

        $content = '';
        $total = 0;
        $no = 1;
        foreach ($dataqtydatadetail as $k => $v) {
            if ($k == 0) {
                $content .= @$rmrd['fh'];
            }

            $record = '';
            $qtylasttable = 0;
            $subtotal = 0;
            $isappend = false;
            $qtylastrecord = 0;
            foreach ($resultdetail as $k2 => $v2) {
                if ($k2 >= $v->start && $k2 <= $v->finish) {
                    $isappend = true;
                    if (@$v2->keterangan) {
                        if (strlen($v2->keterangan) >= $charketerangan) {
                            $keterangan = @substr($v2->keterangan, 0, $charketerangan).'...';
                        } else {
                            $keterangan = $v2->keterangan;
                        }
                    } else {
                        $keterangan = '';
                    }
                    $record .= "<tr>
                                    <td>".$no."</td>
                                    <td>".@$keterangan."</td>
                                    <td>".@$v2->idso."</td>
                                    <td>".@$v2->idpo."</td>
                                    <td>".@$v2->idpo."</td>
                                    <td style='text-align: right'>".@$v2->nominal."</td>
                                </tr>";
                    $no++;
                    $qtylasttable++;
                    $subtotal += @$v2->nominal;
                    if (count($dataqtydatadetail) == $k+1) {
                        $qtylastrecord++;
                    }
                }
            }
            // die;

            if ($isappend) {
                if ($k == 0) {
                    $content .= '<div class="content-1-even-ef0">'.$template->reporttemplate.'</div>';
                } else {
                    if ($k%2 == 0) {
                        $content .= '<div class="content-1-even-ef">'.$template->reporttemplate.'</div>';
                    } else {
                        $content .= '<div class="content-1-even-of">'.$template->reporttemplate.'</div>';
                    }
                }
                $total += $subtotal;
                $content = str_replace('$datadetail', $record, $content);
                $content = str_replace('$subtotal_jumlah_datadetail', $subtotal, $content);
                $content = str_replace('$tax_jumlah_datadetail', 0, $content);
                $content = str_replace('$total_jumlah_datadetail', $total, $content);
            }
            
            if (count($dataqtydatadetail) == $k+1) {
                if ($rmrd['isshoweverypagelf']) {
                    // if ($qtylastrecord == 2) {
                    //     $rmrd['marginlf'] = '62px 0px 0px 0px';
                    // } else if ($qtylastrecord == 3) {
                    //     $rmrd['marginlf'] = '62px 0px 0px 0px';
                    // } else if ($qtylastrecord == 4) {
                        // if ($k%2 == 1) {
                            // $rmrd['marginlf'] = '62px 0px 0px 0px';
                        // }
                    // }
                    // echo $qtylastrecord.'<br>';die;
                    $content .= '<div style="margin: '.$rmrd['marginlf'].'; height: '.$heightlf.'px">'.$template->reportfooter.'</div>';
                } else {
                    $content .= '<div style="margin: '.$rmrd['marginlf'].'; height: '.$heightlf.'px"></div>';
                }
            } else {
                // if ($isappend) {
                    if ($k == 0) {
                        $content .= @$rmrd['ef0'];
                        $content .= @$rmrd['oh'];
                    } else {
                        // dd($rmrd);
                        if ($k%2 == 0) {
                            $content .= @$rmrd['ef'];
                            if (count($dataqtydatadetail) > $k+2) {
                                $content .= @$rmrd['oh'];
                            } else {
                                if ($rmrd['showof']) {
                                    $content .= @$rmrd['oh'];
                                } else {
                                    $content .= '<div>&nbsp;</div>';
                                }
                            }
                        } else {
                            $content .= @$rmrd['of'];
                            if (count($dataqtydatadetail) > $k+2) {
                                $content .= @$rmrd['eh'];
                            } else {
                                if ($rmrd['showef']) {
                                    $content .= @$rmrd['eh'];
                                } else {
                                    $content .= '<div>&nbsp;</div>';
                                }
                            }
                        }
                    }
                // }
            }
        }
        // dd($content);
        // dd($dataqtydatadetail);
        /////////

        $html = str_replace('$content', $content, $html);
        $html = str_replace('$logo', '<img src="assets/lycon.jpeg" alt="" class="img-logo">', $html);
        $html = str_replace('$genmcompany_nama', $resultheader->genmcompany_nama, $html);
        $html = str_replace('$today', date('d M Y'), $html);
        $html = str_replace('$kode', $resultheader->$tablemaster_kode, $html);
        $html = str_replace('$revdate', $resultheader->$tablemaster_revno.'/'.date('d M Y'), $html);

        $pdf = PDF::loadHTML($html);
        $pdf->setPaper($template->defaultpaper, $ispotrait);
        return $pdf->stream('users.pdf');
    }

    public function print_backup($noid, $urlprint) {
        $rptmreport_noid = 25101;
        $urlprint = explode('/', base64_decode($urlprint));
        $template = DB::connection('mysql2')->table('rptmreport')->where('noid', $rptmreport_noid)->first();
        
        if ($template->ispotrait) {
            $ispotrait = 'potrait';
            $margintop_even_first = 25;
            $margintop_even = 25;
            $margintop_odd_first = 555;
            $margintop_odd = 219;
            $header_margintop_first = 30;
            $header_margintop = 560;
            $footer_margintop_first = 373;
            $footer_margintop = 900;
        } else {
            // $margintop_even_first = 35;
            // $margintop_odd_first = 185;
            // $margintop_even = 35;
            // $margintop_odd = 290;
            // $header_margintop_first = 0;
            // $header_margintop = 525;
            // $footer_margintop_first = 35;
        }
        
        $html = '<!DOCTYPE html>
                <html lang="">
                    <head>
                        <meta charset="utf-8">
                        <meta http-equiv="X-UA-Compatible" content="IE=edge">
                        <meta name="viewport" content="width=device-width, initial-scale=1">
                        <title>'.$template->nama.'</title>
                        <style>
                            $css
                            
                            #watermark1 {
                                position: fixed;
                                top:   21cm;
                                // left:     1.5cm;
                                width:    8cm;
                                height:   8cm;
                                z-index:  -1000;
                                transform: rotate(300deg); 
                                color: #f9f8f7;
                                font-size: 50px;
                            }

                            #watermark2 {
                                position: fixed;
                                top:   10cm;
                                left:     -5cm;
                                width:    8cm;
                                height:   8cm;
                                z-index:  -1000;
                                transform: rotate(300deg); 
                                color: #f9f8f7;
                                font-size: 50px;
                            }
                        </style>
                    </head>
                    <body>
                        <div id="watermark1"> 
                            <h1>'.$template->reportwatermark.'</h1>
                        </div> 

                        <div id="watermark2"> 
                            <h1>'.$template->reportwatermark.'</h1>
                        </div> 

                        $_header

                        $_footer

                        $_container
                    </body>
                </html>';

        $body_margin = $template->margintop.' '.$template->marginright.' '.$template->marginbottom.' '.$template->marginleft;

        $css = 'body {
                    font-family: "MS Serif", "New York", sans-serif;
                    font-size: 11px;
                    margin: '.$body_margin.';
                }

                .container {
                    // position: fixed;
                }

                .header {
                    position: fixed;
                    margin: '.$body_margin.';
                }

                .footer {
                    position: fixed;
                    margin: '.$body_margin.';
                }

                .font-size-address {
                    font-size: 10px
                }

                .img-logo {
                    height: 50px;
                    margin: -15px 0px 0px 10px;
                }

                .address {
                    margin: -75px 0px 0px 65px;
                    font-size: 8px;
                }

                .title {
                    position: absolute;
                    z-index: 10;
                    margin: -48px 0px 80px 70px;
                    font-size: 12px;
                }

                .title div {
                    border-style: double;
                    padding: 3px 20px 3px 20px;
                    width: 200px;
                }

                .title b {
                    font-size: 20px;
                }

                .title p {
                    margin: 10px 0px 0px 0px;
                }

                .date {
                    margin: -65px 0px 20px 430px;
                    font-size: 12px;
                }

                .dibayar-kepada {
                    margin: 0px 0px 0px 15px;
                }
                
                .table-custom-1 {
                    // width: 100%;
                    margin: 0px 0px 0px 0px;
                    border-collapse: collapse;
                }

                .table-custom-1, .table-custom-1 thead tr th, .table-custom-1 tbody tr td {
                    border: 1px solid black;
                    padding: 3px 3px 3px 3px;
                }
                
                .table-custom-1 thead tr th:nth-child(1) {
                    width:0px;
                }

                .table-custom-1 thead tr th:nth-child(2) {
                    width:285px;
                }

                .table-custom-1 thead tr th:nth-child(3) {
                    width: 80px;
                }

                .table-custom-1 thead tr th:nth-child(4) {
                    width: 80px;
                }

                .table-custom-1 thead tr th:nth-child(5) {
                    width: 50px;
                }

                .table-custom-1 thead tr th:nth-child(6) {
                    width: 100px;
                }

                .table-custom-1 tbody tr .giro-cek {
                    border-left: 1px solid white !important;
                    border-bottom: 1px solid white !important;
                }

                .giro-cek b {
                    margin-bottom: 0px !important;
                }



                .table-custom-2 {
                    // width: 100%;
                    border-collapse: collapse;
                }

                .table-custom-2 thead tr th:nth-child(1) {
                    width:144px;
                }
                
                .table-custom-2 thead tr th:nth-child(2) {
                    width:144px;
                }

                .table-custom-2 thead tr th:nth-child(3) {
                    width:144px;
                }

                .table-custom-2 thead tr th:nth-child(4) {
                    width:144px;
                }

                .table-custom-2 thead tr th:nth-child(5) {
                    width:51px;
                }

                .table-custom-2, .table-custom-2 thead tr th, .table-custom-2 tbody tr td {
                    border: 1px solid black;
                    padding: 5px 3px 5px 3px;
                    text-align: center;
                }
                
                .text-right {
                    text-align: right;
                }
                '
        ;
        
        
        $html = str_replace('$css', $css, $html);

        $tablemaster = DB::table('cmswidget')->select('maintable')->where('kode', $noid)->orderBy('noid', 'ASC')->first()->maintable;
        $resultdetail = DB::table('cmswidget')->select('maintable')->where('kode', $noid)->orderBy('noid', 'ASC')->get();
        $tabledetail = [];
        foreach ($resultdetail as $k => $v) {
            if ($k > 0) {
                $tabledetail[] = $v->maintable;
            }
        }
        // $tabledetail = [
        //     'fincashdisbursmentdetail',
        //     'fincashdisbursmentdetail',
        //     'fincashbankdetail',
        //     'fincashbankdetail',
        //     'fincashbankdetail'
        // ];
        // dd($tabledetail);

        $tablemaster_kode = $tablemaster.'_kode';
        $tablemaster_revno = $tablemaster.'_revno';
        $datamaster = DB::connection('mysql2')->select("SELECT $tablemaster.kode AS $tablemaster_kode, $tablemaster.revno AS $tablemaster_revno, genmcompany.nama AS genmcompany_nama FROM $tablemaster JOIN genmcompany ON $tablemaster.idcompany = genmcompany.noid WHERE $tablemaster.noid=$urlprint[1] LIMIT 1")[0];
        // $datadetail = [];
        $showdatapertable = 10;
        $_container = '';
        foreach ($tabledetail as $k => $v) {
            $datadetail = DB::connection('mysql2')->select("SELECT * FROM $v WHERE idmaster=$urlprint[1]");
            if (count($datadetail) >= $showdatapertable) {
                $qtydatadetail = count($datadetail)-count($datadetail)%$showdatapertable;
                $qtydatadetail = $qtydatadetail/$showdatapertable;
                if (count($datadetail)%$showdatapertable != 0) {
                    $qtydatadetail++;
                }
            } else {
                $qtydatadetail = 1;
            }
            
            $dataqtydatadetail = [];
            $start = 0;
            $finish = $showdatapertable-1;
            $_no = 1;
            for ($i=0; $i<$qtydatadetail; $i++) {
                if ($k == 0) {
                    if ($i%2 == 0) {
                        if ($i == 2) {
                            $margintop = $margintop_even*$_no;
                        } else {
                            if ($qtydatadetail > $i+1) {
                                $margintop = $margintop_even*$_no;
                            } else {
                                $margintop = $margintop_even*$_no;
                            }
                        }
                    } else {
                        if ($i == 3) {
                            $margintop = $margintop_odd*$_no;
                        } else {
                            $margintop = $margintop_odd*$_no;
                        }
                    }
                } else {
                    if ($i%2 == 0) {
                        if ($i == 0) {
                            $margintop = $margintop_even*$_no;
                        } else {
                            if ($qtydatadetail > $i+1) {
                                $margintop = $margintop_even*$_no;
                            } else {
                                $margintop = $margintop_even*$_no;
                            }
                        }
                    } else {
                        if ($i == 1) {
                            $margintop = $margintop_odd*$_no;
                        } else {
                            $margintop = $margintop_odd*$_no;
                        }
                    }
                }
                $dataqtydatadetail[] = (Object)[
                    'start' => $start,
                    'finish' => $finish,
                    'margintop' => $margintop
                ];
                $start = $finish+1;
                $finish = $start+($showdatapertable-1);
            }

            $_header = '';
            $_footer = '';
            for ($i=0; $i<$qtydatadetail; $i++) {
                if ($i == 0) {
                    $_header .= '<div class="header" style="margin-top: '.$header_margintop_first.'px">
                                    '.$template->reportheader.'
                                </div>';
                    $_footer .= '<div class="footer" style="margin-top: '.$footer_margintop_first.'px">
                                    '.$template->reportfooter.'
                                </div>';
                    $_header .= '<div class="header" style="margin-top: '.$header_margintop.'px">
                                    '.$template->reportheader.'
                                </div>';
                    $_footer .= '<div class="footer" style="margin-top: '.$footer_margintop.'px">
                        '.$template->reportfooter.'
                    </div>';
                }
            }
            
            $html = str_replace('$_header', $_header, $html);
            $html = str_replace('$_footer', $_footer, $html);
            
            $no = 1;
            $_total = 0;
            foreach ($dataqtydatadetail as $k3 => $v3) {
                if (count($tabledetail) > 0) {
                    if ($k3 != 0 && $k3 != 1) {
                        if ($k3%2 == 0) {
                            if (count($dataqtydatadetail) == $k3+1) {
                                $_container .= '<br>';
                            }
                        }
                    }
                }
                if ($k == 0) {
                    if ($k3 == 0) {
                        $_container .= '<div class="container" style="position: absolute; margin-top: '.$margintop_even_first.'px;">'.$tabledetail[$k].'-'.$k3.$template->reporttemplate;
                    } else if ($k3 == 1) {
                        $_container .= '<div class="container" style="position: absolute; margin-top: '.$margintop_odd_first.'px">'.$tabledetail[$k].'-'.$k3.$template->reporttemplate;
                    } else {
                        if ($k3%2 == 0) {
                            if ($k3 == 2) {
                                $_container .= '<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>';
                                
                                for ($i=0; $i<$k3/2; $i++) {
                                    $_container .= '<br><br><br><br>';
                                }
                            } else {
                                $_container .= '<br><br><br><br>';
                            }
                            $_container .= '<div class="container" style="margin-top: '.$v3->margintop.'px">'.$tabledetail[$k].'-'.$k3.$template->reporttemplate;
                        } else {
                            $_container .= '<div class="container" style="margin-top: '.$v3->margintop.'px">'.$tabledetail[$k].'-'.$k3.$template->reporttemplate;
                        }
                    }
                } else {
                    if ($k3 == 0) {
                        $_container .= '<div class="container" style="margin-top: '.$v3->margintop.'px">'.$tabledetail[$k].'-'.$k3.$template->reporttemplate;
                    } else if ($k3 == 1) {
                        $_container .= '<div class="container" style="margin-top: '.$v3->margintop.'px">'.$tabledetail[$k].'-'.$k3.$template->reporttemplate;
                    } else {
                        if ($k3%2 == 0) {
                            $_container .= '<br><br><br><br><br><br><br>';
                        }
                        $_container .= '<div class="container" style="margin-top: '.$v3->margintop.'px">'.$tabledetail[$k].'-'.$k3.$template->reporttemplate;
                    }
                }
                $_dd = '';
                $_no = 1;
                $_subtotal = 0;
                foreach ($datadetail as $k2 => $v2) {
                    if ($k2 >= $v3->start && $k2 <= $v3->finish) {
                        if (@$v2->keterangan) {
                            if (strlen($v2->keterangan) >= 50) {
                                $keterangan = @substr($v2->keterangan, 0, 50).'...';
                            } else {
                                $keterangan = $v2->keterangan;
                            }
                        } else {
                            $keterangan = '';
                        }
                        $_dd .= "<tr>
                                    <td>".$no."</td>
                                    <td>".$keterangan."</td>
                                    <td>".@$v2->idso."</td>
                                    <td>".@$v2->idpo."</td>
                                    <td>".@$v2->idpo."</td>
                                    <td style='text-align: right'>".@$v2->nominal."</td>
                                </tr>";
                        $no++;
                        $_no++;
                        $_subtotal += @$v2->nominal;
                    }
                }

                $_total += $_subtotal;
                $_container = str_replace('$datadetail', $_dd, $_container);
                $_container = str_replace('$subtotal_jumlah_datadetail', $_subtotal, $_container);
                $_container = str_replace('$tax_jumlah_datadetail', 0, $_container);
                $_container = str_replace('$total_jumlah_datadetail', $_total, $_container);
                $_container .= '</div>';
                
                if (count($tabledetail) > 1) {
                    if (count($dataqtydatadetail) <= 2) {
                        if ($k3%2 == 0) {
                            if (count($dataqtydatadetail) == $k3+1) {
                                    $_container .= '<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>';
                                    $_container .= '<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>';
                                    $_container .= '<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>';
                                if ($k == 0) {
                                    $_container .= '<br><br><br><br><br><br><br><br><br><br><br><br>';
                                }
                            }
                        } else {
                            if (count($dataqtydatadetail) == $k3+1) {
                                if (count($tabledetail) != $k+1) {
                                    $_container .= '<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>';
                                    $_container .= '<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>';
                                    $_container .= '<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>';
                                }
                                if (count($datadetail)-$v3->start == 1) {

                                } else {
                                    $_container .= '<br><br><br><br>';
                                }
                            }
                        }
                    } else {
                        if ($k3 != 0 && $k3 != 1) {
                            if ($k3%2 == 0) {
                                if (count($dataqtydatadetail) == $k3+1) {
                                    $_container .= '<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>';
                                    $_container .= '<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>';
                                }
                            } else {
                                if (count($dataqtydatadetail) == $k3+1) {
                                    if (count($datadetail)-$v3->start == 1) {
                                        $_container .= '<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>';
                                    } else if (count($datadetail)-$v3->start == 2) {
                                        $_container .= '<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>';
                                    } else if (count($datadetail)-$v3->start == 3) {
                                        $_container .= '<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>';
                                    } else if (count($datadetail)-$v3->start == 4) {
                                        $_container .= '<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>';
                                    } else if (count($datadetail)-$v3->start == 5) {
                                        $_container .= '<br><br><br><br><br><br><br><br><br><br><br><br><br>';
                                    } else if (count($datadetail)-$v3->start == 6) {
                                        $_container .= '<br><br><br><br><br><br><br><br><br><br><br>';
                                    } else if (count($datadetail)-$v3->start == 7) {
                                        $_container .= '<br><br><br><br><br><br><br><br><br>';
                                    } else if (count($datadetail)-$v3->start == 8) {
                                        $_container .= '<br><br><br><br><br><br><br><br>';
                                    } else if (count($datadetail)-$v3->start == 9) {
                                        $_container .= '<br><br><br><br><br><br>';
                                    } else if (count($datadetail)-$v3->start == 10) {
                                        $_container .= '<br><br><br><br><br>';
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $no++;
        }
        $html = str_replace('$_container', $_container, $html);
        $html = str_replace('$logo', '<img src="assets/lycon.jpeg" alt="" class="img-logo">', $html);
        $html = str_replace('$genmcompany_nama', $datamaster->genmcompany_nama, $html);
        $html = str_replace('$today', date('d M Y'), $html);
        $html = str_replace('$kode', $datamaster->$tablemaster_kode, $html);
        $html = str_replace('$revdate', $datamaster->$tablemaster_revno.'/'.date('d M Y'), $html);

        $pdf = PDF::loadHTML($html);
        $pdf->setPaper($template->defaultpaper, $ispotrait);
        return $pdf->stream('users.pdf');
    }

    public function index() {
        if(!Session::get('login')){
            
            // echo 'asd';die;
            $idhomepagelink =  request()->path();
            if ($idhomepagelink == '' || $idhomepagelink == '/') {
                $idhomepagelink = 10;
            }
            $user = User::where('noid', 0)->first();
            $menu = Menu::where('noid',$idhomepagelink)->first();
            $public = $menu->ispublic;
            $page = Page::where('noid',$menu->linkidpage)->first();
            $panel = Panel::where('idpage', $page->noid)->orderBy('norow','asc')->orderBy('nocol','asc')->get();
            $panel1 = Panel::where('idpage', $page->noid)->where('nocol',1)->orderBy('norow','asc')->orderBy('nocol','asc')->first();
            $panel2 = Panel::where('idpage', $page->noid)->where('nocol',0)->orderBy('norow','asc')->orderBy('nocol','asc')->first();

            $page_title = $page->pagetitle;
            $page_description = $page->pagetitle;
            $idhomepage =  $user->idhomepage;
            $noid = 'null';
            $myusername = 'Guest';
            $slug = $page->noid;
            $widget = Menu::getAllPanel($slug);

            $tabletitle = $page_title;
            $sessionname = $myusername;

            return view('lam_dashboard.dashboard', compact(
                'noid',
                'public',
                'widget',
                'myusername',
                'idhomepagelink',
                'page',
                'panel',
                'panel1',
                'panel2',
                'page_title',
                'tabletitle',
                'sessionname',
                'idhomepage',
                'page_description'
            ));

            // dd($user);
        } else {
            $idhomepagelink =  request()->path();
            if ($idhomepagelink == '' || $idhomepagelink == '/') {
                $idhomepagelink = 10;
            }
            $user = User::where('noid', Session::get('noid'))->first();
            // dd($user);
            $menu = Menu::where('noid',$idhomepagelink)->first();
            $public = $menu->ispublic;
            $page = Page::where('noid',$menu->linkidpage)->first();
            $panel = Panel::where('idpage', $page->noid)->orderBy('norow','asc')->orderBy('nocol','asc')->get();
            $panel1 = Panel::where('idpage', $page->noid)->where('nocol',1)->get();
            // dd($panel1);
            $panel2 = Panel::where('idpage', $page->noid)->orderBy('norow','asc')->orderBy('nocol','asc')->get();
            $page_title = $page->pagetitle;
            $page_description = $page->pagetitle;
            $idhomepage =  $user->idhomepage;
            $noid =  Session::get('noid');
            $myusername = Session::get('myusername');
            $slug = $page->noid;
            $widget = Menu::getAllPanel($slug);

            $tabletitle = $page_title;
            $sessionname = Session::get('nama');
            $positions = User::findPosition($myusername);
            $userdetail = User::findData(Session::get('myusername'));
            
            return view('lam_dashboard.dashboard', compact(
                'noid',
                'public',
                'widget',
                'myusername',
                'idhomepagelink',
                'page',
                'panel',
                'page_title',
                'tabletitle',
                'sessionname',
                'positions',
                'userdetail',
                'idhomepage',
                'page_description'
            ));
        }
    }

    public function page($slug,Request $request) {
        if(!Session::get('login')){
            return view('pages.login');
        }
        $menu = Menu::where('noid',$slug)->first();
        $page = Page::where('noid',$menu->linkidpage)->first();
        // if($page->idtypepage == 9){
        //     return Redirect::to('system/index');
        // }
        $idhomepagelink = $slug;
        $user = User::where('noid', Session::get('noid'))->first();
        $public = $menu->ispublic;
        $panel = Panel::where('idpage', $page->noid)->orderBy('norow','asc')->orderBy('nocol','asc')->get();
        $page_title = $page->pagetitle;
        $page_description = $page->pagetitle;
        $idhomepage =  $user->idhomepage;
        $noid = Session::get('noid');
        $myusername = Session::get('myusername');

        if($menu->ispublic == 1){
            return view('lam_dashboard.index', compact(
                'noid',
                'public',
                'myusername',
                'idhomepagelink',
                'page',
                'panel',
                'page_title',
                'idhomepage',
                'page_description'));
        } else {
            if (!Session::get('login')) {
                return view('pages.login');
            } else {
                return view('lam_dashboard.index', compact(
                'noid',
                'public',
                'myusername',
                'idhomepagelink',
                'page',
                'panel',
                'page_title',
                'idhomepage',
                'page_description'));

            }
        }
    }
}
