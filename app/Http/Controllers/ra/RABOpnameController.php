<?php

namespace App\Http\Controllers\ra;

use App\Http\Controllers\Controller;
use App\Model\RA\RAPOpname as MP;
use App\Model\Purchase\PurchaseOrder as MP2;
use App\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use DB;
use Image;
use Storage;

class RABOpnameController extends Controller {

    public $color = [
        'white' => '#FFFFFF',
        'default' => '#E1E5EC',
        'dark' => '#2F353B',
        'blue' => '#3598DC',
        'blue-madison' => '#578EBE',
        'blue-chambray' => '#2C3E50',
        'blue-ebonyclay' => '#22313F',
        'blue-hoki' => '#67809F',
        'blue-steel' => '#4B77BE',
        'blue-soft' => '#4C87B9',
        'blue-dark' => '#5E738B',
        'blue-sharp' => '#5C9BD1',
        'blue-oleo' => '#94A0B2',
        'green' => '#32C5D2',
        'green-meadow' => '#1BBC9B',
        'green-seagreen' => '#1BA39C',
        'green-torquoise' => '#36D7B7',
        'green-haze' => '#44B6AE',
        'green-jungle' => '#26C281',
        'green-soft' => '#3FABA4',
        'green-dark' => '#4DB3A2',
        'green-sharp' => '#2AB4C0',
        'green-steel' => '#29B4B6',
        'grey' => '#E5E5E5',
        'grey-steel' => '#E9EDEF',
        'grey-cararra' => '#FAFAFA',
        'grey-gallery' => '#555555',
        'grey-cascade' => '#95A5A6',
        'grey-silver' => '#BFBFBF',
        'grey-salsa' => '#ACB5C3',
        'grey-salt' => '#BFCAD1',
        'grey-mint' => '#525E64',
        'red' => '#E7505A',
        'red-pink' => '#E08283',
        'red-sunglo' => '#E26A6A',
        'red-intense' => '#E35B5A',
        'red-thunderbird' => '#D91E18',
        'red-flamingo' => '#EF4836',
        'red-soft' => '#D05454',
        'red-haze' => '#F36A5A',
        'red-mint' => '#E43A45',
        'yellow' => '#C49F47',
        'yellow-gold' => '#E87E04',
        'yellow-casablanca' => '#F2784B',
        'yellow-crusta' => '#F3C200',
        'yellow-lemon' => '#F7CA18',
        'yellow-saffron' => '#F4D03F',
        'yellow-soft' => '#C8D046',
        'yellow-haze' => '#C5BF66',
        'yellow-mint' => '#C5B96B',
        'purple' => '#8E44AD',
        'purple-plum' => '#8775A7',
        'purple-medium' => '#BF55EC',
        'purple-studio' => '#8E44AD',
        'purple-wisteria' => '#9B59B6',
        'purple-seance' => '#9A12B3',
        'purple-intense' => '#8775A7',
        'purple-sharp' => '#796799',
        'purple-soft' => '#8877A9',
    ];
    public static $main = [
        'table' => 'prjmopnamerap',
        'tablemilestone' => 'prjmopnamerabmilestone',
        'tableheader' => 'prjmopnamerabheader',
        'tabledetail' => 'prjmrabdetail',
        'tablecdf' => 'prjmopnamerabdocument',
        'table-title' => 'RAB Progress',
        'table2' => 'purchaseoffer',
        'tabledetail2' => 'purchaseofferdetail',
        'table2-title' => 'Purchase Offer',
        'generatecode' => 'generatecode/604/1',
        'generatecode2' => 'generatecode/604/1',
        'idtypetranc' => 605,
        'idtypetranc2' => 606,
        'noid' => 50011111,
        'noidnew' => 50011112,
        'printcode' => 'print_purchase_request',
        'condition' => [
            'mttts' => [
                [],
                [
                    'mttts' => 'draft',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => true,
                        'd' => true,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => true,
                        'd' => true,
                    ],
                    'draft' => false,
                    'issued' => true,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => false,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'issued',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => true,
                    'issued' => false,
                    'verified' => true,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => false,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'verified',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => true,
                    'verified' => false,
                    'approved' => true,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => false,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'approved',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'pending',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'canceled',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
                [
                    'mttts' => 'finished',
                    'crud' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'crudd' => [
                        'c' => true,
                        'r' => true,
                        'u' => false,
                        'd' => false,
                    ],
                    'draft' => false,
                    'issued' => false,
                    'verified' => false,
                    'approved' => false,
                    'pending' => false,
                    'canceled' => false,
                    'finished' => false,
                    'apcf' => true,
                    'print' => true,
                    'log' => true,
                ],
            ],
        ]
    ];

    public static function index() {
        $idtypepage = static::$main['noid'];
        $sessionnoid = Session::get('noid');
        $sessionmyusername = Session::get('myusername');
        $sessionname = Session::get('nama');
        $sessionidcompany = Session::get('idcompany');
        $sessioniddepartment = Session::get('iddepartment');
        $sessionidgudang = Session::get('idgudang');

        $maintable = static::$main['table'];
        $breadcrumb = Menu::getBreadcrumb(static::$main['noid']);
        $pagenoid = static::$main['noid'];

        $defaulttanggal = date('d-M-Y');
        $defaulttanggaldue = date('d-M-Y', strtotime('+7 day'));
        $data = MP::getData(['table'=>static::$main['table'],'select'=>['noid','idstatustranc','idstatuscard','dostatus','idtypetranc','kode','kodereff','tanggal','tanggaltax','tanggaldue']]);

        $columns = [
            ['data'=>'checkbox','orderable'=>false,'class'=>'text-center'],
            ['data'=>'no','orderable'=>false,'class'=>'text-right'],
            // ['data'=>'noid'],
            ['data'=>'idtypetranc'],
            ['data'=>'idstatustranc'],
            // ['data'=>'idstatuscard'],
            // ['data'=>'dostatus'],
            ['data'=>'kode'],
            ['data'=>'kodereff'],
            ['data'=>'tanggal'],
            ['data'=>'generaltotal','class'=>'text-right'],
            ['data'=>'keterangan'],
            ['data'=>'idcompany'],
            ['data'=>'iddepartment'],
            ['data'=>'idgudang'],
            ['data'=>'action','orderable'=>false],
        ];
        $columnslog = [
            ['data'=>'checkbox','orderable'=>false,'class'=>'text-center'],
            ['data'=>'no','orderable'=>false,'class'=>'text-right'],
            ['data'=>'idtypetranc'],
            ['data'=>'idtypeaction'],
            ['data'=>'logsubject'],
            ['data'=>'keterangan'],
            ['data'=>'idreff'],
            ['data'=>'idcreate'],
            ['data'=>'docreate'],
            ['data'=>'action','orderable'=>false],
        ];
        $columnscostcontrol = [
            ['data'=>'checkbox','orderable'=>false,'class'=>'text-center'],
            ['data'=>'no','orderable'=>false,'class'=>'text-right'],
            ['data'=>'idstatustranc'],
            ['data'=>'idtypetranc'],
            ['data'=>'kode'],
            ['data'=>'kodereff'],
            ['data'=>'idcompany'],
            ['data'=>'iddepartment'],
            ['data'=>'generaltotal','class'=>'text-right'],
            ['data'=>'keterangan'],
            ['data'=>'tanggal'],
            ['data'=>'action','orderable'=>false],
        ];
        $columnsdetail = [
            ['data'=>'checkbox','orderable'=>false,'class'=>'text-center'],
            ['data'=>'no','orderable'=>false,'class'=>'text-right'],
            ['data'=>'kodeinventor'],
            ['data'=>'namainventor'],
            ['data'=>'namainventor2'],
            ['data'=>'keterangan'],
            // ['data'=>'idgudang'],
            // ['data'=>'idcompany'],
            // ['data'=>'iddepartment'],
            // ['data'=>'idsatuan'],
            // ['data'=>'konvsatuan'],
            ['data'=>'unitqty','class'=>'text-right'],
            ['data'=>'namasatuan'],
            ['data'=>'unitprice','class'=>'text-right','render'=>"$.fn.dataTable.render.number( ',', '.', 3, 'Rp' )"],
            // ['data'=>'subtotal'],
            // ['data'=>'discountvar'],
            // ['data'=>'discount'],
            // ['data'=>'nilaidisc'],
            // ['data'=>'idtypetax'],
            // ['data'=>'idtax'],
            ['data'=>'hargatotal','class'=>'text-right'],
            ['data'=>'namacurrency'],
            // ['data'=>'idakunhpp'],
            // ['data'=>'idakunsales'],
            // ['data'=>'idakunpersediaan'],
            // ['data'=>'idakunsalesreturn'],
            // ['data'=>'idakunpurchasereturn'],
            // ['data'=>'serialnumber'],
            // ['data'=>'garansitanggal'],
            // ['data'=>'garansiexpired'],
            // ['data'=>'typetrancprev'],
            // ['data'=>'trancprev'],
            // ['data'=>'trancprevd'],
            // ['data'=>'typetrancorder'],
            // ['data'=>'trancorder'],
            // ['data'=>'trancorderd'],
            ['data'=>'action','orderable'=>false],
        ];

        $columndefs = [
            ['targets'=>0,'width'=>'5%'],
            ['targets'=>1,'width'=>'5%'],
            ['targets'=>2,'width'=>'5%'],
            ['targets'=>3,'width'=>'15%'],
            ['targets'=>4,'width'=>'15%'],
            ['targets'=>5,'width'=>'15%'],
            ['targets'=>6,'width'=>'15%'],
            ['targets'=>7,'width'=>'15%'],
            ['targets'=>8,'width'=>'5%'],
            ['targets'=>9,'width'=>'5%'],
            ['targets'=>10,'width'=>'5%'],
            ['targets'=>10,'width'=>'5%']
        ];

        $columndefslog = [
            ['targets'=>0,'width'=>'5%'],
            ['targets'=>1,'width'=>'5%'],
            ['targets'=>2,'width'=>'5%'],
            ['targets'=>3,'width'=>'15%'],
            ['targets'=>4,'width'=>'15%'],
            ['targets'=>5,'width'=>'15%'],
            ['targets'=>6,'width'=>'15%'],
            ['targets'=>7,'width'=>'15%'],
            ['targets'=>8,'width'=>'5%'],
            ['targets'=>9,'width'=>'5%'],
            ['targets'=>10,'width'=>'5%']
        ];

        $columndefscostcontrol = [
            ['targets'=>0,'width'=>'5%'],
            ['targets'=>1,'width'=>'5%'],
            ['targets'=>2,'width'=>'5%'],
            ['targets'=>3,'width'=>'15%'],
            ['targets'=>4,'width'=>'15%'],
            ['targets'=>5,'width'=>'15%'],
            ['targets'=>6,'width'=>'15%'],
            ['targets'=>7,'width'=>'15%'],
            ['targets'=>8,'width'=>'5%'],
            ['targets'=>9,'width'=>'5%'],
            ['targets'=>10,'width'=>'5%'],
            ['targets'=>10,'width'=>'5%']
        ];

        for ($i = 0; $i <= 12; $i++) {
            if ($i <= 8) {
                $columndefsdetail[] = ['target'=>$i];
            }
        }
        
        $columns = json_encode($columns);
        $columndefs = json_encode($columndefs);
        $columnsdetail = json_encode($columnsdetail);
        $columndefsdetail = json_encode($columndefsdetail);
        $columnslog = json_encode($columnslog);
        $columndefslog = json_encode($columndefslog);
        $columnscostcontrol = json_encode($columnscostcontrol);
        $columndefscostcontrol = json_encode($columndefscostcontrol);

        $mtypecostcontrol = MP::getData(['table'=>'mtypecostcontrol','select'=>['noid','kode','nama'],'isactive'=>true,'orderby'=>[['nama','asc']]]);
        $mcardcustomer = MP::getData(['table'=>'mcardcustomer','select'=>['noid','kode','nama'],'isactive'=>true]);
        $mcardsupplier = MP::getMCardSupplier();
        $mtt = MP::findMTypeTranc();
        $mtypetranc_noid = $mtt->noid;
        $mtypetranc_nama = $mtt->nama;
        $mtypetranc_kode = $mtt->kode;
        $mtypetranctypestatus = MP::getMTypeTrancTypeStatus(static::$main['idtypetranc']);

        $btnexternal = [];
        foreach ($mtypetranctypestatus as $k => $v) {
            if ($v->isinternal == 0 && $v->isexternal == 1) {
                $btnexternal[] = [
                    'noid' => $v->noid,
                    'idstatusbase' => $v->idstatusbase,
                    'nama' => $v->nama,
                    'classicon' => $v->classicon,
                    'classcolor' => (new static)->color[$v->classcolor],
                ];
            }
        }

        $conditions = [];
        foreach ($mtypetranctypestatus as $k => $v) {
            if ($k != 0) {
                $updatedropdown[strtolower($v->idstatusbase)] = [
                    'noid' => $v->noid,
                    'kode' => $v->kode,
                    'nama' => $v->nama,
                    'classicon' => $v->classicon,
                    'classcolor' => (new static)->color[$v->classcolor],
                ];
            }
            $conditions[$v->noid] = [
                'noid' => $v->noid,
                'idstatusbase' => $v->idstatusbase,
                'nama' => $v->nama,
                'prev_noid' => @$mtypetranctypestatus[$k-1]->noid ? $mtypetranctypestatus[$k-1]->noid : null,
                'prev_idstatusbase' => @$mtypetranctypestatus[$k-1]->idstatusbase ? $mtypetranctypestatus[$k-1]->idstatusbase : null,
                'prev_nama' => @$mtypetranctypestatus[$k-1]->nama ? $mtypetranctypestatus[$k-1]->nama : null,
                'prev_classicon' => @$mtypetranctypestatus[$k-1]->classicon ? $mtypetranctypestatus[$k-1]->classicon : null,
                'prev_classcolor' => @$mtypetranctypestatus[$k-1]->classcolor ? (new static)->color[$mtypetranctypestatus[$k-1]->classcolor] : null,
                'next_noid' => @$mtypetranctypestatus[$k+1]->noid ? $mtypetranctypestatus[$k+1]->noid : null,
                'next_idstatusbase' => @$mtypetranctypestatus[$k+1]->idstatusbase ? $mtypetranctypestatus[$k+1]->idstatusbase : null,
                'next_nama' => @$mtypetranctypestatus[$k+1]->nama ? $mtypetranctypestatus[$k+1]->nama : null,
                'next_classicon' => @$mtypetranctypestatus[$k+1]->classicon ? $mtypetranctypestatus[$k+1]->classicon : null,
                'next_classcolor' => @$mtypetranctypestatus[$k+1]->classcolor ? (new static)->color[$mtypetranctypestatus[$k+1]->classcolor] : null,
                'next_table' => static::$main['table2-title'],
                'isinternal' => $v->isinternal == 1 ? 1 : 0,
                'isexternal' => $v->isexternal == 1 ? 1 : 0,
                'btnexternal' => $btnexternal,
                'actview' => $v->actview,
                'actedit' => $v->actedit,
                'actdelete' => $v->actdelete,
                'actreportdetail' => $v->actreportdetail,
                'actreportmaster' => $v->actreportmaster,
                'actsetprevstatus' => $v->actsetprevstatus,
                'actsetnextstatus' => $v->actsetnextstatus,
                'actsetstatus' => $v->actsetstatus,
                'appmtypeaction_noid' => $v->appmtypeaction_noid,
                'appmtypeaction_nama' => $v->appmtypeaction_nama
            ];
        }
        Session::put(static::$main['noid'].'-conditions', $conditions);
        $mttts = MP::findMTypeTrancTypeStatus(static::$main['idtypetranc']);
        $mtypetranctypestatus_noid = $mttts->noid;
        $mtypetranctypestatus_nama = $mttts->nama;
        $mtypetranctypestatus_classcolor = $mttts->classcolor;
        $mcard = MP::getData(['table'=>'mcard','select'=>['noid','kode','nama','isemployee','iscustomer','issupplier'],'isactive'=>true,'orderby'=>[['nama','asc']]]);
        $accmkodeakun = MP::getData(['table'=>'accmkodeakun','isactive'=>true]);
        $accmcashbank = MP::getData(['table'=>'accmcashbank','select'=>['noid','kode','nama']]);
        $accmcurrency = MP::getData(['table'=>'accmcurrency','select'=>['noid','kode','nama'],'isactive'=>true]);
        $genmgudang = MP::getData(['table'=>'genmgudang','select'=>['noid','kode','namalias'],'isactive'=>true]);
        $genmcompany = MP::getData(['table'=>'genmcompany','select'=>['noid','kode','nama'],'isactive'=>true]);
        // $genmcompany = MP::getGenMCompany();
        $genmdepartment = MP::getData(['table'=>'genmdepartment','select'=>['noid','kode','nama'],'isactive'=>true]);
        $mtypetranc = MP::getData(['table'=>'mtypetranc','select'=>['noid','kode','nama']]);
        $imi = MP::getInvMInventory();
        foreach ($imi as $k => $v) {
            $invminventory[$k] = $v;
            $invminventory[$k]->purchaseprice2 = 'RP.'.number_format($v->purchaseprice2);
        }
        $invmsatuan = MP::getData(['table'=>'invmsatuan','select'=>['noid','kode','nama','konversi'],'isactive'=>true]);
        $accmpajak = MP::getData(['table'=>'accmpajak','select'=>['noid','kode','nama','prosentax'],'isactive'=>true]);
        $color = (new static)->color;
            $lastdata = MP::getLastMonth();
            $lastdata = $lastdata->_year == null && $lastdata->_month == null ? (object)['_year'=>date('y'),'_month'=>date('m')] : $lastdata;
        $datefilter = [
            'a' => 'All',
            't' => date('M d, Y').' - '.date('M d, Y'),
            'y' => date('M d, Y', strtotime('-1 day')).' - '.date('M d, Y', strtotime('-1 day')),
            'l7d' => date('M d, Y').' - '.date('M d, Y', strtotime('-7 day')),
            'l30d' => date('M d, Y').' - '.date('M d, Y', strtotime('1 month')),
            'tm' => date('M d, Y', strtotime('first day of this month')).' - '.date('M d, Y', strtotime('last day of this month')),
            'lm' => date('M d, Y', strtotime('first day of previous month')).' - '.date('M d, Y', strtotime('last day of previous month')),
        ];
        $datefilterdefault = 'tm';
        $tabletitle = static::$main['table-title'];
        $sendto = static::$main['table2-title'];

        $noidnew = explode('/', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH))[1];
        $triggercreate = $noidnew == static::$main['noidnew'] ? true : false;

        return view('lam_custom_transaksi_master_detail.'.static::$main['table'].'.index', compact(
            'idtypepage',
            'sessionnoid',
            'sessionmyusername',
            'sessionname',
            'sessionidcompany',
            'sessioniddepartment',
            'sessionidgudang',
            'maintable',
            'breadcrumb',
            'pagenoid',
            'defaulttanggal',
            'defaulttanggaldue',
            'data',
            'columns',
            'columndefs',
            'columnsdetail',
            'columndefsdetail',
            'columnslog',
            'columndefslog',
            'columnscostcontrol',
            'columndefscostcontrol',
            'mtypecostcontrol',
            'mcardcustomer',
            'mcardsupplier',
            'mtypetranc_noid',
            'mtypetranc_nama',
            'mtypetranc_kode',
            'mtypetranctypestatus',
            'updatedropdown',
            'mtypetranctypestatus_noid',
            'mtypetranctypestatus_nama',
            'mtypetranctypestatus_classcolor',
            'mcard',
            'accmkodeakun',
            'accmcashbank',
            'accmcurrency',
            'genmgudang',
            'genmcompany',
            'genmdepartment',
            'mtypetranc',
            'invminventory',
            'invmsatuan',
            'accmpajak',
            'color',
            'datefilter',
            'datefilterdefault',
            'tabletitle',
            'sendto',
            'triggercreate'
        ));
    }

    
    public function get(Request $request) {
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->get('search')['value'];
        $orderbycolumn = $request->get('columns')[$request->get('order')[0]['column']]['data'];
        if ($orderbycolumn == 'checkbox' || $orderbycolumn == 'no') {
            $orderbycolumn = 'noid';
        }
        $orderby = [
            'column' => $orderbycolumn,
            'sort' => $request->get('order')[0]['dir']
        ];
        $filter = $request->get('filter');
        $datefilter = $request->get('datefilter');
        $datefilter2 = $request->get('datefilter2');

        $result = MP::getRAP($start, $length, $search, $orderby, $filter, $datefilter, $datefilter2);
        $data = $result['data'];
        $allnoid = MP::getAllNoid(static::$main['table']);

        $resulttotal = count(MP::getRAP(null, null, $search, $orderby, $filter, $datefilter, $datefilter2)['data']);

        $resultsnst = MP::getMTypeTrancTypeStatus(static::$main['idtypetranc']);
        foreach ($resultsnst as $k => $v) {
            $snst[$v->nourut] = $v;
        }

        $conditions = Session::get(static::$main['noid'].'-conditions');
        $no = $start+1;
        foreach ($data as $k => $v) {
            $checkbox['checkbox'] = '<input type="checkbox" value="FALSE" class="checkbox-m" id="checkbox-m-'.$v->noid.'" onchange="selectCheckboxM('.$v->noid.')">';
            $_no['no'] = $no;
            $data[$k]->mtypetranctypestatus_nama = '<span style="padding: 2px; color: white; background: '.(new static)->color[$v->mtypetranctypestatus_classcolor].'">'.$v->mtypetranctypestatus_nama.'</span>';
            $data[$k]->tanggal = $this->dbdateTohumandate($data[$k]->tanggal);
            $data[$k]->tanggaldue = $this->dbdateTohumandate($data[$k]->tanggaldue);
            $data[$k]->totalsubtotal = $v->totalsubtotal;
            $data[$k]->generaltotal = $v->generaltotal;
            $data[$k]->progresstodate = @$v->progresstodate ? $this->dbdateTohumandate($v->progresstodate) : '';
            $data[$k]->progressopname = @$v->progressopname ? $v->progressopname.'%' : '0%';
            $array = (array)$data[$k];

            $btnprev = $conditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase'] && $conditions[$v->mtypetranctypestatus_noid]['isinternal'] 
                ? '<a onclick="showModalSNST('.$v->noid.','.$v->mtypetranc_noid.', '.$snst[$conditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']]->noid.', \''.$snst[$conditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']]->nama.'\')" title="Set to '.$snst[$conditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']]->nama.'" class="btn btn-sm flat" style="background-color: '.(new static)->color[$snst[$conditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']]->classcolor].'"><i class="'.$snst[$conditions[$v->mtypetranctypestatus_noid]['prev_idstatusbase']]->classicon.' icon-xs text-light" style="text-align:center"></i> </a>' 
                : '';

            $btnnext = $conditions[$v->mtypetranctypestatus_noid]['next_idstatusbase'] && $conditions[$v->mtypetranctypestatus_noid]['isinternal'] 
                ? '<a onclick="showModalSNST('.$v->noid.','.$v->mtypetranc_noid.', '.$snst[$conditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->noid.', \''.$snst[$conditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->nama.'\')" title="Set to '.$snst[$conditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->nama.'" class="btn btn-sm flat" style="background-color: '.(new static)->color[$snst[$conditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->classcolor].'"><i class="'.$snst[$conditions[$v->mtypetranctypestatus_noid]['next_idstatusbase']]->classicon.' icon-xs text-light" style="text-align:center"></i> </a>' 
                : '';

            $btnsapcf = $conditions[$v->mtypetranctypestatus_noid]['actsetstatus'] ? '<a onclick="showModalSAPCF({noid:'.$v->noid.',idtypetranc:'.$v->mtypetranc_noid.',idstatustranc:'.$v->mtypetranctypestatus_noid.'})" title="Set Status" class="btn btn-dark btn-sm flat"><i class="icon-cogs icon-xs text-light" style="text-align:center"></i> </a>' : '';
            $btnprint = '<a onclick="printData('.static::$main['noid'].',\''.base64_encode(static::$main['printcode'].'/'.$v->noid.'/'.date('Ymdhis')).'\')" title="Print Data" class="btn btn-sm flat" style="background-color: '.(new static)->color['blue-soft'].'"><i class="fa fa-file icon-xs text-light" style="text-align:center;"></i> </a>';
            $btnlog = '<a onclick="showModalLog('.$v->noid.', \''.$v->kode.'\', \''.$v->mtypetranc_nama.'\')" title="Show Log Data" class="btn btn-sm flat" style="background-color: #8775A7"><i class="icon-file-text icon-xs text-light"></i> </a>';
            $btnview = $conditions[$v->mtypetranctypestatus_noid]['actview'] ? '<a onclick="viewData('.$v->noid.')" title="View Data" class="btn btn-info btn-sm flat"><i class="fa fa-eye icon-xs"></i> </a>' : '';
            $btnedit = $conditions[$v->mtypetranctypestatus_noid]['actedit'] ? '<a onclick="editData('.$v->noid.')" title="Edit Data" class="btn btn-warning btn-sm flat"><i class="fa fa-edit icon-xs" style="text-align:center"></i> </a>' : '';
            $btndelete = $conditions[$v->mtypetranctypestatus_noid]['actdelete'] ? '<a  onclick="showModalDelete('.$v->noid.')" title="Delete Data" class="btn btn-danger btn-sm flat"><i class="fa fa-trash icon-xs"></i> </a>' : '';

            $allbtn = $btnprev;
            $allbtn .= $btnnext;
            $allbtn .= $btnsapcf;
            $allbtn .= $btnprint;
            $allbtn .= $btnlog;
            $allbtn .= $btnview;
            $allbtn .= $btnedit;
            $allbtn .= $btndelete;

            $action = [
                'action' => '<div class="d-flex align-items-center pull-right button-action button-action-'.$k.'">
                    '.$allbtn.'
                </div>'
            ];
            $array['idtypetranc'] = $data[$k]->mtypetranc_nama;
            $array['idstatustranc'] = $data[$k]->mtypetranctypestatus_nama;
            $data[$k] = (object)array_merge($checkbox, $_no, $array, $action);
            $no++;
        }
        
        return response()->json([
            'data' => $data,
            'draw' => intval($request['draw']),
            'recordsFiltered' => $resulttotal,
            'recordsTotal' => $resulttotal,
            'allnoid' => $allnoid,
            'datefilter' => $result['datefilter']
        ]);
    }

    public function get_log(Request $request) {
        $statusadd = $request->get('statusadd');
        $noid = $request->get('noid');
        $kode = $request->get('kode');
        $noidmore = $request->get('noidmore');
        $fieldmore = $request->get('fieldmore');
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->get('search')['value'];
        $orderbycolumn = $request->get('columns')[$request->get('order')[0]['column']]['data'];
        if ($orderbycolumn == 'checkbox' || $orderbycolumn == 'no') {
            $orderbycolumn = 'noid';
        }
        $orderby = [
            'column' => $orderbycolumn,
            'sort' => $request->get('order')[0]['dir']
        ];

        $data = MP::getLog($noid, $kode, $start, $length, $search, $orderby);
        
        if ($search) {
            $resulttotal = count($data);
        } else {
            $resulttotal = count(MP::findLog($noid, $kode));
        }
        // $resulttotal = count($data);

        $no = $start+1;
        foreach ($data as $k => $v) {
            // $checkbox['checkbox'] = '<input type="checkbox" id="checkbox-'.$k.'" onchange="selectCheckbox(\'checkbox-'.$k.'\')">';
            $_no['no'] = $no;
            // $data[$k]->keterangan = substr($v->keterangan, 0, 39).' <button class="btn btn-info btn-sm" onclick="getDatatableLog('.$v->idreff.', \''.$v->kode.'\', '.$v->idreff.', \'keterangan\')">More</button>';
            $data[$k]->logsubjectfull = $v->logsubject;
            $data[$k]->keteranganfull = $v->keterangan;


            // $btnlogsubject = strlen($v->logsubject) <= 44 ? '' : ' <button id="morelog-logsubject-'.$v->noid.'" class="btn btn-success btn-sm" onclick="moreLog('.$v->noid.', \'logsubject\')"><i class="icon-angle-right"></i></button>';
            // $btnketerangan = strlen($v->keterangan) <= 44 ? '' : ' <button id="morelog-keterangan-'.$v->noid.'" class="btn btn-success btn-sm" onclick="moreLog('.$v->noid.', \'keterangan\')"><i class="icon-angle-right"></i></button>';

            $btnlogsubject = strlen($v->logsubject) <= 44 ? '' : '...';
            $btnketerangan = strlen($v->keterangan) <= 44 ? '' : '...';

            $data[$k]->logsubject = substr($v->logsubject, 0, 44).'<span id="logsubject-'.$v->noid.'" style="display: none">'.substr($v->logsubject, 44).'</span>'.$btnlogsubject;
            $data[$k]->keterangan = substr($v->keterangan, 0, 44).'<span id="keterangan-'.$v->noid.'" style="display: none">'.substr($v->keterangan, 44).'</span>'.$btnketerangan;
            $data[$k]->docreate = $this->dbdateTohumandate($data[$k]->docreate);
            $array = (array)$data[$k];
            $data[$k] = (object)array_merge($_no, $array);
            $no++;
        }
        
        return response()->json([
            'data' => $data,
            'draw' => intval($request['draw']),
            'recordsFiltered' => $resulttotal,
            'recordsTotal' => $resulttotal
        ]);
    }

    public function get_costcontrol(Request $request) {
        $statusadd = $request->get('statusadd');
        $statusdetail = $request->get('statusdetail');
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->get('search')['value'];
        $orderbycolumn = $request->get('columns')[$request->get('order')[0]['column']]['data'];
        if ($orderbycolumn == 'checkbox' || $orderbycolumn == 'no') {
            $orderbycolumn = 'noid';
        }
        $orderby = [
            'column' => $orderbycolumn,
            'sort' => $request->get('order')[0]['dir']
        ];

        $data = MP::getCostControl($start, $length, $search, $orderby);
        
        if ($search) {
            $resulttotal = count($data);
        } else {
            $resulttotal = count(MP::getData(['table'=>'salesorder','select'=>['noid'],'isactive'=>true]));
        }

        $no = $start+1;
        foreach ($data as $k => $v) {
            $checkbox['checkbox'] = '<input type="checkbox" id="checkbox-'.$k.'" onchange="selectCheckbox(\'checkbox-'.$k.'\')">';
            $_no['no'] = $no;
            $data[$k]->tanggal = $this->dbdateTohumandate($data[$k]->tanggal);
            $array = (array)$data[$k];
            $action = [
                'action' => $statusdetail == 'true' ? '' : '<div class="d-flex align-items-center text-center button-action button-action-'.$k.'">
                    <a onclick="chooseCostControl('.$v->noid.', \''.$v->kode.'\', '.$v->idtypecostcontrol.')" title="Choose" class="btn btn-success btn-sm flat"><i class="fa fa-hand-o-up icon-xs" style="text-align:center"></i> Choose</a>
                </div>'
            ];
            $data[$k] = (object)array_merge($checkbox, $_no, $array, $action);
            $no++;
        }
        
        return response()->json([
            'data' => $data,
            'draw' => intval($request['draw']),
            'recordsFiltered' => $resulttotal,
            'recordsTotal' => $resulttotal
        ]);
    }

    public function get_detailpi(Request $request) {
        $statusdetail = json_decode($request->get('statusdetail'));
        $idcardsupplier = json_decode($request->get('idcardsupplier'));
        $idcompany = json_decode($request->get('idcompany'));
        $iddepartment = json_decode($request->get('iddepartment'));
        $idgudang = json_decode($request->get('idgudang'));
        $detailprev = json_decode($request->get('detailprev'));
        $unitqtyminpo = json_decode($request->get('unitqtyminpo'));
        $stp = $request->get('searchtypeprev');
        $searchprev = $request->get('searchprev');
        $idelement = $request->get('idelement');
        $search = $request->get('search')['value'];
        $start = $request->get('start');
        $length = $request->get('length');
        $nextnoid = MP::getNextNoid(static::$main['table']);
        $nextnoiddetail = MP::getNextNoid(static::$main['tabledetail']);
        
        $params = [
            'idcardsupplier' => $idcardsupplier,
            'idcompany' => $idcompany,
            'iddepartment' => $iddepartment,
            'idgudang' => $idgudang,
            'detailprev' => $detailprev,
            'searchtypeprev' => $stp,
            'searchprev' => $searchprev,
            'search' => $search,
            'start' => $start,
            'length' => $length,
        ];
        $data = $stp == 1 ? MP::getDetailPrev($params) : MP::getDetailInv($params);

        if ($search) {
            $qty = count($data);
        } else {
            $params['start'] = null;
            $params['length'] = null;
            $qty = count($stp == 1 ? MP::getDetailPrev($params) : MP::getDetailInv($params));
        }

        $no = $start+1;
        $index = 0;
        $result = [];
        $pattern = "/".$search."/i";

        foreach ($data as $k => $v) {
            $v = (array)$v;
            $v['checkbox'] = '<input type="checkbox" id="checkbox-detail-'.$k.'" onchange="selectCheckbox(\'checkbox-detail-'.$k.'\')">';
            $v['no'] = $no;
            if ($stp == 2) {
                $v['noid'] = $nextnoid;
                $v['idmaster'] = '-';
                $v['kodeprev'] = '-';
                $v['namainventor2'] = '-';
                $v['keterangan'] = '-';
                $v['unitqty'] = '-';
                $v['unitqtysisa'] = '-';
                $v['subtotal'] = '-';
                $v['discountvar'] = '-';
                $v['discount'] = '-';
                $v['nilaidisc'] = '-';
                $v['idtypetax'] = '-';
                $v['idtax'] = '-';
                $v['prosentax'] = '-';
                $v['nilaitax'] = '-';
                $v['hargatotal'] = '-';
                $v['namacurrency'] = '-';
                $v['idcompany'] = $idcompany;
                $v['iddepartment'] = $iddepartment;
                $v['idgudang'] = $idgudang;
            } 

            $isaction = true;
            foreach ($detailprev as $k2 => $v2) {
                if ($v2->noid == $v['noid']) {
                    if ($stp == 1) {
                        $v['unitqtysisa'] = $v['unitqtysisa']-$v2->unitqty < 0 ? 0 : $v['unitqtysisa']-$v2->unitqty;
                        $isaction = false;
                        break;
                    }
                }
            }

            $v['action'] = $isaction ? '<div class="d-flex align-items-center text-center button-action-dprev button-action-dprev-'.$k.'">
                <a onclick="chooseDetailPrev({
                    idelement: \''.$idelement.'\',
                    data: {
                        noid: '.$v['noid'].',
                        idmaster: \''.$v['idmaster'].'\',
                        kodeprev: \''.$v['kodeprev'].'\',
                        idinventor: \''.$v['idinventor'].'\',
                        kodeinventor: \''.$v['kodeinventor'].'\',
                        namainventor: \''.$v['namainventor'].'\',
                        namainventor2: \''.$v['namainventor2'].'\',
                        keterangan: \''.$v['keterangan'].'\',
                        idgudang: \''.$v['idgudang'].'\',
                        idcompany: \''.$v['idcompany'].'\',
                        iddepartment: \''.$v['iddepartment'].'\',
                        idsatuan: \''.$v['idsatuan'].'\',
                        namasatuan: \''.$v['namasatuan'].'\',
                        konvsatuan: \''.$v['konvsatuan'].'\',
                        unitqty: \''.$v['unitqty'].'\',
                        unitqtysisa: \''.$v['unitqtysisa'].'\',
                        unitprice: \''.$v['unitprice'].'\',
                        subtotal: \''.$v['subtotal'].'\',
                        discountvar: \''.$v['discountvar'].'\',
                        discount: \''.$v['discount'].'\',
                        nilaidisc: \''.$v['nilaidisc'].'\',
                        idtypetax: \''.$v['idtypetax'].'\',
                        idtax: \''.$v['idtax'].'\',
                        prosentax: \''.$v['prosentax'].'\',
                        nilaitax: \''.$v['nilaitax'].'\',
                        hargatotal: \''.$v['hargatotal'].'\',
                        namacurrency: \''.$v['namacurrency'].'\'
                    }
                })" title="Choose" class="btn btn-success btn-sm flat"><i class="icon-hand-up icon-xs" style="text-align:center"></i> Choose</a>
            </div>' : '';

                $result[] = $v;
            $no++;
            $nextnoid;
        }

        return response()->json([
            'data' => $result,
            'draw' => intval($request['draw']),
            'recordsFiltered' => $qty,
            'recordsTotal' => $qty,
            'length' => $request->get('length'),
            'idelement' => $request->get('idelement'),
        ]);
    }

    public function get_milestone_and_header(Request $request) {
        $idmaster = $request->get('idmaster');
        $data = json_decode($request->get('data'));

        $response = MP::getMilestoneAndHeader(['idmaster'=>$idmaster]);
        // $response['milestone'] = ($data) ? $data : $response['milestone'];
        
        if ($data) {
            $milestone = [];
            $idxh = 0;
            foreach ($data as $k => $v) {
                $header = [];
                foreach ($v->header as $k2 => $v2) {
                    $header[$k2] = $v2;
                    $header[$k2]->idmilestone = $v->noid;
                    $header[$k2]->idelement = 'header-'.$idxh;
                    $idxh++;
                }
                $milestone[$k] = $v;
                $milestone[$k]->header = $header;
                $milestone[$k]->idelement = 'milestone-'.$k;
            }
            $response['list']['milestone'] = $milestone;
            $response['milestone'] = $milestone;
        } else {
            $response['list']['milestone'] = [];
            $response['milestone'] = [];
        }

        return response()->json([
            'success' => true,
            'message' => 'Get data successfully.',
            'data' => $response
        ]);
    }
    
    public function get_milestone(Request $request) {
        $data = json_decode($request->get('data'));
        $statusdetail = json_decode($request->get('statusdetail'));
        $condition = json_decode($request->get('condition'));
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->get('search')['value'];

        $qtyshow = 0;
        $qtyall = 0;
        $no = 1;
        $index = 0;
        $result = [];
        $pattern = "/".$search."/i";
        // dd($start);

        foreach ($data as $k => $v) {
            if ($v != null) {
                if ($k >= (int)$start && $qtyshow <= $length) {
                    $insertrecord = false;
                    // foreach ($v as $k2 => $v2) {
                    //     if ($v2->name != 'idinventor') {
                    //         if (preg_match($pattern, $v2->value)) {
                    //             $insertrecord = true;
                    //         }
                    //         if ($v2->name == 'subtotal') {
                    //             $result[$index][$v2->name] = is_numeric($v2->value) ? 'Rp.'.number_format($v2->value) : $v2->value;
                    //         } else if ($v2->name == 'unitprice' || $v2->name == 'hargatotal') {
                    //             $result[$index][$v2->name] = filter_var($v2->value, FILTER_SANITIZE_NUMBER_INT);
                    //         } else {
                    //             $result[$index][$v2->name] = $v2->value;
                    //         }
                    //     }
                    // }
                    $result[$index] = (array)$v;
                    // if (!$insertrecord) {
                    //     array_pop($result);
                    //     continue;
                    // }
                    $result[$index]['checkbox'] = '<input type="checkbox" id="checkbox-detail-'.$k.'" onchange="selectCheckbox(\'checkbox-detail-'.$k.'\')">';
                    $result[$index]['no'] = $no;

                    $allbtn = $condition->v ? '<a onclick="editMilestone({keymilestone:\''.$k.'\'})"  class="btn btn-success btn-sm flat mr-2"><i class="fa fa-search icon-xs"></i> </a>' : '';
                    $allbtn .= $condition->u ? '<a onclick="editMilestone({keymilestone:\''.$k.'\'})" class="btn btn-warning btn-sm flat mr-1"><i class="fa fa-edit icon-xs" style="text-align:center"></i> </a>': '';
                    $allbtn .= $condition->d ? '<a  onclick="removeMilestone({keymilestone:\''.$k.'\'})" class="btn btn-danger btn-sm flat mr-0"><i class="fa fa-trash icon-xs"></i> </a>' : '';
    
                    $result[$index]['action'] = '<div class="text-right button-action-tab button-action-tab-'.$k.'">
                        '.$allbtn.'
                    </div>';
                    if (count($result) == $length || count($data) == $k+1) {
                        break;
                    }
                    $index++;
                    $qtyshow++;
                }
                $no++;
            }
        }

        return response()->json([
            'data' => $result,
            'draw' => intval($request['draw']),
            'recordsFiltered' => $search ? count($result) : count($data),
            'recordsTotal' => $search ? count($result) : count($data),
            'length' => $request->get('length')
        ]);
    }

    public function get_header(Request $request) {
        $idmilestone = $request->get('idmilestone');
        $idmaster = $request->get('idmaster');
        $milestone = (array)json_decode($request->get('milestone'));
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->get('search')['value'];
        $condition = json_decode($request->get('condition'));

        $params = [
            'idmilestone' => $idmilestone,
            'idmaster' => $idmaster,
        ];
        
        $result = [];

        foreach ($milestone as $k => $v) {
            if ($v->noid == $idmilestone) {
                $result = $v->header;
            }
        }

        $data = [];
        $no = 1;
        foreach ($result as $k => $v) {
            $data[$k] = $v;
            $data[$k]->checkbox = '<input type="checkbox" id="checkbox-detail-'.$k.'" onchange="selectCheckbox(\'checkbox-detail-'.$k.'\')">';
            $data[$k]->no = $no;

            $allbtn = $condition->v ? '<a onclick="editHeader({keyheader:\''.$k.'\',idmilestone:\''.$v->idmilestone.'\'})"  class="btn btn-success btn-sm flat mr-2"><i class="fa fa-search icon-xs"></i> </a>' : '';
            $allbtn .= $condition->u ? '<a onclick="editHeader({keyheader:\''.$k.'\',idmilestone:\''.$v->idmilestone.'\'})" class="btn btn-warning btn-sm flat mr-1"><i class="fa fa-edit icon-xs" style="text-align:center"></i> </a>': '';
            $allbtn .= $condition->d ? '<a  onclick="removeHeader({keyheader:\''.$k.'\',idmilestone:\''.$v->idmilestone.'\'})" class="btn btn-danger btn-sm flat mr-0"><i class="fa fa-trash icon-xs"></i> </a>' : '';

            $data[$k]->action = '<div class="text-right button-action-tab button-action-tab-'.$k.'">
                '.$allbtn.'
            </div>';
        }
        
        return response()->json([
            'data' => $data,
            'draw' => intval($request['draw']),
            'recordsFiltered' => $search ? count($result) : count($result),
            'recordsTotal' => $search ? count($result) : count($result),
            'length' => $request->get('length')
        ]);
    }

    public function get_detail(Request $request) {
        $idelement = $request->get('idelement');
        $idheader = $request->get('idheader');
        $idmaster = $request->get('idmaster');
        $milestone = (array)json_decode($request->get('milestone'));
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->get('search')['value'];
        $field = $request->get('columns')[$request->get('order')[0]['column']]['data'];
        $sort = $request->get('order')[0]['dir'];
        $condition = json_decode($request->get('condition'));

        $params = [
            'idheader' => $idheader,
            'idmaster' => $idmaster,
        ];
        
        $result = [];

        foreach ($milestone as $k => $v) {
            foreach ($v->header as $k2 => $v2) {
                if ($v2->noid == $idheader) {
                    foreach ($v2->detail as $k3 => $v3) {
                        $v3->idmilestone = $v->noid;
                        $result[] = $v3;
                    }
                }
            }
        }
        
        usort($result, function($a, $b) use ($field, $sort){
            if ($field == 'checkbox') {
                return $a->namainventor > $b->namainventor;
            } else {
                if (strtolower($sort) == 'asc') {
                    return strtolower($a->$field) > strtolower($b->$field);
                } else {
                    return strtolower($a->$field) < strtolower($b->$field);
                }
            }
        });

        $data = [];
        $no = 1;
        $qtydata = 0;
        $key = 0;

        if ($search) {
            foreach ($result as $k => $v) {
                if (strstr(strtolower($v->kodeinventor),strtolower($search)) ||
                    strstr(strtolower($v->namainventor),strtolower($search)) ||
                    strstr(strtolower($v->namainventor2),strtolower($search)) ||
                    strstr(strtolower($v->keterangan),strtolower($search)) ||
                    strstr(strtolower($v->unitqty),strtolower($search)) ||
                    strstr(strtolower($v->unitqtysisa),strtolower($search)) ||
                    strstr(strtolower($v->namasatuan),strtolower($search)) ||
                    strstr(strtolower($v->unitprice),strtolower($search)) ||
                    strstr(strtolower($v->hargatotal),strtolower($search)) ||
                    strstr(strtolower($v->namasatuan),strtolower($search))) {
                    if ($start <= $k) {
                        $qtydata++;
                        $data[$key] = $v;
                        $data[$key]->collapse = '';
                        $data[$key]->checkbox = '<input type="checkbox" id="checkbox-detail-'.$key.'" onchange="selectCheckbox(\'checkbox-detail-'.$key.'\')">';
                        $data[$key]->no = $no;
                        $data[$key]->kodeprev = '-';
                        $data[$key]->namacurrency = '-';
        
                        $allbtn = $condition->v ? '<a onclick="editDetail({keydetail:\''.$key.'\',idheader:\''.$v->idheader.'\',idmilestone:\''.$v->idmilestone.'\'})"  class="btn btn-success btn-sm flat mr-2"><i class="fa fa-search icon-xs"></i> </a>' : '';
                        $allbtn .= $condition->u ? '<a onclick="editDetail({keydetail:\''.$key.'\',idheader:\''.$v->idheader.'\',idmilestone:\''.$v->idmilestone.'\'})" class="btn btn-warning btn-sm flat mr-1"><i class="fa fa-edit icon-xs" style="text-align:center"></i> </a>': '';
                        $allbtn .= $condition->d ? '<a  onclick="removeDetail({keydetail:\''.$key.'\',idheader:\''.$v->idheader.'\',idmilestone:\''.$v->idmilestone.'\'})" class="btn btn-danger btn-sm flat mr-0"><i class="fa fa-trash icon-xs"></i> </a>' : '';
        
                        $data[$key]->action = '<div class="text-right button-action-tab button-action-tab-'.$key.'-'.$idelement.'">
                            '.$allbtn.'
                        </div>';
                        if ($length == $qtydata) {
                            break;
                        }
                        $key++;
                    }
                    $no++;
                }
            }
        } else {
            foreach ($result as $k => $v) {
                if ($start <= $k) {
                    $qtydata++;
                    $data[$key] = $v;
                    $data[$key]->collapse = '';
                    $data[$key]->checkbox = '<input type="checkbox" id="checkbox-detail-'.$key.'" onchange="selectCheckbox(\'checkbox-detail-'.$key.'\')">';
                    $data[$key]->no = $no;
                    $data[$key]->kodeprev = '-';
                    $data[$key]->namacurrency = '-';

                    $allbtn = $condition->v ? '<a onclick="editDetail({keydetail:\''.$key.'\',idheader:\''.$v->idheader.'\',idmilestone:\''.$v->idmilestone.'\'})"  class="btn btn-success btn-sm flat mr-2"><i class="fa fa-search icon-xs"></i> </a>' : '';
                    $allbtn .= $condition->u ? '<a onclick="editDetail({keydetail:\''.$key.'\',idheader:\''.$v->idheader.'\',idmilestone:\''.$v->idmilestone.'\'})" class="btn btn-warning btn-sm flat mr-1"><i class="fa fa-edit icon-xs" style="text-align:center"></i> </a>': '';
                    $allbtn .= $condition->d ? '<a  onclick="removeDetail({keydetail:\''.$key.'\',idheader:\''.$v->idheader.'\',idmilestone:\''.$v->idmilestone.'\'})" class="btn btn-danger btn-sm flat mr-0"><i class="fa fa-trash icon-xs"></i> </a>' : '';

                    $data[$key]->action = '<div class="text-right button-action-tab button-action-tab-'.$key.'-'.$idelement.'">
                        '.$allbtn.'
                    </div>';
                    if ($length == $qtydata) {
                        break;
                    }
                    $key++;
                }
                $no++;
            }
        }

        return response()->json([
            'data' => $data,
            'draw' => intval($request['draw']),
            'recordsFiltered' => $search ? count($result) : count($result),
            'recordsTotal' => $search ? count($result) : count($result),
            'length' => $request->get('length')
        ]);
    }

    public function get_detail_backup(Request $request) {
        $data = json_decode($request->get('data'));
        $statusdetail = json_decode($request->get('statusdetail'));
        $condition = json_decode($request->get('condition'));
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->get('search')['value'];

        $qtyshow = 0;
        $qtyall = 0;
        $no = 1;
        $index = 0;
        $result = [];
        $pattern = "/".$search."/i";
        // dd($start);

        foreach ($data as $k => $v) {
            if ($v != null) {
                if ($k >= (int)$start && $qtyshow <= $length) {
                    $insertrecord = false;
                    foreach ($v as $k2 => $v2) {
                        if ($v2->name != 'idinventor') {
                            if (preg_match($pattern, $v2->value)) {
                                $insertrecord = true;
                            }
                            if ($v2->name == 'subtotal') {
                                $result[$index][$v2->name] = is_numeric($v2->value) ? 'Rp.'.number_format($v2->value) : $v2->value;
                            } else if ($v2->name == 'unitprice' || $v2->name == 'hargatotal') {
                                $result[$index][$v2->name] = filter_var($v2->value, FILTER_SANITIZE_NUMBER_INT);
                            } else {
                                $result[$index][$v2->name] = $v2->value;
                            }
                        }
                    }
                    if (!$insertrecord) {
                        array_pop($result);
                        continue;
                    }
                    $result[$index]['checkbox'] = '<input type="checkbox" id="checkbox-detail-'.$k.'" onchange="selectCheckbox(\'checkbox-detail-'.$k.'\')">';
                    $result[$index]['no'] = $no;

                    $allbtn = $condition->v ? '<a onclick="editDetail(\''.$k.'\')"  class="btn btn-success btn-sm flat mr-2"><i class="fa fa-search icon-xs"></i> </a>' : '';
                    $allbtn .= $condition->u ? '<a onclick="editDetail(\''.$k.'\')" class="btn btn-warning btn-sm flat mr-1"><i class="fa fa-edit icon-xs" style="text-align:center"></i> </a>': '';
                    $allbtn .= $condition->d ? '<a  onclick="removeDetail(\''.$k.'\')" class="btn btn-danger btn-sm flat mr-0"><i class="fa fa-trash icon-xs"></i> </a>' : '';
    
                    $result[$index]['action'] = '<div class="text-right button-action-tab button-action-tab-'.$k.'">
                        '.$allbtn.'
                    </div>';
                    if (count($result) == $length || count($data) == $k+1) {
                        break;
                    }
                    $index++;
                    $qtyshow++;
                }
                $no++;
            }
        }

        return response()->json([
            'data' => $result,
            'draw' => intval($request['draw']),
            'recordsFiltered' => $search ? count($result) : count($data),
            'recordsTotal' => $search ? count($result) : count($data),
            'length' => $request->get('length')
        ]);
    }

    private function humandateTodbdate($date) {
        if ($date) {
            $arrdate = explode('-', $date);
            $arrtime = explode(' ', $date);
            $year = explode(' ',$arrdate[2])[0];
            $month = $arrdate[1];
            $day = $arrdate[0];
            $time = count(explode(' ',$arrdate[2])) == 2 ? explode(' ',$arrdate[2])[1] : '';

            if ($month == 'Jan') {
                $month = '01';
            } else if ($month == 'Feb') {
                $month = '02';
            } else if ($month == 'Mar') {
                $month = '03';
            } else if ($month == 'Apr') {
                $month = '04';
            } else if ($month == 'May') {
                $month = '05';
            } else if ($month == 'Jun') {
                $month = '06';
            } else if ($month == 'Jul') {
                $month = '07';
            } else if ($month == 'Aug') {
                $month = '08';
            } else if ($month == 'Sep') {
                $month = '09';
            } else if ($month == 'Oct') {
                $month = '10';
            } else if ($month == 'Nov') {
                $month = '11';
            } else if ($month == 'Dec') {
                $month = '12';
            }

            return $year.'-'.$month.'-'.$day.' '.$time;
        } else {
            return NULL;
        }
    }

    private function dbdateTohumandate($date) {
        if ($date) {
            $arrdate = explode('-', $date);
            $arrtime = explode(' ', $date);
            $year = $arrdate[0];
            $month = $arrdate[1];
            $day = explode(' ',$arrdate[2])[0];

            if ($month == '01') {
                $month = 'Jan';
            } else if ($month == '02') {
                $month = 'Feb';
            } else if ($month == '03') {
                $month = 'Mar';
            } else if ($month == '04') {
                $month = 'Apr';
            } else if ($month == '05') {
                $month = 'May';
            } else if ($month == '06') {
                $month = 'Jun';
            } else if ($month == '07') {
                $month = 'Jul';
            } else if ($month == '08') {
                $month = 'Aug';
            } else if ($month == '09') {
                $month = 'Sep';
            } else if ($month == '10') {
                $month = 'Oct';
            } else if ($month == '11') {
                $month = 'Nov';
            } else if ($month == '12') {
                $month = 'Dec';
            }

            return count($arrtime) > 1 ? $day.'-'.$month.'-'.$year.' '.$arrtime[1] : $day.'-'.$month.'-'.$year;
        } else {
            return NULL;
        }
    }

    public function save(Request $request) {
        $cdf_checks = json_decode($request->get('cdf_checks'));
        $cdf_selecteds = (array)json_decode($request->get('cdf_selecteds'));
        $statusadd = json_decode($request->get('statusadd'));
        $next_idstatustranc = json_decode($request->get('next_idstatustranc'));
        $next_nama = json_decode($request->get('next_nama'));
        $next_keterangan = json_decode($request->get('next_keterangan'));
        $noid = $request->get('noid');
        $nextnoid = MP::getNextNoid(static::$main['table']);
        $nextnoiddetail = MP::getNextNoid(static::$main['tabledetail']);
        $nextnoidcdf = MP::getNextNoid(static::$main['tablecdf']);
        $milestone = json_decode($request->get('milestone'));

        foreach ($request->get('formheader') as $k => $v) {
            if ($k == 'kodetypetranc') {
                continue;
            } else if ($k == 'idcostcontrol') {
                $formheader[$k] = $v ? $v : 0;
            } else if ($k == 'dostatus' || $k == 'tanggal' || $k == 'tanggaldue' || $k == 'progresstodate') {
                $formheader[$k] = $this->humandateTodbdate($v);
            } else if ($k == 'idstatustranc') {
                $formheader[$k] = $next_idstatustranc ? $next_idstatustranc : 6051;
            } else {
                $formheader[$k] = $v;
            }
        }
        foreach ($request->get('formmodaltanggal') as $k => $v) {
            $formmodaltanggal[$k] = $this->humandateTodbdate($v);
        }
        foreach ($request->get('formfooter') as $k => $v) {
            if ($k == 'subtotal') {
                $formfooter['totalsubtotal'] = filter_var($v, FILTER_SANITIZE_NUMBER_INT);
            } else if ($k == 'total') {
                $formfooter['generaltotal'] = filter_var($v, FILTER_SANITIZE_NUMBER_INT);
            } else {
                $formfooter[$k] = $v;
            }
        }
        $input = [
            'formheader' => $formheader,
            'forminternal' => $request->get('forminternal'),
            'formfooter' => $formfooter,
            'formmodaltanggal' => $formmodaltanggal,
            'prd' => $request->get('purchasedetail')
        ];

        $addorupdate = $statusadd
            ? ['idcreate' => Session::get('noid'), 'docreate' => date('Y-m-d H:i:s')]
            : ['idupdate' => Session::get('noid'), 'lastupdate' => date('Y-m-d H:i:s')];

        $table = array_merge(
            [
                'noid' => $statusadd ? $nextnoid : $noid,
                'idtypetranc' => 0,
                'idcardopname' => null,
                'progresstodate' => null,
                'progressopname' => null,
            ],
            $addorupdate,
            $input['formheader'],
            $input['forminternal'],
            $input['formmodaltanggal'],
            $input['formfooter']
        );

        $tabledetail = [];
        if ($input['prd'] != null) {
            foreach ($input['prd'] as $k => $v) {
                $tabledetail[$k]['noid'] = $nextnoiddetail;
                $tabledetail[$k]['idmaster'] = $statusadd ? $nextnoid : $noid;
                foreach ($v as $k2 => $v2) {
                    if ($v2['name'] == 'noid' || $v2['name'] == 'kodeinventor' || $v2['name'] == 'namainventor' || $v2['name'] == 'idcurrency' || $v2['name'] == 'namacurrency' || $v2['name'] == 'namasatuan') {
                        continue;
                    } else if ($v2['name'] == 'namainventor2') {
                        $tabledetail[$k]['namainventor'] = $v2['value'];
                    } else if ($v2['name'] == 'unitprice' || $v2['name'] == 'subtotal' || $v2['name'] == 'hargatotal') {
                        $tabledetail[$k][$v2['name']] = filter_var($v2['value'], FILTER_SANITIZE_NUMBER_INT);
                    } else if ($v2['name'] == 'discountvar') {
                        $tabledetail[$k][$v2['name']] = $v2['value'] ? $v2['value'] : 0;
                    } else {
                        $tabledetail[$k][$v2['name']] = $v2['value'];
                    }
                }
                $nextnoiddetail++;
            }
        }

        $formcdf = (array)$request->get('formcdf');
        $cdf = [];
        if (count($cdf_checks) > 0) {
            foreach ($cdf_checks as $k => $v) {
                $cdf[$k] = [
                    'noid' => $nextnoidcdf,
                    'idrap' => $statusadd ? $nextnoid : $noid,
                    'idstatusfile' => 3,
                    'idcardstatus' => 1001,
                    'dostatus' => date('Y-m-d H:i:s'),
                    'kode' => '',
                    'nama' => $formcdf['cdf_detail_'.$k.'_nama'],
                    'namaalias' => $cdf_selecteds['cdf-'.$v]->nama,
                    'keterangan' => $formcdf['cdf_detail_'.$k.'_keterangan'],
                    'iddmsfile' => $cdf_selecteds['cdf-'.$v]->noid,
                    'idcreate' => Session::get('noid'),
                    'docreate' => date('Y-m-d H:i:s'),
                    'idupdate' => Session::get('noid'),
                    'lastupdate' => date('Y-m-d H:i:s')
                ];
                $nextnoidcdf++;
            }
        }

        $formdmilestone = [];
        $formdheader = [];
        $formddetail = [];
        $idxh = 0;
        $idxd = 0;
        $totalprogressopnamemaster = 0;
        $qtyprogressopnamemaster = 0;
        $progressopnamemaster = 0;
        foreach ($milestone as $k => $v) {
            $totalqty = 0;
            $totalitem = 0;
            $totalsubtotal = 0;
            $generaltotal = 0;
            if (array_key_exists('header',(array)$v) > 0) {
                foreach ($v->header as $k2 => $v2) {
                    $formdheader[$idxh]['noid'] = $v2->noid;
                    @$v2->fakenoid && $formdheader[$idxh]['fakenoid'] = $v2->fakenoid;
                    $formdheader[$idxh]['idmaster'] = $v2->idmaster;
                    $formdheader[$idxh]['idmilestone'] = $v2->idmilestone;
                    $formdheader[$idxh]['nama'] = $v2->nama;
                    $formdheader[$idxh]['nourut'] = $v2->nourut;
                    $formdheader[$idxh]['keterangan'] = $v2->keterangan;
                    $formdheader[$idxh]['tanggalstart'] = $this->humandateTodbdate($v2->tanggalstart);
                    $formdheader[$idxh]['tanggalstop'] = $this->humandateTodbdate($v2->tanggalstop);
                    $formdheader[$idxh]['tanggalfinished'] = @$v2->tanggalfinished
                        ? $this->humandateTodbdate($v2->tanggalfinished)
                        : null;
                    $formdheader[$idxh]['idcardopname'] = @$v2->idcardopname
                        ? $v2->idcardopname
                        : null;
                    $formdheader[$idxh]['progresstodate'] = @$v2->progresstodate
                        ? $this->humandateTodbdate($v2->progresstodate)
                        : null;
                    $formdheader[$idxh]['progressopname'] = @$v2->progressopname
                        ? $v2->progressopname
                        : null;
                    $formdheader[$idxh]['iscollapse'] = $v2->iscollapse;
                    $formdheader[$idxh]['idcardpj'] = $v2->idcardpj;
                    $formdheader[$idxh]['idcardpj_nama'] = $v2->idcardpj_nama;
                    $formdheader[$idxh]['idcardpjwakil'] = $v2->idcardpjwakil;
                    $formdheader[$idxh]['totalqty'] = $totalqty;
                    $formdheader[$idxh]['totalitem'] = $totalitem;
                    $formdheader[$idxh]['totalsubtotal'] = $totalsubtotal;
                    $formdheader[$idxh]['generaltotal'] = $generaltotal;
                    $formdheader[$idxh]['idcreate'] = Session::get('noid');
                    $formdheader[$idxh]['docreate'] = date('Y-m-d H:i:s');
                    $formdheader[$idxh] = (object)$formdheader[$idxh];
                    $idxh++;
                }
            } else {
                $formdheader[$idxh] = [];
            }

            $formdmilestone[$k]['noid'] = $v->noid;
            @$v->fakenoid && $formdmilestone[$k]['fakenoid'] = $v->fakenoid;
            $formdmilestone[$k]['idmaster'] = $v->idmaster;
            $formdmilestone[$k]['nama'] = $v->nama;
            $formdmilestone[$k]['nourut'] = $v->nourut;
            $formdmilestone[$k]['keterangan'] = $v->keterangan;
            $formdmilestone[$k]['tanggalstart'] = $this->humandateTodbdate($v->tanggalstart);
            $formdmilestone[$k]['tanggalstop'] = $this->humandateTodbdate($v->tanggalstop);
            $formdmilestone[$k]['tanggalfinished'] = @$v->tanggalfinished
                ? $this->humandateTodbdate($v->tanggalfinished)
                : null;
            $formdmilestone[$k]['idcardopname'] = @$v->idcardopname
                ? $v->idcardopname
                : null;
            $formdmilestone[$k]['progresstodate'] = @$v->progresstodate
                ? $this->humandateTodbdate($v->progresstodate)
                : null;
            $formdmilestone[$k]['progressopname'] = @$v->progressopname
                ? $v->progressopname
                : null;
            $formdmilestone[$k]['iscollapse'] = $v->iscollapse;
            $formdmilestone[$k]['idcardpj'] = $v->idcardpj;
            $formdmilestone[$k]['idcardpj_nama'] = $v->idcardpj_nama;
            $formdmilestone[$k]['idcardpjwakil'] = $v->idcardpjwakil;
            $formdmilestone[$k]['totalqty'] = $totalqty;
            $formdmilestone[$k]['totalitem'] = $totalitem;
            $formdmilestone[$k]['totalsubtotal'] = $totalsubtotal;
            $formdmilestone[$k]['generaltotal'] = $generaltotal;
            $formdmilestone[$k]['idcreate'] = Session::get('noid');
            $formdmilestone[$k]['docreate'] = date('Y-m-d H:i:s');
            $formdmilestone[$k] = (object)$formdmilestone[$k];
            
            $idmilestones[] = $v->noid;

            $totalprogressopnamemaster += @$v->progressopname ? $v->progressopname : 0;
            $qtyprogressopnamemaster++;
        }
        $progressopname = $totalprogressopnamemaster/$qtyprogressopnamemaster;
        // $table['progressopname'] = $progressopname;

        $data = [
            'statusadd' => $statusadd,
            'noid' => $noid,
            'master' => $table,
            'milestone' => $formdmilestone,
            'header' => $formdheader,
            'cdf' => $cdf,
        ];

        if ($statusadd) {
            $savetonext = $next_idstatustranc ? ' (Save to '.$next_nama.')' : '';
            $logsubject = 'Data '.$request->get('formheader')['kodetypetranc'].' Kode = '.$data['master']['kode'].' CREATE'.$savetonext;
            if ($next_idstatustranc) {
                $keterangan = $next_keterangan;
            } else {
                $keterangan = 'Data '.$request->get('formheader')['kodetypetranc'].' Kode = '.$data['master']['kode'].' CREATE'.$savetonext.' BY '.Session::get('myusername').' ON '.date('Y-m-d H:i:s');
            }
        } else {
            $savetonext = $next_idstatustranc ? ' (Edit to '.$next_nama.')' : '';
            $logsubject = 'Data '.$request->get('formheader')['kodetypetranc'].' Kode = '.$data['master']['kode'].' EDIT'.$savetonext;
            if ($next_idstatustranc) {
                $keterangan = $next_keterangan;
            } else {
                $keterangan = 'Data '.$request->get('formheader')['kodetypetranc'].' Kode = '.$data['master']['kode'].' EDIT'.$savetonext.' BY '.Session::get('myusername').' ON '.date('Y-m-d H:i:s');
            }
        }

        $datalog = [
            'idtypetranc' => $data['master']['idtypetranc'],
            'idtypeaction' => $statusadd ? 1 : 2,
            'logsubject' => $logsubject,
            'keterangan' => $keterangan,
            'idreff' => $data['master']['noid'],
            'idreffd' => 0,
            'iddevice' => 0,
            'devicetoken' => $request->get('_token'),
            'idcreate' => $statusadd ? $data['master']['idcreate'] : $data['master']['idupdate'],
            'docreate' => $statusadd ? $data['master']['docreate'] : $data['master']['lastupdate'],
        ];
        
        $data['log'] = $datalog;
        
        if ($statusadd) {
            // Declare variable
            $_tablemaster = static::$main['table'];
            $_tablemilestone = static::$main['tablemilestone'];
            $_tableheader = static::$main['tableheader'];
            $_tabledetail = static::$main['tabledetail'];
            $_tableso = 'salesorder';
            $_tablesodetail = 'salesorderdetail';
            $_datamaster = $data['master'];
            $_datamilestone = [];
            $_dataheader = [];
            $_datadetail = [];
            $_dataso = $data['salesorder'];
            $_datasodetail = [];
            $_nextnoidmaster = MP::getNextNoid($_tablemaster);
            $_nextnoidmilestone = MP::getNextNoid($_tablemilestone);
            $_nextnoidheader = MP::getNextNoid($_tableheader);
            $_nextnoiddetail = MP::getNextNoid($_tabledetail);
            $_nextnoidsodetail = MP::getNextNoid($_tablesodetail);
            $_changeidmilestone = [];
            $_changeidheader = [];
    
            // Make data milestone & sales order detail
            foreach ($data['milestone'] as $k => $v) {
                $_changeidmilestone[$v->noid] = $_nextnoidmilestone;
                $_datamilestone[$k] = (array)$v;
                $_datamilestone[$k]['noid'] = $_nextnoidmilestone;
                $_datamilestone[$k]['idmaster'] = $_nextnoidmaster;
    
                $_datasodetail[$k]['noid'] = $_nextnoidsodetail;
                $_datasodetail[$k]['idmaster'] = $_dataso['noid'];
                $_datasodetail[$k]['idinventor'] = $v->idinventor;
                $_datasodetail[$k]['namainventor'] = $v->nama;
                $_datasodetail[$k]['keterangan'] = $v->keterangan;
                $_datasodetail[$k]['idgudang'] = $_datamaster['idgudang'];
                $_datasodetail[$k]['idcompany'] = 0;
                $_datasodetail[$k]['iddepartment'] = 0;
                $_datasodetail[$k]['idsatuan'] = 0;
                $_datasodetail[$k]['konvsatuan'] = 0;
                $_datasodetail[$k]['unitqty'] = 0;
                $_datasodetail[$k]['unitprice'] = 0;
                $_datasodetail[$k]['subtotal'] = 0;
                $_datasodetail[$k]['discountvar'] = 0;
                $_datasodetail[$k]['discount'] = 0;
                $_datasodetail[$k]['nilaidisc'] = 0;
                $_datasodetail[$k]['idtypetax'] = 0;
                $_datasodetail[$k]['idtax'] = 0;
                $_datasodetail[$k]['prosentax'] = 0;
                $_datasodetail[$k]['nilaitax'] = 0;
                $_datasodetail[$k]['hargatotal'] = 0;
                $_datasodetail[$k]['hargapokok'] = 0;
                $_datasodetail[$k]['idakunhpp'] = 0;
                $_datasodetail[$k]['idakunsales'] = 0;
                $_datasodetail[$k]['idakunpersediaan'] = 0;
                $_datasodetail[$k]['idakunsalesreturn'] = 0;
                $_datasodetail[$k]['idakunpurchasereturn'] = 0;
                $_datasodetail[$k]['serialnumber'] = 0;
                $_datasodetail[$k]['garansitanggal'] = $v->tanggalstart;
                $_datasodetail[$k]['garansiexpired'] = $v->tanggalstop;
                $_datasodetail[$k]['idtypetrancprev'] = 0;
                $_datasodetail[$k]['idtrancprev'] = 0;
                $_datasodetail[$k]['idtrancprevd'] = 0;
                $_datasodetail[$k]['idtypetrancorder'] = 0;
                $_datasodetail[$k]['idtrancorder'] = 0;
                $_datasodetail[$k]['idtrancorderd'] = 0;
                $_datasodetail[$k]['unitqtysisa'] = 0;
    
                unset($_datamilestone[$k]['fakenoid']);
                unset($_datamilestone[$k]['iscollapse']);
                unset($_datamilestone[$k]['idcardpj_nama']);
                unset($_datamilestone[$k]['idcardpjwakil_nama']);
                $_nextnoidmilestone++;
                $_nextnoidsodetail++;
            }
    
            // Make data header
            foreach ($data['header'] as $k => $v) {
                $_changeidheader[$v->noid] = $_nextnoidheader;
                $_dataheader[$k] = (array)$v;
                $_dataheader[$k]['noid'] = $_nextnoidheader;
                $_dataheader[$k]['idmaster'] = $_nextnoidmaster;
                $_dataheader[$k]['idmilestone'] = @$_changeidmilestone[$_dataheader[$k]['idmilestone']]
                    ? $_changeidmilestone[$_dataheader[$k]['idmilestone']]
                    : $v->idmilestone;
                unset($_dataheader[$k]['fakenoid']);
                unset($_dataheader[$k]['iscollapse']);
                unset($_dataheader[$k]['idcardpj_nama']);
                unset($_dataheader[$k]['idcardpjwakil_nama']);
                $_nextnoidheader++;
            }
    
            // Make data detail
            foreach ($data['detail'] as $k => $v) {
                $_datadetail[$k] = (array)$v;
                $_datadetail[$k]['noid'] = $_nextnoiddetail;
                $_datadetail[$k]['idmaster'] = $_nextnoidmaster;
                $_datadetail[$k]['idheader'] = @$_changeidheader[$_datadetail[$k]['idheader']]
                    ? $_changeidheader[$_datadetail[$k]['idheader']]
                    : $v->idheader;
                unset($_datadetail[$k]['keydetail']);
                unset($_datadetail[$k]['kodeinventor']);
                unset($_datadetail[$k]['namainventor2']);
                unset($_datadetail[$k]['namasatuan']);
                $_nextnoiddetail++;
            }

            $result = MP::saveData([
                'statusadd' => $statusadd,
                'noid' => $noid,
                'master' => $table,
                'cdf' => $cdf,
                'milestone' => $_datamilestone,
                'header' => $_dataheader,
                'detail' => $_datadetail,
                'salesorder' => $salesorder,
                'log' => $datalog
            ]);
        } else {
            $_tablemaster = static::$main['table'];
            $_tablemilestone = static::$main['tablemilestone'];
            $_tableheader = static::$main['tableheader'];
            $_tabledetail = static::$main['tabledetail'];
            $_tablecdf = static::$main['tablecdf'];
            $_datamaster = $data['master'];
            $_datamilestone = [];
            $_dataheader = [];
            $_datadetail = [];

            $deletemilestone = MP::removeData(['table'=>$_tablemilestone,'where'=>[['idmaster','=',$data['noid']]]]);
            if (!$deletemilestone['success']) return ['success' => $deletemilestone['success'], 'message' => $deletemilestone['message']];

            $deleteheader = MP::removeData(['table'=>$_tableheader,'where'=>[['idmaster','=',$data['noid']]]]);
            if (!$deleteheader['success']) return ['success' => $deleteheader['success'], 'message' => $deleteheader['message']];

            $deletedetail = MP::removeData(['table'=>$_tabledetail,'where'=>[['idmaster','=',$data['noid']]]]);
            if (!$deletedetail['success']) return ['success' => $deletedetail['success'], 'message' => $deletedetail['message']];

            $deletecdf = MP::removeData(['table'=>$_tablecdf,'where'=>[['idrap','=',$data['noid']]]]);
            if (!$deletecdf['success']) return ['success' => $deletecdf['success'], 'message' => $deletecdf['message']];

            $_nextnoidmilestone = MP::getNextNoid($_tablemilestone);
            $_nextnoidheader = MP::getNextNoid($_tableheader);
            $_nextnoiddetail = MP::getNextNoid($_tabledetail);

            $_changeidmilestone = [];
            $_changeidheader = [];

            //ORDERING MILESTONE
            usort($data['milestone'], function($f, $s){
                return $f->noid > $s->noid;
            });

            foreach ($data['milestone'] as $k => $v) {
                $_datamilestone[$k] = (array)$v;
                if (array_key_exists('fakenoid', $_datamilestone[$k])) {
                    if ($_datamilestone[$k]['fakenoid']) {
                        if ($_nextnoidmilestone == @$_datamilestone[$k-1]['noid']) {
                            $_nextnoidmilestone = $_nextnoidmilestone+1;
                            $_nextnoidmilestone++;
                            $_nextnoidmilestone++;
                        } else {
                            // dd($data[$_tableheader]);
                            // $_noid = $v->noid;
                            // $_newarray = array_filter($data[$_tableheader], function($obj) use ($_noid) {
                            //     if ($obj->idmilestone != $_noid) {
                            //         $obj->idmilestone = $_noid;
                            //     }
                            //     return true;
                            // });
                            // dd($_newarray);
                            $_nextnoidmilestone = $_nextnoidmilestone;
                        }
                        $_changeidmilestone[$v->noid] = $_nextnoidmilestone;
                        $_datamilestone[$k]['noid'] = $_nextnoidmilestone;
                    }
                    unset($_datamilestone[$k]['fakenoid']);
                } else {
                    $_datamilestone[$k]['noid'] = @$v->noid ? $v->noid : $_nextnoidmilestone;
                }
                unset($_datamilestone[$k]['iscollapse']);
                unset($_datamilestone[$k]['idcardpj_nama']);
                unset($_datamilestone[$k]['idcardpjwakil_nama']);
            }
            // dd($_changeidmilestone);

            //ORDERING HEADER
            usort($data['header'], function($f, $s){
                return $f->noid > $s->noid;
            });

            // dd($_nextnoidheader);
            // dd($data[$_tableheader]);
            foreach ($data['header'] as $k => $v) {
                $_dataheader[$k] = [
                    'noid' => $v->noid,
                    'fakenoid' => @$v->fakenoid,
                    'idmaster' => $v->idmaster,
                    'idmilestone' => $v->idmilestone,
                    'nama' => $v->nama,
                    'tanggalstart' => $v->tanggalstart,
                    'tanggalstop' => $v->tanggalstop,
                    'tanggalfinished' => $v->tanggalfinished,
                    'idcardopname' => $v->idcardopname,
                    'progresstodate' => $v->progresstodate,
                    'progressopname' => $v->progressopname,
                    'keterangan' => $v->keterangan,
                    'nourut' => $v->nourut,
                    'idcardpj' => $v->idcardpj,
                    'idcardpjwakil' => $v->idcardpjwakil,
                    'totalqty' => $v->totalqty,
                    'totalitem' => $v->totalitem,
                    'totalsubtotal' => $v->totalsubtotal,
                    'generaltotal' => $v->generaltotal,
                    'idcreate' => $v->idcreate,
                    'docreate' => $v->docreate,
                ];

                if (array_key_exists('fakenoid', $_dataheader[$k])) {
                    if ($_dataheader[$k]['fakenoid']) {
                        if ($v->noid >= $_nextnoidheader) {
                            $_nextnoidheader = $v->noid;
                        }
                        $_changeidheader[$v->noid] = $_nextnoidheader;
                        $_dataheader[$k]['noid'] = $_nextnoidheader;
                        $_dataheader[$k]['idmilestone'] = @$_changeidmilestone[$v->idmilestone]
                            ? $_changeidmilestone[$v->idmilestone]
                            : $v->idmilestone;
                        $_nextnoidheader++;
                    }
                    unset($_dataheader[$k]['fakenoid']);
                } else {
                    if (@$v->noid) {
                        $_dataheader[$k]['noid'] = $v->noid;
                    } else {
                        $_dataheader[$k]['noid'] = $_nextnoidheader;
                        $_nextnoidheader++;
                    }
                }
                unset($_dataheader[$k]['iscollapse']);
                unset($_dataheader[$k]['idcardpj_nama']);
                unset($_dataheader[$k]['idcardpjwakil_nama']);
            }

            $_detailwithnoid = [];
            $_detailwithoutnoid = [];
            // $_maxnoid = 0;
            // dd($data[$_tabledetail]);
            // $_nextnoiddetail = $_nextnoiddetail
            // dd($_nextnoiddetail);
            $_detailordered = array_merge($_detailwithnoid,$_detailwithoutnoid);
            // dd($_detailordered);

            foreach ($_detailordered as $k => $v) {
                if (@$v->noid) {
                    $_finddetailnoid = $v->noid;
                } else {
                    $_nextnoiddetail++;
                    $_finddetailnoid = $_nextnoiddetail;
                }
                
                // $_datadetail[$k] = (array)$v;
                $_datadetail[$k]['noid'] = $_finddetailnoid;
                $_datadetail[$k]['idheader'] = @$_changeidheader[$v->idheader]
                    ? $_changeidheader[$v->idheader]
                    : $v->idheader;
                $_datadetail[$k]['idinventor'] = $v->idinventor;
                $_datadetail[$k]['namainventor'] = $v->namainventor;
                $_datadetail[$k]['unitqty'] = $v->unitqty;
                $_datadetail[$k]['idgudang'] = $v->idgudang;
                $_datadetail[$k]['keterangan'] = $v->keterangan;
                $_datadetail[$k]['idsatuan'] = $v->idsatuan;
                $_datadetail[$k]['konvsatuan'] = @$_datadetail[$k]['konvsatuan']
                    ? $_datadetail[$k]['konvsatuan']
                    : null;
                $_datadetail[$k]['unitqtysisa'] = $v->unitqtysisa;
                $_datadetail[$k]['unitprice'] = $v->unitprice;
                $_datadetail[$k]['subtotal'] = $v->subtotal;
                $_datadetail[$k]['discountvar'] = $v->discountvar;
                $_datadetail[$k]['discount'] = $v->discount;
                $_datadetail[$k]['nilaidisc'] = $v->nilaidisc;
                $_datadetail[$k]['idtypetax'] = $v->idtypetax;
                $_datadetail[$k]['idtax'] = $v->idtax;
                $_datadetail[$k]['prosentax'] = $v->prosentax;
                $_datadetail[$k]['nilaitax'] = $v->nilaitax;
                $_datadetail[$k]['hargatotal'] = $v->hargatotal;
                $_datadetail[$k]['idmaster'] = $v->idmaster;
                // if ($k == 1) {
                //     dd($_datadetail[$k]['noid']);
                // }
                // $_datadetail[$k]['idheader'] = $_headernoids[$v->idheader];
                unset($_datadetail[$k]['keydetail']);
                unset($_datadetail[$k]['kodeinventor']);
                unset($_datadetail[$k]['namainventor2']);
                unset($_datadetail[$k]['namasatuan']);
            }
            // dd($_dataheader);
            // dd($_changeidmilestone);

            $result = MP::updateData([
                'statusadd' => $statusadd,
                'noid' => $noid,
                'master' => $table,
                'cdf' => $cdf,
                'milestone' => $_datamilestone,
                'header' => $_dataheader,
                'log' => $datalog
            ]);
        }

        return response()->json(['success' => $result['success'], 'message' => $result['message']]);
    }

    public function save_milestone(Request $request) {
        $statusadd = $request->get('statusadd') == 'true' ? true : false;
        $statusadd2 = $request->get('statusadd2') == 'save' ? true : false;
        $formmilestone = $request->get('formmilestone');
        $milestone = json_decode($request->get('milestone'));
        $header = json_decode($request->get('header'));
        $resultmcard = MP::getData(['table'=>'mcard','select'=>['noid','kode','nama','isemployee','iscustomer','issupplier'],'isactive'=>true]);
        $form = [];
        $mcard = [];

        foreach ($resultmcard as $k => $v) {
            $mcard[$v->noid] = $v;
        }

        if ($statusadd) {
            $form['noid'] = $milestone
                ? end($milestone)->noid+1
                : MP::getNextNoid(static::$main['tablemilestone']);
            $form['fakenoid'] = false;
        } else {
            if ($statusadd2) {
                $form['noid'] = $milestone
                    ? end($milestone)->noid+1
                    : MP::getNextNoid(static::$main['tablemilestone']);
                $form['fakenoid'] = true;
            } else {
                $form['noid'] = $formmilestone['noid'];
                $form['fakenoid'] = false;
            }
        }

        $form['idmaster'] = $request->get('noid');
        foreach ($formmilestone as $k => $v) {
            $form[$k] = $v;
        }
        $form['iscollapse'] = @$formmilestone['iscollapse']
            ? $formmilestone['iscollapse'] == 'false'
                ? false
                : true
            : true;
        // $form['idcardpj_nama'] = $mcard[$form['idcardpj']]->nama;
        // $form['idcardpjwakil_nama'] = $mcard[$form['idcardpjwakil']]->nama;

        foreach ($milestone as $k => $v) {
            if ($v->noid == $form['noid']) {
                $form['idcardpj'] = $v->idcardpj;
                $form['idcardpj_nama'] = $v->idcardpj_nama;
                $form['idcardpjwakil'] = $v->idcardpjwakil;
                $form['idcardpjwakil_nama'] = $v->idcardpjwakil_nama;
                break;
            }
        }

        $form['header'] = $header ? $header : [];
        
        return response()->json([
            'success' => true,
            'message' => 'Save Milestone Successfully',
            'data' => $form
        ]);
    }

    public function save_header(Request $request) {
        $statusadd = $request->get('statusadd') == 'true' ? true : false;
        $statusadd2 = $request->get('statusadd2') == 'save' ? true : false;
        $formheader = $request->get('formheader');
        $milestone = json_decode($request->get('milestone'));
        $detail = json_decode($request->get('detail'));
        $resultmcard = MP::getData(['table'=>'mcard','select'=>['noid','kode','nama','isemployee','iscustomer','issupplier'],'isactive'=>true]);
        $form = [];
        $mcard = [];

        foreach ($resultmcard as $k => $v) {
            $mcard[$v->noid] = $v;
        }

        $header = [];
        foreach ($milestone as $k => $v) {
            if (count($v->header) > 0) {
                foreach ($v->header as $k2 => $v2) {
                    $header[] = $v2;
                }
            }
        }
        if ($statusadd) {
            $form['noid'] = $header
                ? end($header)->noid+1
                : MP::getNextNoid(static::$main['tableheader']);
                $form['fakenoid'] = false;
        } else {
            if ($statusadd2) {
                $form['noid'] = $header
                    ? end($header)->noid+1
                    : MP::getNextNoid(static::$main['tableheader']);
                $form['fakenoid'] = true;
            } else {
                $form['noid'] = $formheader['noid'];
                $form['fakenoid'] = false;
            }
        }
        $form['idmaster'] = $request->get('noid');
        foreach ($formheader as $k => $v) {
            // if ($k == 'tanggalstart' || $k == 'tanggalstop' || $k == 'tanggalfinished') {
            //     $form[$k] = $this->humandateTodbdate($v);
            // } else {
                $form[$k] = $v;
            // }
        }
        $form['iscollapse'] = @$formheader['iscollapse']
            ? $formheader['iscollapse'] == 'false'
                ? false
                : true
            : true;
        // $form['idcardpj_nama'] = $mcard[$form['idcardpj']]->nama;
        // $form['idcardpjwakil_nama'] = $mcard[$form['idcardpjwakil']]->nama;

        foreach ($milestone as $k => $v) {
            foreach ($v->header as $k2 => $v2) {
                if ($v2->noid == $form['noid']) {
                    $form['idcardpj'] = $v2->idcardpj;
                    $form['idcardpj_nama'] = $v2->idcardpj_nama;
                    $form['idcardpjwakil'] = $v2->idcardpjwakil;
                    $form['idcardpjwakil_nama'] = $v2->idcardpjwakil_nama;
                    break;
                }
            }
        }

        $form['detail'] = $detail ? $detail : [];

        return response()->json([
            'success' => true,
            'message' => 'Save Header Successfully',
            'data' => $form
        ]);
    }

    public function find(Request $request) {
        $noid = $request->get('noid');
        $result = MP::findData($noid);
        $result['master']->dostatus = $this->dbdateTohumandate($result['master']->dostatus);
        $result['master']->tanggal = $this->dbdateTohumandate($result['master']->tanggal);
        $result['master']->tanggaltax = !$result['master']->tanggaltax
            ? ''
            : $this->dbdateTohumandate($result['master']->tanggaltax);
        $result['master']->tanggaldue = $this->dbdateTohumandate($result['master']->tanggaldue);
        $result['master']->progresstodate = $this->dbdateTohumandate($result['master']->progresstodate);
        $result['master']->mtypetranctypestatus_nama = $result['master']->idstatustranc ? $result['master']->mtypetranctypestatus_nama : 'DRAFT';
        $result['master']->mtypetranctypestatus_classcolor = $result['master']->idstatustranc ? (new static)->color[$result['master']->mtypetranctypestatus_classcolor] : 'grey';

        $result['mttts'] = Session::get(static::$main['noid'].'-conditions')[$result['master']->idstatustranc];

        return response()->json([
            'success' => true,
            'message' => 'Find Successfully',
            'data' => $result
        ]);
    }
    
    public function generate_code(Request $request) {
        $noid = $request->get('noid');
        $nextnoid = MP::getNextNoid(static::$main['table']);

        foreach ($request->get('formheader') as $k => $v) {
            if ($k == 'kodetypetranc') {
                continue;
            } else if ($k == 'idcostcontrol') {
                $formheader[$k] = $v ? $v : 0;
            } else if ($k == 'dostatus' || $k == 'tanggal' || $k == 'tanggaldue') {
                $formheader[$k] = $this->humandateTodbdate($v);
            } else if ($k == 'idstatustranc') {
                $formheader[$k] = @$next_idstatustranc ? $next_idstatustranc : 5011;
            } else {
                $formheader[$k] = $v;
            }
        }
        // $nextnourut = MP::getNextNourut(['table'=>static::$main['table'],'where'=>$formheader['idcostcontrol']]);
        // $formheader['nourut'] = $nextnourut;

        foreach ($request->get('formmodaltanggal') as $k => $v) {
            $formmodaltanggal[$k] = $this->humandateTodbdate($v);
        }
        foreach ($request->get('formfooter') as $k => $v) {
            if ($k == 'subtotal') {
                $formfooter['totalsubtotal'] = filter_var($v, FILTER_SANITIZE_NUMBER_INT);
            } else if ($k == 'total') {
                $formfooter['generaltotal'] = filter_var($v, FILTER_SANITIZE_NUMBER_INT);
            } else {
                $formfooter[$k] = $v;
            }
        }

        $input = [
            'formheader' => $formheader,
            'forminternal' => $request->get('forminternal'),
            'formfooter' => $formfooter,
            'formmodaltanggal' => $formmodaltanggal,
            'formmodalsupplier' => $request->get('formmodalsupplier'),
            'prd' => $request->get('purchasedetail')
        ];

        $table = array_merge(
            [
                'noid' => $nextnoid,
                'idtypetranc' => 0,
                'idcreate' => Session::get('noid'),
                'docreate' => date('Y-m-d H:i:s'),
                'idupdate' => Session::get('noid'),
                'lastupdate' => date('Y-m-d H:i:s'),
            ],
            $input['formheader'],
            $input['forminternal'],
            $input['formmodaltanggal'],
            $input['formmodalsupplier'],
            $input['formfooter']
        );

        $params = [
            'tablemaster' => static::$main['table'],
            'generatecode' => static::$main['generatecode'],
            'idcostcontrol' => $request->get('idcostcontrol'),
            'kodecostcontrol' => $request->get('kodecostcontrol'),
            'data' => $table
        ];
        
        $result = MP::generateCode($params);

        return response()->json([
            'success' => true,
            'message' => 'Find Successfully',
            'data' => $result
        ]);
    }

    public function get_info() {
        $costcontrol = MP::getDefaultCostControl();

        return response()->json([
            'success' => true,
            'message' => 'Find Successfully',
            'data' => [
                'pr' => (object)[
                    'noid' => null,
                    'idtypetranc' => static::$main['idtypetranc']
                ],
                'mttts' => Session::get(static::$main['noid'].'-conditions')[static::$main['idtypetranc'].'1'],
                'mtypetranctypestatus' => [
                    'noid' => MP::findMTypeTrancTypeStatus(static::$main['idtypetranc'])->noid,
                    'nama' => 'DRAFT',
                    'classcolor' => (new static)->color['grey-silver']
                ],
                'mcarduser' => [
                    'noid' => Session::get('noid'),
                    'nama' => Session::get('myusername')
                ],
                'idtypecostcontrol' => $costcontrol->idtypecostcontrol,
                'idcostcontrol' => $costcontrol->noid,
                'kodecostcontrol' => $costcontrol->kode,
                'dostatus' => date('d-M-Y H:i:s'),
                'idcompany' => Session::get('idcompany'),
                'iddepartment' => Session::get('iddepartment'),
                'idgudang' => Session::get('idgudang'),
            ]
        ]);
    }

    public function find_last_supplier() {
        $result = MP::findLastSupplier();

        return response()->json([
            'success' => true,
            'message' => 'Find Successfully',
            'data' => $result
        ]);
    }

    public function send_to_next(Request $request) {
        $devicetoken = $request->get('_token');
        $noid = $request->get('noid');
        $idcardsupplier = $request->get('idcardsupplier');
        $find = MP::findData($noid);
        $nextnoid = MP::getNextNoid(static::$main['table2']);
        $nextnoiddetail = MP::getNextNoid(static::$main['tabledetail2']);
        $nextkodetypetranc = MP::getKode('mtypetranc',static::$main['idtypetranc2']);
        $gcpo = MP2::generateCode(static::$main['table2'], static::$main['generatecode2']);
        $typetranc = $find['purchase']->kodetypetranc;
        $idreffd = '';
        if ($find['purchasedetail']) {
            foreach ($find['purchasedetail'] as $k => $v) {
                $idreffd .= $v->noid;
            }
        }

        if ($find['purchase']->idstatustranc == 7) {
            $sendtopo = 'OTHERS (Send to '.static::$main['table2-title'].')';
        } else {
            $sendtopo = 'SET APPROVED (Approved to '.static::$main['table2-title'].')';
        }

        $datalog = [
            [
                'idtypetranc' => static::$main['idtypetranc'],
                'idtypeaction' => $find['purchase']->idstatustranc == 5037 ? 99 : 10,
                'logsubject' => 'Data '.$find['purchase']->kodetypetranc.' Kode = '.$find['purchase']->kode.' '.$sendtopo,
                'keterangan' => 'Data '.$find['purchase']->kodetypetranc.' Kode = '.$find['purchase']->kode.' '.$sendtopo.' BY '.Session::get('myusername').' ON '.date('Y-m-d H:i:s'),
                'idreff' => $find['purchase']->noid,
                'idreffd' => $idreffd,
                'iddevice' => 0,
                'devicetoken' => $devicetoken,
                'idcreate' => Session::get('noid'),
                'docreate' => date('Y-m-d H:i:s'),
            ],[
                'idtypetranc' => static::$main['idtypetranc2'],
                'idtypeaction' => 1,
                'logsubject' => 'Data '.$nextkodetypetranc.' Kode = '.$gcpo['kode'].' CREATE',
                'keterangan' => 'Data '.$nextkodetypetranc.' Kode = '.$gcpo['kode'].' CREATE BY '.Session::get('myusername').' ON '.date('Y-m-d H:i:s'),
                'idreff' => $nextnoid,
                'idreffd' => $idreffd,
                'iddevice' => 0,
                'devicetoken' => $devicetoken,
                'idcreate' => Session::get('noid'),
                'docreate' => date('Y-m-d H:i:s'),
            ]
        ];

        $params = [
            'noid' => $noid,
            'idcardsupplier' => $idcardsupplier,
            'nextnoid' => $nextnoid,
            'nextnoiddetail' => $nextnoiddetail,
            'datalog' => $datalog,
        ];
        $result = MP::sendToNext($params);

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Send to '.static::$main['table2-title'].' Successfully',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Send to '.static::$main['table2-title'].' Error!',
            ]);
        }
    }

    public function snst(Request $request) {
        $noid = $request->get('noid');
        $find = MP::findData($noid);
        $idtypetranc = $find['master']->idtypetranc;
        $idstatustranc = $request->get('idstatustranc');
        $statustranc = $request->get('statustranc');

        $typeaction = Session::get(static::$main['noid'].'-conditions')[$idstatustranc]['appmtypeaction_nama'];
        $idtypeaction = Session::get(static::$main['noid'].'-conditions')[$idstatustranc]['appmtypeaction_noid'];
        
        $idreff = $find['master']->noid;
        $logsubject = 'Data '.$find['master']->kodetypetranc.' Kode = '.$find['master']->kode.' '.$typeaction;

        $data = [
            'idstatustranc' => $idstatustranc
        ];

        $datalog = [
            'idtypetranc' => $idtypetranc,
            'idtypeaction' => $idtypeaction,
            'logsubject' => $logsubject,
            'keterangan' => $request->get('keterangan'),
            'idreff' => $idreff,
            'idreffd' => 0,
            'iddevice' => 0,
            'devicetoken' => $request->get('_token'),
            'idcreate' => Session::get('noid'),
            'docreate' => date('Y-m-d H:i:s'),
        ];

        $result = MP::snst($data, $noid, $datalog);

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Set '.$statustranc.' Successfully',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Set '.$statustranc.' Error!',
            ]);
        }
    }

    public function div(Request $request) {
        $devicetoken = $request->get('_token');
        $noid = $request->get('noid');
        $find = MP::findData($noid);
        $idtypetranc = $request->get('idtypetranc');
        $typetranc = $find['purchase']->kodetypetranc;
        $idstatustranc = $request->get('idstatustranc');
        $statustranc = $request->get('statustranc');
        $keterangan = $request->get('keterangan');
        $idreff = $find['purchase']->noid;
        $idreffd = '';
        // if ($find['purchasedetail']) {
        //     foreach ($find['purchasedetail'] as $k => $v) {
        //         $idreffd .= $v->noid;
        //     }
        // }
        if ($idstatustranc == 1) {
            $idtypeaction = 7;
            $typeaction = 'SET DRAFT';
        } else if ($idstatustranc == 2) {
            $idtypeaction = 8;
            $typeaction = 'SET ISSUED';
        } else if ($idstatustranc == 3) {
            $idtypeaction = 9;
            $typeaction = 'SET VERIFIED';
        }
        $logsubject = 'Data '.$typetranc.' Kode = '.$find['purchase']->kode.' '.$typeaction;

        $data = [
            'idstatustranc' => $idstatustranc
        ];

        $datalog = [
            'idtypetranc' => $idtypetranc,
            'idtypeaction' => $idtypeaction,
            'logsubject' => $logsubject,
            'keterangan' => $keterangan,
            'idreff' => $idreff,
            'idreffd' => $idreffd,
            'iddevice' => 0,
            'devicetoken' => $devicetoken,
            'idcreate' => Session::get('noid'),
            'docreate' => date('Y-m-d H:i:s'),
        ];

        $result = MP::div($data, $noid, $datalog);

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Set '.$statustranc.' Successfully',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Set '.$statustranc.' Error!',
            ]);
        }
    }

    public function sapcf(Request $request) {
        $devicetoken = $request->get('_token');
        $noid = $request->get('noid');
        $find = MP::findData($noid);
        $idtypetranc = $request->get('idtypetranc');
        $typetranc = $find['master']->kodetypetranc;
        $idstatustranc = $request->get('idstatustranc');
        $statustranc = $request->get('statustranc');
        $keterangan = $request->get('keterangan');
        $idreff = $find['master']->noid;
        $idreffd = '';
        // if ($find['purchasedetail']) {
        //     foreach ($find['purchasedetail'] as $k => $v) {
        //         $idreffd .= $v->noid;
        //     }
        // }
        $idtypeaction = Session::get(static::$main['noid'].'-conditions')[$idstatustranc]['appmtypeaction_noid'];
        $typeaction = Session::get(static::$main['noid'].'-conditions')[$idstatustranc]['appmtypeaction_nama'];
        $logsubject = 'Data '.$typetranc.' Kode = '.$find['master']->kode.' '.$typeaction;

        $data = [
            'idstatustranc' => $idstatustranc
        ];

        $datalog = [
            'idtypetranc' => $idtypetranc,
            'idtypeaction' => $idtypeaction,
            'logsubject' => $logsubject,
            'keterangan' => $keterangan,
            'idreff' => $idreff,
            'idreffd' => $idreffd,
            'iddevice' => 0,
            'devicetoken' => $devicetoken,
            'idcreate' => Session::get('noid'),
            'docreate' => date('Y-m-d H:i:s'),
        ];

        $result = MP::sapcf($data, $noid, $datalog);

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Set '.$statustranc.' Successfully',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Set '.$statustranc.' Error!',
            ]);
        }
    }

    public function set_pending(Request $request) {
        $noid = $request->get('noid');

        $data = [
            'idstatustranc' => 5
        ];

        $datalog = [
            'idtypetranc' => $request->get('idtypetranc'),
            'idtypeaction' => 9,
            'logsubject' => 'PURCHASE-REQUEST/PENDING/'.$noid,
            'keterangan' => $request->get('keterangan'),
            'idreff' => 0,
            'idreffd' => 0,
            'iddevice' => 0,
            'devicetoken' => $request->get('_token'),
            'idcreate' => Session::get('noid'),
            'docreate' => date('Y-m-d H:i:s'),
        ];

        $result = MP::setPending($data, $noid, $datalog);

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Set Pending Successfully',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Set Pending Error!',
            ]);
        }
    }

    public function set_canceled(Request $request) {
        $noid = $request->get('noid');

        $data = [
            'idstatustranc' => 6
        ];

        $datalog = [
            'idtypetranc' => $request->get('idtypetranc'),
            'idtypeaction' => 9,
            'logsubject' => 'PURCHASE-REQUEST/CANCELED/'.$noid,
            'keterangan' => $request->get('keterangan'),
            'idreff' => 0,
            'idreffd' => 0,
            'iddevice' => 0,
            'devicetoken' => $request->get('_token'),
            'idcreate' => Session::get('noid'),
            'docreate' => date('Y-m-d H:i:s'),
        ];

        $result = MP::setCanceled($data, $noid, $datalog);

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Set Canceled Successfully',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Set Canceled Error!',
            ]);
        }
    }

    public function delete_select_m(Request $request) {
        $selecteds = $request->get('selecteds');
        $selectcheckboxmall = $request->get('selectcheckboxmall');

        if ($selectcheckboxmall) {
            $datalog = [
                'idtypetranc' => static::$main['idtypetranc'],
                'idtypeaction' => 3,
                'logsubject' => 'PURCHASE-REQUEST/DELETEALL/All',
                'keterangan' => 'Data All Deleted',
                'idreff' => 0,
                'idreffd' => 0,
                'iddevice' => 0,
                'devicetoken' => $request->get('_token'),
                'idcreate' => Session::get('noid'),
                'docreate' => date('Y-m-d H:i:s'),
            ];
        } else {
            $datalog = [];
            foreach ($selecteds as $k => $v) {
                $datalog[$v] = [
                    'idtypetranc' => static::$main['idtypetranc'],
                    'idtypeaction' => 3,
                    'logsubject' => 'PURCHASE-REQUEST/DELETE/'.$v,
                    'keterangan' => 'Data Deleted',
                    'idreff' => 0,
                    'idreffd' => 0,
                    'iddevice' => 0,
                    'devicetoken' => $request->get('_token'),
                    'idcreate' => Session::get('noid'),
                    'docreate' => date('Y-m-d H:i:s'),
                ];
            }
        }

        $result = MP::deleteSelectM($selecteds, $selectcheckboxmall, $datalog);

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Delete Successfully',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Delete Error!',
            ]);
        }
    }

    public function delete(Request $request) {
        $noid = $request->get('noid');
        $find = MP::findData($noid);
        $datalog = [
            'idtypetranc' => $find['master']->idtypetranc,
            'idtypeaction' => 3,
            'logsubject' => 'Data '.$find['master']->kodetypetranc.' Kode = '.$find['master']->kode.' DELETE',
            'keterangan' => 'Data '.$find['master']->kodetypetranc.' Kode = '.$find['master']->kode.' DELETE BY '.Session::get('nama').' ON '.date('Y-m-d H:i:s'),
            'idreff' => $find['master']->noid,
            'idreffd' => 0,
            'iddevice' => 0,
            'devicetoken' => $request->get('_token'),
            'idcreate' => Session::get('noid'),
            'docreate' => date('Y-m-d H:i:s'),
        ];
        
        $result = MP::deleteData(['noid' => $noid, 'datalog' => $datalog]);

        return response()->json(['success' => $result['success'], 'message' => $result['message']]);
    }
    
    public function cdf_get(Request $request) {
        $whereformat = $request->get('whereformat');
        $wheresearch = $request->get('wheresearch');
        
        $querywhere = 'WHERE 1=1 ';
        if (@$whereformat) {
            if ($whereformat == 'img') {
                $querywhere .= "AND (dmsmtypefile.fileextention = 'png'
                                    OR dmsmtypefile.fileextention = 'jpg'
                                    OR dmsmtypefile.fileextention = 'jpeg'
                                    OR dmsmtypefile.fileextention = 'gif') ";
            } else {
                $querywhere .= "AND dmsmtypefile.fileextention = '$whereformat'";
            }
        }
        if (@$wheresearch) {
            $querywhere .= "AND (dmsmfile.nama LIKE '%$wheresearch%'
                                OR dmsmfile.filename LIKE '%$wheresearch%')";
        }

        $query = "SELECT 
                        dmsmfile.*, 
                        dmsmtypefile.fileextention AS fileextention, 
                        dmsmtypefile.classicon AS classicon, 
                        dmsmtypefile.classcolor AS classcolor,
                        dmsmtypefile.fileimage AS fileimage
                    FROM dmsmfile
                    JOIN dmsmtypefile ON dmsmfile.idtypefile = dmsmtypefile.noid
                    $querywhere
                    ORDER BY dmsmfile.docreate DESC";
        $result = DB::connection('mysql2')->select($query);
    
        $data = [];
        foreach ($result as $k => $v) {
            $data[] = $v;
        }

        return response()->json($data);
    }

    function getStringBetween($str,$from,$to) {
        $string = substr($str, strpos($str, $from) + strlen($from));
        if (strstr($string,$to,TRUE) != FALSE) {
            $string = strstr($string,$to,TRUE);
        } else {
            $string = $str;
        }
        return $string;
    }

    public function cdf_upload(Request $request) {
        $extensions = ['ai','avi','bin','bmp','cdr','css','csv','dat','dll','doc','docx','dwg','eml','eps','exe','fla','flv','gif','html','iso','jar','jpg','jpeg','m4p','m4v','mdb','midi','mov','mp3','mp4','mpeg','mpg','ogg','pdf','php','png','ppt','pptx','ps','psd','pub','rar','sql','swf','tiff','txt','url','wav','wma','wmv','xls','xlsx','xml','zip','zipx'];

        $validator = Validator::make($request->all(), [
            'files' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $request->get('files')
            ]);
        }

        $files = $request->file('files');
        $filenames = $request->get('filenames');
        $filepublics = $request->get('filepublics');
        $fileextensions = [];

        foreach ($files as $k => $v) {
            $_ext = strtolower(substr(strrchr($v->getClientOriginalName(),'.'),1));
            $fileextensions[] = $_ext;
            // dd(!in_array($_ext,$extensions));
            if (!in_array($_ext,$extensions)) {
                // dd('format salah');
                return response()->json([
                    'success' => false,
                    'message' => 'Format not available!'
                ]);
            }
            // dd('format benar');
        }

        // try {
        //     echo $ngentot;
        // } catch (\Throwable $th) {
        //     return response()->json([
        //         'success' => false,
        //         'message' => $th->getMessage()
        //     ]);
        // }
        // die;

        foreach ($files as $k => $v) {
            $filenameori = basename($v->getClientOriginalName(), '.'.$fileextensions[$k]);
            $filenameoriwithextension = $filenameori.'.'.$fileextensions[$k];
            $filenameedit = $filenames[$k];
            $filenameeditwithextension = $filenameedit.'.'.$fileextensions[$k];
            $filecheckexist = DB::connection('mysql2')->table('dmsmfile')->select('noid','filename')->where('filename',$filenameeditwithextension)->orderBy('noid', 'desc')->first();

            if ($filecheckexist) {
                $filecheckdup = DB::connection('mysql2')->table('dmsmfile')->select('noid','filename')->where('filename','like',"$filenameeditwithextension%")->orderBy('noid', 'desc')->first();
    
                $getdup = $this->getStringBetween($filecheckdup->filename,'(',')');
                if ($getdup == $filecheckdup->filename) {
                    $filenamefix = $filenameori.' (2)';
                    $filenamewithextensionfix = $filenamefix.'.'.$fileextensions[$k];
                } else {
                    $filenamefix = $filenameori.' ('.((int)$getdup+1).')';
                    $filenamewithextensionfix = $filenamefix.'.'.$fileextensions[$k];
                }
            } else {
                $filenamefix = $filenameori;
                $filenamewithextensionfix = $filenamefix.'.'.$fileextensions[$k];
            }

            $filesize = $v->getSize();
            $fileextension = $fileextensions[$k];

            $resultlastnoid = DB::connection('mysql2')->table('dmsmfile')->select('noid')->orderBy('noid', 'desc')->first();
            $lastnoid = ($resultlastnoid != null) ? $resultlastnoid->noid+1 : 1;
        
            $filelocation = 'filemanager/original';
            $filepreview = 'filemanager/preview';
            $filethumbnail = 'filemanager/thumb';

            if ($fileextension == 'png' ||
                $fileextension == 'jpg' ||
                $fileextension == 'jpeg' ||
                $fileextension == 'gif') {
                //SAVE TO ORIGINAL
                try {
                    $imageresizeoriginal = Image::make($v->path());
                    $imageresizeoriginal->save($filelocation.'/'.$filenamewithextensionfix);
                } catch (\Throwable $e) {
                    return response()->json([
                        'success' => false,
                        'message' => $e->getMessage()
                    ]);
                }
                //SAVE TO PREVIEW
                try {
                    $imageresizepreview = Image::make($v->path());
                    $imageresizepreview->resize(900, 300, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($filepreview.'/'.$filenamewithextensionfix);
                } catch (\Throwable $e) {
                    return response()->json([
                        'success' => false,
                        'message' => $e->getMessage()
                    ]);
                }
                //SAVE TO THUMBNAIL
                try {
                    $imageresizethumb = Image::make($v->path());
                    $imageresizethumb->resize(300, 300, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($filethumbnail.'/'.$filenamewithextensionfix);
                } catch (\Throwable $e) {
                    return response()->json([
                        'success' => false,
                        'message' => $e->getMessage()
                    ]);
                }
            } else if (in_array($fileextension, $fileextensions)) {
                try {
                    $v->move(public_path().'/filemanager/original/',$filenamewithextensionfix);
                } catch (\Throwable $e) {
                    return response()->json([
                        'success' => false,
                        'message' => $e->getMessage()
                    ]);
                }
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Format not available!'
                ]);
            }

            $ispublic = $filepublics[$k];
            $resulttypefile = DB::connection('mysql2')->table('dmsmtypefile')->select('noid')->where('fileextention', $fileextension)->first();
            $idtypefile = ($resulttypefile != null) ? $resulttypefile->noid : 9;

            $data = [
                'noid' => $lastnoid,
                'nama' => $filenamefix,
                'filename' => $filenamewithextensionfix,
                'filelocation' => $filelocation,
                'filepreview' => $filepreview,
                'filethumbnail' => $filethumbnail,
                'filesize' => $filesize,
                'idtypefile' => $idtypefile,
                'ispublic' => $ispublic,
                'docreate' => date('Y-m-d H:i:s'),
            ];

            $insert = DB::connection('mysql2')->table('dmsmfile')->insert($data);
        }
        


        


        return response()->json([
            'success' => true,
            'message' => count($files) == 1
                ? count($files).' attachment successfully uploaded.'
                : count($files).' attachments successfully uploaded.',
            'data' => json_encode($request->file('file'))
        ]);
    }

    public function cdf_edit(Request $request) {
        $filetag = '';
        if ($request->get('filetag') != '') {
            foreach ($request->get('filetag') as $k => $v) {
                $filetag .= count($request->get('filetag'))-1 == $k ? $v : $v.',';
            }
        }

        $result = DB::connection('mysql2')
                    ->table('dmsmfile')
                    ->where('noid', $request->get('noid'))
                    ->update([
                        'nama' => $request->get('filenames'),
                        'ispublic' => $request->get('filestatus'),
                        'filetag' => $filetag
                    ]);
        
        return response()->json([
            'success' => $result ? true : false,
            'message' => $result
                ? 'Update '.$request->get('filenames').' successfully'
                : 'Update '.$request->get('filenames').' error!',
            'data' => json_encode($request->file('file'))
        ]);
    }
}