<?php

namespace App\Http\Controllers\lam_error;

use App\Http\Controllers\Controller;
Use DB;
use App\User;
use App\Menu;
use App\Page;
use App\Panel;
use App\Widget;
use App\WidgetGrid;
use App\WidgetGridField;
use App\Departement;
use App\Company;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Schema;
use Carbon;

class ErrorController extends Controller
{
    public static function UnderMaintenance($slug)
    {
      // dd($slug);
        $menu = Menu::where('noid',$slug)->first();
        $page = Page::where('noid',$menu->linkidpage)->first();
        $idhomepagelink = $slug;
        $user = User::where('noid', Session::get('noid'))->first();
        $public = $menu->ispublic;
        $panel = Panel::where('idpage', $page->noid)->orderBy('norow','asc')->orderBy('nocol','asc')->get();
        $page_title = $page->pagetitle;
        $page_description = $page->pagetitle;
        $idhomepage =  $user->idhomepage;
        $noid = Session::get('noid');
        $myusername = Session::get('myusername');
        // return view('lam_error.UnderMaintenance', compact(
        // dd($page);
        if ($slug == 99211114) {
          return view('lam_error.UnderMaintenanceError', compact(
            'noid',
            'public',
            'myusername',
            'idhomepagelink',
            'page',
            'page_title',
            'idhomepage',
            'menu',
            'namatable',
             'page_description'));
        }else{
          // dd('ok');
          return view('lam_error.UnderMaintenance2', compact(
            'noid',
            'public',
            'myusername',
            'idhomepagelink',
            'page',
            'page_title',
            'idhomepage',
            'menu',
            'namatable',
             'page_description'));
        }

    }
    public static function UnauthorizedPage($slug)
    {
        $menu = Menu::where('noid',$slug)->first();
        $page = Page::where('noid',$menu->linkidpage)->first();
        $idhomepagelink = $slug;
        $user = User::where('noid', Session::get('noid'))->first();
        $public = $menu->ispublic;
        $panel = Panel::where('idpage', $page->noid)->orderBy('norow','asc')->orderBy('nocol','asc')->get();
        $page_title = $page->pagetitle;
        $page_description = $page->pagetitle;
        $idhomepage =  $user->idhomepage;
        $noid = Session::get('noid');
        $myusername = Session::get('myusername');
        return view('lam_error.UnauthorizedPage', compact(
          'noid',
          'public',
          'myusername',
          'idhomepagelink',
          'page',
          'page_title',
          'idhomepage',
          'menu',
          'namatable',
           'page_description'));
    }
    public static function UnderMaintenanceFun($slug)
    {
        $menu = Menu::where('noid',$slug)->first();
        $page = Page::where('noid',$menu->linkidpage)->first();
        $view = $page->pagecontroller;
        $keywords = explode("-", str_replace(array("\n", "\t", "\r", "\a", "/", "//",":"), "-",$view));
        $controllerView = $keywords[3];
        $viewRedi = $keywords[4];
        $folder = $keywords[0];
        $idhomepagelink = $slug;
        $user = User::where('noid', Session::get('noid'))->first();
        $public = $menu->ispublic;
        $panel = Panel::where('idpage', $page->noid)->orderBy('norow','asc')->orderBy('nocol','asc')->get();
        $page_title = $page->pagetitle;
        $page_description = $page->pagetitle;
        $idhomepage =  $user->idhomepage;
        $noid = Session::get('noid');
        $myusername = Session::get('myusername');
        return view('lam_error.UnderMaintenanceFun', compact(
          'noid',
          'viewRedi',
          'public',
          'myusername',
          'idhomepagelink',
          'page',
          'page_title',
          'idhomepage',
          'menu',
          'namatable',
           'page_description'));
    }
    public function Unauthorized($slug)
    {
      $menu = Menu::where('noid',$slug)->first();
      $page = Page::where('noid',$menu->linkidpage)->first();
      $idhomepagelink = $slug;
      $user = User::where('noid', Session::get('noid'))->first();
      $public = $menu->ispublic;
      $panel = Panel::where('idpage', $page->noid)->orderBy('norow','asc')->orderBy('nocol','asc')->get();
      $page_title = $page->pagetitle;
      $page_description = $page->pagetitle;
      $idhomepage =  $user->idhomepage;
      $noid = Session::get('noid');
      $myusername = Session::get('myusername');
      // return view('lam_error.UnderMaintenance', compact(
      return view('lam_error.Unauthorized', compact(
        'noid',
        'public',
        'myusername',
        'idhomepagelink',
        'page',
        'page_title',
        'idhomepage',
        'menu',
        'namatable',
         'page_description'));
    }

    public static function error404($slug)
    {
      $menu = Menu::where('noid',$slug)->first();
      $page = Page::where('noid',$menu->linkidpage)->first();
      $idhomepagelink = $slug;
      $user = User::where('noid', Session::get('noid'))->first();
      $public = $menu->ispublic;
      $page_title = 'contoh page';
      $page_description = 'contoh';
      $idhomepage =  $user->idhomepage;
      $noid = Session::get('noid');
      $myusername = Session::get('myusername');
      // return view('lam_error.UnderMaintenance', compact(
      return view('lam_error.error404_2', compact(
        'noid',
        'public',
        'myusername',
        'idhomepagelink',
        'page',
        'page_title',
        'idhomepage',
        'menu',
        'namatable',
         'page_description'));




    }
	
    public static function pageNull($idhomepagelink) {
		$noid = Session::get('noid');
		$myusername = Session::get('myusername');

		$page = Page::where('noid', 2)->first();
		$page_title = $page->pagetitle;
		$page_description = $page->pagetitle;

		$user = User::where('noid', Session::get('noid'))->first();
		$idhomepage = $user->idhomepage;
		
		return view('lam_error.page_null', compact(
			'noid',
			'myusername',
			'page',
			'page_title',
			'page_description',
			'idhomepage',
			'idhomepagelink'
		));
    }

    public static function underConstruction() {
      return view('errors/underconstruction');
    }
}
