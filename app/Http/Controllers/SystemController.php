<?php

namespace App\Http\Controllers;

use App\Http\Controllers\lam_system\FrontPageController;
use App\Http\Controllers\lam_error\ErrorController;
use App\Http\Controllers\lam_custom\ErrorController as ErrorControllerFunc;
use App\Http\Controllers\lam_system\ErrorController as ErrorControllerFunc2;
Use DB;
use App\User;
use App\Menu;
use App\Page;
use App\Panel;
use App\Widget;
use App\WidgetGrid;
use App\WidgetGridField;
use App\Departement;
use App\Company;
use App\ViewGridField;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Schema;
use Carbon;

class SystemController extends Controller {

    public static function index($slug) {
        if(!Session::get('login')){
            return view('pages.login');
        }

        $menu = Menu::where('noid',$slug)->first();
        if(strpos($menu->menucaption, ' ') !== false) {
            $pecah = explode(' ', $menu->menucaption);
            $namaview = strtolower($pecah[1]);
        } else {
            $namaview = strtolower($menu->menucaption);
        }
        $idhomepagelink = $slug;
        $page = Page::where('noid',$menu->linkidpage)->first();

        $user = User::where('noid', Session::get('noid'))->first();
        $public = $menu->ispublic;
        $idhomepage =  $user->idhomepage;
        $noid = Session::get('noid');
        $myusername = Session::get('myusername');

        if ($page->pagecontroller == null) {
            return ErrorController::UnauthorizedPage($slug);
        } else {
            $view = $page->pagecontroller;
            $keywords = explode("-", str_replace(array("\n", "\t", "\r", "\a", "/", "//",":"), "-",$view));
            $controllerView = $keywords[3];
            $viewRedi = $keywords[4];
            $folder = $keywords[0];
            $urlnya = 'App\Http\Controllers';
            $existsfile = $urlnya." \ ".$folder." \ ".$controllerView;
            $tamba = $urlnya." \ ".$folder." \ ".$controllerView;
            $tamba = str_replace(" ","",$tamba);
            $existsfile = str_replace(" ","",$existsfile);
            
            if (!class_exists($tamba)) {
                if ($folder == 'lam_system') {
                    return ErrorControllerFunc2::UnderMaintenance($slug);
                } else if ($folder == 'lam_custom') {
                    return ErrorControllerFunc::UnderMaintenance($slug);
                } else {
                    return ErrorController::UnderMaintenance($slug);
                }
            } else if (!method_exists($existsfile,$viewRedi)) {
                if ($folder == 'lam_system') {
                    return ErrorControllerFunc2::UnderMaintenanceFun2($slug);
                } else if ($folder == 'lam_custom') {
                    return ErrorControllerFunc::UnderMaintenanceFun2($slug);
                } else {
                    return ErrorController::UnderMaintenanceFun($slug);
                }
            } else {
                if ($folder == 'lam_system') {
                    return app($tamba)->$viewRedi($slug);
                } else if ($folder == 'lam_custom') {
                    return app($tamba)->$viewRedi($slug);
                } else {
                    return app($tamba)->$viewRedi($slug);
                }
            }
        }
    }


    public function savedata(Request $request) {
        // dd($request);
        $data = $request->all();

        $datapost=  array();
        $table = $data['data']['tabel'];
        parse_str($data['data']['data'], $datapost);
        if ($datapost['noid'] == '') {
            $total = DB::connection('mysql2')->table($table)->orderBy('noid','desc')->first();
            foreach($datapost as $key => $value) {
                $datapost['noid'] = $total->noid+1;
            }
            $in = DB::connection('mysql2')->table($table)->insert($datapost);
            echo json_encode(array(
                'status'=>'add',
                'table'=>'Company',
                'success'=>true
            ));
        } else {
            $array = \array_diff($datapost, ["noid" => $datapost['noid']]);
            $total = DB::connection('mysql2')->table($table)->where('noid',$datapost['noid'])->update($array);
            echo json_encode(array(
                'status'=>'edit',
                'table'=>'Company',
                'success'=>true
            ));
        }
    }

    public function ambildata2(Request $request) {
        $kode = $request->get('table');
        $menu = Menu::where('noid',$kode)->first();
        $page = Page::where('noid',$menu->linkidpage)->first();
        dd($page);
        $widget = Widget::where('kode', $kode)->first();
        // dd($widget);
        $table = $widget->maintable;
        // $table = $request->get('table');
        // dd($table);
        // $widget = Widget::where('maintable', $table)->first();
        $widgetgrid = WidgetGrid::where('idcmswidget', $widget->noid)->first();
        $widgetgridfield = WidgetGridField::where('idcmswidgetgrid', $widgetgrid->noid)->where('newshow',1)->orderBy('formurut','asc')->get();
        // foreach ($widgetgridfield as $key => $value) {
        //   $dd[] = $value->coltypefield;
        // }
        // dd($widgetgridfield);
        $isifield = '';
        foreach ($widgetgridfield as $key => $value) {
            if ($value->newreq == 1) {
                $required = 'required';
            } else {
                $required = '';
            }
            if ($value->coltypefield == 96) {
                $select = WidgetGridField::getField($value,$value->fieldname,$value->fieldcaption,$value->newreq);
                // dd($select);
                $isifield = $select;
            } elseif ($value->coltypefield == 1) {
                $isifield = '<input width="'.$value->colheight.'"  '.$required.' type="text" class="form-control form-control"
                id="'.$value->fieldname.'" name="'.$value->fieldname.'" placeholder="'.$value->fieldcaption.'">';
            } elseif ($value->coltypefield == 4 ) {
            $isifield = '<textarea width="'.$value->colheight.'" '.$required.' class="form-control form-control"  id="'.$value->fieldname.'" name="'.$value->fieldname.'" placeholder="'.$value->fieldcaption.'"></textarea>';
            } elseif ($value->coltypefield == 31 ) {
                $isifield = '<div class="checkbox-list">
                    <label class="checkbox">
                        <input width="'.$value->colheight.'" '.$required.' type="checkbox" id="'.$value->fieldname.'" name="'.$value->fieldname.'">
                        <span></span>
                        '.$value->fieldname.'
                    </label>
                    </div>';
            } elseif ($value->coltypefield == 41 ) {
                $isifield = '<div class="checkbox-list">
                    <label class="checkbox">
                        <input width="'.$value->colheight.'" '.$required.' type="checkbox" id="'.$value->fieldname.'" name="'.$value->fieldname.'">
                        <span></span>
                        '.$value->fieldname.'
                    </label>
                    </div>';
            } elseif ($value->coltypefield == 51  ) {
                $isifield = '<div class="checkbox-list">
                    <label class="checkbox">
                        <input width="'.$value->colheight.'" '.$required.' type="checkbox" id="'.$value->fieldname.'" name="'.$value->fieldname.'">
                        <span></span>
                        '.$value->fieldname.'
                    </label>
                    </div>';
            } elseif ($value->coltypefield == 12  ) {
                $mytime = \Carbon\Carbon::now()->format('d-m-Y');
                $isifield = '<div class="input-group date">
                    <input type="text" '.$required.' width="'.$value->colheight.'" class="datepicker form-control" data-provide="datepicker" readonly="readonly" value="'.$mytime.'"  id="'.$value->fieldname.'" name="'.$value->fieldname.'">
                    <div class="input-group-append">
                    <span class="input-group-text">
                        <i class="la la-calendar"></i>
                    </span>
                    </div>
                </div>';
            } elseif ($value->coltypefield == 21  ) {
                $mytime = \Carbon\Carbon::now()->format('d-m-Y H:i');
                $isifield = '<div class="input-group date" id="kt_datetimepicker_1" data-target-input="nearest">
                                <input type="text" class="datepicker2 form-control datetimepicker-input" placeholder="Select date &amp; time" data-target="#kt_datetimepicker_1">
                                <div class="input-group-append" data-target="#kt_datetimepicker_1" data-toggle="datetimepicker">
                                    <span class="input-group-text">
                                        <i class="ki ki-calendar"></i>
                                    </span>
                                </div>
                            </div>';
                // $isifield = '<div class="input-group date">
                //   <input type="text" '.$required.' class="datepicker form-control" data-provide="datepicker" readonly="readonly" value="'.$mytime.'"  id="'.$value->fieldname.'" name="'.$value->fieldname.'">
                //   <div class="input-group-append">
                //     <span class="input-group-text">
                //       <i class="la la-calendar"></i>
                //     </span>
                //   </div>
                // </div>';
            } else {
                $isifield = '';
            }

            $data['field'][]= array(
            'label'=>$value->fieldcaption,
            'kode'=>$value->coltypefield,
            'field'=>$isifield,
            );
                // dd($data);
        }
      // dd($data['field']);

        header("Content-type: application/json");
        header("Cache-Control: no-cache, must-revalidate");
        echo json_encode($data);
    }


    public function filtertabel(Request $request) {
        $id = $request->get('id');
        $kode = $request->get('table');
        $widget = Widget::where('kode', $kode)->first();

        dd($widget);
    }

    public static function ambildata(Request $request) {
        $kode = $request->get('table');
        // $kode = $request->get('table');
        // dd($kode);
        $menu = Menu::where('noid',$kode)->first();

        $page = Page::where('noid',$menu->linkidpage)->first();
        $panel = Panel::where('idpage',$page->noid)->first();
        $widget = Widget::where('noid', $panel->idwidget)->first();
        $filtertable = json_decode($widget->configwidget);
        $fieldtabel = $filtertable->WidgetFilter;
        $table = $widget->maintable;
        dd($table);
        $menu = Menu::where('noid',$kode)->first();
        // dd($menu);
        $page = Page::where('noid',$menu->linkidpage)->first();
        $panel = Panel::where('idpage',$page->noid)->first();
        $widget = Widget::where('noid', $panel->idwidget)->first();
        $filtertable = json_decode($widget->configwidget);
        $fieldtabel = $filtertable->WidgetFilter;
        $table = $widget->maintable;
        $widgetgrid = WidgetGrid::where('idcmswidget', $widget->noid)->first();
        // $widgetgridfield = WidgetGridField::where('idcmswidgetgrid', $widgetgrid->noid)->orderBy('nourut','asc')->orderBy('noid','asc')->get();
        // $widgetgridfield = WidgetGridField::where('idcmswidgetgrid', $widgetgrid->noid)->orderBy('nourut','asc')->orderBy('noid','asc')->get();
        $widgetgridfield2 = WidgetGridField::where('idcmswidgetgrid', $widgetgrid->noid)->where('colshow',1)->orderBy('nourut','asc')->orderBy('noid','asc')->get();
        $widgetgridfield = WidgetGridField::where('idcmswidgetgrid', $widgetgrid->noid)->where('colshow',1)->orderBy('nourut','asc')->orderBy('noid','asc')->get();
        // dd($widgetgridfield3);
        // $widgetgridfield2 = WidgetGridField::where('idcmswidgetgrid', $widgetgrid->noid)->orderBy('nourut','asc')->orderBy('noid','asc')->get();
        // dd($widgetgridfield2);
        $data['buttonfilter'] = [];
        foreach ($fieldtabel as $key => $value) {
            foreach ($value->button as $key2 => $value2) {
                $pecah = explode('.', $value2->condition[0]->querywhere[0]->fieldname);
                $oncl = 'onclick="caridata("'.$pecah[1].'")" ';
                $data['buttonfilter'][$key][] = array(
                    'nama'=>$value2->buttoncaption,
                    'buttonactive'=>$value2->buttonactive,
                    'fieldname'=>$pecah[1],
                    'taga'=>'<a href="#" class="btn btn-outline-secondary btn-sm"   style="background-color:#E3F2FD;"  '.$oncl.'   >'.$value2->buttoncaption.' </a>',
                    'value'=>$value2->condition[0]->querywhere[0]->value
                );
            }
        }
        $data['columns'] = array('<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike">','No');
        $data['width'] = array();
        $data['colheaderalign'] = array();
        $data['coltypefield'] = array();
        $data['colgroupname'] = array();
        $data['colheaderalign'] = array();
        $data['coldefault'] = array();

        for ($i=0; $i <count($widgetgridfield) ; $i++) {
            array_push($data['columns'], $widgetgridfield[$i]->fieldcaption);
            array_push($data['width'], $widgetgridfield[$i]->colwidth.'%');
            array_push($data['colheaderalign'], $widgetgridfield[$i]->colheaderalign);
            array_push($data['coltypefield'], $widgetgridfield[$i]->coltypefield);
            array_push($data['colgroupname'], $widgetgridfield[$i]->colgroupname);
            array_push($data['coldefault'], $widgetgridfield[$i]->coldefault);
        }
        // dd($data['colheaderalign']);
        if (!empty($data['colgroupname'])){
            $data['header_atas'] = '<tr class="atas" role="group-header">';
            $data['header_atas'] .= '<th style="text-align:center" colspan="2">#</th>';
            $data['header_atas'] .= '<th style="text-align:center" colspan="'.count($widgetgridfield).'">---</th>';
            $data['header_atas'] .= '<th colspan="1" style="text-align:center">---</th>';
            $data['header_atas'] .= '</tr>';
        } else {
            $datahead = array_count_values($data['colgroupname']);
            $data['header_atas'] = '<tr class="footter">';
            $data['header_atas'] .= '<th style="text-align:center" colspan="2">#</th>';
            foreach ($datahead as $key => $value) {
            $data['header_atas'] .= '<th style="text-align:center" colspan="'.$value.'">'.$key.'</th>';
            }
            $data['header_atas'] .= '<th colspan="1" style="text-align:center">---</th>';
            $data['header_atas'] .= '</tr>';
        }
        // dd($data['footter']);
        array_push($data['columns'],'Action');
        $columns =$data['columns'];
        $TEXT_QUERY = '';
        $SELECT_QUERY = "SELECT ";
        $WHERE_QUERY = "FROM ".$table." a";
        $TABEL_ALIAS = "a";
        foreach ($widgetgridfield2 as $key => $value) {
            if ($widgetgridfield2[$key]->coltypefield == 96) {
                $TABEL_ALIAS = chr(ord($TABEL_ALIAS) + 1);
                $koma = ( $key !== count( $widgetgridfield2 ) -1 ) ? "," : " ";
                $SELECT_QUERY .= "a.".$value->fieldname." AS ".$value->fieldname.', ';
                $isirelasi = str_replace(";"," ",$widgetgridfield2[$key]->tablelookup);
                $ex = explode(' ', $isirelasi);
                if( strpos($ex[2], ",") !== false ) {
                    $ex2 = explode(',', $ex[2]);
                    ${'FIELD_FOREIGN'.$key} =  $TABEL_ALIAS.'.'.$ex2[0].' , '.$TABEL_ALIAS.'.'.$ex2[1];
                } else {
                    ${'FIELD_FOREIGN'.$key} = $TABEL_ALIAS.'.'.$ex[2];
                }
                ${'FIELD_NAME'.$key} = $ex[0];
                ${'FIELD_RELASI'.$key} = $ex[1];
                ${'TABLE_RELASI'.$key} = $ex[3];
                $SELECT_QUERY .= ' CONCAT('.${'FIELD_FOREIGN'.$key}.') as '.  ${'FIELD_NAME'.$key}.'_nama '.$koma;
                $WHERE_QUERY .= " LEFT JOIN ".${'TABLE_RELASI'.$key}." ".$TABEL_ALIAS." ON a.".  ${'FIELD_NAME'.$key}."=".$TABEL_ALIAS.".".${'FIELD_RELASI'.$key};
            } else {
                $koma = ( $key !== count( $widgetgridfield2 ) -1 ) ? "," : " ";
                $SELECT_QUERY .= "a.".$value->fieldname.$koma;
            }
        }
        $TEXT_QUERY .= $SELECT_QUERY;
        $TEXT_QUERY .= $WHERE_QUERY.' where a.noid > 0';
        // dd($TEXT_QUERY);
        $limit = $widgetgrid->recperpage;

        $sql =  DB::connection('mysql2')->select($TEXT_QUERY);
            $totalsql = count($sql);
            // dd($sql);
        // dd(count($data['columns']));
        $data['footter'] = '<tfoot><tr>';
        $data['footter'] .= '<th style="text-align:right" colspan="4">'.$totalsql.'</th>';
        $data['footter'] .= '<th style="text-align:right" colspan="8"></th>';
        $data['footter'] .= '</tr></tfoot>';

        $no = 1;
        foreach ($sql as $key => $value) {
            // dd($value);
            $row = array();
            array_push($row,'<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike">');
            array_push($row,$no++);
            // dd($widgetgridfield2[$key]->coltypefield);
            // $string = (string)$widgetgridfield2[$key]->fieldname;
            //     array_push($row,$value->$string);

            for ($i=0; $i <count($widgetgridfield2) ; $i++) {
                if ($widgetgridfield2[$i]->coltypefield == 4) {
                    // dd($widgetgridfield2[$i]->coltypefield);
                    $string = (string)$widgetgridfield2[$i]->fieldname;
                    $isiketrangan = \Illuminate\Support\Str::limit($value->$string, 100, $end='...');
                    array_push($row,$isiketrangan);
                } else if ($widgetgridfield2[$i]->coltypefield == 96) {
                    $string = (string)$widgetgridfield2[$i]->fieldname.'_nama';
                    array_push($row,$value->$string);
                    // }else if ($widgetgridfield2[$i]->coltypefield == 11) {
                    //     $string = (string)$widgetgridfield2[$i]->fieldname;
                    //   // dd($string);
                    //     array_push($row,$value->idcompany_nama);
                } else if ($widgetgridfield2[$i]->coltypefield == 31) {
                    $string = (string)$widgetgridfield2[$i]->fieldname;
                    $check = '<input type="checkbox" checked id="'.$widgetgridfield2[$i]->fieldname.'_'.$value->$string.'" name="'.$widgetgridfield2[$i]->fieldname.'_'.$value->$string.'" value="'.$value->$string.'">';
                    array_push($row,$check);
                } else {
                    $string = (string)$widgetgridfield2[$i]->fieldname;
                    array_push($row,$value->$string);
                }
            }
            array_push($row,'<div class="tools"><a href="javascript:;" class="act_row_edit tooltips btn btn-xs yellow" data-original-title="Ubah Data" data-container="body" data-placement="top"><i class="fa fa fa-edit"></i></a><a href="javascript:;" class="act_row_view tooltips btn btn-xs green" data-original-title="Lihat Data" data-container="body" data-placement="top"><i class="fa fa fa-search"></i></a><a href="javascript:;" class="act_row_delete tooltips btn btn-xs red" data-original-title="Hapus Data" data-container="body" data-placement="top"><i class="fa fa fa-trash"></i></a>');
                           // dd($row);
            $data['data'][] = $row;
        }

            // dd($data['data']);
                // $output = array(
                //     "recordsTotal" =>$totalData,
                //     "recordsFiltered" => $limit,
                //     "data" => $sql,
                // );
        header("Content-type: application/json");
        header("Cache-Control: no-cache, must-revalidate");
        echo json_encode($data);
    }



    public function hapusdepartemen() {
        $namatable = $_POST['namatable'];
        $in =   DB::connection('mysql2')->table($namatable)->where('noid',$_POST['id'])->delete();
        echo json_encode('Berhasil');
    }

    public function editdepartemen(Request $request) {
        $data = $request->all();
        $table = $data['namatable'];
        $widget = Widget::where('maintable', $table)->first();
        $widgetgrid = WidgetGrid::where('idcmswidget', $widget->noid)->first();
        $widgetgridfield = WidgetGridField::where('idcmswidgetgrid', $widgetgrid->noid)->where('editshow',1)->get();
        $id = $data['id'];
        $datatab = DB::connection('mysql2')->table($table)->where('noid',$id)->first();
        $no = 1;

        foreach ($widgetgridfield as $key => $value) {
            $string = (string)$widgetgridfield[$key]->fieldname;
            $hasil[] = array(
            $widgetgridfield[$key]->fieldname =>$datatab->$string,
            );
        }
        $hasil2[] = array(
            'noid' =>$id,
        );
        $hasilsemua = array_merge($hasil,$hasil2);
        // dd($hasil);
        echo json_encode($hasilsemua);
        //       foreach ($datatab as $key => $value) {
        //           $dataarray = array(
        //
        //           );
        //       }
        //       dd($datatab);
        // if ($_POST['namatable'] == 'genmcompany') {
        //   $departement = Company::where('noid',$_POST['id'])->first();
        //   $data = array(
        //     'noid'=>$departement->noid,
        //     'kode'=>$departement->kode,
        //     'nama'=>$departement->nama,
        //     'namaalias'=>$departement->namaalias,
        //     'keterangan'=>$departement->keterangan,
        //     'isactive'=>$departement->isactive,
        //     'issystem'=>$departement->issystem,
        //     'nourut'=>$departement->nourut,
        //     'idparent'=>$departement->idparent,
        //     'depthlevel'=>$departement->depthlevel,
        //     'classicon'=>$departement->classicon,
        //     'idcreate'=>$departement->idcreate,
        //     'docreate'=>$departement->docreate,
        //     'idupdate'=>$departement->idupdate,
        //     'lastupdate'=>$departement->lastupdate
        //   );
        //   echo json_encode($data);
        // }else{
        //   $departement = Departement::where('noid',$_POST['id'])->first();
        //   $company = Company::where('noid',$departement->idcompany)->first();
        //   $data = array(
        //     'noid'=>$departement->noid,
        //     'company'=>$departement->idcompany,
        //     'kode'=>$departement->kode,
        //     'nama'=>$departement->nama,
        //     'namaalias'=>$departement->namaalias,
        //     'keterangan'=>$departement->keterangan,
        //     'isactive'=>$departement->isactive,
        //     'issystem'=>$departement->issystem,
        //     'nourut'=>$departement->nourut,
        //     'idparent'=>$departement->idparent,
        //     'depthlevel'=>$departement->depthlevel,
        //     'classicon'=>$departement->classicon,
        //     'idcreate'=>$departement->idcreate,
        //     'docreate'=>$departement->docreate,
        //     'idupdate'=>$departement->idupdate,
        //     'lastupdate'=>$departement->lastupdate
        //   );
        //   echo json_encode($data);
        // }
    }

    public function read_notification(Request $request) {
        $noid = $request->get('noid');
        $response = DB::connection('mysql2')
                        ->table('appnotification')
                        ->where('noid',$noid)
                        ->update(['isread'=>1]);
        return response()->json([
            'success' => true,
            'message' => 'Successfully read notification.'
        ]);
    }
}
