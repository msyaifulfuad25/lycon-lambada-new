<?php

namespace App\Http\Controllers\lam_mobile;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Auth;

class LoginController extends Controller {

    public function test() {
        print_r($_POST);
    }

    public function login_(Request $request){
        $credentials = $request->only(['myusername','password']);
        //Alternatively
        /*$credentials = [
        'email' => $request->email,
        'password' => $request->password
        ];
        */
        // dd($credentials);
        $token = auth()->attempt($credentials);
        // dd('asd');
        return response()->json([
            "message" => "Login Successful",
            "token"=> $token
        ], 201);
    }

    // public function logout(){
    //     auth()->logout();
    //     return response()->json([
    //     "message" => "Logout Successful",
    //     ], 201);
    // }

    public function getCsrfToken() {
        echo json_encode(csrf_token());
    }

    public function login(Request $request){
        $myusername = $request->username;
        $password = $request->password;

        $data = User::where('myusername',$myusername)->first();
        if ($data){ //apakah email tersebut ada atau tidak
            if ($data->mypassword == sha1($password)){
                Session::put('myusername', $data->myusername);
                Session::put('idhomepage', $data->idhomepage);
                Session::put('noid', $data->noid);
                Session::put('groupuser', $data->groupuser);
                Session::put('page',[]);
                Session::put('login',TRUE);

                $response = [
                    'success' => true,
                    'message' => 'Login has been Succesfully'
                ];
                echo json_encode($response);
            } else {
                $response = [
                    'success' => false,
                    'message' => 'Login error!'
                ];
                echo json_encode($response);
            }
        } else {
            $response = [
                'success' => false,
                'message' => 'Login error!'
            ];
            echo json_encode($response);
        }
    }

    public function logout(){
        Session::flush();
        return redirect('/');
    }

    public function register(Request $request){
        return view('register');
    }

    public function registerPost(Request $request){
        $this->validate($request, [
            'name' => 'required|min:4',
            'email' => 'required|min:4|email|unique:users',
            'password' => 'required',
            'confirmation' => 'required|same:password',
        ]);

        $data =  new ModelUser();
        $data->name = $request->name;
        $data->email = $request->email;
        $data->password = bcrypt($request->password);
        $data->save();
        return redirect('login')->with('alert-success','Kamu berhasil Register');
    }
}
