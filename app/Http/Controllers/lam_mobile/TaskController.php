<?php

namespace App\Http\Controllers\lam_mobile;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class TaskController extends Controller {

    public function get() {
        $result = DB::connection('mysql2')->table('apptask')->get();

        echo json_encode($result);
    }

    public function save(Request $request) {

        $resultlastnoid = DB::connection('mysql2')->table('apptask')->select('noid')->orderBy('noid', 'desc')->first();
        
        if ($resultlastnoid) {
            $lastnoid = $resultlastnoid->noid+1;
        } else {
            $lastnoid = 0;
        }

        $data = [
            'noid' => $lastnoid,
            'nama' => $request->get('nama'),
            'keterangan' => $request->get('keterangan'),
            'tanggal' => $request->get('tanggal'),
        ];

        $insert = DB::connection('mysql2')->table('apptask')->insert($data);

        if ($insert) {
            $response = [
                'success' => true,
                'message' => 'Insert has been Successfully'
            ];
            echo json_encode($response);
        } else {
            $response = [
                'success' => true,
                'message' => 'Insert Error!'
            ];
            echo json_encode($response);
        }
    }

}