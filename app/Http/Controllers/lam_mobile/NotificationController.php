<?php

namespace App\Http\Controllers\lam_mobile;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use JWTAuth;

class NotificationController extends Controller {

    public function get() {
        // dd(JWTAuth::parseToken()->authenticate());
        // $accessToken = auth()->user()->createToken('authToken')->accessToken;
        // dd(Session::get('login'));
        $result = DB::connection('mysql2')->table('appmtypenotification')->where('noid', '!=', 0)->get();
        $resultan = DB::connection('mysql2')->table('appnotification')->get();

        $data = [];
        foreach ($resultan as $k => $v) {
            if (@$data['notification-'.$v->idtypenotification] == 0) {
                $data['notification-'.$v->idtypenotification] = [];
            }
            array_push($data['notification-'.$v->idtypenotification], $v);
        }
        echo json_encode($data);
    }

    public function read($noid) {
        $result = DB::connection('mysql2')->table('appnotification')->where('noid', $noid)->update(['isread' => 1]);

        if ($result) {
            $response = [
                'success' => true,
                'message' => 'Notification was readed.'
            ];
            echo json_encode($response);
        } else {
            $response = [
                'success' => true,
                'message' => 'Notification was\'nt read.'
            ];
            echo json_encode($response);
        }
    }

}