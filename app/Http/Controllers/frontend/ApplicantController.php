<?php namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;
use App\Menu;
use DB;
use Validator;

class ApplicantController extends Controller {

    function index() {
        echo 'asd';
    }

    public static function getNextNoid($table) {
        $result = DB::connection('mysql2')
            ->table($table)
            ->orderBy('noid', 'desc')
            ->first();
        return $result ? $result->noid+1 : 1;
    }

    public function save(Request $request) {
        $form = $request->all();
        
        $rules = [
            'jobappliedfor' => 'required',
            'firstname' => 'required',
            // 'lastname' => 'required',
            'gender' => 'required',
            'lasteducation' => 'required',
            'birthplace' => 'required',
            'birthdate' => 'required',
            'ktpaddress' => 'required',
            'ktprt' => 'required',
            'ktprw' => 'required',
            'ktpposcode' => 'required',
            'ktpvillage' => 'required',
            'ktpdistrict' => 'required',
            'ktpcity' => 'required',
            'ktpprovince' => 'required',
            'localaddress' => 'required',
            'localrt' => 'required',
            'localrw' => 'required',
            'localposcode' => 'required',
            'localvillage' => 'required',
            'localdistrict' => 'required',
            'localcity' => 'required',
            'localprovince' => 'required',
            'phone' => 'required',
            'whatsapp' => 'required',
            'kknumber' => 'required',
            'ktpnumber' => 'required',
            'height' => 'required',
            'weight' => 'required',
            'status' => 'required',
            'religion' => 'required',
            'nationality' => 'required',
            'residence' => 'required',
            'vehiclestatus' => 'required',
            'vehicletype' => 'required',
            'language' => 'required',
            'email' => 'required|email',
            'photo' => 'required|image|mimes:jpeg,jpg,png,gif|max:2048',
        ];

        $messages = [
            'jobappliedfor.required' => 'Posisi harus diisi.',
            'firstname.required' => 'Nama Depan harus diisi.',
            // 'lastname.required' => 'Nama Belakang harus diisi.',
            'gender.required' => 'Jenis Kelamin harus diisi.',
            'lasteducation.required' => 'Pendidikan Terakhir harus diisi.',
            'birthplace.required' => 'Tempat Lahir harus diisi.',
            'birthdate.required' => 'Tanggal Lahir harus diisi.',
            'ktpaddress.required' => 'Alamat harus diisi.',
            'ktprt.required' => 'RT harus diisi.',
            'ktprw.required' => 'RW harus diisi.',
            'ktpposcode.required' => 'Kode Pos harus diisi.',
            'ktpvillage.required' => 'Kelurahan harus diisi.',
            'ktpdistrict.required' => 'Kecamatan harus diisi.',
            'ktpcity.required' => 'Kota diisi.',
            'ktpprovince.required' => 'Provinsi diisi.',
            'localaddress.required' => 'Alamat harus diisi.',
            'localrt.required' => 'RT harus diisi.',
            'localrw.required' => 'RW harus diisi.',
            'localposcode.required' => 'Kode Pos harus diisi.',
            'localvillage.required' => 'Kelurahan harus diisi.',
            'localdistrict.required' => 'Kecamatan harus diisi.',
            'localcity.required' => 'Kota diisi.',
            'localprovince.required' => 'Provinsi diisi.',
            'phone.required' => 'Nomor Telepon harus diisi.',
            'whatsapp.required' => 'Nomor Whatsapp harus diisi.',
            'kknumber.required' => 'Nomor KK harus diisi.',
            'ktpnumber.required' => 'Nomor KTP harus diisi.',
            'height.required' => 'Tinggi Badan harus diisi.',
            'weight.required' => 'Berat Badan harus diisi.',
            'status.required' => 'Status harus diisi.',
            'religion.required' => 'Agama harus diisi.',
            'nationality.required' => 'Kebangsaan harus diisi.',
            'residence.required' => 'Tempat Tinggal harus diisi.',
            'vehiclestatus.required' => 'Kendaraan harus diisi.',
            'vehicletype.required' => 'Jenis Kendaraan harus diisi.',
            'language.required' => 'Bahasa harus diisi.',
            'email.required' => 'Email harus diisi.',
            'email.email' => 'Email tidak valid.',
            'photo.required' => 'Foto harus diisi.',
            'photo.image' => 'Format foto tidak dikenal.',
            'photo.max' => 'Format maksimal 2mb.',
        ];

        // Last Education
        if (array_key_exists('lasteducation', $form)) {
            if ($form['lasteducation'] == 'SMK' || 
                $form['lasteducation'] == 'SMP' || 
                $form['lasteducation'] == 'D1' || 
                $form['lasteducation'] == 'D2' || 
                $form['lasteducation'] == 'D3' || 
                $form['lasteducation'] == 'D4' || 
                $form['lasteducation'] == 'S1' || 
                $form['lasteducation'] == 'S2') {
                    $rules['major'] = 'required';
                    $rules['college'] = 'required';
                    $messages['major.required'] = 'Jurusan harus diisi.';
                    $messages['college.required'] = 'Sekolah/Universitas harus diisi.';
            }
        }

        $isphoto = count($_FILES) > 0;
        if (!$isphoto) {
            $rules['photo'] = 'required';
            $messages['photo.required'] = 'Foto harus diisi.';
        }


        $form['noid'] = $this->getNextNoid('applicant');
        $form['ktpaddress'] = $form['ktpaddress'].", RT.".$form['ktprt']." RW.".$form['ktprw']." ".$form['ktpposcode'].", Ds.".$form['ktpvillage'].", Kec.".$form['ktpdistrict'].", ".$form['ktpcity'].", ".$form['ktpprovince'];
        $form['localaddress'] = $form['localaddress'].", RT.".$form['localrt']." RW.".$form['localrw']." ".$form['localposcode'].", Ds.".$form['localvillage'].", Kec.".$form['localdistrict'].", ".$form['localcity'].", ".$form['localprovince'];
        $form['birthdate'] = date('Y-m-d H:i:s', strtotime($form['birthdate']));
        $form['sima'] = array_key_exists('sima', $form) ? 1 : 0;
        $form['simb1'] = array_key_exists('simb1', $form) ? 1 : 0;
        $form['simb2'] = array_key_exists('simb2', $form) ? 1 : 0;
        $form['simc'] = array_key_exists('simc', $form) ? 1 : 0;
        $form['simd'] = array_key_exists('simd', $form) ? 1 : 0;
        $form['photo'] = @$_FILES['photo']['name'];

        // Language
        if (array_key_exists('language', $form)) {
            $formlanguage = '';
            foreach ($form['language'] as $k => $v) {
                $formlanguage .= $v.';';
            }
            $form['language'] = $formlanguage;
        }

        
        $isemergency = array_key_exists('emergency', $form);
        $isfamily = array_key_exists('family', $form);
        $iseducation = array_key_exists('education', $form);
        $istraining = array_key_exists('training', $form);
        $isworked = array_key_exists('worked', $form);
        $isposneg = array_key_exists('description', $form['positive']) && array_key_exists('description', $form['negative']);

        $errors = [];



        // Emergency
        if ($isemergency) {
            // Emergency Custom Errors
            if (is_null($form['emergency']['status'][0])) { $errors['emergency-row-0-status'][] = 'Status Hubungan harus diisi'; }
            if (is_null($form['emergency']['name'][0])) { $errors['emergency-row-0-name'][] = 'Nama Lengkap harus diisi'; }
            if (is_null($form['emergency']['address'][0])) { $errors['emergency-row-0-address'][] = 'Alamat Lengkap harus diisi'; }
            if (is_null($form['emergency']['phone'][0])) { $errors['emergency-row-0-phone'][] = 'Nomor Telepon harus diisi'; }
            if (is_null($form['emergency']['whatsapp'][0])) { $errors['emergency-row-0-whatsapp'][] = 'Nomor Whatsapp harus diisi'; }
            // Emergency Set
            $formemergency = [];
            $emergencyid = $this->getNextNoid('applicantemergency');
            foreach ($form['emergency']['status'] as $k => $v) {
                $formemergency[] = [
                    'noid' => $emergencyid,
                    'idapplicant' => $form['noid'],
                    'status' => $v,
                    'name' => $form['emergency']['name'][$k],
                    'address' => $form['emergency']['address'][$k],
                    'phone' => $form['emergency']['phone'][$k],
                    'whatsapp' => $form['emergency']['whatsapp'][$k]
                ];
                $emergencyid++;
            }
            unset($form['emergency']);
        }

        // Family
        if ($isfamily) {
            // Family Custom Errors
            for ($i=0;$i<=2;$i++) {
                if (is_null($form['family']['status'][$i])) { $errors["family-row-$i-status"][] = 'Status Hubungan harus diisi'; }
                if (is_null($form['family']['name'][$i])) { $errors["family-row-$i-name"][] = 'Nama Lengkap harus diisi'; }
                if (is_null($form['family']['phone'][$i])) { $errors["family-row-$i-phone"][] = 'Nomor Telepon harus diisi'; }
                if (is_null($form['family']['whatsapp'][$i])) { $errors["family-row-$i-whatsapp"][] = 'Nomor Whatsapp harus diisi'; }
                if (is_null($form['family']['gender'][$i])) { $errors["family-row-$i-gender"][] = 'Jenis Kelamin harus diisi'; }
                if (is_null($form['family']['birthplace'][$i])) { $errors["family-row-$i-birthplace"][] = 'Tempat Lahir harus diisi'; }
                if (is_null($form['family']['birthdate'][$i])) { $errors["family-row-$i-birthdate"][] = 'Tanggal Lahir harus diisi'; }
                if (is_null($form['family']['education'][$i])) { $errors["family-row-$i-education"][] = 'Pendidikan harus diisi'; }
            }
            // Family Set
            $formfamily = [];
            $familyid = $this->getNextNoid('applicantfamily');
            foreach ($form['family']['status'] as $k => $v) {
                $formfamily[] = [
                    'noid' => $familyid,
                    'idapplicant' => $form['noid'],
                    'status' => $v,
                    'name' => $form['family']['name'][$k],
                    'phone' => $form['family']['phone'][$k],
                    'whatsapp' => $form['family']['whatsapp'][$k],
                    'gender' => $form['family']['gender'][$k],
                    'birthplace' => $form['family']['birthplace'][$k],
                    'birthdate' => date('Y-m-d H:i:s', strtotime($form['family']['birthdate'][$k])),
                    'education' => $form['family']['education'][$k],
                ];
                $familyid++;
            }
            unset($form['family']);
        }

        // Education
        if ($iseducation) {
            // Education Custom Errors
            for ($i=0;$i<=0;$i++) {
                if (is_null($form['education']['in'][$i])) { $errors["education-row-$i-in"][] = 'Masuk harus diisi'; }
                if (is_null($form['education']['out'][$i])) { $errors["education-row-$i-out"][] = 'Lulus harus diisi'; }
                if (is_null($form['education']['school'][$i])) { $errors["education-row-$i-school"][] = 'Sekolah/PT harus diisi'; }
                if (is_null($form['education']['major'][$i])) { $errors["education-row-$i-major"][] = 'Jurusan harus diisi'; }
                if (is_null($form['education']['score'][$i])) { $errors["education-row-$i-score"][] = 'IPK/NEM harus diisi'; }
                if (is_null($form['education']['city'][$i])) { $errors["education-row-$i-city"][] = 'Kota harus diisi'; }
                if (is_null($form['education']['certificate'][$i])) { $errors["education-row-$i-certificate"][] = 'Ijazah harus diisi'; }
            }
            // Education Set
            $formeducation = [];
            $educationid = $this->getNextNoid('applicanteducation');
            foreach ($form['education']['in'] as $k => $v) {
                $formeducation[] = [
                    'noid' => $educationid,
                    'idapplicant' => $form['noid'],
                    'in' => $v,
                    'out' => $form['education']['out'][$k],
                    'school' => $form['education']['school'][$k],
                    'major' => $form['education']['major'][$k],
                    'score' => $form['education']['score'][$k],
                    'city' => $form['education']['city'][$k],
                    'certificate' => $form['education']['certificate'][$k],
                ];
                $educationid++;
            }
            unset($form['education']);
        }

        // Training
        if ($istraining) {
            $formtraining = [];
            $trainingid = $this->getNextNoid('applicanttraining');
            foreach ($form['training']['name'] as $k => $v) {
                $formtraining[] = [
                    'noid' => $trainingid,
                    'idapplicant' => $form['noid'],
                    'name' => $v,
                    'city' => $form['training']['city'][$k],
                    'organizer' => $form['training']['organizer'][$k],
                    'year' => $form['training']['year'][$k],
                    'certificate' => $form['training']['certificate'][$k],
                ];
                $trainingid++;
            }
            unset($form['training']);
        }

        // Worked
        if ($isworked) {
            $formworked = [];
            $workedid = $this->getNextNoid('applicantworked');
            foreach ($form['worked']['in'] as $k => $v) {
                $formworked[] = [
                    'noid' => $workedid,
                    'idapplicant' => $form['noid'],
                    'in' => $v,
                    'out' => $form['worked']['out'][$k],
                    'company' => $form['worked']['company'][$k],
                    'city' => $form['worked']['city'][$k],
                    'position' => $form['worked']['position'][$k],
                    'salary' => $form['worked']['salary'][$k],
                    'stopreason' => $form['worked']['stopreason'][$k],
                ];
                $workedid++;
            }
            unset($form['worked']);
        }

        // Positive Negative
        if ($isposneg) {
            // Positive Custom Errors
            for ($i=0;$i<=2;$i++) {
                if (is_null($form['positive']['description'][$i])) { $errors["positive-row-$i-description"][] = 'Description harus diisi'; }
            }
            // Positive Set
            $formposneg = [];
            $posnegid = $this->getNextNoid('applicantpositivenegative');
            foreach ($form['positive']['description'] as $k => $v) {
                $formposneg[] = [
                    'noid' => $posnegid,
                    'idapplicant' => $form['noid'],
                    'description' => $v,
                    'ispositive' => 1,
                ];
                $posnegid++;
            }
            // Negative Custom Errors
            for ($i=0;$i<=2;$i++) {
                if (is_null($form['negative']['description'][$i])) { $errors["negative-row-$i-description"][] = 'Description harus diisi'; }
            }
            // Negative Set
            foreach ($form['negative']['description'] as $k => $v) {
                $formposneg[] = [
                    'noid' => $posnegid,
                    'idapplicant' => $form['noid'],
                    'description' => $v,
                    'ispositive' => 0,
                ];
                $posnegid++;
            }
            unset($form['positive']);
            unset($form['negative']);
        }

        if (is_null($form['hobby'])) { $errors["hobby"][] = 'Data harus diisi'; }
        if (is_null($form['resignfactor'])) { $errors["resignfactor"][] = 'Data harus diisi'; }
        if (is_null($form['lokerinfo'])) { $errors["lokerinfo"][] = 'Data harus diisi'; }
        if (is_null($form['responsibility'])) { $errors["responsibility"][] = 'Data harus diisi'; }
        if (is_null($form['appliedbefore'])) { $errors["appliedbefore"][] = 'Data harus diisi'; }
        if (is_null($form['workingrelative'])) { $errors["workingrelative"][] = 'Data harus diisi'; }
        if (is_null($form['seriouslyill'])) { $errors["seriouslyill"][] = 'Data harus diisi'; }
        if (is_null($form['policeproblem'])) { $errors["policeproblem"][] = 'Data harus diisi'; }
        if (is_null($form['placedready'])) { $errors["placedready"][] = 'Data harus diisi'; }
        if (is_null($form['officialtravel'])) { $errors["officialtravel"][] = 'Data harus diisi'; }
        if (is_null($form['workingdate'])) { $errors["workingdate"][] = 'Data harus diisi'; }
        if (is_null($form['salary'])) { $errors["salary"][] = 'Data harus diisi'; }


        // if (count($errors) > 0) {
        //     return response()->json([
        //         'success' => false,
        //         'message' => 'Data harus dilengkapi.',
        //         'errors' => $errors
        //     ]);
        // }


        unset($form['_token']);
        unset($form['ktprt']);
        unset($form['ktprw']);
        unset($form['ktpposcode']);
        unset($form['ktpvillage']);
        unset($form['ktpdistrict']);
        unset($form['ktpcity']);
        unset($form['ktpprovince']);
        unset($form['localrt']);
        unset($form['localrw']);
        unset($form['localposcode']);
        unset($form['localvillage']);
        unset($form['localdistrict']);
        unset($form['localcity']);
        unset($form['localprovince']);



        // Validator
        $validator = Validator::make($request->all(), $rules, $messages);
        if (!$validator->passes() || count($errors) > 0) {
            return response()->json([
                'success' => false,
                'message' => 'Data harus dilengkapi.',
                'errors' => array_merge($validator->errors()->toArray(), $errors)
            ]);
        }


        // Photo
        if ($isphoto) {
            $file = $request->file('photo');
            $path = '/uploads/frontend/';
            $newFileName = date('YmdHis_').$file->getClientOriginalName();
            $upSuccess = $file->move(public_path() . $path, $newFileName);
            // $result = file_exists(public_path() . $path . $newFileName);
            // $fileData =[
            //     'fileType' => $file->getClientOriginalExtension(),
            //     'filePath' => substr($path, 1) . $newFileName
            // ];
        }
        // End Photo

        
        $save = DB::connection('mysql2')
            ->table('applicant')
            ->insert($form);

        if ($isemergency) {
            $saveemergency = DB::connection('mysql2')
                ->table('applicantemergency')
                ->insert($formemergency);
        }

        if ($isfamily) {
            $savefamily = DB::connection('mysql2')
                ->table('applicantfamily')
                ->insert($formfamily);
        }

        if ($iseducation) {
            $saveeducation = DB::connection('mysql2')
                ->table('applicanteducation')
                ->insert($formeducation);
        }

        if ($istraining) {
            $savetraining = DB::connection('mysql2')
                ->table('applicanttraining')
                ->insert($formtraining);
        }

        if ($isworked) {
            $saveworked = DB::connection('mysql2')
                ->table('applicantworked')
                ->insert($formworked);
        }

        if ($isposneg) {
            $saveposneg = DB::connection('mysql2')
                ->table('applicantpositivenegative')
                ->insert($formposneg);
        }
        
        return response()->json([
            'success' => true,
            'message' => 'Sukses submit data.',
        ]);
        // return view('frontend.applicant.index');
    }
}