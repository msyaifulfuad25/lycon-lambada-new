<?php

namespace App\Http\Controllers;
Use DB;
use App\User;
use App\Menu;
use App\Page;
use App\Panel;
use App\Widget;
use App\WidgetGrid;
use App\WidgetGridField;
use App\Departement;
use App\Company;
use App\Widgetchart;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Schema;
use Carbon;

class ChartController extends Controller {

    public function ambilTabel(Request $request) {
        $id_tabel = $request->get('namatable');
        $widget = Widget::where('maintable', $id_tabel)->first();
        $widgetgrid = WidgetGrid::where('idcmswidget', $widget->noid)->first();
        $widgetgridfield = WidgetGridField::where('idcmswidgetgrid', $widgetgrid->noid)->where('newshow',1)->orderBy('formurut','asc')->get();
        // dd($widgetgridfield);
        $isifield = '';

        foreach ($widgetgridfield as $key => $value) {
            if ($value->newreq == 1) {
                $required = 'required';
            } else {
                $required = '';
            }

            if ($value->coltypefield == 96) {
                $select = WidgetGridField::getField($value,$value->fieldname,$value->fieldcaption,$value->newreq);
                // dd($select);
                $isifield = $select;
            } elseif ($value->coltypefield == 1) {
                $isifield = '<input width="'.$value->colheight.'"  '.$required.' type="text" class="form-control form-control"
                id="'.$value->fieldname.'" name="'.$value->fieldname.'" placeholder="'.$value->fieldcaption.'">';
            } elseif ($value->coltypefield == 4 ) {
                $isifield = '<textarea width="'.$value->colheight.'" '.$required.' class="form-control form-control"  id="'.$value->fieldname.'" name="'.$value->fieldname.'" placeholder="'.$value->fieldcaption.'"></textarea>';
            } elseif ($value->coltypefield == 31 ) {
                $isifield = '<div class="checkbox-list">
                    <label class="checkbox">
                        <input width="'.$value->colheight.'" '.$required.' type="checkbox" id="'.$value->fieldname.'" name="'.$value->fieldname.'">
                        <span></span>
                        '.$value->fieldname.'
                    </label>
                    </div>';
            } elseif ($value->coltypefield == 41 ) {
                $isifield = '<div class="checkbox-list">
                    <label class="checkbox">
                        <input width="'.$value->colheight.'" '.$required.' type="checkbox" id="'.$value->fieldname.'" name="'.$value->fieldname.'">
                        <span></span>
                        '.$value->fieldname.'
                    </label>
                    </div>';
            } elseif ($value->coltypefield == 51  ) {
                $isifield = '<div class="checkbox-list">
                    <label class="checkbox">
                        <input width="'.$value->colheight.'" '.$required.' type="checkbox" id="'.$value->fieldname.'" name="'.$value->fieldname.'">
                        <span></span>
                        '.$value->fieldname.'
                    </label>
                    </div>';
            } elseif ($value->coltypefield == 12  ) {
                $mytime = \Carbon\Carbon::now()->format('d-m-Y');
                $isifield = '<div class="input-group date">
                    <input type="text" '.$required.' width="'.$value->colheight.'" class="datepicker form-control" data-provide="datepicker" readonly="readonly" value="'.$mytime.'"  id="'.$value->fieldname.'" name="'.$value->fieldname.'">
                    <div class="input-group-append">
                    <span class="input-group-text">
                        <i class="la la-calendar"></i>
                    </span>
                    </div>
                </div>';
            } elseif ($value->coltypefield == 21  ) {
                $mytime = \Carbon\Carbon::now()->format('d-m-Y H:i');
                $isifield = '<div class="input-group date" id="kt_datetimepicker_1" data-target-input="nearest">
                            <input type="text" class="datepicker2 form-control datetimepicker-input" placeholder="Select date &amp; time" data-target="#kt_datetimepicker_1">
                            <div class="input-group-append" data-target="#kt_datetimepicker_1" data-toggle="datetimepicker">
                                <span class="input-group-text">
                                    <i class="ki ki-calendar"></i>
                                </span>
                            </div>
                        </div>';
                // $isifield = '<div class="input-group date">
                //   <input type="text" '.$required.' class="datepicker form-control" data-provide="datepicker" readonly="readonly" value="'.$mytime.'"  id="'.$value->fieldname.'" name="'.$value->fieldname.'">
                //   <div class="input-group-append">
                //     <span class="input-group-text">
                //       <i class="la la-calendar"></i>
                //     </span>
                //   </div>
                // </div>';
            } else {
                $isifield = '';
            }

            $data['field'][] = array(
                'label' => $value->fieldcaption,
                'kode' => $value->coltypefield,
                'field' => $isifield,
            );
            // dd($data);
        }
        // dd($data['field']);

        header("Content-type: application/json");
        header("Cache-Control: no-cache, must-revalidate");
    }

    public function ambilchart(Request $request) {
        $id_cart = $request->get('id_cart');
        $widget = Menu::getAllPanelCart($id_cart);
        $datachart2 = json_decode($widget[0]->widget_dataquery);
        $isiwidget = $widget[0];
        $chart = Widgetchart::where('idcmswidget',$id_cart)->first();
        $hasi =\App\Menu::ambilQuery($datachart2[0]->query);
        header("Content-type: application/json");
        header("Cache-Control: no-cache, must-revalidate");
        echo json_encode(array(
            'hasil'=>$hasi,
            'widget'=>$isiwidget,
            'defaultchart'=>$chart->defaultchart
        ));
    }
    
    public function get_chart(Request $request) {
        $noid = $request->get('noid');
        $widget = Menu::getAllPanelCart($noid);
        $datachart2 = json_decode($widget[0]->widget_dataquery);
        $isiwidget = $widget[0];
        $chart = Widgetchart::where('idcmswidget',$noid)->first();
        $hasi =\App\Menu::ambilQuery($datachart2[0]->query);
        header("Content-type: application/json");
        header("Cache-Control: no-cache, must-revalidate");
        echo json_encode(array(
            'hasil' => $hasi,
            'widget' => $isiwidget,
            'defaultchart' => $chart->defaultchart
        ));
    }

    public function ambilSlide(Request $request) {
        $id_slider = $request->get('id_slider');
        $widget = Menu::getAllPanelSlide($id_slider);
        $hasi = json_decode($widget[0]->widget_configwidget);
        $hasil = $hasi->Widget_Slider->datasource->dataquery;
        // dd($hasil);
        $result =Menu::ambilQuery($hasil);
        header("Content-type: application/json");
        header("Cache-Control: no-cache, must-revalidate");
        echo json_encode(array(
            'hasil'=>$result
        ));
    }
}
