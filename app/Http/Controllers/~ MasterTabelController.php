<?php

namespace App\Http\Controllers;
Use DB;
use App\User;
use App\Menu;
use App\Page;
use App\Panel;
use App\Widget;
use App\WidgetGrid;
use App\WidgetGridField;
use App\Departement;
use App\Company;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Schema;
use Carbon;

class MasterTabelController extends Controller
{
  public static function index($id)
  {
      //
    if(!Session::get('login')){
        return view('pages.login');
    }
    $menu = Menu::where('noid',$id)->first();
    // dd($menu);
    $page = Page::where('noid',$menu->linkidpage)->first();
    if(strpos($menu->menucaption, ' ') !== false) {
      $pecah = explode(' ', $menu->menucaption);
      $namaview = strtolower($pecah[1]);
      } else {
        $namaview = strtolower($menu->menucaption);
      }
    $idhomepagelink = $id;
    $user = User::where('noid', Session::get('noid'))->first();
    $widget = Widget::where('kode', $id)->first();
      $public = $menu->ispublic;
      $idhomepage =  $user->idhomepage;
      $noid = Session::get('noid');
      $myusername = Session::get('myusername');
      if ($page == null) {
          $page = Page::where('noid', 2)->first();
          $page_title = $page->pagetitle;
          $page_description = $page->pagetitle;
        return view('lam_custom.notfound', compact(
          'noid',
          'myusername',
          'idhomepage',
          'idhomepagelink',
          'page',
          'page_title',
           'page_description'));

      }else{
          $panel = Panel::where('idpage',$page->noid)->first();
          $page_title = $page->pagetitle;
          $page_description = $page->pagetitle;
      }


        if ($widget == null) {
          $widgetgrid = null;
          $widgetgridfield = array();
          $widgetgridfield2 = array();
          $namatable = null;
        }else{
          $widgetgrid = WidgetGrid::where('idcmswidget', $widget->noid)->first();
          $widgetgridfield = WidgetGridField::where('idcmswidgetgrid', $widgetgrid->noid)->where('colshow',1)->get();
          $namatable = $id;
          $tabledata =  DB::connection('mysql2')->table($widget->maintable)->first();
        }

        $menu2 = Menu::where('noid',$id)->first();
        $page2 = Page::where('noid',$menu2->linkidpage)->first();
        // dd($page2);
         $widget2 = Widget::where('kode', $id)->first();
         $table = $widget2->maintable;


        $widgetgrid2 = WidgetGrid::where('idcmswidget', $widget2->noid)->first();
        // dd($widgetgrid2->recperpage);
        if (isset($widgetgrid2)) {
        $widgetgridfield2 = WidgetGridField::where('idcmswidgetgrid', $widgetgrid2->noid)->where('colshow',1)->orderBy('nourut','asc')->orderBy('noid','asc')->get();
         $data['columns'] = array();
         $data['colgroupname'] = array();

         for ($i=0; $i <count($widgetgridfield2) ; $i++) {
                  array_push($data['columns'], '<th style="width:'.$widgetgridfield2[$i]->colwidth.' px" >'.$widgetgridfield2[$i]->fieldcaption.'</th>');
            array_push($data['colgroupname'], $widgetgridfield2[$i]->colgroupname);
         }

         $columns =$data['columns'];
         // dd($columns);

         if(!empty($data['colgroupname'])){
             $data['header_atas'] = '<tr class="atas" role="group-header">';
             $data['header_atas'] .= '<th style="text-align:center" colspan="2">#</th>';
               $data['header_atas'] .= '<th style="text-align:center" colspan="'.count($widgetgridfield2).'">---</th>';
               $data['header_atas'] .= '<th colspan="1" style="text-align:center">---</th>';
             $data['header_atas'] .= '</tr>';
         }else{
             $datahead = array_count_values($data['colgroupname']);
                   $data['header_atas'] = '<tr class="footter">';
                   $data['header_atas'] .= '<th style="text-align:center" colspan="2">#</th>';
                   foreach ($datahead as $key => $value) {
                     $data['header_atas'] .= '<th style="text-align:center" colspan="'.$value.'">'.$key.'</th>';
                   }
                     $data['header_atas'] .= '<th colspan="1" style="text-align:center">---</th>';
                   $data['header_atas'] .= '</tr>';
         }
         $header_atas =   $data['header_atas'];
    }else{
      $header_atas = '';
    }

        // dd($header_atas);
        return view('lam_master_tabel.index', compact(
          'noid',
          'menu',
          'widget',
          'columns',
          'header_atas',
          'widgetgrid',
          'widgetgrid2',
          'widgetgridfield',
          'widgetgridfield2',
          'departement',
          'public',
          'myusername',
          'idhomepagelink',
          'page',
          'panel',
          'page_title',
          'namatable',
          'idhomepage',
           'page_description'));
    }
    // dd($tabledata);

}
