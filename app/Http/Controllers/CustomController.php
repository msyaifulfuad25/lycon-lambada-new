<?php

namespace App\Http\Controllers;

use App\Http\Controllers\lam_project\RABController;
use App\Http\Controllers\lam_project\CustomSampleController;
use App\Http\Controllers\lam_error\ErrorController;
use App\Http\Controllers\lam_custom\ErrorController as ErrorControllerFunc;
use App\Http\Controllers\lam_system\ErrorController as ErrorControllerFunc2;
Use DB;
use App\User;
use App\Menu;
use App\Page;
use App\Panel;
use App\Widget;
use App\WidgetGrid;
use App\WidgetGridField;
use App\Departement;
use App\Company;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Schema;
use Carbon;

class CustomController extends Controller {

	public static function index($slug) {
    	if (!Session::get('login')) {
        	return view('pages.login');
    	}

		$menu = Menu::where('noid',$slug)->first();
		$page = Page::where('noid',$menu->linkidpage)->first();

    	if (strpos($menu->menucaption, ' ') !== false) {
    		$pecah = explode(' ', $menu->menucaption);
    		$namaview = strtolower($pecah[1]);
		} else {
			$namaview = strtolower($menu->menucaption);
		}

		$idhomepagelink = $slug;
		$user = User::where('noid', Session::get('noid'))->first();
		$widget = Widget::where('kode', $slug)->first();
		$public = $menu->ispublic;
		$idhomepage =  $user->idhomepage;
		$noid = Session::get('noid');
		$myusername = Session::get('myusername');
      // dd($page->pagecontroller);

		if ($page == null) {
			$page = Page::where('noid', 2)->first();
			$page_title = $page->pagetitle;
			$page_description = $page->pagetitle;
		} else {
			$panel = Panel::where('idpage',$page->noid)->first();
			$page_title = $page->pagetitle;
			$page_description = $page->pagetitle;
		}

		if ($page->pagecontroller == null) {
			// dd('ok');
			return ErrorControllerFunc::UnderMaintenance($slug);
			// return view('lam_system.'.$namaview.'', compact(
			//   'noid',
			//   'public',
			//   'myusername',
			//   'idhomepagelink',
			//   'page',
			//   'page_title',
			//   'idhomepage',
			//   'menu',
			//   'namatable',
			//    'page_description'));
		} else {
			$view = $page->pagecontroller;

			if (is_null(json_decode($view))) {
				// OLD
				$keywords = explode("-", str_replace(array("\n", "\t", "\r", "\a", "/", "//",":"), "-",$view));
				$controllerView = $keywords[3];
				$viewRedi = $keywords[4];
				$folder = $keywords[0];
			} else {
				// NEW
				$keywords = json_decode($view);
				$controllerView = $keywords->controller;
				$viewRedi = $keywords->function;
				$folder = str_replace('/', '\\', $keywords->folder);
			}
			
			$urlnya = 'App\Http\Controllers';
			$existsfile = $urlnya." \ ".$folder." \ ".$controllerView;
			$tamba = $urlnya." \ ".$folder." \ ".$controllerView;
			$tamba = str_replace(" ","",$tamba);
			$existsfile = str_replace(" ","",$existsfile);
			if (!class_exists($tamba)) {
				return ErrorControllerFunc::UnderMaintenance2($slug);
			} else if (!method_exists($existsfile,$viewRedi)) {
				return ErrorControllerFunc::UnderMaintenanceFun($slug);
			} else {
				return app($tamba)->$viewRedi($slug);
			}
		}


		if ($widget == null) {
			$widgetgrid = null;
			$widgetgridfield = array();
			$widgetgridfield2 = array();
			$namatable = null;
		} else {
			$widgetgrid = WidgetGrid::where('idcmswidget', $widget->noid)->first();
			// dd($widgetgrid);

			if ($widgetgrid == null) {
				$widgetgridfield ='';
				$namatable ='';
				$tabledata ='';
				// code...
			} else {
				$widgetgridfield = WidgetGridField::where('idcmswidgetgrid', $widgetgrid->noid)->where('colshow',1)->get();
				$namatable = $slug;
				$tabledata =  DB::connection('mysql2')->table($widget->maintable)->first();
			}

		}
		// dd($namaview);

		return view('lam_modules.'.$namaview.'', compact(
			'noid',
			'menu',
			'widget',
			'widgetgrid',
			'widgetgridfield',
			'widgetgridfield2',
			'departement',
			'public',
			'myusername',
			'idhomepagelink',
			'page',
			'panel',
			'page_title',
			'namatable',
			'idhomepage',
			'page_description')
		);
	}
}
