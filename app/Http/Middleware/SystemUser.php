<?php

namespace App\Http\Middleware;

use Closure;
use App\Menu;
use App\Page;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class SystemUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

     public function handle($request, Closure $next)
         {
             $slug = $request->getPathInfo();
             // dd($slug);
             $str = ltrim($request->getPathInfo(), '/');

             if ($slug == '') {
               return redirect('/');

             }
             return $next($request);
             // else{
               $menu = Menu::where('noid',$str)->first();

               if (is_null($menu))
                   return redirect('/');
               $page = Page::where('noid',$menu->linkidpage)->first();
               if ($page->idtypepage == 2)
               dd('2');
              return redirect()->route('dashboard', [$str]);
                           return $next($request);
              if ($page->idtypepage == 9)
              return redirect()->route('custom', [$str]);


                           return $next($request);
              // }


           }
    // public function handle($request, Closure $next)
    // {
    //     $slug = $request->getPathInfo();
    //     $str = ltrim($request->getPathInfo(), '/');
    //     $menu = Menu::where('noid',$str)->first();
    //
    //     if (is_null($menu))
    //         return redirect('/');
    //
    //     $page = Page::where('noid',$menu->linkidpage)->first();
    //
    //     if ($page->idtypepage == 2) {
    //         $halaman = 'custom';
    //         return redirect($halaman,[$str]);
    //     }
    //
    //     if ($page->idtypepage == 9) {
    //         $halaman = 'dashboard';
    //         return redirect($halaman);
    //     }
    //
    //     return $next($request);
    // }
}
