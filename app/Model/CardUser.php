<?php

namespace App\Model;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class CardUser extends Authenticatable
{
    use Notifiable;
    protected $table = 'mcard';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
          'kode',
          'nama',
          'firstname',
          'middlename',
          'lastname',
          'gelar',
          'email',
          'idtypecard',
          'isgroupuser',
          'isuser',
          'isemployee',
          'iscustomer',
          'issupplier',
          'is3rparty',
          'isactive',
          'profilepicture',
          'idcreate',
          'docreate',
          'idupdate',
          'lastupdate',
    ];
    public function dataUser(){
    //setiap profil memiliki satu mahasiswa
    return $this->hasOne('App\Model\User','myusername');
  }

}
