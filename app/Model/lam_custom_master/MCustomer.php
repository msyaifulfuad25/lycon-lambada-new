<?php

namespace App\Model\lam_custom_master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
Use DB;
use Illuminate\Support\Facades\Session;
use App\Helpers\MyHelper;
use App\Model\Sales\SalesOrder;

class MCustomer extends Model {
    use Notifiable;

    protected $connection = 'mysql2';
    protected $table = 'mcardcustomer';

    public $main = [
        'table' => 'mcardcustomer',
        'tablemilestone' => 'prjmrabmilestone',
        'tableheader' => 'prjmrabheader',
        'tabledetail' => 'prjmrabdetail',
        'tablecdf' => 'prjmrabdocument',

        'table2' => 'prjmrap',
        'tablemilestone2' => 'prjmrapmilestone',
        'tableheader2' => 'prjmrapheader',
        'tabledetail2' => 'prjmrapdetail',
        'tablecdf2' => 'prjmrapdocument',

        'tabletemplate' => 'prjmtemplate',
        'tabletemplatemilestone' => 'prjmtemplatemilestone',
        'tabletemplateheader' => 'prjmtemplateheader',
        'idtypetranc' => 602,
        'idtypetranc2' => 604,
        'generatecode2' => 'generatecode/402/1',
    ];

    public static function getData($params) {
        $table = $params['table'];
        $select = @$params['select'] ? $params['select'] : ['noid','nama'];
        $connection = @$params['connection'] ? $params['connection'] : 'mysql2';
        $isactive = @$params['isactive'] ? $params['isactive'] : false;
        $where = @$params['where'];
        $orderby = @$params['orderby'];
        if ($isactive) $select[] = 'isactive';

        $result = DB::connection($connection)
                    ->table($table)
                    ->select($select)
                    ->where(function($query) use ($isactive) {
                        if ($isactive) $query->where('isactive', 1);
                    })
                    ->where(function($query) use ($where) {
                        if ($where) {
                            foreach ($where as $k => $v) {
                                $query->where($v[0], $v[1], $v[2]);
                            }
                        }
                    })
                    ->orderByRaw('noid=0 desc');
        
        if (@$orderby) {
            if (count($orderby)>0) {
                foreach ($orderby as $k => $v) {
                    $result->orderBy($v[0],$v[1]);
                }
            }
        } else {
            $result->orderBy('noid', 'asc');
        }

        $result->limit(500);
        
        $result = $result->get();
                    
        return $result;
    }

    public static function getMilestoneAndHeader($params) {
        $_idmaster = $params['idmaster'];
        $resultmilestone = DB::connection('mysql2')
                            ->table((new static)->main['tablemilestone'])
                            ->select([
                                'noid',
                                'idmaster',
                                'nama',
                                DB::raw("DATE_FORMAT(tanggalstart, '%d-%b-%Y') as tanggalstart"),
                                DB::raw("DATE_FORMAT(tanggalstop, '%d-%b-%Y') as tanggalstop"),
                                DB::raw("DATE_FORMAT(tanggalfinished, '%d-%b-%Y') as tanggalfinished"),
                                'keterangan',
                                'nourut',
                                'idcardpj',
                                'idcardpjwakil',
                            ])
                            ->where('idmaster',$_idmaster)
                            ->orderBy('nourut','asc')
                            ->get();

        $resultheader = DB::connection('mysql2')
                            ->table((new static)->main['tableheader'])
                            ->select([
                                'noid',
                                'idmaster',
                                'idmilestone',
                                'nama',
                                DB::raw("DATE_FORMAT(tanggalstart, '%d-%b-%Y') as tanggalstart"),
                                DB::raw("DATE_FORMAT(tanggalstop, '%d-%b-%Y') as tanggalstop"),
                                DB::raw("DATE_FORMAT(tanggalfinished, '%d-%b-%Y') as tanggalfinished"),
                                'keterangan',
                                'nourut',
                                'idcardpj',
                                'idcardpjwakil',
                            ])
                            ->where('idmaster',$_idmaster)
                            ->orderBy('idmilestone','asc')
                            ->orderBy('nourut','asc')
                            ->get();

        $resultdetail = DB::connection('mysql2')
                        ->table((new static)->main['tabledetail'].' AS prd')
                        ->leftJoin('invminventory AS imi', 'prd.idinventor', '=', 'imi.noid')
                        ->leftJoin('invmsatuan AS ims', 'prd.idsatuan', '=', 'ims.noid')
                        ->select([
                            'prd.noid AS noid',
                            'prd.idheader AS idheader',
                            'prd.idinventor AS idinventor',
                            'imi.kode AS kodeinventor',
                            'imi.nama AS namainventor',
                            'prd.namainventor AS namainventor2',
                            'prd.unitqty AS unitqty',
                            'prd.idcompany AS idcompany',
                            'prd.iddepartment AS iddepartment',
                            'prd.idgudang AS idgudang',
                            'prd.keterangan AS keterangan',
                            'prd.idsatuan AS idsatuan',
                            'ims.nama AS namasatuan',
                            'prd.konvsatuan AS konvsatuan',
                            'prd.unitqtysisa AS unitqtysisa',
                            'prd.unitprice AS unitprice',
                            'prd.subtotal AS subtotal',
                            'prd.discountvar AS discountvar',
                            'prd.discount AS discount',
                            'prd.nilaidisc AS nilaidisc',
                            'prd.idtypetax AS idtypetax',
                            'prd.idtax AS idtax',
                            'prd.prosentax AS prosentax',
                            'prd.nilaitax AS nilaitax',
                            'prd.hargatotal AS hargatotal',
                        ])
                        ->where('prd.idmaster', $_idmaster)
                        ->orderBy('imi.kode', 'asc')
                        ->get();

        $resultmcard = DB::connection('mysql2')
                        ->table('mcard')
                        ->get();

        $mcard = [];
        foreach ($resultmcard as $k => $v) {
            $mcard[$v->noid] = $v;
        }

        $header = [];
        foreach ($resultdetail as $k => $v) {
            $key = "header-$v->idheader";
            if (!array_key_exists($key, $header)) {
                $header[$key] = [];
            }
            array_push($header[$key], $v);
        }

        $milestone = [];
        foreach ($resultheader as $k => $v) {
            $key = "milestone-$v->idmilestone";
            if (!array_key_exists($key, $milestone)) {
                $milestone[$key] = [];
            }
            $v->idelement = 'header-'.$v->noid;
            $v->idcardpj_nama = $mcard[$v->idcardpj]->nama;
            $v->idcardpjwakil_nama = $mcard[$v->idcardpjwakil]->nama;
            $v->detail = array_key_exists("header-$v->noid",$header)
                ? $header["header-$v->noid"]
                : [];
            array_push($milestone[$key], $v);
        }

        $datamilestone = [];
        foreach ($resultmilestone as $k => $v) {
            $v->header = array_key_exists("milestone-$v->noid",$milestone)
                ? $milestone["milestone-$v->noid"]
                : [];
            $v->idelement = 'milestone-'.$v->noid;
            $v->idcardpj_nama = $mcard[$v->idcardpj]->nama;
            $v->idcardpjwakil_nama = $mcard[$v->idcardpjwakil]->nama;
            $datamilestone[] = $v;
        }

        $listimimilestone = (new static)->getInvMInventory(['where'=>[['imi.isnote','=','1']]]);
        $listimiheader = (new static)->getInvMInventory(['where'=>[['imi.isnote','=','1']]]);
        $listmcard = (new static)->getData(['table'=>'mcard','select'=>['noid','kode','nama','isemployee','iscustomer','issupplier']]);
                 
        return [
            'milestone' => $datamilestone,
            'list' => [
                'milestone' => $resultmilestone,
                'imimilestone' => $listimimilestone,
                'imiheader' => $listimiheader,
                'mcard' => $listmcard
            ]
        ];
    }

    public static function getDetailInv($params) {
        $querypo = DB::connection('mysql2')
                    ->table('invminventory AS imi')
                    ->leftJoin('invmsatuan AS ims', 'imi.idsatuan', '=', 'ims.noid')
                    ->select([
                        // 'tdp.noid AS noid',
                        // 'tdp.idmaster AS idmaster',
                        // 'tp.kode AS kodepo',
                        'imi.noid AS idinventor',
                        // 'tdp.idgudang AS idgudang',
                        // 'tdp.idcompany AS idcompany',
                        // 'tdp.iddepartment AS iddepartment',
                        'imi.idsatuan AS idsatuan',
                        'imi.konversi AS konvsatuan',
                        // 'tdp.subtotal AS subtotal',
                        // 'tdp.discountvar AS discountvar',
                        // 'tdp.discount AS discount',
                        // 'tdp.nilaidisc AS nilaidisc',
                        // 'tdp.idtypetax AS idtypetax',
                        // 'tdp.idtax AS idtax',
                        // 'tdp.prosentax AS prosentax',
                        // 'tdp.nilaitax AS nilaitax',
                        // 'tp.kode AS purchaseorder_kode',
                        'imi.kode AS kodeinventor',
                        'imi.nama AS namainventor',
                        // 'tdp.namainventor AS namainventor2',
                        // 'tdp.keterangan AS keterangan',
                        // 'tdp.unitqty AS unitqty',
                        // 'tdp.unitqtysisa AS unitqtysisa',
                        'ims.nama AS namasatuan',
                        'imi.purchaseprice AS unitprice',
                        // 'tdp.hargatotal AS hargatotal',
                        // 'amc.nama AS namacurrency',
                    ])
                    ->where('imi.noid', '!=', 0)
                    ->where('imi.isactive', '!=', 0)
                    ->where('imi.isjual', '!=', 0);
                    // ->where('tp.idstatustranc', 3)
                    // ->where('tp.idcardsupplier', $params['idcardsupplier'])
                    // ->where('tdp.unitqtysisa', '>', 0);
        
        if ($params['searchprev']) {
            $searchprev = $params['searchprev'];
            $querypo->where(function($query) use ($searchprev) {
                $query->where('imi.kode', 'like', "%$searchprev%")
                    ->orWhere('imi.nama', 'like', "%$searchprev%")
                    ->orWhere('ims.nama', 'like', "%$searchprev%")
                    ->orWhere('imi.purchaseprice', 'like', "%$searchprev%");
            });
        }

        // $result->whereNotIn('tdp.noid', $detailprev)
        //         ->orderBy('tdp.noid', 'asc');
        
        if ($params['length'] != null && $params['length'] != null) {
            $querypo->offset($params['start'])
                    ->limit($params['length']);
        }

        $resultprev = $querypo->get();

        $result = DB::connection('mysql2')
                    ->table((new static)->main['tabledetail'])
                    ->where('idtrancprev', $params['searchprev'])
                    ->get();

        $res = [];
        foreach ($result as $k => $v) {
            $res[$v->idtrancprevd] = @$res[$v->idtrancprevd] ? $res[$v->idtrancprevd]+$v->unitqty : $v->unitqty;
        }

        $response = [];
        foreach ($resultprev as $k => $v) {
            $response[$k] = $v;
            // if (array_key_exists($v->noid, $res)) {
            //     $response[$k]->unitqtysisa = $v->unitqtysisa-$res[$v->noid];
            // }
        }

        return $response;
    }

    public static function getAllNoid($table) {
        $result = DB::connection('mysql2')
                    ->table($table)
                    ->select('noid')
                    ->get();

        $response = [];
        foreach($result as $k => $v) {
            $response[] = $v->noid;
        }
        return $response;
    }

    public static function findMTypeTranc() {
        $result = DB::connection('mysql2')
                    ->table('mtypetranc')
                    ->select(['noid','kode','nama'])
                    ->where('noid', (new static)->main['idtypetranc'])
                    ->orderBy('noid', 'asc')
                    ->first();
        return $result;
    }

    public static function findMTypeTrancTypeStatus($idtypetranc) {
        $result = DB::connection('mysql2')
                    ->table('mtypetranctypestatus')
                    ->select(['noid','idtypetranc','kode','nama','classicon','classcolor'])
                    ->where('idtypetranc', $idtypetranc)
                    ->where('kode', 'DRF')
                    ->orderBy('nourut', 'asc')
                    ->first();
        return $result;
    }

    public static function getLastMonth() {
        $result = DB::connection('mysql2')->select("SELECT year(max(tanggal)) AS _year, month(max(tanggal)) AS _month FROM ".(new static)->main['table']." LIMIT 1");
        return $result ? $result[0] : (object)['_year'=>date('Y'),'_month'=>date('m')];
    }

    public static function getGenMCompany() {
        $result = DB::connection('mysql2')
                    ->table('genmdepartment AS gmd')
                    ->leftJoin('genmcompany AS gmc', 'gmd.idcompany', '=', 'gmc.noid')
                    ->select([
                        'gmd.noid AS genmdepartment_noid',
                        'gmd.kode AS genmdepartment_kode',
                        'gmd.nama AS genmdepartment_nama',
                        'gmc.noid AS genmcompany_noid',
                        'gmc.kode AS genmcompany_kode',
                        'gmc.nama AS genmcompany_nama',
                    ])
                    ->orderBy('gmc.nama', 'asc')
                    ->orderBy('gmd.nama', 'asc')
                    ->get();
        return $result;
    }

    public static function getInvMInventory($params=[]) {
        $result = DB::connection('mysql2')
            ->table('invminventory AS imi')
            ->leftJoin('invmsatuan AS ims', 'imi.idsatuan', '=', 'ims.noid')
            ->select([
                'imi.noid AS noid',
                'imi.kode AS kode',
                'imi.nama AS nama',
                'imi.idsatuan AS idsatuan',
                'ims.nama AS namasatuan',
                'imi.konversi AS konversi',
                'imi.idtax AS idtax',
                'imi.taxprocent AS taxprocent',
                'imi.purchaseprice AS purchaseprice',
                'imi.purchaseprice AS purchaseprice2',
                'imi.isjual AS isjual',
                'imi.isbeli AS isbeli',
                'imi.issimpan AS issimpan',
                'imi.isnote AS isnote',
            ]);
        
        if (@$params['where']) {
            foreach ($params['where'] as $k => $v) {
                $result->where($v[0],$v[1],$v[2]);
            }
        }

        $result = $result->orderBy('imi.nama', 'asc')->get();
        return $result;
    }

    public static function getMaster($start=null, $length=null, $search, $orderby, $filter=null, $datefilter, $datefilter2) {
        if ($search) {
            $result = DB::connection('mysql2')
                ->table((new static)->main['table'].' AS m')
                ->leftJoin('mlokasiprop AS mlprop', 'mlprop.noid', '=', 'm.idlokasiprop')
                ->leftJoin('mlokasikab AS mlkab', 'mlkab.noid', '=', 'm.idlokasikota')
                ->leftJoin('mlokasikec AS mlkec', 'mlkec.noid', '=', 'm.idlokasikec')
                ->leftJoin('mlokasikel AS mlkel', 'mlkel.noid', '=', 'm.idlokasikel')
                ->select([
                    'm.noid',
                    'm.kode',
                    'm.nama',
                    'm.namaalias',
                    'm.nophone',
                    'm.email',
                    'mlprop.nama AS idlokasiprop_nama',
                    'mlkab.nama AS idlokasikota_nama',
                    'mlkec.nama AS idlokasikec_nama',
                    'mlkel.nama AS idlokasikel_nama',
                    'm.isactive',
                ])
                ->where(function($query) use($search) {
                    $query->orWhere('m.kode', 'like', "%$search%")
                        ->orWhere('m.nama', 'like', "%$search%")
                        ->orWhere('m.namaalias', 'like', "%$search%")
                        ->orWhere('m.nophone', 'like', "%$search%")
                        ->orWhere('m.email', 'like', "%$search%")
                        ->orWhere('mlprop.nama', 'like', "%$search%")
                        ->orWhere('mlkab.nama', 'like', "%$search%")
                        ->orWhere('mlkec.nama', 'like', "%$search%")
                        ->orWhere('mlkel.nama', 'like', "%$search%")
                        ->orWhere('m.isactive', 'like', "%$search%");
                });

            // $resultdatefilterfrom = DB::connection('mysql2')
            //                             ->table((new static)->main['table'])
            //                             ->select('docreate');
            // $resultdatefilterto = DB::connection('mysql2')
            //                             ->table((new static)->main['table'])
            //                             ->select('docreate');

            // if ($filter['is'] != 'all') {
            //     $result->where('m.'.$filter['is'], '=', 1);
            // }

            // if ($datefilter['code'] == 'a') {
            //     $resultf = $resultdatefilterfrom->orderBy('docreate', 'asc')->first() ? date('M d, Y', strtotime($resultdatefilterfrom->orderBy('docreate', 'asc')->first()->docreate)) : date('M d, Y');
            //     $resultt = $resultdatefilterto->orderBy('docreate', 'desc')->first() ? date('M d, Y', strtotime($resultdatefilterto->orderBy('docreate', 'desc')->first()->docreate)) : date('M d, Y');
            // } else if ($datefilter['code'] == 't') {
            //     $result->whereDate('m.docreate', date('Y-m-d'));
            //     $resultf = date('M d, Y');
            //     $resultt = date('M d, Y');
            // } else if ($datefilter['code'] == 'y') {
            //     $result->whereDate('m.docreate', date('Y-m-d', strtotime('-1 day')));
            //     $resultf = date('M d, Y', strtotime('-1 day'));
            //     $resultt = date('M d, Y', strtotime('-1 day'));
            // } else if ($datefilter['code'] == 'l7d') {
            //     // $result->whereRaw('m.docreate >= DATE(NOW()) - INTERVAL 7 DAY');
            //     $result->whereDate('m.docreate', '<=', date('Y-m-d'));
            //     $result->whereDate('m.docreate', '>=', date('Y-m-d', strtotime('-7 day')));
            //     $resultf = date('M d, Y', strtotime('-7 day'));
            //     $resultt = date('M d, Y');
            // } else if ($datefilter['code'] == 'l30d') {
            //     $result->whereDate('m.docreate', '<=', date('Y-m-d'));
            //     $result->whereDate('m.docreate', '>=', date('Y-m-d', strtotime('-1 month')));
            //     $resultf = date('M d, Y', strtotime('-1 month'));
            //     $resultt = date('M d, Y');
            // } else if ($datefilter['code'] == 'tm') {
            //     $result->whereYear('m.docreate', date('Y', strtotime('this year')));
            //     $result->whereMonth('m.docreate', date('m', strtotime('this month')));
            //     $resultf = date('M d, Y', strtotime('first day of this month'));
            //     $resultt = date('M d, Y', strtotime('last day of this month'));
            // } else if ($datefilter['code'] == 'lm') {
            //     $lmm = date('m', strtotime('-1 months'));
            //     $lmy = $lmm == 12 ? date('Y', strtotime('-1 years')) : date('Y');
            //     $result->whereYear('m.docreate', $lmy);
            //     $result->whereMonth('m.docreate', $lmm);
            //     $resultf = date('M d, Y', strtotime('first day of previous month'));
            //     $resultt = date('M d, Y', strtotime('last day of previous month'));
            // } else if ($datefilter['code'] == 'cr') {
            //     $result->whereDate('m.docreate', '>=', date('Y-m-d', strtotime($datefilter['custom']['from'])));
            //     $result->whereDate('m.docreate', '<=', date('Y-m-d', strtotime($datefilter['custom']['to'])));
            //     $resultf = date('M d, Y', strtotime($datefilter['custom']['from']));
            //     $resultt = date('M d, Y', strtotime($datefilter['custom']['to']));
            // }

            // $result->whereDate('m.docreate', '>=', $datefilter2['from']);
            // $result->whereDate('m.docreate', '<=', $datefilter2['to']);

            $result->orderBy(
                $orderby['column'] == 0 ? 'm.noid' : $orderby['column'], 
                $orderby['column'] == 0 ? 'asc' : $orderby['sort']
            );

            if ($start != null && $length != null) {
                $result->offset($start)
                        ->limit($length);
            }
        } else {
            $result = DB::connection('mysql2')
                ->table((new static)->main['table'].' AS m')
                ->leftJoin('mlokasiprop AS mlprop', 'mlprop.noid', '=', 'm.idlokasiprop')
                ->leftJoin('mlokasikab AS mlkab', 'mlkab.noid', '=', 'm.idlokasikota')
                ->leftJoin('mlokasikec AS mlkec', 'mlkec.noid', '=', 'm.idlokasikec')
                ->leftJoin('mlokasikel AS mlkel', 'mlkel.noid', '=', 'm.idlokasikel')
                ->select([
                    'm.noid',
                    'm.kode',
                    'm.nama',
                    'm.namaalias',
                    'm.nophone',
                    'm.email',
                    'mlprop.nama AS idlokasiprop_nama',
                    'mlkab.nama AS idlokasikota_nama',
                    'mlkec.nama AS idlokasikec_nama',
                    'mlkel.nama AS idlokasikel_nama',
                    'm.isactive',
                ]);
                        
            // $resultdatefilterfrom = DB::connection('mysql2')
            //                             ->table((new static)->main['table'])
            //                             ->select('docreate');
            // $resultdatefilterto = DB::connection('mysql2')
            //                             ->table((new static)->main['table'])
            //                             ->select('docreate');

            // if ($filter['is'] != 'all') {
            //     $result->where('m.'.$filter['is'], '=', 1);
            // }

            // if ($datefilter['code'] == 'a') {
            //     $resultf = $resultdatefilterfrom->orderBy('docreate', 'asc')->first() ? date('M d, Y', strtotime($resultdatefilterfrom->orderBy('docreate', 'asc')->first()->docreate)) : date('M d, Y');
            //     $resultt = $resultdatefilterto->orderBy('docreate', 'desc')->first() ? date('M d, Y', strtotime($resultdatefilterto->orderBy('docreate', 'desc')->first()->docreate)) : date('M d, Y');
            // } else if ($datefilter['code'] == 't') {
            //     $result->whereDate('m.docreate', date('Y-m-d'));
            //     $resultf = date('M d, Y');
            //     $resultt = date('M d, Y');
            // } else if ($datefilter['code'] == 'y') {
            //     $result->whereDate('m.docreate', date('Y-m-d', strtotime('-1 day')));
            //     $resultf = date('M d, Y', strtotime('-1 day'));
            //     $resultt = date('M d, Y', strtotime('-1 day'));
            // } else if ($datefilter['code'] == 'l7d') {
            //     // $result->whereRaw('m.docreate >= DATE(NOW()) - INTERVAL 7 DAY');
            //     $result->whereDate('m.docreate', '<=', date('Y-m-d'));
            //     $result->whereDate('m.docreate', '>=', date('Y-m-d', strtotime('-7 day')));
            //     $resultf = date('M d, Y', strtotime('-7 day'));
            //     $resultt = date('M d, Y');
            // } else if ($datefilter['code'] == 'l30d') {
            //     $result->whereDate('m.docreate', '<=', date('Y-m-d'));
            //     $result->whereDate('m.docreate', '>=', date('Y-m-d', strtotime('-1 month')));
            //     $resultf = date('M d, Y', strtotime('-1 month'));
            //     $resultt = date('M d, Y');
            // } else if ($datefilter['code'] == 'tm') {
            //     $result->whereYear('m.docreate', date('Y', strtotime('this year')));
            //     $result->whereMonth('m.docreate', date('m', strtotime('this month')));
            //     $resultf = date('M d, Y', strtotime('first day of this month'));
            //     $resultt = date('M d, Y', strtotime('last day of this month'));
            // } else if ($datefilter['code'] == 'lm') {
            //     $lmm = date('m', strtotime('-1 months'));
            //     $lmy = $lmm == 12 ? date('Y', strtotime('-1 years')) : date('Y');
            //     $result->whereYear('m.docreate', $lmy);
            //     $result->whereMonth('m.docreate', $lmm);
            //     $resultf = date('M d, Y', strtotime('first day of previous month'));
            //     $resultt = date('M d, Y', strtotime('last day of previous month'));
            // } else if ($datefilter['code'] == 'cr') {
            //     $result->whereDate('m.docreate', '>=', date('Y-m-d', strtotime($datefilter['custom']['from'])));
            //     $result->whereDate('m.docreate', '<=', date('Y-m-d', strtotime($datefilter['custom']['to'])));
            //     $resultf = date('M d, Y', strtotime($datefilter['custom']['from']));
            //     $resultt = date('M d, Y', strtotime($datefilter['custom']['to']));
            // }

            // $result->whereDate('m.docreate', '>=', $datefilter2['from']);
            // $result->whereDate('m.docreate', '<=', $datefilter2['to']);

            $result->orderBy(
                $orderby['column'] == 0 ? 'm.noid' : $orderby['column'], 
                $orderby['column'] == 0 ? 'asc' : $orderby['sort']
            );
            
            if ($start != null && $length != null) {
                $result->offset($start)
                        ->limit($length);
            }
        }
        
        return [
            'datefilter' => [
                'type' => $datefilter['code'],
                // 'from' => $resultf,
                // 'to' => $resultt
            ],
            'data' => $result->get(),
        ];
    }

    public static function getLog($noid, $kode, $start, $length, $search, $orderby) {
        if ($search) {
            $result = DB::connection('mysql2')
                        ->table('applog AS al')
                        ->leftJoin('appmtypeaction AS amta', 'al.idtypeaction', '=', 'amta.noid')
                        ->leftJoin('mcarduser AS mcu', 'al.idcreate', '=', 'mcu.noid')
                        ->leftJoin((new static)->main['table'].' AS prq', 'al.idreff', '=', 'prq.noid')
                        ->select([
                            'al.noid AS noid',
                            'amta.nama AS idtypeaction',
                            'al.logsubject AS logsubject',
                            'al.keterangan AS keterangan',
                            'al.idreff AS idreff',
                            'prq.kode AS kode',
                            'mcu.myusername AS idcreate',
                            'al.docreate AS docreate'
                        ])
                        ->where('al.idreff', $noid)
                        ->where('al.idtypetranc', (new static)->main['idtypetranc'])
                        ->where('al.logsubject', 'like', "%$kode%")
                        ->where(function($query) use ($search) {
                            $query->where('al.noid', 'like', "%$search%");
                            $query->orWhere('amta.nama', 'like', "%$search%");
                            $query->orWhere('al.logsubject', 'like', "%$search%");
                            $query->orWhere('al.keterangan', 'like', "%$search%");
                            $query->orWhere('mcu.myusername', 'like', "%$search%");
                            $query->orWhere('al.docreate', 'like', "%$search%");
                        })
                        ->orderBy($orderby['column'], $orderby['sort'])
                        ->offset($start)
                        ->limit($length)
                        ->get();
        } else {
            $result = DB::connection('mysql2')
                        ->table('applog AS al')
                        ->leftJoin('mtypetranc AS mtt', 'al.idtypetranc', '=', 'mtt.noid')
                        ->leftJoin('appmtypeaction AS amta', 'al.idtypeaction', '=', 'amta.noid')
                        ->leftJoin('mcarduser AS mcu', 'al.idcreate', '=', 'mcu.noid')
                        ->leftJoin((new static)->main['table'].' AS prq', 'al.idreff', '=', 'prq.noid')
                        ->select([
                            'al.noid AS noid',
                            'amta.nama AS idtypeaction',
                            'al.logsubject AS logsubject',
                            'al.keterangan AS keterangan',
                            'al.idreff AS idreff',
                            'prq.kode AS kode',
                            'mcu.myusername AS idcreate',
                            'al.docreate AS docreate'
                        ])
                        ->where('al.idreff', $noid)
                        ->where('al.idtypetranc', (new static)->main['idtypetranc'])
                        ->where('al.logsubject', 'like', "%$kode%")
                        ->orderBy($orderby['column'], $orderby['sort'])
                        ->offset($start)
                        ->limit($length)
                        ->get();
        }
        return $result;
    }

    public static function getDefaultCostControl() {
        $result = DB::connection('mysql2')
                    ->table('salesorder')
                    ->where('noid', 0)
                    ->first();
        return $result;
    }

    public static function getGenerateTemplate($params) {
        $_tanggal = $params['tanggal'];
        $_tanggaldue = $params['tanggaldue'];
        $_start = $params['start'];
        $_length = $params['length'];
        $_search = $params['search'];
        $_orderby = $params['orderby'];
        $_tabletemplate = (new static)->main['tabletemplate'];
        $_tabletemplatemilestone = (new static)->main['tabletemplatemilestone'];
        $_tabletemplateheader = (new static)->main['tabletemplateheader'];

        if ($_search && false) {
            $result = DB::connection('mysql2')
                        ->table('salesorder AS so')
                        ->leftJoin('mtypecostcontrol AS mtcc', 'so.idtypecostcontrol', '=', 'mtcc.noid')
                        ->leftJoin('mtypetranctypestatus AS mttts', 'so.idstatustranc', '=', 'mttts.noid')
                        ->leftJoin('mtypetranc AS mtt', 'so.idtypetranc', '=', 'mtt.noid')
                        ->leftJoin('genmcompany AS gmc', 'so.idcompany', '=', 'gmc.noid')
                        ->leftJoin('genmdepartment AS gmd', 'so.iddepartment', '=', 'gmd.noid')
                        ->leftJoin('mcarduser AS mcu', 'so.idcreate', '=', 'mcu.noid')
                        ->select([
                            'so.noid',
                            'mtt.nama AS mtt_nama',
                            'mttts.nama AS mttts_nama',
                            'so.kode',
                            'so.kodereff',
                            'so.tanggal',
                            'so.idtypecostcontrol',
                            'mtcc.kode AS mtcc_kode',
                            'mtcc.nama AS mtcc_nama',
                            'so.generaltotal',
                            'so.keterangan',
                            'mcu.myusername AS mcu_myusername',
                            'gmc.nama AS gmc_nama',
                            'gmd.nama AS gmd_nama',
                        ])
                        ->where('so.noid', 'like', "%$_search%")
                        ->orWhere('mttts.nama', 'like', "%$_search%")
                        ->orWhere('mtt.nama', 'like', "%$_search%")
                        ->orWhere('so.kode', 'like', "%$_search%")
                        ->orWhere('so.kodereff', 'like', "%$_search%")
                        ->orWhere('gmc.nama', 'like', "%$_search%")
                        ->orWhere('gmd.nama', 'like', "%$_search%")
                        ->orWhere('so.generaltotal', 'like', "%$_search%")
                        ->orWhere('so.keterangan', 'like', "%$_search%")
                        ->orWhere('so.tanggal', 'like', "%$_search%")
                        ->orderBy($_orderby['column'], $_orderby['sort'])
                        ->offset($_start)
                        ->limit($_length)
                        ->get();
        } else {
            $resulttemplate = DB::connection('mysql2')
                                ->table($_tabletemplate.' AS tt')
                                ->leftJoin('accmcurrency AS amc','tt.idcurrency','=','amc.noid')
                                ->select([
                                    'tt.noid AS noid',
                                    'tt.kode AS kode',
                                    'tt.kodereff AS kodereff',
                                    'tt.keterangan AS keterangan',
                                    'tt.idcurrency AS idcurrency',
                                    'amc.nama AS amc_nama',
                                    'tt.konvcurr AS konvcurr'
                                ])
                                ->orderBy('tt.'.$_orderby['column'], $_orderby['sort'])
                                ->offset($_start)
                                ->limit($_length);
            if ($_search) {
                $resulttemplate->where(function($query) use ($_search) {
                    $query->orWhere('tt.kode', 'like', "%$_search%")
                        ->orWhere('tt.kodereff', 'like', "%$_search%")
                        ->orWhere('tt.keterangan', 'like', "%$_search%")
                        ->orWhere('amc.nama', 'like', "%$_search%")
                        ->orWhere('tt.konvcurr', 'like', "%$_search%");
                });
            }
            $resulttemplate = $resulttemplate->get();

            $resultmilestone = DB::connection('mysql2')
                                ->table($_tabletemplatemilestone)
                                ->select([
                                    'noid',
                                    'idmaster',
                                    'idinventor',
                                    'nama',
                                    DB::raw("DATE_FORMAT(tanggalstart, '%d-%b-%Y') as tanggalstart"),
                                    DB::raw("DATE_FORMAT(tanggalstop, '%d-%b-%Y') as tanggalstop"),
                                    DB::raw("DATE_FORMAT(tanggalfinished, '%d-%b-%Y') as tanggalfinished"),
                                    'keterangan',
                                    'nourut',
                                    'idcardpj',
                                    'idcardpjwakil',
                                ])
                                ->orderBy('nourut','asc')
                                ->get();

            $resultheader = DB::connection('mysql2')
                                ->table($_tabletemplateheader)
                                ->select([
                                    'noid',
                                    'idmaster',
                                    'idmilestone',
                                    'idinventor',
                                    'nama',
                                    DB::raw("DATE_FORMAT(tanggalstart, '%d-%b-%Y') as tanggalstart"),
                                    DB::raw("DATE_FORMAT(tanggalstop, '%d-%b-%Y') as tanggalstop"),
                                    DB::raw("DATE_FORMAT(tanggalfinished, '%d-%b-%Y') as tanggalfinished"),
                                    'keterangan',
                                    'nourut',
                                    'idcardpj',
                                    'idcardpjwakil',
                                ])
                                ->orderBy('idmilestone','asc')
                                ->orderBy('nourut','asc')
                                ->get();

            $resultmcard = DB::connection('mysql2')
                            ->table('mcard')
                            ->get();

            $mcard = [];
            foreach ($resultmcard as $k => $v) {
                $mcard[$v->noid] = $v;
            }
            
            $milestone = [];
            foreach ($resultheader as $k => $v) {
                $key = "milestone-$v->idmilestone";
                if (!array_key_exists($key, $milestone)) {
                    $milestone[$key] = [];
                }
                // if (strtotime($v->tanggalstart) <= strtotime($_tanggal)) {
                //     $v->tanggalstart = $_tanggal;
                // }
                // if (strtotime($v->tanggalstop) >= strtotime($_tanggaldue) || strtotime($v->tanggalstop) <= $v->tanggalstart) {
                //     $v->tanggalstop = $_tanggaldue;
                // }
                // $v->tanggalstart = 
                $v->idelement = 'header-'.$v->noid;
                $v->idcardpj_nama = $mcard[$v->idcardpj]->nama;
                $v->idcardpjwakil_nama = $mcard[$v->idcardpjwakil]->nama;
                $v->iscollapse = true;
                $v->detail = [];
                array_push($milestone[$key], $v);
            }

            $datamilestone = [];
            foreach ($resultmilestone as $k => $v) {
                $key = "template-$v->idmaster";
                if (!array_key_exists($key, $datamilestone)) {
                    $datamilestone[$key] = [];
                }
                $v->header = array_key_exists("milestone-$v->noid",$milestone)
                    ? $milestone["milestone-$v->noid"]
                    : [];
                $v->idelement = 'milestone-'.$v->noid;
                $v->idcardpj_nama = $mcard[$v->idcardpj]->nama;
                $v->idcardpjwakil_nama = $mcard[$v->idcardpjwakil]->nama;
                $v->iscollapse = true;
                array_push($datamilestone[$key], $v);
            }

            $listtemplate = [];
            foreach ($resulttemplate as $k => $v) {
                $v->milestone = @$datamilestone['template-'.$v->noid]
                    ? $datamilestone['template-'.$v->noid]
                    : [];
                $listtemplate[] = $v;
            }
        }
        return $listtemplate;
    }

    public static function getMCardSupplier() {
        $result = DB::connection('mysql2')
                    ->table('mcardsupplier')
                    ->leftJoin('mlokasiprop','mcardsupplier.idlokasiprop','=','mlokasiprop.noid')
                    ->leftJoin('mlokasikab','mcardsupplier.idlokasikota','=','mlokasikab.noid')
                    ->leftJoin('mlokasikec','mcardsupplier.idlokasikec','=','mlokasikec.noid')
                    ->leftJoin('mlokasikel','mcardsupplier.idlokasikel','=','mlokasikel.noid')
                    ->select([
                        'mcardsupplier.noid AS mcardsupplier_noid',
                        'mcardsupplier.nama AS mcardsupplier_nama',
                        'mlokasiprop.noid AS mlokasiprop_noid',
                        'mlokasiprop.kode AS mlokasiprop_kode',
                        'mlokasiprop.nama AS mlokasiprop_nama',
                        'mlokasikab.noid AS mlokasikab_noid',
                        'mlokasikab.kode AS mlokasikab_kode',
                        'mlokasikab.nama AS mlokasikab_nama',
                        'mlokasikec.noid AS mlokasikec_noid',
                        'mlokasikec.kode AS mlokasikec_kode',
                        'mlokasikec.nama AS mlokasikec_nama',
                        'mlokasikel.noid AS mlokasikel_noid',
                        'mlokasikel.kode AS mlokasikel_kode',
                        'mlokasikel.nama AS mlokasikel_nama',
                    ])
                    ->orderBy('mcardsupplier_noid', 'asc')
                    ->get();
        return $result;
    }

    public static function getMTypeTrancTypeStatus($idtypetranc) {
        $result = DB::connection('mysql2')
                    ->table('mtypetranctypestatus AS mttts')
                    ->leftJoin('appmtypeaction AS amta', 'mttts.noid', '=', 'amta.idstatustrancbase')
                    ->select([
                        'mttts.noid',
                        'mttts.idstatusbase',
                        'mttts.nourut',
                        'mttts.kode',
                        'mttts.nama',
                        'mttts.classicon',
                        'mttts.classcolor',
                        'mttts.isinternal',
                        'mttts.isexternal',
                        'mttts.actview',
                        'mttts.actedit',
                        'mttts.actdelete',
                        'mttts.actreportdetail',
                        'mttts.actreportmaster',
                        'mttts.actsetprevstatus',
                        'mttts.actsetnextstatus',
                        'mttts.actsetstatus',
                        'amta.noid AS appmtypeaction_noid',
                        'amta.nama AS appmtypeaction_nama'
                    ])
                    ->where('mttts.idtypetranc', $idtypetranc)
                    ->where('mttts.isactive', '>', 0)
                    ->orderBy('mttts.nourut', 'asc')
                    ->get();
        return $result;
    }

    public static function saveData($params) {
        $insert = DB::connection('mysql2')
            ->table((new static)->main['table'])
            ->insert($params['formmaster']);

        if (!$insert) return ['success' => false, 'message' => 'Error Insert'];

        return ['success' => true, 'message' => 'Success Insert Data'];
    }

    public static function saveMilestone($params) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['tablemilestone'])
                    ->insert($params['form']);
        
        return $result;
    }
    
    public static function updateData($params) {
        $update = DB::connection('mysql2')
            ->table((new static)->main['table'])
            ->where('noid', $params['noid'])
            ->update($params['formmaster']);
        if (!$update) return ['success' => false, 'message' => 'Error Update'];

        return ['success' => true, 'message' => 'Success Update Data'];
    }

    public static function getNextNoid($table) {
        $result = DB::connection('mysql2')
            ->table($table)
            ->orderBy('noid', 'desc')
            ->first();
        
        return $result ? $result->noid+1 : 1;
    }

    public static function getNextNourut($table) {
        $result = DB::connection('mysql2')
            ->table($table)
            ->orderBy('nourut', 'desc')
            ->first();
        
        return $result ? $result->nourut+1 : 1;
    }
    
    public static function getKode($table,$noid) {
        $result = DB::connection('mysql2')
                    ->table($table)
                    ->select('kode')
                    ->where('noid', $noid)
                    ->first();
        
        return $result ? $result->kode : '';
    }

    public static function getLastNoid($table) {
        $result = DB::connection('mysql2')
                    ->table($table)
                    ->orderBy('noid', 'desc')
                    ->first();
        
        return $result ? $result->noid : 1;
    }

    public static function findLog($noid, $kode) {
        $result = DB::connection('mysql2')
                    ->table('applog')
                    ->select('noid')
                    ->where('idreff', $noid)
                    ->where('logsubject', 'like', "%$kode%")
                    ->orderBy('noid', 'asc')
                    ->get();
        return $result;
    }

    public static function findData($noid) {
        $master = DB::connection('mysql2')
            ->table((new static)->main['table'].' AS m')
            ->leftJoin('mlokasiprop AS mlprop', 'mlprop.noid', '=', 'm.idlokasiprop')
            ->leftJoin('mlokasikab AS mlkab', 'mlkab.noid', '=', 'm.idlokasikota')
            ->leftJoin('mlokasikec AS mlkec', 'mlkec.noid', '=', 'm.idlokasikec')
            ->leftJoin('mlokasikel AS mlkel', 'mlkel.noid', '=', 'm.idlokasikel')
            ->select([
                'm.noid',
                'm.kode',
                'm.nama',
                'm.namaalias',
                'm.nophone',
                'm.nofax',
                'm.email',
                'm.keterangan',
                'm.idlokasiprop',
                'm.idlokasikota',
                'm.idlokasikec',
                'm.idlokasikel',
                'mlprop.nama AS idlokasiprop_nama',
                'mlkab.nama AS idlokasikota_nama',
                'mlkec.nama AS idlokasikec_nama',
                'mlkel.nama AS idlokasikel_nama',
                'm.alamatrw',
                'm.alamatrt',
                'm.alamat1',
                'm.isactive',
            ])
            ->where('m.noid', $noid)
            ->first();
        if (!$master) return ['success' => false, 'message' => 'Error Find'];

        return [
            'success' => true, 
            'message' => 'Success Find Data', 
            'data' => [
                'master' => $master,
                // 'findgeneral' => $findgeneral,
                // 'findaddress' => $findaddress,
                // 'findcontact' => $findcontact,
                // 'finduser' => @$finduser,
                // 'findprivilidge' => @$findprivilidge,
                // 'findemployee' => @$findemployee,
                // 'findcustomer' => @$findcustomer,
                // 'findsupplier' => @$findsupplier,
            ]
        ];


        $result = DB::connection('mysql2')
            ->table((new static)->main['table'].' AS pr')
            ->leftJoin('mtypetranc AS mtt', 'pr.idtypetranc', '=', 'mtt.noid')
            ->leftJoin('mtypetranctypestatus AS mttts', 'pr.idstatustranc', '=', 'mttts.noid')
            ->leftJoin('mcarduser AS mcu', 'pr.idstatuscard', '=', 'mcu.noid')
            ->leftJoin('accmcurrency AS amc', 'pr.idcurrency', '=', 'amc.noid')
            ->select([
                'pr.*',
                'mtt.kode AS kodetypetranc',
                'mttts.nama AS mtypetranctypestatus_nama',
                'mttts.classcolor AS mtypetranctypestatus_classcolor',
                'mcu.myusername AS mcarduser_myusername',
                'amc.nama AS namacurrency',
            ])
            ->where('pr.noid', $noid)
            ->first();

        $resultmilestone = DB::connection('mysql2')
            ->table((new static)->main['tablemilestone'])
            ->select([
                'noid',
                'idmaster',
                'idinventor',
                'nama',
                DB::raw("DATE_FORMAT(tanggalstart, '%d-%b-%Y') as tanggalstart"),
                DB::raw("DATE_FORMAT(tanggalstop, '%d-%b-%Y') as tanggalstop"),
                DB::raw("DATE_FORMAT(tanggalfinished, '%d-%b-%Y') as tanggalfinished"),
                'keterangan',
                'nourut',
                'idcardpj',
                'idcardpjwakil',
            ])
            ->where('idmaster',$noid)
            ->orderBy('nourut','asc')
            ->get();

        $resultheader = DB::connection('mysql2')
            ->table((new static)->main['tableheader'])
            ->select([
                'noid',
                'idmaster',
                'idmilestone',
                'idinventor',
                'nama',
                DB::raw("DATE_FORMAT(tanggalstart, '%d-%b-%Y') as tanggalstart"),
                DB::raw("DATE_FORMAT(tanggalstop, '%d-%b-%Y') as tanggalstop"),
                DB::raw("DATE_FORMAT(tanggalfinished, '%d-%b-%Y') as tanggalfinished"),
                'keterangan',
                'nourut',
                'idcardpj',
                'idcardpjwakil',
            ])
            ->where('idmaster',$noid)
            ->orderBy('idmilestone','asc')
            ->orderBy('nourut','asc')
            ->get();

        $resultdetail = DB::connection('mysql2')
            ->table((new static)->main['tabledetail'].' AS prd')
            ->leftJoin('invminventory AS imi', 'prd.idinventor', '=', 'imi.noid')
            ->leftJoin('invmsatuan AS ims', 'prd.idsatuan', '=', 'ims.noid')
            ->select([
                'prd.noid AS noid',
                'prd.idheader AS idheader',
                'prd.idinventor AS idinventor',
                'imi.kode AS kodeinventor',
                'imi.nama AS namainventor',
                'prd.namainventor AS namainventor2',
                'prd.unitqty AS unitqty',
                'prd.idcompany AS idcompany',
                'prd.iddepartment AS iddepartment',
                'prd.idgudang AS idgudang',
                'prd.keterangan AS keterangan',
                'prd.idsatuan AS idsatuan',
                'ims.nama AS namasatuan',
                'prd.konvsatuan AS konvsatuan',
                'prd.unitqtysisa AS unitqtysisa',
                'prd.unitprice AS unitprice',
                'prd.subtotal AS subtotal',
                'prd.discountvar AS discountvar',
                'prd.discount AS discount',
                'prd.nilaidisc AS nilaidisc',
                'prd.idtypetax AS idtypetax',
                'prd.idtax AS idtax',
                'prd.prosentax AS prosentax',
                'prd.nilaitax AS nilaitax',
                'prd.hargatotal AS hargatotal',
            ])
            ->where('prd.idmaster', $noid)
            ->orderBy('imi.kode', 'asc')
            ->get();

        $resultmcard = DB::connection('mysql2')->table('mcard')->get();

        $mcard = [];
        foreach ($resultmcard as $k => $v) {
            $mcard[$v->noid] = $v;
        }

        $header = [];
        foreach ($resultdetail as $k => $v) {
            $key = "header-$v->idheader";
            if (!array_key_exists($key, $header)) {
                $header[$key] = [];
            }
            array_push($header[$key], $v);
        }

        $milestone = [];
        foreach ($resultheader as $k => $v) {
            $key = "milestone-$v->idmilestone";
            if (!array_key_exists($key, $milestone)) {
                $milestone[$key] = [];
            }
            $v->idelement = 'header-'.$v->noid;
            $v->iscollapse = true;
            $v->idcardpj_nama = $mcard[$v->idcardpj]->nama;
            $v->idcardpjwakil_nama = $mcard[$v->idcardpjwakil]->nama;
            $v->detail = array_key_exists("header-$v->noid",$header)
                ? $header["header-$v->noid"]
                : [];
            array_push($milestone[$key], $v);
        }

        $datamilestone = [];
        foreach ($resultmilestone as $k => $v) {
            $v->header = array_key_exists("milestone-$v->noid",$milestone)
                ? $milestone["milestone-$v->noid"]
                : [];
            $v->idelement = 'milestone-'.$v->noid;
            $v->iscollapse = true;
            $v->idcardpj_nama = $mcard[$v->idcardpj]->nama;
            $v->idcardpjwakil_nama = $mcard[$v->idcardpjwakil]->nama;
            $datamilestone[] = $v;
        }

        $resultcdf = DB::connection('mysql2')
            ->table((new static)->main['tablecdf'].' AS cdf')
            ->leftJoin('dmsmfile AS dmf', 'dmf.noid', '=', 'cdf.iddmsfile')
            ->select([
                'cdf.noid AS cdf_noid',
                'cdf.nama AS cdf_nama',
                'cdf.keterangan AS cdf_keterangan',
                'cdf.iddmsfile AS cdf_iddmsfile',
                'dmf.filename AS dmf_filename',
                'dmf.filepreview AS dmf_filepreview'
            ])
            ->where('cdf.idrab', $noid)
            ->orderBy('cdf.noid', 'asc')
            ->get();

        $cdf = [];
        foreach ($resultcdf as $k => $v) {
            $dmf_filename = explode('.', $v->dmf_filename);
            if (end($dmf_filename) == 'jpg' ||
                end($dmf_filename) == 'jpeg' ||
                end($dmf_filename) == 'png' ||
                end($dmf_filename) == 'gif') {
                $dmf_filepreview = $v->dmf_filepreview;
                $dmf_urlfilepreview = $v->dmf_filepreview.'/'.$v->dmf_filename;
            } else {
                $dmf_filepreview = 'filemanager/default';
                $dmf_urlfilepreview = 'filemanager/default/'.end($dmf_filename).'.png';
            }

            $cdf[] = (object)[
                'cdf_noid' => $v->cdf_noid,
                'cdf_nama' => $v->cdf_nama,
                'cdf_keterangan' => $v->cdf_keterangan,
                'cdf_iddmsfile' => $v->cdf_iddmsfile,
                'dmf_filename' => $v->dmf_filename,
                'dmf_filepreview' => $dmf_filepreview,
                'dmf_urlfilepreview' => $dmf_urlfilepreview,
            ];
        }

        $data = [
            'master' => $result,
            // 'purchase' => $result,
            'milestone' => $datamilestone,
            // 'purchasedetail' => $resultdetail ? $resultdetail : [],
            'cdf' => $cdf
        ];
        // dd($data);

        return $data;
    }

    public static function findSalesOrder($params) {
        $result = DB::connection('mysql2')
            ->table('salesorder')
            ->where('kodereff', $params['kodereff'])
            ->first();
        return $result ? $result : [];
    }

    public static function findHeader($params) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['tableheader'])
                    ->select([
                        'noid',
                        'idmaster',
                        'idmilestone',
                        'tanggalstart',
                        'tanggalstop',
                        'tanggalfinished',
                        'keterangan',
                    ])
                    ->where('idmilestone',$params['idmilestone'])
                    ->where('idmaster',$params['idmaster'])
                    ->orderBy('idmilestone','asc')
                    ->orderBy('nourut','asc')
                    ->get();
        return $result;
    }

    public static function findDetail($params) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['tabledetail'].' AS prd')
                    ->leftJoin('invminventory AS imi', 'prd.idinventor', '=', 'imi.noid')
                    ->leftJoin('invmsatuan AS ims', 'prd.idsatuan', '=', 'ims.noid')
                    ->select([
                        'prd.noid AS noid',
                        'prd.idheader AS idheader',
                        'prd.idinventor AS idinventor',
                        'imi.kode AS kodeinventor',
                        'imi.nama AS namainventor',
                        'prd.namainventor AS namainventor2',
                        'prd.unitqty AS unitqty',
                        'prd.idcompany AS idcompany',
                        'prd.iddepartment AS iddepartment',
                        'prd.idgudang AS idgudang',
                        'prd.keterangan AS keterangan',
                        'prd.idsatuan AS idsatuan',
                        'ims.nama AS namasatuan',
                        'prd.konvsatuan AS konvsatuan',
                        'prd.unitqtysisa AS unitqtysisa',
                        'prd.unitprice AS unitprice',
                        'prd.subtotal AS subtotal',
                        'prd.discountvar AS discountvar',
                        'prd.discount AS discount',
                        'prd.nilaidisc AS nilaidisc',
                        'prd.idtypetax AS idtypetax',
                        'prd.idtax AS idtax',
                        'prd.prosentax AS prosentax',
                        'prd.nilaitax AS nilaitax',
                        'prd.hargatotal AS hargatotal',
                    ])
                    ->where('prd.idheader', $params['idheader'])
                    ->where('prd.idmaster', $params['idmaster'])
                    ->orderBy('imi.kode', 'asc')
                    ->get();
        return $result;
    }

    public static function findLastSupplier() {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->select([
                        'noid',
                        'idcardsupplier',
                        'idakunsupplier',
                        'suppliernama',
                        'supplieraddress1',
                        'supplieraddress2',
                        'supplieraddressrt',
                        'supplieraddressrw',
                        'supplierlokasikel',
                        'supplierlokasikec',
                        'supplierlokasikab',
                        'supplierlokasiprop',
                        'supplierlokasineg',
                    ])
                    ->orderBy('noid', 'desc')
                    ->first();

        return $result;
    }

    public static function sendToNext($params) {
        $generatecode = $params['generatecode'];
        $data = $params['find'];
        $idtrancprev = $data['master']->noid;
        $idstatustrancprq = $data['master']->idstatustranc;
        $data['master']->noid = $params['nextnoid'];
        $data['master']->idstatustranc = 6041;
        $data['master']->idtypetranc = (new static)->main['idtypetranc2'];
        $data['master']->kodereff = $data['master']->kode;
        $data['master']->kode = $generatecode['kode'];
        $data['master']->nourut = $params['datalog'][1]['idreff'];
        $data['master']->idtypetrancprev = (new static)->main['idtypetranc'];
        $data['master']->idtrancprev = $idtrancprev;
        unset($data['master']->mtypetranctypestatus_nama);
        unset($data['master']->mtypetranctypestatus_classcolor);
        unset($data['master']->mcarduser_myusername);
        unset($data['master']->kodecostcontrol);
        unset($data['master']->namacurrency);
        unset($data['master']->kodetypetranc);
        unset($data['master']->idcardpj_nama);

        $milestone = [];
        $header = [];
        $detail = [];
        $cdf = [];
        $nextnoidmilestone = (new static)->getNextNoid((new static)->main['tablemilestone2']);
        $nextnoidheader = (new static)->getNextNoid((new static)->main['tableheader2']);
        $nextnoiddetail = (new static)->getNextNoid((new static)->main['tabledetail2']);
        $nextnoidcdf = (new static)->getNextNoid((new static)->main['tablecdf2']);

        foreach ($data['milestone'] as $k => $v) {
            $v = (array)$v;
            $v['noid'] = $nextnoidmilestone;
            $v['idmaster'] = $params['nextnoid'];
            $v['tanggalstart'] = MyHelper::humandateTodbdate($v['tanggalstart']);
            $v['tanggalstop'] = MyHelper::humandateTodbdate($v['tanggalstop']);
            $v['tanggalfinished'] = MyHelper::humandateTodbdate($v['tanggalfinished']);

            foreach ($v['header'] as $k2 => $v2) {
                $v2 = (array)$v2;
                $v2['noid'] = $nextnoidheader;
                $v2['idmaster'] = $params['nextnoid'];
                $v2['idmilestone'] = $nextnoidmilestone;
                $v2['tanggalstart'] = MyHelper::humandateTodbdate($v2['tanggalstart']);
                $v2['tanggalstop'] = MyHelper::humandateTodbdate($v2['tanggalstop']);
                $v2['tanggalfinished'] = MyHelper::humandateTodbdate($v2['tanggalfinished']);

                foreach ($v2['detail'] as $k3 => $v3) {
                    $v3 = (array)$v3;
                    $v3['noid'] = $nextnoiddetail;
                    $v3['idmaster'] = $params['nextnoid'];
                    $v3['idheader'] = $nextnoidheader;

                    unset($v3['kodeinventor']);
                    unset($v3['namainventor2']);
                    unset($v3['namasatuan']);
                    unset($v3['idcardpj_nama']);
                    unset($v3['idcardpjwakil_nama']);
                    unset($v3['idelement']);
                    unset($v3['iscollapse']);
                    $detail[] = $v3;
                    $nextnoiddetail++;
                }

                unset($v2['idcardpj_nama']);
                unset($v2['idcardpjwakil_nama']);
                unset($v2['idelement']);
                unset($v2['iscollapse']);
                unset($v2['detail']);
                $header[] = $v2;
                $nextnoidheader++;
            }

            unset($v['idcardpj_nama']);
            unset($v['idcardpjwakil_nama']);
            unset($v['idelement']);
            unset($v['iscollapse']);
            unset($v['header']);
            $milestone[] = $v;
            $nextnoidmilestone++;
        }

        foreach ((array)$params['find']['cdf'] as $k => $v) {
            $cdf[] = [
                'noid' => $nextnoidcdf,
                'idrap' => $params['nextnoid'],
                'idstatusfile' => 3,
                'idcardstatus' => 1001,
                'dostatus' => date('Y-m-d H:i:s'),
                'nama' => $v->cdf_nama,
                'namaalias' => $v->cdf_nama,
                'keterangan' => $v->cdf_keterangan,
                'iddmsfile' => $v->cdf_iddmsfile,
                'idcreate' => Session::get('noid'),
                'docreate' => date('Y-m-d H:i:s')
            ];
            $nextnoidcdf++;
        }

        $insertmaster = DB::connection('mysql2')->table((new static)->main['table2'])->insert((array)$data['master']);
        if (!$insertmaster) return false;

        $insertmilestone = DB::connection('mysql2')->table((new static)->main['tablemilestone2'])->insert($milestone);
        if (!$insertmilestone) return false;

        $insertheader = DB::connection('mysql2')->table((new static)->main['tableheader2'])->insert($header);
        if (!$insertheader) return false;

        $insertdetail = DB::connection('mysql2')->table((new static)->main['tabledetail2'])->insert($detail);
        if (!$insertdetail) return false;

        $insertcdf = DB::connection('mysql2')->table((new static)->main['tablecdf2'])->insert($cdf);
        if (!$insertcdf) return false;

        return true;
    }

    public static function snst($params) {
        if ($params['idstatustranc'] == 6024) {
            if ($params['answer'] == 1) {
                $insertso = DB::connection('mysql2')->table($params['tableso'])->insert($params['so']);
                if (!$insertso) return ['success' => false, 'message' => 'Error Insert Sales Order'];
        
                $insertsodetail = DB::connection('mysql2')->table($params['tablesod'])->insert($params['sod']);
                if (!$insertsodetail) return ['success' => false, 'message' => 'Error Insert Detail Sales Order'];
        
                $updaterab = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $params['noid'])
                    ->update(['idcostcontrol'=>$params['so']['noid'],'idstatustranc'=>6025]);
                if (!$updaterab) return ['success' => false, 'message' => 'Error Update RAB'];
            } else if ($params['answer'] == 2) {
                $updaterab = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $params['noid'])
                    ->update(['idstatustranc'=>6027]);
                if (!$updaterab) return ['success' => false, 'message' => 'Error Update RAB'];
                
                $insertmaster = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->insert($params['rab']);
                if (!$insertmaster) return ['success' => false, 'message' => 'Error Insert Master'];
                
                $insertmilestone = DB::connection('mysql2')
                    ->table((new static)->main['tablemilestone'])
                    ->insert($params['milestone']);
                if (!$insertmilestone) return ['success' => false, 'message' => 'Error Insert Milestone'];

                $insertheader = DB::connection('mysql2')
                    ->table((new static)->main['tableheader'])
                    ->insert($params['header']);
                if (!$insertheader) return ['success' => false, 'message' => 'Error Insert Header'];

                $insertdetail = DB::connection('mysql2')
                    ->table((new static)->main['tabledetail'])
                    ->insert($params['detail']);
                if (!$insertdetail) return ['success' => false, 'message' => 'Error Insert Detail'];

                $insertcdf = DB::connection('mysql2')
                    ->table((new static)->main['tablecdf'])
                    ->insert($params['cdf']);
                if (!$insertcdf) return ['success' => false, 'message' => 'Error Insert File'];
            } else if ($params['answer'] == 3) {
                $updaterab = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $params['noid'])
                    ->update(['idstatustranc'=>6027]);
                if (!$updaterab) return ['success' => false, 'message' => 'Error Update RAB'];
            }
        } else {
            // Update Master
            $updatemaster = DB::connection('mysql2')
                ->table((new static)->main['table'])
                ->where('noid', $params['noid'])
                ->update($params['data']);
            if (!$updatemaster) return ['success' => false, 'message' => 'Error Update Master'];
        }

        // Insert Log
        $insertlog = DB::connection('mysql2')
            ->table('applog')
            ->insert($params['datalog']);
        if (!$insertlog) return ['success' => false, 'message' => 'Error Insert Log'];

        return ['success' => true, 'message' => 'Success'];
    }

    public static function div($data, $noid, $datalog) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($data);

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultlog ? 1 : 0;
    }

    public static function sapcf($data, $noid, $datalog) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($data);

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultlog ? 1 : 0;
    }

    public static function setPending($data, $noid, $datalog) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($data);

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultlog ? 1 : 0;
    }

    public static function setCanceled($data, $noid, $datalog) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($data);

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultlog ? 1 : 0;
    }

    public static function getStringBetween($str,$from,$to) {
        $string = substr($str, strpos($str, $from) + strlen($from));
        if (strstr($string,$to,TRUE) != FALSE) {
            $string = strstr($string,$to,TRUE);
        } else {
            $string = FALSE;
        }
        return $string;
    }

    public static function generateCode($params) {
        $tablemaster = $params['tablemaster'];
        $costcontrol = (new static)->getDefaultCostControl();
        $idcostcontrol = @$params['idcostcontrol'] ? $params['idcostcontrol'] : $costcontrol->noid;
        $kodecostcontrol = @$params['kodecostcontrol'] ? $params['kodecostcontrol'] : $costcontrol->kode;
        $formdefault = explode('/', $params['generatecode']);
        
        $result_appmkode = DB::connection('mysql2')->table('appmkode')->where('noid', $formdefault[1])->first();
        $result_appmkodedetail = DB::connection('mysql2')->table('appmkodedetail')->where('idkode', $formdefault[1])->orderBy('nourut', 'ASC')->get();


        $formdefault = '';
        foreach ($result_appmkodedetail as $k2 => $v2) {
            if ($v2->kodename == 'KODE') {
                $formdefault .= $v2->kodevalue;
            } else if ($v2->kodename == 'FIELDTABLE') {
                $fungsigroupby = explode(';',$v2->fungsigroupby);
                $resultfieldtable = DB::connection('mysql2')->table($v2->kodeformat)->where($fungsigroupby[0], $params['data'][$fungsigroupby[1]])->first();
                $fieldname = $v2->fieldname;
                $formdefault .= str_replace('[FIELDTABLE]', $resultfieldtable->$fieldname, $v2->kodevalue);
            } else if ($v2->kodename == 'NOURUT') {
                $groupby = '';
                $fungsigroupby = explode(';', $v2->fungsigroupby);
                $select = '';
                $arrayselect = [];
                $where = 'WHERE 1=1 ';
                if ($v2->fungsigroupby != '' && $v2->fungsigroupby != null) {
                    $groupby .= 'GROUP BY ';
                    foreach ($fungsigroupby as $k3 => $v3) {
                        if ($v3) {
                            $groupby .= $v3.',';
                        }
                        $select .= $v3.',';
                        $arrayselect[] = $v3;

                        $_gsb_month = (new static)->getStringBetween($v3,'MONTH(',')');
                        $_gsb_year = (new static)->getStringBetween($v3,'YEAR(',')');

                        if (@$params['data'][$_gsb_month]) {
                            $where .= 'AND '.$v3.'='.date('m', strtotime($params['data'][$_gsb_month])).' ';
                        } else if (@$params['data'][$_gsb_year]) {
                            $where .= 'AND '.$v3.'='.date('Y', strtotime($params['data'][$_gsb_year])).' ';
                        } else {
                            $where .= 'AND '.$v3.'='.$params['data'][$v3].' ';
                        }
                    }
                    $groupby = substr($groupby, 0 ,-1);
                }
                $fieldname = $v2->fieldname;
                $fungsi = $v2->fungsi;
                // $qtylastnourut = "SELECT MAX(nourut) as qtylnu FROM $tablemaster GROUP BY $groupby";

                $qtylastnourut = "SELECT $select $fungsi($fieldname) as qtylnu FROM $tablemaster $where $groupby";
                $resultqtylnu = DB::connection('mysql2')->select($qtylastnourut);

                

                $qtylnu = 1;
                if (count($resultqtylnu) > 0) {
                    foreach($resultqtylnu as $k3 => $v3) {
                        $found = false; 

                        if (count((array)$v3) > 1) {
                            foreach($v3 as $k4 => $v4) {
                                $_gsb_month = (new static)->getStringBetween($k4,'MONTH(',')');
                                $_gsb_year = (new static)->getStringBetween($k4,'YEAR(',')');
    
                                if ($k4 != 'qtylnu') {
                                    // print($v4.'-'.@(string)$params[$k4].'////');
                                    if (@$params['data'][$_gsb_month] || @$params['data'][$_gsb_year]) {
                                        $qtylnu = $v3->qtylnu+1;
                                        // $found = true;
                                        // break;
                                    } else if ((string)$v4 == @(string)$params['data'][$k4]) {
                                        $qtylnu = $v3->qtylnu+1;
                                        // $found = true;
                                        // break;
                                    } else {
                                        $found = false;
                                    }
                                }
                            }
                        } else {
                            $qtylnu = $v3->qtylnu+1;
                            $found = true;
                            break;
                        }

                        if ($found) {
                            break;
                        }
                    }
                }
                // dd($qtylnu);
                // die;
                // $fungsigroupby = explode(';', $v2->fungsigroupby);
                // // $query_nourut = "SELECT MAX(noid) AS noid FROM $tablemaster WHERE $fungsigroupby[2] = $v2->idkode GROUP BY ";
                // $query_nourut = "SELECT MAX(docreate) AS noid FROM $tablemaster ";
                // foreach ($fungsigroupby as $k3 => $v3) {
                //     if ($v3 != '') {
                //         if ($k3 == 0) {
                //             $query_nourut .= 'GROUP BY ';
                //         }
                //         if ($v3) {
                //             $query_nourut .= "$v3,";
                //         }
                //     }
                // }
                // $query_nourut = substr($query_nourut, 0, -1);
                // $query_nourut .= " LIMIT 1";
                // $result_nourut = DB::connection('mysql2')->select($query_nourut);
                // if ($result_nourut) {
                //     $nourut = substr($v2->kodeformat, 0, strlen($v2->kodeformat)-strlen($result_nourut[0]->noid)).$result_nourut[0]->noid;
                // } else {
                //     $nourut = 0;
                // }
                $nourut = '';
                $idx = 0;
                for ($i=strlen($qtylnu); $i<strlen($v2->kodeformat); $i ++) {
                    $nourut .= $v2->kodeformat[$idx];
                    $idx++;
                }
                $nourut .= $qtylnu;
                $formdefault .= $nourut;
            } else if ($v2->kodename == 'BULAN') {
                $formdefault .= str_replace('[BULAN]', date('m', strtotime($params['data']['tanggal'])), $v2->kodevalue);
                // dd(date(explode('%', $v2->kodeformat)[1]));
            } else if ($v2->kodename == 'TAHUN') {
                $formdefault .= str_replace('[TAHUN]', date('Y', strtotime($params['data']['tanggal'])), $v2->kodevalue);
            }
        }
        
        $resultnourut = DB::connection('mysql2')->select("SELECT MAX(nourut) AS nourut FROM $tablemaster");

        return [
            'kode' => $formdefault,
            // 'nourut' => count($resultnourut) == 0 ? 1 : $resultnourut[0]->nourut+1,
            'nourut' => $qtylnu,
        ];
    }

    public static function deleteSelectM($params) {
        $delete = DB::connection('mysql2')->table((new static)->main['table']);
        (bool)$params['selectcheckboxmall']
            ? $delete->delete()
            : $delete->whereIn('noid', $params['selecteds'])->delete();
        if (!$delete) return ['success' => false, 'message' => 'Error Delete'];

        if ($params['datalog']) {
            $insertlog = DB::connection('mysql2')
                ->table('applog')
                ->insert($params['datalog']);
            if (!$insertlog) return ['success' => false, 'message' => 'Error Insert Log'];
        }

        return ['success' => true, 'message' => 'Success Delete'];
    }

    public static function deleteData($params) {
        $find = DB::connection('mysql2')
            ->table((new static)->main['table'])
            ->where('noid', $params['noid'])
            ->first();

        return ['success' => true, 'message' => 'Success Delete'];
    }

    static function removeData($params) {
        $where = @$params['where'];

        $result = DB::connection('mysql2')
            ->table($params['table'])
            ->where(function($query) use ($where) {
                if ($where) {
                    foreach ($where as $k => $v) {
                        $query->where($v[0], $v[1], $v[2]);
                    }
                }
            });

        $get = $result->get();
        if (!$get) return ['success' => false, 'message' => 'Data not found'];
        if (count($get) > 0)
            if (!$result->delete()) return ['success' => false, 'message' => 'Error Remove Data '.$params['table']];
        
        return ['success' => true, 'message' => 'Success Remove Data'];
    }

}
