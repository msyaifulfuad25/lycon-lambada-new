<?php

namespace App\Model\Sales;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
Use DB;

class SalesOrder extends Model {
    use Notifiable;

    protected $connection = 'mysql2';
    protected $table = 'salesorder';

    public $main = [
        'table' => 'salesorder',
        'tabledetail' => 'salesorderdetail',
        'tablecdf' => 'dmsmdocument',
        'idtypetranc' => 402,
        'idtypetranc2' => 604,
        'table2' => 'prjmrab',
        'tablemilestone2' => 'prjmrabmilestone',
        'tableheader2' => 'prjmrabheader',
        'tabledetail2' => 'prjmrabdetail',
        'tablecdf2' => 'prjmrabdocument',
        'generatecode2' => 'generatecode/604/1',
    ];

    public static function isRule($params) {
        $mcardkode = $params['mcardkode'];
        $mpositionkode = $params['mpositionkode'];

        $resultmcardposition = DB::connection('mysql2')
            ->table('mcardposition AS mcp')
            ->select([
                '*'
            ])
            ->leftJoin('mposition AS mp', 'mp.noid', '=', 'mcp.idposition')
            ->where('mcp.kodecard', $mcardkode)
            ->where('mp.kode', $mpositionkode)
            ->first();

        if (!$resultmcardposition) return false;
        return true;
    }
    
    public static function getData($params) {
        $table = $params['table'];
        $select = @$params['select'] ? $params['select'] : ['noid','nama'];
        $connection = @$params['connection'] ? $params['connection'] : 'mysql2';
        $isactive = @$params['isactive'];
        $orderby = @$params['orderby'];
        if ($isactive) $select[] = 'isactive';

        $result = DB::connection($connection)
                    ->table($table)
                    ->select($select)
                    ->where(function($query) use ($isactive) {
                        if ($isactive) $query->where('isactive', 1);
                    })
                    ->orderByRaw('noid=0 desc');
        
        if (@$orderby) {
            if (count($orderby)>0) {
                foreach ($orderby as $k => $v) {
                    $result->orderBy($v[0],$v[1]);
                }
            }
        } else {
            $result->orderBy('noid', 'asc');
        }
        
        $result = $result->get();
                    
        return $result;
    }

    public static function getDetailInv($params) {
        $querypo = DB::connection('mysql2')
                    ->table('invminventory AS imi')
                    ->leftJoin('invmsatuan AS ims', 'imi.idsatuan', '=', 'ims.noid')
                    ->select([
                        // 'tdp.noid AS noid',
                        // 'tdp.idmaster AS idmaster',
                        // 'tp.kode AS kodepo',
                        'imi.noid AS idinventor',
                        // 'tdp.idgudang AS idgudang',
                        // 'tdp.idcompany AS idcompany',
                        // 'tdp.iddepartment AS iddepartment',
                        'imi.idsatuan AS idsatuan',
                        'imi.konversi AS konvsatuan',
                        // 'tdp.subtotal AS subtotal',
                        // 'tdp.discountvar AS discountvar',
                        // 'tdp.discount AS discount',
                        // 'tdp.nilaidisc AS nilaidisc',
                        // 'tdp.idtypetax AS idtypetax',
                        // 'tdp.idtax AS idtax',
                        // 'tdp.prosentax AS prosentax',
                        // 'tdp.nilaitax AS nilaitax',
                        // 'tp.kode AS purchaseorder_kode',
                        'imi.kode AS kodeinventor',
                        'imi.nama AS namainventor',
                        // 'tdp.namainventor AS namainventor2',
                        // 'tdp.keterangan AS keterangan',
                        // 'tdp.unitqty AS unitqty',
                        // 'tdp.unitqtysisa AS unitqtysisa',
                        'ims.nama AS namasatuan',
                        'imi.purchaseprice AS unitprice',
                        // 'tdp.hargatotal AS hargatotal',
                        // 'amc.nama AS namacurrency',
                    ])
                    ->where('imi.noid', '!=', 0)
                    ->where('imi.isactive', '!=', 0);
                    // ->where('tp.idstatustranc', 3)
                    // ->where('tp.idcardsupplier', $params['idcardsupplier'])
                    // ->where('tdp.unitqtysisa', '>', 0);
        
        if ($params['searchprev']) {
            $searchprev = $params['searchprev'];
            $querypo->where(function($query) use ($searchprev) {
                $query->where('imi.kode', 'like', "%$searchprev%")
                    ->orWhere('imi.nama', 'like', "%$searchprev%");
            });
        }

        // $result->whereNotIn('tdp.noid', $detailprev)
        //         ->orderBy('tdp.noid', 'asc');
        
        // if ($params['length'] != null && $params['length'] != null) {
        //     $querypo->offset($params['start'])
        //             ->limit($params['length']);
        // }

        $resultprev = $querypo->get();

        $result = DB::connection('mysql2')
                    ->table((new static)->main['tabledetail'])
                    ->where('idtrancprev', $params['searchprev'])
                    ->get();

        $res = [];
        foreach ($result as $k => $v) {
            $res[$v->idtrancprevd] = @$res[$v->idtrancprevd] ? $res[$v->idtrancprevd]+$v->unitqty : $v->unitqty;
        }

        $response = [];
        foreach ($resultprev as $k => $v) {
            $response[$k] = $v;
            // if (array_key_exists($v->noid, $res)) {
            //     $response[$k]->unitqtysisa = $v->unitqtysisa-$res[$v->noid];
            // }
        }

        return $response;
    }

    public static function getAllNoid($table) {
        $result = DB::connection('mysql2')
                    ->table($table)
                    ->select('noid')
                    ->get();

        $response = [];
        foreach($result as $k => $v) {
            $response[] = $v->noid;
        }
        return $response;
    }

    public static function findMTypeTranc() {
        $result = DB::connection('mysql2')
                    ->table('mtypetranc')
                    ->select(['noid','kode','nama'])
                    ->where('noid', (new static)->main['idtypetranc'])
                    ->orderBy('noid', 'asc')
                    ->first();
        return $result;
    }

    public static function findMTypeTrancTypeStatus($idtypetranc) {
        $result = DB::connection('mysql2')
                    ->table('mtypetranctypestatus')
                    ->select(['noid','idtypetranc','kode','nama','classicon','classcolor'])
                    ->where('idtypetranc', $idtypetranc)
                    ->where('kode', 'DRF')
                    ->orderBy('nourut', 'asc')
                    ->first();
        return $result;
    }

    public static function getLastMonth() {
        $result = DB::connection('mysql2')->select("SELECT year(max(tanggal)) AS _year, month(max(tanggal)) AS _month FROM ".(new static)->main['table']." LIMIT 1");
        return $result ? $result[0] : (object)['_year'=>date('Y'),'_month'=>date('m')];
    }

    public static function getGenMCompany() {
        $result = DB::connection('mysql2')
                    ->table('genmdepartment AS gmd')
                    ->leftJoin('genmcompany AS gmc', 'gmd.idcompany', '=', 'gmc.noid')
                    ->select([
                        'gmd.noid AS genmdepartment_noid',
                        'gmd.kode AS genmdepartment_kode',
                        'gmd.nama AS genmdepartment_nama',
                        'gmc.noid AS genmcompany_noid',
                        'gmc.kode AS genmcompany_kode',
                        'gmc.nama AS genmcompany_nama',
                    ])
                    ->orderBy('gmc.nama', 'asc')
                    ->orderBy('gmd.nama', 'asc')
                    ->get();
        return $result;
    }

    public static function getInvMInventory() {
        $result = DB::connection('mysql2')
                    ->table('invminventory AS imi')
                    ->leftJoin('invmsatuan AS ims', 'imi.idsatuan', '=', 'ims.noid')
                    ->select([
                        'imi.noid AS noid',
                        'imi.kode AS kode',
                        'imi.nama AS nama',
                        'imi.idsatuan AS idsatuan',
                        'ims.nama AS namasatuan',
                        'imi.konversi AS konversi',
                        'imi.idtax AS idtax',
                        'imi.taxprocent AS taxprocent',
                        'imi.purchaseprice AS purchaseprice',
                        'imi.purchaseprice AS purchaseprice2'
                    ])
                    ->orderBy('imi.noid', 'asc')
                    ->get();
        return $result;
    }

    public static function getSales($start=null, $length=null, $search, $orderby, $filter=null, $datefilter, $datefilter2) {
        if ($search) {
            $result = DB::connection('mysql2')
                        ->table((new static)->main['table'].' AS si')
                        ->leftJoin('mtypetranctypestatus AS mttts', 'si.idstatustranc', '=', 'mttts.noid')
                        ->leftJoin('mtypetranc AS mtt', 'si.idtypetranc', '=', 'mtt.noid')
                        ->leftJoin('genmcompany AS gmc', 'si.idcompany', '=', 'gmc.noid')
                        ->leftJoin('genmdepartment AS gmd', 'si.iddepartment', '=', 'gmd.noid')
                        ->leftJoin('genmgudang AS gmg', 'si.idgudang', '=', 'gmg.noid')
                        ->leftJoin('mcardcustomer AS mc1', 'mc1.noid', '=', 'si.idcardcustomer')
                        ->leftJoin('mcard AS mc2', 'mc2.noid', '=', 'si.idcardsales')
                        ->select(
                            'si.noid',
                            // 'po.noid AS purchaseorder_noid',
                            'mttts.noid AS mtypetranctypestatus_noid',
                            'mttts.idstatusbase AS mtypetranctypestatus_idstatusbase',
                            'mttts.nama AS mtypetranctypestatus_nama',
                            'mttts.classcolor AS mtypetranctypestatus_classcolor',
                            'mtt.noid AS mtypetranc_noid',
                            'mtt.nama AS mtypetranc_nama',
                            'si.kode',
                            'si.kodereff',
                            'si.tanggal',
                            'si.tanggaldue',
                            // DB::raw('DATE_FORMAT(si.tanggal, "%d-%b-%Y") AS tanggal_human'),
                            // 'DATE_FORMAT(si.tanggaldue, "%d %b %Y") AS tanggaldue_human',
                            'gmc.nama AS idcompany',
                            'gmd.nama AS iddepartment',
                            'gmg.nama AS idgudang',
                            'si.totalsubtotal',
                            'si.generaltotal',
                            'si.idcardsales',
                            'si.idcarddrafter',
                            'mc1.nama AS idcardcustomer_nama',
                            'mc2.nama AS idcardsales_nama',
                            'si.generaltotal',
                            'si.projectvalue',
                            'si.projectname',
                            'si.keterangan'
                        )
                        ->where('si.idtypetranc', '=', (new static)->main['idtypetranc'])
                        ->where(function($query) use($search) {
                            $query->orWhere('si.noid', 'like', "%$search%")
                                ->orWhere('mttts.nama', 'like', "%$search%")
                                ->orWhere('mtt.nama', 'like', "%$search%")
                                ->orWhere('si.kode', 'like', "%$search%")
                                ->orWhere('si.kodereff', 'like', "%$search%")
                                ->orWhere('gmc.nama', 'like', "%$search%")
                                ->orWhere('gmd.nama', 'like', "%$search%")
                                ->orWhere('si.generaltotal', 'like', "%$search%")
                                ->orWhere('si.keterangan', 'like', "%$search%")
                                ->orWhere('si.tanggal', 'like', "%$search%")
                                ->orWhere('si.tanggaldue', 'like', "%$search%")
                                ->orWhere('gmg.nama', 'like', "%$search%");
                                // ->orWhere('si.tanggal', 'like', '%'.DB::raw().'%');
                                // ->orWhere(DB::raw('DATE_FORMAT(tanggal, "%d-%b-%Y") like %"'.$search.'"%'));
                                // ->orWhere('si.tanggaldue_human', 'like', "%$search%");
                        });

            $resultdatefilterfrom = DB::connection('mysql2')
                                        ->table((new static)->main['table'])
                                        ->select('tanggal');
            $resultdatefilterto = DB::connection('mysql2')
                                        ->table((new static)->main['table'])
                                        ->select('tanggal');
                                        
            if ($filter) {
                foreach ($filter as $k => $v) {
                    if ($v) {
                        $result->where('si.'.$k, '=', $v);
                    }
                }
            }

            if ($datefilter['code'] == 'a') {
                $resultf = $resultdatefilterfrom->orderBy('tanggal', 'asc')->first() ? date('M d, Y', strtotime($resultdatefilterfrom->orderBy('tanggal', 'asc')->first()->tanggal)) : date('M d, Y');
                $resultt = $resultdatefilterto->orderBy('tanggal', 'desc')->first() ? date('M d, Y', strtotime($resultdatefilterto->orderBy('tanggal', 'desc')->first()->tanggal)) : date('M d, Y');
            } else if ($datefilter['code'] == 't') {
                $result->whereDate('si.tanggal', date('Y-m-d'));
                $resultf = date('M d, Y');
                $resultt = date('M d, Y');
            } else if ($datefilter['code'] == 'y') {
                $result->whereDate('si.tanggal', date('Y-m-d', strtotime('-1 day')));
                $resultf = date('M d, Y', strtotime('-1 day'));
                $resultt = date('M d, Y', strtotime('-1 day'));
            } else if ($datefilter['code'] == 'l7d') {
                // $result->whereRaw('si.tanggal >= DATE(NOW()) - INTERVAL 7 DAY');
                $result->whereDate('si.tanggal', '<=', date('Y-m-d'));
                $result->whereDate('si.tanggal', '>=', date('Y-m-d', strtotime('-7 day')));
                $resultf = date('M d, Y', strtotime('-7 day'));
                $resultt = date('M d, Y');
            } else if ($datefilter['code'] == 'l30d') {
                $result->whereDate('si.tanggal', '<=', date('Y-m-d'));
                $result->whereDate('si.tanggal', '>=', date('Y-m-d', strtotime('-1 month')));
                $resultf = date('M d, Y', strtotime('-1 month'));
                $resultt = date('M d, Y');
            } else if ($datefilter['code'] == 'tm') {
                $result->whereYear('si.tanggal', date('Y', strtotime('this year')));
                $result->whereMonth('si.tanggal', date('m', strtotime('this month')));
                $resultf = date('M d, Y', strtotime('first day of this month'));
                $resultt = date('M d, Y', strtotime('last day of this month'));
            } else if ($datefilter['code'] == 'lm') {
                $result->whereYear('si.tanggal', date('Y', strtotime('next year')));
                $result->whereMonth('si.tanggal', date('m', strtotime('next month')));
                $resultf = date('M d, Y', strtotime('first day of previous month'));
                $resultt = date('M d, Y', strtotime('last day of previous month'));
            } else if ($datefilter['code'] == 'cr') {
                $result->whereDate('si.tanggal', '>=', date('Y-m-d', strtotime($datefilter['custom']['from'])));
                $result->whereDate('si.tanggal', '<=', date('Y-m-d', strtotime($datefilter['custom']['to'])));
                $resultf = date('M d, Y', strtotime($datefilter['custom']['from']));
                $resultt = date('M d, Y', strtotime($datefilter['custom']['to']));
            }

            // $result->whereDate('si.tanggal', '>=', $datefilter2['from']);
            // $result->whereDate('si.tanggal', '<=', $datefilter2['to']);

            $result->orderBy(
                $orderby['column'] == 0 ? 'si.docreate' : $orderby['column'], 
                $orderby['column'] == 0 ? 'desc' : $orderby['sort']
            );

            if ($start != null && $length != null) {
                $result->offset($start)
                        ->limit($length);
            }
        } else {
            $result = DB::connection('mysql2')
                        ->table((new static)->main['table'].' AS si')
                        ->leftJoin('mtypetranctypestatus AS mttts', 'si.idstatustranc', '=', 'mttts.noid')
                        ->leftJoin('mtypetranc AS mtt', 'si.idtypetranc', '=', 'mtt.noid')
                        ->leftJoin('genmcompany AS gmc', 'si.idcompany', '=', 'gmc.noid')
                        ->leftJoin('genmdepartment AS gmd', 'si.iddepartment', '=', 'gmd.noid')
                        ->leftJoin('genmgudang AS gmg', 'si.idgudang', '=', 'gmg.noid')
                        ->leftJoin('mcardcustomer AS mc1', 'mc1.noid', '=', 'si.idcardcustomer')
                        ->leftJoin('mcard AS mc2', 'mc2.noid', '=', 'si.idcardsales')
                        ->select([
                            'si.noid',
                            // 'po.noid AS purchaseorder_noid',
                            'mttts.noid AS mtypetranctypestatus_noid',
                            'mttts.idstatusbase AS mtypetranctypestatus_idstatusbase',
                            'mttts.nama AS mtypetranctypestatus_nama',
                            'mttts.classcolor AS mtypetranctypestatus_classcolor',
                            'mtt.noid AS mtypetranc_noid',
                            'mtt.nama AS mtypetranc_nama',
                            'si.kode',
                            'si.kodereff',
                            'si.tanggal',
                            'si.tanggaldue',
                            'gmc.nama AS idcompany',
                            'gmd.nama AS iddepartment',
                            'gmg.nama AS idgudang',
                            'si.totalsubtotal',
                            'si.generaltotal',
                            'si.idcardsales',
                            'si.idcarddrafter',
                            'mc1.nama AS idcardcustomer_nama',
                            'mc2.nama AS idcardsales_nama',
                            'si.generaltotal',
                            'si.projectvalue',
                            'si.projectname',
                            'si.keterangan',
                            'si.tanggal'
                        ])
                        ->where('si.idtypetranc', '=', (new static)->main['idtypetranc']);
                        
            $resultdatefilterfrom = DB::connection('mysql2')
                                        ->table((new static)->main['table'])
                                        ->select('tanggal');
            $resultdatefilterto = DB::connection('mysql2')
                                        ->table((new static)->main['table'])
                                        ->select('tanggal');

                                        if ($filter) {
                foreach ($filter as $k => $v) {
                    if ($v) {
                        $result->where('si.'.$k, '=', $v);
                    }
                }
            }

            if ($datefilter['code'] == 'a') {
                $resultf = $resultdatefilterfrom->orderBy('tanggal', 'asc')->first() ? date('M d, Y', strtotime($resultdatefilterfrom->orderBy('tanggal', 'asc')->first()->tanggal)) : date('M d, Y');
                $resultt = $resultdatefilterto->orderBy('tanggal', 'desc')->first() ? date('M d, Y', strtotime($resultdatefilterto->orderBy('tanggal', 'desc')->first()->tanggal)) : date('M d, Y');
            } else if ($datefilter['code'] == 't') {
                $result->whereDate('si.tanggal', date('Y-m-d'));
                $resultf = date('M d, Y');
                $resultt = date('M d, Y');
            } else if ($datefilter['code'] == 'y') {
                $result->whereDate('si.tanggal', date('Y-m-d', strtotime('-1 day')));
                $resultf = date('M d, Y', strtotime('-1 day'));
                $resultt = date('M d, Y', strtotime('-1 day'));
            } else if ($datefilter['code'] == 'l7d') {
                // $result->whereRaw('si.tanggal >= DATE(NOW()) - INTERVAL 7 DAY');
                $result->whereDate('si.tanggal', '<=', date('Y-m-d'));
                $result->whereDate('si.tanggal', '>=', date('Y-m-d', strtotime('-7 day')));
                $resultf = date('M d, Y', strtotime('-7 day'));
                $resultt = date('M d, Y');
            } else if ($datefilter['code'] == 'l30d') {
                $result->whereDate('si.tanggal', '<=', date('Y-m-d'));
                $result->whereDate('si.tanggal', '>=', date('Y-m-d', strtotime('-1 month')));
                $resultf = date('M d, Y', strtotime('-1 month'));
                $resultt = date('M d, Y');
            } else if ($datefilter['code'] == 'tm') {
                $result->whereYear('si.tanggal', date('Y', strtotime('this year')));
                $result->whereMonth('si.tanggal', date('m', strtotime('this month')));
                $resultf = date('M d, Y', strtotime('first day of this month'));
                $resultt = date('M d, Y', strtotime('last day of this month'));
            } else if ($datefilter['code'] == 'lm') {
                $result->whereYear('si.tanggal', date('Y', strtotime('next year')));
                $result->whereMonth('si.tanggal', date('m', strtotime('next month')));
                $resultf = date('M d, Y', strtotime('first day of previous month'));
                $resultt = date('M d, Y', strtotime('last day of previous month'));
            } else if ($datefilter['code'] == 'cr') {
                $result->whereDate('si.tanggal', '>=', date('Y-m-d', strtotime($datefilter['custom']['from'])));
                $result->whereDate('si.tanggal', '<=', date('Y-m-d', strtotime($datefilter['custom']['to'])));
                $resultf = date('M d, Y', strtotime($datefilter['custom']['from']));
                $resultt = date('M d, Y', strtotime($datefilter['custom']['to']));
            }

            // $result->whereDate('si.tanggal', '>=', $datefilter2['from']);
            // $result->whereDate('si.tanggal', '<=', $datefilter2['to']);

            $result->orderBy(
                $orderby['column'] == 0 ? 'si.docreate' : $orderby['column'], 
                $orderby['column'] == 0 ? 'desc' : $orderby['sort']
            );
            
            if ($start != null && $length != null) {
                $result->offset($start)
                        ->limit($length);
            }
        }
        
        return [
            'datefilter' => [
                'type' => $datefilter['code'],
                'from' => $resultf,
                'to' => $resultt
            ],
            'data' => $result->get(),
        ];
    }

    public static function getLog($noid, $kode, $start, $length, $search, $orderby) {
        if ($search) {
            $result = DB::connection('mysql2')
                        ->table('applog AS al')
                        ->leftJoin('appmtypeaction AS amta', 'al.idtypeaction', '=', 'amta.noid')
                        ->leftJoin('mcarduser AS mcu', 'al.idcreate', '=', 'mcu.noid')
                        ->leftJoin((new static)->main['table'].' AS prq', 'al.idreff', '=', 'prq.noid')
                        ->select([
                            'al.noid AS noid',
                            'amta.nama AS idtypeaction',
                            'al.logsubject AS logsubject',
                            'al.keterangan AS keterangan',
                            'al.idreff AS idreff',
                            'prq.kode AS kode',
                            'mcu.myusername AS idcreate',
                            'al.docreate AS docreate'
                        ])
                        ->where('al.idreff', $noid)
                        ->where('al.idtypetranc', (new static)->main['idtypetranc'])
                        ->where('al.logsubject', 'like', "%$kode%")
                        ->where(function($query) use ($search) {
                            $query->where('al.noid', 'like', "%$search%");
                            $query->orWhere('amta.nama', 'like', "%$search%");
                            $query->orWhere('al.logsubject', 'like', "%$search%");
                            $query->orWhere('al.keterangan', 'like', "%$search%");
                            $query->orWhere('mcu.myusername', 'like', "%$search%");
                            $query->orWhere('al.docreate', 'like', "%$search%");
                        })
                        ->orderBy($orderby['column'], $orderby['sort'])
                        ->offset($start)
                        ->limit($length)
                        ->get();
        } else {
            $result = DB::connection('mysql2')
                        ->table('applog AS al')
                        ->leftJoin('mtypetranc AS mtt', 'al.idtypetranc', '=', 'mtt.noid')
                        ->leftJoin('appmtypeaction AS amta', 'al.idtypeaction', '=', 'amta.noid')
                        ->leftJoin('mcarduser AS mcu', 'al.idcreate', '=', 'mcu.noid')
                        ->leftJoin((new static)->main['table'].' AS prq', 'al.idreff', '=', 'prq.noid')
                        ->select([
                            'al.noid AS noid',
                            'amta.nama AS idtypeaction',
                            'al.logsubject AS logsubject',
                            'al.keterangan AS keterangan',
                            'al.idreff AS idreff',
                            'prq.kode AS kode',
                            'mcu.myusername AS idcreate',
                            'al.docreate AS docreate'
                        ])
                        ->where('al.idreff', $noid)
                        ->where('al.idtypetranc', (new static)->main['idtypetranc'])
                        ->where('al.logsubject', 'like', "%$kode%")
                        ->orderBy($orderby['column'], $orderby['sort'])
                        ->offset($start)
                        ->limit($length)
                        ->get();
        }
        return $result;
    }

    public static function getDefaultCostControl() {
        $result = DB::connection('mysql2')
                    ->table('salesorder')
                    ->where('noid', 0)
                    ->first();
        return $result;
    }

    public static function getCostControl($start, $length, $search, $orderby) {
        if ($search) {
            $result = DB::connection('mysql2')
                        ->table('salesorder AS so')
                        ->leftJoin('mtypecostcontrol AS mtcc', 'so.idtypecostcontrol', '=', 'mtcc.noid')
                        ->leftJoin('mtypetranctypestatus AS mttts', 'so.idstatustranc', '=', 'mttts.noid')
                        ->leftJoin('mtypetranc AS mtt', 'so.idtypetranc', '=', 'mtt.noid')
                        ->leftJoin('genmcompany AS gmc', 'so.idcompany', '=', 'gmc.noid')
                        ->leftJoin('genmdepartment AS gmd', 'so.iddepartment', '=', 'gmd.noid')
                        ->leftJoin('mcarduser AS mcu', 'so.idcreate', '=', 'mcu.noid')
                        ->select([
                            'so.noid',
                            'mtt.nama AS mtt_nama',
                            'mttts.nama AS mttts_nama',
                            'so.kode',
                            'so.kodereff',
                            'so.tanggal',
                            'so.idtypecostcontrol',
                            'mtcc.kode AS mtcc_kode',
                            'mtcc.nama AS mtcc_nama',
                            'so.generaltotal',
                            'so.keterangan',
                            'mcu.myusername AS mcu_myusername',
                            'gmc.nama AS gmc_nama',
                            'gmd.nama AS gmd_nama',
                        ])
                        ->where('so.noid', 'like', "%$search%")
                        ->orWhere('mttts.nama', 'like', "%$search%")
                        ->orWhere('mtt.nama', 'like', "%$search%")
                        ->orWhere('so.kode', 'like', "%$search%")
                        ->orWhere('so.kodereff', 'like', "%$search%")
                        ->orWhere('gmc.nama', 'like', "%$search%")
                        ->orWhere('gmd.nama', 'like', "%$search%")
                        ->orWhere('so.generaltotal', 'like', "%$search%")
                        ->orWhere('so.keterangan', 'like', "%$search%")
                        ->orWhere('so.tanggal', 'like', "%$search%")
                        ->orderBy($orderby['column'], $orderby['sort'])
                        ->offset($start)
                        ->limit($length)
                        ->get();
        } else {
            $result = DB::connection('mysql2')
                        ->table('salesorder AS so')
                        ->leftJoin('mtypecostcontrol AS mtcc', 'so.idtypecostcontrol', '=', 'mtcc.noid')
                        ->leftJoin('mtypetranctypestatus AS mttts', 'so.idstatustranc', '=', 'mttts.noid')
                        ->leftJoin('mtypetranc AS mtt', 'so.idtypetranc', '=', 'mtt.noid')
                        ->leftJoin('genmcompany AS gmc', 'so.idcompany', '=', 'gmc.noid')
                        ->leftJoin('genmdepartment AS gmd', 'so.iddepartment', '=', 'gmd.noid')
                        ->leftJoin('mcarduser AS mcu', 'so.idcreate', '=', 'mcu.noid')
                        ->select([
                            'so.noid',
                            'mtt.nama AS mtt_nama',
                            'mttts.nama AS mttts_nama',
                            'so.kode',
                            'so.kodereff',
                            'so.tanggal',
                            'so.idtypecostcontrol',
                            'so.kode AS mtcc_kode',
                            'mtcc.nama AS mtcc_nama',
                            'so.generaltotal',
                            'so.keterangan',
                            'mcu.myusername AS mcu_myusername',
                            'gmc.nama AS gmc_nama',
                            'gmd.nama AS gmd_nama',
                        ])
                        ->orderBy($orderby['column'], $orderby['sort'])
                        ->offset($start)
                        ->limit($length)
                        ->get();
        }
        return $result;
    }

    public static function getMCardSupplier() {
        $result = DB::connection('mysql2')
                    ->table('mcardsupplier')
                    ->leftJoin('mlokasiprop','mcardsupplier.idlokasiprop','=','mlokasiprop.noid')
                    ->leftJoin('mlokasikab','mcardsupplier.idlokasikota','=','mlokasikab.noid')
                    ->leftJoin('mlokasikec','mcardsupplier.idlokasikec','=','mlokasikec.noid')
                    ->leftJoin('mlokasikel','mcardsupplier.idlokasikel','=','mlokasikel.noid')
                    ->select([
                        'mcardsupplier.noid AS mcardsupplier_noid',
                        'mcardsupplier.nama AS mcardsupplier_nama',
                        'mlokasiprop.noid AS mlokasiprop_noid',
                        'mlokasiprop.kode AS mlokasiprop_kode',
                        'mlokasiprop.nama AS mlokasiprop_nama',
                        'mlokasikab.noid AS mlokasikab_noid',
                        'mlokasikab.kode AS mlokasikab_kode',
                        'mlokasikab.nama AS mlokasikab_nama',
                        'mlokasikec.noid AS mlokasikec_noid',
                        'mlokasikec.kode AS mlokasikec_kode',
                        'mlokasikec.nama AS mlokasikec_nama',
                        'mlokasikel.noid AS mlokasikel_noid',
                        'mlokasikel.kode AS mlokasikel_kode',
                        'mlokasikel.nama AS mlokasikel_nama',
                    ])
                    ->orderBy('mcardsupplier_noid', 'asc')
                    ->get();
        return $result;
    }

    public static function getMTypeTrancTypeStatus($idtypetranc) {
        $result = DB::connection('mysql2')
                    ->table('mtypetranctypestatus AS mttts')
                    ->leftJoin('appmtypeaction AS amta', 'mttts.noid', '=', 'amta.idstatustrancbase')
                    ->select([
                        'mttts.noid',
                        'mttts.idstatusbase',
                        'mttts.nourut',
                        'mttts.kode',
                        'mttts.nama',
                        'mttts.classicon',
                        'mttts.classcolor',
                        'mttts.isinternal',
                        'mttts.isexternal',
                        'mttts.actview',
                        'mttts.actedit',
                        'mttts.actdelete',
                        'mttts.actreportdetail',
                        'mttts.actreportmaster',
                        'mttts.actsetprevstatus',
                        'mttts.actsetnextstatus',
                        'mttts.actsetstatus',
                        'amta.noid AS appmtypeaction_noid',
                        'amta.nama AS appmtypeaction_nama'
                    ])
                    ->where('mttts.idtypetranc', $idtypetranc)
                    ->where('mttts.isactive', '>', 0)
                    ->orderBy('mttts.nourut', 'asc')
                    ->get();
        return $result;
    }

    public static function saveData($params) {
        // Unset field tidak penting
        if (count($params['data'][(new static)->main['tabledetail']]) > 0) {
            foreach ($params['data'][(new static)->main['tabledetail']] as $k => $v) {
                unset($params['data'][(new static)->main['tabledetail']][$k]['kodeprev']);
                unset($params['data'][(new static)->main['tabledetail']][$k]['unitqtysisaprev']);
            }
        }

        // Insert Master
        $insertmaster = DB::connection('mysql2')
            ->table((new static)->main['table'])
            ->insert($params['data'][(new static)->main['table']]);
        if (!$insertmaster) return ['success' => false, 'message' => 'Error Insert Master'];
        
        // Insert Detail
        $insertdetail = DB::connection('mysql2')
            ->table((new static)->main['tabledetail'])
            ->insert($params['data'][(new static)->main['tabledetail']]);
        if (!$insertdetail) return ['success' => false, 'message' => 'Error Insert Detail'];

        //Insert CDF
        if (@$params['data']['cdf']) {
            $insertcdf = DB::connection('mysql2')
                ->table((new static)->main['tablecdf'])
                ->insert($params['data']['cdf']);
            if (!$insertcdf) return ['success' => false, 'message' => 'Error Insert File'];
        }

        //Insert Log
        if (@$params['datalog']) {
            $insertlog = DB::connection('mysql2')
                ->table('applog')
                ->insert($params['datalog']);
            if (!$insertmaster) return ['success' => false, 'message' => 'Error Insert Log'];
        }

        return ['success' => true, 'message' => 'Success Insert Data'];
    }
    
    public static function updateData($params) {
        // Update Master
        $updatemaster = DB::connection('mysql2')
            ->table((new static)->main['table'])
            ->where('noid', $params['noid'])
            ->update($params['data'][(new static)->main['table']]);
        if (!$updatemaster) return ['success' => false, 'message' => 'Error Update Master'];
        
        // // Delete Detail
        // $wheredetail = DB::connection('mysql2')
        //     ->table((new static)->main['tabledetail'])
        //     ->where('idmaster', $params['noid']);
        // if ($wheredetail->get()) {
        //     if (!$wheredetail->delete()) return ['success' => false, 'message' => 'Error Delete Detail'];
        // }

        // // Insert Detail
        // $datadetail = [];
        // foreach ($params['data'][(new static)->main['tabledetail']] as $k => $v) {
        //     $datadetail[$k] = $v;
        //     unset($datadetail[$k]['kodeprev']);
        //     unset($datadetail[$k]['idtypetrancprev']);
        //     unset($datadetail[$k]['idtrancprev']);
        //     unset($datadetail[$k]['idtrancprevd']);
        // }
        // $insertdetail = DB::connection('mysql2')
        //     ->table((new static)->main['tabledetail'])
        //     ->insert($datadetail);
        // if (!$insertdetail) return ['success' => false, 'message' => 'Error Insert Detail'];

        // Delete CDF
        $wherecdf = DB::connection('mysql2')
            ->table((new static)->main['tablecdf'])
            ->where('idreff', $params['data'][(new static)->main['table']]['noid']);
        if ($wherecdf->first())
            if (!$wherecdf->delete()) return ['success' => false, 'message' => 'Error Delete File'];

        // Insert CDF
        $insertcdf = DB::connection('mysql2')
            ->table((new static)->main['tablecdf'])
            ->insert($params['data']['cdf']);
        if (!$insertcdf) return ['success' => false, 'message' => 'Error Insert File'];

        // Insert Log
        $insertlog = DB::connection('mysql2')
            ->table('applog')
            ->insert($params['datalog']);
        if (!$insertlog) return ['success' => false, 'message' => 'Error Insert Log'];

        return ['success' => true, 'message' => 'Success Update Data'];
    }

    public static function getNextNoid($table) {
        $result = DB::connection('mysql2')
            ->table($table)
            ->orderBy('noid', 'desc')
            ->first();
        return $result ? $result->noid+1 : 1;
    }
    
    public static function getKode($table,$noid) {
        $result = DB::connection('mysql2')
            ->table($table)
            ->select('kode')
            ->where('noid', $noid)
            ->first();
        return $result ? $result->kode : '';
    }

    public static function getLastNoid($table) {
        $result = DB::connection('mysql2')
            ->table($table)
            ->orderBy('noid', 'desc')
            ->first();
        return $result ? $result->noid : 1;
    }

    public static function findLog($noid, $kode) {
        $result = DB::connection('mysql2')
            ->table('applog')
            ->select('noid')
            ->where('idreff', $noid)
            ->where('logsubject', 'like', "%$kode%")
            ->orderBy('noid', 'asc')
            ->get();
        return $result;
    }

    public static function findData($params) {
        $queryresult = DB::connection('mysql2')
                    ->table((new static)->main['table'].' AS pr')
                    ->leftJoin('mtypetranc AS mtt', 'pr.idtypetranc', '=', 'mtt.noid')
                    ->leftJoin('mtypetranctypestatus AS mttts', 'pr.idstatustranc', '=', 'mttts.noid')
                    ->leftJoin('mcarduser AS mcu', 'pr.idstatuscard', '=', 'mcu.noid')
                    ->leftJoin('accmcurrency AS amc', 'pr.idcurrency', '=', 'amc.noid')
                    ->leftJoin('mtypecostcontrol AS mtcc', 'mtcc.noid', '=', 'pr.idtypecostcontrol')
                    ->leftJoin('mtypeproject AS mtp', 'mtp.noid', '=', 'pr.projecttype')
                    ->leftJoin('mcardcustomer AS mccustomer', 'mccustomer.noid', '=', 'pr.idcardcustomer')
                    ->leftJoin('mcard AS mcmarketing', 'mcmarketing.noid', '=', 'pr.idcardsales')
                    ->leftJoin('mcard AS mcprojectkoor', 'mcprojectkoor.noid', '=', 'pr.idcardkoor')
                    ->leftJoin('mcard AS mcprojectmanager', 'mcprojectmanager.noid', '=', 'pr.idcardpj')
                    ->leftJoin('mcard AS mcadminsiteproject', 'mcadminsiteproject.noid', '=', 'pr.idcardsitekoor')
                    ->leftJoin('mcard AS mcdrafter', 'mcdrafter.noid', '=', 'pr.idcarddrafter')
                    ->select([
                        'pr.*',
                        'mtt.kode AS kodetypetranc',
                        'mttts.nama AS mtypetranctypestatus_nama',
                        'mttts.classcolor AS mtypetranctypestatus_classcolor',
                        'mcu.myusername AS mcarduser_myusername',
                        'amc.nama AS namacurrency',
                        'mtp.nama AS projecttype_nama',
                        'mtcc.nama AS idtypecostcontrol_nama',
                        'mccustomer.nama AS idcardcustomer_nama',
                        'mcmarketing.nama AS idcardsales_nama',
                        'mcprojectkoor.nama AS idcardkoor_nama',
                        'mcprojectmanager.nama AS idcardpj_nama',
                        'mcadminsiteproject.nama AS idcardsitekoor_nama',
                        'mcdrafter.nama AS idcarddrafter_nama',
                    ]);

        if (array_key_exists('noid', $params)) {
            $queryresult->where('pr.noid', $params['noid']);
        } else if (array_key_exists('kode', $params)) {
            $queryresult->where('pr.kode', $params['kode']);
        } else if (array_key_exists('kodereff', $params)) {
            $queryresult->where('pr.kodereff', $params['kodereff']);
        }
        
        $result = $queryresult->first();

        $resultdetail = DB::connection('mysql2')
                        ->table((new static)->main['tabledetail'].' AS prd')
                        ->leftJoin('invminventory AS imi', 'prd.idinventor', '=', 'imi.noid')
                        ->leftJoin('invmsatuan AS ims', 'prd.idsatuan', '=', 'ims.noid')
                        ->select([
                            'prd.noid AS noid',
                            'prd.idinventor AS idinventor',
                            'imi.kode AS kodeinventor',
                            'imi.nama AS namainventor',
                            'prd.namainventor AS namainventor2',
                            'prd.unitqty AS unitqty',
                            'prd.idcompany AS idcompany',
                            'prd.iddepartment AS iddepartment',
                            'prd.idgudang AS idgudang',
                            'prd.keterangan AS keterangan',
                            'prd.idsatuan AS idsatuan',
                            'ims.nama AS namasatuan',
                            'prd.konvsatuan AS konvsatuan',
                            'prd.unitqtysisa AS unitqtysisa',
                            'prd.unitprice AS unitprice',
                            'prd.subtotal AS subtotal',
                            'prd.discountvar AS discountvar',
                            'prd.discount AS discount',
                            'prd.nilaidisc AS nilaidisc',
                            'prd.idtypetax AS idtypetax',
                            'prd.idtax AS idtax',
                            'prd.prosentax AS prosentax',
                            'prd.nilaitax AS nilaitax',
                            'prd.hargatotal AS hargatotal',
                        ])
                        ->where('prd.idmaster', $result->noid)
                        ->orderBy('imi.kode', 'asc')
                        ->get();

        $resultcdf = DB::connection('mysql2')
                ->table((new static)->main['tablecdf'].' AS cdf')
                ->leftJoin('dmsmfile AS dmf', 'dmf.noid', '=', 'cdf.iddmsfile')
                ->select([
                    'cdf.noid AS cdf_noid',
                    'cdf.iddmsfile AS cdf_iddmsfile',
                    'dmf.nama AS dmf_nama',
                    'dmf.filename AS dmf_filename',
                    'dmf.filepreview AS dmf_filepreview',
                    'cdf.nama AS cdf_nama',
                    'cdf.keterangan AS cdf_keterangan',
                    'cdf.ishardcopy AS cdf_ishardcopy',
                    'cdf.ispublic AS cdf_ispublic',
                    'cdf.doexpired AS cdf_doexpired',
                    'cdf.doreviewlast AS cdf_doreviewlast',
                    'cdf.doreviewnext AS cdf_doreviewnext',
                ])
                ->where('cdf.idreff', $result->noid)
                ->where('cdf.idtypetranc', (new static)->main['idtypetranc'])
                ->orderBy('cdf.noid', 'asc')
                ->get();

        $cdf = [];
        foreach ($resultcdf as $k => $v) {
            $dmf_filename = explode('.', $v->dmf_filename);
            if (end($dmf_filename) == 'jpg' ||
                end($dmf_filename) == 'jpeg' ||
                end($dmf_filename) == 'png' ||
                end($dmf_filename) == 'gif') {
                $dmf_filepreview = $v->dmf_filepreview;
                $dmf_urlfilepreview = $v->dmf_filepreview.'/'.$v->dmf_filename;
            } else {
                $dmf_filepreview = 'filemanager/default';
                $dmf_urlfilepreview = 'filemanager/default/'.end($dmf_filename).'.png';
            }

            $cdf[] = (object)[
                'cdf_noid' => $v->cdf_noid,
                'cdf_nama' => $v->cdf_nama,
                'cdf_iddmsfile' => $v->cdf_iddmsfile,
                'dmf_nama' => $v->dmf_nama,
                'dmf_filename' => $v->dmf_filename,
                'dmf_filepreview' => $dmf_filepreview,
                'dmf_urlfilepreview' => $dmf_urlfilepreview,
                'cdf_nama' => $v->cdf_nama,
                'cdf_keterangan' => $v->cdf_keterangan,
                'cdf_ishardcopy' => $v->cdf_ishardcopy,
                'cdf_ispublic' => $v->cdf_ispublic,
                'cdf_doexpired' => date('d-M-Y', strtotime($v->cdf_doexpired)),
                'cdf_doreviewlast' => date('d-M-Y', strtotime($v->cdf_doreviewlast)),
                'cdf_doreviewnext' => date('d-M-Y', strtotime($v->cdf_doreviewnext)),
            ];
        }

        $data = [
            'master' => $result,
            'detail' => $resultdetail ? $resultdetail : [],
            'cdf' => $cdf
        ];

        return $data;
    }

    public static function findLastSupplier() {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->select([
                        'noid',
                        'idcardsupplier',
                        'idakunsupplier',
                        'suppliernama',
                        'supplieraddress1',
                        'supplieraddress2',
                        'supplieraddressrt',
                        'supplieraddressrw',
                        'supplierlokasikel',
                        'supplierlokasikec',
                        'supplierlokasikab',
                        'supplierlokasiprop',
                        'supplierlokasineg',
                    ])
                    ->orderBy('noid', 'desc')
                    ->first();

        return $result;
    }

    public static function findRAP($params) {
        $salesinquiry = DB::connection('mysql2')
            ->table('salesorder AS so')
            ->select(['rap.kode AS kodereff'])
            ->leftJoin('salesinquiry AS si', 'si.kode', '=', 'so.kodereff')
            ->leftJoin('prjmrap AS rap', 'rap.kodereff', '=', 'si.kode')
            ->where('so.noid', $params['noidsalesorder'])
            ->first();
        
        if (!$salesinquiry) {
            return ['success' => false, 'message' => 'Sales Inquiry not found'];
        } else {
            $koderap = $salesinquiry->kodereff;
        }

        $master = DB::connection('mysql2')
            ->table('prjmrap')
            ->leftJoin('salesorder', 'prjmrap.idcostcontrol', '=', 'salesorder.noid')
            ->leftJoin('mtypetranc', 'prjmrap.idtypetranc', '=', 'mtypetranc.noid')
            ->select([
                'prjmrap.*',
                'mtypetranc.kode AS mtypetranc_kode',
                'salesorder.kode AS salesorder_kode'
            ])
            ->where('prjmrap.kode', $koderap)
            ->first();
        if (!$master) return ['success' => false, 'message' => 'RAP not found'];
        
        $milestone = DB::connection('mysql2')
            ->table('prjmrapmilestone')
            ->where('idmaster', $master->noid)
            ->get();
        if (!$milestone) return ['success' => false, 'message' => 'Error Get Milestone'];

        $header = DB::connection('mysql2')
            ->table('prjmrapheader')
            ->where('idmaster', $master->noid)
            ->get();
        if (!$header) return ['success' => false, 'message' => 'Error Get Header'];

        $detail = DB::connection('mysql2')
            ->table('prjmrapdetail')
            ->where('idmaster', $master->noid)
            ->get();
        if (!$detail) return ['success' => false, 'message' => 'Error Get Detail'];

        $cdf = DB::connection('mysql2')
            ->table('prjmrapdocument')
            ->where('idrap', $master->noid)
            ->get();
        if (!$detail) return ['success' => false, 'message' => 'Error Get File'];

        return [
            'success' => true, 
            'message' => 'Success Get Data', 
            'data' => [
                'master' => (array)$master,
                'milestone' => $milestone,
                'header' => $header,
                'detail' => $detail,
                'cdf' => $cdf,
            ]
        ];
    }

    public static function sendToNext($params) {
        $insertmaster = DB::connection('mysql2')
            ->table((new static)->main['table2'])
            ->insert($params['master']);
        if (!$insertmaster) return ['success' => false, 'message' => 'Error Insert Master'];
        
        $insertmilestone = DB::connection('mysql2')
            ->table((new static)->main['tablemilestone2'])
            ->insert($params['milestone']);
        if (!$insertmilestone) return ['success' => false, 'message' => 'Error Insert Milestone'];

        $insertheader = DB::connection('mysql2')
            ->table((new static)->main['tableheader2'])
            ->insert($params['header']);
        if (!$insertheader) return ['success' => false, 'message' => 'Error Insert Header'];

        $insertdetail = DB::connection('mysql2')
            ->table((new static)->main['tabledetail2'])
            ->insert($params['detail']);
        if (!$insertdetail) return ['success' => false, 'message' => 'Error Insert Detail'];

        $insertcdf = DB::connection('mysql2')
            ->table((new static)->main['tablecdf2'])
            ->insert($params['cdf']);
        if (!$insertcdf) return ['success' => false, 'message' => 'Error Insert File'];
        
        $insertlog = DB::connection('mysql2')
            ->table('applog')
            ->insert($params['log']);
        if (!$insertlog) return ['success' => false, 'message' => 'Error Insert Log'];

        $updateprev = DB::connection('mysql2')
            ->table((new static)->main['table'])
            ->where('kodereff', $params['kodereff'])
            ->update(['idstatustranc' => 4024]);
        if (!$updateprev) return ['success' => false, 'message' => 'Error Update Prev'];

        return ['success' => true, 'message' => 'Success'];
    }

    public static function snst($params) {
        $noid = $params['noid'];
        $data = $params['data'];
        $datalog = $params['datalog'];

        if (!$noid) {
            $noid = (new static)->getLastNoid((new static)->main['table']);
            $datalog['idtypetranc'] = (new static)->main['idtypetranc'];
            $datalog['logsubject'] = $datalog['logsubject'].$noid;
            $datalog['keterangan'] = 'Save to Issued';
        }

        $updatemaster = DB::connection('mysql2')
            ->table((new static)->main['table'])
            ->where('noid', $noid)
            ->update($data);
        if (!$updatemaster) return ['success' => false, 'message' => 'Error Update RAP'];

        $insertlog = DB::connection('mysql2')
            ->table('applog')
            ->insert($datalog);
        if (!$insertlog) return ['success' => false, 'message' => 'Error Insert LOG'];

        return ['success' => true, 'message' => 'Success'];
    }

    public static function div($data, $noid, $datalog) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($data);

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultlog ? 1 : 0;
    }

    public static function sapcf($data, $noid, $datalog) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($data);

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultlog ? 1 : 0;
    }

    public static function setPending($data, $noid, $datalog) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($data);

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultlog ? 1 : 0;
    }

    public static function setCanceled($data, $noid, $datalog) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($data);

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultlog ? 1 : 0;
    }

    public static function generateCode($params) {
        $tablemaster = $params['tablemaster'];
        $formdefault = explode('/', $params['generatecode']);
        
        $result_appmkode = DB::connection('mysql2')->table('appmkode')->where('noid', $formdefault[1])->first();
        $result_appmkodedetail = DB::connection('mysql2')->table('appmkodedetail')->where('idkode', $formdefault[1])->orderBy('nourut', 'ASC')->get();

        $formdefault = '';
        foreach ($result_appmkodedetail as $k2 => $v2) {
            if ($v2->kodename == 'KODE') {
                $formdefault .= $v2->kodevalue;
            } else if ($v2->kodename == 'NOURUT') {
                $groupby = '';
                $fungsigroupby = explode(';', $v2->fungsigroupby);
                foreach ($fungsigroupby as $k3 => $v3) {
                    if ($v3) {
                        $groupby .= $v3.',';
                    }
                }
                $groupby = substr($groupby, 0 ,-1);
                $fieldname = $v2->fieldname;
                $fungsi = $v2->fungsi;
                $where = 'idtypecostcontrol='.$params['idtypecostcontrol'].' AND YEAR(docreate)='.date('Y');
                // $qtylastnourut = "SELECT MAX(nourut) as qtylnu FROM $tablemaster GROUP BY $groupby";

                $qtylastnourut = "SELECT $fungsi($fieldname) as qtylnu FROM $tablemaster WHERE $where GROUP BY $groupby";
                $resultqtylnu = DB::connection('mysql2')->select($qtylastnourut);
                // if (count($resultqtylnu) == 0) {
                //     $qtylastnourut = "SELECT $fungsi($fieldname) as qtylnu FROM $tablemaster GROUP BY $groupby";
                //     $resultqtylnu = DB::connection('mysql2')->select($qtylastnourut);
                // }                
                if (count($resultqtylnu) == 0) {
                    $qtylnu = 1;
                } else {
                    $qtylnu = $resultqtylnu[0]->qtylnu+1;
                }
                
                // $fungsigroupby = explode(';', $v2->fungsigroupby);
                // // $query_nourut = "SELECT MAX(noid) AS noid FROM $tablemaster WHERE $fungsigroupby[2] = $v2->idkode GROUP BY ";
                // $query_nourut = "SELECT MAX(docreate) AS noid FROM $tablemaster ";
                // foreach ($fungsigroupby as $k3 => $v3) {
                //     if ($v3 != '') {
                //         if ($k3 == 0) {
                //             $query_nourut .= 'GROUP BY ';
                //         }
                //         if ($v3) {
                //             $query_nourut .= "$v3,";
                //         }
                //     }
                // }
                // $query_nourut = substr($query_nourut, 0, -1);
                // $query_nourut .= " LIMIT 1";
                // $result_nourut = DB::connection('mysql2')->select($query_nourut);
                // if ($result_nourut) {
                //     $nourut = substr($v2->kodeformat, 0, strlen($v2->kodeformat)-strlen($result_nourut[0]->noid)).$result_nourut[0]->noid;
                // } else {
                //     $nourut = 0;
                // }
                $nourut = '';
                $idx = 0;
                for ($i=strlen($qtylnu); $i<strlen($v2->kodeformat); $i ++) {
                    $nourut .= $v2->kodeformat[$idx];
                    $idx++;
                }
                $nourut .= $qtylnu;
                $formdefault .= date('y').'.'.($params['kodetypecostcontrol']).'.'.$nourut;
            } else if ($v2->kodename == 'BULAN') {
                $check1 = explode('[', $v2->kodevalue)[0];
                if (substr($check1, 0, 1) == '/') {
                    $check2 = explode(']', explode('[', $v2->kodevalue)[1]);
                    if (count($check2) > 1) {
                        $function = $check2[0];
                    }
                } else {
                    $function = 'FALSE';
                }
                if ($function == $v2->fungsi) {
                    $kodeformat = explode('%', $v2->kodeformat);
                    $formdefault .= '/'.date($kodeformat[1]);
                }
            } else if ($v2->kodename == 'TAHUN') {
                $check1 = explode('[', $v2->kodevalue)[0];
                if (substr($check1, 0, 1) == '/') {
                    $check2 = explode(']', explode('[', $v2->kodevalue)[1]);
                    if (count($check2) > 1) {
                        $function = $check2[0];
                    }
                } else {
                    $function = 'FALSE';
                }
                if ($function == $v2->fungsi) {
                    $kodeformat = explode('%', $v2->kodeformat);
                    $formdefault .= '/'.date($kodeformat[1]);
                }
            }
        }

        $resultnourut = DB::connection('mysql2')->select("SELECT MAX(nourut) AS nourut FROM $tablemaster WHERE idtypecostcontrol=".$params['idtypecostcontrol']." AND YEAR(docreate)=".date('Y'));

        return [
            'kode' => $formdefault,
            'nourut' => count($resultnourut) == 0 ? 1 : $resultnourut[0]->nourut+1,
        ];
    }

    public static function deleteSelectM($selecteds, $selectcheckboxmall, $datalog) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table']);
        if ((bool)$selectcheckboxmall) {
            $result->delete();
        } else {
            $result->whereIn('noid', $selecteds)->delete();
        }

        if ((bool)$selectcheckboxmall) {
            $resultdetail = DB::connection('mysql2')
                ->table((new static)->main['tabledetail'])
                ->delete();
        } else {
            foreach ($selecteds as $k => $v) {
                $checkresultdetail = (new static)->findData(['noid'=>$v])['purchasedetail'];
                if (count($checkresultdetail) > 0) {
                    $resultdetail = DB::connection('mysql2')
                                    ->table((new static)->main['tabledetail'])
                                    ->where('idmaster', $v)
                                    ->delete();
                } else {
                    $resultdetail = true;
                }
            }
        }

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultdetail && $resultlog ? 1 : 0;
    }

    public static function deleteData($params) {
        $datalog['idtypetranc'] = (new static)->main['idtypetranc'];
        
        // Delete Master
        $master = DB::connection('mysql2')
            ->table((new static)->main['table'])
            ->where('noid', $params['noid']);
        $masterfirst = $master->first();
        if (!$masterfirst) return ['success' => false, 'message' => 'Error Get Data Master'];
        $masterdelete = $master->delete();
        if (!$masterdelete) return ['success' => false, 'message' => 'Error Delete Data Master'];

        // // Delete Detail
        $detail = DB::connection('mysql2')
            ->table((new static)->main['tabledetail'])
            ->where('idmaster', $params['noid']);
        $detailget = $detail->get();
        if (!$detailget) return ['success' => false, 'message' => 'Error Get Data Master'];
        $detaildelete = $detail->delete();
        if (!$detaildelete) return ['success' => false, 'message' => 'Error Delete Data Master'];

        // Delete CDF
        $cdf = DB::connection('mysql2')
            ->table((new static)->main['tablecdf'])
            ->where('idtypetranc', (new static)->main['idtypetranc'])
            ->where('idreff', $params['noid']);
        $cdfget = $cdf->get();
        if (!$cdfget) return ['success' => false, 'message' => 'Error Get Data Master'];
        $cdfdelete = $cdf->delete();
        if (!$cdfdelete) return ['success' => false, 'message' => 'Error Delete Data Master'];

        // Insert Log
        $insertlog = DB::connection('mysql2')
            ->table('applog')
            ->insert($params['datalog']);
        if (!$insertlog) return ['success' => false, 'message' => 'Error Insert Log'];
        
        return ['success' => true, 'message' => 'Success Delete Data'];
    }

}
