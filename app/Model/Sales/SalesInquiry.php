<?php

namespace App\Model\Sales;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
Use DB;
Use App\Model\RA\RAP;
use Illuminate\Support\Facades\Session;

class SalesInquiry extends Model {
    use Notifiable;

    protected $connection = 'mysql2';
    protected $table = 'salesinquiry';

    public $main = [
        'noid' => '40011102',
        'table' => 'salesinquiry',
        'tabledetail' => 'salesinquirydetail',
        'tablecdf' => 'dmsmdocument',
        'idtypetranc' => 401,
        'idtypetranc2' => 602,
        'table2' => 'prjmrap',
        'tabledetail2' => 'prjmrapdetail',
        'generatecode2' => 'generatecode/402/1',
    ];

    public static function getUrlDownload($params) {
        $result = DB::connection('mysql2')
            ->table('dmsmfile')
            ->where('noid', $params['id'])
            ->first();
        return $result;
    }

    public static function isRule($params) {
        $mcardkode = $params['mcardkode'];
        $mpositionkode = $params['mpositionkode'];

        $resultmcardposition = DB::connection('mysql2')
            ->table('mcardposition AS mcp')
            ->select([
                '*'
            ])
            ->leftJoin('mposition AS mp', 'mp.noid', '=', 'mcp.idposition')
            ->where('mcp.kodecard', $mcardkode)
            ->where('mp.kode', $mpositionkode)
            ->first();

        if (!$resultmcardposition) return false;
        return true;
    }

    public static function getData($params) {
        $table = $params['table'];
        $select = @$params['select'] ? $params['select'] : ['noid','nama'];
        $connection = @$params['connection'] ? $params['connection'] : 'mysql2';
        $isactive = @$params['isactive'] ? $params['isactive'] : false;
        $where = @$params['where'];
        $isshowzero = @$params['isshowzero'];
        $orderby = @$params['orderby'];
        if ($isactive) $select[] = 'isactive';

        $result = DB::connection($connection)
                    ->table($table)
                    ->select($select)
                    ->where(function($query) use ($isactive) {
                        if ($isactive) $query->where('isactive', 1);
                    })
                    ->where(function($query) use ($where, $isshowzero) {
                        if ($where) {
                            foreach ($where as $k => $v) {
                                $query->where($v[0], $v[1], $v[2]);
                            }
                        }
                        if ($isshowzero) {
                            $query->orWhere('noid', 0);
                        }
                    })
                    ->orderByRaw('noid=0 desc');
        
        if (@$orderby) {
            if (count($orderby)>0) {
                foreach ($orderby as $k => $v) {
                    $result->orderBy($v[0],$v[1]);
                }
            }
        } else {
            $result->orderBy('noid', 'asc');
        }
        
        $result = $result->get();
                    
        return $result;
    }

    public static function getDetailInv($params) {
        $querypo = DB::connection('mysql2')
                    ->table('invmgroup AS img')
                    ->join('invminventory AS imi', 'imi.noid', '=', 'img.noid')
                    // ->leftJoin('invmsatuan AS ims', 'ims.noid', '=', 'imi.idsatuan')
                    ->select([
                        // 'tdp.noid AS noid',
                        // 'tdp.idmaster AS idmaster',
                        // 'tp.kode AS kodepo',
                        // 'imi.noid AS idinventor',
                        'imi.noid AS idinventor',
                        // 'tdp.idgudang AS idgudang',
                        // 'tdp.idcompany AS idcompany',
                        // 'tdp.iddepartment AS iddepartment',
                        // 'imi.idsatuan AS idsatuan',
                        // 'imi.konversi AS konvsatuan',
                        // 'tdp.subtotal AS subtotal',
                        // 'tdp.discountvar AS discountvar',
                        // 'tdp.discount AS discount',
                        // 'tdp.nilaidisc AS nilaidisc',
                        // 'tdp.idtypetax AS idtypetax',
                        // 'tdp.idtax AS idtax',
                        // 'tdp.prosentax AS prosentax',
                        // 'tdp.nilaitax AS nilaitax',
                        // 'tp.kode AS purchaseorder_kode',
                        'imi.kode AS kodeinventor',
                        'imi.nama AS namainventor',
                        // 'tdp.namainventor AS namainventor2',
                        // 'tdp.keterangan AS keterangan',
                        // 'tdp.unitqty AS unitqty',
                        // 'tdp.unitqtysisa AS unitqtysisa',
                        // 'ims.nama AS namasatuan',
                        // 'imi.purchaseprice AS unitprice',
                        // 'tdp.hargatotal AS hargatotal',
                        // 'amc.nama AS namacurrency',
                    ])
                    ->where('img.noid', '!=', null)
                    ->where('img.noid', '!=', 0)
                    ->where('imi.isactive', '!=', 0);
                    // ->where('tp.idstatustranc', 3)
                    // ->where('tp.idcardsupplier', $params['idcardsupplier'])
                    // ->where('tdp.unitqtysisa', '>', 0);
        
        if ($params['searchprev']) {
            $searchprev = $params['searchprev'];
            $querypo->where(function($query) use ($searchprev) {
                $query->where('imi.kode', 'like', "%$searchprev%")
                    ->orWhere('imi.nama', 'like', "%$searchprev%");
            });
        }

        // $result->whereNotIn('tdp.noid', $detailprev)
        //         ->orderBy('tdp.noid', 'asc');
        
        if ($params['length'] != null && $params['length'] != null) {
            $querypo->offset($params['start'])
                    ->limit($params['length']);
        }

        $resultprev = $querypo->get();

        $result = DB::connection('mysql2')
                    ->table((new static)->main['tabledetail'])
                    ->where('idtrancprev', $params['searchprev'])
                    ->get();

        $res = [];
        foreach ($result as $k => $v) {
            $res[$v->idtrancprevd] = @$res[$v->idtrancprevd] ? $res[$v->idtrancprevd]+$v->unitqty : $v->unitqty;
        }

        $response = [];
        foreach ($resultprev as $k => $v) {
            $response[$k] = $v;
            // if (array_key_exists($v->noid, $res)) {
            //     $response[$k]->unitqtysisa = $v->unitqtysisa-$res[$v->noid];
            // }
        }

        return $response;
    }

    public static function getAllNoid($table) {
        $result = DB::connection('mysql2')
                    ->table($table)
                    ->select('noid')
                    ->get();

        $response = [];
        foreach($result as $k => $v) {
            $response[] = $v->noid;
        }
        return $response;
    }

    public static function findMTypeTranc() {
        $result = DB::connection('mysql2')
                    ->table('mtypetranc')
                    ->select(['noid','kode','nama'])
                    ->where('noid', (new static)->main['idtypetranc'])
                    ->orderBy('noid', 'asc')
                    ->first();
        return $result;
    }

    public static function findMTypeTrancTypeStatus($idtypetranc) {
        $result = DB::connection('mysql2')
                    ->table('mtypetranctypestatus')
                    ->select(['noid','idtypetranc','kode','nama','classicon','classcolor'])
                    ->where('idtypetranc', $idtypetranc)
                    ->where('kode', 'DRF')
                    ->orderBy('nourut', 'asc')
                    ->first();
        return $result;
    }

    public static function getLastMonth() {
        $result = DB::connection('mysql2')->select("SELECT year(max(tanggal)) AS _year, month(max(tanggal)) AS _month FROM ".(new static)->main['table']." LIMIT 1");
        return $result ? $result[0] : (object)['_year'=>date('Y'),'_month'=>date('m')];
    }

    public static function getGenMCompany() {
        $result = DB::connection('mysql2')
                    ->table('genmdepartment AS gmd')
                    ->leftJoin('genmcompany AS gmc', 'gmd.idcompany', '=', 'gmc.noid')
                    ->select([
                        'gmd.noid AS genmdepartment_noid',
                        'gmd.kode AS genmdepartment_kode',
                        'gmd.nama AS genmdepartment_nama',
                        'gmc.noid AS genmcompany_noid',
                        'gmc.kode AS genmcompany_kode',
                        'gmc.nama AS genmcompany_nama',
                    ])
                    ->orderBy('gmc.nama', 'asc')
                    ->orderBy('gmd.nama', 'asc')
                    ->get();
        return $result;
    }

    public static function getInvMInventory() {
        $result = DB::connection('mysql2')
                    ->table('invminventory')
                    ->select([
                        'noid',
                        'kode',
                        'nama',
                        'hpp',
                    ])
                    ->where('level', 3)
                    ->where('isactive', '!=', 0)
                    ->get();
        return $result;
        $result = DB::connection('mysql2')
                    ->table('invminventory AS imi')
                    ->leftJoin('invmsatuan AS ims', 'imi.idsatuan', '=', 'ims.noid')
                    ->select([
                        'imi.noid AS noid',
                        'imi.kode AS kode',
                        'imi.nama AS nama',
                        'imi.idsatuan AS idsatuan',
                        'ims.nama AS namasatuan',
                        'imi.konversi AS konversi',
                        'imi.idtax AS idtax',
                        'imi.taxprocent AS taxprocent',
                        'imi.purchaseprice AS purchaseprice',
                        'imi.purchaseprice AS purchaseprice2',
                        'imi.isjual AS isjual',
                        'imi.isbeli AS isbeli',
                    ])
                    ->orderBy('imi.nama', 'asc')
                    ->get();
        return $result;
    }

    public static function getSales($start=null, $length=null, $search, $orderby, $filter=null, $datefilter, $datefilter2) {
        if ($search) {
            $result = DB::connection('mysql2')
                        ->table((new static)->main['table'].' AS si')
                        ->leftJoin('mtypetranctypestatus AS mttts', 'si.idstatustranc', '=', 'mttts.noid')
                        ->leftJoin('mtypetranc AS mtt', 'si.idtypetranc', '=', 'mtt.noid')
                        // ->leftJoin('mcardsupplier AS mcs', 'si.idcardsupplier', '=', 'mcs.noid')
                        ->leftJoin('genmcompany AS gmc', 'si.idcompany', '=', 'gmc.noid')
                        ->leftJoin('genmdepartment AS gmd', 'si.iddepartment', '=', 'gmd.noid')
                        ->leftJoin('genmgudang AS gmg', 'si.idgudang', '=', 'gmg.noid')
                        ->leftJoin('mcardcustomer AS mc1', 'mc1.noid', '=', 'si.idcardcustomer')
                        ->leftJoin('mcard AS mc2', 'mc2.noid', '=', 'si.idcardsales')
                        ->select([
                            'si.noid',
                            // 'po.noid AS purchaseorder_noid',
                            'mttts.noid AS mtypetranctypestatus_noid',
                            'mttts.idstatusbase AS mtypetranctypestatus_idstatusbase',
                            'mttts.nama AS mtypetranctypestatus_nama',
                            'mttts.classcolor AS mtypetranctypestatus_classcolor',
                            'mtt.noid AS mtypetranc_noid',
                            'mtt.nama AS mtypetranc_nama',
                            'si.idtypecostcontrol',
                            'si.kode',
                            'si.kodereff',
                            'si.tanggal',
                            'si.tanggaldue',
                            // 'mcs.nama AS mcardsupplier_nama',
                            'gmc.nama AS idcompany',
                            'gmd.nama AS iddepartment',
                            'gmg.nama AS idgudang',
                            'si.totalsubtotal',
                            'si.generaltotal',
                            'si.idcardpj',
                            'si.idcardsales',
                            'si.idcarddrafter',
                            'si.idcardestimator',
                            'mc1.nama AS idcardcustomer_nama',
                            'mc2.nama AS idcardsales_nama',
                            'si.projectname',
                            'si.projectvalue',
                            'si.keterangan',
                            'si.tanggal',
                            'si.idcreate',
                        ])
                        ->where('si.idtypetranc', '=', (new static)->main['idtypetranc'])
                        ->where(function($query) use($search) {
                            $query->orWhere('mttts.nama', 'like', "%$search%")
                                ->orWhere('mtt.nama', 'like', "%$search%")
                                ->orWhere('si.kode', 'like', "%$search%")
                                ->orWhere('si.projectname', 'like', "%$search%")
                                ->orWhere('si.projectvalue', 'like', "%$search%")
                                ->orWhere('mc1.nama', 'like', "%$search%")
                                ->orWhere('mc2.nama', 'like', "%$search%");
                                // ->orWhere('si.generaltotal', 'like', "%$search%")
                                // ->orWhere('si.keterangan', 'like', "%$search%")
                                // ->orWhere('si.tanggal', 'like', "%$search%")
                                // ->orWhere('gmg.nama', 'like', "%$search%");
                        });

            $resultdatefilterfrom = DB::connection('mysql2')
                                        ->table((new static)->main['table'])
                                        ->select('tanggal');
            $resultdatefilterto = DB::connection('mysql2')
                                        ->table((new static)->main['table'])
                                        ->select('tanggal');

            if ($filter) {
                foreach ($filter as $k => $v) {
                    if ($v) {
                        $result->where('si.'.$k, '=', $v);
                    }
                }
            }

            if ($datefilter['code'] == 'a') {
                $resultf = $resultdatefilterfrom->orderBy('tanggal', 'asc')->first() ? date('M d, Y', strtotime($resultdatefilterfrom->orderBy('tanggal', 'asc')->first()->tanggal)) : date('M d, Y');
                $resultt = $resultdatefilterto->orderBy('tanggal', 'desc')->first() ? date('M d, Y', strtotime($resultdatefilterto->orderBy('tanggal', 'desc')->first()->tanggal)) : date('M d, Y');
            } else if ($datefilter['code'] == 't') {
                $result->whereDate('si.tanggal', date('Y-m-d'));
                $resultf = date('M d, Y');
                $resultt = date('M d, Y');
            } else if ($datefilter['code'] == 'y') {
                $result->whereDate('si.tanggal', date('Y-m-d', strtotime('-1 day')));
                $resultf = date('M d, Y', strtotime('-1 day'));
                $resultt = date('M d, Y', strtotime('-1 day'));
            } else if ($datefilter['code'] == 'l7d') {
                // $result->whereRaw('si.tanggal >= DATE(NOW()) - INTERVAL 7 DAY');
                $result->whereDate('si.tanggal', '<=', date('Y-m-d'));
                $result->whereDate('si.tanggal', '>=', date('Y-m-d', strtotime('-7 day')));
                $resultf = date('M d, Y', strtotime('-7 day'));
                $resultt = date('M d, Y');
            } else if ($datefilter['code'] == 'l30d') {
                $result->whereDate('si.tanggal', '<=', date('Y-m-d'));
                $result->whereDate('si.tanggal', '>=', date('Y-m-d', strtotime('-1 month')));
                $resultf = date('M d, Y', strtotime('-1 month'));
                $resultt = date('M d, Y');
            } else if ($datefilter['code'] == 'tm') {
                $result->whereYear('si.tanggal', date('Y', strtotime('this year')));
                $result->whereMonth('si.tanggal', date('m', strtotime('this month')));
                $resultf = date('M d, Y', strtotime('first day of this month'));
                $resultt = date('M d, Y', strtotime('last day of this month'));
            } else if ($datefilter['code'] == 'lm') {
                $result->whereYear('si.tanggal', date('Y', strtotime('next year')));
                $result->whereMonth('si.tanggal', date('m', strtotime('next month')));
                $resultf = date('M d, Y', strtotime('first day of previous month'));
                $resultt = date('M d, Y', strtotime('last day of previous month'));
            } else if ($datefilter['code'] == 'cr') {
                $result->whereDate('si.tanggal', '>=', date('Y-m-d', strtotime($datefilter['custom']['from'])));
                $result->whereDate('si.tanggal', '<=', date('Y-m-d', strtotime($datefilter['custom']['to'])));
                $resultf = date('M d, Y', strtotime($datefilter['custom']['from']));
                $resultt = date('M d, Y', strtotime($datefilter['custom']['to']));
            }

            // $result->whereDate('si.tanggal', '>=', $datefilter2['from']);
            // $result->whereDate('si.tanggal', '<=', $datefilter2['to']);

            if ($orderby['column'] == 'noid') {
                $result->orderBy('si.docreate', 'desc');
            } else {
                if ($orderby['column'] == 'idtypetranc' || $orderby['column'] == 'idstatustranc') {
                    $result->orderBy('si.'.$orderby['column'], $orderby['sort']);
                } else {
                    $result->orderBy($orderby['column'], $orderby['sort']);
                }
            }

            if ($start != null && $length != null) {
                $result->offset($start)
                        ->limit($length);
            }
        } else {
            $result = DB::connection('mysql2')
                        ->table((new static)->main['table'].' AS si')
                        ->leftJoin('mtypetranctypestatus AS mttts', 'si.idstatustranc', '=', 'mttts.noid')
                        ->leftJoin('mtypetranc AS mtt', 'si.idtypetranc', '=', 'mtt.noid')
                        ->leftJoin('genmcompany AS gmc', 'si.idcompany', '=', 'gmc.noid')
                        ->leftJoin('genmdepartment AS gmd', 'si.iddepartment', '=', 'gmd.noid')
                        ->leftJoin('genmgudang AS gmg', 'si.idgudang', '=', 'gmg.noid')
                        ->leftJoin('mcardcustomer AS mc1', 'mc1.noid', '=', 'si.idcardcustomer')
                        ->leftJoin('mcard AS mc2', 'mc2.noid', '=', 'si.idcardsales')
                        ->select([
                            'si.noid',
                            // 'po.noid AS purchaseorder_noid',
                            'mttts.noid AS mtypetranctypestatus_noid',
                            'mttts.idstatusbase AS mtypetranctypestatus_idstatusbase',
                            'mttts.nama AS mtypetranctypestatus_nama',
                            'mttts.classcolor AS mtypetranctypestatus_classcolor',
                            'mtt.noid AS mtypetranc_noid',
                            'mtt.nama AS mtypetranc_nama',
                            'si.idtypecostcontrol',
                            'si.kode',
                            'si.kodereff',
                            'si.tanggal',
                            'si.tanggaldue',
                            'gmc.nama AS idcompany',
                            'gmd.nama AS iddepartment',
                            'gmg.nama AS idgudang',
                            'si.totalsubtotal',
                            'si.generaltotal',
                            'si.idcardpj',
                            'si.idcardsales',
                            'si.idcarddrafter',
                            'si.idcardestimator',
                            'mc1.nama AS idcardcustomer_nama',
                            'mc2.nama AS idcardsales_nama',
                            'si.generaltotal',
                            'si.projectname',
                            'si.projectvalue',
                            'si.keterangan',
                            'si.tanggal',
                            'si.idcreate',
                        ])
                        ->where('si.idtypetranc', '=', (new static)->main['idtypetranc']);
                        
            $resultdatefilterfrom = DB::connection('mysql2')
                                        ->table((new static)->main['table'])
                                        ->select('tanggal');
            $resultdatefilterto = DB::connection('mysql2')
                                        ->table((new static)->main['table'])
                                        ->select('tanggal');

                                        if ($filter) {
                foreach ($filter as $k => $v) {
                    if ($v) {
                        $result->where('si.'.$k, '=', $v);
                    }
                }
            }

            if ($datefilter['code'] == 'a') {
                $resultf = $resultdatefilterfrom->orderBy('tanggal', 'asc')->first() ? date('M d, Y', strtotime($resultdatefilterfrom->orderBy('tanggal', 'asc')->first()->tanggal)) : date('M d, Y');
                $resultt = $resultdatefilterto->orderBy('tanggal', 'desc')->first() ? date('M d, Y', strtotime($resultdatefilterto->orderBy('tanggal', 'desc')->first()->tanggal)) : date('M d, Y');
            } else if ($datefilter['code'] == 't') {
                $result->whereDate('si.tanggal', date('Y-m-d'));
                $resultf = date('M d, Y');
                $resultt = date('M d, Y');
            } else if ($datefilter['code'] == 'y') {
                $result->whereDate('si.tanggal', date('Y-m-d', strtotime('-1 day')));
                $resultf = date('M d, Y', strtotime('-1 day'));
                $resultt = date('M d, Y', strtotime('-1 day'));
            } else if ($datefilter['code'] == 'l7d') {
                // $result->whereRaw('si.tanggal >= DATE(NOW()) - INTERVAL 7 DAY');
                $result->whereDate('si.tanggal', '<=', date('Y-m-d'));
                $result->whereDate('si.tanggal', '>=', date('Y-m-d', strtotime('-7 day')));
                $resultf = date('M d, Y', strtotime('-7 day'));
                $resultt = date('M d, Y');
            } else if ($datefilter['code'] == 'l30d') {
                $result->whereDate('si.tanggal', '<=', date('Y-m-d'));
                $result->whereDate('si.tanggal', '>=', date('Y-m-d', strtotime('-1 month')));
                $resultf = date('M d, Y', strtotime('-1 month'));
                $resultt = date('M d, Y');
            } else if ($datefilter['code'] == 'tm') {
                $result->whereYear('si.tanggal', date('Y', strtotime('this year')));
                $result->whereMonth('si.tanggal', date('m', strtotime('this month')));
                $resultf = date('M d, Y', strtotime('first day of this month'));
                $resultt = date('M d, Y', strtotime('last day of this month'));
            } else if ($datefilter['code'] == 'lm') {
                $result->whereYear('si.tanggal', date('Y', strtotime('next year')));
                $result->whereMonth('si.tanggal', date('m', strtotime('next month')));
                $resultf = date('M d, Y', strtotime('first day of previous month'));
                $resultt = date('M d, Y', strtotime('last day of previous month'));
            } else if ($datefilter['code'] == 'cr') {
                $result->whereDate('si.tanggal', '>=', date('Y-m-d', strtotime($datefilter['custom']['from'])));
                $result->whereDate('si.tanggal', '<=', date('Y-m-d', strtotime($datefilter['custom']['to'])));
                $resultf = date('M d, Y', strtotime($datefilter['custom']['from']));
                $resultt = date('M d, Y', strtotime($datefilter['custom']['to']));
            }

            // $result->whereDate('si.tanggal', '>=', $datefilter2['from']);
            // $result->whereDate('si.tanggal', '<=', $datefilter2['to']);

            if ($orderby['column'] == 'noid') {
                $result->orderBy('si.docreate', 'desc');
            } else {
                if ($orderby['column'] == 'idtypetranc' || $orderby['column'] == 'idstatustranc') {
                    $result->orderBy('si.'.$orderby['column'], $orderby['sort']);
                } else {
                    $result->orderBy($orderby['column'], $orderby['sort']);
                }
            }
            
            if ($start != null && $length != null) {
                $result->offset($start)
                        ->limit($length);
            }
        }
        
        return [
            'datefilter' => [
                'type' => $datefilter['code'],
                'from' => $resultf,
                'to' => $resultt
            ],
            'data' => $result->get(),
        ];
    }

    public static function getLog($noid, $kode, $start, $length, $search, $orderby) {
        if ($search) {
            $result = DB::connection('mysql2')
                        ->table('applog AS al')
                        ->leftJoin('appmtypeaction AS amta', 'al.idtypeaction', '=', 'amta.noid')
                        ->leftJoin('mcard AS mc', 'al.idcreate', '=', 'mc.noid')
                        ->leftJoin((new static)->main['table'].' AS prq', 'al.idreff', '=', 'prq.noid')
                        ->select([
                            'al.noid AS noid',
                            'amta.nama AS idtypeaction',
                            'al.logsubject AS logsubject',
                            'al.keterangan AS keterangan',
                            'al.idreff AS idreff',
                            'prq.kode AS kode',
                            'mc.nama AS idcreate',
                            'al.docreate AS docreate'
                        ])
                        ->where('al.idreff', $noid)
                        ->where('al.idtypetranc', (new static)->main['idtypetranc'])
                        ->where('al.logsubject', 'like', "%$kode%")
                        ->where(function($query) use ($search) {
                            $query->where('al.noid', 'like', "%$search%");
                            $query->orWhere('amta.nama', 'like', "%$search%");
                            $query->orWhere('al.logsubject', 'like', "%$search%");
                            $query->orWhere('al.keterangan', 'like', "%$search%");
                            $query->orWhere('mc.nama', 'like', "%$search%");
                            $query->orWhere('al.docreate', 'like', "%$search%");
                        })
                        ->orderBy($orderby['column'], $orderby['sort'])
                        ->offset($start)
                        ->limit($length)
                        ->get();
        } else {
            $result = DB::connection('mysql2')
                        ->table('applog AS al')
                        ->leftJoin('mtypetranc AS mtt', 'al.idtypetranc', '=', 'mtt.noid')
                        ->leftJoin('appmtypeaction AS amta', 'al.idtypeaction', '=', 'amta.noid')
                        ->leftJoin('mcard AS mc', 'al.idcreate', '=', 'mc.noid')
                        ->leftJoin((new static)->main['table'].' AS prq', 'al.idreff', '=', 'prq.noid')
                        ->select([
                            'al.noid AS noid',
                            'amta.nama AS idtypeaction',
                            'al.logsubject AS logsubject',
                            'al.keterangan AS keterangan',
                            'al.idreff AS idreff',
                            'prq.kode AS kode',
                            'mc.nama AS idcreate',
                            'al.docreate AS docreate'
                        ])
                        ->where('al.idreff', $noid)
                        ->where('al.idtypetranc', (new static)->main['idtypetranc'])
                        ->where('al.logsubject', 'like', "%$kode%")
                        ->orderBy($orderby['column'], $orderby['sort'])
                        ->offset($start)
                        ->limit($length)
                        ->get();
        }
        return $result;
    }

    public static function getDefaultCostControl() {
        $result = DB::connection('mysql2')
                    ->table('salesorder')
                    ->where('noid', 0)
                    ->first();
        return $result;
    }

    public static function getCostControl($start, $length, $search, $orderby) {
        if ($search) {
            $result = DB::connection('mysql2')
                        ->table('salesorder AS so')
                        ->leftJoin('mtypecostcontrol AS mtcc', 'so.idtypecostcontrol', '=', 'mtcc.noid')
                        ->leftJoin('mtypetranctypestatus AS mttts', 'so.idstatustranc', '=', 'mttts.noid')
                        ->leftJoin('mtypetranc AS mtt', 'so.idtypetranc', '=', 'mtt.noid')
                        ->leftJoin('genmcompany AS gmc', 'so.idcompany', '=', 'gmc.noid')
                        ->leftJoin('genmdepartment AS gmd', 'so.iddepartment', '=', 'gmd.noid')
                        ->leftJoin('mcarduser AS mcu', 'so.idcreate', '=', 'mcu.noid')
                        ->select([
                            'so.noid',
                            'mtt.nama AS mtt_nama',
                            'mttts.nama AS mttts_nama',
                            'so.kode',
                            'so.kodereff',
                            'so.tanggal',
                            'so.idtypecostcontrol',
                            'mtcc.kode AS mtcc_kode',
                            'mtcc.nama AS mtcc_nama',
                            'so.generaltotal',
                            'so.keterangan',
                            'mcu.myusername AS mcu_myusername',
                            'gmc.nama AS gmc_nama',
                            'gmd.nama AS gmd_nama',
                        ])
                        ->where('so.noid', 'like', "%$search%")
                        ->orWhere('mttts.nama', 'like', "%$search%")
                        ->orWhere('mtt.nama', 'like', "%$search%")
                        ->orWhere('so.kode', 'like', "%$search%")
                        ->orWhere('so.kodereff', 'like', "%$search%")
                        ->orWhere('gmc.nama', 'like', "%$search%")
                        ->orWhere('gmd.nama', 'like', "%$search%")
                        ->orWhere('so.generaltotal', 'like', "%$search%")
                        ->orWhere('so.keterangan', 'like', "%$search%")
                        ->orWhere('so.tanggal', 'like', "%$search%")
                        ->orderBy($orderby['column'], $orderby['sort'])
                        ->offset($start)
                        ->limit($length)
                        ->get();
        } else {
            $result = DB::connection('mysql2')
                        ->table('salesorder AS so')
                        ->leftJoin('mtypecostcontrol AS mtcc', 'so.idtypecostcontrol', '=', 'mtcc.noid')
                        ->leftJoin('mtypetranctypestatus AS mttts', 'so.idstatustranc', '=', 'mttts.noid')
                        ->leftJoin('mtypetranc AS mtt', 'so.idtypetranc', '=', 'mtt.noid')
                        ->leftJoin('genmcompany AS gmc', 'so.idcompany', '=', 'gmc.noid')
                        ->leftJoin('genmdepartment AS gmd', 'so.iddepartment', '=', 'gmd.noid')
                        ->leftJoin('mcarduser AS mcu', 'so.idcreate', '=', 'mcu.noid')
                        ->select([
                            'so.noid',
                            'mtt.nama AS mtt_nama',
                            'mttts.nama AS mttts_nama',
                            'so.kode',
                            'so.kodereff',
                            'so.tanggal',
                            'so.idtypecostcontrol',
                            'so.kode AS mtcc_kode',
                            'mtcc.nama AS mtcc_nama',
                            'so.generaltotal',
                            'so.keterangan',
                            'mcu.myusername AS mcu_myusername',
                            'gmc.nama AS gmc_nama',
                            'gmd.nama AS gmd_nama',
                        ])
                        ->orderBy($orderby['column'], $orderby['sort'])
                        ->offset($start)
                        ->limit($length)
                        ->get();
        }
        return $result;
    }

    public static function getMCardByKodePosition($params) {
        $result = DB::connection('mysql2')
            ->table('mcard AS mc')
            ->select([
                'mc.noid',
                'mc.kode',
                'mc.nama',
            ])
            ->leftjoin('mcardposition AS mcp', 'mcp.kodecard', '=', 'mc.kode')
            ->leftjoin('mposition AS mp', 'mp.noid', '=', 'mcp.idposition')
            ->where('mp.kode', $params['kode'])
            ->orderBy('mc.nama', 'ASC')
            ->get();
        return $result;
    }

    public static function getMCardSupplier() {
        $result = DB::connection('mysql2')
                    ->table('mcardsupplier')
                    ->leftJoin('mlokasiprop','mcardsupplier.idlokasiprop','=','mlokasiprop.noid')
                    ->leftJoin('mlokasikab','mcardsupplier.idlokasikota','=','mlokasikab.noid')
                    ->leftJoin('mlokasikec','mcardsupplier.idlokasikec','=','mlokasikec.noid')
                    ->leftJoin('mlokasikel','mcardsupplier.idlokasikel','=','mlokasikel.noid')
                    ->select([
                        'mcardsupplier.noid AS mcardsupplier_noid',
                        'mcardsupplier.nama AS mcardsupplier_nama',
                        'mlokasiprop.noid AS mlokasiprop_noid',
                        'mlokasiprop.kode AS mlokasiprop_kode',
                        'mlokasiprop.nama AS mlokasiprop_nama',
                        'mlokasikab.noid AS mlokasikab_noid',
                        'mlokasikab.kode AS mlokasikab_kode',
                        'mlokasikab.nama AS mlokasikab_nama',
                        'mlokasikec.noid AS mlokasikec_noid',
                        'mlokasikec.kode AS mlokasikec_kode',
                        'mlokasikec.nama AS mlokasikec_nama',
                        'mlokasikel.noid AS mlokasikel_noid',
                        'mlokasikel.kode AS mlokasikel_kode',
                        'mlokasikel.nama AS mlokasikel_nama',
                    ])
                    ->orderBy('mcardsupplier_noid', 'asc')
                    ->get();
        return $result;
    }

    public static function getMCardCustomer() {
        $result = DB::connection('mysql2')
            ->table('mcardcustomer as mcc')
            ->leftJoin('mlokasiprop as mlprop', 'mcc.idlokasiprop', '=', 'mlprop.noid')
            ->leftJoin('mlokasikab as mlkab', 'mcc.idlokasikota', '=', 'mlkab.noid')
            ->leftJoin('mlokasikec as mlkec', 'mcc.idlokasikec', '=', 'mlkec.noid')
            ->leftJoin('mlokasikel as mlkel', 'mcc.idlokasikel', '=', 'mlkel.noid')
            ->select([
                'mcc.*',
                'mlprop.nama AS idlokasiprop_nama',
                'mlkab.nama AS idlokasikota_nama',
                'mlkec.nama AS idlokasikec_nama',
                'mlkel.nama AS idlokasikel_nama'
            ])
            ->orderBy('nama', 'ASC')
            ->get();
        return $result;
    }

    public static function getMTypeTrancTypeStatus($idtypetranc) {
        $result = DB::connection('mysql2')
                    ->table('mtypetranctypestatus AS mttts')
                    ->leftJoin('appmtypeaction AS amta', 'mttts.noid', '=', 'amta.idstatustrancbase')
                    ->select([
                        'mttts.noid',
                        'mttts.idstatusbase',
                        'mttts.nourut',
                        'mttts.kode',
                        'mttts.nama',
                        'mttts.classicon',
                        'mttts.classcolor',
                        'mttts.isinternal',
                        'mttts.isexternal',
                        'mttts.actview',
                        'mttts.actedit',
                        'mttts.actdelete',
                        'mttts.actreportdetail',
                        'mttts.actreportmaster',
                        'mttts.actsetprevstatus',
                        'mttts.actsetnextstatus',
                        'mttts.actsetstatus',
                        'amta.noid AS appmtypeaction_noid',
                        'amta.nama AS appmtypeaction_nama'
                    ])
                    ->where('mttts.idtypetranc', $idtypetranc)
                    ->where('mttts.isactive', '>', 0)
                    ->orderBy('mttts.nourut', 'asc')
                    ->get();
        return $result;
    }

    public static function saveData($params) {
        $resultmaster = DB::connection('mysql2')
            ->table('salesinquiry')
            ->insert($params['formmaster']);
        if (!$resultmaster) return ['success'=>false,'message'=>'Error Insert Master'];

        if (count($params['formjobs']) > 0) {
            $resultjob = DB::connection('mysql2')
                ->table('salesinquiryjob')
                ->insert($params['formjobs']);
            if (!$resultjob) return ['success'=>false,'message'=>'Error Insert Jobs'];
        }

        if (count($params['formgroups']) > 0) {
            $resultgroup = DB::connection('mysql2')
                ->table('salesinquirygroup')
                ->insert($params['formgroups']);
            if (!$resultgroup) return ['success'=>false,'message'=>'Error Insert Groups'];
        }

        if (count($params['formpurchaseschedules']) > 0) {
            $resultgroup = DB::connection('mysql2')
                ->table('salesinquirypurchaseschedule')
                ->insert($params['formpurchaseschedules']);
            if (!$resultgroup) return ['success'=>false,'message'=>'Error Insert Purchase Schedule'];
        }
        
        if (count($params['formsubgroups']) > 0) {
            $resultsubgroup = DB::connection('mysql2')
                ->table('salesinquirysubgroup')
                ->insert($params['formsubgroups']);
            if (!$resultsubgroup) return ['success'=>false,'message'=>'Error Insert Subgroups'];
        }
        
        if (count($params['forminventories']) > 0) {
            $resultinventory = DB::connection('mysql2')
                ->table('salesinquiryinventory')
                ->insert($params['forminventories']);
            if (!$resultinventory) return ['success'=>false,'message'=>'Error Insert Subgroups'];
        }

        if (count($params['formmilestones']) > 0) {
            $resultmilestone = DB::connection('mysql2')
                ->table('salesinquirymilestone')
                ->insert($params['formmilestones']);
            if (!$resultmilestone) return ['success'=>false,'message'=>'Error Insert Milestones'];
        }

        if (count($params['formheaders']) > 0) {
            $resultheader = DB::connection('mysql2')
                ->table('salesinquiryheader')
                ->insert($params['formheaders']);
            if (!$resultheader) return ['success'=>false,'message'=>'Error Insert Headers'];
        }

        if (count($params['formdetails']) > 0) {
            $resultdetail = DB::connection('mysql2')
                ->table('salesinquirydetail')
                ->insert($params['formdetails']);
            if (!$resultdetail) return ['success'=>false,'message'=>'Error Insert Details'];
        }

        $params['formcustomer']['idupdate'] = Session::get('noid');
        $params['formcustomer']['lastupdate'] = date('Y-m-d H:i:s');
        $resultcustomer = DB::connection('mysql2')
            ->table('mcardcustomer')
            ->where('noid', $params['formmaster']['idcardcustomer'])
            ->update($params['formcustomer']);
        // if (!$resultcustomer) return ['success'=>false,'message'=>'Error Update Customer'];

        if (count($params['formcdf']) > 0) {
            $resultcdf = DB::connection('mysql2')
                ->table('dmsmdocument')
                ->insert($params['formcdf']);
            if (!$resultcdf) return ['success'=>false,'message'=>'Error Insert File'];
        }

        $resultlog = DB::connection('mysql2')
            ->table('applog')
            ->insert($params['formlog']);
        if (!$resultlog) return ['success'=>false,'message'=>'Error Insert Log'];

        return ['success'=>true,'message'=>'Success Insert Sales Inquiry'];
    }

    public static function saveData_($params) {
        // Unset field tidak penting
        if (count($params['data']['detail']) > 0) {
            foreach ($params['data']['detail'] as $k => $v) {
                unset($params['data']['detail'][$k]['kodeprev']);
                unset($params['data']['detail'][$k]['unitqtysisaprev']);
            }
        }

        // Insert Master
        $insertmaster = DB::connection('mysql2')
            ->table((new static)->main['table'])
            ->insert($params['data']['master']);
        if (!$insertmaster) return ['success' => false, 'message' => 'Error Insert Master'];
        
        // Insert Detail
        $insertdetail = DB::connection('mysql2')
            ->table((new static)->main['tabledetail'])
            ->insert($params['data']['detail']);
        if (!$insertdetail) return ['success' => false, 'message' => 'Error Insert Detail'];

        // Insert CDF
        $insertcdf = DB::connection('mysql2')
            ->table((new static)->main['tablecdf'])
            ->insert($params['data']['cdf']);
        if (!$insertcdf) return ['success' => false, 'message' => 'Error Insert File'];

        // Insert Log
        $insertlog = DB::connection('mysql2')
            ->table('applog')
            ->insert($params['datalog']);
        if (!$insertlog) return ['success' => false, 'message' => 'Error Insert Log'];

        //NOTIF SAVE
        (new static)->insertNotification([
            'idtypenotification' => $params['datalog']['idtypeaction'],
            'idtypetranc' => (new static)->main['idtypetranc'],
            'kodereff' => $params['data']['master']['kode'],
            'statusreff' => $params['data']['master']['idstatustranc'],
            'urlreff' => (new static)->main['noid'],
            'idcardassigns' => [$params['data']['master']['idcardsales']],
            'tanggal' => $params['data']['master']['tanggal'],
            'keterangan' => 'Project '.$params['data']['master']['kode']. ' perlu proposal',
        ]);

        return ['success' => true, 'message' => 'Success Insert Data'];
    }
    
    public static function updateData($params) {
        $deletejobs = DB::connection('mysql2')->table('salesinquiryjob')->where('idsalesinquiry', $params['idsalesinquiry'])->delete();
        $deletegroups = DB::connection('mysql2')->table('salesinquirygroup')->where('idsalesinquiry', $params['idsalesinquiry'])->delete();
        $deletepurchaseschedules = DB::connection('mysql2')->table('salesinquirypurchaseschedule')->where('idsalesinquiry', $params['idsalesinquiry'])->delete();
        $deletesubgroups = DB::connection('mysql2')->table('salesinquirysubgroup')->where('idsalesinquiry', $params['idsalesinquiry'])->delete();
        $deleteinventories = DB::connection('mysql2')->table('salesinquiryinventory')->where('idsalesinquiry', $params['idsalesinquiry'])->delete();
        $deletemilestone = DB::connection('mysql2')->table('salesinquirymilestone')->where('idsalesinquiry', $params['idsalesinquiry'])->delete();
        $deleteheader = DB::connection('mysql2')->table('salesinquiryheader')->where('idsalesinquiry', $params['idsalesinquiry'])->delete();
        $deletedetail = DB::connection('mysql2')->table('salesinquirydetail')->where('idsalesinquiry', $params['idsalesinquiry'])->delete();
        $deletecdf = DB::connection('mysql2')->table('dmsmdocument')->where('idreff', $params['idsalesinquiry'])->delete();


        $resultmaster = DB::connection('mysql2')
            ->table('salesinquiry')
            ->where('noid', $params['idsalesinquiry'])
            ->update($params['formmaster']);
        if (!$resultmaster) return ['success'=>false,'message'=>'Error Insert Master'];

        if (count($params['formjobs']) > 0) {
            $resultjob = DB::connection('mysql2')
                ->table('salesinquiryjob')
                ->insert($params['formjobs']);
            if (!$resultjob) return ['success'=>false,'message'=>'Error Insert Jobs'];
        }

        if (count($params['formgroups']) > 0) {
            $resultgroup = DB::connection('mysql2')
                ->table('salesinquirygroup')
                ->insert($params['formgroups']);
            if (!$resultgroup) return ['success'=>false,'message'=>'Error Insert Groups'];
        }

        if (count($params['formpurchaseschedules']) > 0) {
            $resultpurchaseschedule = DB::connection('mysql2')
                ->table('salesinquirypurchaseschedule')
                ->insert($params['formpurchaseschedules']);
            if (!$resultpurchaseschedule) return ['success'=>false,'message'=>'Error Insert Purchase Schedule'];
        }
        
        if (count($params['formsubgroups']) > 0) {
            $resultsubgroup = DB::connection('mysql2')
                ->table('salesinquirysubgroup')
                ->insert($params['formsubgroups']);
            if (!$resultsubgroup) return ['success'=>false,'message'=>'Error Insert Subgroups'];
        }
        
        if (count($params['forminventories']) > 0) {
            $resultinventory = DB::connection('mysql2')
                ->table('salesinquiryinventory')
                ->insert($params['forminventories']);
            if (!$resultinventory) return ['success'=>false,'message'=>'Error Insert Subgroups'];
        }

        if (count($params['formmilestones']) > 0) {
            $resultmilestone = DB::connection('mysql2')
                ->table('salesinquirymilestone')
                ->insert($params['formmilestones']);
            if (!$resultmilestone) return ['success'=>false,'message'=>'Error Insert Milestones'];
        }

        if (count($params['formheaders']) > 0) {
            $resultheader = DB::connection('mysql2')
                ->table('salesinquiryheader')
                ->insert($params['formheaders']);
            if (!$resultheader) return ['success'=>false,'message'=>'Error Insert Headers'];
        }

        if (count($params['formdetails']) > 0) {
            $resultdetail = DB::connection('mysql2')
                ->table('salesinquirydetail')
                ->insert($params['formdetails']);
            if (!$resultdetail) return ['success'=>false,'message'=>'Error Insert Details'];
        }

        $isupdatecustomer = false;
        foreach ($params['formcustomer'] as $k => $v) {
            if (!is_null($v)) {
                $isupdatecustomer = true;
                break;
            }
        }

        if ($isupdatecustomer) {
            $resultcustomer = DB::connection('mysql2')
                ->table('mcardcustomer')
                ->where('noid', $params['formmaster']['idcardcustomer'])
                ->update($params['formcustomer']);
            // if (!$resultcustomer) return ['success'=>false,'message'=>'Error Update Customer'];
        }

        if (count($params['formcdf']) > 0) {
            $resultcdf = DB::connection('mysql2')
                ->table('dmsmdocument')
                ->insert($params['formcdf']);
            if (!$resultcdf) return ['success'=>false,'message'=>'Error Insert File'];
        }

        $resultlog = DB::connection('mysql2')
            ->table('applog')
            ->insert($params['formlog']);
        if (!$resultlog) return ['success'=>false,'message'=>'Error Insert Log'];

        return ['success'=>true,'message'=>'Success Update Sales Inquiry'];
    }

    public static function getNextNoid($table) {
        $result = DB::connection('mysql2')
                    ->table($table)
                    ->orderBy('noid', 'desc')
                    ->first();
        
        return $result ? $result->noid+1 : 1;
    }
    
    public static function getKode($table,$noid) {
        $result = DB::connection('mysql2')
                    ->table($table)
                    ->select('kode')
                    ->where('noid', $noid)
                    ->first();
        
        return $result ? $result->kode : '';
    }

    public static function getLastNoid($table) {
        $result = DB::connection('mysql2')
                    ->table($table)
                    ->orderBy('noid', 'desc')
                    ->first();
        
        return $result ? $result->noid : 1;
    }

    public static function findLog($noid, $kode) {
        $result = DB::connection('mysql2')
                    ->table('applog')
                    ->select('noid')
                    ->where('idreff', $noid)
                    ->where('logsubject', 'like', "%$kode%")
                    ->orderBy('noid', 'asc')
                    ->get();
        return $result;
    }

    public static function findData($params) {
        $queryresult = DB::connection('mysql2')
            ->table((new static)->main['table'].' AS pr')
            ->leftJoin('mtypetranc AS mtt', 'pr.idtypetranc', '=', 'mtt.noid')
            ->leftJoin('mtypetranctypestatus AS mttts', 'pr.idstatustranc', '=', 'mttts.noid')
            ->leftJoin('mcarduser AS mcu', 'pr.idstatuscard', '=', 'mcu.noid')
            ->leftJoin('accmcurrency AS amc', 'pr.idcurrency', '=', 'amc.noid')
            ->leftJoin('mtypecostcontrol AS mtcc', 'mtcc.noid', '=', 'pr.idtypecostcontrol')
            ->leftJoin('mtypeproject AS mtp', 'mtp.noid', '=', 'pr.projecttype')
            ->leftJoin('mcardcustomer AS mccustomer', 'mccustomer.noid', '=', 'pr.idcardcustomer')
            ->leftJoin('mcard AS mcmarketing', 'mcmarketing.noid', '=', 'pr.idcardsales')
            ->leftJoin('mcard AS mcprojectkoor', 'mcprojectkoor.noid', '=', 'pr.idcardkoor')
            ->leftJoin('mcard AS mcprojectmanager', 'mcprojectmanager.noid', '=', 'pr.idcardpj')
            ->leftJoin('mcard AS mcadminsiteproject', 'mcadminsiteproject.noid', '=', 'pr.idcardsitekoor')
            ->leftJoin('mcard AS mcdrafter', 'mcdrafter.noid', '=', 'pr.idcarddrafter')
            ->select([
                'pr.*',
                'mtt.kode AS kodetypetranc',
                'mttts.nama AS mtypetranctypestatus_nama',
                'mttts.classcolor AS mtypetranctypestatus_classcolor',
                'mcu.myusername AS mcarduser_myusername',
                'amc.nama AS namacurrency',
                'mtp.nama AS projecttype_nama',
                'mtcc.nama AS idtypecostcontrol_nama',
                'mccustomer.nama AS idcardcustomer_nama',
                'mcmarketing.nama AS idcardsales_nama',
                'mcprojectkoor.nama AS idcardkoor_nama',
                'mcprojectmanager.nama AS idcardpj_nama',
                'mcadminsiteproject.nama AS idcardsitekoor_nama',
                'mcdrafter.nama AS idcarddrafter_nama',
            ]);

        if (array_key_exists('noid', $params)) {
            $queryresult->where('pr.noid', $params['noid']);
        } else if (array_key_exists('kode', $params)) {
            $queryresult->where('pr.kode', $params['kode']);
        } else if (array_key_exists('kodereff', $params)) {
            $queryresult->where('pr.kodereff', $params['kodereff']);
        }
        
        $result = $queryresult->first();

        $resultcustomer = DB::connection('mysql2')
            ->table('mcardcustomer as mcc')
            ->leftJoin('mlokasiprop as mlprop', 'mcc.idlokasiprop', '=', 'mlprop.noid')
            ->leftJoin('mlokasikab as mlkab', 'mcc.idlokasikota', '=', 'mlkab.noid')
            ->leftJoin('mlokasikec as mlkec', 'mcc.idlokasikec', '=', 'mlkec.noid')
            ->leftJoin('mlokasikel as mlkel', 'mcc.idlokasikel', '=', 'mlkel.noid')
            ->select([
                'mcc.*',
                'mlprop.nama AS idlokasiprop_nama',
                'mlkab.nama AS idlokasikota_nama',
                'mlkec.nama AS idlokasikec_nama',
                'mlkel.nama AS idlokasikel_nama'
            ])
            ->where('mcc.noid', $result->idcardcustomer)
            ->first();

        $resultjob = DB::connection('mysql2')
            ->table('salesinquiryjob')
            ->where('idsalesinquiry', $result->noid)
            ->orderBy('noid', 'asc')
            ->get();

        $resultgroup = DB::connection('mysql2')
            ->table('salesinquirygroup AS sig')
            ->leftJoin('invmgroup AS img', 'img.noid', '=', 'sig.idinventory')
            ->select([
                'sig.*',
                'img.nama AS idinventory_nama'
            ])
            ->where('sig.idsalesinquiry', $result->noid)
            ->orderBy('sig.noid', 'asc')
            ->get();

        $resultpurchaseschedule = DB::connection('mysql2')
            ->table('salesinquirypurchaseschedule')
            ->where('idsalesinquiry', $result->noid)
            ->orderBy('noid', 'asc')
            ->get();

        $resultsubgroup = DB::connection('mysql2')
            ->table('salesinquirysubgroup AS sis')
            ->leftJoin('invmkategori AS imk', 'imk.noid', '=', 'sis.idinventory')
            ->select([
                'sis.*',
                'imk.nama AS idinventory_nama'
            ])
            ->where('sis.idsalesinquiry', $result->noid)
            ->orderBy('sis.noid', 'asc')
            ->get();

        $resultinventory = DB::connection('mysql2')
            ->table('salesinquiryinventory AS sii')
            ->leftJoin('invminventory AS imi', 'imi.noid', '=', 'sii.idinventory')
            ->leftJoin('accmpajak AS amp', 'amp.noid', '=', 'sii.idtax')
            ->select([
                'sii.*',
                'imi.nama AS idinventory_nama',
                'amp.prosentax AS idtax_prosentax'
            ])
            ->where('sii.idsalesinquiry', $result->noid)
            ->orderBy('sii.noid', 'asc')
            ->get();

        $resultmilestone = DB::connection('mysql2')
            ->table('salesinquirymilestone AS sim')
            ->select([
                'sim.noid',
                // 'sim.idmilestone',
                'sim.name',
                'mm.name AS idmilestone_name',
                'sim.startdate',
                'sim.stopdate',
                'sim.description',
                'sim.isshowreport',
            ])
            ->leftJoin('mmilestone AS mm', 'mm.noid', '=', 'sim.idmilestone')
            ->where('sim.idsalesinquiry', $result->noid)
            ->orderBy('sim.noid', 'asc')
            ->get();

        $resultheader = DB::connection('mysql2')
            ->table('salesinquiryheader')
            ->where('idsalesinquiry', $result->noid)
            ->orderBy('noid', 'asc')
            ->get();

        $resultdetail = DB::connection('mysql2')
            ->table('salesinquirydetail')
            ->where('idsalesinquiry', $result->noid)
            ->orderBy('noid', 'asc')
            ->get();

        $resultcdf = DB::connection('mysql2')
                ->table((new static)->main['tablecdf'].' AS cdf')
                ->leftJoin('dmsmfile AS dmf', 'dmf.noid', '=', 'cdf.iddmsfile')
                ->select([
                    'cdf.noid AS cdf_noid',
                    'cdf.iddmsfile AS cdf_iddmsfile',
                    'dmf.nama AS dmf_nama',
                    'dmf.filename AS dmf_filename',
                    'dmf.filepreview AS dmf_filepreview',
                    'cdf.nama AS cdf_nama',
                    'cdf.keterangan AS cdf_keterangan',
                    'cdf.ishardcopy AS cdf_ishardcopy',
                    'cdf.ispublic AS cdf_ispublic',
                    'cdf.doexpired AS cdf_doexpired',
                    'cdf.doreviewlast AS cdf_doreviewlast',
                    'cdf.doreviewnext AS cdf_doreviewnext',
                ])
                ->where('cdf.idreff', $result->noid)
                ->where('cdf.idtypetranc', (new static)->main['idtypetranc'])
                ->orderBy('cdf.noid', 'asc')
                ->get();

        $cdf = [];
        foreach ($resultcdf as $k => $v) {
            $dmf_filename = explode('.', $v->dmf_filename);
            if (end($dmf_filename) == 'jpg' ||
                end($dmf_filename) == 'jpeg' ||
                end($dmf_filename) == 'png' ||
                end($dmf_filename) == 'gif') {
                $dmf_filepreview = $v->dmf_filepreview;
                $dmf_urlfilepreview = $v->dmf_filepreview.'/'.$v->dmf_filename;
            } else {
                $dmf_filepreview = 'filemanager/default';
                $dmf_urlfilepreview = 'filemanager/default/'.end($dmf_filename).'.png';
            }

            $cdf[] = (object)[
                'cdf_noid' => $v->cdf_noid,
                'cdf_nama' => $v->cdf_nama,
                'cdf_iddmsfile' => $v->cdf_iddmsfile,
                'dmf_nama' => $v->dmf_nama,
                'dmf_filename' => $v->dmf_filename,
                'dmf_filepreview' => $dmf_filepreview,
                'dmf_urlfilepreview' => $dmf_urlfilepreview,
                'cdf_nama' => $v->cdf_nama,
                'cdf_keterangan' => $v->cdf_keterangan,
                'cdf_ishardcopy' => $v->cdf_ishardcopy,
                'cdf_ispublic' => $v->cdf_ispublic,
                'cdf_doexpired' => date('d-M-Y', strtotime($v->cdf_doexpired)),
                'cdf_doreviewlast' => date('d-M-Y', strtotime($v->cdf_doreviewlast)),
                'cdf_doreviewnext' => date('d-M-Y', strtotime($v->cdf_doreviewnext)),
            ];
        }

        $resultpurchase = DB::connection('mysql2')
            ->table('salesorder AS so')
            ->leftJoin('purchaserequest AS prq', 'prq.idcostcontrol', '=', 'so.noid')
            ->leftJoin('purchaseoffer AS pof', 'pof.kodereff', '=', 'prq.kode')
            ->select([
                'pof.*'
            ])
            ->where('so.kodereff', $result->kode)
            ->where('pof.idstatustranc', '>=', 5024)
            ->get();

        $totalactualpurchase = 0;
        foreach ($resultpurchase as $k => $v) {
            $totalactualpurchase += $v->generaltotal;
        }

        $data = [
            'master' => $result,
            'jobs' => $resultjob ? $resultjob : [],
            'groups' => $resultgroup ? $resultgroup : [],
            'purchaseschedules' => $resultpurchaseschedule ? $resultpurchaseschedule : [],
            'subgroups' => $resultsubgroup ? $resultsubgroup : [],
            'inventories' => $resultinventory ? $resultinventory : [],
            'milestones' => $resultmilestone ? $resultmilestone : [],
            'headers' => $resultheader ? $resultheader : [],
            'details' => $resultdetail ? $resultdetail : [],
            'cdf' => $cdf,
            'mcc' => $resultcustomer ? $resultcustomer : [],
            'totalactualpurchase' => $totalactualpurchase
        ];

        return $data;
    }

    public static function findLastSupplier() {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->select([
                        'noid',
                        'idcardsupplier',
                        'idakunsupplier',
                        'suppliernama',
                        'supplieraddress1',
                        'supplieraddress2',
                        'supplieraddressrt',
                        'supplieraddressrw',
                        'supplierlokasikel',
                        'supplierlokasikec',
                        'supplierlokasikab',
                        'supplierlokasiprop',
                        'supplierlokasineg',
                    ])
                    ->orderBy('noid', 'desc')
                    ->first();

        return $result;
    }

    public static function sendToNext($params) {
        $resultso = DB::connection('mysql2')
            ->table('salesorder')
            ->insert($params['formmaster']);
        if (!$resultso) return ['success'=>false,'message'=>'Error Insert Master Sales Order'];
        
        return ['success'=>true,'message'=>'Success Insert Sales Master'];
    }

    public static function sendToNext_($params) {
        $generatecode = (new static)->generateCode(['tablemaster'=>(new static)->main['table2'],'generatecode'=>(new static)->main['generatecode2'],'kodetypecostcontrol'=>$params['kodetypecostcontrol']]);
        $data = (new static)->findData(['noid'=>$params['noid']]);
        $idtrancprev = $data['purchase']->noid;
        $idstatustrancprq = $data['purchase']->idstatustranc;
        $data['purchase']->noid = $params['nextnoid'];
        $data['purchase']->idstatustranc = 6021;
        $data['purchase']->idtypetranc = (new static)->main['idtypetranc2'];
        $data['purchase']->kodereff = $data['purchase']->kode;
        $data['purchase']->kode = $generatecode['kode'];
        $data['purchase']->nourut = $params['datalog'][1]['idreff'];
        $data['purchase']->idtypetrancprev = (new static)->main['idtypetranc'];
        $data['purchase']->idtrancprev = $idtrancprev;
        unset($data['purchase']->mtypetranctypestatus_nama);
        unset($data['purchase']->mtypetranctypestatus_classcolor);
        unset($data['purchase']->mcarduser_myusername);
        unset($data['purchase']->kodecostcontrol);
        unset($data['purchase']->namacurrency);
        unset($data['purchase']->kodetypetranc);
        unset($data['purchase']->idtypecostcontrol);

        $detailpo = [];
        foreach($data['purchasedetail'] as $k => $v) {
            $idtrancprevd = $data['purchasedetail'][$k]->noid;
            $data['purchasedetail'][$k]->noid = $params['nextnoiddetail'];
            $data['purchasedetail'][$k]->idmaster = $params['nextnoid'];
            $data['purchasedetail'][$k]->idtypetrancprev = (new static)->main['idtypetranc'];
            $data['purchasedetail'][$k]->idtrancprev = $idtrancprev;
            $data['purchasedetail'][$k]->idtrancprevd = $idtrancprevd;
            unset($data['purchasedetail'][$k]->kodeprev);
            unset($data['purchasedetail'][$k]->kodeinventor);
            unset($data['purchasedetail'][$k]->namainventor2);
            unset($data['purchasedetail'][$k]->namasatuan);
            $detailpo[] = (array)$v;
            $params['nextnoiddetail']++;
        }

        if ($idstatustrancprq != 4) {
            $resultprq = DB::connection('mysql2')
                            ->table((new static)->main['table'])
                            ->where('noid', $params['noid'])
                            ->update(['idstatustranc'=>4014]);
        } else {
            $resultprq = true;
        }

        $resultpo = DB::connection('mysql2')
                    ->table((new static)->main['table2'])
                    ->insert((array)$data['purchase']);
        
        $resultdetailpo = DB::connection('mysql2')
                            ->table((new static)->main['tabledetail2'])
                            ->insert($detailpo);

        $resultlog = DB::connection('mysql2')
                        ->table('applog')
                        ->insert($params['datalog']);

        return $resultprq && $resultpo && $resultdetailpo && $resultlog ? 1 : 0;
    }

    public static function completeCheck($params) {
        $inventories = DB::connection('mysql2')
            ->table('salesinquiryinventory')
            ->where('idsalesinquiry', $params['noid'])
            ->get();

        foreach ($inventories as $k => $v) {
            $iscomplete = false;
            if ($params['idtypecostcontrol'] == 1 || $params['idtypecostcontrol'] == 2) {
                $iscomplete = $v->idinventory != '' && $v->alias != '' && $v->idunit != '' && $v->quantity != '' && $v->buyprice != 0 && $v->baseprice != 0 && $v->offerprice != 0;
            } else {
                $iscomplete = $v->idinventory != '' && $v->alias != '' && $v->idunit != '' && $v->quantity != '' && $v->offerprice != 0;
            }

            if (!$iscomplete) {
                return [
                    'success' => false,
                    // 'message' => 'Ada harga inventory yg belum diisi. Mohon untuk dilengkapi!'
                    'message' => 'Data belum lengkap. Mohon untuk dilengkapi!'
                ];
            }
        }

        return [
            'success' => true,
            'message' => 'Data lengkap.'
        ];
    }

    public static function snst($params) {
        $noid = $params['noid'];
        $kode = $params['kode'];
        $data = $params['data'];
        $datalog = $params['datalog'];
        $keterangannotif = $params['keterangannotif'];
        $isdirector = $params['isdirector'];
        $iscreater = $params['iscreater'];
        $isestimator = $params['isestimator'];
        $ismarketing = $params['ismarketing'];
        $isprojectmanager = $params['isprojectmanager'];
        $resultdirectors = (new static)->getMCardByKodePosition(['kode'=>'DIR']);
        $estimator = $params['estimator'];
        $marketing = $params['marketing'];
        $projectmanager = $params['projectmanager'];
        $directors = [];
        if (count($resultdirectors) > 0) {
            foreach ($resultdirectors as $k => $v) {
                $directors[] = $v->noid;
            }
        }


        if ($data['idstatustranc'] == 4010) {
            // CANCELED NOTIF
            // $resultdelete = (new static)->deleteNotification([
            //     'tablereff' => (new static)->main['table'],
            //     'kodereff' => $kode,
            //     'statusreff' => 4011,
            // ]);
        } else if ($data['idstatustranc'] == 4011) { // DRAFT
            // CANCELED NOTIF
            // $resultdelete = (new static)->deleteNotification([
            //     'tablereff' => (new static)->main['table'],
            //     'kodereff' => $kode,
            //     'statusreff' => 4013,
            // ]);

            // NOTIF PROJECT KOOR
            // if (Session::get('noid') != $data['idcardkoor']) {
            //     (new static)->insertNotification([
            //         'insert' => [
            //             'noid' => (new static)->getNextNoid('appnotification'),
            //             'idtypenotification' => 1, 
            //             'idtypetranc' => 401, 
            //             'idreff' => 0, 
            //             'tablereff' => (new static)->main['table'], 
            //             'kodereff' => $kode, 
            //             'statusreff' => $data['idstatustranc'], 
            //             'idcardassign' => $data['idcardkoor'], 
            //             'tanggal' => @$data['tanggal'], 
            //             'keterangan' => $keterangannotif.' (Need to Verify)', 
            //             'isviewed' => 0, 
            //             'isread' => 0, 
            //             'urlreff' => (new static)->main['noid'], 
            //             'isneedaction' => 0, 
            //             'idcreate' => Session::get('noid'), 
            //             'docreate' => date('Y-m-d H:i:s')
            //         ],
            //         'idtypetranc' => (new static)->main['idtypetranc'],
            //         'idtypeaction' => @$idtypeaction,
            //         'urlreff' => (new static)->main['noid'],
            //         'tanggal' => @$data['tanggal'],
            //         'keterangan' => @$data['keterangan'],
            //     ]);
            // }
        } else if ($data['idstatustranc'] == 4012) { // READY TO PROCESS
            if ($iscreater) {
                // NOTIF ESTIMATOR
                if (count($estimator) > 0) {
                    (new static)->insertNotification([
                        'idtypenotification' => $params['datalog']['idtypeaction'],
                        'idtypetranc' => (new static)->main['idtypetranc'],
                        'kodereff' => $params['kode'],
                        'statusreff' => $params['data']['idstatustranc'],
                        'urlreff' => (new static)->main['noid'],
                        'idcardassigns' => $estimator,
                        'tanggal' => $params['data']['tanggal'],
                        'keterangan' => 'Sales Inquiry '.$params['kode']. ' perlu di ACC BY ESTIMATOR',
                    ]);
                }
            }
        } else if ($data['idstatustranc'] == 4013) { // ACC BY ESTIMATOR
            if ($isestimator) {
                // NOTIF MARKETING
                if (count($marketing) > 0) {
                    (new static)->insertNotification([
                        'idtypenotification' => $params['datalog']['idtypeaction'],
                        'idtypetranc' => (new static)->main['idtypetranc'],
                        'kodereff' => $params['kode'],
                        'statusreff' => $params['data']['idstatustranc'],
                        'urlreff' => (new static)->main['noid'],
                        'idcardassigns' => $marketing,
                        'tanggal' => $params['data']['tanggal'],
                        'keterangan' => 'Sales Inquiry '.$params['kode']. ' perlu di ACC BY MARKETING',
                    ]);
                }
            }
        } else if ($data['idstatustranc'] == 4014) { // ACC BY MARKETING
            if ($ismarketing) {
                // NOTIF ESTIMATOR
                if (count($projectmanager) > 0) {
                    (new static)->insertNotification([
                        'idtypenotification' => $params['datalog']['idtypeaction'],
                        'idtypetranc' => (new static)->main['idtypetranc'],
                        'kodereff' => $params['kode'],
                        'statusreff' => $params['data']['idstatustranc'],
                        'urlreff' => (new static)->main['noid'],
                        'idcardassigns' => $projectmanager,
                        'tanggal' => $params['data']['tanggal'],
                        'keterangan' => 'Sales Inquiry '.$params['kode']. ' perlu di ACC BY PROJECT MANAGER',
                    ]);
                }
            }
        } else if ($data['idstatustranc'] == 4015) { // ACC BY PROJECT MANAGER
            if ($isprojectmanager) {
                // NOTIF DIRECTOR
                if (count($directors) > 0) {
                    (new static)->insertNotification([
                        'idtypenotification' => $params['datalog']['idtypeaction'],
                        'idtypetranc' => (new static)->main['idtypetranc'],
                        'kodereff' => $params['kode'],
                        'statusreff' => $params['data']['idstatustranc'],
                        'urlreff' => (new static)->main['noid'],
                        'idcardassigns' => $directors,
                        'tanggal' => $params['data']['tanggal'],
                        'keterangan' => 'Sales Inquiry '.$params['kode']. ' perlu di ACC BY DIRECTOR',
                    ]);
                }
            }
        } else if ($data['idstatustranc'] == 4016) { // ACC BY DIRECTOR
            if ($isdirector) {
                // NOTIF MARKETING
                if (count($marketing) > 0) {
                    (new static)->insertNotification([
                        'idtypenotification' => $params['datalog']['idtypeaction'],
                        'idtypetranc' => (new static)->main['idtypetranc'],
                        'kodereff' => $params['kode'],
                        'statusreff' => $params['data']['idstatustranc'],
                        'urlreff' => (new static)->main['noid'],
                        'idcardassigns' => $marketing,
                        'tanggal' => $params['data']['tanggal'],
                        'keterangan' => 'Sales Inquiry '.$params['kode']. ' perlu di ACC BY MARKETING',
                    ]);
                }
            }
        }

        // (new static)->insertNotification([
        //     'idtypetranc' => (new static)->main['idtypetranc'],
        //     'idtypeaction' => $data['idstatustranc'],
        //     'urlreff' => (new static)->main['noid'],
        //     'tanggal' => $data['tanggal'],
        //     'keterangan' => $datalog['keterangan'],
        // ]);

        if (!$noid) {
            $noid = (new static)->getLastNoid((new static)->main['table']);
            $datalog['idtypetranc'] = (new static)->main['idtypetranc'];
            $datalog['logsubject'] = $datalog['logsubject'].$noid;
            $datalog['keterangan'] = 'Save to Issued';
        }

        $dataupdate = [
            'tanggal' => $data['tanggal'],
            'idstatustranc' => $data['idstatustranc'],
        ];

        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($dataupdate);

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultlog ? 1 : 0;
    }

    public static function snst_($data, $noid, $datalog) {
        if (!$noid) {
            $noid = (new static)->getLastNoid((new static)->main['table']);
            $datalog['idtypetranc'] = (new static)->main['idtypetranc'];
            $datalog['logsubject'] = $datalog['logsubject'].$noid;
            $datalog['keterangan'] = 'Save to Issued';
        }

        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($data);

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultlog ? 1 : 0;
    }

    public static function div($data, $noid, $datalog) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($data);

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultlog ? 1 : 0;
    }

    public static function sapcf($data, $noid, $datalog) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($data);

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultlog ? 1 : 0;
    }

    public static function setPending($data, $noid, $datalog) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($data);

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultlog ? 1 : 0;
    }

    public static function setCanceled($data, $noid, $datalog) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($data);

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultlog ? 1 : 0;
    }

    public static function generateCode($params) {
        $tablemaster = $params['tablemaster'];
        $formdefault = explode('/', $params['generatecode']);
        
        $result_appmkode = DB::connection('mysql2')->table('appmkode')->where('noid', $formdefault[1])->first();
        $result_appmkodedetail = DB::connection('mysql2')->table('appmkodedetail')->where('idkode', $formdefault[1])->orderBy('nourut', 'ASC')->get();

        $formdefault = '';
        foreach ($result_appmkodedetail as $k2 => $v2) {
            if ($v2->kodename == 'KODE') {
                $formdefault .= $v2->kodevalue;
            } else if ($v2->kodename == 'NOURUT') {
                $groupby = '';
                $fungsigroupby = explode(';', $v2->fungsigroupby);
                foreach ($fungsigroupby as $k3 => $v3) {
                    if ($v3) {
                        $groupby .= $v3.',';
                    }
                }
                $groupby = substr($groupby, 0 ,-1);
                $fieldname = $v2->fieldname;
                $fungsi = $v2->fungsi;
                $where = 'idtypecostcontrol='.$params['idtypecostcontrol'].' AND YEAR(docreate)='.date('Y');
                // $qtylastnourut = "SELECT MAX(nourut) as qtylnu FROM $tablemaster GROUP BY $groupby";

                $qtylastnourut = "SELECT $fungsi($fieldname) as qtylnu FROM $tablemaster WHERE $where GROUP BY $groupby ORDER BY qtylnu DESC LIMIT 1";
                $resultqtylnu = DB::connection('mysql2')->select($qtylastnourut);
                // if (count($resultqtylnu) == 0) {
                //     $qtylastnourut = "SELECT $fungsi($fieldname) as qtylnu FROM $tablemaster GROUP BY $groupby";
                //     $resultqtylnu = DB::connection('mysql2')->select($qtylastnourut);
                // }                
                if (count($resultqtylnu) == 0) {
                    $qtylnu = 1;
                } else {
                    $qtylnu = $resultqtylnu[0]->qtylnu+1;
                }
                
                // $fungsigroupby = explode(';', $v2->fungsigroupby);
                // // $query_nourut = "SELECT MAX(noid) AS noid FROM $tablemaster WHERE $fungsigroupby[2] = $v2->idkode GROUP BY ";
                // $query_nourut = "SELECT MAX(docreate) AS noid FROM $tablemaster ";
                // foreach ($fungsigroupby as $k3 => $v3) {
                //     if ($v3 != '') {
                //         if ($k3 == 0) {
                //             $query_nourut .= 'GROUP BY ';
                //         }
                //         if ($v3) {
                //             $query_nourut .= "$v3,";
                //         }
                //     }
                // }
                // $query_nourut = substr($query_nourut, 0, -1);
                // $query_nourut .= " LIMIT 1";
                // $result_nourut = DB::connection('mysql2')->select($query_nourut);
                // if ($result_nourut) {
                //     $nourut = substr($v2->kodeformat, 0, strlen($v2->kodeformat)-strlen($result_nourut[0]->noid)).$result_nourut[0]->noid;
                // } else {
                //     $nourut = 0;
                // }
                $nourut = '';
                $idx = 0;
                for ($i=strlen($qtylnu); $i<strlen($v2->kodeformat); $i ++) {
                    $nourut .= $v2->kodeformat[$idx];
                    $idx++;
                }
                $nourut .= $qtylnu;
                $formdefault .= date('y').'.'.($params['kodetypecostcontrol']).'.'.$nourut;
            } else if ($v2->kodename == 'BULAN') {
                $check1 = explode('[', $v2->kodevalue)[0];
                if (substr($check1, 0, 1) == '/') {
                    $check2 = explode(']', explode('[', $v2->kodevalue)[1]);
                    if (count($check2) > 1) {
                        $function = $check2[0];
                    }
                } else {
                    $function = 'FALSE';
                }
                if ($function == $v2->fungsi) {
                    $kodeformat = explode('%', $v2->kodeformat);
                    $formdefault .= '/'.date($kodeformat[1]);
                }
            } else if ($v2->kodename == 'TAHUN') {
                $check1 = explode('[', $v2->kodevalue)[0];
                if (substr($check1, 0, 1) == '/') {
                    $check2 = explode(']', explode('[', $v2->kodevalue)[1]);
                    if (count($check2) > 1) {
                        $function = $check2[0];
                    }
                } else {
                    $function = 'FALSE';
                }
                if ($function == $v2->fungsi) {
                    $kodeformat = explode('%', $v2->kodeformat);
                    $formdefault .= '/'.date($kodeformat[1]);
                }
            }
        }

        $resultnourut = DB::connection('mysql2')->select("SELECT MAX(nourut) AS nourut FROM $tablemaster WHERE idtypecostcontrol=".$params['idtypecostcontrol']." AND YEAR(docreate)=".date('Y'));

        return [
            'kode' => $formdefault,
            'nourut' => count($resultnourut) == 0 ? 1 : $resultnourut[0]->nourut+1,
        ];
    }

    public static function deleteSelectM($selecteds, $selectcheckboxmall, $datalog) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table']);
        if ((bool)$selectcheckboxmall) {
            $result->delete();
        } else {
            $result->whereIn('noid', $selecteds)->delete();
        }

        if ((bool)$selectcheckboxmall) {
            $resultdetail = DB::connection('mysql2')
                ->table((new static)->main['tabledetail'])
                ->delete();
        } else {
            foreach ($selecteds as $k => $v) {
                $checkresultdetail = (new static)->findData(['noid'=>$v])['detail'];
                if (count($checkresultdetail) > 0) {
                    $resultdetail = DB::connection('mysql2')
                                    ->table((new static)->main['tabledetail'])
                                    ->where('idmaster', $v)
                                    ->delete();
                } else {
                    $resultdetail = true;
                }
            }
        }

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultdetail && $resultlog ? 1 : 0;
    }

    public static function deleteData($params) {
        $noid = $params['noid'];
        $log = $params['log'];

        $deletemaster = DB::connection('mysql2')
            ->table((new static)->main['table'])
            ->where('noid', $noid)
            ->delete();
        // if (!$deletemaster) return ['success' => false, 'message' => 'Error Delete Master'];

        $deletejob = DB::connection('mysql2')
            ->table('salesinquiryjob')
            ->where('idsalesinquiry', $noid)
            ->delete();
        // if (!$deletejob) return ['success' => false, 'message' => 'Error Delete Job'];

        $deletegroup = DB::connection('mysql2')
            ->table('salesinquirygroup')
            ->where('idsalesinquiry', $noid)
            ->delete();
        // if (!$deletegroup) return ['success' => false, 'message' => 'Error Delete Group'];

        $deletesubgroup = DB::connection('mysql2')
            ->table('salesinquirysubgroup')
            ->where('idsalesinquiry', $noid)
            ->delete();
        // if (!$deletesubgroup) return ['success' => false, 'message' => 'Error Delete Subgroup'];

        $deleteinventory = DB::connection('mysql2')
            ->table('salesinquiryinventory')
            ->where('idsalesinquiry', $noid)
            ->delete();
        // if (!$deleteinventory) return ['success' => false, 'message' => 'Error Delete Inventory'];

        $deletemilestone = DB::connection('mysql2')
            ->table('salesinquirymilestone')
            ->where('idsalesinquiry', $noid)
            ->delete();
        // if (!$deletemilestone) return ['success' => false, 'message' => 'Error Delete Milestone'];

        $deleteheader = DB::connection('mysql2')
            ->table('salesinquiryheader')
            ->where('idsalesinquiry', $noid)
            ->delete();
        // if (!$deleteheader) return ['success' => false, 'message' => 'Error Delete Header'];

        $deletedetail = DB::connection('mysql2')
            ->table('salesinquirydetail')
            ->where('idsalesinquiry', $noid)
            ->delete();
        // if (!$deletedetail) return ['success' => false, 'message' => 'Error Delete Detail'];

        $deletecdf = DB::connection('mysql2')
            ->table((new static)->main['tablecdf'])
            ->where('idreff', $noid)
            ->delete();
        // if (!$deletecdf) return ['success' => false, 'message' => 'Error Delete File'];

        // $insertlog = DB::connection('mysql2')
        //     ->table('applog')
        //     ->insert($log);
        // if (!$insertlog) return ['success' => false, 'message' => 'Error Insert Log'];

        $deletelog = DB::connection('mysql2')
            ->table('applog')
            ->where('idtypetranc', (new static)->main['idtypetranc'])
            ->where('idreff', $noid)
            ->delete();

        return ['success' => true, 'message' => 'Success Delete Data'];
    }


    public static function insertNotification($params) {
        $nextnoidan = (new static)->getNextNoid('appnotification');

        $forman = [];
        foreach ($params['idcardassigns'] as $k => $v) {
            $forman[] = [
                'noid' => $nextnoidan,
                'idtypenotification' => $params['idtypenotification'],
                'idtypetranc' => $params['idtypetranc'],
                'idreff' => @$params['idreff'] ? $params['idreff'] : 0,
                'tablereff' => (new static)->main['table'],
                'kodereff' => $params['kodereff'],
                'statusreff' => $params['statusreff'],
                'idcardassign' => $v,
                'tanggal' => $params['tanggal'],
                'keterangan' => $params['keterangan'],
                'isviewed' => @$params['isviewed'] ? $params['isviewed'] : 0,
                'isread' => @$params['isread'] ? $params['isread'] : 0,
                'urlreff' => @$params['urlreff'],
                'isneedaction' => @$params['isneedaction'] ? $params['isneedaction'] : 0,
                'idcreate' => Session::get('noid'),
                'docreate' => date('Y-m-d H:i:s')
            ];
            $nextnoidan++;
        }

        $result = DB::connection('mysql2')->table('appnotification')->insert($forman);
        return $result;
    }

    public static function getNotification() {
        return DB::connection('mysql2')
            ->table('appnotification AS an')
            ->leftJoin('appmtypenotification AS amtn','an.idtypenotification','=','amtn.noid')
            ->select([
                'an.noid AS an_noid',
                'an.keterangan AS an_keterangan',
                'an.urlreff AS an_urlreff',
                'an.docreate AS an_docreate',
                'amtn.nama AS amtn_nama',
                'amtn.classicon AS amtn_classicon',
                'amtn.classcolor AS amtn_classcolor'
            ])
            ->where('an.idcardassign',Session::get('noid'))
            ->where('an.isread',0)
            ->orderBy('an.docreate','desc')
            ->get();
    }

    public static function checkRuleNotification($params) {
        $checkexist = DB::connection('mysql2')
                        ->table('mcardnotification')
                        ->where('idcarduser',$params['idcarduser'])
                        ->where('idcardrolenotif',$params['idcardrolenotif'])
                        ->where('idcardnotif',$params['idcardnotif'])
                        ->where('idtypetranc',$params['idtypetranc'])
                        ->where('idtypeaction',$params['idtypeaction'])
                        ->first();
        return $checkexist ? true : false;
    }

    public static function makeRuleNotification($params) {
        $result = DB::connection('mysql2')
                    ->table('mcardnotification')
                    ->insert($params);
        return $result;
    }

    public static function getSubgroup($params) {
        $result = DB::connection('mysql2')
            ->table('invmkategori')
            ->select(['noid', 'kode', 'nama'])
            ->where('idinvgroup', $params['idgroup'])
            // ->where('level', 2)
            ->get();
        return $result;
    }

    public static function getTax() {
        $result = DB::connection('mysql2')
            ->table('accmpajak')
            ->get();
        return $result;
    }

}
