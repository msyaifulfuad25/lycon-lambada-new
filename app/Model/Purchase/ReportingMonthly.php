<?php

namespace App\Model\Purchase;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
Use DB;
use Illuminate\Support\Facades\Session;

class ReportingMonthly extends Model {
    use Notifiable;

    protected $connection = 'mysql2';
    protected $table = 'purchaseinvoice';

    public $main = [
        'table' => 'purchaseinvoice',
        'tabledetail' => 'purchaseinvoicedetail',
        'tableprev' => 'purchaseorder',
        'tabledetailprev' => 'purchaseorderdetail',
        'idtypetranc' => 505,
        'idtypetranc2' => 503,
        'table2' => 'purchaseorder',
        'tabledetail2' => 'purchaseorderdetail',
        'generatecode2' => 'generatecode/503/1',
    ];

    public static function getData($params) {
        $result = DB::connection(@$params['connection'] ? $params['connection'] : 'mysql2')
                    ->table($params['table'])
                    ->select(@$params['select'] ? $params['select'] : ['noid','nama']);
        @$params['isactive'] ? $result->where('isactive', '>', 0)->orderBy('noid', 'asc') : $result->orderBy('noid', 'asc');
        return $result->get();
    }

    public static function getDetailInv($params) {
        $orderby = [
            'noid' => 'imi.noid',
            'kodeprev' => 'imi.noid',
            'idinventor' => 'imi.noid',
            'idsatuan' => 'imi.idsatuan',
            'konvsatuan' => 'imi.konversi',
            'kodeinventor' => 'imi.kode',
            'namainventor' => 'imi.nama',
            'namainventor2' => 'imi.noid',
            'keterangan' => 'imi.noid',
            'unitqty' => 'imi.noid',
            'unitqtysisa' => 'imi.noid',
            'invmgroup' => 'imi.invmgroup',
            'namasatuan' => 'ims.nama',
            'unitprice' => 'imi.purchaseprice',
            'hargatotal' => 'imi.noid',
            'namacurrency' => 'imi.noid',
        ];
        $querypo = DB::connection('mysql2')
                    ->table('invminventory AS imi')
                    ->leftJoin('invmsatuan AS ims', 'imi.idsatuan', '=', 'ims.noid')
                    ->select([
                        // 'tdp.noid AS noid',
                        // 'tdp.idmaster AS idmaster',
                        // 'tp.kode AS kodepo',
                        'imi.noid AS idinventor',
                        // 'tdp.idgudang AS idgudang',
                        // 'tdp.idcompany AS idcompany',
                        // 'tdp.iddepartment AS iddepartment',
                        'imi.idsatuan AS idsatuan',
                        'imi.konversi AS konvsatuan',
                        // 'tdp.subtotal AS subtotal',
                        // 'tdp.discountvar AS discountvar',
                        // 'tdp.discount AS discount',
                        // 'tdp.nilaidisc AS nilaidisc',
                        // 'tdp.idtypetax AS idtypetax',
                        // 'tdp.idtax AS idtax',
                        // 'tdp.prosentax AS prosentax',
                        // 'tdp.nilaitax AS nilaitax',
                        // 'tp.kode AS purchaseorder_kode',
                        'imi.kode AS kodeinventor',
                        'imi.nama AS namainventor',
                        // 'tdp.namainventor AS namainventor2',
                        // 'tdp.keterangan AS keterangan',
                        // 'tdp.unitqty AS unitqty',
                        // 'tdp.unitqtysisa AS unitqtysisa',
                        'ims.nama AS namasatuan',
                        'imi.purchaseprice AS unitprice',
                        // 'tdp.hargatotal AS hargatotal',
                        // 'amc.nama AS namacurrency',
                    ])
                    ->where('imi.noid', '!=', 0)
                    ->where('imi.isactive', '!=', 0);
                    // ->where('tp.idstatustranc', 3)
                    // ->where('tp.idcardsupplier', $params['idcardsupplier'])
                    // ->where('tdp.unitqtysisa', '>', 0);
        
        if ($params['searchprev']) {
            $searchprev = $params['searchprev'];
            $querypo->where(function($query) use ($searchprev) {
                $query->where('imi.kode', 'like', "%$searchprev%")
                    ->orWhere('imi.nama', 'like', "%$searchprev%");
            });
        }

        // $result->whereNotIn('tdp.noid', $detailprev)
        //         ->orderBy('tdp.noid', 'asc');
        $querypo->orderBy($orderby[$params['ordercolumn']], $params['orderdir']);
        
        if ($params['length'] != null && $params['length'] != null) {
            $querypo->offset($params['start'])
                    ->limit($params['length']);
        }

        $resultprev = $querypo->get();

        $result = DB::connection('mysql2')
                    ->table((new static)->main['tabledetail'])
                    ->where('idtrancprev', $params['searchprev'])
                    ->get();

        $res = [];
        foreach ($result as $k => $v) {
            $res[$v->idtrancprevd] = @$res[$v->idtrancprevd] ? $res[$v->idtrancprevd]+$v->unitqty : $v->unitqty;
        }

        $response = [];
        foreach ($resultprev as $k => $v) {
            $response[$k] = $v;
            // if (array_key_exists($v->noid, $res)) {
            //     $response[$k]->unitqtysisa = $v->unitqtysisa-$res[$v->noid];
            // }
        }

        return $response;
    }
    
    public static function getDetailPrev($params) {
        $orderby = [
            'noid' => 'imi.noid',
            'kodeprev' => 'imi.noid',
            'idinventor' => 'imi.noid',
            'idsatuan' => 'imi.idsatuan',
            'konvsatuan' => 'imi.konversi',
            'kodeinventor' => 'imi.kode',
            'namainventor' => 'imi.nama',
            'namainventor2' => 'imi.noid',
            'keterangan' => 'imi.noid',
            'unitqty' => 'imi.noid',
            'unitqtysisa' => 'imi.noid',
            'invmgroup' => 'imi.invmgroup',
            'namasatuan' => 'ims.nama',
            'unitprice' => 'imi.purchaseprice',
            'hargatotal' => 'imi.noid',
            'namacurrency' => 'imi.noid',
        ];
        $querypo = DB::connection('mysql2')
                    ->table((new static)->main['tabledetailprev'].' AS tdp')
                    ->leftJoin('invminventory AS imi', 'tdp.idinventor', '=', 'imi.noid')
                    ->leftJoin('invmsatuan AS ims', 'tdp.idsatuan', '=', 'ims.noid')
                    ->leftJoin((new static)->main['tableprev'].' AS tp', 'tdp.idmaster', '=', 'tp.noid')
                    ->leftJoin('accmcurrency AS amc', 'tp.idcurrency', '=', 'amc.noid')
                    ->select([
                        'tdp.noid AS noid',
                        'tdp.idmaster AS idmaster',
                        'tp.kode AS kodeprev',
                        'tdp.idinventor AS idinventor',
                        'tdp.idgudang AS idgudang',
                        'tdp.idcompany AS idcompany',
                        'tdp.iddepartment AS iddepartment',
                        'tdp.idsatuan AS idsatuan',
                        'tdp.konvsatuan AS konvsatuan',
                        'tdp.subtotal AS subtotal',
                        'tdp.discountvar AS discountvar',
                        'tdp.discount AS discount',
                        'tdp.nilaidisc AS nilaidisc',
                        'tdp.idtypetax AS idtypetax',
                        'tdp.idtax AS idtax',
                        'tdp.prosentax AS prosentax',
                        'tdp.nilaitax AS nilaitax',
                        'tp.kode AS purchaseorder_kode',
                        'imi.kode AS kodeinventor',
                        'imi.nama AS namainventor',
                        'tdp.namainventor AS namainventor2',
                        'tdp.keterangan AS keterangan',
                        'tdp.unitqty AS unitqty',
                        'tdp.unitqtysisa AS unitqtysisa',
                        'ims.nama AS namasatuan',
                        'tdp.unitprice AS unitprice',
                        'tdp.hargatotal AS hargatotal',
                        'amc.nama AS namacurrency',
                    ])
                    ->where('tp.idstatustranc', 5034)
                    // ->where('tp.idcardsupplier', $params['idcardsupplier'])
                    ->where('tdp.unitqtysisa', '>', 0);
        
        if ($params['searchprev']) {
            $searchprev = $params['searchprev'];
            $querypo->where(function($query) use ($searchprev) {
                $query->where('tp.kode', 'like', "%$searchprev%")
                    ->orWhere('imi.kode', 'like', "%$searchprev%")
                    ->orWhere('imi.nama', 'like', "%$searchprev%");
            });
        }

        if ($params['search']) {
            $search = $params['search'];
            $querypo->where(function($query) use ($search) {
                $query->where('tp.kode', 'like', "%$search%")
                    ->orWhere('imi.kode', 'like', "%$search%")
                    ->orWhere('imi.nama', 'like', "%$search%");
            });
        }

        // $result->whereNotIn('tdp.noid', $detailprev)
        //         ->orderBy('tdp.noid', 'asc');
        $querypo->orderBy($orderby[$params['ordercolumn']], $params['orderdir']);
        
        if ($params['length'] != null && $params['length'] != null) {
            $querypo->offset($params['start'])
                    ->limit($params['length']);
        }

        $resultprev = $querypo->get();

        $result = DB::connection('mysql2')
                    ->table((new static)->main['tabledetail'])
                    ->where('idtrancprev', $params['searchprev'])
                    ->get();

        $res = [];
        foreach ($result as $k => $v) {
            $res[$v->idtrancprevd] = @$res[$v->idtrancprevd] ? $res[$v->idtrancprevd]+$v->unitqty : $v->unitqty;
        }

        $response = [];
        foreach ($resultprev as $k => $v) {
            $response[$k] = $v;
            if (array_key_exists($v->noid, $res)) {
                $response[$k]->unitqtysisa = $v->unitqtysisa-$res[$v->noid];
            }
        }

        return $response;
    }

    public static function getItemOrder($params) {
        $orderby = [
            'noid' => 'imi.noid',
            'kodeprev' => 'imi.noid',
            'idinventor' => 'imi.noid',
            'idsatuan' => 'imi.idsatuan',
            'konvsatuan' => 'imi.konversi',
            'kodeinventor' => 'imi.kode',
            'namainventor' => 'imi.nama',
            'namainventor2' => 'imi.noid',
            'keterangan' => 'imi.noid',
            'unitqty' => 'imi.noid',
            'unitqtysisa' => 'imi.noid',
            'invmgroup' => 'imi.invmgroup',
            'namasatuan' => 'ims.nama',
            'unitprice' => 'imi.purchaseprice',
            'hargatotal' => 'imi.noid',
            'namacurrency' => 'imi.noid',
        ];
        $querypo = DB::connection('mysql2')
                    ->table((new static)->main['tabledetailprev'].' AS tdp')
                    ->leftJoin('invminventory AS imi', 'tdp.idinventor', '=', 'imi.noid')
                    ->leftJoin('invmsatuan AS ims', 'tdp.idsatuan', '=', 'ims.noid')
                    ->leftJoin((new static)->main['tableprev'].' AS tp', 'tdp.idmaster', '=', 'tp.noid')
                    ->leftJoin('accmcurrency AS amc', 'tp.idcurrency', '=', 'amc.noid')
                    ->select([
                        'tdp.noid AS noid',
                        'tdp.idmaster AS idmaster',
                        'tp.kode AS kodeprev',
                        'tdp.idinventor AS idinventor',
                        'tdp.idgudang AS idgudang',
                        'tdp.idcompany AS idcompany',
                        'tdp.iddepartment AS iddepartment',
                        'tdp.idsatuan AS idsatuan',
                        'tdp.konvsatuan AS konvsatuan',
                        'tdp.subtotal AS subtotal',
                        'tdp.discountvar AS discountvar',
                        'tdp.discount AS discount',
                        'tdp.nilaidisc AS nilaidisc',
                        'tdp.idtypetax AS idtypetax',
                        'tdp.idtax AS idtax',
                        'tdp.prosentax AS prosentax',
                        'tdp.nilaitax AS nilaitax',
                        'tp.kode AS purchaseorder_kode',
                        'imi.kode AS kodeinventor',
                        'imi.nama AS namainventor',
                        'tdp.namainventor AS namainventor2',
                        'tdp.keterangan AS keterangan',
                        'tdp.unitqty AS unitqty',
                        'tdp.unitqtysisa AS unitqtysisa',
                        'ims.nama AS namasatuan',
                        'tdp.unitprice AS unitprice',
                        'tdp.hargatotal AS hargatotal',
                        'amc.nama AS namacurrency',
                    ])
                    ->where('tdp.idmaster', $params['idmaster'])
                    // ->where('tp.idstatustranc', 5014)
                    // ->where('tp.idcardsupplier', $params['idcardsupplier'])
                    ->where('tdp.unitqtysisa', '>', 0);
        
        if ($params['searchprev']) {
            $searchprev = $params['searchprev'];
            $querypo->where(function($query) use ($searchprev) {
                $query->where('tp.kode', 'like', "%$searchprev%")
                    ->orWhere('imi.kode', 'like', "%$searchprev%")
                    ->orWhere('imi.nama', 'like', "%$searchprev%");
            });
        }

        if ($params['search']) {
            $search = $params['search'];
            $querypo->where(function($query) use ($search) {
                $query->where('tp.kode', 'like', "%$search%")
                    ->orWhere('imi.kode', 'like', "%$search%")
                    ->orWhere('imi.nama', 'like', "%$search%");
            });
        }

        // $result->whereNotIn('tdp.noid', $detailprev)
        //         ->orderBy('tdp.noid', 'asc');
        $querypo->orderBy($orderby[$params['ordercolumn']], $params['orderdir']);
        
        if ($params['length'] != null && $params['length'] != null) {
            $querypo->offset($params['start'])
                    ->limit($params['length']);
        }

        $resultprev = $querypo->get();

        $result = DB::connection('mysql2')
                    ->table((new static)->main['tabledetail'])
                    ->where('idtrancprev', $params['searchprev'])
                    ->get();

        $res = [];
        foreach ($result as $k => $v) {
            $res[$v->idtrancprevd] = @$res[$v->idtrancprevd] ? $res[$v->idtrancprevd]+$v->unitqty : $v->unitqty;
        }

        $response = [];
        foreach ($resultprev as $k => $v) {
            $response[$k] = $v;
            if (array_key_exists($v->noid, $res)) {
                $response[$k]->unitqtysisa = $v->unitqtysisa-$res[$v->noid];
            }
        }

        return $response;
    }

    public static function getAllNoid($table) {
        $result = DB::connection('mysql2')
                    ->table($table)
                    ->select('noid')
                    ->get();

        $response = [];
        foreach($result as $k => $v) {
            $response[] = $v->noid;
        }
        return $response;
    }

    public static function findMTypeTranc() {
        $result = DB::connection('mysql2')
                    ->table('mtypetranc')
                    ->select(['noid','kode','nama'])
                    ->where('noid', (new static)->main['idtypetranc'])
                    ->orderBy('noid', 'asc')
                    ->first();
        return $result;
    }

    public static function findMTypeTrancTypeStatus($idtypetranc) {
        $result = DB::connection('mysql2')
                    ->table('mtypetranctypestatus')
                    ->select(['noid','idtypetranc','kode','nama','classicon','classcolor'])
                    ->where('idtypetranc', $idtypetranc)
                    ->where('kode', 'DRF')
                    ->orderBy('nourut', 'asc')
                    ->first();
        return $result;
    }

    public static function getLastMonth() {
        $result = DB::connection('mysql2')->select("SELECT year(max(tanggal)) AS _year, month(max(tanggal)) AS _month FROM ".(new static)->main['table']." LIMIT 1");
        return $result ? $result[0] : (object)['_year'=>date('Y'),'_month'=>date('m')];
    }

    public static function getGenMCompany() {
        $result = DB::connection('mysql2')
                    ->table('genmdepartment AS gmd')
                    ->leftJoin('genmcompany AS gmc', 'gmd.idcompany', '=', 'gmc.noid')
                    ->select([
                        'gmd.noid AS genmdepartment_noid',
                        'gmd.kode AS genmdepartment_kode',
                        'gmd.nama AS genmdepartment_nama',
                        'gmc.noid AS genmcompany_noid',
                        'gmc.kode AS genmcompany_kode',
                        'gmc.nama AS genmcompany_nama',
                    ])
                    ->orderBy('gmc.nama', 'asc')
                    ->orderBy('gmd.nama', 'asc')
                    ->get();
        return $result;
    }

    public static function getInvMInventory() {
        $result = DB::connection('mysql2')
                    ->table('invminventory AS imi')
                    ->leftJoin('invmsatuan AS ims', 'imi.idsatuan', '=', 'ims.noid')
                    ->select([
                        'imi.noid AS noid',
                        'imi.kode AS kode',
                        'imi.nama AS nama',
                        'imi.idsatuan AS idsatuan',
                        'ims.nama AS namasatuan',
                        'imi.konversi AS konversi',
                        'imi.idtax AS idtax',
                        'imi.taxprocent AS taxprocent',
                        'imi.purchaseprice AS purchaseprice',
                        'imi.purchaseprice AS purchaseprice2'
                    ])
                    ->orderBy('imi.noid', 'asc')
                    ->get();
        return $result;
    }

    public static function getPurchase($params) {
        $table = @$params['table'];
        $table2 = @$params['table2'];
        $idtypetranc = @$params['idtypetranc'];
        $idtypetranc2 = @$params['idtypetranc2'];
        $start = @$params['start'];
        $length = @$params['length'];
        $search = @$params['search'];
        $orderby = @$params['orderby'];
        $filter = @$params['filter'];
        $datefilter = @$params['datefilter'];
        $datefilter2 = @$params['datefilter2'];
        $kodereffs = @$params['kodereffs'];
        $select = [
            'pr.noid',
            // 'po.noid AS purchaseorder_noid',
            'mttts.noid AS mtypetranctypestatus_noid',
            'mttts.idstatusbase AS mtypetranctypestatus_idstatusbase',
            'mttts.nama AS mtypetranctypestatus_nama',
            'mttts.classcolor AS mtypetranctypestatus_classcolor',
            'mtt.noid AS mtypetranc_noid',
            'mtt.nama AS mtypetranc_nama',
            'pr.kode',
            'pr.kodereff',
            'pr.tanggal',
            'pr.tanggaldue',
            'mcs.nama AS mcardsupplier_nama',
            'gmc.nama AS idcompany',
            'gmd.nama AS iddepartment',
            'gmg.nama AS idgudang',
            'so.kode AS kodecostcontrol',
            'pr.totalsubtotal',
            'pr.generaltotal',
            'pr.keterangan',
            'pr.tanggal',
            'pr.idtrancprev',
            'mc1.nama AS idcardpj_nama',
            'mc2.nama AS idcardkoor_nama',
            'mc3.nama AS idcardsitekoor_nama',
            'mc4.nama AS idcarddrafter_nama',
            'mc5.nama AS idcardrequest_nama'
        ];
        
        if ($table == 'purchaseorder') {
            array_push($select, 'pr.isdeliverydone');
            array_push($select, 'pr.isinvoicedone');
        }

        if ($search) {
            $result = DB::connection('mysql2')
                        ->table($table.' AS pr')
                        // ->leftJoin((new static)->main['table2'].' AS po', function($query) {
                        //     $query->on('po.kodereff','=','pr.kode')
                        //         ->whereRaw('po.noid IN (select MAX(po2.noid) from '.(new static)->main['table2'].' as po2 join '.(new static)->main['table'].' as pr2 on pr2.kode = po2.kodereff group by pr2.noid)');
                        // })
                        ->leftJoin('mtypetranctypestatus AS mttts', 'pr.idstatustranc', '=', 'mttts.noid')
                        ->leftJoin('mtypetranc AS mtt', 'pr.idtypetranc', '=', 'mtt.noid')
                        ->leftJoin('mcardsupplier AS mcs', 'pr.idcardsupplier', '=', 'mcs.noid')
                        ->leftJoin('genmcompany AS gmc', 'pr.idcompany', '=', 'gmc.noid')
                        ->leftJoin('genmdepartment AS gmd', 'pr.iddepartment', '=', 'gmd.noid')
                        ->leftJoin('genmgudang AS gmg', 'pr.idgudang', '=', 'gmg.noid')
                        ->leftJoin('salesorder AS so', 'pr.idcostcontrol', '=', 'so.noid')
                        ->leftJoin('mcard AS mc1', 'so.idcardpj', '=', 'mc1.noid')
                        ->leftJoin('mcard AS mc2', 'so.idcardkoor', '=', 'mc2.noid')
                        ->leftJoin('mcard AS mc3', 'so.idcardsitekoor', '=', 'mc3.noid')
                        ->leftJoin('mcard AS mc4', 'so.idcarddrafter', '=', 'mc4.noid')
                        ->leftJoin('mcard AS mc5', 'pr.idcardrequest', '=', 'mc5.noid')
                        ->select($select)
                        ->where(function($query) use($table) {
                            if ($table == 'purchaseorder') {
                                $query->where('pr.idstatustranc', 5034);
                            }
                        })
                        ->where('pr.idtypetranc', $idtypetranc)
                        ->where(function($query) use($kodereffs) {
                            if ($kodereffs) {
                                $query->whereIn('pr.kodereff', $kodereffs);
                            }
                        })
                        ->where(function($query) use($search) {
                            $query->orWhere('pr.noid', 'like', "%$search%")
                                ->orWhere('mttts.nama', 'like', "%$search%")
                                ->orWhere('mtt.nama', 'like', "%$search%")
                                ->orWhere('pr.kode', 'like', "%$search%")
                                ->orWhere('pr.kodereff', 'like', "%$search%")
                                ->orWhere('pr.tanggal', 'like', "%$search%")
                                ->orWhere('pr.tanggaldue', 'like', "%$search%")
                                ->orWhere('pr.totalsubtotal', 'like', "%$search%")
                                ->orWhere('pr.generaltotal', 'like', "%$search%")
                                ->orWhere('pr.keterangan', 'like', "%$search%")
                                ->orWhere('so.kode', 'like', "%$search%")
                                ->orWhere('gmc.nama', 'like', "%$search%")
                                ->orWhere('gmd.nama', 'like', "%$search%")
                                ->orWhere('mc1.nama', 'like', "%$search%")
                                ->orWhere('mc2.nama', 'like', "%$search%")
                                ->orWhere('mc3.nama', 'like', "%$search%")
                                ->orWhere('mc4.nama', 'like', "%$search%")
                                ->orWhere('mc5.nama', 'like', "%$search%");
                        });
                        // ->where('mttts.noid', '!=', '5056');
                                              
            if (Session::get('groupuser') != 0) {
                // $result->where('pr.idcreate', '=', Session::get('noid'));
            }
                        
            $resultdatefilterfrom = DB::connection('mysql2')
                                        ->table((new static)->main['table'])
                                        ->select('tanggal');
            $resultdatefilterto = DB::connection('mysql2')
                                        ->table((new static)->main['table'])
                                        ->select('tanggal');

            if ($filter) {
                foreach ($filter as $k => $v) {
                    if ($v) {
                        $result->where('pr.'.$k, '=', $v);
                    }
                }
            }

            if ($datefilter['code'] == 'a' || is_null($datefilter)) {
                $resultf = $resultdatefilterfrom->orderBy('tanggal', 'asc')->first() ? date('M d, Y', strtotime($resultdatefilterfrom->orderBy('tanggal', 'asc')->first()->tanggal)) : date('M d, Y');
                $resultt = $resultdatefilterto->orderBy('tanggal', 'desc')->first() ? date('M d, Y', strtotime($resultdatefilterto->orderBy('tanggal', 'desc')->first()->tanggal)) : date('M d, Y');
            } else if ($datefilter['code'] == 't') {
                $result->whereDate('pr.tanggal', date('Y-m-d'));
                $resultf = date('M d, Y');
                $resultt = date('M d, Y');
            } else if ($datefilter['code'] == 'y') {
                $result->whereDate('pr.tanggal', date('Y-m-d', strtotime('-1 day')));
                $resultf = date('M d, Y', strtotime('-1 day'));
                $resultt = date('M d, Y', strtotime('-1 day'));
            } else if ($datefilter['code'] == 'l7d') {
                // $result->whereRaw('pr.tanggal >= DATE(NOW()) - INTERVAL 7 DAY');
                $result->whereDate('pr.tanggal', '<=', date('Y-m-d'));
                $result->whereDate('pr.tanggal', '>=', date('Y-m-d', strtotime('-7 day')));
                $resultf = date('M d, Y', strtotime('-7 day'));
                $resultt = date('M d, Y');
            } else if ($datefilter['code'] == 'l30d') {
                $result->whereDate('pr.tanggal', '<=', date('Y-m-d'));
                $result->whereDate('pr.tanggal', '>=', date('Y-m-d', strtotime('-1 month')));
                $resultf = date('M d, Y', strtotime('-1 month'));
                $resultt = date('M d, Y');
            } else if ($datefilter['code'] == 'tm') {
                $result->whereYear('pr.tanggal', date('Y', strtotime('this year')));
                $result->whereMonth('pr.tanggal', date('m', strtotime('this month')));
                $resultf = date('M d, Y', strtotime('first day of this month'));
                $resultt = date('M d, Y', strtotime('last day of this month'));
            } else if ($datefilter['code'] == 'lm') {
                $lmm = date('m', strtotime('-1 months'));
                $lmy = $lmm == 12 ? date('Y', strtotime('-1 years')) : date('Y');
                $result->whereYear('pr.tanggal', $lmy);
                $result->whereMonth('pr.tanggal', $lmm);
                $resultf = date('M d, Y', strtotime('first day of previous month'));
                $resultt = date('M d, Y', strtotime('last day of previous month'));
            } else if ($datefilter['code'] == 'cr') {
                $result->whereDate('pr.tanggal', '>=', date('Y-m-d', strtotime($datefilter['custom']['from'])));
                $result->whereDate('pr.tanggal', '<=', date('Y-m-d', strtotime($datefilter['custom']['to'])));
                $resultf = date('M d, Y', strtotime($datefilter['custom']['from']));
                $resultt = date('M d, Y', strtotime($datefilter['custom']['to']));
            }

            // $result->whereDate('pr.tanggal', '>=', $datefilter2['from']);
            // $result->whereDate('pr.tanggal', '<=', $datefilter2['to']);

            if (is_null($orderby)) {
                $result->orderBy('pr.docreate', 'asc');
            } else if ($orderby['column'] == 'noid') {
                $result->orderBy('pr.docreate', 'desc');
            } else {
                if ($orderby['column'] == 'idtypetranc' || $orderby['column'] == 'idstatustranc') {
                    $result->orderBy('pr.'.$orderby['column'], $orderby['sort']);
                } else {
                    $result->orderBy($orderby['column'], $orderby['sort']);
                }
            }

            // $result->orderBy(
            //     $orderby['column'] == 0 ? 'pr.noid' : $orderby['column'], 
            //     $orderby['sort']
            // );

            if ($start != null && $length != null) {
                $result->offset($start)
                        ->limit($length);
            }
        } else {
            $prq = DB::connection('mysql2')
                ->table('purchaserequest AS p')
                ->leftJoin('salesorder AS so', 'so.noid', '=', 'p.idcostcontrol')
                ->select(['p.noid',DB::raw('COUNT(p.noid) AS total'),'p.docreate'])
                ->where(function($query) use($filter) { 
                    if (@$filter['idcardkoor']) { $query->where('so.idcardkoor', $filter['idcardkoor']); }
                    if (@$filter['idcardpj']) { $query->where('so.idcardpj', $filter['idcardpj']); }
                    if (@$filter['idcardsitekoor']) { $query->where('so.idcardsitekoor', $filter['idcardsitekoor']); }
                    if (@$filter['idcarddrafter']) { $query->where('so.idcarddrafter', $filter['idcarddrafter']); }
                    $query->where('p.docreate', 'LIKE', $filter['date'].'%'); 
                })
                ->get();

            $pof = DB::connection('mysql2')
                ->table('purchaseoffer AS p')
                ->leftJoin('salesorder AS so', 'so.noid', '=', 'p.idcostcontrol')
                ->select(['p.noid',DB::raw('COUNT(p.noid) AS total'),'p.docreate'])
                ->where(function($query) use($filter) { 
                    if (@$filter['idcardkoor']) { $query->where('so.idcardkoor', $filter['idcardkoor']); }
                    if (@$filter['idcardpj']) { $query->where('so.idcardpj', $filter['idcardpj']); }
                    if (@$filter['idcardsitekoor']) { $query->where('so.idcardsitekoor', $filter['idcardsitekoor']); }
                    if (@$filter['idcarddrafter']) { $query->where('so.idcarddrafter', $filter['idcarddrafter']); }
                    $query->where('p.docreate', 'LIKE', $filter['date'].'%'); 
                })
                ->get();

            $por = DB::connection('mysql2')
                ->table('purchaseorder AS p')
                ->leftJoin('salesorder AS so', 'so.noid', '=', 'p.idcostcontrol')
                ->select(['p.noid',DB::raw('COUNT(p.noid) AS total'),'p.docreate'])
                ->where(function($query) use($filter) { 
                    if (@$filter['idcardkoor']) { $query->where('so.idcardkoor', $filter['idcardkoor']); }
                    if (@$filter['idcardpj']) { $query->where('so.idcardpj', $filter['idcardpj']); }
                    if (@$filter['idcardsitekoor']) { $query->where('so.idcardsitekoor', $filter['idcardsitekoor']); }
                    if (@$filter['idcarddrafter']) { $query->where('so.idcarddrafter', $filter['idcarddrafter']); }
                    $query->where('p.docreate', 'LIKE', $filter['date'].'%'); 
                })
                ->get();

            $prd = DB::connection('mysql2')
                ->table('purchasedelivery AS p')
                ->leftJoin('salesorder AS so', 'so.noid', '=', 'p.idcostcontrol')
                ->select(['p.noid',DB::raw('COUNT(p.noid) AS total'),'p.docreate'])
                ->where(function($query) use($filter) { 
                    if (@$filter['idcardkoor']) { $query->where('so.idcardkoor', $filter['idcardkoor']); }
                    if (@$filter['idcardpj']) { $query->where('so.idcardpj', $filter['idcardpj']); }
                    if (@$filter['idcardsitekoor']) { $query->where('so.idcardsitekoor', $filter['idcardsitekoor']); }
                    if (@$filter['idcarddrafter']) { $query->where('so.idcarddrafter', $filter['idcarddrafter']); }
                    $query->where('p.docreate', 'LIKE', $filter['date'].'%'); 
                })
                ->get();

            $piv = DB::connection('mysql2')
                ->table('purchaseinvoice AS p')
                ->leftJoin('salesorder AS so', 'so.noid', '=', 'p.idcostcontrol')
                ->select(['p.noid',DB::raw('COUNT(p.noid) AS total'),'p.docreate'])
                ->where(function($query) use($filter) { 
                    if (@$filter['idcardkoor']) { $query->where('so.idcardkoor', $filter['idcardkoor']); }
                    if (@$filter['idcardpj']) { $query->where('so.idcardpj', $filter['idcardpj']); }
                    if (@$filter['idcardsitekoor']) { $query->where('so.idcardsitekoor', $filter['idcardsitekoor']); }
                    if (@$filter['idcarddrafter']) { $query->where('so.idcarddrafter', $filter['idcarddrafter']); }
                    $query->where('p.docreate', 'LIKE', $filter['date'].'%'); 
                })
                ->get();

            if (is_null($filter['menu'])) {
                $res = $prq->merge($pof);
                $res = $res->merge($por);
                $res = $res->merge($prd);
                $res = $res->merge($piv);
            } else if ($filter['menu'] == 'purchaserequest') {
                $res = $prq;
            } else if ($filter['menu'] == 'purchaseoffer') {
                $res = $pof;
            } else if ($filter['menu'] == 'purchaseorder') {
                $res = $por;
            } else if ($filter['menu'] == 'purchasedelivery') {
                $res = $prd;
            } else if ($filter['menu'] == 'purchaseinvoice') {
                $res = $piv;
            }
        }
        
        return [
            'datefilter' => [
                'type' => $datefilter['code'],
                // 'from' => $resultf,
                // 'to' => $resultt
            ],
            'data' => $res,
        ];
    }

    public static function getLog($noid, $kode, $start, $length, $search, $orderby) {
        if ($search) {
            $result = DB::connection('mysql2')
                        ->table('applog AS al')
                        ->leftJoin('appmtypeaction AS amta', 'al.idtypeaction', '=', 'amta.noid')
                        ->leftJoin('mcarduser AS mcu', 'al.idcreate', '=', 'mcu.noid')
                        ->leftJoin((new static)->main['table'].' AS prq', 'al.idreff', '=', 'prq.noid')
                        ->select([
                            'al.noid AS noid',
                            'amta.nama AS idtypeaction',
                            'al.logsubject AS logsubject',
                            'al.keterangan AS keterangan',
                            'al.idreff AS idreff',
                            'prq.kode AS kode',
                            'mcu.myusername AS idcreate',
                            'al.docreate AS docreate'
                        ])
                        ->where('al.idreff', $noid)
                        ->where('al.idtypetranc', (new static)->main['idtypetranc'])
                        ->where('al.logsubject', 'like', "%$kode%")
                        ->where(function($query) use ($search) {
                            $query->where('al.noid', 'like', "%$search%");
                            $query->orWhere('amta.nama', 'like', "%$search%");
                            $query->orWhere('al.logsubject', 'like', "%$search%");
                            $query->orWhere('al.keterangan', 'like', "%$search%");
                            $query->orWhere('mcu.myusername', 'like', "%$search%");
                            $query->orWhere('al.docreate', 'like', "%$search%");
                        })
                        ->orderBy($orderby['column'], $orderby['sort'])
                        ->offset($start)
                        ->limit($length)
                        ->get();
        } else {
            $result = DB::connection('mysql2')
                        ->table('applog AS al')
                        ->leftJoin('mtypetranc AS mtt', 'al.idtypetranc', '=', 'mtt.noid')
                        ->leftJoin('appmtypeaction AS amta', 'al.idtypeaction', '=', 'amta.noid')
                        ->leftJoin('mcarduser AS mcu', 'al.idcreate', '=', 'mcu.noid')
                        ->leftJoin((new static)->main['table'].' AS prq', 'al.idreff', '=', 'prq.noid')
                        ->select([
                            'al.noid AS noid',
                            'amta.nama AS idtypeaction',
                            'al.logsubject AS logsubject',
                            'al.keterangan AS keterangan',
                            'al.idreff AS idreff',
                            'prq.kode AS kode',
                            'mcu.myusername AS idcreate',
                            'al.docreate AS docreate'
                        ])
                        ->where('al.idreff', $noid)
                        ->where('al.idtypetranc', (new static)->main['idtypetranc'])
                        ->where('al.logsubject', 'like', "%$kode%")
                        ->orderBy($orderby['column'], $orderby['sort'])
                        ->offset($start)
                        ->limit($length)
                        ->get();
        }
        return $result;
    }

    public static function getDefaultCostControl() {
        $result = DB::connection('mysql2')
                    ->table('salesorder')
                    ->where('noid', 0)
                    ->first();
        return $result;
    }

    public static function getCostControl($params) {
        $start = $params['start'];
        $length = $params['length'];
        $search = $params['search'];
        $orderby = $params['orderby'];
        $idtypecostcontrol = $params['idtypecostcontrol'];
        $getall = $params['getall'];

        $resultcc = DB::connection('mysql2')
                    ->table('purchaseorder')
                    ->select('idcostcontrol')
                    ->where('idcostcontrol','!=',0)
                    ->where('idstatustranc','!=',5036)
                    ->get()->toArray();

        $cc = [];
        // foreach ($resultcc as $k => $v) {
        //     !in_array($v->idcostcontrol, $cc) && $cc[] = $v->idcostcontrol;
        // }
        // dd($cc);
        if ($search) {
            $result = DB::connection('mysql2')
                        ->table('salesorder AS so')
                        ->leftJoin('mtypecostcontrol AS mtcc', 'so.idtypecostcontrol', '=', 'mtcc.noid')
                        ->leftJoin('mtypetranctypestatus AS mttts', 'so.idstatustranc', '=', 'mttts.noid')
                        ->leftJoin('mtypetranc AS mtt', 'so.idtypetranc', '=', 'mtt.noid')
                        ->leftJoin('genmcompany AS gmc', 'so.idcompany', '=', 'gmc.noid')
                        ->leftJoin('genmdepartment AS gmd', 'so.iddepartment', '=', 'gmd.noid')
                        ->leftJoin('mcarduser AS mcu', 'so.idcreate', '=', 'mcu.noid')
                        ->leftJoin('mcard AS mc1', 'so.idcardpj', '=', 'mc1.noid')
                        ->leftJoin('mcard AS mc2', 'so.idcardkoor', '=', 'mc2.noid')
                        ->leftJoin('mcard AS mc3', 'so.idcardsitekoor', '=', 'mc3.noid')
                        ->leftJoin('mcard AS mc4', 'so.idcarddrafter', '=', 'mc4.noid')
                        ->select([
                            'so.noid',
                            'mtt.nama AS mtt_nama',
                            'mttts.nama AS mttts_nama',
                            'so.kode',
                            'so.kodereff',
                            'so.tanggal',
                            'so.idtypecostcontrol',
                            'so.projectname',
                            'so.projectlocation',
                            'mtcc.kode AS mtcc_kode',
                            'mtcc.nama AS mtcc_nama',
                            'so.generaltotal',
                            'so.keterangan',
                            'mcu.myusername AS mcu_myusername',
                            'gmc.nama AS gmc_nama',
                            'gmd.nama AS gmd_nama',
                            'mc1.nama AS idcardpj_nama',
                            'mc2.nama AS idcardkoor_nama',
                            'mc3.nama AS idcardsitekoor_nama',
                            'mc4.nama AS idcarddrafter_nama'
                        ])
                        ->where('so.idtypecostcontrol',$idtypecostcontrol)
                        ->where(function($query) use ($search) {
                            $query->where('so.noid', 'like', "%$search%")
                                ->orWhere('mttts.nama', 'like', "%$search%")
                                ->orWhere('mtt.nama', 'like', "%$search%")
                                ->orWhere('so.kode', 'like', "%$search%")
                                ->orWhere('so.kodereff', 'like', "%$search%")
                                ->orWhere('gmc.nama', 'like', "%$search%")
                                ->orWhere('gmd.nama', 'like', "%$search%")
                                ->orWhere('so.generaltotal', 'like', "%$search%")
                                ->orWhere('so.keterangan', 'like', "%$search%")
                                ->orWhere('so.tanggal', 'like', "%$search%");
                        })
                        ->orderBy($orderby['column'], $orderby['sort']);
            if (!$getall) {
                $result->offset($start)->limit($length);
            }
            $result = $result->get();
        } else {
            $result = DB::connection('mysql2')
                        ->table('salesorder AS so')
                        ->leftJoin('mtypecostcontrol AS mtcc', 'so.idtypecostcontrol', '=', 'mtcc.noid')
                        ->leftJoin('mtypetranctypestatus AS mttts', 'so.idstatustranc', '=', 'mttts.noid')
                        ->leftJoin('mtypetranc AS mtt', 'so.idtypetranc', '=', 'mtt.noid')
                        ->leftJoin('genmcompany AS gmc', 'so.idcompany', '=', 'gmc.noid')
                        ->leftJoin('genmdepartment AS gmd', 'so.iddepartment', '=', 'gmd.noid')
                        ->leftJoin('mcarduser AS mcu', 'so.idcreate', '=', 'mcu.noid')
                        ->leftJoin('mcard AS mc1', 'so.idcardpj', '=', 'mc1.noid')
                        ->leftJoin('mcard AS mc2', 'so.idcardkoor', '=', 'mc2.noid')
                        ->leftJoin('mcard AS mc3', 'so.idcardsitekoor', '=', 'mc3.noid')
                        ->leftJoin('mcard AS mc4', 'so.idcarddrafter', '=', 'mc4.noid')
                        ->select([
                            'so.noid',
                            'mtt.nama AS mtt_nama',
                            'mttts.nama AS mttts_nama',
                            'so.kode',
                            'so.kodereff',
                            'so.tanggal',
                            'so.idtypecostcontrol',
                            'so.projectname',
                            'so.projectlocation',
                            'so.kode AS mtcc_kode',
                            'mtcc.nama AS mtcc_nama',
                            'so.generaltotal',
                            'so.keterangan',
                            'mcu.myusername AS mcu_myusername',
                            'gmc.nama AS gmc_nama',
                            'gmd.nama AS gmd_nama',
                            'mc1.nama AS idcardpj_nama',
                            'mc2.nama AS idcardkoor_nama',
                            'mc3.nama AS idcardsitekoor_nama',
                            'mc4.nama AS idcarddrafter_nama'
                        ])
                        ->whereNotIn('so.noid', $cc)
                        ->where('so.idtypecostcontrol',$idtypecostcontrol)
                        ->orderBy($orderby['column'], $orderby['sort']);
            if (!$getall) {
                $result->offset($start)->limit($length);
            }
            $result = $result->get();
        }
        return $result;
    }

    public static function getMCardSupplier() {
        $result = DB::connection('mysql2')
                    ->table('mcard')
                    ->leftJoin('mcardsupplier','mcard.noid','=','mcardsupplier.noid')
                    ->leftJoin('mlokasiprop','mcardsupplier.idlokasiprop','=','mlokasiprop.noid')
                    ->leftJoin('mlokasikab','mcardsupplier.idlokasikota','=','mlokasikab.noid')
                    ->leftJoin('mlokasikec','mcardsupplier.idlokasikec','=','mlokasikec.noid')
                    ->leftJoin('mlokasikel','mcardsupplier.idlokasikel','=','mlokasikel.noid')
                    ->select([
                        'mcardsupplier.noid AS mcardsupplier_noid',
                        'mcardsupplier.nama AS mcardsupplier_nama',
                        'mlokasiprop.noid AS mlokasiprop_noid',
                        'mlokasiprop.kode AS mlokasiprop_kode',
                        'mlokasiprop.nama AS mlokasiprop_nama',
                        'mlokasikab.noid AS mlokasikab_noid',
                        'mlokasikab.kode AS mlokasikab_kode',
                        'mlokasikab.nama AS mlokasikab_nama',
                        'mlokasikec.noid AS mlokasikec_noid',
                        'mlokasikec.kode AS mlokasikec_kode',
                        'mlokasikec.nama AS mlokasikec_nama',
                        'mlokasikel.noid AS mlokasikel_noid',
                        'mlokasikel.kode AS mlokasikel_kode',
                        'mlokasikel.nama AS mlokasikel_nama',
                    ])
                    ->where('mcard.isactive',1)
                    ->where('mcard.issupplier',1)
                    ->orderBy('mcardsupplier_noid', 'asc')
                    ->get();
        return $result;
    }

    public static function getMTypeTrancTypeStatus($idtypetranc) {
        $result = DB::connection('mysql2')
                    ->table('mtypetranctypestatus AS mttts')
                    ->leftJoin('appmtypeaction AS amta', 'mttts.noid', '=', 'amta.idstatustrancbase')
                    ->select([
                        'mttts.noid',
                        'mttts.idstatusbase',
                        'mttts.nourut',
                        'mttts.kode',
                        'mttts.nama',
                        'mttts.classicon',
                        'mttts.classcolor',
                        'mttts.isinternal',
                        'mttts.isexternal',
                        'mttts.actview',
                        'mttts.actedit',
                        'mttts.actdelete',
                        'mttts.actreportdetail',
                        'mttts.actreportmaster',
                        'mttts.actsetprevstatus',
                        'mttts.actsetnextstatus',
                        'mttts.actsetstatus',
                        'amta.noid AS appmtypeaction_noid',
                        'amta.nama AS appmtypeaction_nama'
                    ])
                    ->where('mttts.idtypetranc', $idtypetranc)
                    ->where('mttts.isactive', '>', 0)
                    ->orderBy('mttts.nourut', 'asc')
                    ->get();
        return $result;
    }

    public static function getNotification() {
        return DB::connection('mysql2')
            ->table('appnotification AS an')
            ->leftJoin('appmtypenotification AS amtn','an.idtypenotification','=','amtn.noid')
            ->select([
                'an.noid AS an_noid',
                'an.keterangan AS an_keterangan',
                'an.urlreff AS an_urlreff',
                'an.docreate AS an_docreate',
                'amtn.nama AS amtn_nama',
                'amtn.classicon AS amtn_classicon',
                'amtn.classcolor AS amtn_classcolor'
            ])
            ->where('an.idcardassign',Session::get('noid'))
            ->where('an.isread',0)
            ->get();
    }

    public static function getSupplierInOffer($params) {
        $result =  DB::connection('mysql2')
                ->table((new static)->main['table'])
                ->select(['idcardsupplier'])
                ->where('kodereff', $params['kodereff'])
                ->get();
        
        $wherenotin = [];
        foreach ($result as $k => $v) {
            $wherenotin[] = $v->idcardsupplier;
        }

        $resultsupplier = DB::connection('mysql2')
                            ->table('mcard AS mc')
                            ->leftJoin('mcardsupplier AS mcs','mc.noid','=','mcs.noid')
                            ->leftJoin('mlokasiprop AS mlprop','mcs.idlokasiprop','=','mlprop.noid')
                            ->leftJoin('mlokasikab AS mlkab','mcs.idlokasikota','=','mlkab.noid')
                            ->leftJoin('mlokasikec AS mlkec','mcs.idlokasikec','=','mlkec.noid')
                            ->leftJoin('mlokasikel AS mlkel','mcs.idlokasikel','=','mlkel.noid')
                            ->select([
                                'mcs.noid AS mcardsupplier_noid',
                                'mcs.nama AS mcardsupplier_nama',
                                'mlprop.noid AS mlokasiprop_noid',
                                'mlprop.kode AS mlokasiprop_kode',
                                'mlprop.nama AS mlokasiprop_nama',
                                'mlkab.noid AS mlokasikab_noid',
                                'mlkab.kode AS mlokasikab_kode',
                                'mlkab.nama AS mlokasikab_nama',
                                'mlkec.noid AS mlokasikec_noid',
                                'mlkec.kode AS mlokasikec_kode',
                                'mlkec.nama AS mlokasikec_nama',
                                'mlkel.noid AS mlokasikel_noid',
                                'mlkel.kode AS mlokasikel_kode',
                                'mlkel.nama AS mlokasikel_nama',
                            ])
                            ->whereNotIn('mcs.noid',$wherenotin)
                            ->where('mc.issupplier', 1)
                            ->get();

        return $resultsupplier;
    }
    
    public static function duplicateData($data, $datalog) {
        // $queryupdate = '';
        // foreach ($data[(new static)->main['tabledetail']] as $k => $v) {
        //     $queryupdate .= 'UPDATE '.(new static)->main['tabledetailprev'].' SET unitqtysisa='.$v['unitqtysisaprev'].' WHERE noid='.$v['idtrancprevd'].' AND idmaster='.$v['idtrancprev'].';';
        //     unset($data[(new static)->main['tabledetail']][$k]['unitqtysisaprev']);
        // }
        // $resultupdate = DB::connection('mysql2')
        //                     ->select($queryupdate);
        unset($data['purchase']->kodetypetranc);
        unset($data['purchase']->mtypetranctypestatus_nama);
        unset($data['purchase']->mtypetranctypestatus_classcolor);
        unset($data['purchase']->mcarduser_myusername);
        unset($data['purchase']->kodecostcontrol);
        unset($data['purchase']->namacurrency);

        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->insert((array)$data['purchase']);
        
        $nextnoiddetail = (new static)->getNextNoid((new static)->main['tabledetail']);
        $purchasedetail = [];
        foreach ($data['purchasedetail'] as $k => $v) {
            unset($v->kodeinventor);
            unset($v->kodeprev);
            unset($v->namainventor2);
            unset($v->namasatuan);
            $v->noid = $nextnoiddetail;
            $v->idmaster = $data['purchase']->noid;
            $purchasedetail[] = (array)$v;
            $nextnoiddetail++;
        }

        $resultdetail = DB::connection('mysql2')
                            ->table((new static)->main['tabledetail'])
                            ->insert($purchasedetail);

        $resultlog = DB::connection('mysql2')
                        ->table('applog')
                        ->insert($datalog);

        return $result && $resultdetail && $resultlog ? 1 : 0;
    }

    public static function saveData($data, $datalog, $isdeliverynote, $iswithso) {
        // $queryupdate = '';
        // foreach ($data[(new static)->main['tabledetail']] as $k => $v) {
        //     $queryupdate .= 'UPDATE '.(new static)->main['tabledetailprev'].' SET unitqtysisa='.$v['unitqtysisaprev'].' WHERE noid='.$v['idtrancprevd'].' AND idmaster='.$v['idtrancprev'].';';
        //     unset($data[(new static)->main['tabledetail']][$k]['unitqtysisaprev']);
        // }
        // $resultupdate = DB::connection('mysql2')
        //                     ->select($queryupdate);

        // if (count($data['purchasedetail']) > 0) {
        //     foreach ($data['purchasedetail'] as $k => $v) {
        //         if (!is_null($v['idtrancprev']) && !is_null($v['idtrancprevd'])) {
        //             $resultupdate[] = DB::connection('mysql2')
        //                                 ->table((new static)->main['tabledetailprev'])
        //                                 ->where('noid', $v['idtrancprevd'])
        //                                 ->where('idmaster', $v['idtrancprev'])
        //                                 // ->first();
        //                                 ->update(['unitqtysisa' => $v['unitqtysisaprev'] < 1 ? 0 : $v['unitqtysisaprev']-$v['unitqty']]);
        //         }
        //         unset($data['purchasedetail'][$k]['noiddetailrecent']);
        //         unset($data['purchasedetail'][$k]['kodeprev']);
        //         unset($data['purchasedetail'][$k]['unitqtysisaprev']);
        //     }
        // }

        if ($iswithso == 1) {
            $pd = [];
            foreach ($data['purchasedetail'] as $k => $v) {
                $pd[$v['idinventor']] = $v;
            }
            
            $selectpo = DB::connection('mysql2')
                ->table('purchaseorder')
                ->where('kode', $data['purchase']['kodereff'])
                ->first();
    
            $selectdpo = DB::connection('mysql2')
                ->table('purchaseorderdetail')
                ->where('idmaster', $selectpo->noid)
                ->get();
    
            $unitqtysisa = 0;
            foreach ($selectdpo as $k => $v) {
                $form = (array)$v;
                $form['unitqtysisa'] = $form['unitqtysisa']-$pd[$form['idinventor']]['unitqty'];
                $unitqtysisa += $form['unitqtysisa'];
    
                $updatedpo = DB::connection('mysql2')
                    ->table('purchaseorderdetail')
                    ->where('noid', $form['noid'])
                    ->update($form);
            }
    
            if ($unitqtysisa == 0) {
                $updatepo = DB::connection('mysql2')
                    ->table('purchaseorder')
                    ->where('kode', $data['purchase']['kodereff'])
                    ->update(['isinvoicedone' => 1]);
                if (!$updatepo) return ['success' => false, 'message' => 'Error Update PO'];
            }
        }



        $insert = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->insert($data['purchase']);
        if (!$insert) return ['success' => false, 'message' => 'Error Insert Master'];
        
        $insertdetail = DB::connection('mysql2')
                            ->table((new static)->main['tabledetail'])
                            ->insert($data['purchasedetail']);
        if (!$insertdetail) return ['success' => false, 'message' => 'Error Insert Detail'];
        
        $insertlog = DB::connection('mysql2')
            ->table('applog')
            ->insert($datalog);
        if (!$insertlog) return ['success' => false, 'message' => 'Error Insert Log'];


        return ['success' => true, 'message' => 'Success Insert'];
    }
    
    public static function updateData($data, $noid, $datalog) {
        $dataprev = [];
        if (count($data['purchasedetail']) > 0) {
            foreach ($data['purchasedetail'] as $k => $v) {
                unset($data['purchasedetail'][$k]['kodeprev']);
                unset($data['purchasedetail'][$k]['unitqtysisaprev']);
                if (!array_key_exists('unitqtysisaprev', $v)) {
                    continue;
                }

                // $recent = DB::connection('mysql2')
                //     ->table((new static)->main['tabledetailprev'])
                //     ->select(['unitqty',])
                //     ->where('noid', $v['idtrancprevd'])
                //     ->first();

                // SET DATA PREV
                // $dataprev[] = [
                //     'noid' => $v['idtrancprevd'],
                //     'unitqty' => $recent->unitqty,
                //     'unitqtysisa' => $recent->unitqty-$v['unitqty']
                // ];

                // SET NOID
                $data['purchasedetail'][$k]['noid'] = $v['noiddetailrecent'];
                unset($data['purchasedetail'][$k]['noiddetailrecent']);

                // UPDATE DETAIL
                $updatedetail = DB::connection('mysql2')
                    ->table((new static)->main['tabledetail'])
                    ->where('noid', $data['purchasedetail'][$k]['noid'])
                    ->update($data['purchasedetail'][$k]);
            }
        }

        // PREV
        // foreach ($dataprev as $k => $v) {
        //     $updateprev[] = DB::connection('mysql2')
        //         ->table((new static)->main['tabledetailprev'])
        //         ->where('noid', $v['noid'])
        //         ->update($v);
        // }

        $insert = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($data['purchase']);
        if (!$insert) return ['success' => false, 'message' => 'Error Update Master'];
        
        // @$deletedetail = DB::connection('mysql2')
        //             ->table((new static)->main['tabledetail'])
        //             ->where('idmaster', $noid)
        //             ->delete();
        // if (!$deletedetail) return ['success' => false, 'message' => 'Error Delete Detail'];
        
        // $insertdetail = DB::connection('mysql2')
        //     ->table((new static)->main['tabledetail'])
        //     ->insert($data['purchasedetail']);
        // if (!$insertdetail) return ['success' => false, 'message' => 'Error Update Detail'];

        $insertlog = DB::connection('mysql2')
                        ->table('applog')
                        ->insert($datalog);
        if (!$insertlog) return ['success' => false, 'message' => 'Error Update Log'];

        return ['success' => true, 'message' => 'Success Update'];
    }

    public static function getNextNoid($table) {
        $result = DB::connection('mysql2')
                    ->table($table)
                    ->orderBy('noid', 'desc')
                    ->first();
        
        return $result ? $result->noid+1 : 1;
    }

    public static function getNextNourut($params) {
        $result = DB::connection('mysql2')
                    ->table($params['table'])
                    ->select('noid','nourut')
                    ->where('idcostcontrol',$params['where'])
                    ->orderBy('noid', 'desc')
                    ->first();
        
        return $result ? $result->nourut+1 : 1;
    }

    public static function findDuplicateNo($params) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->select('noid','duplicateno')
                    ->where('idduplicatefrom',$params['noid'])
                    ->orderBy('duplicateno', 'desc')
                    ->first();
        
        return $result ? $result->duplicateno+1 : 1;
    }
    
    public static function getKode($table,$noid) {
        $result = DB::connection('mysql2')
                    ->table($table)
                    ->select('kode')
                    ->where('noid', $noid)
                    ->first();
        
        return $result ? $result->kode : '';
    }

    public static function getLastNoid($table) {
        $result = DB::connection('mysql2')
                    ->table($table)
                    ->orderBy('noid', 'desc')
                    ->first();
        
        return $result ? $result->noid : 1;
    }

    public static function findLog($noid, $kode) {
        $result = DB::connection('mysql2')
                    ->table('applog')
                    ->select('noid')
                    ->where('idreff', $noid)
                    ->where('logsubject', 'like', "%$kode%")
                    ->orderBy('noid', 'asc')
                    ->get();
        return $result;
    }

    public static function findDataPor($noid) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['tableprev'].' AS pr')
                    ->leftJoin('mtypetranc AS mtt', 'pr.idtypetranc', '=', 'mtt.noid')
                    ->leftJoin('mtypetranctypestatus AS mttts', 'pr.idstatustranc', '=', 'mttts.noid')
                    ->leftJoin('mcarduser AS mcu', 'pr.idstatuscard', '=', 'mcu.noid')
                    ->leftJoin('salesorder AS so', 'pr.idcostcontrol', '=', 'so.noid')
                    ->leftJoin('accmcurrency AS amc', 'pr.idcurrency', '=', 'amc.noid')
                    ->leftJoin('mtypecostcontrol AS mtcc', 'pr.idtypecostcontrol', '=', 'mtcc.noid')
                    ->leftJoin('genmgudang AS gmg', 'pr.idgudang', '=', 'gmg.noid')
                    ->select([
                        'pr.*',
                        'mtt.kode AS kodetypetranc',
                        'mttts.nama AS mtypetranctypestatus_nama',
                        'mttts.classcolor AS mtypetranctypestatus_classcolor',
                        'mcu.myusername AS mcarduser_myusername',
                        'so.kode AS kodecostcontrol',
                        'amc.nama AS namacurrency',
                        'mtcc.nama AS idtypecostcontrol_nama',
                        'gmg.nama AS idgudang_nama'
                    ])
                    ->where('pr.noid', $noid)
                    ->first();

        $resultdetail = DB::connection('mysql2')
                        ->table((new static)->main['tabledetailprev'].' AS prd')
                        ->leftJoin('invminventory AS imi', 'prd.idinventor', '=', 'imi.noid')
                        ->leftJoin('invmsatuan AS ims', 'prd.idsatuan', '=', 'ims.noid')
                        ->select([
                            'prd.noid AS noid',
                            'prd.idinventor AS idinventor',
                            'imi.kode AS kodeinventor',
                            'imi.nama AS namainventor',
                            'prd.namainventor AS namainventor2',
                            'prd.unitqty AS unitqty',
                            'prd.idcompany AS idcompany',
                            'prd.iddepartment AS iddepartment',
                            'prd.idgudang AS idgudang',
                            'prd.keterangan AS keterangan',
                            'prd.idsatuan AS idsatuan',
                            'ims.nama AS namasatuan',
                            'prd.konvsatuan AS konvsatuan',
                            'prd.unitqtysisa AS unitqtysisa',
                            'prd.unitprice AS unitprice',
                            'prd.subtotal AS subtotal',
                            'prd.discountvar AS discountvar',
                            'prd.discount AS discount',
                            'prd.nilaidisc AS nilaidisc',
                            'prd.idtypetax AS idtypetax',
                            'prd.idtax AS idtax',
                            'prd.prosentax AS prosentax',
                            'prd.nilaitax AS nilaitax',
                            'prd.hargatotal AS hargatotal',
                            'prd.idtypetrancprev AS idtypetrancprev',
                            'prd.idtrancprev AS idtrancprev',
                            'prd.idtrancprevd AS idtrancprevd',
                        ])
                        ->where('prd.idmaster', $noid)
                        ->orderBy('imi.kode', 'asc')
                        ->get();

        $data = [
            'purchase' => $result,
            'purchasedetail' => $resultdetail ? $resultdetail : [],
        ];

        return $data;
    }

    public static function findData($noid) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'].' AS pr')
                    ->leftJoin('mtypetranc AS mtt', 'pr.idtypetranc', '=', 'mtt.noid')
                    ->leftJoin('mtypetranctypestatus AS mttts', 'pr.idstatustranc', '=', 'mttts.noid')
                    ->leftJoin('mcarduser AS mcu', 'pr.idstatuscard', '=', 'mcu.noid')
                    ->leftJoin('salesorder AS so', 'pr.idcostcontrol', '=', 'so.noid')
                    ->leftJoin('accmcurrency AS amc', 'pr.idcurrency', '=', 'amc.noid')
                    ->leftJoin('mtypecostcontrol AS mtcc', 'pr.idtypecostcontrol', '=', 'mtcc.noid')
                    ->leftJoin('genmgudang AS gmg', 'pr.idgudang', '=', 'gmg.noid')
                    ->select([
                        'pr.*',
                        'mtt.kode AS kodetypetranc',
                        'mttts.nama AS mtypetranctypestatus_nama',
                        'mttts.classcolor AS mtypetranctypestatus_classcolor',
                        'mcu.myusername AS mcarduser_myusername',
                        'so.kode AS kodecostcontrol',
                        'amc.nama AS namacurrency',
                        'mtcc.nama AS idtypecostcontrol_nama',
                        'gmg.nama AS idgudang_nama'
                    ])
                    ->where('pr.noid', $noid)
                    ->first();

        $resultdetail = DB::connection('mysql2')
                        ->table((new static)->main['tabledetail'].' AS prd')
                        ->leftJoin((new static)->main['tableprev'].' AS pr', 'prd.idtrancprev', '=', 'pr.noid')
                        ->leftJoin('invminventory AS imi', 'prd.idinventor', '=', 'imi.noid')
                        ->leftJoin('invmsatuan AS ims', 'prd.idsatuan', '=', 'ims.noid')
                        ->select([
                            'prd.noid AS noid',
                            'pr.kode AS kodeprev',
                            'prd.idinventor AS idinventor',
                            'imi.kode AS kodeinventor',
                            'imi.nama AS namainventor',
                            'prd.namainventor AS namainventor2',
                            'prd.unitqty AS unitqty',
                            'prd.idcompany AS idcompany',
                            'prd.iddepartment AS iddepartment',
                            'prd.idgudang AS idgudang',
                            'prd.keterangan AS keterangan',
                            'prd.idsatuan AS idsatuan',
                            'ims.nama AS namasatuan',
                            'prd.konvsatuan AS konvsatuan',
                            'prd.unitqtysisa AS unitqtysisa',
                            'prd.unitprice AS unitprice',
                            'prd.subtotal AS subtotal',
                            'prd.discountvar AS discountvar',
                            'prd.discount AS discount',
                            'prd.nilaidisc AS nilaidisc',
                            'prd.idtypetax AS idtypetax',
                            'prd.idtax AS idtax',
                            'prd.prosentax AS prosentax',
                            'prd.nilaitax AS nilaitax',
                            'prd.hargatotal AS hargatotal',
                            'prd.idtypetrancprev AS idtypetrancprev',
                            'prd.idtrancprev AS idtrancprev',
                            'prd.idtrancprevd AS idtrancprevd',
                        ])
                        ->where('prd.idmaster', $noid)
                        ->orderBy('imi.kode', 'asc')
                        ->get();

        $data = [
            'purchase' => $result,
            'purchasedetail' => $resultdetail ? $resultdetail : [],
        ];

        return $data;
    }

    public static function findLastSupplier() {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->select([
                        'noid',
                        'idcardsupplier',
                        'idakunsupplier',
                        'suppliernama',
                        'supplieraddress1',
                        'supplieraddress2',
                        'supplieraddressrt',
                        'supplieraddressrw',
                        'supplierlokasikel',
                        'supplierlokasikec',
                        'supplierlokasikab',
                        'supplierlokasiprop',
                        'supplierlokasineg',
                    ])
                    ->orderBy('noid', 'desc')
                    ->first();

        return $result;
    }

    public static function setSupplier($params) {
        $data = $params['data'];

        $insert = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->insert($data['purchase']);
        if (!$insert) return ['success' => false, 'message' => 'Error Insert Master'];
        
        $insertdetail = DB::connection('mysql2')
                            ->table((new static)->main['tabledetail'])
                            ->insert($data['purchasedetail']);
        if (!$insertdetail) return ['success' => false, 'message' => 'Error Insert Detail'];

        return ['success' => true, 'message' => 'Success Insert'];
    }

    public static function sendToNext($params) {
        $generatecode = (new static)->generateCode([
            'tablemaster' => (new static)->main['table2'],
            'generatecode' => (new static)->main['generatecode2'],
            'data' => $params['data']
        ]);
        $data = (new static)->findData($params['noid']);
        $idtrancprev = $data['purchase']->noid;
        $idstatustrancprev = $data['purchase']->idstatustranc;
        $data['purchase']->noid = $params['nextnoid'];
        $data['purchase']->idstatustranc = 5031;
        $data['purchase']->idtypetranc = (new static)->main['idtypetranc2'];
        $data['purchase']->kodereff = $data['purchase']->kode;
        $data['purchase']->kode = $generatecode['kode'];
        $data['purchase']->nourut = $generatecode['nourut'];
        $data['purchase']->idtypetrancprev = (new static)->main['idtypetranc'];
        $data['purchase']->idtrancprev = $idtrancprev;
        unset($data['purchase']->idduplicatefrom);
        unset($data['purchase']->duplicateno);
        unset($data['purchase']->mtypetranctypestatus_nama);
        unset($data['purchase']->mtypetranctypestatus_classcolor);
        unset($data['purchase']->mcarduser_myusername);
        unset($data['purchase']->kodecostcontrol);
        unset($data['purchase']->namacurrency);
        unset($data['purchase']->kodetypetranc);
        unset($data['purchase']->typepaymentvar);

        $detailpo = [];
        foreach($data['purchasedetail'] as $k => $v) {
            $idtrancprevd = $data['purchasedetail'][$k]->noid;
            $data['purchasedetail'][$k]->noid = $params['nextnoiddetail'];
            $data['purchasedetail'][$k]->idmaster = $params['nextnoid'];
            $data['purchasedetail'][$k]->idtypetrancprev = (new static)->main['idtypetranc'];
            $data['purchasedetail'][$k]->idtrancprev = $idtrancprev;
            $data['purchasedetail'][$k]->idtrancprevd = $idtrancprevd;
            unset($data['purchasedetail'][$k]->kodeprev);
            unset($data['purchasedetail'][$k]->kodeinventor);
            unset($data['purchasedetail'][$k]->namainventor2);
            unset($data['purchasedetail'][$k]->namasatuan);
            $detailpo[] = (array)$v;
            $params['nextnoiddetail']++;
        }
        $resultprevcanceled = DB::connection('mysql2')
                                ->table((new static)->main['table'])
                                ->where('kodereff', $params['kodereff'])
                                ->update(['idstatustranc'=>5056]);

        if ($idstatustrancprev != 4) {
            $resultprevapproved = DB::connection('mysql2')
                            ->table((new static)->main['table'])
                            ->where('noid', $params['noid'])
                            ->update(['idstatustranc'=>5054]);
        } else {
            $resultprevapproved = true;
        }
        
        $resultpo = DB::connection('mysql2')
                    ->table((new static)->main['table2'])
                    ->insert((array)$data['purchase']);
        
        $resultdetailpo = DB::connection('mysql2')
                            ->table((new static)->main['tabledetail2'])
                            ->insert($detailpo);

        $resultlog = DB::connection('mysql2')
                        ->table('applog')
                        ->insert($params['datalog']);

        return $resultprevapproved && $resultprevcanceled && $resultpo && $resultdetailpo && $resultlog ? 1 : 0;
    }

    public static function snst($data, $noid, $datalog) {
        if (!$noid) {
            $noid = (new static)->getLastNoid((new static)->main['table']);
            $datalog['idtypetranc'] = (new static)->main['idtypetranc'];
            $datalog['logsubject'] = $datalog['logsubject'].$noid;
            $datalog['keterangan'] = 'Save to Issued';
        }

        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($data);

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultlog ? 1 : 0;
    }

    public static function div($data, $noid, $datalog) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($data);

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultlog ? 1 : 0;
    }

    public static function sapcf($data, $noid, $datalog) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($data);

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultlog ? 1 : 0;
    }

    public static function setPending($data, $noid, $datalog) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($data);

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultlog ? 1 : 0;
    }

    public static function setCanceled($data, $noid, $datalog) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($data);

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultlog ? 1 : 0;
    }

    public static function getStringBetween($str,$from,$to) {
        $string = substr($str, strpos($str, $from) + strlen($from));
        if (strstr($string,$to,TRUE) != FALSE) {
            $string = strstr($string,$to,TRUE);
        } else {
            $string = FALSE;
        }
        return $string;
    }

    public static function generateCode($params) {
        $tablemaster = $params['tablemaster'];
        $costcontrol = (new static)->getDefaultCostControl();
        $idcostcontrol = @$params['idcostcontrol'] ? $params['idcostcontrol'] : $costcontrol->noid;
        $kodecostcontrol = @$params['kodecostcontrol'] ? $params['kodecostcontrol'] : $costcontrol->kode;
        $formdefault = explode('/', $params['generatecode']);
        
        $result_appmkode = DB::connection('mysql2')->table('appmkode')->where('noid', $formdefault[1])->first();
        $result_appmkodedetail = DB::connection('mysql2')->table('appmkodedetail')->where('idkode', $formdefault[1])->orderBy('nourut', 'ASC')->get();


        $formdefault = '';
        foreach ($result_appmkodedetail as $k2 => $v2) {
            if ($v2->kodename == 'KODE') {
                $formdefault .= $v2->kodevalue;
            } else if ($v2->kodename == 'FIELDTABLE') {
                $fungsigroupby = explode(';',$v2->fungsigroupby);
                $resultfieldtable = DB::connection('mysql2')->table($v2->kodeformat)->where($fungsigroupby[0], $params['data'][$fungsigroupby[1]])->first();
                $fieldname = $v2->fieldname;
                $formdefault .= str_replace('[FIELDTABLE]', $resultfieldtable->$fieldname, $v2->kodevalue);
            } else if ($v2->kodename == 'NOURUT') {
                $groupby = '';
                $fungsigroupby = explode(';', $v2->fungsigroupby);
                $select = '';
                $arrayselect = [];
                $where = 'WHERE 1=1 ';
                if ($v2->fungsigroupby != '' && $v2->fungsigroupby != null) {
                    $groupby .= 'GROUP BY ';
                    foreach ($fungsigroupby as $k3 => $v3) {
                        if ($v3) {
                            $groupby .= $v3.',';
                        }
                        $select .= $v3.',';
                        $arrayselect[] = $v3;

                        $_gsb_month = (new static)->getStringBetween($v3,'MONTH(',')');
                        $_gsb_year = (new static)->getStringBetween($v3,'YEAR(',')');

                        if (@$params['data'][$_gsb_month]) {
                            $where .= 'AND '.$v3.'='.date('m', strtotime($params['data'][$_gsb_month])).' ';
                        } else if (@$params['data'][$_gsb_year]) {
                            $where .= 'AND '.$v3.'='.date('Y', strtotime($params['data'][$_gsb_year])).' ';
                        } else {
                            $where .= 'AND '.$v3.'='.$params['data'][$v3].' ';
                        }
                    }
                    $groupby = substr($groupby, 0 ,-1);
                }
                $fieldname = $v2->fieldname;
                $fungsi = $v2->fungsi;
                // $qtylastnourut = "SELECT MAX(nourut) as qtylnu FROM $tablemaster GROUP BY $groupby";

                $qtylastnourut = "SELECT $select $fungsi($fieldname) as qtylnu FROM $tablemaster $where $groupby";
                $resultqtylnu = DB::connection('mysql2')->select($qtylastnourut);

                

                $qtylnu = 1;
                if (count($resultqtylnu) > 0) {
                    foreach($resultqtylnu as $k3 => $v3) {
                        $found = false; 

                        if (count((array)$v3) > 1) {
                            foreach($v3 as $k4 => $v4) {
                                $_gsb_month = (new static)->getStringBetween($k4,'MONTH(',')');
                                $_gsb_year = (new static)->getStringBetween($k4,'YEAR(',')');
    
                                if ($k4 != 'qtylnu') {
                                    // print($v4.'-'.@(string)$params[$k4].'////');
                                    if (@$params['data'][$_gsb_month] || @$params['data'][$_gsb_year]) {
                                        $qtylnu = $v3->qtylnu+1;
                                        // $found = true;
                                        // break;
                                    } else if ((string)$v4 == @(string)$params['data'][$k4]) {
                                        $qtylnu = $v3->qtylnu+1;
                                        // $found = true;
                                        // break;
                                    } else {
                                        $found = false;
                                    }
                                }
                            }
                        } else {
                            $qtylnu = $v3->qtylnu+1;
                            $found = true;
                            break;
                        }

                        if ($found) {
                            break;
                        }
                    }
                }
                // dd($qtylnu);
                // die;
                // $fungsigroupby = explode(';', $v2->fungsigroupby);
                // // $query_nourut = "SELECT MAX(noid) AS noid FROM $tablemaster WHERE $fungsigroupby[2] = $v2->idkode GROUP BY ";
                // $query_nourut = "SELECT MAX(docreate) AS noid FROM $tablemaster ";
                // foreach ($fungsigroupby as $k3 => $v3) {
                //     if ($v3 != '') {
                //         if ($k3 == 0) {
                //             $query_nourut .= 'GROUP BY ';
                //         }
                //         if ($v3) {
                //             $query_nourut .= "$v3,";
                //         }
                //     }
                // }
                // $query_nourut = substr($query_nourut, 0, -1);
                // $query_nourut .= " LIMIT 1";
                // $result_nourut = DB::connection('mysql2')->select($query_nourut);
                // if ($result_nourut) {
                //     $nourut = substr($v2->kodeformat, 0, strlen($v2->kodeformat)-strlen($result_nourut[0]->noid)).$result_nourut[0]->noid;
                // } else {
                //     $nourut = 0;
                // }
                $nourut = '';
                $idx = 0;
                for ($i=strlen($qtylnu); $i<strlen($v2->kodeformat); $i ++) {
                    $nourut .= $v2->kodeformat[$idx];
                    $idx++;
                }
                $nourut .= $qtylnu;
                $formdefault .= $nourut;
            } else if ($v2->kodename == 'BULAN') {
                $month = date(explode('%', $v2->kodeformat)[1]);
                if ($month == 1) {
                    $m = 'I';
                } else if ($month == 2){
                    $m = 'II';
                } else if ($month == 3){
                    $m = 'III';
                } else if ($month == 4){
                    $m = 'IV';
                } else if ($month == 5){
                    $m = 'V';
                } else if ($month == 6){
                    $m = 'VI';
                } else if ($month == 7){
                    $m = 'VII';
                } else if ($month == 8){
                    $m = 'VIII';
                } else if ($month == 9){
                    $m = 'IX';
                } else if ($month == 10){
                    $m = 'X';
                } else if ($month == 11){
                    $m = 'XI';
                } else if ($month == 12){
                    $m = 'XII';
                }
                $formdefault .= str_replace('[BULAN]', $m, $v2->kodevalue);
            } else if ($v2->kodename == 'TAHUN') {
                $formdefault .= str_replace('[TAHUN]', date(explode('%', $v2->kodeformat)[1]), $v2->kodevalue);
            }
        }

        $resultnourut = DB::connection('mysql2')->select("SELECT MAX(nourut) AS nourut FROM $tablemaster");

        return [
            'kode' => $formdefault,
            // 'nourut' => count($resultnourut) == 0 ? 1 : $resultnourut[0]->nourut+1,
            'nourut' => $qtylnu,
        ];
    }

    public static function deleteSelectM($selecteds, $selectcheckboxmall, $datalog) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table']);
        if ((bool)$selectcheckboxmall) {
            $result->delete();
        } else {
            $result->whereIn('noid', $selecteds)->delete();
        }

        if ((bool)$selectcheckboxmall) {
            $resultdetail = DB::connection('mysql2')
                ->table((new static)->main['tabledetail'])
                ->delete();
        } else {
            foreach ($selecteds as $k => $v) {
                $checkresultdetail = (new static)->findData($v)['purchasedetail'];
                if (count($checkresultdetail) > 0) {
                    $resultdetail = DB::connection('mysql2')
                                    ->table((new static)->main['tabledetail'])
                                    ->where('idmaster', $v)
                                    ->delete();
                } else {
                    $resultdetail = true;
                }
            }
        }

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultdetail && $resultlog ? 1 : 0;
    }

    public static function deleteData($noid, $kodereff, $datalog) {
        // FIND PARAMETER PREV (null,exist)
        $find = DB::connection('mysql2')
                    ->table((new static)->main['table'].' AS t')
                    ->leftJoin((new static)->main['tabledetail'].' AS td', 't.noid', '=', 'td.idmaster')
                    ->select([
                        'td.noid',
                        'td.idtrancprev',
                        'td.idtrancprevd',
                    ])
                    ->where('t.noid', $noid)
                    ->get();

        // FIND DATA PREV (exist)
        $findprev = [];
        foreach ($find as $k => $v) {
            if (is_null($v->idtrancprev) && is_null($v->idtrancprevd)) {
                continue;
            }

            $findprev[] = DB::connection('mysql2')
                ->table((new static)->main['tableprev'].' AS t')
                ->leftJoin((new static)->main['tabledetailprev'].' AS td', 't.noid', '=', 'td.idmaster')
                ->select([
                    'td.noid',
                    'td.unitqty',
                ])
                ->where('t.noid', $v->idtrancprev)
                ->where('td.noid', $v->idtrancprevd)
                ->first();
        }

        // $dataprevtemp = [];
        // foreach ($findprev as $k => $v) {
        //     $dataprevtemp[$v->noid] = [
        //         'unitqty' => $v->unitqty
        //     ];
        // }

        // FIND DATA NOW
        $findnow = [];
        foreach ($find as $k => $v) {
            if (is_null($v->idtrancprev) && is_null($v->idtrancprevd)) {
                continue;
            }

            $findnowtemp = DB::connection('mysql2')
                ->table((new static)->main['tabledetail'])
                ->select([
                    'idtrancprev',
                    'idtrancprevd',
                    'idinventor',
                    'unitqty',
                ])
                ->where('noid', '!=', $v->noid)
                ->where('idtrancprev', $v->idtrancprev)
                ->where('idtrancprevd', $v->idtrancprevd)
                ->get();
            
            if (count($findnowtemp) != 0) {
                $findnow[] = $findnowtemp;
            }
        }

        $updateprev = [];
        if (count($findnow) == 0) {
            // if (!is_null($find[0]->idtrancprev) && !is_null($find[0]->idtrancprevd)) {
            //     $updateprev[] = DB::connection('mysql2')
            //         ->table((new static)->main['tabledetailprev'])
            //         ->where('noid', $find[0]->idtrancprevd)
            //         ->where('idmaster', $find[0]->idtrancprev)
            //         ->update([
            //             'unitqtysisa' => $findprev[0]->unitqty
            //         ]);
            // }
        } else {
            foreach ($findnow as $k => $v) {
                $idtrancprev = $v[0]->idtrancprev;
                $idtrancprevd = $v[0]->idtrancprevd;
                $dataprev = ['unitqtysisa' => 0];
                foreach ($v as $k2 => $v2) {
                    $dataprev['unitqtysisa'] += $v2->unitqty;
                }
                // UPDETE UNITQTYSISAPREV
                $updateprev[] = DB::connection('mysql2')
                    ->table((new static)->main['tabledetailprev'])
                    ->where('noid', $idtrancprevd)
                    ->where('idmaster', $idtrancprev)
                    ->update($dataprev);
                    // ->first();
            }
        }

        // UPDATE ISINVOICEDONE
        $updatepo = DB::connection('mysql2')
            ->table('purchaseorder')
            ->where('kode', $kodereff)
            ->update(['isinvoicedone' => 0]);


        $idtypetranc = (new static)->findData($noid)['purchase']->idtypetranc;
        $datalog['idtypetranc'] = $idtypetranc;
        
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->delete();

        $checkresultdetail = (new static)->findData($noid)['purchasedetail'];
        if (count($checkresultdetail) > 0) {
            $resultdetail = DB::connection('mysql2')
                            ->table((new static)->main['tabledetail'])
                            ->where('idmaster', $noid)
                            ->delete();
        } else {
            $resultdetail = true;
        }

        $resultlog = DB::connection('mysql2')
                        ->table('applog')
                        ->insert($datalog);

        return $result && $resultdetail && $resultlog ? 1 : 0;
    }

}
