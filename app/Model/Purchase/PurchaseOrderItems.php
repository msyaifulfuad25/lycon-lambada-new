<?php

namespace App\Model\Purchase;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
Use DB;
use Illuminate\Support\Facades\Session;

class PurchaseOrderItems extends Model {
    use Notifiable;

    protected $connection = 'mysql2';
    protected $table = 'purchaseorder';

    public $main = [
        'table' => 'purchaseorder',
        'tabledetail' => 'purchaseorderdetail',
        'tableprev' => 'purchaseoffer',
        'tabledetailprev' => 'purchaseofferdetail',
        'idtypetranc' => 503,
        'idtypetranc2' => 504,
        'table2' => 'purchasedelivery',
        'tabledetail2' => 'purchasedeliverydetail',
        'generatecode2' => 'generatecode/504/1',
    ];

    public static function getData($table,$select=['noid','nama'],$connection='mysql2') {
        $result = DB::connection($connection)
                    ->table($table)
                    ->select($select)
                    ->orderBy('noid', 'asc')
                    ->get();
        return $result;
    }

    public static function getDetailInv($params) {
        $querypo = DB::connection('mysql2')
                    ->table('invminventory AS imi')
                    ->leftJoin('invmsatuan AS ims', 'imi.idsatuan', '=', 'ims.noid')
                    ->select([
                        // 'tdp.noid AS noid',
                        // 'tdp.idmaster AS idmaster',
                        // 'tp.kode AS kodepo',
                        'imi.noid AS idinventor',
                        // 'tdp.idgudang AS idgudang',
                        // 'tdp.idcompany AS idcompany',
                        // 'tdp.iddepartment AS iddepartment',
                        'imi.idsatuan AS idsatuan',
                        'imi.konversi AS konvsatuan',
                        // 'tdp.subtotal AS subtotal',
                        // 'tdp.discountvar AS discountvar',
                        // 'tdp.discount AS discount',
                        // 'tdp.nilaidisc AS nilaidisc',
                        // 'tdp.idtypetax AS idtypetax',
                        // 'tdp.idtax AS idtax',
                        // 'tdp.prosentax AS prosentax',
                        // 'tdp.nilaitax AS nilaitax',
                        // 'tp.kode AS purchaseorder_kode',
                        'imi.kode AS kodeinventor',
                        'imi.nama AS namainventor',
                        // 'tdp.namainventor AS namainventor2',
                        // 'tdp.keterangan AS keterangan',
                        // 'tdp.unitqty AS unitqty',
                        // 'tdp.unitqtysisa AS unitqtysisa',
                        'ims.nama AS namasatuan',
                        'imi.purchaseprice AS unitprice',
                        // 'tdp.hargatotal AS hargatotal',
                        // 'amc.nama AS namacurrency',
                    ])
                    ->where('imi.noid', '!=', 0)
                    ->where('imi.isactive', '!=', 0);
                    // ->where('tp.idstatustranc', 3)
                    // ->where('tp.idcardsupplier', $params['idcardsupplier'])
                    // ->where('tdp.unitqtysisa', '>', 0);
        
        if ($params['searchprev']) {
            $searchprev = $params['searchprev'];
            $querypo->where(function($query) use ($searchprev) {
                $query->where('imi.kode', 'like', "%$searchprev%")
                    ->orWhere('imi.nama', 'like', "%$searchprev%");
            });
        }

        // $result->whereNotIn('tdp.noid', $detailprev)
        //         ->orderBy('tdp.noid', 'asc');
        
        // if ($params['length'] != null && $params['length'] != null) {
        //     $querypo->offset($params['start'])
        //             ->limit($params['length']);
        // }

        $resultprev = $querypo->get();

        $result = DB::connection('mysql2')
                    ->table((new static)->main['tabledetail'])
                    ->where('idtrancprev', $params['searchprev'])
                    ->get();

        $res = [];
        foreach ($result as $k => $v) {
            $res[$v->idtrancprevd] = @$res[$v->idtrancprevd] ? $res[$v->idtrancprevd]+$v->unitqty : $v->unitqty;
        }

        $response = [];
        foreach ($resultprev as $k => $v) {
            $response[$k] = $v;
            // if (array_key_exists($v->noid, $res)) {
            //     $response[$k]->unitqtysisa = $v->unitqtysisa-$res[$v->noid];
            // }
        }

        return $response;
    }
    
    public static function getDetailPrev($params) {
        $orderby = [
            'noid' => 'imi.noid',
            'kodeprev' => 'imi.noid',
            'idinventor' => 'imi.noid',
            'idsatuan' => 'imi.idsatuan',
            'konvsatuan' => 'imi.konversi',
            'kodeinventor' => 'imi.kode',
            'namainventor' => 'imi.nama',
            'namainventor2' => 'imi.noid',
            'keterangan' => 'imi.noid',
            'unitqty' => 'imi.noid',
            'unitqtysisa' => 'imi.noid',
            'invmgroup' => 'imi.invmgroup',
            'namasatuan' => 'ims.nama',
            'unitprice' => 'imi.purchaseprice',
            'hargatotal' => 'imi.noid',
            'namacurrency' => 'imi.noid',
        ];
        $querypo = DB::connection('mysql2')
                    ->table((new static)->main['tabledetailprev'].' AS tdp')
                    ->leftJoin('invminventory AS imi', 'tdp.idinventor', '=', 'imi.noid')
                    ->leftJoin('invmsatuan AS ims', 'tdp.idsatuan', '=', 'ims.noid')
                    ->leftJoin((new static)->main['tableprev'].' AS tp', 'tdp.idmaster', '=', 'tp.noid')
                    ->leftJoin('accmcurrency AS amc', 'tp.idcurrency', '=', 'amc.noid')
                    ->select([
                        'tdp.noid AS noid',
                        'tdp.idmaster AS idmaster',
                        'tp.kode AS kodeprev',
                        'tdp.idinventor AS idinventor',
                        'tdp.idgudang AS idgudang',
                        'tdp.idcompany AS idcompany',
                        'tdp.iddepartment AS iddepartment',
                        'tdp.idsatuan AS idsatuan',
                        'tdp.konvsatuan AS konvsatuan',
                        'tdp.subtotal AS subtotal',
                        'tdp.discountvar AS discountvar',
                        'tdp.discount AS discount',
                        'tdp.nilaidisc AS nilaidisc',
                        'tdp.idtypetax AS idtypetax',
                        'tdp.idtax AS idtax',
                        'tdp.prosentax AS prosentax',
                        'tdp.nilaitax AS nilaitax',
                        'tp.kode AS purchaseorder_kode',
                        'imi.kode AS kodeinventor',
                        'imi.nama AS namainventor',
                        'tdp.namainventor AS namainventor2',
                        'tdp.keterangan AS keterangan',
                        'tdp.unitqty AS unitqty',
                        'tdp.unitqtysisa AS unitqtysisa',
                        'ims.nama AS namasatuan',
                        'tdp.unitprice AS unitprice',
                        'tdp.hargatotal AS hargatotal',
                        'amc.nama AS namacurrency',
                        'tp.idstatustranc AS idstatustranc',
                    ])
                    ->where('tp.idstatustranc', 5023)
                    ->where('tp.idcardsupplier', $params['idcardsupplier'])
                    ->where('tdp.unitqtysisa', '>', 0);
        
        if ($params['searchprev']) {
            $searchprev = $params['searchprev'];
            $querypo->where(function($query) use ($searchprev) {
                $query->where('tp.kode', 'like', "%$searchprev%")
                    ->orWhere('imi.kode', 'like', "%$searchprev%")
                    ->orWhere('imi.nama', 'like', "%$searchprev%");
            });
        }

        if ($params['search']) {
            $search = $params['search'];
            $querypo->where(function($query) use ($search) {
                $query->where('tp.kode', 'like', "%$search%")
                    ->orWhere('imi.kode', 'like', "%$search%")
                    ->orWhere('imi.nama', 'like', "%$search%");
            });
        }

        // $result->whereNotIn('tdp.noid', $detailprev)
        //         ->orderBy('tdp.noid', 'asc');
        $querypo->orderBy($orderby[$params['ordercolumn']], $params['orderdir']);
        
        if ($params['length'] != null && $params['length'] != null) {
            $querypo->offset($params['start'])
                    ->limit($params['length']);
        }

        $resultprev = $querypo->get();

        $result = DB::connection('mysql2')
                    ->table((new static)->main['tabledetail'])
                    ->where('idtrancprev', $params['searchprev'])
                    ->get();

        $res = [];
        foreach ($result as $k => $v) {
            $res[$v->idtrancprevd] = @$res[$v->idtrancprevd] ? $res[$v->idtrancprevd]+$v->unitqty : $v->unitqty;
        }

        $response = [];
        foreach ($resultprev as $k => $v) {
            $response[$k] = $v;
            if (array_key_exists($v->noid, $res)) {
                $response[$k]->unitqtysisa = $v->unitqtysisa-$res[$v->noid];
            }
        }

        return $response;
    }

    public static function getAllNoid($table) {
        $result = DB::connection('mysql2')
                    ->table($table)
                    ->select('noid')
                    ->get();

        $response = [];
        foreach($result as $k => $v) {
            $response[] = $v->noid;
        }
        return $response;
    }

    public static function findMTypeTranc() {
        $result = DB::connection('mysql2')
                    ->table('mtypetranc')
                    ->select(['noid','kode','nama'])
                    ->where('noid', (new static)->main['idtypetranc'])
                    ->orderBy('noid', 'asc')
                    ->first();
        return $result;
    }

    public static function findMTypeTrancTypeStatus($idtypetranc) {
        $result = DB::connection('mysql2')
                    ->table('mtypetranctypestatus')
                    ->select(['noid','idtypetranc','kode','nama','classicon','classcolor'])
                    ->where('idtypetranc', $idtypetranc)
                    ->where('kode', 'DRF')
                    ->orderBy('nourut', 'asc')
                    ->first();
        return $result;
    }

    public static function getLastMonth() {
        $result = DB::connection('mysql2')->select("SELECT year(max(tanggal)) AS _year, month(max(tanggal)) AS _month FROM ".(new static)->main['table']." LIMIT 1");
        return $result ? $result[0] : (object)['_year'=>date('Y'),'_month'=>date('m')];
    }

    public static function getGenMCompany() {
        $result = DB::connection('mysql2')
                    ->table('genmdepartment AS gmd')
                    ->leftJoin('genmcompany AS gmc', 'gmd.idcompany', '=', 'gmc.noid')
                    ->select([
                        'gmd.noid AS genmdepartment_noid',
                        'gmd.kode AS genmdepartment_kode',
                        'gmd.nama AS genmdepartment_nama',
                        'gmc.noid AS genmcompany_noid',
                        'gmc.kode AS genmcompany_kode',
                        'gmc.nama AS genmcompany_nama',
                    ])
                    ->orderBy('gmc.nama', 'asc')
                    ->orderBy('gmd.nama', 'asc')
                    ->get();
        return $result;
    }

    public static function getInvMInventory() {
        $result = DB::connection('mysql2')
                    ->table('invminventory AS imi')
                    ->leftJoin('invmsatuan AS ims', 'imi.idsatuan', '=', 'ims.noid')
                    ->select([
                        'imi.noid AS noid',
                        'imi.kode AS kode',
                        'imi.nama AS nama',
                        'imi.idsatuan AS idsatuan',
                        'ims.nama AS namasatuan',
                        'imi.konversi AS konversi',
                        'imi.idtax AS idtax',
                        'imi.taxprocent AS taxprocent',
                        'imi.purchaseprice AS purchaseprice',
                        'imi.purchaseprice AS purchaseprice2'
                    ])
                    ->orderBy('imi.noid', 'asc')
                    ->get();
        return $result;
    }

    public static function getPurchase($start=null, $length=null, $search, $orderby, $filter=null, $datefilter, $datefilter2, $supplierfilter) {
        if ($search) {
            $result = DB::connection('mysql2')
                ->table((new static)->main['tabledetail'].' AS td')
                ->leftJoin((new static)->main['table'].' AS t', 'td.idmaster', '=', 't.noid')
                ->leftJoin('mtypetranctypestatus AS mttts', 't.idstatustranc', '=', 'mttts.noid')
                ->leftJoin('salesorder AS so', 't.idcostcontrol', '=', 'so.noid')
                ->leftJoin('invminventory AS imi', 'td.idinventor', '=', 'imi.noid')
                ->leftJoin('mcardsupplier AS mcs', 't.idcardsupplier', '=', 'mcs.noid')
                ->leftJoin('genmgudang AS gmg', 'td.idgudang', '=', 'gmg.noid')
                ->leftJoin('mtypepackage AS mtp', 'td.idtypepackage', '=', 'mtp.noid')
                ->leftJoin('invmsatuan AS ims', 'td.idsatuan', '=', 'ims.noid')
                ->leftJoin('accmpajak AS amp', 'td.idtax', '=', 'amp.noid')
                ->leftJoin((new static)->main['tableprev'].' AS tp', 'td.idtrancprev', '=', 'tp.noid')
                ->select([
                    'td.*',
                    't.tanggal AS tanggal',
                    'tp.kode AS idtrancprev_kode',
                    't.kode AS idmaster_kode',
                    'mttts.nama AS idstatustranc_nama',
                    'mttts.classcolor AS idstatustranc_classcolor',
                    'so.kode AS idcostcontrol_kode',
                    'mcs.nama AS idcardsupplier_nama',
                    'imi.nama AS idinventor_nama',
                    'gmg.nama AS idgudang_nama',
                    'mtp.nama AS idtypepackage_nama',
                    'ims.nama AS idsatuan_nama',
                    'amp.nama AS idtax_nama',
                ])
                // ->where('pr.idtypetranc', '=', (new static)->main['idtypetranc'])
                ->where(function($query) use($search) {
                    $query->orWhere('tp.kode', 'like', "%$search%")
                        ->orWhere('t.kode', 'like', "%$search%")
                        ->orWhere('mttts.nama', 'like', "%$search%")
                        ->orWhere('td.namainventor', 'like', "%$search%")
                        ->orWhere('imi.nama', 'like', "%$search%")
                        ->orWhere('td.keterangan', 'like', "%$search%")
                        ->orWhere('gmg.nama', 'like', "%$search%")
                        ->orWhere('mtp.nama', 'like', "%$search%")
                        ->orWhere('ims.nama', 'like', "%$search%")
                        ->orWhere('td.konvsatuan', 'like', "%$search%")
                        ->orWhere('td.unitqty', 'like', "%$search%")
                        ->orWhere('td.unitqtysisa', 'like', "%$search%")
                        ->orWhere('td.unitprice', 'like', "%$search%")
                        ->orWhere('td.subtotal', 'like', "%$search%")
                        ->orWhere('td.discountvar', 'like', "%$search%")
                        ->orWhere('td.discount', 'like', "%$search%")
                        ->orWhere('td.nilaidisc', 'like', "%$search%")
                        ->orWhere('amp.nama', 'like', "%$search%")
                        ->orWhere('td.prosentax', 'like', "%$search%")
                        ->orWhere('td.nilaitax', 'like', "%$search%")
                        ->orWhere('td.hargatotal', 'like', "%$search%");
                });
                // ->where('mttts.noid', '!=', '5026');
                                              
            if (Session::get('groupuser') != 0) {
                // $result->where('t.idcreate', '=', Session::get('noid'));
            }
                        
            $resultdatefilterfrom = DB::connection('mysql2')
                                        ->table((new static)->main['table'])
                                        ->select('tanggal');
            $resultdatefilterto = DB::connection('mysql2')
                                        ->table((new static)->main['table'])
                                        ->select('tanggal');

            if ($filter) {
                foreach ($filter as $k => $v) {
                    if ($v) {
                        $result->where('t.'.$k, '=', $v);
                    }
                }
            }

            if ($datefilter['code'] == 'a') {
                $resultf = $resultdatefilterfrom->orderBy('tanggal', 'asc')->first() ? date('M d, Y', strtotime($resultdatefilterfrom->orderBy('tanggal', 'asc')->first()->tanggal)) : date('M d, Y');
                $resultt = $resultdatefilterto->orderBy('tanggal', 'desc')->first() ? date('M d, Y', strtotime($resultdatefilterto->orderBy('tanggal', 'desc')->first()->tanggal)) : date('M d, Y');
            } else if ($datefilter['code'] == 't') {
                $result->whereDate('t.tanggal', date('Y-m-d'));
                $resultf = date('M d, Y');
                $resultt = date('M d, Y');
            } else if ($datefilter['code'] == 'y') {
                $result->whereDate('t.tanggal', date('Y-m-d', strtotime('-1 day')));
                $resultf = date('M d, Y', strtotime('-1 day'));
                $resultt = date('M d, Y', strtotime('-1 day'));
            } else if ($datefilter['code'] == 'l7d') {
                // $result->whereRaw('t.tanggal >= DATE(NOW()) - INTERVAL 7 DAY');
                $result->whereDate('t.tanggal', '<=', date('Y-m-d'));
                $result->whereDate('t.tanggal', '>=', date('Y-m-d', strtotime('-7 day')));
                $resultf = date('M d, Y', strtotime('-7 day'));
                $resultt = date('M d, Y');
            } else if ($datefilter['code'] == 'l30d') {
                $result->whereDate('t.tanggal', '<=', date('Y-m-d'));
                $result->whereDate('t.tanggal', '>=', date('Y-m-d', strtotime('-1 month')));
                $resultf = date('M d, Y', strtotime('-1 month'));
                $resultt = date('M d, Y');
            } else if ($datefilter['code'] == 'tm') {
                $result->whereYear('t.tanggal', date('Y', strtotime('this year')));
                $result->whereMonth('t.tanggal', date('m', strtotime('this month')));
                $resultf = date('M d, Y', strtotime('first day of this month'));
                $resultt = date('M d, Y', strtotime('last day of this month'));
            } else if ($datefilter['code'] == 'lm') {
                $lmm = date('m', strtotime('-1 months'));
                $lmy = $lmm == 12 ? date('Y', strtotime('-1 years')) : date('Y');
                $result->whereYear('t.tanggal', $lmy);
                $result->whereMonth('t.tanggal', $lmm);
                $resultf = date('M d, Y', strtotime('first day of previous month'));
                $resultt = date('M d, Y', strtotime('last day of previous month'));
            } else if ($datefilter['code'] == 'cr') {
                $result->whereDate('t.tanggal', '>=', date('Y-m-d', strtotime($datefilter['custom']['from'])));
                $result->whereDate('t.tanggal', '<=', date('Y-m-d', strtotime($datefilter['custom']['to'])));
                $resultf = date('M d, Y', strtotime($datefilter['custom']['from']));
                $resultt = date('M d, Y', strtotime($datefilter['custom']['to']));
            }

            if ($supplierfilter) {
                $result->where('t.idcardsupplier', $supplierfilter);
            }

            // $result->whereDate('t.tanggal', '>=', $datefilter2['from']);
            // $result->whereDate('t.tanggal', '<=', $datefilter2['to']);

            if ($orderby['column'] == 'noid') {
                $result->orderBy('t.docreate', 'desc');
            } else {
                if ($orderby['column'] == 'idtypetranc' || $orderby['column'] == 'idstatustranc') {
                    $result->orderBy('t.'.$orderby['column'], $orderby['sort']);
                } else {
                    $result->orderBy($orderby['column'], $orderby['sort']);
                }
            }

            // $result->orderBy(
            //     $orderby['column'] == 0 ? 't.noid' : $orderby['column'], 
            //     $orderby['sort']
            // );

            if ($start != null && $length != null) {
                $result->offset($start)
                        ->limit($length);
            }
        } else {
            $result = DB::connection('mysql2')
                ->table((new static)->main['tabledetail'].' AS td')
                ->leftJoin((new static)->main['table'].' AS t', 'td.idmaster', '=', 't.noid')
                ->leftJoin('mtypetranctypestatus AS mttts', 't.idstatustranc', '=', 'mttts.noid')
                ->leftJoin('salesorder AS so', 't.idcostcontrol', '=', 'so.noid')
                ->leftJoin('invminventory AS imi', 'td.idinventor', '=', 'imi.noid')
                ->leftJoin('mcardsupplier AS mcs', 't.idcardsupplier', '=', 'mcs.noid')
                ->leftJoin('genmgudang AS gmg', 'td.idgudang', '=', 'gmg.noid')
                ->leftJoin('mtypepackage AS mtp', 'td.idtypepackage', '=', 'mtp.noid')
                ->leftJoin('invmsatuan AS ims', 'td.idsatuan', '=', 'ims.noid')
                ->leftJoin('accmpajak AS amp', 'td.idtax', '=', 'amp.noid')
                ->leftJoin((new static)->main['tableprev'].' AS tp', 'td.idtrancprev', '=', 'tp.noid')
                ->select([
                    'td.*',
                    't.tanggal AS tanggal',
                    'tp.kode AS idtrancprev_kode',
                    't.kode AS idmaster_kode',
                    'mttts.nama AS idstatustranc_nama',
                    'mttts.classcolor AS idstatustranc_classcolor',
                    'so.kode AS idcostcontrol_kode',
                    'imi.nama AS idinventor_nama',
                    'mcs.nama AS idcardsupplier_nama',
                    'gmg.nama AS idgudang_nama',
                    'mtp.nama AS idtypepackage_nama',
                    'ims.nama AS idsatuan_nama',
                    'amp.nama AS idtax_nama',
                ]);
                // ->where('pr.idtypetranc', '=', (new static)->main['idtypetranc'])
                // ->where('mttts.noid', '!=', '5026');
                                              
            if (Session::get('groupuser') != 0) {
                // $result->where('pr.idcreate', '=', Session::get('noid'));
            }
            
            $resultdatefilterfrom = DB::connection('mysql2')
                                        ->table((new static)->main['table'])
                                        ->select('tanggal');
            $resultdatefilterto = DB::connection('mysql2')
                                        ->table((new static)->main['table'])
                                        ->select('tanggal');
            
            if ($filter) {
                foreach ($filter as $k => $v) {
                    if ($v) {
                        $result->where('t.'.$k, '=', $v);
                    }
                }
            }

            if ($datefilter['code'] == 'a') {
                $resultf = $resultdatefilterfrom->orderBy('tanggal', 'asc')->first() ? date('M d, Y', strtotime($resultdatefilterfrom->orderBy('tanggal', 'asc')->first()->tanggal)) : date('M d, Y');
                $resultt = $resultdatefilterto->orderBy('tanggal', 'desc')->first() ? date('M d, Y', strtotime($resultdatefilterto->orderBy('tanggal', 'desc')->first()->tanggal)) : date('M d, Y');
            } else if ($datefilter['code'] == 't') {
                $result->whereDate('t.tanggal', date('Y-m-d'));
                $resultf = date('M d, Y');
                $resultt = date('M d, Y');
            } else if ($datefilter['code'] == 'y') {
                $result->whereDate('t.tanggal', date('Y-m-d', strtotime('-1 day')));
                $resultf = date('M d, Y', strtotime('-1 day'));
                $resultt = date('M d, Y', strtotime('-1 day'));
            } else if ($datefilter['code'] == 'l7d') {
                // $result->whereRaw('t.tanggal >= DATE(NOW()) - INTERVAL 7 DAY');
                $result->whereDate('t.tanggal', '<=', date('Y-m-d'));
                $result->whereDate('t.tanggal', '>=', date('Y-m-d', strtotime('-7 day')));
                $resultf = date('M d, Y', strtotime('-7 day'));
                $resultt = date('M d, Y');
            } else if ($datefilter['code'] == 'l30d') {
                $result->whereDate('t.tanggal', '<=', date('Y-m-d'));
                $result->whereDate('t.tanggal', '>=', date('Y-m-d', strtotime('-1 month')));
                $resultf = date('M d, Y', strtotime('-1 month'));
                $resultt = date('M d, Y');
            } else if ($datefilter['code'] == 'tm') {
                $result->whereYear('t.tanggal', date('Y', strtotime('this year')));
                $result->whereMonth('t.tanggal', date('m', strtotime('this month')));
                $resultf = date('M d, Y', strtotime('first day of this month'));
                $resultt = date('M d, Y', strtotime('last day of this month'));
            } else if ($datefilter['code'] == 'lm') {
                $lmm = date('m', strtotime('-1 months'));
                $lmy = $lmm == 12 ? date('Y', strtotime('-1 years')) : date('Y');
                $result->whereYear('t.tanggal', $lmy);
                $result->whereMonth('t.tanggal', $lmm);
                $resultf = date('M d, Y', strtotime('first day of previous month'));
                $resultt = date('M d, Y', strtotime('last day of previous month'));
            } else if ($datefilter['code'] == 'cr') {
                $result->whereDate('t.tanggal', '>=', date('Y-m-d', strtotime($datefilter['custom']['from'])));
                $result->whereDate('t.tanggal', '<=', date('Y-m-d', strtotime($datefilter['custom']['to'])));
                $resultf = date('M d, Y', strtotime($datefilter['custom']['from']));
                $resultt = date('M d, Y', strtotime($datefilter['custom']['to']));
            }

            if ($supplierfilter) {
                $result->where('t.idcardsupplier', $supplierfilter);
            }

            // $result->whereDate('t.tanggal', '>=', $datefilter2['from']);
            // $result->whereDate('t.tanggal', '<=', $datefilter2['to']);

            if ($orderby['column'] == 'noid') {
                $result->orderBy('t.docreate', 'desc');
            } else {
                if ($orderby['column'] == 'idtypetranc' || $orderby['column'] == 'idstatustranc') {
                    $result->orderBy('t.'.$orderby['column'], $orderby['sort']);
                } else {
                    $result->orderBy($orderby['column'], $orderby['sort']);
                }
            }

            // $result->orderBy(
            //     $orderby['column'] == 0 ? 't.noid' : $orderby['column'], 
            //     $orderby['sort']
            // );
            
            if ($start != null && $length != null) {
                $result->offset($start)
                        ->limit($length);
            }
        }
        
        return [
            'datefilter' => [
                'type' => $datefilter['code'],
                'from' => $resultf,
                'to' => $resultt
            ],
            'data' => $result->get(),
        ];
    }

    public static function getLog($noid, $kode, $start, $length, $search, $orderby) {
        if ($search) {
            $result = DB::connection('mysql2')
                        ->table('applog AS al')
                        ->leftJoin('appmtypeaction AS amta', 'al.idtypeaction', '=', 'amta.noid')
                        ->leftJoin('mcarduser AS mcu', 'al.idcreate', '=', 'mcu.noid')
                        ->leftJoin((new static)->main['table'].' AS prq', 'al.idreff', '=', 'prq.noid')
                        ->select([
                            'al.noid AS noid',
                            'amta.nama AS idtypeaction',
                            'al.logsubject AS logsubject',
                            'al.keterangan AS keterangan',
                            'al.idreff AS idreff',
                            'prq.kode AS kode',
                            'mcu.myusername AS idcreate',
                            'al.docreate AS docreate'
                        ])
                        ->where('al.idreff', $noid)
                        ->where('al.idtypetranc', (new static)->main['idtypetranc'])
                        ->where('al.logsubject', 'like', "%$kode%")
                        ->where(function($query) use ($search) {
                            $query->where('al.noid', 'like', "%$search%");
                            $query->orWhere('amta.nama', 'like', "%$search%");
                            $query->orWhere('al.logsubject', 'like', "%$search%");
                            $query->orWhere('al.keterangan', 'like', "%$search%");
                            $query->orWhere('mcu.myusername', 'like', "%$search%");
                            $query->orWhere('al.docreate', 'like', "%$search%");
                        })
                        ->orderBy($orderby['column'], $orderby['sort'])
                        ->offset($start)
                        ->limit($length)
                        ->get();
        } else {
            $result = DB::connection('mysql2')
                        ->table('applog AS al')
                        ->leftJoin('mtypetranc AS mtt', 'al.idtypetranc', '=', 'mtt.noid')
                        ->leftJoin('appmtypeaction AS amta', 'al.idtypeaction', '=', 'amta.noid')
                        ->leftJoin('mcarduser AS mcu', 'al.idcreate', '=', 'mcu.noid')
                        ->leftJoin((new static)->main['table'].' AS prq', 'al.idreff', '=', 'prq.noid')
                        ->select([
                            'al.noid AS noid',
                            'amta.nama AS idtypeaction',
                            'al.logsubject AS logsubject',
                            'al.keterangan AS keterangan',
                            'al.idreff AS idreff',
                            'prq.kode AS kode',
                            'mcu.myusername AS idcreate',
                            'al.docreate AS docreate'
                        ])
                        ->where('al.idreff', $noid)
                        ->where('al.idtypetranc', (new static)->main['idtypetranc'])
                        ->where('al.logsubject', 'like', "%$kode%")
                        ->orderBy($orderby['column'], $orderby['sort'])
                        ->offset($start)
                        ->limit($length)
                        ->get();
        }
        return $result;
    }

    public static function getDefaultCostControl() {
        $result = DB::connection('mysql2')
                    ->table('salesorder')
                    ->where('noid', 0)
                    ->first();
        return $result;
    }
    
    public static function getCostControl($params) {
        $start = $params['start'];
        $length = $params['length'];
        $search = $params['search'];
        $orderby = $params['orderby'];
        $idtypecostcontrol = $params['idtypecostcontrol'];
        $getall = $params['getall'];

        $resultcc = DB::connection('mysql2')
                    ->table('purchaserequest')
                    ->select('idcostcontrol')
                    ->where('idcostcontrol','!=',0)
                    ->where('idstatustranc','!=',5016)
                    ->get()->toArray();

        $cc = [];
        // foreach ($resultcc as $k => $v) {
        //     !in_array($v->idcostcontrol, $cc) && $cc[] = $v->idcostcontrol;
        // }
        // dd($cc);
        if ($search) {
            $result = DB::connection('mysql2')
                        ->table('salesorder AS so')
                        ->leftJoin('mtypecostcontrol AS mtcc', 'so.idtypecostcontrol', '=', 'mtcc.noid')
                        ->leftJoin('mtypetranctypestatus AS mttts', 'so.idstatustranc', '=', 'mttts.noid')
                        ->leftJoin('mtypetranc AS mtt', 'so.idtypetranc', '=', 'mtt.noid')
                        ->leftJoin('genmcompany AS gmc', 'so.idcompany', '=', 'gmc.noid')
                        ->leftJoin('genmdepartment AS gmd', 'so.iddepartment', '=', 'gmd.noid')
                        ->leftJoin('mcarduser AS mcu', 'so.idcreate', '=', 'mcu.noid')
                        ->leftJoin('mcard AS mc1', 'so.idcardpj', '=', 'mc1.noid')
                        ->leftJoin('mcard AS mc2', 'so.idcardkoor', '=', 'mc2.noid')
                        ->leftJoin('mcard AS mc3', 'so.idcardsitekoor', '=', 'mc3.noid')
                        ->leftJoin('mcard AS mc4', 'so.idcarddrafter', '=', 'mc4.noid')
                        ->select([
                            'so.noid',
                            'mtt.nama AS mtt_nama',
                            'mttts.nama AS mttts_nama',
                            'so.kode',
                            'so.kodereff',
                            'so.tanggal',
                            'so.idtypecostcontrol',
                            'so.projectname',
                            'so.projectlocation',
                            'mtcc.kode AS mtcc_kode',
                            'mtcc.nama AS mtcc_nama',
                            'so.generaltotal',
                            'so.keterangan',
                            'mcu.myusername AS mcu_myusername',
                            'gmc.nama AS gmc_nama',
                            'gmd.nama AS gmd_nama',
                            'mc1.nama AS idcardpj_nama',
                            'mc2.nama AS idcardkoor_nama',
                            'mc3.nama AS idcardsitekoor_nama',
                            'mc4.nama AS idcarddrafter_nama'
                        ])
                        ->where('so.idtypecostcontrol',$idtypecostcontrol)
                        ->where(function($query) use ($search) {
                            $query->where('so.noid', 'like', "%$search%")
                                ->orWhere('mttts.nama', 'like', "%$search%")
                                ->orWhere('mtt.nama', 'like', "%$search%")
                                ->orWhere('so.kode', 'like', "%$search%")
                                ->orWhere('so.kodereff', 'like', "%$search%")
                                ->orWhere('gmc.nama', 'like', "%$search%")
                                ->orWhere('gmd.nama', 'like', "%$search%")
                                ->orWhere('so.generaltotal', 'like', "%$search%")
                                ->orWhere('so.keterangan', 'like', "%$search%")
                                ->orWhere('so.tanggal', 'like', "%$search%");
                        })
                        ->orderBy($orderby['column'], $orderby['sort']);
            if (!$getall) {
                $result->offset($start)->limit($length);
            }
            $result = $result->get();
        } else {
            $result = DB::connection('mysql2')
                        ->table('salesorder AS so')
                        ->leftJoin('mtypecostcontrol AS mtcc', 'so.idtypecostcontrol', '=', 'mtcc.noid')
                        ->leftJoin('mtypetranctypestatus AS mttts', 'so.idstatustranc', '=', 'mttts.noid')
                        ->leftJoin('mtypetranc AS mtt', 'so.idtypetranc', '=', 'mtt.noid')
                        ->leftJoin('genmcompany AS gmc', 'so.idcompany', '=', 'gmc.noid')
                        ->leftJoin('genmdepartment AS gmd', 'so.iddepartment', '=', 'gmd.noid')
                        ->leftJoin('mcarduser AS mcu', 'so.idcreate', '=', 'mcu.noid')
                        ->leftJoin('mcard AS mc1', 'so.idcardpj', '=', 'mc1.noid')
                        ->leftJoin('mcard AS mc2', 'so.idcardkoor', '=', 'mc2.noid')
                        ->leftJoin('mcard AS mc3', 'so.idcardsitekoor', '=', 'mc3.noid')
                        ->leftJoin('mcard AS mc4', 'so.idcarddrafter', '=', 'mc4.noid')
                        ->select([
                            'so.noid',
                            'mtt.nama AS mtt_nama',
                            'mttts.nama AS mttts_nama',
                            'so.kode',
                            'so.kodereff',
                            'so.tanggal',
                            'so.idtypecostcontrol',
                            'so.projectname',
                            'so.projectlocation',
                            'so.kode AS mtcc_kode',
                            'mtcc.nama AS mtcc_nama',
                            'so.generaltotal',
                            'so.keterangan',
                            'mcu.myusername AS mcu_myusername',
                            'gmc.nama AS gmc_nama',
                            'gmd.nama AS gmd_nama',
                            'mc1.nama AS idcardpj_nama',
                            'mc2.nama AS idcardkoor_nama',
                            'mc3.nama AS idcardsitekoor_nama',
                            'mc4.nama AS idcarddrafter_nama'
                        ])
                        ->whereNotIn('so.noid', $cc)
                        ->where('so.idtypecostcontrol',$idtypecostcontrol)
                        ->orderBy($orderby['column'], $orderby['sort']);
            if (!$getall) {
                $result->offset($start)->limit($length);
            }
            $result = $result->get();
        }
        return $result;
    }

    public static function getMCardSupplier() {
        $result = DB::connection('mysql2')
                    ->table('mcard')
                    ->leftJoin('mcardsupplier','mcard.noid','=','mcardsupplier.noid')
                    ->leftJoin('mlokasiprop','mcardsupplier.idlokasiprop','=','mlokasiprop.noid')
                    ->leftJoin('mlokasikab','mcardsupplier.idlokasikota','=','mlokasikab.noid')
                    ->leftJoin('mlokasikec','mcardsupplier.idlokasikec','=','mlokasikec.noid')
                    ->leftJoin('mlokasikel','mcardsupplier.idlokasikel','=','mlokasikel.noid')
                    ->select([
                        'mcardsupplier.noid AS mcardsupplier_noid',
                        'mcardsupplier.nama AS mcardsupplier_nama',
                        'mlokasiprop.noid AS mlokasiprop_noid',
                        'mlokasiprop.kode AS mlokasiprop_kode',
                        'mlokasiprop.nama AS mlokasiprop_nama',
                        'mlokasikab.noid AS mlokasikab_noid',
                        'mlokasikab.kode AS mlokasikab_kode',
                        'mlokasikab.nama AS mlokasikab_nama',
                        'mlokasikec.noid AS mlokasikec_noid',
                        'mlokasikec.kode AS mlokasikec_kode',
                        'mlokasikec.nama AS mlokasikec_nama',
                        'mlokasikel.noid AS mlokasikel_noid',
                        'mlokasikel.kode AS mlokasikel_kode',
                        'mlokasikel.nama AS mlokasikel_nama',
                    ])
                    ->where('mcard.isactive',1)
                    ->where('mcard.issupplier',1)
                    ->orderBy('mcardsupplier_noid', 'asc')
                    ->get();
        return $result;
    }

    public static function getMTypeTrancTypeStatus($idtypetranc) {
        $result = DB::connection('mysql2')
                    ->table('mtypetranctypestatus AS mttts')
                    ->leftJoin('appmtypeaction AS amta', 'mttts.noid', '=', 'amta.idstatustrancbase')
                    ->select([
                        'mttts.noid',
                        'mttts.idstatusbase',
                        'mttts.nourut',
                        'mttts.kode',
                        'mttts.nama',
                        'mttts.classicon',
                        'mttts.classcolor',
                        'mttts.isinternal',
                        'mttts.isexternal',
                        'mttts.actview',
                        'mttts.actedit',
                        'mttts.actdelete',
                        'mttts.actreportdetail',
                        'mttts.actreportmaster',
                        'mttts.actsetprevstatus',
                        'mttts.actsetnextstatus',
                        'mttts.actsetstatus',
                        'amta.noid AS appmtypeaction_noid',
                        'amta.nama AS appmtypeaction_nama'
                    ])
                    ->where('mttts.idtypetranc', $idtypetranc)
                    ->where('mttts.isactive', '>', 0)
                    ->orderBy('mttts.nourut', 'asc')
                    ->get();
        return $result;
    }

    public static function saveData($data, $datalog) {
        // $queryupdate = '';
        // foreach ($data[(new static)->main['tabledetail']] as $k => $v) {
        //     $queryupdate .= 'UPDATE '.(new static)->main['tabledetailprev'].' SET unitqtysisa='.$v['unitqtysisaprev'].' WHERE noid='.$v['idtrancprevd'].' AND idmaster='.$v['idtrancprev'].';';
        //     unset($data[(new static)->main['tabledetail']][$k]['unitqtysisaprev']);
        // }
        // $resultupdate = DB::connection('mysql2')
        //                     ->select($queryupdate);

        if (count($data[(new static)->main['tabledetail']]) > 0) {
            foreach ($data[(new static)->main['tabledetail']] as $k => $v) {
                $resultupdate[] = DB::connection('mysql2')
                                    ->table((new static)->main['tabledetailprev'])
                                    ->where('noid', $v['idtrancprevd'])
                                    ->where('idmaster', $v['idtrancprev'])
                                    ->update(['unitqtysisa' => $v['unitqtysisaprev'] < 1 ? 0 : $v['unitqtysisaprev']]);
                unset($data[(new static)->main['tabledetail']][$k]['kodeprev']);
                unset($data[(new static)->main['tabledetail']][$k]['unitqtysisaprev']);
            }
        }
        
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->insert($data[(new static)->main['table']]);
        
        $resultdetail = DB::connection('mysql2')
                            ->table((new static)->main['tabledetail'])
                            ->insert($data[(new static)->main['tabledetail']]);

        $resultlog = DB::connection('mysql2')
                        ->table('applog')
                        ->insert($datalog);

        return $result && $resultdetail && $resultlog && @$resultupdate ? 1 : 0;
    }
    
    public static function updateData($data, $noid, $datalog) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($data[(new static)->main['table']]);
        
        @$deletedetail = DB::connection('mysql2')
                    ->table((new static)->main['tabledetail'])
                    ->where('idmaster', $noid)
                    ->delete();

        $resultdetail = DB::connection('mysql2')
                            ->table((new static)->main['tabledetail'])
                            ->insert($data[(new static)->main['tabledetail']]);

        $resultlog = DB::connection('mysql2')
                        ->table('applog')
                        ->insert($datalog);

        return $result && $resultdetail && $resultlog ? 1 : 0;
    }

    public static function getNextNoid($table) {
        $result = DB::connection('mysql2')
                    ->table($table)
                    ->orderBy('noid', 'desc')
                    ->first();
        
        return $result ? $result->noid+1 : 1;
    }

    public static function getNextNourut($params) {
        $result = DB::connection('mysql2')
                    ->table($params['table'])
                    ->select('noid','nourut')
                    ->where('idcostcontrol',$params['where'])
                    ->orderBy('noid', 'desc')
                    ->first();
        
        return $result ? $result->nourut+1 : 1;
    }
    
    public static function getKode($table,$noid) {
        $result = DB::connection('mysql2')
                    ->table($table)
                    ->select('kode')
                    ->where('noid', $noid)
                    ->first();
        
        return $result ? $result->kode : '';
    }

    public static function getLastNoid($table) {
        $result = DB::connection('mysql2')
                    ->table($table)
                    ->orderBy('noid', 'desc')
                    ->first();
        
        return $result ? $result->noid : 1;
    }

    public static function findLog($noid, $kode) {
        $result = DB::connection('mysql2')
                    ->table('applog')
                    ->select('noid')
                    ->where('idreff', $noid)
                    ->where('logsubject', 'like', "%$kode%")
                    ->orderBy('noid', 'asc')
                    ->get();
        return $result;
    }

    public static function findData($noid) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'].' AS pr')
                    ->leftJoin('mtypetranc AS mtt', 'pr.idtypetranc', '=', 'mtt.noid')
                    ->leftJoin('mtypetranctypestatus AS mttts', 'pr.idstatustranc', '=', 'mttts.noid')
                    ->leftJoin('mcarduser AS mcu', 'pr.idstatuscard', '=', 'mcu.noid')
                    ->leftJoin('salesorder AS so', 'pr.idcostcontrol', '=', 'so.noid')
                    ->leftJoin('accmcurrency AS amc', 'pr.idcurrency', '=', 'amc.noid')
                    ->select([
                        'pr.*',
                        'mtt.kode AS kodetypetranc',
                        'mttts.nama AS mtypetranctypestatus_nama',
                        'mttts.classcolor AS mtypetranctypestatus_classcolor',
                        'mcu.myusername AS mcarduser_myusername',
                        'so.kode AS kodecostcontrol',
                        'amc.nama AS namacurrency',
                    ])
                    ->where('pr.noid', $noid)
                    ->first();

        $resultdetail = DB::connection('mysql2')
                        ->table((new static)->main['tabledetail'].' AS prd')
                        ->leftJoin((new static)->main['tableprev'].' AS pr', 'prd.idtrancprev', '=', 'pr.noid')
                        ->leftJoin('invminventory AS imi', 'prd.idinventor', '=', 'imi.noid')
                        ->leftJoin('invmsatuan AS ims', 'prd.idsatuan', '=', 'ims.noid')
                        ->select([
                            'prd.noid AS noid',
                            'pr.kode AS kodeprev',
                            'prd.idinventor AS idinventor',
                            'imi.kode AS kodeinventor',
                            'imi.nama AS namainventor',
                            'prd.namainventor AS namainventor2',
                            'prd.unitqty AS unitqty',
                            'prd.idcompany AS idcompany',
                            'prd.iddepartment AS iddepartment',
                            'prd.idgudang AS idgudang',
                            'prd.keterangan AS keterangan',
                            'prd.idsatuan AS idsatuan',
                            'ims.nama AS namasatuan',
                            'prd.konvsatuan AS konvsatuan',
                            'prd.unitqtysisa AS unitqtysisa',
                            'prd.unitprice AS unitprice',
                            'prd.subtotal AS subtotal',
                            'prd.discountvar AS discountvar',
                            'prd.discount AS discount',
                            'prd.nilaidisc AS nilaidisc',
                            'prd.idtypetax AS idtypetax',
                            'prd.idtax AS idtax',
                            'prd.prosentax AS prosentax',
                            'prd.nilaitax AS nilaitax',
                            'prd.hargatotal AS hargatotal',
                        ])
                        ->where('prd.idmaster', $noid)
                        ->orderBy('imi.kode', 'asc')
                        ->get();

        $data = [
            'purchase' => $result,
            'purchasedetail' => $resultdetail ? $resultdetail : [],
        ];

        return $data;
    }

    public static function findMaster($params) {
        $master = DB::connection('mysql2')
            ->table((new static)->main['table'].' AS t')
            ->leftJoin('salesorder AS so', 't.idcostcontrol', '=', 'so.noid')
            ->select([
                't.*',
                'so.kode AS idcostcontrol_kode'
            ])
            ->where('t.noid', $params['noid'])
            ->first();
        if (!$master) return ['success' => false, 'message' => 'Error Find Master'];

        return ['success' => true, 'message' => 'Success Find Master', 'data' => $master];
    }

    public static function findMasters($params) {
        // FIND NOID MASTERS
        $findnoidmasters = DB::connection('mysql2')
            ->table((new static)->main['tabledetail'])
            ->select(['idmaster'])
            ->whereIn('noid', $params['noids'])
            ->get();
        if (!$findnoidmasters) return ['success' => false, 'message' => 'Error Find Noid Masters'];
        $noidmasterstemp = [];
        foreach ($findnoidmasters as $k => $v) {
            $noidmasterstemp[] = $v->idmaster;
        }
        $noidmasters = array_unique($noidmasterstemp);
        
        // FIND MASTERS
        $findmasters = DB::connection('mysql2')
            ->table((new static)->main['table'].' AS t')
            ->leftJoin('salesorder AS so', 't.idcostcontrol', '=', 'so.noid')
            ->select([
                't.*',
                'so.kode AS idcostcontrol_kode'
            ])
            ->whereIn('t.noid', $noidmasters)
            ->get();
        if (!$findmasters) return ['success' => false, 'message' => 'Error Find Masters'];

        return ['success' => true, 'message' => 'Success Find Masters', 'data' => $findmasters];
    }

    public static function findDetail($params) {
        $detail = DB::connection('mysql2')
            ->table((new static)->main['tabledetail'])
            ->where('noid', $params['noid'])
            ->first();
        if (!$detail) return ['success' => false, 'message' => 'Error Find Detail'];

        return ['success' => true, 'message' => 'Success Find Detail', 'data' => $detail];
    }

    public static function findDetails($params) {
        $details = DB::connection('mysql2')
            ->table((new static)->main['tabledetail'])
            ->whereIn('noid', $params['noids'])
            ->get();
        if (!$details) return ['success' => false, 'message' => 'Error Find Details'];

        return ['success' => true, 'message' => 'Success Find Details', 'data' => $details];
    }
    
    public static function findLastSupplier() {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->select([
                        'noid',
                        'idcardsupplier',
                        'idakunsupplier',
                        'suppliernama',
                        'supplieraddress1',
                        'supplieraddress2',
                        'supplieraddressrt',
                        'supplieraddressrw',
                        'supplierlokasikel',
                        'supplierlokasikec',
                        'supplierlokasikab',
                        'supplierlokasiprop',
                        'supplierlokasineg',
                    ])
                    ->orderBy('noid', 'desc')
                    ->first();

        return $result;
    }

    public static function sendToNext($params) {
        $insertmaster = DB::connection('mysql2')
            ->table((new static)->main['table2'])
            ->insert($params['master']);
        if (!$insertmaster) return ['success' => false, 'message' => 'Error Insert Master'];

        $insertdetail = DB::connection('mysql2')
            ->table((new static)->main['tabledetail2'])
            ->insert($params['detail']);
        if (!$insertdetail) return ['success' => false, 'message' => 'Error Insert Detail'];

        return ['success' => true, 'message' => 'Success Send to Next'];
    }

    public static function sendToNexts($params) {
        foreach($params['masters'] as $k => $v) {
            $generatecode = (new static)->generateCode($v['generatecode_params']);
            $v['kode'] = $generatecode['kode'];
            $v['nourut'] = $generatecode['nourut'];
            unset($v['generatecode_params']);

            $insertmaster = DB::connection('mysql2')
                ->table((new static)->main['table2'])
                ->insert($v);
            if (!$insertmaster) return ['success' => false, 'message' => 'Error Insert Master'];
        }

        $insertdetails = DB::connection('mysql2')
            ->table((new static)->main['tabledetail2'])
            ->insert($params['details']);
        if (!$insertdetails) return ['success' => false, 'message' => 'Error Insert Details'];

        return ['success' => true, 'message' => 'Success Send to Nexts'];
    }

    public static function snst($data, $noid, $datalog) {
        if (!$noid) {
            $noid = (new static)->getLastNoid((new static)->main['table']);
            $datalog['idtypetranc'] = (new static)->main['idtypetranc'];
            $datalog['logsubject'] = $datalog['logsubject'].$noid;
            $datalog['keterangan'] = 'Save to Issued';
        }

        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($data);

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultlog ? 1 : 0;
    }

    public static function div($data, $noid, $datalog) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($data);

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultlog ? 1 : 0;
    }

    public static function sapcf($data, $noid, $datalog) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($data);

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultlog ? 1 : 0;
    }

    public static function setPending($data, $noid, $datalog) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($data);

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultlog ? 1 : 0;
    }

    public static function setCanceled($data, $noid, $datalog) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($data);

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultlog ? 1 : 0;
    }

    public static function getStringBetween($str,$from,$to) {
        $string = substr($str, strpos($str, $from) + strlen($from));
        if (strstr($string,$to,TRUE) != FALSE) {
            $string = strstr($string,$to,TRUE);
        } else {
            $string = FALSE;
        }
        return $string;
    }

    public static function generateCode($params) {
        $tablemaster = $params['tablemaster'];
        $costcontrol = (new static)->getDefaultCostControl();
        $idcostcontrol = @$params['idcostcontrol'] ? $params['idcostcontrol'] : $costcontrol->noid;
        $kodecostcontrol = @$params['kodecostcontrol'] ? $params['kodecostcontrol'] : $costcontrol->kode;
        $formdefault = explode('/', $params['generatecode']);
        
        $result_appmkode = DB::connection('mysql2')->table('appmkode')->where('noid', $formdefault[1])->first();
        $result_appmkodedetail = DB::connection('mysql2')->table('appmkodedetail')->where('idkode', $formdefault[1])->orderBy('nourut', 'ASC')->get();


        $formdefault = '';
        foreach ($result_appmkodedetail as $k2 => $v2) {
            if ($v2->kodename == 'KODE') {
                $formdefault .= $v2->kodevalue;
            } else if ($v2->kodename == 'FIELDTABLE') {
                $fungsigroupby = explode(';',$v2->fungsigroupby);
                $resultfieldtable = DB::connection('mysql2')->table($v2->kodeformat)->where($fungsigroupby[0], $params['data'][$fungsigroupby[1]])->first();
                $fieldname = $v2->fieldname;
                $formdefault .= str_replace('[FIELDTABLE]', $resultfieldtable->$fieldname, $v2->kodevalue);
            } else if ($v2->kodename == 'NOURUT') {
                $groupby = '';
                $fungsigroupby = explode(';', $v2->fungsigroupby);
                $select = '';
                $arrayselect = [];
                $where = 'WHERE 1=1 ';
                if ($v2->fungsigroupby != '' && $v2->fungsigroupby != null) {
                    $groupby .= 'GROUP BY ';
                    foreach ($fungsigroupby as $k3 => $v3) {
                        if ($v3) {
                            $groupby .= $v3.',';
                        }
                        $select .= $v3.',';
                        $arrayselect[] = $v3;

                        $_gsb_month = (new static)->getStringBetween($v3,'MONTH(',')');
                        $_gsb_year = (new static)->getStringBetween($v3,'YEAR(',')');

                        if (@$params['data'][$_gsb_month]) {
                            $where .= 'AND '.$v3.'='.date('m', strtotime($params['data'][$_gsb_month])).' ';
                        } else if (@$params['data'][$_gsb_year]) {
                            $where .= 'AND '.$v3.'='.date('Y', strtotime($params['data'][$_gsb_year])).' ';
                        } else {
                            $where .= 'AND '.$v3.'='.$params['data'][$v3].' ';
                        }
                    }
                    $groupby = substr($groupby, 0 ,-1);
                }
                $fieldname = $v2->fieldname;
                $fungsi = $v2->fungsi;
                // $qtylastnourut = "SELECT MAX(nourut) as qtylnu FROM $tablemaster GROUP BY $groupby";

                $qtylastnourut = "SELECT $select $fungsi($fieldname) as qtylnu FROM $tablemaster $where $groupby";
                $resultqtylnu = DB::connection('mysql2')->select($qtylastnourut);

                

                $qtylnu = 1;
                if (count($resultqtylnu) > 0) {
                    foreach($resultqtylnu as $k3 => $v3) {
                        $found = false; 

                        if (count((array)$v3) > 1) {
                            foreach($v3 as $k4 => $v4) {
                                $_gsb_month = (new static)->getStringBetween($k4,'MONTH(',')');
                                $_gsb_year = (new static)->getStringBetween($k4,'YEAR(',')');
    
                                if ($k4 != 'qtylnu') {
                                    // print($v4.'-'.@(string)$params[$k4].'////');
                                    if (@$params['data'][$_gsb_month] || @$params['data'][$_gsb_year]) {
                                        $qtylnu = $v3->qtylnu+1;
                                        // $found = true;
                                        // break;
                                    } else if ((string)$v4 == @(string)$params['data'][$k4]) {
                                        $qtylnu = $v3->qtylnu+1;
                                        // $found = true;
                                        // break;
                                    } else {
                                        $found = false;
                                    }
                                }
                            }
                        } else {
                            $qtylnu = $v3->qtylnu+1;
                            $found = true;
                            break;
                        }

                        if ($found) {
                            break;
                        }
                    }
                }
                // dd($qtylnu);
                // die;
                // $fungsigroupby = explode(';', $v2->fungsigroupby);
                // // $query_nourut = "SELECT MAX(noid) AS noid FROM $tablemaster WHERE $fungsigroupby[2] = $v2->idkode GROUP BY ";
                // $query_nourut = "SELECT MAX(docreate) AS noid FROM $tablemaster ";
                // foreach ($fungsigroupby as $k3 => $v3) {
                //     if ($v3 != '') {
                //         if ($k3 == 0) {
                //             $query_nourut .= 'GROUP BY ';
                //         }
                //         if ($v3) {
                //             $query_nourut .= "$v3,";
                //         }
                //     }
                // }
                // $query_nourut = substr($query_nourut, 0, -1);
                // $query_nourut .= " LIMIT 1";
                // $result_nourut = DB::connection('mysql2')->select($query_nourut);
                // if ($result_nourut) {
                //     $nourut = substr($v2->kodeformat, 0, strlen($v2->kodeformat)-strlen($result_nourut[0]->noid)).$result_nourut[0]->noid;
                // } else {
                //     $nourut = 0;
                // }
                $nourut = '';
                $idx = 0;
                for ($i=strlen($qtylnu); $i<strlen($v2->kodeformat); $i ++) {
                    $nourut .= $v2->kodeformat[$idx];
                    $idx++;
                }
                $nourut .= $qtylnu;
                $formdefault .= $nourut;
            } else if ($v2->kodename == 'BULAN') {
                $formdefault .= str_replace('[BULAN]', date(explode('%', $v2->kodeformat)[1]), $v2->kodevalue);
            } else if ($v2->kodename == 'TAHUN') {
                $formdefault .= str_replace('[TAHUN]', date(explode('%', $v2->kodeformat)[1]), $v2->kodevalue);
            }
        }

        $resultnourut = DB::connection('mysql2')->select("SELECT MAX(nourut) AS nourut FROM $tablemaster");

        return [
            'kode' => $formdefault,
            // 'nourut' => count($resultnourut) == 0 ? 1 : $resultnourut[0]->nourut+1,
            'nourut' => $qtylnu,
        ];
    }

    public static function deleteSelectM($selecteds, $selectcheckboxmall, $datalog) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table']);
        if ((bool)$selectcheckboxmall) {
            $result->delete();
        } else {
            $result->whereIn('noid', $selecteds)->delete();
        }

        if ((bool)$selectcheckboxmall) {
            $resultdetail = DB::connection('mysql2')
                ->table((new static)->main['tabledetail'])
                ->delete();
        } else {
            foreach ($selecteds as $k => $v) {
                $checkresultdetail = (new static)->findData($v)['purchasedetail'];
                if (count($checkresultdetail) > 0) {
                    $resultdetail = DB::connection('mysql2')
                                    ->table((new static)->main['tabledetail'])
                                    ->where('idmaster', $v)
                                    ->delete();
                } else {
                    $resultdetail = true;
                }
            }
        }

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultdetail && $resultlog ? 1 : 0;
    }

    public static function deleteData($noid, $datalog) {
        $idtypetranc = (new static)->findData($noid)['purchase']->idtypetranc;
        $datalog['idtypetranc'] = $idtypetranc;
        
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->delete();

        $checkresultdetail = (new static)->findData($noid)['purchasedetail'];
        if (count($checkresultdetail) > 0) {
            $resultdetail = DB::connection('mysql2')
                            ->table((new static)->main['tabledetail'])
                            ->where('idmaster', $noid)
                            ->delete();
        } else {
            $resultdetail = true;
        }

        $resultlog = DB::connection('mysql2')
                        ->table('applog')
                        ->insert($datalog);

        return $result && $resultdetail && $resultlog ? 1 : 0;
    }

}
