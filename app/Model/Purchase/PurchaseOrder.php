<?php

namespace App\Model\Purchase;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
Use DB;
use Illuminate\Support\Facades\Session;

class PurchaseOrder extends Model {
    use Notifiable;

    protected $connection = 'mysql2';
    protected $table = 'purchaseorder';

    public $main = [
        'noid' => 40021302,
        'noid2a' => 40021402,
        'noid2b' => 40021502,
        'table' => 'purchaseorder',
        'tabledetail' => 'purchaseorderdetail',
        'tableprev' => 'purchaseoffer',
        'tabledetailprev' => 'purchaseofferdetail',
        'idtypetranc' => 503,
        'idtypetranc2a' => 504,
        'idtypetranc2b' => 505,
        'table2a' => 'purchasedelivery',
        'table2b' => 'purchaseinvoice',
        'tabledetail2a' => 'purchasedeliverydetail',
        'tabledetail2b' => 'purchaseinvoicedetail',
        'generatecode2a' => 'generatecode/504/1',
        'generatecode2b' => 'generatecode/505/1',
        'defaultidstatustranc2a' => 5041,
        'defaultidstatustranc2b' => 5051,
        'purchasing' => [
            [
                'noid' => 5009,
                'kode' => 'L0916007'
            ],
            [
                'noid' => 5016,
                'kode' => 'L1709016'
            ],
        ]
    ];

    public static function isRule($params) {
        $mcardkode = $params['mcardkode'];
        $mpositionkode = $params['mpositionkode'];

        $resultmcardposition = DB::connection('mysql2')
            ->table('mcardposition AS mcp')
            ->select([
                '*'
            ])
            ->leftJoin('mposition AS mp', 'mp.noid', '=', 'mcp.idposition')
            ->where('mcp.kodecard', $mcardkode)
            ->where('mp.kode', $mpositionkode)
            ->first();

        if (!$resultmcardposition) return false;
        return true;
    }

    
    public static function getData($params) {
        $table = $params['table'];
        $select = @$params['select'] ? $params['select'] : ['noid','nama'];
        $connection = @$params['connection'] ? $params['connection'] : 'mysql2';
        $isactive = @$params['isactive'] ? $params['isactive'] : false;
        $where = @$params['where'];
        if ($isactive) $select[] = 'isactive';

        $result = DB::connection($connection)
                    ->table($table)
                    ->select($select)
                    ->where(function($query) use ($isactive) {
                        if ($isactive) $query->where('isactive', 1);
                    })
                    ->where(function($query) use ($where) {
                        if ($where) {
                            foreach ($where as $k => $v) {
                                $query->where($v[0], $v[1], $v[2]);
                            }
                        }
                    })
                    ->orderBy('noid', 'asc')
                    ->get();
                    
        return $result;
    }

    public static function getDetailInv($params) {
        $orderby = [
            'noid' => 'imi.noid',
            'kodeprev' => 'imi.noid',
            'idinventor' => 'imi.noid',
            'idsatuan' => 'imi.idsatuan',
            'konvsatuan' => 'imi.konversi',
            'kodeinventor' => 'imi.kode',
            'namainventor' => 'imi.nama',
            'namainventor2' => 'imi.noid',
            'keterangan' => 'imi.noid',
            'unitqty' => 'imi.noid',
            'unitqtysisa' => 'imi.noid',
            'invmgroup' => 'imi.invmgroup',
            'namasatuan' => 'ims.nama',
            'unitprice' => 'imi.purchaseprice',
            'hargatotal' => 'imi.noid',
            'namacurrency' => 'imi.noid',
        ];
        $querypo = DB::connection('mysql2')
                    ->table('invminventory AS imi')
                    ->leftJoin('invmsatuan AS ims', 'imi.idsatuan', '=', 'ims.noid')
                    ->select([
                        // 'tdp.noid AS noid',
                        // 'tdp.idmaster AS idmaster',
                        // 'tp.kode AS kodepo',
                        'imi.noid AS idinventor',
                        // 'tdp.idgudang AS idgudang',
                        // 'tdp.idcompany AS idcompany',
                        // 'tdp.iddepartment AS iddepartment',
                        'imi.idsatuan AS idsatuan',
                        'imi.konversi AS konvsatuan',
                        // 'tdp.subtotal AS subtotal',
                        // 'tdp.discountvar AS discountvar',
                        // 'tdp.discount AS discount',
                        // 'tdp.nilaidisc AS nilaidisc',
                        // 'tdp.idtypetax AS idtypetax',
                        // 'tdp.idtax AS idtax',
                        // 'tdp.prosentax AS prosentax',
                        // 'tdp.nilaitax AS nilaitax',
                        // 'tp.kode AS purchaseorder_kode',
                        'imi.kode AS kodeinventor',
                        'imi.nama AS namainventor',
                        // 'tdp.namainventor AS namainventor2',
                        // 'tdp.keterangan AS keterangan',
                        // 'tdp.unitqty AS unitqty',
                        // 'tdp.unitqtysisa AS unitqtysisa',
                        'ims.nama AS namasatuan',
                        'imi.purchaseprice AS unitprice',
                        // 'tdp.hargatotal AS hargatotal',
                        // 'amc.nama AS namacurrency',
                    ])
                    ->where('imi.noid', '!=', 0)
                    ->where('imi.isactive', '!=', 0);
                    // ->where('tp.idstatustranc', 3)
                    // ->where('tp.idcardsupplier', $params['idcardsupplier'])
                    // ->where('tdp.unitqtysisa', '>', 0);
        
        if ($params['searchprev']) {
            $searchprev = $params['searchprev'];
            $querypo->where(function($query) use ($searchprev) {
                $query->where('imi.kode', 'like', "%$searchprev%")
                    ->orWhere('imi.nama', 'like', "%$searchprev%");
            });
        }

        // $result->whereNotIn('tdp.noid', $detailprev)
        //         ->orderBy('tdp.noid', 'asc');
        $querypo->orderBy($orderby[$params['ordercolumn']], $params['orderdir']);
        
        if ($params['length'] != null && $params['length'] != null) {
            $querypo->offset($params['start'])
                    ->limit($params['length']);
        }

        $resultprev = $querypo->get();

        $result = DB::connection('mysql2')
                    ->table((new static)->main['tabledetail'])
                    ->where('idtrancprev', $params['searchprev'])
                    ->get();

        $res = [];
        foreach ($result as $k => $v) {
            $res[$v->idtrancprevd] = @$res[$v->idtrancprevd] ? $res[$v->idtrancprevd]+$v->unitqty : $v->unitqty;
        }

        $response = [];
        foreach ($resultprev as $k => $v) {
            $response[$k] = $v;
            // if (array_key_exists($v->noid, $res)) {
            //     $response[$k]->unitqtysisa = $v->unitqtysisa-$res[$v->noid];
            // }
        }

        return $response;
    }
    
    public static function getDetailPrev($params) {
        $orderby = [
            'noid' => 'imi.noid',
            'kodeprev' => 'imi.noid',
            'idinventor' => 'imi.noid',
            'idsatuan' => 'imi.idsatuan',
            'konvsatuan' => 'imi.konversi',
            'kodeinventor' => 'imi.kode',
            'namainventor' => 'imi.nama',
            'namainventor2' => 'imi.noid',
            'keterangan' => 'imi.noid',
            'unitqty' => 'imi.noid',
            'unitqtysisa' => 'imi.noid',
            'invmgroup' => 'imi.invmgroup',
            'namasatuan' => 'ims.nama',
            'unitprice' => 'imi.purchaseprice',
            'hargatotal' => 'imi.noid',
            'namacurrency' => 'imi.noid',
        ];
        $querypo = DB::connection('mysql2')
                    ->table((new static)->main['tabledetailprev'].' AS tdp')
                    ->leftJoin('invminventory AS imi', 'tdp.idinventor', '=', 'imi.noid')
                    ->leftJoin('invmsatuan AS ims', 'tdp.idsatuan', '=', 'ims.noid')
                    ->leftJoin((new static)->main['tableprev'].' AS tp', 'tdp.idmaster', '=', 'tp.noid')
                    ->leftJoin('accmcurrency AS amc', 'tp.idcurrency', '=', 'amc.noid')
                    ->select([
                        'tdp.noid AS noid',
                        'tdp.idmaster AS idmaster',
                        'tp.kode AS kodeprev',
                        'tdp.idinventor AS idinventor',
                        'tdp.idgudang AS idgudang',
                        'tdp.idcompany AS idcompany',
                        'tdp.iddepartment AS iddepartment',
                        'tdp.idsatuan AS idsatuan',
                        'tdp.konvsatuan AS konvsatuan',
                        'tdp.subtotal AS subtotal',
                        'tdp.discountvar AS discountvar',
                        'tdp.discount AS discount',
                        'tdp.nilaidisc AS nilaidisc',
                        'tdp.idtypetax AS idtypetax',
                        'tdp.idtax AS idtax',
                        'tdp.prosentax AS prosentax',
                        'tdp.nilaitax AS nilaitax',
                        'tp.kode AS purchaseorder_kode',
                        'imi.kode AS kodeinventor',
                        'imi.nama AS namainventor',
                        'tdp.namainventor AS namainventor2',
                        'tdp.keterangan AS keterangan',
                        'tdp.unitqty AS unitqty',
                        'tdp.unitqtysisa AS unitqtysisa',
                        'ims.nama AS namasatuan',
                        'tdp.unitprice AS unitprice',
                        'tdp.hargatotal AS hargatotal',
                        'amc.nama AS namacurrency',
                        'tp.idstatustranc AS idstatustranc',
                    ])
                    ->where('tp.idstatustranc', 5023)
                    // ->where('tp.idcardsupplier', $params['idcardsupplier'])
                    ->where('tdp.unitqtysisa', '>', 0);
        
        if ($params['searchprev']) {
            $searchprev = $params['searchprev'];
            $querypo->where(function($query) use ($searchprev) {
                $query->where('tp.kode', 'like', "%$searchprev%")
                    ->orWhere('imi.kode', 'like', "%$searchprev%")
                    ->orWhere('imi.nama', 'like', "%$searchprev%");
            });
        }

        if ($params['search']) {
            $search = $params['search'];
            $querypo->where(function($query) use ($search) {
                $query->where('tp.kode', 'like', "%$search%")
                    ->orWhere('imi.kode', 'like', "%$search%")
                    ->orWhere('imi.nama', 'like', "%$search%");
            });
        }

        // $result->whereNotIn('tdp.noid', $detailprev)
        //         ->orderBy('tdp.noid', 'asc');
        $querypo->orderBy($orderby[$params['ordercolumn']], $params['orderdir']);
        
        if ($params['length'] != null && $params['length'] != null) {
            $querypo->offset($params['start'])
                    ->limit($params['length']);
        }

        $resultprev = $querypo->get();

        $result = DB::connection('mysql2')
                    ->table((new static)->main['tabledetail'])
                    ->where('idtrancprev', $params['searchprev'])
                    ->get();

        $res = [];
        foreach ($result as $k => $v) {
            $res[$v->idtrancprevd] = @$res[$v->idtrancprevd] ? $res[$v->idtrancprevd]+$v->unitqty : $v->unitqty;
        }

        $response = [];
        foreach ($resultprev as $k => $v) {
            $response[$k] = $v;
            if (array_key_exists($v->noid, $res)) {
                $response[$k]->unitqtysisa = $v->unitqtysisa-$res[$v->noid];
            }
        }

        return $response;
    }

    public static function getAllNoid($table) {
        $result = DB::connection('mysql2')
                    ->table($table)
                    ->select('noid')
                    ->get();

        $response = [];
        foreach($result as $k => $v) {
            $response[] = $v->noid;
        }
        return $response;
    }

    public static function findMTypeTranc() {
        $result = DB::connection('mysql2')
                    ->table('mtypetranc')
                    ->select(['noid','kode','nama'])
                    ->where('noid', (new static)->main['idtypetranc'])
                    ->orderBy('noid', 'asc')
                    ->first();
        return $result;
    }

    public static function findMTypeTrancTypeStatus($idtypetranc) {
        $result = DB::connection('mysql2')
                    ->table('mtypetranctypestatus')
                    ->select(['noid','idtypetranc','kode','nama','classicon','classcolor'])
                    ->where('idtypetranc', $idtypetranc)
                    ->where('kode', 'DRF')
                    ->orderBy('nourut', 'asc')
                    ->first();
        return $result;
    }

    public static function getLastMonth() {
        $result = DB::connection('mysql2')->select("SELECT year(max(tanggal)) AS _year, month(max(tanggal)) AS _month FROM ".(new static)->main['table']." LIMIT 1");
        return $result ? $result[0] : (object)['_year'=>date('Y'),'_month'=>date('m')];
    }

    public static function getGenMCompany() {
        $result = DB::connection('mysql2')
                    ->table('genmdepartment AS gmd')
                    ->leftJoin('genmcompany AS gmc', 'gmd.idcompany', '=', 'gmc.noid')
                    ->select([
                        'gmd.noid AS genmdepartment_noid',
                        'gmd.kode AS genmdepartment_kode',
                        'gmd.nama AS genmdepartment_nama',
                        'gmc.noid AS genmcompany_noid',
                        'gmc.kode AS genmcompany_kode',
                        'gmc.nama AS genmcompany_nama',
                    ])
                    ->orderBy('gmc.nama', 'asc')
                    ->orderBy('gmd.nama', 'asc')
                    ->get();
        return $result;
    }

    public static function getInvMInventory() {
        $result = DB::connection('mysql2')
                    ->table('invminventory AS imi')
                    ->leftJoin('invmgroup AS img', 'img.noid', '=', 'imi.idinvmgroup')
                    ->leftJoin('invmkategori AS imk', 'imk.noid', '=', 'imi.idinvmkategori')
                    ->leftJoin('invmsatuan AS ims', 'imi.idsatuan', '=', 'ims.noid')
                    ->select([
                        'imi.noid AS noid',
                        'imi.kode AS kode',
                        'imi.nama AS nama',
                        'imi.idinvmgroup AS idinvmgroup',
                        'img.nama AS idinvmgroup_nama',
                        'imi.idinvmkategori AS idinvmkategori',
                        'imk.nama AS idinvmkategori_nama',
                        'imi.idsatuan AS idsatuan',
                        'ims.nama AS namasatuan',
                        'imi.konversi AS konversi',
                        'imi.idtax AS idtax',
                        'imi.taxprocent AS taxprocent',
                        'imi.purchaseprice AS purchaseprice',
                        'imi.purchaseprice AS purchaseprice2'
                    ])
                    ->where('imi.level', 3)
                    ->orderBy('imi.noid', 'asc')
                    ->get();
        return $result;
    }

    public static function getPurchase($start=null, $length=null, $search, $orderby, $filter=null, $datefilter, $datefilter2, $arrayis) {
        if ($search) {
            $result = DB::connection('mysql2')
                        ->table((new static)->main['table'].' AS por')
                        // ->leftJoin((new static)->main['table2'].' AS po', function($query) {
                        //     $query->on('po.kodereff','=','pr.kode')
                        //         ->whereRaw('po.noid IN (select MAX(po2.noid) from '.(new static)->main['table2'].' as po2 join '.(new static)->main['table'].' as pr2 on pr2.kode = po2.kodereff group by pr2.noid)');
                        // })
                        ->leftJoin('purchaseoffer AS pof', 'pof.kode', '=', 'por.kodereff')
                        ->leftJoin('purchaserequest AS pr', 'pr.kode', '=', 'pof.kodereff')
                        ->leftJoin('mtypetranctypestatus AS mttts', 'por.idstatustranc', '=', 'mttts.noid')
                        ->leftJoin('mtypetranc AS mtt', 'por.idtypetranc', '=', 'mtt.noid')
                        ->leftJoin('mcardsupplier AS mcs', 'por.idcardsupplier', '=', 'mcs.noid')
                        ->leftJoin('genmcompany AS gmc', 'por.idcompany', '=', 'gmc.noid')
                        ->leftJoin('genmdepartment AS gmd', 'por.iddepartment', '=', 'gmd.noid')
                        ->leftJoin('genmgudang AS gmg', 'por.idgudang', '=', 'gmg.noid')
                        ->leftJoin('salesorder AS so', 'por.idcostcontrol', '=', 'so.noid')
                        ->leftJoin('mcard AS mc1', 'so.idcardpj', '=', 'mc1.noid')
                        ->leftJoin('mcard AS mc2', 'so.idcardkoor', '=', 'mc2.noid')
                        ->leftJoin('mcard AS mc3', 'so.idcardsitekoor', '=', 'mc3.noid')
                        ->leftJoin('mcard AS mc4', 'so.idcarddrafter', '=', 'mc4.noid')
                        ->leftJoin('mcard AS mc5', 'por.idcardrequest', '=', 'mc5.noid')
                        ->leftJoin('mcard AS mc6', 'por.idcreate', '=', 'mc6.noid')
                        ->select([
                            'por.noid',
                            // 'po.noid AS purchaseorder_noid',
                            'mttts.noid AS mtypetranctypestatus_noid',
                            'mttts.idstatusbase AS mtypetranctypestatus_idstatusbase',
                            'mttts.nama AS mtypetranctypestatus_nama',
                            'mttts.classcolor AS mtypetranctypestatus_classcolor',
                            'mtt.noid AS mtypetranc_noid',
                            'mtt.nama AS mtypetranc_nama',
                            'por.kode',
                            'por.kodereff',
                            'pr.kode AS pr_kode',
                            'por.tanggal',
                            'por.tanggaldue',
                            'mcs.nama AS mcardsupplier_nama',
                            'gmc.nama AS idcompany',
                            'gmd.nama AS iddepartment',
                            'gmg.nama AS idgudang',
                            'por.idtypecostcontrol',
                            'por.idcostcontrol',
                            'so.kode AS kodecostcontrol',
                            'so.idcardpj AS so_idcardpj',
                            'so.idcardkoor AS so_idcardkoor',
                            'so.idcardsitekoor AS so_idcardsitekoor',
                            'so.idcarddrafter AS so_idcarddrafter',
                            'por.idcardrequest',
                            'por.totalsubtotal',
                            'por.generaltotal',
                            'por.keterangan',
                            'por.tanggal',
                            'mc1.nama AS idcardpj_nama',
                            'mc2.nama AS idcardkoor_nama',
                            'mc3.nama AS idcardsitekoor_nama',
                            'mc4.nama AS idcarddrafter_nama',
                            'mc5.nama AS idcardrequest_nama',
                            'mc6.nama AS idcreate_nama',
                            'por.idcreate'
                        ])
                        ->where('por.idtypetranc', '=', (new static)->main['idtypetranc'])
                        ->where(function($query) use($search) {
                            $query->orWhere('por.noid', 'like', "%$search%")
                                ->orWhere('mttts.nama', 'like', "%$search%")
                                ->orWhere('mtt.nama', 'like', "%$search%")
                                ->orWhere('por.kode', 'like', "%$search%")
                                // ->orWhere('por.kodereff', 'like', "%$search%")
                                ->orWhere('pr.kode', 'like', "%$search%")
                                ->orWhere('por.tanggal', 'like', "%$search%")
                                ->orWhere('por.tanggaldue', 'like', "%$search%")
                                ->orWhere('por.totalsubtotal', 'like', "%$search%")
                                ->orWhere('por.generaltotal', 'like', "%$search%")
                                ->orWhere('por.keterangan', 'like', "%$search%")
                                ->orWhere('so.kode', 'like', "%$search%")
                                ->orWhere('gmc.nama', 'like', "%$search%")
                                ->orWhere('gmd.nama', 'like', "%$search%")
                                ->orWhere('mc1.nama', 'like', "%$search%")
                                ->orWhere('mc2.nama', 'like', "%$search%")
                                ->orWhere('mc3.nama', 'like', "%$search%")
                                ->orWhere('mc4.nama', 'like', "%$search%")
                                ->orWhere('mc5.nama', 'like', "%$search%");
                        })
                        ->where(function($query) use ($arrayis) {
                            if (!$arrayis['isroot'] && !$arrayis['isdirector'] && !$arrayis['ispurchasing']) {
                                $query->orwhere('por.idcardrequest', '=', Session::get('noid'))
                                    ->orWhere('so.idcardkoor', '=', Session::get('noid'))
                                    ->orWhere('so.idcardpj', '=', Session::get('noid'))
                                    ->orWhere('so.idcardsitekoor', '=', Session::get('noid'))
                                    ->orWhere('so.idcarddrafter', '=', Session::get('noid'))
                                    ->orWhere('por.idcreate', '=', Session::get('noid'));
                            }
                        });
                                                         
            if (Session::get('groupuser') != 0) {
                // $result->where('por.idcreate', '=', Session::get('noid'));
            }
                        
            $resultdatefilterfrom = DB::connection('mysql2')
                                        ->table((new static)->main['table'])
                                        ->select('tanggal');
            $resultdatefilterto = DB::connection('mysql2')
                                        ->table((new static)->main['table'])
                                        ->select('tanggal');


            if ($filter) {
                foreach ($filter as $k => $v) {
                    if ($v) {
                        $result->where('por.'.$k, '=', $v);
                    }
                }
            }

            if ($datefilter['code'] == 'a') {
                $resultf = $resultdatefilterfrom->orderBy('tanggal', 'asc')->first() ? date('M d, Y', strtotime($resultdatefilterfrom->orderBy('tanggal', 'asc')->first()->tanggal)) : date('M d, Y');
                $resultt = $resultdatefilterto->orderBy('tanggal', 'desc')->first() ? date('M d, Y', strtotime($resultdatefilterto->orderBy('tanggal', 'desc')->first()->tanggal)) : date('M d, Y');
            } else if ($datefilter['code'] == 't') {
                $result->whereDate('por.tanggal', date('Y-m-d'));
                $resultf = date('M d, Y');
                $resultt = date('M d, Y');
            } else if ($datefilter['code'] == 'y') {
                $result->whereDate('por.tanggal', date('Y-m-d', strtotime('-1 day')));
                $resultf = date('M d, Y', strtotime('-1 day'));
                $resultt = date('M d, Y', strtotime('-1 day'));
            } else if ($datefilter['code'] == 'l7d') {
                // $result->whereRaw('por.tanggal >= DATE(NOW()) - INTERVAL 7 DAY');
                $result->whereDate('por.tanggal', '<=', date('Y-m-d'));
                $result->whereDate('por.tanggal', '>=', date('Y-m-d', strtotime('-7 day')));
                $resultf = date('M d, Y', strtotime('-7 day'));
                $resultt = date('M d, Y');
            } else if ($datefilter['code'] == 'l30d') {
                $result->whereDate('por.tanggal', '<=', date('Y-m-d'));
                $result->whereDate('por.tanggal', '>=', date('Y-m-d', strtotime('-1 month')));
                $resultf = date('M d, Y', strtotime('-1 month'));
                $resultt = date('M d, Y');
            } else if ($datefilter['code'] == 'tm') {
                $result->whereYear('por.tanggal', date('Y', strtotime('this year')));
                $result->whereMonth('por.tanggal', date('m', strtotime('this month')));
                $resultf = date('M d, Y', strtotime('first day of this month'));
                $resultt = date('M d, Y', strtotime('last day of this month'));
            } else if ($datefilter['code'] == 'lm') {
                $lmm = date('m', strtotime('-1 months'));
                $lmy = $lmm == 12 ? date('Y', strtotime('-1 years')) : date('Y');
                $result->whereYear('por.tanggal', $lmy);
                $result->whereMonth('por.tanggal', $lmm);
                $resultf = date('M d, Y', strtotime('first day of previous month'));
                $resultt = date('M d, Y', strtotime('last day of previous month'));
            } else if ($datefilter['code'] == 'cr') {
                $result->whereDate('por.tanggal', '>=', date('Y-m-d', strtotime($datefilter['custom']['from'])));
                $result->whereDate('por.tanggal', '<=', date('Y-m-d', strtotime($datefilter['custom']['to'])));
                $resultf = date('M d, Y', strtotime($datefilter['custom']['from']));
                $resultt = date('M d, Y', strtotime($datefilter['custom']['to']));
            }

            // $result->whereDate('por.tanggal', '>=', $datefilter2['from']);
            // $result->whereDate('por.tanggal', '<=', $datefilter2['to']);

            if ($orderby['column'] == 'noid') {
                $result->orderBy('por.docreate', 'desc');
            } else {
                if ($orderby['column'] == 'idtypetranc' || $orderby['column'] == 'idstatustranc') {
                    $result->orderBy('por.'.$orderby['column'], $orderby['sort']);
                } else {
                    $result->orderBy($orderby['column'], $orderby['sort']);
                }
            }

            if ($start != null && $length != null) {
                $result->offset($start)
                        ->limit($length);
            }
        } else {
            $result = DB::connection('mysql2')
                        ->table((new static)->main['table'].' AS por')
                        // ->leftJoin((new static)->main['table2'].' AS po', function($query) {
                        //     $query->on('po.kodereff','=','pr.kode')
                        //         ->whereRaw('po.noid IN (select MAX(po2.noid) from '.(new static)->main['table2'].' as po2 join '.(new static)->main['table'].' as pr2 on pr2.kode = po2.kodereff group by pr2.noid)');
                        // })
                        ->leftJoin('purchaseoffer AS pof', 'pof.kode', '=', 'por.kodereff')
                        ->leftJoin('purchaserequest AS pr', 'pr.kode', '=', 'pof.kodereff')
                        ->leftJoin('mtypetranctypestatus AS mttts', 'por.idstatustranc', '=', 'mttts.noid')
                        ->leftJoin('mtypetranc AS mtt', 'por.idtypetranc', '=', 'mtt.noid')
                        ->leftJoin('mcardsupplier AS mcs', 'por.idcardsupplier', '=', 'mcs.noid')
                        ->leftJoin('genmcompany AS gmc', 'por.idcompany', '=', 'gmc.noid')
                        ->leftJoin('genmdepartment AS gmd', 'por.iddepartment', '=', 'gmd.noid')
                        ->leftJoin('genmgudang AS gmg', 'por.idgudang', '=', 'gmg.noid')
                        ->leftJoin('salesorder AS so', 'por.idcostcontrol', '=', 'so.noid')
                        ->leftJoin('mcard AS mc1', 'so.idcardpj', '=', 'mc1.noid')
                        ->leftJoin('mcard AS mc2', 'so.idcardkoor', '=', 'mc2.noid')
                        ->leftJoin('mcard AS mc3', 'so.idcardsitekoor', '=', 'mc3.noid')
                        ->leftJoin('mcard AS mc4', 'so.idcarddrafter', '=', 'mc4.noid')
                        ->leftJoin('mcard AS mc5', 'por.idcardrequest', '=', 'mc5.noid')
                        ->leftJoin('mcard AS mc6', 'por.idcreate', '=', 'mc6.noid')
                        ->select([
                            'por.noid',
                            // 'po.noid AS purchaseorder_noid',
                            'mttts.noid AS mtypetranctypestatus_noid',
                            'mttts.idstatusbase AS mtypetranctypestatus_idstatusbase',
                            'mttts.nama AS mtypetranctypestatus_nama',
                            'mttts.classcolor AS mtypetranctypestatus_classcolor',
                            'mtt.noid AS mtypetranc_noid',
                            'mtt.nama AS mtypetranc_nama',
                            'por.kode',
                            'por.kodereff',
                            'pr.kode AS pr_kode',
                            'por.tanggal',
                            'por.tanggaldue',
                            'mcs.nama AS mcardsupplier_nama',
                            'gmc.nama AS idcompany',
                            'gmd.nama AS iddepartment',
                            'gmg.nama AS idgudang',
                            'so.idcardpj AS so_idcardpj',
                            'so.idcardpj AS so_idcardpj',
                            'so.idcardkoor AS so_idcardkoor',
                            'so.idcardsitekoor AS so_idcardsitekoor',
                            'so.idcarddrafter AS so_idcarddrafter',
                            'por.idtypecostcontrol',
                            'por.idcostcontrol',
                            'so.kode AS kodecostcontrol',
                            'por.idcardrequest',
                            'por.totalsubtotal',
                            'por.generaltotal',
                            'por.keterangan',
                            'por.tanggal',
                            'mc1.nama AS idcardpj_nama',
                            'mc2.nama AS idcardkoor_nama',
                            'mc3.nama AS idcardsitekoor_nama',
                            'mc4.nama AS idcarddrafter_nama',
                            'mc5.nama AS idcardrequest_nama',
                            'mc6.nama AS idcreate_nama',
                            'por.idcreate'
                        ])
                        ->where('por.idtypetranc', '=', (new static)->main['idtypetranc'])
                        ->where(function($query) use ($arrayis) {
                            if (!$arrayis['isroot'] && !$arrayis['isdirector'] && !$arrayis['ispurchasing']) {
                                $query->orwhere('por.idcardrequest', '=', Session::get('noid'))
                                    ->orWhere('so.idcardkoor', '=', Session::get('noid'))
                                    ->orWhere('so.idcardpj', '=', Session::get('noid'))
                                    ->orWhere('so.idcardsitekoor', '=', Session::get('noid'))
                                    ->orWhere('so.idcarddrafter', '=', Session::get('noid'))
                                    ->orWhere('por.idcreate', '=', Session::get('noid'));
                            }
                        });
                                                         
            if (Session::get('groupuser') != 0) {
                // $result->where('por.idcreate', '=', Session::get('noid'));
            }
                        
            $resultdatefilterfrom = DB::connection('mysql2')
                                        ->table((new static)->main['table'])
                                        ->select('tanggal');
            $resultdatefilterto = DB::connection('mysql2')
                                        ->table((new static)->main['table'])
                                        ->select('tanggal');
            
            if ($filter) {
                foreach ($filter as $k => $v) {
                    if ($v) {
                        $result->where('por.'.$k, '=', $v);
                    }
                }
            }

            if ($datefilter['code'] == 'a') {
                $resultf = $resultdatefilterfrom->orderBy('tanggal', 'asc')->first() ? date('M d, Y', strtotime($resultdatefilterfrom->orderBy('tanggal', 'asc')->first()->tanggal)) : date('M d, Y');
                $resultt = $resultdatefilterto->orderBy('tanggal', 'desc')->first() ? date('M d, Y', strtotime($resultdatefilterto->orderBy('tanggal', 'desc')->first()->tanggal)) : date('M d, Y');
            } else if ($datefilter['code'] == 't') {
                $result->whereDate('por.tanggal', date('Y-m-d'));
                $resultf = date('M d, Y');
                $resultt = date('M d, Y');
            } else if ($datefilter['code'] == 'y') {
                $result->whereDate('por.tanggal', date('Y-m-d', strtotime('-1 day')));
                $resultf = date('M d, Y', strtotime('-1 day'));
                $resultt = date('M d, Y', strtotime('-1 day'));
            } else if ($datefilter['code'] == 'l7d') {
                // $result->whereRaw('por.tanggal >= DATE(NOW()) - INTERVAL 7 DAY');
                $result->whereDate('por.tanggal', '<=', date('Y-m-d'));
                $result->whereDate('por.tanggal', '>=', date('Y-m-d', strtotime('-7 day')));
                $resultf = date('M d, Y', strtotime('-7 day'));
                $resultt = date('M d, Y');
            } else if ($datefilter['code'] == 'l30d') {
                $result->whereDate('por.tanggal', '<=', date('Y-m-d'));
                $result->whereDate('por.tanggal', '>=', date('Y-m-d', strtotime('-1 month')));
                $resultf = date('M d, Y', strtotime('-1 month'));
                $resultt = date('M d, Y');
            } else if ($datefilter['code'] == 'tm') {
                $result->whereYear('por.tanggal', date('Y', strtotime('this year')));
                $result->whereMonth('por.tanggal', date('m', strtotime('this month')));
                $resultf = date('M d, Y', strtotime('first day of this month'));
                $resultt = date('M d, Y', strtotime('last day of this month'));
            } else if ($datefilter['code'] == 'lm') {
                $lmm = date('m', strtotime('-1 months'));
                $lmy = $lmm == 12 ? date('Y', strtotime('-1 years')) : date('Y');
                $result->whereYear('por.tanggal', $lmy);
                $result->whereMonth('por.tanggal', $lmm);
                $resultf = date('M d, Y', strtotime('first day of previous month'));
                $resultt = date('M d, Y', strtotime('last day of previous month'));
            } else if ($datefilter['code'] == 'cr') {
                $result->whereDate('por.tanggal', '>=', date('Y-m-d', strtotime($datefilter['custom']['from'])));
                $result->whereDate('por.tanggal', '<=', date('Y-m-d', strtotime($datefilter['custom']['to'])));
                $resultf = date('M d, Y', strtotime($datefilter['custom']['from']));
                $resultt = date('M d, Y', strtotime($datefilter['custom']['to']));
            }

            // $result->whereDate('por.tanggal', '>=', $datefilter2['from']);
            // $result->whereDate('por.tanggal', '<=', $datefilter2['to']);

            if ($orderby['column'] == 'noid') {
                $result->orderBy('por.docreate', 'desc');
            } else {
                if ($orderby['column'] == 'idtypetranc' || $orderby['column'] == 'idstatustranc') {
                    $result->orderBy('por.'.$orderby['column'], $orderby['sort']);
                } else {
                    $result->orderBy($orderby['column'], $orderby['sort']);
                }
            }
            
            if ($start != null && $length != null) {
                $result->offset($start)
                        ->limit($length);
            }
        }
        
        $data = $result->get();
        
        $wipod = [];
        foreach ($data as $k => $v) {
            $wipod[] = $v->kode;
        }
        
        $resultpod = DB::connection('mysql2')
                        ->table('purchaseoffer')
                        ->whereIn('kodereff',$wipod)
                        ->get();

        $pod = [];
        foreach ($resultpod as $k => $v) {
            $pod[$v->kodereff] = $v;
        }        
        
        return [
            'datefilter' => [
                'type' => $datefilter['code'],
                'from' => $resultf,
                'to' => $resultt
            ],
            'data' => $data,
            'pod' => $pod
        ];
    }

    public static function getLog($noid, $kode, $start, $length, $search, $orderby) {
        if ($search) {
            $result = DB::connection('mysql2')
                        ->table('applog AS al')
                        ->leftJoin('appmtypeaction AS amta', 'al.idtypeaction', '=', 'amta.noid')
                        ->leftJoin('mcarduser AS mcu', 'al.idcreate', '=', 'mcu.noid')
                        ->leftJoin((new static)->main['table'].' AS prq', 'al.idreff', '=', 'prq.noid')
                        ->select([
                            'al.noid AS noid',
                            'amta.nama AS idtypeaction',
                            'al.logsubject AS logsubject',
                            'al.keterangan AS keterangan',
                            'al.idreff AS idreff',
                            'prq.kode AS kode',
                            'mcu.myusername AS idcreate',
                            'al.docreate AS docreate'
                        ])
                        ->where('al.idreff', $noid)
                        ->where('al.idtypetranc', (new static)->main['idtypetranc'])
                        ->where('al.logsubject', 'like', "%$kode%")
                        ->where(function($query) use ($search) {
                            $query->where('al.noid', 'like', "%$search%");
                            $query->orWhere('amta.nama', 'like', "%$search%");
                            $query->orWhere('al.logsubject', 'like', "%$search%");
                            $query->orWhere('al.keterangan', 'like', "%$search%");
                            $query->orWhere('mcu.myusername', 'like', "%$search%");
                            $query->orWhere('al.docreate', 'like', "%$search%");
                        })
                        ->orderBy($orderby['column'], $orderby['sort'])
                        ->offset($start)
                        ->limit($length)
                        ->get();
        } else {
            $result = DB::connection('mysql2')
                        ->table('applog AS al')
                        ->leftJoin('mtypetranc AS mtt', 'al.idtypetranc', '=', 'mtt.noid')
                        ->leftJoin('appmtypeaction AS amta', 'al.idtypeaction', '=', 'amta.noid')
                        ->leftJoin('mcarduser AS mcu', 'al.idcreate', '=', 'mcu.noid')
                        ->leftJoin((new static)->main['table'].' AS prq', 'al.idreff', '=', 'prq.noid')
                        ->select([
                            'al.noid AS noid',
                            'amta.nama AS idtypeaction',
                            'al.logsubject AS logsubject',
                            'al.keterangan AS keterangan',
                            'al.idreff AS idreff',
                            'prq.kode AS kode',
                            'mcu.myusername AS idcreate',
                            'al.docreate AS docreate'
                        ])
                        ->where('al.idreff', $noid)
                        ->where('al.idtypetranc', (new static)->main['idtypetranc'])
                        ->where('al.logsubject', 'like', "%$kode%")
                        ->orderBy($orderby['column'], $orderby['sort'])
                        ->offset($start)
                        ->limit($length)
                        ->get();
        }
        return $result;
    }

    public static function getDefaultCostControl() {
        $result = DB::connection('mysql2')
                    ->table('salesorder')
                    ->where('noid', 0)
                    ->first();
        return $result;
    }
    
    public static function getCostControl($params) {
        $start = $params['start'];
        $length = $params['length'];
        $search = $params['search'];
        $orderby = $params['orderby'];
        $idtypecostcontrol = $params['idtypecostcontrol'];
        $getall = $params['getall'];

        $resultcc = DB::connection('mysql2')
                    ->table('purchaserequest')
                    ->select('idcostcontrol')
                    ->where('idcostcontrol','!=',0)
                    ->where('idstatustranc','!=',5016)
                    ->get()->toArray();

        $cc = [];
        // foreach ($resultcc as $k => $v) {
        //     !in_array($v->idcostcontrol, $cc) && $cc[] = $v->idcostcontrol;
        // }
        // dd($cc);
        if ($search) {
            $result = DB::connection('mysql2')
                        ->table('salesorder AS so')
                        ->leftJoin('mtypecostcontrol AS mtcc', 'so.idtypecostcontrol', '=', 'mtcc.noid')
                        ->leftJoin('mtypetranctypestatus AS mttts', 'so.idstatustranc', '=', 'mttts.noid')
                        ->leftJoin('mtypetranc AS mtt', 'so.idtypetranc', '=', 'mtt.noid')
                        ->leftJoin('genmcompany AS gmc', 'so.idcompany', '=', 'gmc.noid')
                        ->leftJoin('genmdepartment AS gmd', 'so.iddepartment', '=', 'gmd.noid')
                        ->leftJoin('mcarduser AS mcu', 'so.idcreate', '=', 'mcu.noid')
                        ->leftJoin('mcard AS mc1', 'so.idcardpj', '=', 'mc1.noid')
                        ->leftJoin('mcard AS mc2', 'so.idcardkoor', '=', 'mc2.noid')
                        ->leftJoin('mcard AS mc3', 'so.idcardsitekoor', '=', 'mc3.noid')
                        ->leftJoin('mcard AS mc4', 'so.idcarddrafter', '=', 'mc4.noid')
                        ->select([
                            'so.noid',
                            'mtt.nama AS mtt_nama',
                            'mttts.nama AS mttts_nama',
                            'so.kode',
                            'so.kodereff',
                            'so.tanggal',
                            'so.idtypecostcontrol',
                            'so.projectname',
                            'so.projectlocation',
                            'mtcc.kode AS mtcc_kode',
                            'mtcc.nama AS mtcc_nama',
                            'so.generaltotal',
                            'so.keterangan',
                            'mcu.myusername AS mcu_myusername',
                            'gmc.nama AS gmc_nama',
                            'gmd.nama AS gmd_nama',
                            'mc1.nama AS idcardpj_nama',
                            'mc2.nama AS idcardkoor_nama',
                            'mc3.nama AS idcardsitekoor_nama',
                            'mc4.nama AS idcarddrafter_nama'
                        ])
                        ->where('so.idtypecostcontrol',$idtypecostcontrol)
                        ->where(function($query) use ($search) {
                            $query->where('so.noid', 'like', "%$search%")
                                ->orWhere('mttts.nama', 'like', "%$search%")
                                ->orWhere('mtt.nama', 'like', "%$search%")
                                ->orWhere('so.kode', 'like', "%$search%")
                                ->orWhere('so.kodereff', 'like', "%$search%")
                                ->orWhere('gmc.nama', 'like', "%$search%")
                                ->orWhere('gmd.nama', 'like', "%$search%")
                                ->orWhere('so.generaltotal', 'like', "%$search%")
                                ->orWhere('so.keterangan', 'like', "%$search%")
                                ->orWhere('so.tanggal', 'like', "%$search%");
                        })
                        ->orderBy($orderby['column'], $orderby['sort']);
            if (!$getall) {
                $result->offset($start)->limit($length);
            }
            $result = $result->get();
        } else {
            $result = DB::connection('mysql2')
                        ->table('salesorder AS so')
                        ->leftJoin('mtypecostcontrol AS mtcc', 'so.idtypecostcontrol', '=', 'mtcc.noid')
                        ->leftJoin('mtypetranctypestatus AS mttts', 'so.idstatustranc', '=', 'mttts.noid')
                        ->leftJoin('mtypetranc AS mtt', 'so.idtypetranc', '=', 'mtt.noid')
                        ->leftJoin('genmcompany AS gmc', 'so.idcompany', '=', 'gmc.noid')
                        ->leftJoin('genmdepartment AS gmd', 'so.iddepartment', '=', 'gmd.noid')
                        ->leftJoin('mcarduser AS mcu', 'so.idcreate', '=', 'mcu.noid')
                        ->leftJoin('mcard AS mc1', 'so.idcardpj', '=', 'mc1.noid')
                        ->leftJoin('mcard AS mc2', 'so.idcardkoor', '=', 'mc2.noid')
                        ->leftJoin('mcard AS mc3', 'so.idcardsitekoor', '=', 'mc3.noid')
                        ->leftJoin('mcard AS mc4', 'so.idcarddrafter', '=', 'mc4.noid')
                        ->select([
                            'so.noid',
                            'mtt.nama AS mtt_nama',
                            'mttts.nama AS mttts_nama',
                            'so.kode',
                            'so.kodereff',
                            'so.tanggal',
                            'so.idtypecostcontrol',
                            'so.projectname',
                            'so.projectlocation',
                            'so.kode AS mtcc_kode',
                            'mtcc.nama AS mtcc_nama',
                            'so.generaltotal',
                            'so.keterangan',
                            'mcu.myusername AS mcu_myusername',
                            'gmc.nama AS gmc_nama',
                            'gmd.nama AS gmd_nama',
                            'mc1.nama AS idcardpj_nama',
                            'mc2.nama AS idcardkoor_nama',
                            'mc3.nama AS idcardsitekoor_nama',
                            'mc4.nama AS idcarddrafter_nama'
                        ])
                        ->whereNotIn('so.noid', $cc)
                        ->where('so.idtypecostcontrol',$idtypecostcontrol)
                        ->orderBy($orderby['column'], $orderby['sort']);
            if (!$getall) {
                $result->offset($start)->limit($length);
            }
            $result = $result->get();
        }
        return $result;
    }

    public static function getMCardSupplier() {
        $result = DB::connection('mysql2')
                    ->table('mcardsupplier')
                    ->leftJoin('mlokasiprop','mcardsupplier.idlokasiprop','=','mlokasiprop.noid')
                    ->leftJoin('mlokasikab','mcardsupplier.idlokasikota','=','mlokasikab.noid')
                    ->leftJoin('mlokasikec','mcardsupplier.idlokasikec','=','mlokasikec.noid')
                    ->leftJoin('mlokasikel','mcardsupplier.idlokasikel','=','mlokasikel.noid')
                    ->select([
                        'mcardsupplier.noid AS mcardsupplier_noid',
                        'mcardsupplier.nama AS mcardsupplier_nama',
                        'mlokasiprop.noid AS mlokasiprop_noid',
                        'mlokasiprop.kode AS mlokasiprop_kode',
                        'mlokasiprop.nama AS mlokasiprop_nama',
                        'mlokasikab.noid AS mlokasikab_noid',
                        'mlokasikab.kode AS mlokasikab_kode',
                        'mlokasikab.nama AS mlokasikab_nama',
                        'mlokasikec.noid AS mlokasikec_noid',
                        'mlokasikec.kode AS mlokasikec_kode',
                        'mlokasikec.nama AS mlokasikec_nama',
                        'mlokasikel.noid AS mlokasikel_noid',
                        'mlokasikel.kode AS mlokasikel_kode',
                        'mlokasikel.nama AS mlokasikel_nama',
                    ])
                    ->where('mcardsupplier.isactive',1)
                    ->orderBy('mcardsupplier_noid', 'asc')
                    ->get();
        return $result;
    }

    public static function getMTypeTrancTypeStatus($idtypetranc) {
        $result = DB::connection('mysql2')
                    ->table('mtypetranctypestatus AS mttts')
                    ->leftJoin('appmtypeaction AS amta', 'mttts.noid', '=', 'amta.idstatustrancbase')
                    ->select([
                        'mttts.noid',
                        'mttts.idstatusbase',
                        'mttts.nourut',
                        'mttts.kode',
                        'mttts.nama',
                        'mttts.classicon',
                        'mttts.classcolor',
                        'mttts.isinternal',
                        'mttts.isexternal',
                        'mttts.actview',
                        'mttts.actedit',
                        'mttts.actdelete',
                        'mttts.actreportdetail',
                        'mttts.actreportmaster',
                        'mttts.actsetprevstatus',
                        'mttts.actsetnextstatus',
                        'mttts.actsetstatus',
                        'amta.noid AS appmtypeaction_noid',
                        'amta.nama AS appmtypeaction_nama'
                    ])
                    ->where('mttts.idtypetranc', $idtypetranc)
                    ->where('mttts.isactive', 1)
                    ->orderBy('mttts.nourut', 'asc')
                    ->get();
        return $result;
    }

    public static function deleteNotification($params) {
        $result = DB::connection('mysql2')
            ->table('appnotification')
            ->where('tablereff', $params['tablereff'])
            ->where('kodereff', $params['kodereff'])
            ->where('statusreff', $params['statusreff'])
            ->delete();
        if (!$result) return ['success'=>false,'message'=>'Error Delete Notification'];
        return ['success'=>true,'message'=>'Success Delete Notification'];
    }
    
    public static function insertNotification($params) {
        $noid = (new static)->getNextNoid('appnotification');
        $idtypenotification = 1;
        $idtypetranc = $params['idtypetranc'];
        $idreff = 0;
        $tanggal = @$params['tanggal'] ? $params['tanggal'] : 0;
        $keterangan = @$params['keterangan'] ? $params['keterangan'] : 0;
        $isviewed = 0;
        $isread = 0;
        $urlreff = $params['urlreff'];
        $isneedaction = 0;
        $idcreate = Session::get('noid');
        $docreate = date('Y-m-d H:i:s');

        $mcarduser = DB::connection('mysql2')
                        ->table('mcarduser AS mcu')
                        ->leftJoin('mcard AS mc','mcu.noid','=','mc.noid')
                        ->select([
                            'mcu.noid AS mcu_noid',
                            'mcu.myusername AS mcu_myusername',
                            'mcu.groupuser AS mcu_groupuser',
                        ])
                        ->where('mc.isactive',1)
                        ->get();
        
        $mcardnotification = DB::connection('mysql2')
                                ->table('mcardnotification')
                                ->where('idcarduser',Session::get('noid'))
                                ->where('idtypetranc',$idtypetranc)
                                ->where('idtypeaction',$params['idtypeaction'])
                                ->first();
        
        $isnotif = $mcardnotification ? true : false;
        // $noidsents = [];
        // if ($mcardnotification) {
        //     if ($mcardnotification->idcardrolenotif) {

        //     }
        // }
        // dd($mcardnotification);

        $insert = [];
        $noidsents = [];

        if (@$params['insert']) {
            $insert = $params['insert'];
        } else {
            foreach ($mcarduser as $k => $v) {
                if ($isnotif) {
                    if ($mcardnotification->idcardrolenotif != 0) {
                        if ($v->mcu_groupuser == $mcardnotification->idcardrolenotif) {
                            $noidsents[] = $v->mcu_noid;
                            $insert[] = [
                                'noid' => $noid,
                                'idtypenotification' => $idtypenotification, 
                                'idtypetranc' => $idtypetranc, 
                                'idreff' => $idreff, 
                                'idcardassign' => $v->mcu_noid, 
                                'tanggal' => $tanggal, 
                                'keterangan' => $keterangan, 
                                'isviewed' => $isviewed, 
                                'isread' => $isread, 
                                'urlreff' => $urlreff, 
                                'isneedaction' => $isneedaction, 
                                'idcreate' => $idcreate, 
                                'docreate' => $docreate
                            ];
                        }
                    }
                    if (!in_array($mcardnotification->idcardnotif, $noidsents)) {
                        $noidsents[] = $mcardnotification->idcardnotif;
                        $insert[] = [
                            'noid' => $noid,
                            'idtypenotification' => $idtypenotification, 
                            'idtypetranc' => $idtypetranc, 
                            'idreff' => $idreff, 
                            'idcardassign' => $mcardnotification->idcardnotif, 
                            'tanggal' => $tanggal, 
                            'keterangan' => $keterangan, 
                            'isviewed' => $isviewed, 
                            'isread' => $isread, 
                            'urlreff' => $urlreff, 
                            'isneedaction' => $isneedaction, 
                            'idcreate' => $idcreate, 
                            'docreate' => $docreate
                        ];
                    }
                }
                $noid++;
            }
        }

        $result = DB::connection('mysql2')->table('appnotification')->insert($insert);

        return $result;
    }

    public static function getNotification() {
        return DB::connection('mysql2')
            ->table('appnotification AS an')
            ->leftJoin('appmtypenotification AS amtn','an.idtypenotification','=','amtn.noid')
            ->select([
                'an.noid AS an_noid',
                'an.keterangan AS an_keterangan',
                'an.urlreff AS an_urlreff',
                'an.docreate AS an_docreate',
                'amtn.nama AS amtn_nama',
                'amtn.classicon AS amtn_classicon',
                'amtn.classcolor AS amtn_classcolor'
            ])
            ->where('an.idcardassign',Session::get('noid'))
            ->where('an.isread',0)
            ->orderBy('an.docreate','desc')
            ->get();
    }
    
    public static function saveData($data, $datalog) {
        // $queryupdate = '';
        // foreach ($data[(new static)->main['tabledetail']] as $k => $v) {
        //     $queryupdate .= 'UPDATE '.(new static)->main['tabledetailprev'].' SET unitqtysisa='.$v['unitqtysisaprev'].' WHERE noid='.$v['idtrancprevd'].' AND idmaster='.$v['idtrancprev'].';';
        //     unset($data[(new static)->main['tabledetail']][$k]['unitqtysisaprev']);
        // }
        // $resultupdate = DB::connection('mysql2')
        //                     ->select($queryupdate);

        if (count($data[(new static)->main['tabledetail']]) > 0) {
            foreach ($data[(new static)->main['tabledetail']] as $k => $v) {
                $resultupdate[] = DB::connection('mysql2')
                                    ->table((new static)->main['tabledetailprev'])
                                    ->where('noid', $v['idtrancprevd'])
                                    ->where('idmaster', $v['idtrancprev'])
                                    ->update(['unitqtysisa' => $v['unitqtysisaprev'] < 1 ? 0 : $v['unitqtysisaprev']]);
                unset($data[(new static)->main['tabledetail']][$k]['kodeprev']);
                unset($data[(new static)->main['tabledetail']][$k]['unitqtysisaprev']);
            }
        }
        
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->insert($data[(new static)->main['table']]);
        
        $resultdetail = DB::connection('mysql2')
                            ->table((new static)->main['tabledetail'])
                            ->insert($data[(new static)->main['tabledetail']]);

        $resultlog = DB::connection('mysql2')
                        ->table('applog')
                        ->insert($datalog);

        return $result && $resultdetail && $resultlog && @$resultupdate ? 1 : 0;
    }
    
    public static function updateData($data, $noid, $datalog) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($data[(new static)->main['table']]);
        
        @$deletedetail = DB::connection('mysql2')
                    ->table((new static)->main['tabledetail'])
                    ->where('idmaster', $noid)
                    ->delete();

        $resultdetail = DB::connection('mysql2')
                            ->table((new static)->main['tabledetail'])
                            ->insert($data[(new static)->main['tabledetail']]);

        $resultlog = DB::connection('mysql2')
                        ->table('applog')
                        ->insert($datalog);

        return $result && $resultdetail && $resultlog ? 1 : 0;
    }

    public static function getNextNoid($table) {
        $result = DB::connection('mysql2')
                    ->table($table)
                    ->orderBy('noid', 'desc')
                    ->first();
        
        return $result ? $result->noid+1 : 1;
    }

    public static function getNextNourut($params) {
        $result = DB::connection('mysql2')
                    ->table($params['table'])
                    ->select('noid','nourut')
                    ->where('idcostcontrol',$params['where'])
                    ->orderBy('noid', 'desc')
                    ->first();
        
        return $result ? $result->nourut+1 : 1;
    }
    
    public static function getKode($table,$noid) {
        $result = DB::connection('mysql2')
                    ->table($table)
                    ->select('kode')
                    ->where('noid', $noid)
                    ->first();
        
        return $result ? $result->kode : '';
    }

    public static function getLastNoid($table) {
        $result = DB::connection('mysql2')
                    ->table($table)
                    ->orderBy('noid', 'desc')
                    ->first();
        
        return $result ? $result->noid : 1;
    }

    public static function findLog($noid, $kode) {
        $result = DB::connection('mysql2')
                    ->table('applog')
                    ->select('noid')
                    ->where('idreff', $noid)
                    ->where('logsubject', 'like', "%$kode%")
                    ->orderBy('noid', 'asc')
                    ->get();
        return $result;
    }

    public static function findData($noid) {
        $result = DB::connection('mysql2')
            ->table((new static)->main['table'].' AS pr')
            ->leftJoin('mtypetranc AS mtt', 'pr.idtypetranc', '=', 'mtt.noid')
            ->leftJoin('mtypetranctypestatus AS mttts', 'pr.idstatustranc', '=', 'mttts.noid')
            ->leftJoin('mcarduser AS mcu', 'pr.idstatuscard', '=', 'mcu.noid')
            ->leftJoin('salesorder AS so', 'pr.idcostcontrol', '=', 'so.noid')
            ->leftJoin('accmcurrency AS amc', 'pr.idcurrency', '=', 'amc.noid')
            ->select([
                'pr.*',
                'mtt.kode AS kodetypetranc',
                'mttts.nama AS mtypetranctypestatus_nama',
                'mttts.classcolor AS mtypetranctypestatus_classcolor',
                'mcu.myusername AS mcarduser_myusername',
                'so.kode AS kodecostcontrol',
                'amc.nama AS namacurrency',
            ])
            ->where('pr.noid', $noid)
            ->first();

        $resultdetail = DB::connection('mysql2')
            ->table((new static)->main['tabledetail'].' AS prd')
            ->leftJoin((new static)->main['tableprev'].' AS pr', 'prd.idtrancprev', '=', 'pr.noid')
            ->leftJoin('invminventory AS imi', 'prd.idinventor', '=', 'imi.noid')
            ->leftJoin('invmsatuan AS ims', 'prd.idsatuan', '=', 'ims.noid')
            ->leftJoin('accmpajak AS amp', 'prd.idtax', '=', 'amp.noid')
            ->select([
                'prd.noid AS noid',
                'pr.kode AS kodeprev',
                'prd.idinventor AS idinventor',
                'imi.kode AS kodeinventor',
                'imi.nama AS namainventor',
                'prd.namainventor AS namainventor2',
                'prd.unitqty AS unitqty',
                'prd.idcompany AS idcompany',
                'prd.iddepartment AS iddepartment',
                'prd.idgudang AS idgudang',
                'prd.keterangan AS keterangan',
                'prd.idsatuan AS idsatuan',
                'ims.nama AS namasatuan',
                'prd.konvsatuan AS konvsatuan',
                'prd.unitqtysisa AS unitqtysisa',
                'prd.unitprice AS unitprice',
                'prd.subtotal AS subtotal',
                'prd.discountvar AS discountvar',
                'prd.discount AS discount',
                'prd.nilaidisc AS nilaidisc',
                'prd.idtypetax AS idtypetax',
                'prd.idtax AS idtax',
                'prd.prosentax AS prosentax',
                'prd.nilaitax AS nilaitax',
                'prd.hargatotal AS hargatotal',
                'amp.nama AS idtax_nama',
            ])
            ->where('prd.idmaster', $noid)
            ->orderBy('imi.kode', 'asc')
            ->get();

        if (!is_null($result)) {
            $resultcostcenter = DB::connection('mysql2')
                ->table('salesorder')
                ->where('noid', $result->idcostcontrol)
                ->first();
        }

        $data = [
            'purchase' => $result,
            'purchasedetail' => $resultdetail ? $resultdetail : [],
            'costcenter' => @$resultcostcenter
        ];

        return $data;
    }

    public static function findLastSupplier() {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->select([
                        'noid',
                        'idcardsupplier',
                        'idakunsupplier',
                        'suppliernama',
                        'supplieraddress1',
                        'supplieraddress2',
                        'supplieraddressrt',
                        'supplieraddressrw',
                        'supplierlokasikel',
                        'supplierlokasikec',
                        'supplierlokasikab',
                        'supplierlokasiprop',
                        'supplierlokasineg',
                    ])
                    ->orderBy('noid', 'desc')
                    ->first();

        return $result;
    }

    public static function sendToNext($params) {
        $noid = $params['noid'];
        
        // Find data
        $prev = (new static)->findData($noid);
        if (!$prev) return ['success'=>false,'message'=>'Error Find Data'];
        
        // Convert master to Array
        $master1 = (array)$prev['purchase'];
        $master2 = (array)$prev['purchase'];

        // Next Noid
        $nextnoid1 = (new static)->getNextNoid((new static)->main['table2a']);
        $nextnoid2 = (new static)->getNextNoid((new static)->main['table2b']);
        $nextnoiddetail1 = (new static)->getNextNoid((new static)->main['tabledetail2a']);
        $nextnoiddetail2 = (new static)->getNextNoid((new static)->main['tabledetail2b']);

        // Generate Code
        $gc1 = (new static)->generateCode([
            'tablemaster' => (new static)->main['table2a'],
            'generatecode' => (new static)->main['generatecode2a'],
            'data' => $master1
        ]);
        $gc2 = (new static)->generateCode([
            'tablemaster' => (new static)->main['table2b'],
            'generatecode' => (new static)->main['generatecode2b'],
            'data' => $master2
        ]);

        // Replacing data master
        $master1['noid'] = $nextnoid1;
        $master1['idstatustranc'] = (new static)->main['defaultidstatustranc2a'];
        $master1['idtypetranc'] = (new static)->main['idtypetranc2a'];
        $master1['idstatuscard'] = Session::get('noid');
        $master1['kodereff'] = $master1['kode'];
        $master1['kode'] = $gc1['kode'];
        $master1['nourut'] = $gc1['nourut'];
        $master1['idupdate'] = Session::get('noid');
        $master1['lastupdate'] = date('Y-m-d H:i:s');
        unset($master1['kodetypetranc']);
        unset($master1['mtypetranctypestatus_nama']);
        unset($master1['mtypetranctypestatus_classcolor']);
        unset($master1['mcarduser_myusername']);
        unset($master1['kodecostcontrol']);
        unset($master1['idcardpj']);
        unset($master1['idcardkoor']);
        unset($master1['idcardsitekoor']);
        unset($master1['namacurrency']);
        unset($master1['termofpayment']);
        unset($master1['deliverytime']);
        unset($master1['isdeliverydone']);
        unset($master1['isinvoicedone']);
        unset($master1['typepaymentvar']);

        $master2['noid'] = $nextnoid2;
        $master2['idstatustranc'] = (new static)->main['defaultidstatustranc2b'];
        $master2['idtypetranc'] = (new static)->main['idtypetranc2b'];
        $master2['idstatuscard'] = Session::get('noid');
        $master2['kodereff'] = $master2['kode'];
        $master2['kode'] = $gc2['kode'];
        $master2['nourut'] = $gc2['nourut'];
        $master2['idupdate'] = Session::get('noid');
        $master2['lastupdate'] = date('Y-m-d H:i:s');
        unset($master2['kodetypetranc']);
        unset($master2['mtypetranctypestatus_nama']);
        unset($master2['mtypetranctypestatus_classcolor']);
        unset($master2['mcarduser_myusername']);
        unset($master2['kodecostcontrol']);
        unset($master2['idcardpj']);
        unset($master2['idcardkoor']);
        unset($master2['idcardsitekoor']);
        unset($master2['namacurrency']);
        unset($master2['termofpayment']);
        unset($master2['deliverytime']);
        unset($master2['isdeliverydone']);
        unset($master2['isinvoicedone']);

        // Convert detail to Array
        $detail1 = [];
        $detail2 = [];
        foreach ($prev['purchasedetail'] as $k => $v) {
            $detail1[$k] = (array)$v;
            $detail1[$k]['noid'] = $nextnoiddetail1;
            $detail1[$k]['idmaster'] = $nextnoid1;
            unset($detail1[$k]['kodeinventor']);
            unset($detail1[$k]['namainventor2']);
            unset($detail1[$k]['namasatuan']);
            unset($detail1[$k]['idtax_nama']);
            unset($detail1[$k]['kodeprev']);

            $detail2[$k] = (array)$v;
            $detail2[$k]['noid'] = $nextnoiddetail2;
            $detail2[$k]['idmaster'] = $nextnoid2;
            unset($detail2[$k]['kodeinventor']);
            unset($detail2[$k]['namainventor2']);
            unset($detail2[$k]['namasatuan']);
            unset($detail2[$k]['idtax_nama']);
            unset($detail2[$k]['kodeprev']);

            $nextnoiddetail1++;
            $nextnoiddetail2++;
        }

        // Inserting data
        $insertmaster1 = DB::connection('mysql2')
            ->table((new static)->main['table2a'])
            ->insert($master1);
        if (!$insertmaster1) return ['success'=>false,'message'=>'Error Insert Master 1'];

        $insertmaster2 = DB::connection('mysql2')
            ->table((new static)->main['table2b'])
            ->insert($master2);
        if (!$insertmaster2) return ['success'=>false,'message'=>'Error Insert Master 2'];


        $insertdetail1 = DB::connection('mysql2')
            ->table((new static)->main['tabledetail2a'])
            ->insert($detail1);
        if (!$insertdetail1) return ['success'=>false,'message'=>'Error Insert Detail 1'];

        $insertdetail2 = DB::connection('mysql2')
            ->table((new static)->main['tabledetail2b'])
            ->insert($detail2);
        if (!$insertdetail2) return ['success'=>false,'message'=>'Error Insert Detail 2'];
        
        return [
            'success' => true,
            'message' => 'Success Approve Data',
            'data' => [
                'kode' => [$master1['kode'],$master2['kode']],
                'idstatustranc' => [$master1['idstatustranc'],$master2['idstatustranc']]
            ]
        ];
    }

    public static function snst($params) {
        $noid = $params['noid'];
        $kode = $params['kode'];
        $data = $params['data'];
        $datalog = $params['datalog'];
        $keterangannotif = $params['keterangannotif'];

        if ($data['idstatustranc'] == 5030) {
            // CANCELED NOTIF
            $resultdelete = (new static)->deleteNotification([
                'tablereff' => (new static)->main['table'],
                'kodereff' => $kode,
                'statusreff' => 5031,
            ]);
        } else if ($data['idstatustranc'] == 5031) {
            // CANCELED NOTIF
            $resultdelete = (new static)->deleteNotification([
                'tablereff' => (new static)->main['table'],
                'kodereff' => $kode,
                'statusreff' => 5033,
            ]);

            // NOTIF PROJECT KOOR
            if (Session::get('noid') != $data['idcardkoor']) {
                (new static)->insertNotification([
                    'insert' => [
                        'noid' => (new static)->getNextNoid('appnotification'),
                        'idtypenotification' => 1, 
                        'idtypetranc' => 503, 
                        'idreff' => 0, 
                        'tablereff' => (new static)->main['table'], 
                        'kodereff' => $kode, 
                        'statusreff' => $data['idstatustranc'], 
                        'idcardassign' => $data['idcardkoor'], 
                        'tanggal' => @$data['tanggal'], 
                        'keterangan' => $keterangannotif.' (Need to Verify)', 
                        'isviewed' => 0, 
                        'isread' => 0, 
                        'urlreff' => (new static)->main['noid'], 
                        'isneedaction' => 0, 
                        'idcreate' => Session::get('noid'), 
                        'docreate' => date('Y-m-d H:i:s')
                    ],
                    'idtypetranc' => (new static)->main['idtypetranc'],
                    'idtypeaction' => @$idtypeaction,
                    'urlreff' => (new static)->main['noid'],
                    'tanggal' => @$data['tanggal'],
                    'keterangan' => @$data['keterangan'],
                ]);
            }
        } else if ($data['idstatustranc'] == 5033) {
            // NOTIF PROJECT MANAGER
            if (Session::get('noid') != $data['idcardpj']) {
                (new static)->insertNotification([
                    'insert' => [
                        'noid' => (new static)->getNextNoid('appnotification'),
                        'idtypenotification' => 1, 
                        'idtypetranc' => 503, 
                        'idreff' => 0, 
                        'tablereff' => (new static)->main['table'], 
                        'kodereff' => $kode, 
                        'statusreff' => $data['idstatustranc'], 
                        'idcardassign' => $data['idcardpj'], 
                        'tanggal' => $data['tanggal'], 
                        'keterangan' => $keterangannotif.' (Need to Approve)', 
                        'isviewed' => 0, 
                        'isread' => 0, 
                        'urlreff' => (new static)->main['noid'], 
                        'isneedaction' => 0, 
                        'idcreate' => Session::get('noid'), 
                        'docreate' => date('Y-m-d H:i:s')
                    ],
                    'idtypetranc' => (new static)->main['idtypetranc'],
                    'idtypeaction' => '3',
                    'urlreff' => (new static)->main['noid'],
                    'tanggal' => $data['tanggal'],
                    'keterangan' => $data['keterangan'],
                ]);
            }
        } else if ($data['idstatustranc'] == 5034) {
            // SEND TO NEXT A
            $stn = (new static)->sendToNext(['noid'=>$params['noid']]);
            if (!$stn['success']) return $stn;

            // NOTIF PURCHASING
            foreach ((new static)->main['purchasing'] as $k => $v) {
                $notif1 = (new static)->insertNotification([
                    'insert' => [
                        'noid' => (new static)->getNextNoid('appnotification'),
                        'idtypenotification' => 1, 
                        'idtypetranc' => 504, 
                        'idreff' => 0, 
                        'tablereff' => (new static)->main['table2a'], 
                        'kodereff' => $stn['data']['kode'][0], 
                        'statusreff' => $stn['data']['idstatustranc'][0], 
                        'idcardassign' => $v['noid'], 
                        'tanggal' => $data['tanggal'], 
                        'keterangan' => 'Data PDN Kode = '.$stn['data']['kode'][0].' (Need to Delivery)', 
                        'isviewed' => 0, 
                        'isread' => 0, 
                        'urlreff' => (new static)->main['noid2a'], 
                        'isneedaction' => 0, 
                        'idcreate' => Session::get('noid'), 
                        'docreate' => date('Y-m-d H:i:s')
                    ],
                    'idtypetranc' => (new static)->main['idtypetranc'],
                    'idtypeaction' => '3',
                    'urlreff' => (new static)->main['noid2a'],
                    'tanggal' => $data['tanggal'],
                    'keterangan' => $data['keterangan'],
                ]);

                $notif2 = (new static)->insertNotification([
                    'insert' => [
                        'noid' => (new static)->getNextNoid('appnotification'),
                        'idtypenotification' => 1, 
                        'idtypetranc' => 505, 
                        'idreff' => 0, 
                        'tablereff' => (new static)->main['table2b'], 
                        'kodereff' => $stn['data']['kode'][1], 
                        'statusreff' => $stn['data']['idstatustranc'][1], 
                        'idcardassign' => $v['noid'], 
                        'tanggal' => $data['tanggal'], 
                        'keterangan' => 'Data PIV Kode = '.$stn['data']['kode'][1].' (Need to Sent to PM)', 
                        'isviewed' => 0, 
                        'isread' => 0, 
                        'urlreff' => (new static)->main['noid2b'], 
                        'isneedaction' => 0, 
                        'idcreate' => Session::get('noid'), 
                        'docreate' => date('Y-m-d H:i:s')
                    ],
                    'idtypetranc' => (new static)->main['idtypetranc'],
                    'idtypeaction' => '3',
                    'urlreff' => (new static)->main['noid2b'],
                    'tanggal' => $data['tanggal'],
                    'keterangan' => $data['keterangan'],
                ]);
            }
        }

        // (new static)->insertNotification([
        //     'idtypetranc' => (new static)->main['idtypetranc'],
        //     'idtypeaction' => $data['idstatustranc'],
        //     'urlreff' => (new static)->main['noid'],
        //     'tanggal' => $data['tanggal'],
        //     'keterangan' => $datalog['keterangan'],
        // ]);

        if (!$noid) {
            $noid = (new static)->getLastNoid((new static)->main['table']);
            $datalog['idtypetranc'] = (new static)->main['idtypetranc'];
            $datalog['logsubject'] = $datalog['logsubject'].$noid;
            $datalog['keterangan'] = 'Save to Issued';
        }

        $dataupdate = [
            'tanggal' => $data['tanggal'],
            'idstatustranc' => $data['idstatustranc'],
        ];

        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($dataupdate);

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultlog ? 1 : 0;
    }

    public static function div($data, $noid, $datalog) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($data);

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultlog ? 1 : 0;
    }

    public static function sapcf($data, $noid, $datalog) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($data);

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultlog ? 1 : 0;
    }

    public static function setPending($data, $noid, $datalog) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($data);

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultlog ? 1 : 0;
    }

    public static function setCanceled($data, $noid, $datalog) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->update($data);

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultlog ? 1 : 0;
    }

    public static function getStringBetween($str,$from,$to) {
        $string = substr($str, strpos($str, $from) + strlen($from));
        if (strstr($string,$to,TRUE) != FALSE) {
            $string = strstr($string,$to,TRUE);
        } else {
            $string = FALSE;
        }
        return $string;
    }

    public static function generateCode($params) {
        $tablemaster = $params['tablemaster'];
        $costcontrol = (new static)->getDefaultCostControl();
        $idcostcontrol = @$params['idcostcontrol'] ? $params['idcostcontrol'] : $costcontrol->noid;
        $kodecostcontrol = @$params['kodecostcontrol'] ? $params['kodecostcontrol'] : $costcontrol->kode;
        $formdefault = explode('/', $params['generatecode']);
        
        $result_appmkode = DB::connection('mysql2')->table('appmkode')->where('noid', $formdefault[1])->first();
        $result_appmkodedetail = DB::connection('mysql2')->table('appmkodedetail')->where('idkode', $formdefault[1])->orderBy('nourut', 'ASC')->get();


        $formdefault = '';
        foreach ($result_appmkodedetail as $k2 => $v2) {
            if ($v2->kodename == 'KODE') {
                $formdefault .= $v2->kodevalue;
            } else if ($v2->kodename == 'FIELDTABLE') {
                $fungsigroupby = explode(';',$v2->fungsigroupby);
                $resultfieldtable = DB::connection('mysql2')->table($v2->kodeformat)->where($fungsigroupby[0], $params['data'][$fungsigroupby[1]])->first();
                $fieldname = $v2->fieldname;
                $formdefault .= str_replace('[FIELDTABLE]', $resultfieldtable->$fieldname, $v2->kodevalue);
            } else if ($v2->kodename == 'NOURUT') {
                $groupby = '';
                $fungsigroupby = explode(';', $v2->fungsigroupby);
                $select = '';
                $arrayselect = [];
                $where = 'WHERE 1=1 ';
                if ($v2->fungsigroupby != '' && $v2->fungsigroupby != null) {
                    $groupby .= 'GROUP BY ';
                    foreach ($fungsigroupby as $k3 => $v3) {
                        if ($v3) {
                            $groupby .= $v3.',';
                        }
                        $select .= $v3.',';
                        $arrayselect[] = $v3;

                        $_gsb_month = (new static)->getStringBetween($v3,'MONTH(',')');
                        $_gsb_year = (new static)->getStringBetween($v3,'YEAR(',')');

                        if (@$params['data'][$_gsb_month]) {
                            $where .= 'AND '.$v3.'='.date('m', strtotime($params['data'][$_gsb_month])).' ';
                        } else if (@$params['data'][$_gsb_year]) {
                            $where .= 'AND '.$v3.'='.date('Y', strtotime($params['data'][$_gsb_year])).' ';
                        } else {
                            $where .= 'AND '.$v3.'='.$params['data'][$v3].' ';
                        }
                    }
                    $groupby = substr($groupby, 0 ,-1);
                }
                $fieldname = $v2->fieldname;
                $fungsi = $v2->fungsi;
                // $qtylastnourut = "SELECT MAX(nourut) as qtylnu FROM $tablemaster GROUP BY $groupby";

                $qtylastnourut = "SELECT $select $fungsi($fieldname) as qtylnu FROM $tablemaster $where $groupby";
                $resultqtylnu = DB::connection('mysql2')->select($qtylastnourut);

                

                $qtylnu = 1;
                if (count($resultqtylnu) > 0) {
                    foreach($resultqtylnu as $k3 => $v3) {
                        $found = false; 

                        if (count((array)$v3) > 1) {
                            foreach($v3 as $k4 => $v4) {
                                $_gsb_month = (new static)->getStringBetween($k4,'MONTH(',')');
                                $_gsb_year = (new static)->getStringBetween($k4,'YEAR(',')');
    
                                if ($k4 != 'qtylnu') {
                                    // print($v4.'-'.@(string)$params[$k4].'////');
                                    if (@$params['data'][$_gsb_month] || @$params['data'][$_gsb_year]) {
                                        $qtylnu = $v3->qtylnu+1;
                                        // $found = true;
                                        // break;
                                    } else if ((string)$v4 == @(string)$params['data'][$k4]) {
                                        $qtylnu = $v3->qtylnu+1;
                                        // $found = true;
                                        // break;
                                    } else {
                                        $found = false;
                                    }
                                }
                            }
                        } else {
                            $qtylnu = $v3->qtylnu+1;
                            $found = true;
                            break;
                        }

                        if ($found) {
                            break;
                        }
                    }
                }
                // dd($qtylnu);
                // die;
                // $fungsigroupby = explode(';', $v2->fungsigroupby);
                // // $query_nourut = "SELECT MAX(noid) AS noid FROM $tablemaster WHERE $fungsigroupby[2] = $v2->idkode GROUP BY ";
                // $query_nourut = "SELECT MAX(docreate) AS noid FROM $tablemaster ";
                // foreach ($fungsigroupby as $k3 => $v3) {
                //     if ($v3 != '') {
                //         if ($k3 == 0) {
                //             $query_nourut .= 'GROUP BY ';
                //         }
                //         if ($v3) {
                //             $query_nourut .= "$v3,";
                //         }
                //     }
                // }
                // $query_nourut = substr($query_nourut, 0, -1);
                // $query_nourut .= " LIMIT 1";
                // $result_nourut = DB::connection('mysql2')->select($query_nourut);
                // if ($result_nourut) {
                //     $nourut = substr($v2->kodeformat, 0, strlen($v2->kodeformat)-strlen($result_nourut[0]->noid)).$result_nourut[0]->noid;
                // } else {
                //     $nourut = 0;
                // }
                $nourut = '';
                $idx = 0;
                for ($i=strlen($qtylnu); $i<strlen($v2->kodeformat); $i ++) {
                    $nourut .= $v2->kodeformat[$idx];
                    $idx++;
                }
                $nourut .= $qtylnu;
                $formdefault .= $nourut;
            } else if ($v2->kodename == 'BULAN') {
                $formdefault .= str_replace('[BULAN]', date(explode('%', $v2->kodeformat)[1]), $v2->kodevalue);
            } else if ($v2->kodename == 'TAHUN') {
                $formdefault .= str_replace('[TAHUN]', date(explode('%', $v2->kodeformat)[1]), $v2->kodevalue);
            }
        }

        $resultnourut = DB::connection('mysql2')->select("SELECT MAX(nourut) AS nourut FROM $tablemaster");

        return [
            'kode' => $formdefault,
            // 'nourut' => count($resultnourut) == 0 ? 1 : $resultnourut[0]->nourut+1,
            'nourut' => $qtylnu,
        ];
    }

    public static function deleteSelectM($selecteds, $selectcheckboxmall, $datalog) {
        $result = DB::connection('mysql2')
                    ->table((new static)->main['table']);
        if ((bool)$selectcheckboxmall) {
            $result->delete();
        } else {
            $result->whereIn('noid', $selecteds)->delete();
        }

        if ((bool)$selectcheckboxmall) {
            $resultdetail = DB::connection('mysql2')
                ->table((new static)->main['tabledetail'])
                ->delete();
        } else {
            foreach ($selecteds as $k => $v) {
                $checkresultdetail = (new static)->findData($v)['purchasedetail'];
                if (count($checkresultdetail) > 0) {
                    $resultdetail = DB::connection('mysql2')
                                    ->table((new static)->main['tabledetail'])
                                    ->where('idmaster', $v)
                                    ->delete();
                } else {
                    $resultdetail = true;
                }
            }
        }

        $resultlog = DB::connection('mysql2')
                    ->table('applog')
                    ->insert($datalog);

        return $result && $resultdetail && $resultlog ? 1 : 0;
    }

    public static function deleteData($noid, $datalog) {
        $idtypetranc = (new static)->findData($noid)['purchase']->idtypetranc;
        $datalog['idtypetranc'] = $idtypetranc;

        $checkresultdetail = (new static)->findData($noid)['purchasedetail'];
        if (count($checkresultdetail) > 0) {
            $resultdetail = DB::connection('mysql2')
                            ->table((new static)->main['tabledetail'])
                            ->where('idmaster', $noid)
                            ->delete();
        } else {
            $resultdetail = true;
        }

        $result = DB::connection('mysql2')
                    ->table((new static)->main['table'])
                    ->where('noid', $noid)
                    ->delete();
                        
        $resultlog = DB::connection('mysql2')
                        ->table('applog')
                        ->insert($datalog);

        return $result && $resultdetail && $resultlog ? 1 : 0;
    }

}
