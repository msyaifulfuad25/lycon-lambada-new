<?php

namespace App\Model;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;
    protected $table = 'mcarduser';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'myusername',
        'idhomepage',
         'mypassword',
         'islogin',
         'lastlogin',
         'lastaction',
         'lastactionurl',
         'lastipaddress',
         'lastnotif',
         'lasttask',
         'lastinbox',
         'lastsms',
         'groupuser',
         'securitylevel',
         'profilepic',
         'thumbnailpic',
         'backgroundpic',
         'idcreate',
         'docreate',
         'idupdate',
         'lastupdate',
    ];

public function getAll()
{
  print_r(';ok');die();
//   $names = array('1', '2');
// $this->db->where_in('menulevel', $names);
// $this->db->where("isactive = 1");
// $this->db->where("ispublic = 1");
}

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'mypassword',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];

    static function getPositions($params) {
        $result = DB::connection('mysql2')
            ->table('mcard AS mc')
            ->leftJoin('mcardposition AS mcp', 'mcp.kodecard', '=', 'mc.kode')
            ->leftJoin('mposition AS mp', 'mp.noid', '=', 'mcp.idposition')
            ->select([
                'mc.kode AS mcard_kode',
                'mc.kode AS mcard_kode',
                'mp.kode AS mposition_kode'
            ])
            ->where('mc.noid', $params['noid'])
            ->get();
        return $result;
    }
}
