<?php 
namespace App\Model\excel\exports;
 
use DB;
use App\Model\Purchase\ReportingPurchase AS ReportingPurchaseModel;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
// use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithEvents;
// use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadSheet\Style\NumberFormat;
use PhpOffice\PhpSpreadSheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use App\Helpers\MyHelper;
 
class SalesInquiryPS implements WithEvents, ShouldAutoSize {

    use Exportable;

    protected $cellAfter = 18;

    public function __construct(array $data) {
        $this->dates = $data['dates'];
        $this->jobs = $data['jobs'];
        $this->jobs2 = $data['jobs2'];
        $this->jobs3 = $data['jobs3'];
    }

    public function nextChar($char, $next) {
        return MyHelper::nextChar(['char'=>$char,'next'=>$next]);
    }

    public function getWeekends($params) {
        $saturdaydates = $params['saturdaydates'];
        $sundaydates = $params['sundaydates'];
        $saturdays = $params['saturdays'];
        $sundays = $params['sundays'];
        $event = $params['event'];
        $row = $params['row'];

        foreach ($saturdaydates as $k2 => $v2) {
            $event->sheet->getStyle($saturdays[$v2].$row)->applyFromArray([
                'fill' => [
                    'fillType' => 'solid',
                    'color' => [
                        'rgb' => 'bcb03e'
                    ],
                ],
            ]);
        }

        foreach ($sundaydates as $k2 => $v2) {
            $event->sheet->getStyle($sundays[$v2].$row)->applyFromArray([
                'fill' => [
                    'fillType' => 'solid',
                    'color' => [
                        'rgb' => 'a8711e'
                    ],
                ],
            ]);
        }
    }

    public function getDatesInRange($params) {
        $date1 = $params['date1'];
        $date2 = $params['date2'];
        $type = $params['type'];

        $startdate = strtotime($date1);
        $stopdate = strtotime($date2);
        $firststartdate = date(
            'j', 
            mktime(0,0,0,
                date('n', $startdate),
                date('j', $startdate),
                date('Y', $startdate)
            )
        );
        $laststartdate = date(
            't', 
            mktime(0,0,0,
                date('n', $startdate),
                date('j', $startdate),
                date('Y', $startdate)
            )
        );
        $resultdiffdate = date_diff(date_create($date1), date_create($date2));
        $diffmonth = $resultdiffdate->m+($resultdiffdate->y*12)+($resultdiffdate->d == 0 ? 0 : 1);
        
        $dates = [];
        $week = 0;
        $dayweek = 0;
        for ($h=0; $h<$diffmonth; $h++) {
            $firststartdate = $h > 0 ? 1 : $firststartdate;
            $laststartdate = date(
                't', 
                strtotime(
                    "+$h month",
                    mktime(0,0,0,
                        date('n', $startdate),
                        date('j', $startdate),
                        date('Y', $startdate)
                    )
                )
            );
            $year = date('Y', strtotime("+$h month", $startdate));
            $month = date('n', strtotime("+$h month", $startdate));

            for ($i=$firststartdate; $i<=$laststartdate; $i++) {
                if ($dayweek == 7) {
                    $dayweek = 0;
                    $week++;
                }
                // echo date('Y-n-j', strtotime("$year-$month-$i"))."<br>";
                if ($type == 'all') {
                    $dates[] = [
                        'date' => date('Y-n-j', strtotime("$year-$month-$i")),
                        'y' => $year,
                        'm' => $month,
                        'd' => $i,
                        'w' => $week,
                        'n' => date('N', mktime(0,0,0, $month, $i, $year))
                    ];
                } else if ($type == 'onlydate'){
                    $dates[] = date('Y-m-d', strtotime("$year-$month-$i"));
                }
                // $dates[] = $week;
                if ($date2 == date('Y-m-d', strtotime("$year-$month-$i"))) {
                    break;
                }

                $dayweek++;
            }
        }
        return $dates;
    }

    public function registerEvents(): array {
        return [
            BeforeSheet::class => function (BeforeSheet $event) {
                $basebold = [
                    'font' => [
                        'bold' => true
                    ],
                    'color' => 'black'
                ];

                $baseborder = [
                    'borders' => [
                        'outline' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '000000'],
                        ],
                    ]
                ];

                $baseheader = [
                    'borders' => [
                        'outline' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '000000'],
                        ],
                    ],
                    'font' => [
                        'bold' => true
                    ],
                    'color' => 'black',
                    'alignment' => [
                        'horizontal' => 'center',
                        'vertical' => 'center'
                    ]
                ];

                // DATA
                $event->sheet->mergeCells('B2:B4');
                $event->sheet->mergeCells('C2:C4');
                $event->sheet->mergeCells('D2:D4');
                $event->sheet->mergeCells('E2:E4');
                $event->sheet->mergeCells('F2:F4');
                $event->sheet->mergeCells('G2:G4');
                $event->sheet->mergeCells('H2:H4');
                $event->sheet->setCellValue('B2', 'PR NO')->getStyle('B2:B4')->applyFromArray($baseheader);
                $event->sheet->setCellValue('C2', 'PR DATE')->getStyle('C2:C4')->applyFromArray($baseheader);
                $event->sheet->setCellValue('D2', 'ITEM DESCRIPTION')->getStyle('D2:D4')->applyFromArray($baseheader);
                $event->sheet->setCellValue('E2', 'REQ PO')->getStyle('E2:E4')->applyFromArray($baseheader);
                $event->sheet->setCellValue('F2', 'REQ ON SITE')->getStyle('F2:F4')->applyFromArray($baseheader);
                $event->sheet->setCellValue('G2', 'ORDER STATUS')->getStyle('G2:G4')->applyFromArray($baseheader);
                $event->sheet->setCellValue('H2', 'ANGGARAN')->getStyle('H2:H4')->applyFromArray($baseheader);
                

                $costpergroup = [];
                $total = 0;
                // dd($this->jobs);
                foreach ($this->jobs as $k => $v) {
                    if (!array_key_exists($v->sig_noid, $costpergroup)) {
                        $costpergroup[$v->sig_noid] = 0;
                    }
                    $costpergroup[$v->sig_noid] += $v->sii_offerprice;
                    $total += $v->sii_offerprice;
                }

                // $dates = [];
                $totalperdate = [];
                foreach ($this->jobs3 as $k => $v) {
                    // dd($this->jobs3);
                    if ($v->sips_plandate != '') {
                        $dates[$v->sips_plandate] = 0;
                    }

                    if (!is_null($v->sips_plandate)) {
                        // if (array_key_exists($v->sips_plantotal, $dates) && in_array($v->actutaldate, $dates[$v->sips_plantotal])) {
                            // $dates[$v->sips_plandate]['nodes'][$v->sips_actualdate][$v->sig_noid] = $v->sips_plantotal;
                        // }
                        $dates[($v->sips_plandate == '' ? '-' : $v->sips_plandate).'_'.($v->sips_actualdate == '' ? '-' : $v->sips_actualdate)] = 0;

                        if (!array_key_exists($v->sips_plandate, $totalperdate)) {
                            $totalperdate[$v->sips_plandate] = 0;
                        }

                        
                        $totalperdate[$v->sips_plandate] += $v->sips_plantotal;
                        
                        
                        // if ($k == 5) {
                        //     // dd($v->sips_plantotal);
                        //     dd($totalperdate);
                        // }
                    }
                }

                $i = 0;
                $firstdateperiode = '';
                $lastdateperiode = '';
                $dateperiode = 0;
                $countdateperiode = 1;
                $row = 3;
                foreach ($dates as $k => $v) {
                    if ($i == 0) {
                        $firstdateperiode = $this->nextChar('I', 0);
                    } else {
                        if (!strpos($k, '_')) {
                            $lastdateperiode = $this->nextChar('I', $dateperiode-1);
                            // dd("$firstdateperiode$row:$lastdateperiode$row");
                            $event->sheet->mergeCells("$firstdateperiode$row:$lastdateperiode$row");
                            $event->sheet->setCellValue("$firstdateperiode$row", $countdateperiode)->getStyle("$firstdateperiode$row")->applyFromArray([
                                'alignment' => [ 'horizontal' => 'center' ]
                            ]);

                            $firstdateperiode = $this->nextChar('I', $dateperiode);
                            $countdateperiode++;
                        }

                        if (count($dates) == $i+1) {
                            $lastdateperiode = $this->nextChar('I', $dateperiode);
                            $event->sheet->mergeCells("$firstdateperiode$row:$lastdateperiode$row");
                            $event->sheet->setCellValue("$firstdateperiode$row", $countdateperiode)->getStyle("$firstdateperiode$row:$lastdateperiode$row")->applyFromArray([
                                'borders' => [ 'outline' => [ 'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN, 'color' => ['argb' => '000000'], ], ],
                                'alignment' => [ 'horizontal' => 'center' ]
                            ]);
                            // dd("$firstdateperiode$row:$lastdateperiode$row");
                        }
                    }

                    $dateperiode++;
                    $i++;
                }


                $event->sheet->mergeCells('I2:'.$this->nextChar('I',count($dates)-1).'2');
                $event->sheet->setCellValue('I2', 'PURCHASING SCHEDULE')->getStyle('I2:'.$this->nextChar('I',count($dates)-1).'2')->applyFromArray($baseheader);
                // dd($dates);


                $jobs = [];
                foreach ($this->jobs as $k => $v) {
                    $jobs[$v->sij_noid.'_'.$v->sig_noid.'_'.($v->sips_plandate == '' ? '-' : $v->sips_plandate)] = $v->sips_plantotal;
                    $jobs[$v->sij_noid.'_'.$v->sig_noid.'_'.($v->sips_plandate == '' ? '-' : $v->sips_plandate).'_'.($v->sips_actualdate == '' ? '-' : $v->sips_actualdate)] = $v->sips_actualtotal;
                }         


                // dd($dates);
                $row = 5;
                // $coba = 0;
                foreach ($this->jobs2 as $k => $v) {
                    $event->sheet->setCellValue('B'.$row, $v->sij_name)->getStyle('B'.$row)->applyFromArray($baseborder);
                    $event->sheet->setCellValue('C'.$row, MyHelper::dbdateToHumandate($v->sig_prdate))->getStyle('C'.$row)->applyFromArray($baseborder);
                    $event->sheet->setCellValue('D'.$row, $v->sig_name)->getStyle('D'.$row)->applyFromArray($baseborder);
                    $event->sheet->setCellValue('E'.$row, MyHelper::dbdateToHumandate($v->sig_reqpodate))->getStyle('E'.$row)->applyFromArray($baseborder);
                    $event->sheet->setCellValue('F'.$row, MyHelper::dbdateToHumandate($v->sig_reqonsitedate))->getStyle('F'.$row)->applyFromArray($baseborder);
                    $event->sheet->setCellValue('G'.$row, '')->getStyle('G'.$row)->applyFromArray($baseborder);
                    $event->sheet->setCellValue('H'.$row, MyHelper::currencyConv($costpergroup[$v->sig_noid]))->getStyle('H'.$row)->applyFromArray([
                        'borders' => [ 'outline' => [ 'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN, 'color' => ['argb' => '000000'], ], ],
                        'alignment' => [ 'horizontal' => 'right' ]
                    ]);

                    $i = 0;
                    foreach ($dates as $k2 => $v2) {
                        if (!is_null($v->sips_plandate)) {
                            if (array_key_exists($v->sij_noid.'_'.$v->sig_noid.'_'.$k2, $jobs) && !is_null($jobs[$v->sij_noid.'_'.$v->sig_noid.'_'.$k2])) {
                                $event->sheet->setCellValue($this->nextChar('I',$i).$row, MyHelper::currencyConv($jobs[$v->sij_noid.'_'.$v->sig_noid.'_'.$k2]))->getStyle($this->nextChar('I',$i).$row)->applyFromArray([
                                    'alignment' => [ 'horizontal' => 'right' ]
                                ]);

                                if (!array_key_exists($k2, $dates)) {
                                    $dates[$k2] = $jobs[$v->sij_noid.'_'.$v->sig_noid.'_'.$k2];
                                } else {
                                    $dates[$k2] += $jobs[$v->sij_noid.'_'.$v->sig_noid.'_'.$k2];
                                }

                                // $dates[$v->sips_plandate] += $jobs[$v->sij_noid.'_'.$v->sig_noid.'_'.$v->sips_plandate];
                                // $dates[$v->sips_plandate.'_'.$v->sips_actualdate] += $jobs[$v->sij_noid.'_'.$v->sig_noid.'_'.$k2];

                                // dd($jobs);
                                // $coba += $jobs[$v->sij_noid.'_'.$v->sig_noid.'_'.$v->sips_plandate];
                                // echo $v->sips_plandate.'_'.$v->sips_actualdate.' = '.$jobs[$v->sij_noid.'_'.$v->sig_noid.'_'.$k2].'<br>';
                                // echo 'asd<br>';
                            }

                            // dd($jobs[$v->sij_noid.'_'.$v->sig_noid.'_'.$k2]);
                            // dd($jobs[$v->sij_noid.'_'.$v->sig_noid.'_'.$v->sips_plandate.'_'.$v->sips_actualdate]);
                            // dd($k2);
                            // foreach ($v2 as $k3 => $v3) {
                            //     if ($k3 == $v->sig_noid) {
                            //         $event->sheet->setCellValue($this->nextChar('I',$i).$row, MyHelper::currencyConv($v->sips_plantotal))->getStyle($this->nextChar('I',$i).$row)->applyFromArray([
                            //             'alignment' => [ 'horizontal' => 'right' ]
                            //         ]);
                            //     }
                            // }

                        }

                        // COLORING
                        $event->sheet->getStyle($this->nextChar('I',$i).$row)->applyFromArray([
                            'fill' => [ 'fillType' => 'solid', 'color' => [ 'rgb' => strpos($k2, '_') ? 'e2e285' : 'a7c3d6' ] ]
                        ]);

                        $i++;
                        // dd($row);
                        // dd($dates[$v->sips_plandate]);
                    }
                    // dd($dates[$v->plan]);
                    // dd($dates);

                    $row++;
                }


                $i = 0;
                $accumulationplan = 0;
                $accumulationactual = 0;
                foreach ($dates as $k => $v) {
                    $event->sheet->setCellValue($this->nextChar('I',$i).$row, $v == 0 ? '-' : MyHelper::currencyConv($v))->getStyle($this->nextChar('I',$i).$row)->applyFromArray([
                        'alignment' => [ 'horizontal' => 'right' ],
                        'fill' => [ 'fillType' => 'solid', 'color' => [ 'rgb' => 'f4a927' ] ]
                    ]);
                    
                    // ACCUMULATION
                    if (!strpos($k, '_')) {
                        // ACCUMULATION PLAN
                        $accumulationplan += $v;
                        $event->sheet->setCellValue($this->nextChar('I',$i).($row+1), MyHelper::currencyConv($accumulationplan));
                    } else {
                        // ACCUMULATION ACTUAL
                        $accumulationactual += $v;
                        $event->sheet->setCellValue($this->nextChar('I',$i).($row+2), MyHelper::currencyConv($accumulationactual));
                    }


                    // COLORING ACCUMULATION
                    $event->sheet->getStyle($this->nextChar('I',$i).($row+1))->applyFromArray([
                        'alignment' => [ 'horizontal' => 'right' ],
                        'fill' => [ 'fillType' => 'solid', 'color' => [ 'rgb' => '87efff' ] ]
                    ]);
                    $event->sheet->getStyle($this->nextChar('I',$i).($row+2))->applyFromArray([
                        'alignment' => [ 'horizontal' => 'right' ],
                        'fill' => [ 'fillType' => 'solid', 'color' => [ 'rgb' => 'fff882' ] ]
                    ]);

                    $i++;
                }


                // dd($this->nextChar('I',5).'3');
                // dd($this->dates);
                $i = 0;
                $countdateperiode = 1;
                foreach ($this->dates as $k => $v) {
                    $event->sheet->setCellValue($this->nextChar('I',$i).'3', $countdateperiode)->getStyle($this->nextChar('I',$i).'3')->applyFromArray($baseheader);
                    $countdateperiode++;
                    $event->sheet->setCellValue($this->nextChar('I',$i).'4', MyHelper::dbdateToHumandate($k))->getStyle($this->nextChar('I',$i).'4')->applyFromArray([
                        'borders' => [
                            'outline' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000000'],
                            ],
                        ],
                        'font' => [
                            'bold' => true
                        ],
                        'color' => 'black',
                        'alignment' => [
                            'horizontal' => 'center',
                            'vertical' => 'center'
                        ],
                        'fill' => [
                            'fillType' => 'solid',
                            'color' => [
                                'rgb' => 'a7c3d6'
                            ]
                        ]
                    ]);
                    $i++;

                    foreach ($v as $k2 => $v2) {
                        if (!is_null($k2)) {
                            $event->sheet->setCellValue($this->nextChar('I',$i).'4', MyHelper::dbdateToHumandate($k2) == '' ? '-' : MyHelper::dbdateToHumandate($k2))->getStyle($this->nextChar('I',$i).'4')->applyFromArray([
                                'borders' => [
                                    'outline' => [
                                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                        'color' => ['argb' => '000000'],
                                    ],
                                ],
                                'font' => [
                                    'bold' => true
                                ],
                                'color' => 'black',
                                'alignment' => [
                                    'horizontal' => 'center',
                                    'vertical' => 'center'
                                ],
                                'fill' => [
                                    'fillType' => 'solid',
                                    'color' => [
                                        'rgb' => 'e2e285'
                                    ]
                                ]
                            ]);
                            $i++;
                        }
                    }
                    
                    
                    // $event->sheet->setCellValue($this->nextChar('I',$k).$row, MyHelper::currencyConv($totalperdate[$v]))->getStyle($this->nextChar('I',$k).$row)->applyFromArray([
                    //     'alignment' => [ 'horizontal' => 'right' ]
                    // ]);
                }

                // LAST
                $event->sheet->setCellValue('G'.$row, 'Total');
                $event->sheet->setCellValue('H'.$row, MyHelper::currencyConv($total))->getStyle('H'.$row)->applyFromArray([
                    'alignment' => [ 'horizontal' => 'right' ]
                ]);
                $row++;
                $event->sheet->setCellValue('G'.$row, 'Accumulation Plan');
                $row++;
                $event->sheet->setCellValue('G'.$row, 'Accumulation Actual');
                $event->sheet->getStyle('B2:'.$this->nextChar('I',$i-1).$row)->applyFromArray($baseborder);
                
                
                // dd($this->dates);
                // $event->sheet->setCellValue('B3', 'Project Name')->getStyle('B3')->applyFromArray($basebold);
                // $event->sheet->setCellValue('C3', $this->master->projectname);
                // $event->sheet->setCellValue('B4', 'Client')->getStyle('B4')->applyFromArray($basebold);
                // $event->sheet->setCellValue('C4', $this->master->idcardcustomer_name);
                // $event->sheet->setCellValue('B5', 'RPQ Number')->getStyle('B5')->applyFromArray($basebold);
                // $event->sheet->setCellValue('C5', '-');
                // $event->sheet->setCellValue('B6', 'Date')->getStyle('B6')->applyFromArray($basebold);
                // $event->sheet->setCellValue('C6', MyHelper::dbdateToHumandate($this->master->tanggal));
                // $event->sheet->setCellValue('B7', 'Rev')->getStyle('B7')->applyFromArray($basebold);
                // $event->sheet->setCellValue('C7', '-');
                // $event->sheet->setCellValue('B9', 'No');
                // $event->sheet->setCellValue('C9', 'Description'); //BOLD
                // $event->sheet->setCellValue('D9', 'Vol');
                // $event->sheet->setCellValue('E9', 'Sat'); //BOLD
                // $event->sheet->setCellValue('F9', 'Durasi Pekerjaan');
                // $event->sheet->getStyle('B9:F9')->applyFromArray([
                //     'font' => [
                //         'bold' => true
                //     ],
                //     'fill' => [
                //         'fillType' => 'solid',
                //         'color' => [
                //             'rgb' => 'eda323'
                //         ]
                //     ]
                // ]);

                // SET DATES
                // $dates = $this->getDatesInRange(['date1'=>$this->master->tanggal,'date2'=>$this->master->tanggaldue,'type'=>'all']);

                // $weekto = 1;
                // $jobdates = [];
                // $saturdays = [];
                // $sundays = [];
                // $saturdaydates = [];
                // $sundaydates = [];
                // foreach ($dates as $k => $v) {
                //     $jobdates[date('Y-m-d', strtotime($v['date']))] = $this->nextChar('G',$k);

                //     if ($v['n'] == 6) {
                //         $saturdays[date('Y-m-d', strtotime($v['date']))] = $this->nextChar('G',$k);
                //         $saturdaydates[] = date('Y-m-d', strtotime($v['date']));
                //     }
                //     if ($v['n'] == 7) {
                //         $sundays[date('Y-m-d', strtotime($v['date']))] = $this->nextChar('G',$k);
                //         $sundaydates[] = date('Y-m-d', strtotime($v['date']));
                //     }

                //     if ($k == 0) {
                //         $firstdayofweek = $this->nextChar('G',$k).'8';
                //         $firstdayofmonth = $this->nextChar('G',$k).'7';
                //     }

                //     // Minggu ke
                //     if ($v['n'] == 7) {
                //         $lastdayofweek = $this->nextChar('G',$k).'8';
                //         $weekof = $firstdayofweek.':'.$lastdayofweek;
                //         $event->sheet->mergeCells($weekof)->getStyle($weekof)->applyFromArray([
                //             'borders' => [
                //                 'outline' => [
                //                     'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                //                     'color' => ['argb' => '000000'],
                //                 ],
                //             ],
                //             'alignment' => [
                //                 'horizontal' => 'center'
                //             ]
                //         ]);
                //         $event->sheet->setCellValue($firstdayofweek, "Minggu ke ".$weekto);
                //         if (count($dates) >= $k+1) {
                //             $firstdayofweek = $this->nextChar('G',$k+1).'8';
                //             $weekto++;
                //         }
                //     }

                //     // Whats month
                //     if ($v['d'] == 1 || count($dates) == $k+1) {
                //         $lastdayofmonth = count($dates) == $k+1
                //             ? $this->nextChar('G',$k).'7'
                //             : $this->nextChar('G',$k-1).'7';
                //         $monthof = $firstdayofmonth.':'.$lastdayofmonth;
                //         $event->sheet->mergeCells($monthof)->getStyle($monthof)->applyFromArray([
                //             'borders' => [
                //                 'outline' => [
                //                     'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                //                     'color' => ['argb' => '000000'],
                //                 ],
                //             ],
                //             'alignment' => [
                //                 'horizontal' => 'center'
                //             ]
                //         ]);
                //         $v['d'] == 1
                //             ? $event->sheet->setCellValue($firstdayofmonth, date('F Y', mktime(0,0,0,$dates[$k-1]['m'],$dates[$k-1]['d'],$dates[$k-1]['y'])))
                //             : $event->sheet->setCellValue($firstdayofmonth, date('F Y', mktime(0,0,0,$v['m'],$v['d'],$v['y'])));
                //         $firstdayofmonth = $this->nextChar('G',$k).'7';

                //         if (count($dates) == $k+1) {
                //             $event->sheet->setCellValue($this->nextChar('G',$k+1).'9', 'Remark')->getStyle($this->nextChar('G',$k+1).'9')->applyFromArray([
                //                 'fill' => [
                //                     'fillType' => 'solid',
                //                     'color' => [
                //                         'rgb' => 'eda323'
                //                     ]
                //                 ],
                //                 'borders' => [
                //                     'outline' => [
                //                         'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                //                         'color' => ['argb' => '000000'],
                //                     ],
                //                 ],
                //                 'alignment' => [
                //                     'horizontal' => 'center'
                //                 ]
                //             ]);
                //             // dd($this->nextChar('G',$k+1).'7');
                //         }
                //     }

                //     // Date Range
                //     $event->sheet->setCellValue($this->nextChar('G',$k).'9', $v['d'])->getStyle($this->nextChar('G',$k).'9', $v['d'])->applyFromArray([
                //         'fill' => [
                //             'fillType' => 'solid',
                //             'color' => [
                //                 'rgb' => 'eda323'
                //             ]
                //         ],
                //         'borders' => [
                //             'outline' => [
                //                 'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                //                 'color' => ['argb' => '000000'],
                //             ],
                //         ]
                //     ]);
                // }

                // //STYLES
                // $event->sheet->getStyle('B9')->applyFromArray($baseborder);
                // $event->sheet->getStyle('C9')->applyFromArray($baseborder);
                // $event->sheet->getStyle('D9')->applyFromArray($baseborder);
                // $event->sheet->getStyle('E9')->applyFromArray($baseborder);
                // $event->sheet->getStyle('F9')->applyFromArray($baseborder);
                // $event->sheet->getStyle('B9:F9')->applyFromArray($basebold);

                // $ismilestone = true;
                // $isheader = true;
                // $isdetail = true;
                // $istotaldetail = false;
                // $totaldetail = 0;
                // $col = 'B';
                // $row = 10;
                // $nomilestone = 0;
                // $noheader = 1;
                // $nodetail = 1;
                // $countmilestone = 0;
                // $counttotal = 0;

                // foreach ($this->milestones as $k => $v) {

                //     // SET WEEKEND
                //     $this->getWeekends([
                //         'saturdaydates' => $saturdaydates,
                //         'sundaydates' => $sundaydates,
                //         'saturdays' => $saturdays,
                //         'sundays' => $sundays,
                //         'event' => $event,
                //         'row' => $row
                //     ]);

                //     // GROUP
                //     if ($ismilestone) {
                //         if ($countmilestone > 0) {
                //             $lastrowmilestone = $row-1;
                //             $event->sheet->getStyle('B'.$firstrowmilestone.':B'.$lastrowmilestone)->applyFromArray($baseborder);
                //             $event->sheet->getStyle('C'.$firstrowmilestone.':C'.$lastrowmilestone)->applyFromArray($baseborder);
                //             $event->sheet->getStyle('D'.$firstrowmilestone.':D'.$lastrowmilestone)->applyFromArray($baseborder);
                //             $event->sheet->getStyle('E'.$firstrowmilestone.':E'.$lastrowmilestone)->applyFromArray($baseborder);
                //             $event->sheet->getStyle('F'.$firstrowmilestone.':F'.$lastrowmilestone)->applyFromArray($baseborder);
                //         }
                //         $countmilestone = 0;
                //         $countmilestone++;
                //         if ($countmilestone == 1) {
                //             $firstrowmilestone = $row;
                //         }

                //         //DATA
                //         $event->sheet->setCellValue($this->nextChar($col,0).$row, $this->nextChar('A',$nomilestone));
                //         $event->sheet->setCellValue($this->nextChar($col,1).$row, $v->sim_name);
                //         $event->sheet->getStyle($this->nextChar($col,0).$row.':'.$this->nextChar($col,4).$row)->applyFromArray([
                //             'font' => [
                //                 'bold' => true
                //             ],
                //             'fill' => [
                //                 'fillType' => 'solid',
                //                 'color' => [
                //                     'rgb' => 'e5b664'
                //                 ]
                //             ],
                //         ]);


                //         $isheader = true;
                //         $noheader = 1;
                //         $row++;
                //         $nomilestone++;
                //     }

                //     // // CHECK MILESTONE
                //     if ($k+2 <= count($this->milestones)) {
                //         if ($this->milestones[$k+1]->sim_noid == $v->sim_noid) {
                //             $ismilestone = false;
                //             $istotaldetail = false;
                //         } else {
                //             $ismilestone = true;
                //             $istotaldetail = true;
                //         }
                //     }
                //     if ($k+1 == count($this->milestones)) {
                //         $istotaldetail = true;
                //     }

                //     // // SUBGROUP
                //     if ($isheader) {
                //         // SET WEEKEND
                //         $this->getWeekends([
                //             'saturdaydates' => $saturdaydates,
                //             'sundaydates' => $sundaydates,
                //             'saturdays' => $saturdays,
                //             'sundays' => $sundays,
                //             'event' => $event,
                //             'row' => $row
                //         ]);

                //     //     //DATA
                //     //     // $event->sheet->setCellValue($this->nextChar($col,1).$row, $noheader);
                //         $event->sheet->setCellValue($this->nextChar($col,1).$row, $noheader.'. '.$v->sih_name);


                //         $row++;
                //         $noheader++;
                //         $nodetail = 1;
                //         $isdetail = true;
                //     }

                //     // CHECK HEADER
                //     if ($k+2 <= count($this->milestones)) {
                //         if ($this->milestones[$k+1]->sih_noid == $v->sih_noid) {
                //             $isheader = false;
                //         } else {
                //             $isheader = true;
                //         }
                //     }
                    

                //     // // DETAIL
                //     if ($isdetail && $v->sim_noid == $v->sih_idsalesinquirymilestone && $v->sih_noid == $v->sid_idsalesinquiryheader) {
                        
                //         // SET WEEKEND
                //         $this->getWeekends([
                //             'saturdaydates' => $saturdaydates,
                //             'sundaydates' => $sundaydates,
                //             'saturdays' => $saturdays,
                //             'sundays' => $sundays,
                //             'event' => $event,
                //             'row' => $row
                //         ]);

                //         // dd(MyHelper::nextChar(['char'=>$col,'next'=>$k]).$row);

                //         // SET JOBDATE
                //         $jobdate = $this->getDatesInRange(['date1'=>$v->sid_startdate,'date2'=>$v->sid_stopdate,'type'=>'onlydate']);
                //         foreach ($jobdate as $k2 => $v2) {
                //             $event->sheet->getStyle($jobdates[$v2].$row)->applyFromArray([
                //                 'fill' => [
                //                     'fillType' => 'solid',
                //                     'color' => [
                //                         'rgb' => '3ac61d'
                //                     ],
                //                 ],
                //                 'borders' => [
                //                     'outline' => [
                //                         'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                //                         'color' => ['argb' => '000000'],
                //                     ],
                //                 ]
                //             ]);
                //             // dd($row);
                //             // dd($jobdates[$v2]);
                //         }


                //         $diffdatedetail = date_diff(date_create($v->sid_startdate), date_create($v->sid_stopdate))->d;
                //         // DATA
                //         $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>1]).$row, '          '.'-'.' '.$v->sid_name);                       
                //         $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>2]).$row, $v->sid_quantity);                       
                //         $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>3]).$row, $v->sid_idunit_name);   
                //         $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>4]).$row, $diffdatedetail);
                        
                //         $nodetail++;
                //         $row++;
                //     } else {
                //         $ismilestone = true;
                //         $isheader = true;
                //         $isdetail = false;
                //     }
                    
                //     if (count($this->milestones) == $k+1) {
                        
                //         $rowcounttotal = $row-1;
                //         $event->sheet->getStyle('B2:'.$this->nextChar('G',count($dates)).$rowcounttotal)->applyFromArray($baseborder);
                        
                //         // SET BORDER LAST MILESTONE
                //         $lastrowmilestone = $rowcounttotal;
                //         $event->sheet->getStyle('B'.$firstrowmilestone.':B'.$lastrowmilestone)->applyFromArray($baseborder);
                //         $event->sheet->getStyle('C'.$firstrowmilestone.':C'.$lastrowmilestone)->applyFromArray($baseborder);
                //         $event->sheet->getStyle('D'.$firstrowmilestone.':D'.$lastrowmilestone)->applyFromArray($baseborder);
                //         $event->sheet->getStyle('E'.$firstrowmilestone.':E'.$lastrowmilestone)->applyFromArray($baseborder);
                //         $event->sheet->getStyle('F'.$firstrowmilestone.':F'.$lastrowmilestone)->applyFromArray($baseborder);
                //     }
                // }
            },
            // AfterSheet::class => function(AfterSheet $event) {
            //     $event->sheet->getDelegate()->getStyle('I:M')->applyFromArray([
            //         'alignment' => [
            //             'horizontal' => 'right'
            //         ]
            //     ]);
            // },

        ];
    }
}