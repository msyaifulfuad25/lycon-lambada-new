<?php 
namespace App\Model\excel\exports;
 
use DB;
use App\Model\Purchase\ReportingPurchase AS ReportingPurchaseModel;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
// use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithEvents;
// use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadSheet\Style\NumberFormat;
use PhpOffice\PhpSpreadSheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use App\Helpers\MyHelper;
 
class SalesInquiryRAP implements WithEvents, ShouldAutoSize {

    use Exportable;

    protected $cellAfter = 18;

    public function __construct(array $data) {
        $this->master = $data['master'];
        $this->groups = $data['groups'];
        $this->inventorypergroup = $data['inventorypergroup'];
        $this->allgroups = $data['allgroups'];
        $this->actualinventory = $data['actualinventory'];
    }

    public function nextChar($char, $next) {
        return MyHelper::nextChar(['char'=>$char,'next'=>$next]);
    }

    public function registerEvents(): array {
        return [
            BeforeSheet::class => function (BeforeSheet $event) {
                $basebold = [
                    'font' => [
                        'bold' => true
                    ],
                    'color' => 'black'
                ];

                $baseheader = [
                    'borders' => [
                        'outline' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '000000'],
                        ],
                    ],
                    'font' => [
                        'bold' => true
                    ],
                    'color' => 'black',
                    'alignment' => [
                        'horizontal' => 'center',
                        'vertical' => 'center'
                    ]
                ];

                $basecenter = [
                    'alignment' => [ 'horizontal' => 'center' ]
                ];

                $baseright = [
                    'alignment' => [ 'horizontal' => 'right' ]
                ];

                // DATA
                $event->sheet->setCellValue('B2', 'Rencana Anggaran Project')->getStyle('B2')->applyFromArray([
                    'font' => [
                        'bold' => true,
                        'size' => 20
                    ],
                    'color' => 'black'
                ]);
                $event->sheet->setCellValue('B3', 'Project Name')->getStyle('B3')->applyFromArray($basebold);
                $event->sheet->setCellValue('C3', $this->master->projectname);
                $event->sheet->setCellValue('B4', 'Client')->getStyle('B4')->applyFromArray($basebold);
                $event->sheet->setCellValue('C4', $this->master->idcardcustomer_name);
                $event->sheet->setCellValue('B5', 'RPQ Number')->getStyle('B5')->applyFromArray($basebold);
                $event->sheet->setCellValue('C5', '-');
                $event->sheet->setCellValue('B6', 'Date')->getStyle('B6')->applyFromArray($basebold);
                $event->sheet->setCellValue('C6', MyHelper::dbdateToHumandate($this->master->tanggal));
                $event->sheet->setCellValue('B7', 'Rev')->getStyle('B7')->applyFromArray($basebold);
                $event->sheet->setCellValue('C7', '-');
                $event->sheet->setCellValue('B9', 'No')->getStyle('B9')->applyFromArray($baseheader);
                $event->sheet->setCellValue('C9', 'Description')->getStyle('C9')->applyFromArray($baseheader); //BOLD
                $event->sheet->setCellValue('F9', 'Qty')->getStyle('F9')->applyFromArray($baseheader);
                $event->sheet->setCellValue('H9', 'Unit')->getStyle('H9')->applyFromArray($baseheader); //BOLD
                $event->sheet->setCellValue('I9', 'Harga Satuan')->getStyle('I9')->applyFromArray($baseheader);
                $event->sheet->setCellValue('K9', 'Total Harga Plan')->getStyle('K9')->applyFromArray($baseheader); //BOLD
                $event->sheet->setCellValue('L9', 'Total Harga Actual')->getStyle('L9')->applyFromArray($baseheader); //BOLD
                $event->sheet->setCellValue('M9', 'Jumlah Harga Actual')->getStyle('M9')->applyFromArray($baseheader);
                $event->sheet->mergeCells('C9:E9');
                $event->sheet->mergeCells('F9:G9');
                $event->sheet->mergeCells('I9:J9');

                $baseborder = [
                    'borders' => [
                        'outline' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '000000'],
                        ],
                    ]
                ];

                //STYLES
                $event->sheet->getStyle('B9')->applyFromArray($baseborder);
                $event->sheet->getStyle('C9')->applyFromArray($baseborder);
                $event->sheet->getStyle('D9')->applyFromArray($baseborder);
                $event->sheet->getStyle('E9')->applyFromArray($baseborder);
                $event->sheet->getStyle('F9')->applyFromArray($baseborder);
                $event->sheet->getStyle('G9')->applyFromArray($baseborder);
                $event->sheet->getStyle('H9')->applyFromArray($baseborder);
                $event->sheet->getStyle('I9')->applyFromArray($baseborder);
                $event->sheet->getStyle('J9')->applyFromArray($baseborder);
                $event->sheet->getStyle('K9')->applyFromArray($baseborder);
                $event->sheet->getStyle('L9')->applyFromArray($baseborder);
                $event->sheet->getStyle('M9')->applyFromArray($baseborder);
                $event->sheet->getStyle('B9:M9')->applyFromArray([
                    'fill' => [
                        'fillType' => 'solid',
                        'color' => [
                            'rgb' => 'BFBFBF'
                        ]
                    ]
                ]);
                $event->sheet->getStyle('B9:M9')->applyFromArray($basebold);

                $isgroup = true;
                $issubgroup = true;
                $isdetail = true;
                $isplanpricetotal = false;
                $planpricetotal = 0;
                $actualpricesummary = 0;
                $col = 'B';
                $row = 10;
                $nogroup = 1;
                $nosubgroup = 1;
                $nodetail = 1;
                $countdetail = 0;
                $countplantotal = 0;
                $countactualtotal = 0;

                // dd($this->allgroups);
                foreach ($this->allgroups as $k => $v) {

                    // GROUP
                    if ($isgroup) {
                        //DATA
                        $event->sheet->setCellValue($this->nextChar($col,0).$row, $nogroup)->getStyle($this->nextChar($col,0).$row)->applyFromArray($basecenter);
                        $event->sheet->setCellValue($this->nextChar($col,1).$row, $v->sig_name);
                        $event->sheet->setCellValue($this->nextChar($col,4).$row, 'Plan')->getStyle($this->nextChar($col,4).$row)->applyFromArray($basecenter);
                        $event->sheet->setCellValue($this->nextChar($col,5).$row, 'Actual 1')->getStyle($this->nextChar($col,5).$row)->applyFromArray($basecenter);
                        $event->sheet->setCellValue($this->nextChar($col,6).$row, 'Unit')->getStyle($this->nextChar($col,6).$row)->applyFromArray($basecenter);
                        $event->sheet->setCellValue($this->nextChar($col,7).$row, 'Plan')->getStyle($this->nextChar($col,7).$row)->applyFromArray($basecenter);
                        $event->sheet->setCellValue($this->nextChar($col,8).$row, 'Actual 1')->getStyle($this->nextChar($col,8).$row)->applyFromArray($basecenter);
                        $event->sheet->setCellValue($this->nextChar($col,9).$row, 'Plan')->getStyle($this->nextChar($col,9).$row)->applyFromArray($basecenter);
                        $event->sheet->setCellValue($this->nextChar($col,10).$row, 'Actual 1')->getStyle($this->nextChar($col,10).$row)->applyFromArray($basecenter);
                        $event->sheet->setCellValue($this->nextChar($col,11).$row, 'Actual')->getStyle($this->nextChar($col,11).$row)->applyFromArray($basecenter);

                        // STYLES
                        $event->sheet->getStyle($this->nextChar($col,0).$row)->applyFromArray($baseborder);
                        $event->sheet->getStyle($this->nextChar($col,1).$row.':'.$this->nextChar($col,3).$row)->applyFromArray($baseborder);
                        $event->sheet->getStyle($this->nextChar($col,4).$row)->applyFromArray($baseborder);
                        $event->sheet->getStyle($this->nextChar($col,5).$row)->applyFromArray($baseborder);
                        $event->sheet->getStyle($this->nextChar($col,6).$row)->applyFromArray($baseborder);
                        $event->sheet->getStyle($this->nextChar($col,7).$row)->applyFromArray($baseborder);
                        $event->sheet->getStyle($this->nextChar($col,8).$row)->applyFromArray($baseborder);
                        $event->sheet->getStyle($this->nextChar($col,9).$row)->applyFromArray($baseborder);
                        $event->sheet->getStyle($this->nextChar($col,10).$row)->applyFromArray($baseborder);
                        $event->sheet->getStyle($this->nextChar($col,11).$row)->applyFromArray($baseborder);
                        $event->sheet->getStyle($this->nextChar($col,0).$row.':'.$this->nextChar($col,11).$row)->applyFromArray([
                            'fill' => [
                                'fillType' => 'solid',
                                'color' => [
                                    'rgb' => '95bcbc'
                                ]
                            ]
                        ]);


                        $issubgroup = true;
                        $nosubgroup = 1;
                        $row++;
                        $nogroup++;
                    }

                    // CHECK MILESTONE
                    if ($k+2 <= count($this->allgroups)) {
                        if ($this->allgroups[$k+1]->sig_idinventory == $v->sig_idinventory) {
                            $isgroup = false;
                            $isplanpricetotal = false;
                        } else {
                            $isgroup = true;
                            $isplanpricetotal = true;
                        }
                    }
                    if ($k+1 == count($this->allgroups)) {
                        $isplanpricetotal = true;
                    }


                    // SUBGROUP
                    if ($issubgroup) {
                        if ($countdetail > 0) {
                            $lastrowdetail = $row-3;
                            $lastrowtotal = $row-2;
                            // $event->sheet->getStyle('B'.$firstrowdetail.':D'.$lastrowdetail)->applyFromArray($baseborder);
                            // $event->sheet->getStyle('E'.$firstrowdetail.':E'.$lastrowdetail)->applyFromArray($baseborder);
                            // $event->sheet->getStyle('F'.$firstrowdetail.':F'.$lastrowdetail)->applyFromArray($baseborder);
                            // $event->sheet->getStyle('G'.$firstrowdetail.':G'.$lastrowdetail)->applyFromArray($baseborder);
                            // $event->sheet->getStyle('H'.$firstrowdetail.':H'.$lastrowdetail)->applyFromArray($baseborder);
                            // $event->sheet->getStyle('I'.$firstrowdetail.':I'.$lastrowdetail)->applyFromArray($baseborder);
                            // $event->sheet->getStyle('J'.$firstrowdetail.':J'.$lastrowdetail)->applyFromArray($baseborder);
                            // $event->sheet->getStyle('K'.$firstrowdetail.':K'.$lastrowdetail)->applyFromArray($baseborder);
                            // $event->sheet->getStyle('L'.$firstrowdetail.':L'.$lastrowdetail)->applyFromArray($baseborder);
                            // $event->sheet->getStyle('M'.$firstrowdetail.':M'.$lastrowdetail)->applyFromArray($baseborder);
                            
                            // $event->sheet->getStyle('B'.$lastrowtotal.':J'.$lastrowtotal)->applyFromArray($baseborder);
                            // $event->sheet->getStyle('K'.$lastrowtotal)->applyFromArray($baseborder);
                            // $event->sheet->getStyle('L'.$lastrowtotal)->applyFromArray($baseborder);
                            // $event->sheet->getStyle('M'.$lastrowtotal)->applyFromArray($baseborder);
                            
                            // $event->sheet->getStyle('B'.$lastrowtotal.':M'.$lastrowtotal)->applyFromArray([
                            //     'font' => [
                            //         'bold' => true,
                            //         'color' => [
                            //             'rgb' => 'ff0000'
                            //         ]
                            //     ]
                            // ]);
                        }
                        $countdetail = 0;
                        //DATA
                        // $event->sheet->setCellValue($this->nextChar($col,1).$row, $noheader);
                        $event->sheet->setCellValue($this->nextChar($col,2).$row, $v->sis_name);

                        // STYLES
                        $event->sheet->getStyle($this->nextChar($col,0).$row.':'.$this->nextChar($col,1).$row)->applyFromArray($baseborder);
                        $event->sheet->getStyle($this->nextChar($col,2).$row.':'.$this->nextChar($col,11).$row)->applyFromArray($baseborder);
                        $event->sheet->getStyle($this->nextChar($col,0).$row.':'.$this->nextChar($col,11).$row)->applyFromArray([
                            'fill' => [
                                'fillType' => 'solid',
                                'color' => [
                                    'rgb' => 'b8d8d8'
                                ],
                            ]
                        ]);


                        $row++;
                        $nosubgroup++;
                        $nodetail = 1;
                        $isdetail = true;
                    }

                    // CHECK HEADER
                    if ($k+2 <= count($this->allgroups)) {
                        if ($this->allgroups[$k+1]->sis_noid == $v->sis_noid) {
                            $issubgroup = false;
                        } else {
                            $issubgroup = true;
                        }
                    }



                    // DETAIL
                    if ($isdetail && $v->sig_noid == $v->sis_idsalesinquirygroup && $v->sis_noid == $v->sii_idsalesinquirysubgroup) {
                        $countdetail++;
                        if ($countdetail == 1) {
                            $firstrowdetail = $row;
                        }
                        // DATA
                        $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>2]).$row, $nodetail);                       
                        $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>3]).$row, $v->sii_name.($v->isplan ? '' : ' (Tidak direncanakan)'));                       
                        $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>4]).$row, ($v->isplan ? $v->sii_quantity : '-'))->getStyle(MyHelper::nextChar(['char'=>$col,'next'=>4]).$row)->applyFromArray($basecenter);
                        $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>6]).$row, $v->idunit_name);                       
                        $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>7]).$row, ($v->isplan ? MyHelper::currencyConv($v->sii_offerprice) : '-'))->getStyle(MyHelper::nextChar(['char'=>$col,'next'=>7]).$row)->applyFromArray($baseright);                       
                        $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>9]).$row, ($v->isplan ? MyHelper::currencyConv($v->sii_quantity*$v->sii_offerprice) : '-'))->getStyle(MyHelper::nextChar(['char'=>$col,'next'=>9]).$row)->applyFromArray($baseright);
                        

                        // ACTUAL
                        // dd($this->actualinventory);
                        if (array_key_exists($v->sii_idinventory, $this->actualinventory)) {

                            $actualunitqty = '';
                            $actualunitprice = '';
                            $actualsubtotal = '';
                            $actualtotal = 0;
                            foreach ($this->actualinventory[$v->sii_idinventory] as $k2 => $v2) {
                                $actualunitqty = $actualunitqty == '' ? $v2->unitqty : $actualunitqty.'|'.$v2->unitqty;
                                $actualunitprice = $actualunitprice == '' ? MyHelper::currencyConv($v2->unitprice) : $actualunitprice.'|'.MyHelper::currencyConv($v2->unitprice);
                                $actualsubtotal = $actualsubtotal == '' ? MyHelper::currencyConv($v2->unitqty*$v2->unitprice) : $actualsubtotal.'|'.MyHelper::currencyConv($v2->unitqty*$v2->unitprice);
                                $actualtotal += ($v2->unitqty*$v2->unitprice);
                            }

                            $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>5]).$row, $actualunitqty)->getStyle(MyHelper::nextChar(['char'=>$col,'next'=>5]).$row)->applyFromArray($basecenter);
                            $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>8]).$row, $actualunitprice)->getStyle(MyHelper::nextChar(['char'=>$col,'next'=>8]).$row)->applyFromArray($baseright);
                            $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>10]).$row, $actualsubtotal)->getStyle(MyHelper::nextChar(['char'=>$col,'next'=>10]).$row)->applyFromArray($baseright);
                            $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>11]).$row, MyHelper::currencyConv($actualtotal))->getStyle(MyHelper::nextChar(['char'=>$col,'next'=>11]).$row)->applyFromArray($baseright);
                            
                            $actualpricesummary += $actualtotal;
                            // echo MyHelper::currencyConv($actualtotal).'<br>';
                            $countactualtotal += $actualtotal;
                            
                            // $this->actualinventory[$v->sii_noid]->unitqty = $v->sii_noid - $this->actualinventory[$v->sii_noid]->unitqty;
                        }
                        

                        // STYLES
                        $event->sheet->getStyle(MyHelper::nextChar(['char'=>$col,'next'=>5]).$row)->applyFromArray([
                            'fill' => [
                                'fillType' => 'solid',
                                'color' => [
                                    'rgb' => 'FDE9D9'
                                ],
                            ]
                        ]);
                        $event->sheet->getStyle(MyHelper::nextChar(['char'=>$col,'next'=>8]).$row)->applyFromArray([
                            'fill' => [
                                'fillType' => 'solid',
                                'color' => [
                                    'rgb' => 'FDE9D9'
                                ],
                            ]
                        ]);
                        $event->sheet->getStyle(MyHelper::nextChar(['char'=>$col,'next'=>10]).$row)->applyFromArray([
                            'fill' => [
                                'fillType' => 'solid',
                                'color' => [
                                    'rgb' => 'FDE9D9'
                                ],
                            ]
                        ]);


                        $planpricetotal += $v->sii_quantity*$v->sii_offerprice;
                        if ($v->sig_name != 'LAIN - LAIN') {
                            // echo MyHelper::currencyConv($v->sii_quantity*$v->sii_offerprice).'<br>';
                            $countplantotal += $v->sii_quantity*$v->sii_offerprice;
                        }
                        $nodetail++;
                        $row++;
                    } else {
                        $isgroup = true;
                        $issubgroup = true;
                        $isdetail = false;
                    }


                    // TOTAL DETAIL
                    if ($isplanpricetotal) {
                        $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>8]).$row, 'Jumlah'); 
                        $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>9]).$row, MyHelper::currencyConv($v->sig_name == 'LAIN - LAIN' ? 0 : $planpricetotal))->getStyle(MyHelper::nextChar(['char'=>$col,'next'=>9]).$row)->applyFromArray($baseright);
                        $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>11]).$row, MyHelper::currencyConv($actualpricesummary))->getStyle(MyHelper::nextChar(['char'=>$col,'next'=>11]).$row)->applyFromArray($baseright); 

                        $event->sheet->getStyle(MyHelper::nextChar(['char'=>$col,'next'=>0]).$row.':'.MyHelper::nextChar(['char'=>$col,'next'=>11]).$row)->applyFromArray([
                            'fill' => [
                                'fillType' => 'solid',
                                'color' => [
                                    'rgb' => 'FABF8F'
                                ],
                            ]
                        ]);


                        $isplanpricetotal = false;
                        $planpricetotal = 0;
                        $actualpricesummary = 0;
                        $row++;
                    }

                    if (count($this->allgroups) == $k+1) {
                        $lastrowdetail = $row-2;
                        $lastrowtotal = $row-1;
                        $rowsafetyfactor = $lastrowtotal+1;
                        $rowcountplantotal = $lastrowtotal+2;
                        // $event->sheet->getStyle('B'.$firstrowdetail.':D'.$lastrowdetail)->applyFromArray($baseborder);
                        // $event->sheet->getStyle('E'.$firstrowdetail.':E'.$lastrowdetail)->applyFromArray($baseborder);
                        // $event->sheet->getStyle('F'.$firstrowdetail.':F'.$lastrowdetail)->applyFromArray($baseborder);
                        // $event->sheet->getStyle('G'.$firstrowdetail.':G'.$lastrowdetail)->applyFromArray($baseborder);
                        // $event->sheet->getStyle('H'.$firstrowdetail.':H'.$lastrowdetail)->applyFromArray($baseborder);
                        // $event->sheet->getStyle('I'.$firstrowdetail.':I'.$lastrowdetail)->applyFromArray($baseborder);
                        // $event->sheet->getStyle('J'.$firstrowdetail.':J'.$lastrowdetail)->applyFromArray($baseborder);
                        // $event->sheet->getStyle('K'.$firstrowdetail.':K'.$lastrowdetail)->applyFromArray($baseborder);
                        // $event->sheet->getStyle('L'.$firstrowdetail.':L'.$lastrowdetail)->applyFromArray($baseborder);
                        // $event->sheet->getStyle('M'.$firstrowdetail.':M'.$lastrowdetail)->applyFromArray($baseborder);
                        
                        // $event->sheet->getStyle('B'.$lastrowtotal.':J'.$lastrowtotal)->applyFromArray($baseborder);
                        // $event->sheet->getStyle('K'.$lastrowtotal)->applyFromArray($baseborder);
                        // $event->sheet->getStyle('L'.$lastrowtotal)->applyFromArray($baseborder);
                        // $event->sheet->getStyle('M'.$lastrowtotal)->applyFromArray($baseborder);
                     
                        // $event->sheet->getStyle('B'.$lastrowtotal.':M'.$lastrowtotal)->applyFromArray([
                        //     'font' => [
                        //         'bold' => true,
                        //         'color' => [
                        //             'rgb' => 'ff0000'
                        //         ]
                        //     ]
                        // ]);
                    
                        $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>10]).$rowsafetyfactor, 'Safety Factor'); 
                        $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>11]).$rowsafetyfactor, '0%'); 
                        $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>8]).$rowcountplantotal, 'Jumlah Total'); 
                        $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>9]).$rowcountplantotal, MyHelper::currencyConv($countplantotal))->getStyle(MyHelper::nextChar(['char'=>$col,'next'=>9]).$rowcountplantotal)->applyFromArray($baseright);
                        $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>11]).$rowcountplantotal, MyHelper::currencyConv($countactualtotal))->getStyle(MyHelper::nextChar(['char'=>$col,'next'=>11]).$rowcountplantotal)->applyFromArray($baseright);
                        $event->sheet->getStyle('B'.$rowsafetyfactor.':M'.$rowsafetyfactor)->applyFromArray([
                            'font' => [
                                'bold' => true
                            ],
                            'fill' => [
                                'fillType' => 'solid',
                                'color' => [
                                    'rgb' => '8bb8dd'
                                ],
                            ]
                        ]);
                        $event->sheet->getStyle('B'.$rowsafetyfactor.':M'.$rowsafetyfactor)->applyFromArray($baseborder);
                        $event->sheet->getStyle('M'.$rowsafetyfactor)->applyFromArray($baseborder);
                        $event->sheet->getStyle('B'.$rowcountplantotal.':M'.$rowcountplantotal)->applyFromArray([
                            'font' => [
                                'size' => 20,
                                'bold' => true,
                                'color' => [
                                    'rgb' => 'ce3737'
                                ]
                            ],
                            'fill' => [
                                'fillType' => 'solid',
                                'color' => [
                                    'rgb' => '92dbb1'
                                ],
                            ]
                        ]);
                        $event->sheet->getStyle('B'.$rowcountplantotal.':K'.$rowcountplantotal)->applyFromArray($baseborder);
                        $event->sheet->getStyle('K'.$rowcountplantotal)->applyFromArray($baseborder);
                        $event->sheet->getStyle('L'.$rowcountplantotal)->applyFromArray($baseborder);
                        $event->sheet->getStyle('M'.$rowcountplantotal)->applyFromArray($baseborder);

                        $event->sheet->getStyle('B2:M'.$rowcountplantotal)->applyFromArray($baseborder);

                    
                    }
                }
                // die;
            },
            AfterSheet::class => function(AfterSheet $event) {
                // $event->sheet->getDelegate()->getStyle('F:G')->applyFromArray([
                //     'alignment' => [ 'horizontal' => 'center' ]
                // ]);
                // $event->sheet->getDelegate()->getStyle('I:M')->applyFromArray([
                //     'alignment' => [ 'horizontal' => 'right' ]
                // ]);
            },

        ];
    }
}