<?php 
namespace App\Model\excel\exports;
 
use DB;
use App\Model\Purchase\ReportingPurchase AS ReportingPurchaseModel;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithEvents;
// use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadSheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use App\Helpers\MyHelper;
 
class ReportingPurchase implements WithEvents, ShouldAutoSize {

    use Exportable;

    protected $cellAfter = 18;

    public function __construct(array $url) {
        // STYLES
        $this->basebold = [
            'font' => [ 'bold' => true ],
            'color' => 'black'
        ];

        $this->baseborder = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
            ]
        ];


        $this->from = MyHelper::humandateTodbdate($url[0]);
        $this->to = MyHelper::humandateTodbdate($url[1]);
        $this->idtypecostcontrol = $url[2];
        $this->idcostcontrol = $url[3];
        $this->idgroupinv = $url[4];
        $this->idgroupinv_nama = 'All';
        $this->notes = $url[5];
        $this->idcostcontrol_kode = '-';
        $this->idcostcontrol_projectname = '-';
        $this->idcostcontrol_projectlocation = '-';
        $this->idcardpj_nama = '-';
        
        if ($url[2] != '' && $url[3] != '') {
            $result = DB::connection('mysql2')
                ->table('salesorder AS so')
                ->select([
                    'so.kode AS kode',
                    'so.projectname AS projectname',
                    'so.projectlocation AS projectlocation',
                    'mc.nama AS idcardpj_nama',
                ])
                ->leftJoin('mcard AS mc', 'mc.noid', '=', 'so.idcardpj')
                ->where(function($query) use($url) {
                    if ($url[2] != '') { $query->where('so.idtypecostcontrol', $url[2]); }
                    if ($url[3] != '') { $query->where('so.noid', $url[3]); }
                })->first();

            $this->idcostcontrol_kode = $result->kode;
            $this->idcostcontrol_projectname = $result->projectname;
            $this->idcostcontrol_projectlocation = $result->projectlocation;
            $this->idcardpj_nama = $result->idcardpj_nama;
        }

        if ($url[4] != '') {
            $result2 = DB::connection('mysql2')
                ->table('invmgroup')
                ->where('noid', $url[4])
                ->first();
            $this->idgroupinv_nama = $result2->nama;
        }



        $filter = [
            'from' => $this->from,
            'to' => $this->to,
            'idtypecostcontrol' => $this->idtypecostcontrol,
            'idcostcontrol' => $this->idcostcontrol,
            'idgroupinv' => $this->idgroupinv,

        ];

        $resultmaster = DB::connection('mysql2')
            ->table('purchaserequestdetail AS prd')
            ->leftJoin('purchaserequest AS pr', 'pr.noid', '=', 'prd.idmaster')
            ->leftJoin('invmgroup AS img', 'img.noid', '=', 'prd.idgroupinv')
            ->select([
                'pr.noid',
                'prd.idmaster',
                'pr.tanggal',
                'pr.kode',
                'prd.namainventor',
                'img.nama AS groupinventor',
                'prd.unitqty',
                'prd.keterangan',
            ])
            ->where(function($query) use($filter) {
                $query->whereDate('pr.tanggal', '>=', date('Y-m-d', strtotime($filter['from'])));
                $query->whereDate('pr.tanggal', '<=', date('Y-m-d', strtotime($filter['to'])));
                if ($filter['idtypecostcontrol'] != '') { $query->where('pr.idtypecostcontrol', $filter['idtypecostcontrol']); }
                if ($filter['idcostcontrol'] != '') { $query->where('pr.idcostcontrol', $filter['idcostcontrol']); }
                if ($filter['idgroupinv'] != '') { $query->where('prd.idgroupinv', $filter['idgroupinv']); }
            })
            ->orderBy('pr.noid', 'ASC')
            ->get();

        $master = [];
        $checkmaster = [];
        foreach ($resultmaster as $k => $v) {
            $no = $k+1;

            $master[$k]['no'] = $no;

            if (in_array($v->idmaster, $checkmaster)) {
                $master[$k]['tanggal'] = '';
                $master[$k]['kode'] = '';
            } else {
                $checkmaster[] = $v->idmaster;
                $master[$k]['tanggal'] = $v->tanggal;
                $master[$k]['kode'] = $v->kode;
            }

            $master[$k]['namainventor'] = $v->namainventor;
            $master[$k]['groupinventor'] = $v->groupinventor;
            $master[$k]['unitqty'] = $v->unitqty;
            $master[$k]['keterangan'] = $v->keterangan;
            $master[$k] = (object)$master[$k];
        }

        $this->master = $master;

    }

    // public function columnWidths(): array {
    //     return [
    //         'A' => 25,
    //         'B' => 25,
    //         'E' => 25,
    //         'F' => 20,
    //         'I' => 15,
    //     ];
    // }

    // public function styles(Worksheet $sheet) {
    //     return [
    //         15 => ['font' => ['bold' => true]],
    //     ];
    // }

    public function registerEvents(): array {
        return [
            BeforeSheet::class => function (BeforeSheet $event) {
                $event->sheet->setCellValue('A1', '');
                $event->sheet->setCellValue('B2', 'PT. LYCON ASIA MANDIRI');
                $event->sheet->setCellValue('B3', 'Jemursari Utara II No: 30 Surabaya, East Java, Indonesia, 60237');
                $event->sheet->setCellValue('B4', 'Phone: (+62 31) 8474117-119');
                $event->sheet->setCellValue('C4', 'Fax: (+62 31) 8475920');
                $event->sheet->setCellValue('A5', '');
                $event->sheet->setCellValue('A6', '');
                $event->sheet->setCellValue('D7', 'REPORT PURCHASE')->getStyle('D7')->applyFromArray($this->basebold);
                $event->sheet->setCellValue('A8', '');
                $event->sheet->setCellValue('B9', 'SO Number')->getStyle('B9')->applyFromArray($this->basebold);
                $event->sheet->setCellValue('C9', $this->idcostcontrol_kode);
                $event->sheet->setCellValue('B10', 'Project Information:')->getStyle('B10')->applyFromArray($this->basebold);
                $event->sheet->setCellValue('B11', 'Project Name')->getStyle('B11')->applyFromArray($this->basebold);
                $event->sheet->setCellValue('C11', $this->idcostcontrol_projectname);
                $event->sheet->setCellValue('B12', 'Client')->getStyle('B12')->applyFromArray($this->basebold);
                $event->sheet->setCellValue('C12', 'PT. Pertamina');
                $event->sheet->setCellValue('E9', 'RFQ Number')->getStyle('E9')->applyFromArray($this->basebold);
                $event->sheet->setCellValue('E10', 'Site Address')->getStyle('E10')->applyFromArray($this->basebold);
                $event->sheet->setCellValue('E11', 'Project Manager')->getStyle('E11')->applyFromArray($this->basebold);
                $event->sheet->setCellValue('E12', 'Group Inventor')->getStyle('E12')->applyFromArray($this->basebold);
                $event->sheet->setCellValue('F9', '-');
                $event->sheet->setCellValue('F10', $this->idcostcontrol_projectlocation);
                $event->sheet->setCellValue('F11', $this->idcardpj_nama);
                $event->sheet->setCellValue('F12', $this->idgroupinv_nama);
                $event->sheet->setCellValue('H9', 'Project Periode')->getStyle('H9')->applyFromArray($this->basebold);
                $event->sheet->setCellValue('H10', 'SPK Number')->getStyle('H10')->applyFromArray($this->basebold);
                $event->sheet->setCellValue('H11', 'Rev')->getStyle('H11')->applyFromArray($this->basebold);
                $event->sheet->setCellValue('I9', 'from')->getStyle('I9')->applyFromArray($this->basebold);
                $event->sheet->setCellValue('J9', MyHelper::dbdateToHumandate($this->from));
                $event->sheet->setCellValue('K9', 'to')->getStyle('K9')->applyFromArray($this->basebold);
                $event->sheet->setCellValue('L9', MyHelper::dbdateToHumandate($this->to));
                $event->sheet->setCellValue('I10', '-');
                $event->sheet->setCellValue('I11', '-');
                // $event->sheet->setCellValue('A3', 'Phone: (+62 31) 8474117-119');
                // $event->sheet->setCellValue('B3', 'Fax: (+62 31) 8475920');
                // $event->sheet->setCellValue('G5', 'REPORT PURCHASE REQUEST');
                // $event->sheet->setCellValue('A1', 'Sales Order/Cost Center');
                // $event->sheet->setCellValue('B1', '21.PRO.059');
                // $event->sheet->setCellValue('A2', 'Group Inventor');
                // $event->sheet->setCellValue('B2', 'All');
                // $event->sheet->setCellValue('E1', 'Tanggal Mulai');
                // $event->sheet->setCellValue('F1', '2021-11-01');
                // $event->sheet->setCellValue('E2', 'Tanggal Selesai');
                // $event->sheet->setCellValue('F2', '2021-11-30');
                // $event->sheet->setCellValue('I1', 'Keterangan');
                // $event->sheet->setCellValue('J1', 'Sample reporting');
                // for ($i=1; $i<=100; $i++) {
                //     $event->sheet->setCellValue('A'.$i, '');
                // }

                $row = 15;
                $event->sheet->setCellValue(MyHelper::nextChar(['char'=>'A','next'=>0]).$row, 'No')->getStyle(MyHelper::nextChar(['char'=>'A','next'=>0]).$row)->applyFromArray(array_merge($this->basebold, $this->baseborder));
                $event->sheet->setCellValue(MyHelper::nextChar(['char'=>'A','next'=>1]).$row, 'Tanggal')->getStyle(MyHelper::nextChar(['char'=>'A','next'=>1]).$row)->applyFromArray(array_merge($this->basebold, $this->baseborder));
                $event->sheet->setCellValue(MyHelper::nextChar(['char'=>'A','next'=>2]).$row, 'Purchase')->getStyle(MyHelper::nextChar(['char'=>'A','next'=>2]).$row)->applyFromArray(array_merge($this->basebold, $this->baseborder));
                $event->sheet->setCellValue(MyHelper::nextChar(['char'=>'A','next'=>3]).$row, 'Deskripsi')->getStyle(MyHelper::nextChar(['char'=>'A','next'=>3]).$row)->applyFromArray(array_merge($this->basebold, $this->baseborder));
                $event->sheet->setCellValue(MyHelper::nextChar(['char'=>'A','next'=>4]).$row, 'Group Inventor')->getStyle(MyHelper::nextChar(['char'=>'A','next'=>4]).$row)->applyFromArray(array_merge($this->basebold, $this->baseborder));
                $event->sheet->setCellValue(MyHelper::nextChar(['char'=>'A','next'=>5]).$row, 'Qty')->getStyle(MyHelper::nextChar(['char'=>'A','next'=>5]).$row)->applyFromArray(array_merge($this->basebold, $this->baseborder));
                $event->sheet->setCellValue(MyHelper::nextChar(['char'=>'A','next'=>6]).$row, 'Keterangan')->getStyle(MyHelper::nextChar(['char'=>'A','next'=>6]).$row)->applyFromArray(array_merge($this->basebold, $this->baseborder));
                $row++;

                foreach ($this->master as $k => $v) {
                    $event->sheet->setCellValue(MyHelper::nextChar(['char'=>'A','next'=>0]).$row, $v->no)->getStyle(MyHelper::nextChar(['char'=>'A','next'=>0]).$row)->applyFromArray($this->baseborder);
                    $event->sheet->setCellValue(MyHelper::nextChar(['char'=>'A','next'=>1]).$row, MyHelper::dbdateToHumandate($v->tanggal))->getStyle(MyHelper::nextChar(['char'=>'A','next'=>1]).$row)->applyFromArray($this->baseborder);
                    $event->sheet->setCellValue(MyHelper::nextChar(['char'=>'A','next'=>2]).$row, $v->kode)->getStyle(MyHelper::nextChar(['char'=>'A','next'=>2]).$row)->applyFromArray($this->baseborder);
                    $event->sheet->setCellValue(MyHelper::nextChar(['char'=>'A','next'=>3]).$row, $v->namainventor)->getStyle(MyHelper::nextChar(['char'=>'A','next'=>3]).$row)->applyFromArray($this->baseborder);
                    $event->sheet->setCellValue(MyHelper::nextChar(['char'=>'A','next'=>4]).$row, $v->groupinventor)->getStyle(MyHelper::nextChar(['char'=>'A','next'=>4]).$row)->applyFromArray($this->baseborder);
                    $event->sheet->setCellValue(MyHelper::nextChar(['char'=>'A','next'=>5]).$row, $v->unitqty)->getStyle(MyHelper::nextChar(['char'=>'A','next'=>5]).$row)->applyFromArray($this->baseborder);
                    $event->sheet->setCellValue(MyHelper::nextChar(['char'=>'A','next'=>6]).$row, $v->keterangan)->getStyle(MyHelper::nextChar(['char'=>'A','next'=>6]).$row)->applyFromArray($this->baseborder);
                    $row++;
                }

                $event->sheet->setCellValue('B'.($row+1), 'Notes:')->getStyle('B'.($row+1))->applyFromArray($this->basebold);
                $event->sheet->setCellValue('B'.($row+2), $this->notes);
                // $event->sheet->insertNewRowBefore(13, 2);
                // $event->sheet->insertNewColumnBefore('A', 0);
            },
            AfterSheet::class => function (AfterSheet $event) {
                // $this->cellAfter += count($this->getData());
                // $event->sheet->setCellValue('B'.$this->cellAfter, 'Notes:')->getStyle('B'.$this->cellAfter)->applyFromArray($this->basebold);
                // $event->sheet->setCellValue('B'.($this->cellAfter+1), $this->notes);
            }

        ];
    }


    // public function map($user): array {
    //     return [
    //         $user->no,
    //         $user->tanggal,
    //         $user->kode,
    //         $user->namainventor,
    //         $user->groupinventor,
    //         $user->unitqty,
    //         $user->keterangan,
    //     ];
    // }

    // public function columnFormats(): array {
    //     return [
    //         'B' => NumberFormat::FORMAT_DATE_DDMMYYYY
    //     ];
    // }

    // public function headings(): array {
    //     return[
    //         'No',
    //         'Tanggal',
    //         'Purchase Request',
    //         'Deskripsi',
    //         'Group Inventor',
    //         'Qty', 
    //         'Keterangan' 
    //     ];
    // } 

    // public function collection() {
    //     $master = $this->getData();

    //     $this->cellAfter += count($master);
    //     // dd(count($master));
    //     return collect($master);
    //     // return collect([]);
    // }

    // public function getData() {
    //     $filter = [
    //         'from' => $this->from,
    //         'to' => $this->to,
    //         'idtypecostcontrol' => $this->idtypecostcontrol,
    //         'idcostcontrol' => $this->idcostcontrol,
    //         'idgroupinv' => $this->idgroupinv,

    //     ];

    //     $resultmaster = DB::connection('mysql2')
    //         ->table('purchaserequestdetail AS prd')
    //         ->leftJoin('purchaserequest AS pr', 'pr.noid', '=', 'prd.idmaster')
    //         ->leftJoin('invmgroup AS img', 'img.noid', '=', 'prd.idgroupinv')
    //         ->select([
    //             'pr.noid',
    //             'prd.idmaster',
    //             'pr.tanggal',
    //             'pr.kode',
    //             'prd.namainventor',
    //             'img.nama AS groupinventor',
    //             'prd.unitqty',
    //             'prd.keterangan',
    //         ])
    //         ->where(function($query) use($filter) {
    //             $query->whereDate('pr.tanggal', '>=', date('Y-m-d', strtotime($filter['from'])));
    //             $query->whereDate('pr.tanggal', '<=', date('Y-m-d', strtotime($filter['to'])));
    //             if ($filter['idtypecostcontrol'] != '') { $query->where('pr.idtypecostcontrol', $filter['idtypecostcontrol']); }
    //             if ($filter['idcostcontrol'] != '') { $query->where('pr.idcostcontrol', $filter['idcostcontrol']); }
    //             if ($filter['idgroupinv'] != '') { $query->where('prd.idgroupinv', $filter['idgroupinv']); }
    //         })
    //         ->orderBy('pr.noid', 'ASC')
    //         ->get();

    //     $master = [];
    //     $checkmaster = [];
    //     foreach ($resultmaster as $k => $v) {
    //         $no = $k+1;

    //         $master[$k]['no'] = $no;

    //         if (in_array($v->idmaster, $checkmaster)) {
    //             $master[$k]['tanggal'] = '';
    //             $master[$k]['kode'] = '';
    //         } else {
    //             $checkmaster[] = $v->idmaster;
    //             $master[$k]['tanggal'] = $v->tanggal;
    //             $master[$k]['kode'] = $v->kode;
    //         }

    //         $master[$k]['namainventor'] = $v->namainventor;
    //         $master[$k]['groupinventor'] = $v->groupinventor;
    //         $master[$k]['unitqty'] = $v->unitqty;
    //         $master[$k]['keterangan'] = $v->keterangan;
    //         $master[$k] = (object)$master[$k];
    //     }
    //     dd($master);

    //     return $master;
    // }
}