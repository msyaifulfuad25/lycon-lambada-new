<?php 
namespace App\Model\excel\exports;
 
use DB;
use App\Model\Purchase\ReportingPurchase AS ReportingPurchaseModel;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithEvents;
// use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadSheet\Style\NumberFormat;
use PhpOffice\PhpSpreadSheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use App\Helpers\MyHelper;
 
class RAP implements WithMapping, WithStyles, WithEvents, ShouldAutoSize {

    use Exportable;

    protected $cellAfter = 18;

    public function __construct(array $url) {
        $noid = $url[1];

        $master = DB::connection('mysql2')
            ->table('prjmrap AS rap')
            ->select([
                'rap.*',
                'mc.nama AS idcardcustomer_nama'
            ])
            ->leftJoin('mcard AS mc', 'mc.noid', '=', 'rap.idcardcustomer')
            ->where('rap.noid', $noid)
            ->first();
        if (!$master) return ['success'=>false,'message'=>'Error get RAP Master'];
        $this->master = $master;

        $detail = DB::connection('mysql2')
            ->table('prjmrapmilestone AS mil')
            ->select([
                'mil.noid AS mil_noid',
                'mil.idmaster AS mil_idmaster',
                'mil.nama AS mil_nama',
                'mil.totalqty AS mil_totalqty',
                'mil.totalitem AS mil_totalitem',
                'mil.totalsubtotal AS mil_totalsubtotal',
                'mil.generaltotal AS mil_generaltotal',

                'hea.noid AS hea_noid',
                'hea.idmilestone AS hea_idmilestone',
                'hea.nama AS hea_nama',
                'hea.totalqty AS hea_totalqty',
                'hea.totalitem AS hea_totalitem',
                'hea.totalsubtotal AS hea_totalsubtotal',
                'hea.generaltotal AS hea_generaltotal',

                'det.noid AS det_noid',
                'det.idheader AS det_idheader',
                'det.namainventor AS det_namainventor',
                'ims.nama AS idsatuan_nama',
                'det.unitqty AS det_unitqty',
                'det.unitprice AS det_unitprice',
                'det.subtotal AS det_subtotal',
                'det.hargatotal AS det_hargatotal',
            ])
            ->leftJoin('prjmrapheader AS hea', 'hea.idmilestone', '=', 'mil.noid')
            ->leftJoin('prjmrapdetail AS det', 'det.idheader', '=', 'hea.noid')
            ->leftJoin('invmsatuan AS ims', 'ims.noid', '=', 'det.idsatuan')
            ->where('mil.idmaster', $noid)
            ->orderBy('mil.noid', 'ASC')
            ->orderBy('hea.noid', 'ASC')
            ->orderBy('det.noid', 'ASC')
            ->get();
        if (!$detail) return ['success'=>false,'message'=>'Error get RAP Milestone'];
        $this->detail = $detail;
    }

    // public function columnWidths(): array {
    //     return [
    //         'A' => 25,
    //         'B' => 25,
    //         'E' => 25,
    //         'F' => 20,
    //         'I' => 15,
    //     ];
    // }

    public function nextChar($char, $next) {
        return MyHelper::nextChar(['char'=>$char,'next'=>$next]);
    }

    public function styles(Worksheet $sheet) {
        return [
            'B2' => [
                'font' => [
                    'bold' => true,
                    'size' => 20
                ],
                'color' => 'black'
            ],
            'B3' => [
                'font' => [
                    'bold' => true,
                ],
                'color' => 'blue'
            ],
            'B4' => [
                'font' => [
                    'bold' => true,
                ],
                'color' => 'blue'
            ],
            'B5' => [
                'font' => [
                    'bold' => true,
                ],
                'color' => 'blue'
            ],
            'B6' => [
                'font' => [
                    'bold' => true,
                ],
                'color' => 'blue'
            ],
            'B7' => [
                'font' => [
                    'bold' => true,
                ],
                'color' => 'blue'
            ],
        ];
    }

    public function registerEvents(): array {
        $styleArray = [
            'font' => [
                'bold' => true
            ]
        ];

        return [
            BeforeSheet::class => function (BeforeSheet $event) {
                // DATA
                $event->sheet->setCellValue('B2', 'Rencana Anggaran Project');
                $event->sheet->setCellValue('B3', 'Project Name');
                $event->sheet->setCellValue('C3', $this->master->projectname);
                $event->sheet->setCellValue('B4', 'Client');
                $event->sheet->setCellValue('C4', $this->master->idcardcustomer_nama);
                $event->sheet->setCellValue('B5', 'RPQ Number');
                $event->sheet->setCellValue('C5', '-');
                $event->sheet->setCellValue('B6', 'Date');
                $event->sheet->setCellValue('C6', MyHelper::dbdateTohumandate($this->master->tanggal));
                $event->sheet->setCellValue('B7', 'Rev');
                $event->sheet->setCellValue('C7', '-');
                $event->sheet->setCellValue('B9', 'No');
                $event->sheet->setCellValue('C9', 'Description'); //BOLD
                $event->sheet->setCellValue('F9', 'Qty');
                $event->sheet->setCellValue('H9', 'Unit'); //BOLD
                $event->sheet->setCellValue('I9', 'Harga Satuan');
                $event->sheet->setCellValue('J9', 'Total Harga Plan'); //BOLD
                $event->sheet->setCellValue('L9', 'Total Harga Actual'); //BOLD
                $event->sheet->setCellValue('M9', 'Jumlah Harga Actual');

                //STYLES
                $event->sheet->getStyle('B9:M9')->getFill()
                    ->setFillType(Fill::FILL_SOLID)
                    ->getStartColor()->setRGB('BFBFBF');
                $event->sheet->getStyle('B9:M9')->getFont()->setBold(true);

                $ismilestone = true;
                $isheader = true;
                $isdetail = true;
                $istotaldetail = false;
                $totaldetail = 0;
                $col = 'B';
                $row = 10;
                $nomilestone = 1;
                $noheader = 1;

                foreach ($this->detail as $k => $v) {

                    // MILESTONE
                    if ($ismilestone) {
                        //DATA
                        $event->sheet->setCellValue($this->nextChar($col,0).$row, $nomilestone);
                        $event->sheet->setCellValue($this->nextChar($col,1).$row, $v->mil_nama);
                        $event->sheet->setCellValue($this->nextChar($col,4).$row, 'Plan');
                        $event->sheet->setCellValue($this->nextChar($col,5).$row, 'Actual 1');
                        $event->sheet->setCellValue($this->nextChar($col,6).$row, 'Unit');
                        $event->sheet->setCellValue($this->nextChar($col,7).$row, 'Plan');
                        $event->sheet->setCellValue($this->nextChar($col,8).$row, 'Actual 1');
                        $event->sheet->setCellValue($this->nextChar($col,9).$row, 'Plan');
                        $event->sheet->setCellValue($this->nextChar($col,10).$row, 'Actual 1');
                        $event->sheet->setCellValue($this->nextChar($col,11).$row, 'Actual');

                        // STYLES
                        $event->sheet->getStyle($this->nextChar($col,0).$row.':'.$this->nextChar($col,11).$row)->getFill()
                            ->setFillType(Fill::FILL_SOLID)
                            ->getStartColor()->setRGB('6ac2d8');


                        $isheader = true;
                        $noheader = 1;
                        $row++;
                        $nomilestone++;
                    }

                    // CHECK MILESTONE
                    if ($k+2 <= count($this->detail)) {
                        if ($this->detail[$k+1]->mil_noid == $v->mil_noid) {
                            $ismilestone = false;
                            $istotaldetail = false;
                        } else {
                            $ismilestone = true;
                            $istotaldetail = true;
                        }
                    }
                    if ($k+1 == count($this->detail)) {
                        $istotaldetail = true;
                    }


                    // HEADER
                    if ($isheader) {
                        //DATA
                        // $event->sheet->setCellValue($this->nextChar($col,1).$row, $noheader);
                        $event->sheet->setCellValue($this->nextChar($col,2).$row, $v->hea_nama);

                        // STYLES
                        $event->sheet->getStyle($this->nextChar($col,0).$row.':'.$this->nextChar($col,11).$row)->getFill()
                            ->setFillType(Fill::FILL_SOLID)
                            ->getStartColor()->setRGB('cce0e5');


                        $row++;
                        $noheader++;
                        $isdetail = true;
                    }

                    // CHECK HEADER
                    if ($k+2 <= count($this->detail)) {
                        if ($this->detail[$k+1]->hea_noid == $v->hea_noid) {
                            $isheader = false;
                        } else {
                            $isheader = true;
                        }
                    }



                    // DETAIL
                    $no = 1;
                    if ($isdetail && $v->mil_noid == $v->hea_idmilestone && $v->hea_noid == $v->det_idheader) {
                        // DATA
                        $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>2]).$row, $no);                       
                        $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>3]).$row, $v->det_namainventor);                       
                        $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>4]).$row, $v->det_unitqty);                       
                        $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>6]).$row, $v->idsatuan_nama);                       
                        $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>7]).$row, MyHelper::currencyConv($v->det_unitprice));                       
                        $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>9]).$row, MyHelper::currencyConv($v->det_hargatotal));                       
                        
                        // STYLES
                        $event->sheet->getStyle(MyHelper::nextChar(['char'=>$col,'next'=>5]).$row)->getFill()
                            ->setFillType(Fill::FILL_SOLID)
                            ->getStartColor()->setRGB('FDE9D9');
                        $event->sheet->getStyle(MyHelper::nextChar(['char'=>$col,'next'=>8]).$row)->getFill()
                            ->setFillType(Fill::FILL_SOLID)
                            ->getStartColor()->setRGB('FDE9D9');
                        $event->sheet->getStyle(MyHelper::nextChar(['char'=>$col,'next'=>10]).$row)->getFill()
                            ->setFillType(Fill::FILL_SOLID)
                            ->getStartColor()->setRGB('FDE9D9');


                        $totaldetail += $v->det_hargatotal;
                        $no++;
                        $row++;
                    } else {
                        $ismilestone = true;
                        $isheader = true;
                        $isdetail = false;
                    }


                    // TOTAL DETAIL
                    if ($istotaldetail) {
                        $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>8]).$row, 'Jumlah'); 
                        $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>9]).$row, MyHelper::currencyConv($totaldetail)); 

                        $event->sheet->getStyle(MyHelper::nextChar(['char'=>$col,'next'=>0]).$row.':'.MyHelper::nextChar(['char'=>$col,'next'=>11]).$row)->getFill()
                            ->setFillType(Fill::FILL_SOLID)
                            ->getStartColor()->setRGB('FABF8F');


                        $istotaldetail = false;
                        $totaldetail = 0;
                        $row++;
                    }
                }
                
                // $event->sheet->insertNewRowBefore(13, 2);
                // $event->sheet->insertNewColumnBefore('A', 0);
            },
            // AfterSheet::class => function(AfterSheet $event) {
            //     $event->sheet->getStyle('C7:G7')->getFill()
            //         ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            //         ->getStartColor()->setRGB('b7dee8');
            // },

        ];
    }


    public function map($user): array {
        return [
            $user->no,
            $user->tanggal,
            $user->kode,
            $user->namainventor,
            $user->groupinventor,
            $user->unitqty,
            $user->keterangan,
        ];
    }

    public function columnFormats(): array {
        return [
            'B' => NumberFormat::FORMAT_DATE_DDMMYYYY
        ];
    }

    // public function headings(): array {
    //     return[
    //         'No',
    //         'Tanggal',
    //         'Purchase Request',
    //         'Deskripsi',
    //         'Group Inventor',
    //         'Qty', 
    //         'Keterangan' 
    //     ];
    // } 

    // public function collection() {
    //     // $master = $this->getData();

    //     $this->cellAfter += count($master);
    //     // dd(count($master));
    //     return collect($master);
    //     // return collect([]);
    // }

    public function getData() {
        $filter = [
            'from' => $this->from,
            'to' => $this->to,
            'idtypecostcontrol' => $this->idtypecostcontrol,
            'idcostcontrol' => $this->idcostcontrol,
            'idgroupinv' => $this->idgroupinv,

        ];

        $resultmaster = DB::connection('mysql2')
            ->table('purchaserequestdetail AS prd')
            ->leftJoin('purchaserequest AS pr', 'pr.noid', '=', 'prd.idmaster')
            ->leftJoin('invmgroup AS img', 'img.noid', '=', 'prd.idgroupinv')
            ->select([
                'pr.noid',
                'prd.idmaster',
                'pr.tanggal',
                'pr.kode',
                'prd.namainventor',
                'img.nama AS groupinventor',
                'prd.unitqty',
                'prd.keterangan',
            ])
            ->where(function($query) use($filter) {
                $query->whereDate('pr.tanggal', '>=', date('Y-m-d', strtotime($filter['from'])));
                $query->whereDate('pr.tanggal', '<=', date('Y-m-d', strtotime($filter['to'])));
                if ($filter['idtypecostcontrol'] != '') { $query->where('pr.idtypecostcontrol', $filter['idtypecostcontrol']); }
                if ($filter['idcostcontrol'] != '') { $query->where('pr.idcostcontrol', $filter['idcostcontrol']); }
                if ($filter['idgroupinv'] != '') { $query->where('prd.idgroupinv', $filter['idgroupinv']); }
            })
            ->orderBy('pr.noid', 'ASC')
            ->get();

        $master = [];
        $checkmaster = [];
        foreach ($resultmaster as $k => $v) {
            $no = $k+1;

            $master[$k]['no'] = $no;

            if (in_array($v->idmaster, $checkmaster)) {
                $master[$k]['tanggal'] = '';
                $master[$k]['kode'] = '';
            } else {
                $checkmaster[] = $v->idmaster;
                $master[$k]['tanggal'] = $v->tanggal;
                $master[$k]['kode'] = $v->kode;
            }

            $master[$k]['namainventor'] = $v->namainventor;
            $master[$k]['groupinventor'] = $v->groupinventor;
            $master[$k]['unitqty'] = $v->unitqty;
            $master[$k]['keterangan'] = $v->keterangan;
            $master[$k] = (object)$master[$k];
        }

        return $master;
    }
}