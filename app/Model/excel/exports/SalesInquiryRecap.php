<?php 
namespace App\Model\excel\exports;
 
use DB;
use App\Model\Purchase\ReportingPurchase AS ReportingPurchaseModel;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
// use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithEvents;
// use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadSheet\Style\NumberFormat;
use PhpOffice\PhpSpreadSheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use App\Helpers\MyHelper;
 
class SalesInquiryRecap implements WithEvents, ShouldAutoSize {

    use Exportable;

    protected $cellAfter = 18;

    public function __construct(array $data) {
        $this->master = $data['master'];
        $this->milestones = $data['milestones'];
    }

    public function nextChar($char, $next) {
        return MyHelper::nextChar(['char'=>$char,'next'=>$next]);
    }

    public function getWeekends($params) {
        $saturdaydates = $params['saturdaydates'];
        $sundaydates = $params['sundaydates'];
        $saturdays = $params['saturdays'];
        $sundays = $params['sundays'];
        $event = $params['event'];
        $row = $params['row'];

        foreach ($saturdaydates as $k2 => $v2) {
            $event->sheet->getStyle($saturdays[$v2].$row)->applyFromArray([
                'fill' => [
                    'fillType' => 'solid',
                    'color' => [
                        'rgb' => 'bcb03e'
                    ],
                ],
            ]);
        }

        foreach ($sundaydates as $k2 => $v2) {
            $event->sheet->getStyle($sundays[$v2].$row)->applyFromArray([
                'fill' => [
                    'fillType' => 'solid',
                    'color' => [
                        'rgb' => 'a8711e'
                    ],
                ],
            ]);
        }
    }

    public function getDatesInRange($params) {
        $date1 = $params['date1'];
        $date2 = $params['date2'];
        $type = $params['type'];

        $startdate = strtotime($date1);
        $stopdate = strtotime($date2);
        $firststartdate = date(
            'j', 
            mktime(0,0,0,
                date('n', $startdate),
                date('j', $startdate),
                date('Y', $startdate)
            )
        );
        $laststartdate = date(
            't', 
            mktime(0,0,0,
                date('n', $startdate),
                date('j', $startdate),
                date('Y', $startdate)
            )
        );
        $resultdiffdate = date_diff(date_create($date1), date_create($date2));
        $diffmonth = $resultdiffdate->m+($resultdiffdate->y*12)+($resultdiffdate->d == 0 ? 0 : 1);
        
        $dates = [];
        $week = 0;
        $dayweek = 0;
        for ($h=0; $h<$diffmonth; $h++) {
            $firststartdate = $h > 0 ? 1 : $firststartdate;
            $laststartdate = date(
                't', 
                strtotime(
                    "+$h month",
                    mktime(0,0,0,
                        date('n', $startdate),
                        date('j', $startdate),
                        date('Y', $startdate)
                    )
                )
            );
            $year = date('Y', strtotime("+$h month", $startdate));
            $month = date('n', strtotime("+$h month", $startdate));

            for ($i=$firststartdate; $i<=$laststartdate; $i++) {
                if ($dayweek == 7) {
                    $dayweek = 0;
                    $week++;
                }
                // echo date('Y-n-j', strtotime("$year-$month-$i"))."<br>";
                if ($type == 'all') {
                    $dates[] = [
                        'date' => date('Y-n-j', strtotime("$year-$month-$i")),
                        'y' => $year,
                        'm' => $month,
                        'd' => $i,
                        'w' => $week,
                        'n' => date('N', mktime(0,0,0, $month, $i, $year))
                    ];
                } else if ($type == 'onlydate'){
                    $dates[] = date('Y-m-d', strtotime("$year-$month-$i"));
                }
                // $dates[] = $week;
                if ($date2 == date('Y-m-d', strtotime("$year-$month-$i"))) {
                    break;
                }

                $dayweek++;
            }
        }
        return $dates;
    }

    public function registerEvents(): array {
        return [
            BeforeSheet::class => function (BeforeSheet $event) {
                $basebold = [
                    'font' => [
                        'bold' => true
                    ],
                    'color' => 'black'
                ];

                $baseborder = [
                    'borders' => [
                        'outline' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '000000'],
                        ],
                    ]
                ];

                // DATA
                $event->sheet->mergeCells('A5:D5');
                $event->sheet->mergeCells('A6:D6');
                $event->sheet->getStyle("A5:D6")->applyFromArray($baseborder);
                $event->sheet->mergeCells('A8:D8');
                $event->sheet->mergeCells('A9:D9');
                $event->sheet->getStyle("A8:D9")->applyFromArray($baseborder);
                $event->sheet->setCellValue('A5', 'REKAPITULASI')->getStyle('A5')->applyFromArray([
                    'font' => [ 'bold' => true, 'size' => 20 ],
                    'alignment' => [ 'horizontal' => 'center' ]
                ]);
                $event->sheet->setCellValue('A6', 'Rencana Anggaran Biaya (RAB)')->getStyle('A6')->applyFromArray([
                    'font' => [ 'bold' => true, 'size' => 16 ],
                    'alignment' => [ 'horizontal' => 'center' ]
                ]);
                $event->sheet->setCellValue('A8', $this->master->projectname)->getStyle('A8')->applyFromArray([
                    'font' => [ 'bold' => true, 'italic' => true, 'size' => 16 ],
                    'alignment' => [ 'horizontal' => 'center' ]
                ]);
                $event->sheet->setCellValue('A9', $this->master->projectlocation)->getStyle('A9')->applyFromArray([
                    'font' => [ 'bold' => true, 'italic' => true, 'size' => 14 ],
                    'alignment' => [ 'horizontal' => 'center' ]
                ]);

                $event->sheet->setCellValue('A11', 'No.')->getStyle('A11')->applyFromArray([
                    'font' => [ 'bold' => true ],
                    'alignment' => [ 'horizontal' => 'center' ],
                    'borders' => [ 'outline' => [ 'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN, 'color' => ['argb' => '000000'], ], ],
                ]);
                $event->sheet->mergeCells('B11:C11');
                $event->sheet->setCellValue('B11', 'POKOK PEKERJAAN')->getStyle('B11:C11')->applyFromArray([
                    'font' => [ 'bold' => true ],
                    'alignment' => [ 'horizontal' => 'center' ],
                    'borders' => [ 'outline' => [ 'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN, 'color' => ['argb' => '000000'], ], ],
                ]);
                $event->sheet->setCellValue('D11', 'JUMLAH')->getStyle('D11')->applyFromArray([
                    'font' => [ 'bold' => true ],
                    'alignment' => [ 'horizontal' => 'center' ],
                    'borders' => [ 'outline' => [ 'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN, 'color' => ['argb' => '000000'], ], ],
                ]);


                // PLAN
                $event->sheet->setCellValue('A12', 'A')->getStyle('A12')->applyFromArray([
                    'font' => [ 'bold' => true ],
                    'alignment' => [ 'horizontal' => 'center' ],
                ]);
                $event->sheet->mergeCells('B12:C12');
                $event->sheet->setCellValue('B12', 'RENCANA ANGGARAN BIAYA (RAB) / NILAI KONTRAK')->getStyle('B12')->applyFromArray([
                    'font' => [ 'bold' => true ],
                ]);

                $row = 12;
                $currentmilestone = 0;
                $currentheader = 0;
                $currentdetail = 0;
                $countmilestone = 0;
                $currentplacetotalmilestone = 0;
                $totalmilestone = 0;
                $plantotal = 0;
                foreach ($this->milestones as $k => $v) {
                    if ($k == 0 || $v->sim_isshowreport && $v->sim_noid != $currentmilestone) {
                        // echo 'B'.$row.' '.$v->sim_name.'<br>';
                        $currentmilestone = $v->sim_noid;
                        $row++;
                        $romawi = MyHelper::getRomawi($countmilestone);
                        $event->sheet->setCellValue('B'.$row, $romawi)->getStyle('B'.$row)->applyFromArray([
                            'font' => [ 'bold' => true ],
                            'alignment' => [ 'horizontal' => 'center' ],
                        ]);
                        $event->sheet->setCellValue('C'.$row, $v->sim_name);
                        $countmilestone++;
                        if ($totalmilestone != 0) {
                            $event->sheet->setCellValue($currentplacetotalmilestone, MyHelper::currencyConv($totalmilestone))->getStyle($currentplacetotalmilestone)->applyFromArray([
                                'alignment' => [ 'horizontal' => 'right' ],
                            ]);
                        }
                        $currentplacetotalmilestone = 'D'.$row;
                        $totalmilestone = 0;
                    }

                    if ($v->sih_isshowreport && $v->sih_noid != $currentheader) {
                        $currentheader = $v->sih_noid;
                        $row++;
                        $event->sheet->setCellValue('C'.$row, '          '.$v->sih_name);
                        
                        if ($v->sid_isshowreport && $v->sid_noid != $currentdetail) {
                            $currentdetail = $v->sid_noid;
                            $row++;
                            $event->sheet->setCellValue('C'.$row, '                    '.$v->sid_name);
                        }
                    }


                    // COUNT PRICE
                    if ($v->sim_isshowreport) {
                        $totalmilestone += (int)$v->sid_planprice;
                        $plantotal += (int)$v->sid_planprice;
                    }

                    // LAST TOTAL MILESTONE
                    if (count($this->milestones) == $k+1) {
                        $event->sheet->setCellValue($currentplacetotalmilestone, MyHelper::currencyConv($totalmilestone))->getStyle($currentplacetotalmilestone)->applyFromArray([
                            'alignment' => [ 'horizontal' => 'right' ],
                        ]);
                    }
                }

                $row++;
                $event->sheet->setCellValue('C'.$row, 'GRAND TOTAL A')->getStyle('C'.$row)->applyFromArray([
                    'font' => [ 'bold' => true ],
                    'alignment' => [ 'horizontal' => 'right' ],
                ]);
                $event->sheet->setCellValue('D'.$row, MyHelper::currencyConv($plantotal))->getStyle('D'.$row)->applyFromArray([
                    'font' => [ 'bold' => true ],
                    'alignment' => [ 'horizontal' => 'right' ],
                ]);




                // ACTUAL
                $row++;
                $currentmilestone = 0;
                $currentheader = 0;
                $currentdetail = 0;
                $countmilestone = 0;
                $currentplacetotalmilestone = 0;
                $totalmilestone = 0;
                $actualtotal = 0;


                $event->sheet->setCellValue("A$row", 'B')->getStyle("A$row")->applyFromArray([
                    'font' => [ 'bold' => true ],
                    'alignment' => [ 'horizontal' => 'center' ],
                ]);
                $event->sheet->mergeCells("B$row:C$row");
                $event->sheet->setCellValue("B$row", 'RENCANA ANGGARAN PELAKSANAAN (RAP)')->getStyle("B$row")->applyFromArray([
                    'font' => [ 'bold' => true ],
                ]);
                

                foreach ($this->milestones as $k => $v) {
                    if ($k == 0 || $v->sim_isshowreport && $v->sim_noid != $currentmilestone) {
                        // echo 'B'.$row.' '.$v->sim_name.'<br>';
                        $currentmilestone = $v->sim_noid;
                        $row++;
                        $romawi = MyHelper::getRomawi($countmilestone);
                        $event->sheet->setCellValue('B'.$row, $romawi)->getStyle('B'.$row)->applyFromArray([
                            'font' => [ 'bold' => true ],
                            'alignment' => [ 'horizontal' => 'center' ],
                        ]);
                        $event->sheet->setCellValue('C'.$row, $v->sim_name);
                        $countmilestone++;
                        if ($totalmilestone != 0) {
                            $event->sheet->setCellValue($currentplacetotalmilestone, MyHelper::currencyConv($totalmilestone))->getStyle($currentplacetotalmilestone)->applyFromArray([
                                'alignment' => [ 'horizontal' => 'right' ],
                            ]);
                        }
                        $currentplacetotalmilestone = 'D'.$row;
                        $totalmilestone = 0;
                    }

                    if ($v->sih_isshowreport && $v->sih_noid != $currentheader) {
                        $currentheader = $v->sih_noid;
                        $row++;
                        $event->sheet->setCellValue('C'.$row, '          '.$v->sih_name);
                        
                        if ($v->sid_isshowreport && $v->sid_noid != $currentdetail) {
                            $currentdetail = $v->sid_noid;
                            $row++;
                            $event->sheet->setCellValue('C'.$row, '                    '.$v->sid_name);
                        }
                    }


                    // COUNT PRICE
                    if ($v->sim_isshowreport) {
                        $totalmilestone += (int)$v->sid_actualprice;
                        $actualtotal += (int)$v->sid_actualprice;
                    }

                    // LAST TOTAL MILESTONE
                    if (count($this->milestones) == $k+1) {
                        $event->sheet->setCellValue($currentplacetotalmilestone, MyHelper::currencyConv($totalmilestone))->getStyle($currentplacetotalmilestone)->applyFromArray([
                            'alignment' => [ 'horizontal' => 'right' ],
                        ]);
                    }
                }

                $row++;
                $event->sheet->setCellValue('C'.$row, 'GRAND TOTAL B')->getStyle('C'.$row)->applyFromArray([
                    'font' => [ 'bold' => true ],
                    'alignment' => [ 'horizontal' => 'right' ],
                ]);
                $event->sheet->setCellValue('D'.$row, MyHelper::currencyConv($actualtotal))->getStyle('D'.$row)->applyFromArray([
                    'font' => [ 'bold' => true ],
                    'alignment' => [ 'horizontal' => 'right' ],
                ]);


                //BORDER
                $event->sheet->getStyle("A12:A$row")->applyFromArray($baseborder);
                $event->sheet->getStyle("B12:C$row")->applyFromArray($baseborder);
                $event->sheet->getStyle("D12:D$row")->applyFromArray($baseborder);



                // MARGIN RP
                $row++;
                $event->sheet->mergeCells("A$row:C$row");
                $event->sheet->getStyle("A$row:C$row")->applyFromArray($baseborder);
                $event->sheet->setCellValue('C'.$row, 'Margin (Rp)')->getStyle('C'.$row)->applyFromArray([
                    'font' => [ 'bold' => true ],
                    'alignment' => [ 'horizontal' => 'right' ],
                ]);
                $event->sheet->setCellValue('D'.$row, MyHelper::currencyConv($plantotal-$actualtotal))->getStyle('D'.$row)->applyFromArray([
                    'font' => [ 'bold' => true ],
                    'alignment' => [ 'horizontal' => 'right' ],
                    'borders' => [ 'outline' => [ 'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN, 'color' => ['argb' => '000000'], ], ],
                ]);

                // MARGIN RP
                $row++;
                $event->sheet->mergeCells("A$row:C$row");
                $event->sheet->getStyle("A$row:C$row")->applyFromArray($baseborder);
                $event->sheet->setCellValue('C'.$row, 'Margin (%)')->getStyle('C'.$row)->applyFromArray([
                    'font' => [ 'bold' => true ],
                    'alignment' => [ 'horizontal' => 'right' ],
                ]);
                $event->sheet->setCellValue('D'.$row, @round(($plantotal-$actualtotal)/$plantotal*100, 2).' %')->getStyle('D'.$row)->applyFromArray([
                    'font' => [ 'bold' => true ],
                    'alignment' => [ 'horizontal' => 'right' ],
                    'borders' => [ 'outline' => [ 'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN, 'color' => ['argb' => '000000'], ], ],
                ]);


                // BORDER
                $event->sheet->getStyle("A5:D$row")->applyFromArray($baseborder);





                // die;
                // $event->sheet->setCellValue('B4', 'Client')->getStyle('B4')->applyFromArray($basebold);
                // $event->sheet->setCellValue('C4', $this->master->idcardcustomer_name);
                // $event->sheet->setCellValue('B5', 'RPQ Number')->getStyle('B5')->applyFromArray($basebold);
                // $event->sheet->setCellValue('C5', '-');
                // $event->sheet->setCellValue('B6', 'Date')->getStyle('B6')->applyFromArray($basebold);
                // $event->sheet->setCellValue('C6', MyHelper::dbdateToHumandate($this->master->tanggal));
                // $event->sheet->setCellValue('B7', 'Rev')->getStyle('B7')->applyFromArray($basebold);
                // $event->sheet->setCellValue('C7', '-');
                // $event->sheet->setCellValue('B9', 'No');
                // $event->sheet->setCellValue('C9', 'Description'); //BOLD
                // $event->sheet->setCellValue('D9', 'Vol');
                // $event->sheet->setCellValue('E9', 'Sat'); //BOLD
                // $event->sheet->setCellValue('F9', 'Durasi Pekerjaan');
                // $event->sheet->getStyle('B9:F9')->applyFromArray([
                //     'font' => [
                //         'bold' => true
                //     ],
                //     'fill' => [
                //         'fillType' => 'solid',
                //         'color' => [
                //             'rgb' => 'eda323'
                //         ]
                //     ]
                // ]);

                // SET DATES
                // $dates = $this->getDatesInRange(['date1'=>$this->master->tanggal,'date2'=>$this->master->tanggaldue,'type'=>'all']);

                // $weekto = 1;
                // $jobdates = [];
                // $saturdays = [];
                // $sundays = [];
                // $saturdaydates = [];
                // $sundaydates = [];
                // foreach ($dates as $k => $v) {
                //     $jobdates[date('Y-m-d', strtotime($v['date']))] = $this->nextChar('G',$k);

                //     if ($v['n'] == 6) {
                //         $saturdays[date('Y-m-d', strtotime($v['date']))] = $this->nextChar('G',$k);
                //         $saturdaydates[] = date('Y-m-d', strtotime($v['date']));
                //     }
                //     if ($v['n'] == 7) {
                //         $sundays[date('Y-m-d', strtotime($v['date']))] = $this->nextChar('G',$k);
                //         $sundaydates[] = date('Y-m-d', strtotime($v['date']));
                //     }

                //     if ($k == 0) {
                //         $firstdayofweek = $this->nextChar('G',$k).'8';
                //         $firstdayofmonth = $this->nextChar('G',$k).'7';
                //     }

                //     // Minggu ke
                //     if ($v['n'] == 7) {
                //         $lastdayofweek = $this->nextChar('G',$k).'8';
                //         $weekof = $firstdayofweek.':'.$lastdayofweek;
                //         $event->sheet->mergeCells($weekof)->getStyle($weekof)->applyFromArray([
                //             'borders' => [
                //                 'outline' => [
                //                     'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                //                     'color' => ['argb' => '000000'],
                //                 ],
                //             ],
                //             'alignment' => [
                //                 'horizontal' => 'center'
                //             ]
                //         ]);
                //         $event->sheet->setCellValue($firstdayofweek, "Minggu ke ".$weekto);
                //         if (count($dates) >= $k+1) {
                //             $firstdayofweek = $this->nextChar('G',$k+1).'8';
                //             $weekto++;
                //         }
                //     }

                //     // Whats month
                //     if ($v['d'] == 1 || count($dates) == $k+1) {
                //         $lastdayofmonth = count($dates) == $k+1
                //             ? $this->nextChar('G',$k).'7'
                //             : $this->nextChar('G',$k-1).'7';
                //         $monthof = $firstdayofmonth.':'.$lastdayofmonth;
                //         $event->sheet->mergeCells($monthof)->getStyle($monthof)->applyFromArray([
                //             'borders' => [
                //                 'outline' => [
                //                     'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                //                     'color' => ['argb' => '000000'],
                //                 ],
                //             ],
                //             'alignment' => [
                //                 'horizontal' => 'center'
                //             ]
                //         ]);
                //         $v['d'] == 1
                //             ? $event->sheet->setCellValue($firstdayofmonth, date('F Y', mktime(0,0,0,$dates[$k-1]['m'],$dates[$k-1]['d'],$dates[$k-1]['y'])))
                //             : $event->sheet->setCellValue($firstdayofmonth, date('F Y', mktime(0,0,0,$v['m'],$v['d'],$v['y'])));
                //         $firstdayofmonth = $this->nextChar('G',$k).'7';

                //         if (count($dates) == $k+1) {
                //             $event->sheet->setCellValue($this->nextChar('G',$k+1).'9', 'Remark')->getStyle($this->nextChar('G',$k+1).'9')->applyFromArray([
                //                 'fill' => [
                //                     'fillType' => 'solid',
                //                     'color' => [
                //                         'rgb' => 'eda323'
                //                     ]
                //                 ],
                //                 'borders' => [
                //                     'outline' => [
                //                         'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                //                         'color' => ['argb' => '000000'],
                //                     ],
                //                 ],
                //                 'alignment' => [
                //                     'horizontal' => 'center'
                //                 ]
                //             ]);
                //             // dd($this->nextChar('G',$k+1).'7');
                //         }
                //     }

                //     // Date Range
                //     $event->sheet->setCellValue($this->nextChar('G',$k).'9', $v['d'])->getStyle($this->nextChar('G',$k).'9', $v['d'])->applyFromArray([
                //         'fill' => [
                //             'fillType' => 'solid',
                //             'color' => [
                //                 'rgb' => 'eda323'
                //             ]
                //         ],
                //         'borders' => [
                //             'outline' => [
                //                 'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                //                 'color' => ['argb' => '000000'],
                //             ],
                //         ]
                //     ]);
                // }

                // //STYLES
                // $event->sheet->getStyle('B9')->applyFromArray($baseborder);
                // $event->sheet->getStyle('C9')->applyFromArray($baseborder);
                // $event->sheet->getStyle('D9')->applyFromArray($baseborder);
                // $event->sheet->getStyle('E9')->applyFromArray($baseborder);
                // $event->sheet->getStyle('F9')->applyFromArray($baseborder);
                // $event->sheet->getStyle('B9:F9')->applyFromArray($basebold);

                // $ismilestone = true;
                // $isheader = true;
                // $isdetail = true;
                // $istotaldetail = false;
                // $totaldetail = 0;
                // $col = 'B';
                // $row = 10;
                // $nomilestone = 0;
                // $noheader = 1;
                // $nodetail = 1;
                // $countmilestone = 0;
                // $counttotal = 0;

                // foreach ($this->milestones as $k => $v) {

                //     // SET WEEKEND
                //     $this->getWeekends([
                //         'saturdaydates' => $saturdaydates,
                //         'sundaydates' => $sundaydates,
                //         'saturdays' => $saturdays,
                //         'sundays' => $sundays,
                //         'event' => $event,
                //         'row' => $row
                //     ]);

                //     // GROUP
                //     if ($ismilestone) {
                //         if ($countmilestone > 0) {
                //             $lastrowmilestone = $row-1;
                //             $event->sheet->getStyle('B'.$firstrowmilestone.':B'.$lastrowmilestone)->applyFromArray($baseborder);
                //             $event->sheet->getStyle('C'.$firstrowmilestone.':C'.$lastrowmilestone)->applyFromArray($baseborder);
                //             $event->sheet->getStyle('D'.$firstrowmilestone.':D'.$lastrowmilestone)->applyFromArray($baseborder);
                //             $event->sheet->getStyle('E'.$firstrowmilestone.':E'.$lastrowmilestone)->applyFromArray($baseborder);
                //             $event->sheet->getStyle('F'.$firstrowmilestone.':F'.$lastrowmilestone)->applyFromArray($baseborder);
                //         }
                //         $countmilestone = 0;
                //         $countmilestone++;
                //         if ($countmilestone == 1) {
                //             $firstrowmilestone = $row;
                //         }

                //         //DATA
                //         $event->sheet->setCellValue($this->nextChar($col,0).$row, $this->nextChar('A',$nomilestone));
                //         $event->sheet->setCellValue($this->nextChar($col,1).$row, $v->sim_name);
                //         $event->sheet->getStyle($this->nextChar($col,0).$row.':'.$this->nextChar($col,4).$row)->applyFromArray([
                //             'font' => [
                //                 'bold' => true
                //             ],
                //             'fill' => [
                //                 'fillType' => 'solid',
                //                 'color' => [
                //                     'rgb' => 'e5b664'
                //                 ]
                //             ],
                //         ]);


                //         $isheader = true;
                //         $noheader = 1;
                //         $row++;
                //         $nomilestone++;
                //     }

                //     // // CHECK MILESTONE
                //     if ($k+2 <= count($this->milestones)) {
                //         if ($this->milestones[$k+1]->sim_noid == $v->sim_noid) {
                //             $ismilestone = false;
                //             $istotaldetail = false;
                //         } else {
                //             $ismilestone = true;
                //             $istotaldetail = true;
                //         }
                //     }
                //     if ($k+1 == count($this->milestones)) {
                //         $istotaldetail = true;
                //     }

                //     // // SUBGROUP
                //     if ($isheader) {
                //         // SET WEEKEND
                //         $this->getWeekends([
                //             'saturdaydates' => $saturdaydates,
                //             'sundaydates' => $sundaydates,
                //             'saturdays' => $saturdays,
                //             'sundays' => $sundays,
                //             'event' => $event,
                //             'row' => $row
                //         ]);

                //     //     //DATA
                //     //     // $event->sheet->setCellValue($this->nextChar($col,1).$row, $noheader);
                //         $event->sheet->setCellValue($this->nextChar($col,1).$row, $noheader.'. '.$v->sih_name);


                //         $row++;
                //         $noheader++;
                //         $nodetail = 1;
                //         $isdetail = true;
                //     }

                //     // CHECK HEADER
                //     if ($k+2 <= count($this->milestones)) {
                //         if ($this->milestones[$k+1]->sih_noid == $v->sih_noid) {
                //             $isheader = false;
                //         } else {
                //             $isheader = true;
                //         }
                //     }
                    

                //     // // DETAIL
                //     if ($isdetail && $v->sim_noid == $v->sih_idsalesinquirymilestone && $v->sih_noid == $v->sid_idsalesinquiryheader) {
                        
                //         // SET WEEKEND
                //         $this->getWeekends([
                //             'saturdaydates' => $saturdaydates,
                //             'sundaydates' => $sundaydates,
                //             'saturdays' => $saturdays,
                //             'sundays' => $sundays,
                //             'event' => $event,
                //             'row' => $row
                //         ]);

                //         // dd(MyHelper::nextChar(['char'=>$col,'next'=>$k]).$row);

                //         // SET JOBDATE
                //         $jobdate = $this->getDatesInRange(['date1'=>$v->sid_startdate,'date2'=>$v->sid_stopdate,'type'=>'onlydate']);
                //         foreach ($jobdate as $k2 => $v2) {
                //             $event->sheet->getStyle($jobdates[$v2].$row)->applyFromArray([
                //                 'fill' => [
                //                     'fillType' => 'solid',
                //                     'color' => [
                //                         'rgb' => '3ac61d'
                //                     ],
                //                 ],
                //                 'borders' => [
                //                     'outline' => [
                //                         'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                //                         'color' => ['argb' => '000000'],
                //                     ],
                //                 ]
                //             ]);
                //             // dd($row);
                //             // dd($jobdates[$v2]);
                //         }


                //         $diffdatedetail = date_diff(date_create($v->sid_startdate), date_create($v->sid_stopdate))->d;
                //         // DATA
                //         $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>1]).$row, '          '.'-'.' '.$v->sid_name);                       
                //         $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>2]).$row, $v->sid_quantity);                       
                //         $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>3]).$row, $v->sid_idunit_name);   
                //         $event->sheet->setCellValue(MyHelper::nextChar(['char'=>$col,'next'=>4]).$row, $diffdatedetail);
                        
                //         $nodetail++;
                //         $row++;
                //     } else {
                //         $ismilestone = true;
                //         $isheader = true;
                //         $isdetail = false;
                //     }
                    
                //     if (count($this->milestones) == $k+1) {
                        
                //         $rowcounttotal = $row-1;
                //         $event->sheet->getStyle('B2:'.$this->nextChar('G',count($dates)).$rowcounttotal)->applyFromArray($baseborder);
                        
                //         // SET BORDER LAST MILESTONE
                //         $lastrowmilestone = $rowcounttotal;
                //         $event->sheet->getStyle('B'.$firstrowmilestone.':B'.$lastrowmilestone)->applyFromArray($baseborder);
                //         $event->sheet->getStyle('C'.$firstrowmilestone.':C'.$lastrowmilestone)->applyFromArray($baseborder);
                //         $event->sheet->getStyle('D'.$firstrowmilestone.':D'.$lastrowmilestone)->applyFromArray($baseborder);
                //         $event->sheet->getStyle('E'.$firstrowmilestone.':E'.$lastrowmilestone)->applyFromArray($baseborder);
                //         $event->sheet->getStyle('F'.$firstrowmilestone.':F'.$lastrowmilestone)->applyFromArray($baseborder);
                //     }
                // }
            },
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->getDelegate()->getStyle('I:M')->applyFromArray([
                    'alignment' => [
                        'horizontal' => 'right'
                    ]
                ]);
            },

        ];
    }
}