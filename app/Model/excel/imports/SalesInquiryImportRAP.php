<?php
 
namespace App\Model\excel\imports;
 
use Illuminate\Support\Facades\Session;
use App\Helpers\MyHelper;
use App\Model\Sales\SalesInquiry;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithMappedCells;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
 
class SalesInquiryImportRAP implements ToCollection {
   
    public function collection(Collection $rows) {
        $rap = [];
        $jobs = [];
        $groups = [];
        $subgroups = [];
        $inventories = [];
        $nextnoidjob = SalesInquiry::getNextNoid('salesinquiryjob');
        $nextnoidgroup = SalesInquiry::getNextNoid('salesinquirygroup');
        $nextnoidsubgroup = SalesInquiry::getNextNoid('salesinquirysubgroup');
        $nextnoidinventory = SalesInquiry::getNextNoid('salesinquiryinventory');
        $taxes  = SalesInquiry::getTax();

        // dd($rows);
        foreach ($rows as $k => $v) {
            if ($k > 2) {


                // JOB
                $job_id = $v[0]-1;
                if (!is_null($v[0])) {
                    $job_name = $v[1];
                    $job_unit = $v[2];
                    $job_quantity = $v[3];
                    $job_description = $v[4];
                    $job_subtotal = $v[5];
                    $idunit = explode(' | ', $job_unit);
                    $rap[$job_id] = [
                        'backColor' => '#a1b0c9',
                        'detail' => [
                            'idsatuan' => $idunit[0],
                            'idsatuan_nama' => $idunit[1],
                            'keterangan' => $job_description,
                            'namainventor' => $job_name,
                            'subtotal' => $job_subtotal,
                            'unitqty' => $job_quantity
                        ],
                        'id' => $job_id,
                        'name' => $job_name,
                        'nodes' => [],
                        'parent_id' => 0,
                        'selectable' => true,
                        'state' => [
                            'checked' => false,
                            'disabled' => false,
                            'expanded' => true,
                            'selected' => false
                        ],
                        'text' => $job_name.' [Estimasi=<b>$estimation_'.$job_id.'</b>][Total=<b>$total_'.$job_id.'</b>]
                                <button type="button" class="btn btn-danger btn-sm pull-right" onclick="removeJob({id: '.$job_id.'})"><i class="fa fa-trash"></i></button>
                                <button type="button" class="btn btn-warning btn-sm pull-right" onclick="showModalJob({type: \'update\', id: '.$job_id.'})"><i class="fa fa-edit"></i></button>
                                <button type="button" class="btn btn-info btn-sm pull-right" onclick="showModalGroup({type: \'view\', id: '.$job_id.'})"><i class="fa fa-eye"></i></button>
                                <button type="button" class="btn btn-primary btn-sm pull-right" onclick="showModalGroup({type: \'create\', parentid: '.$job_id.'})"><i class="fa fa-plus"></i>
                            </button>'
                    ];
                }


                // GROUP
                $group_id = explode('.', $v[7]);
                if (!is_null($v[7])) {
                    $group_job_id = $group_id[0]-1;
                    $group_group_id = $group_id[1]-1;
                    $group_name = explode(' | ', $v[8]);
                    $group_name_id = (int)$group_name[0];
                    $group_name_name = $group_name[1];
                    $group_description = $v[9];
                    $group_prdate = $v[10];
                    $group_reqpodate = $v[11];
                    $group_reqonsitedate = $v[12];
                    $rap[$group_job_id]['nodes'][$group_group_id] = [
                        'backColor' => '#95bcbc',
                        'detail' => [
                            'keterangan' => $group_description,
                            'kodeinventor' => $group_name_id,
                            'namainventor' => $group_name_name,
                            'prdate' => $group_prdate,
                            'purchasedates' => '',
                            'purchasedates_array' => [],
                            'purchasedates_array_setdates' => [],
                            'reqonsitedate' => $group_reqonsitedate,
                            'reqpodate' => $group_reqpodate,
                        ],
                        'id' => $group_group_id,
                        'name' => $group_name_name,
                        'nodes' => [],
                        'parent_id' => $group_job_id,
                        'selectable' => true,
                        'state' => [
                            'checked' => false,
                            'disabled' => false,
                            'expanded' => true,
                            'selected' => false
                        ],
                        'text' => $group_name_name.' [Total=<b>$total_'.$group_job_id.'_'.$group_group_id.'</b>]
                                <button type="button" class="btn btn-danger btn-sm pull-right" onclick="removeGroup({parentid: '.$group_job_id.', id: '.$group_group_id.'})"><i class="fa fa-trash"></i></button>
                                <button type="button" class="btn btn-warning btn-sm pull-right" onclick="showModalGroup({type: \'update\', parentid: '.$group_job_id.', id: '.$group_group_id.'})"><i class="fa fa-edit"></i></button>
                                <button type="button" class="btn btn-info btn-sm pull-right" onclick="showModalgroup({type: \'view\', parentid: '.$group_job_id.', id: '.$group_group_id.'})"><i class="fa fa-eye"></i></button>
                                <button type="button" class="btn btn-primary btn-sm pull-right" onclick="showModalSubgroup({type: \'create\', parentid: '.$group_job_id.', parentid2: '.$group_group_id.', idgroup: '.$group_name_id.'})"><i class="fa fa-plus"></i>
                            </button>'
                    ];
                }


                // SUBGROUP
                $subgroup_id = explode('.', $v[15]);
                if (!is_null($v[15])) {
                    $subgroup_job_id = $subgroup_id[0]-1;
                    $subgroup_group_id = $subgroup_id[1]-1;
                    $subgroup_subgroup_id = $subgroup_id[2]-1;
                    $subgroup_name = explode(' | ', $v[16]);
                    $subgroup_name_id = $subgroup_name[0];
                    $subgroup_name_name = $subgroup_name[1];
                    $subgroup_description = $v[17];
                    $rap[$subgroup_job_id]['nodes'][$subgroup_group_id]['nodes'][$subgroup_subgroup_id] = [
                        'backColor' => '#b8d8d8',
                        'detail' => [
                            'keterangan' => $subgroup_description,
                            'kodeinventor' => $subgroup_name_id,
                            'namainventor' => $subgroup_name_name,
                        ],
                        'id' => $subgroup_subgroup_id,
                        'name' => $subgroup_name_name,
                        'nodes' => [],
                        'parent_id' => $subgroup_group_id,
                        'selectable' => true,
                        'state' => [
                            'checked' => false,
                            'disabled' => false,
                            'expanded' => true,
                            'selected' => false
                        ],
                        'text' => $subgroup_name_name.' [Total=$total_'.$subgroup_job_id.'_'.$subgroup_group_id.'_'.$subgroup_subgroup_id.']
                            <button type="button" class="btn btn-danger btn-sm pull-right" onclick="removeSubgroup({parentid: '.$subgroup_job_id.', parentid2: '.$subgroup_group_id.', id: '.$subgroup_subgroup_id.'})"><i class="fa fa-trash"></i></button>
                            <button type="button" class="btn btn-warning btn-sm pull-right" onclick="showModalSubgroup({type: \'update\', parentid: '.$subgroup_job_id.', parentid2: '.$subgroup_group_id.', id: '.$subgroup_subgroup_id.', idgroup: '.$group_name_id.'})"><i class="fa fa-edit"></i></button>
                            <button type="button" class="btn btn-info btn-sm pull-right" onclick="showModalSubgroup({type: \'view\', parentid: '.$subgroup_job_id.', parentid2: '.$subgroup_group_id.', id: '.$subgroup_subgroup_id.', idgroup: '.$group_name_id.'})"><i class="fa fa-eye"></i></button>
                            <button type="button" class="btn btn-primary btn-sm pull-right" onclick="showModalInventory({type: \'create\', parentid: '.$subgroup_job_id.', parentid2: '.$subgroup_group_id.', parentid3: '.$subgroup_subgroup_id.'})"><i class="fa fa-plus"></i></button>'
                    ];
                }


                // INVENTORY
                $inventory_id = explode('.', $v[19]);
                if (!is_null($v[19])) {
                    $inventory_job_id = $inventory_id[0]-1;
                    $inventory_group_id = $inventory_id[1]-1;
                    $inventory_subgroup_id = $inventory_id[2]-1;
                    $inventory_inventory_id = $inventory_id[3];
                    $inventory_name = explode(' | ', $v[20]);
                    $inventory_name_id = $inventory_name[0];
                    $inventory_name_name = $inventory_name[1];
                    $inventory_alias = is_null($v[21]) ? $inventory_name_name : $v[21];
                    $inventory_description = $v[22];
                    $inventory_unit = explode(' | ', $v[23]);
                    $inventory_unit_id = $inventory_unit[0];
                    $inventory_unit_name = $inventory_unit[1];
                    $inventory_quantity = $v[24];
                    $inventory_buyprice = $v[25];
                    $inventory_baseprice = $v[26];
                    $inventory_offerprice = $v[27];
                    $inventory_subtotal = $inventory_quantity*$inventory_offerprice;
                    $inventory_originaltotal = 0;
                    $inventory_tax = explode(' | ', $v[29]);
                    $inventory_tax_id = $inventory_tax[0];
                    $inventory_tax_tax = 0;
                    $inventory_total = 0;
                    foreach ($taxes as $k2 => $v2) {
                        if ($inventory_tax[0] == $v2->noid) {
                            if ($v2->operation == '+') {
                                $inventory_tax_tax = $inventory_subtotal+$inventory_subtotal*$v2->prosentax/100;
                                $inventory_total = $inventory_subtotal+$inventory_tax_tax;
                            } else if ($v2->operation == '-') {
                                $inventory_tax_tax = $inventory_subtotal-$inventory_subtotal*$v2->prosentax/100;
                                $inventory_total = $inventory_subtotal-$inventory_tax_tax;
                            }
                        }
                    }
                    
                    $rap[$inventory_job_id]['nodes'][$inventory_group_id]['nodes'][$inventory_subgroup_id]['nodes'][$inventory_inventory_id] = [
                        'backColor' => '#e5e5e5',
                        'detail' => [
                            'alias' => $inventory_alias,
                            'baseprice' => $inventory_baseprice,
                            'buyprice' => $inventory_buyprice,
                            'description' => $inventory_description,
                            'idinventory' => $inventory_name_id,
                            'idtax' => $inventory_tax_id,
                            'idunit' => $inventory_unit_id,
                            'name' => $inventory_name_name,
                            'offerprice' => $inventory_offerprice,
                            'originaltotal' => 0,
                            'quantity' => $inventory_quantity,
                            'subtotal' => $inventory_subtotal,
                            'tax' => $inventory_tax_tax,
                            'total' => $inventory_total,
                        ],
                        'id' => $inventory_inventory_id,
                        'name' => $inventory_name_name,
                        'nodes' => [],
                        'parent_id' => $inventory_subgroup_id,
                        'selectable' => true,
                        'state' => [
                            'checked' => false,
                            'disabled' => false,
                            'expanded' => true,
                            'selected' => false
                        ],
                        'text' => ' '.$inventory_name_name.'
                            <div class="row">
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-2">
                                    <span>[Quantity='.$inventory_quantity.']</span>
                                </div>
                                <div class="col-md-6">

                                    <span>[Total='.MyHelper::currencyConv($inventory_total).']</span>
                                </div>
                                <div class="col-md-3 pr-0">
                                    <button type="button" class="btn btn-danger btn-sm pull-right" onclick="removeInventory({parentid: '.$inventory_job_id.', parentid2: '.$inventory_group_id.', parentid3: '.$inventory_subgroup_id.', id: '.$inventory_inventory_id.'})"><i class="fa fa-trash"></i></button>
                                    <button type="button" class="btn btn-warning btn-sm pull-right" onclick="showModalInventory({type: \'update\', parentid: '.$inventory_job_id.', parentid2: '.$inventory_group_id.', parentid3: '.$inventory_subgroup_id.', id: '.$inventory_inventory_id.'})"><i class="fa fa-edit"></i></button>
                                    <button type="button" class="btn btn-info btn-sm pull-right" onclick="showModalInventory({type: \'view\', parentid: '.$inventory_job_id.', parentid2: '.$inventory_group_id.', parentid3: '.$inventory_subgroup_id.', id: '.$inventory_inventory_id.'})"><i class="fa fa-eye"></i></button>
                                </div>
                            </div>'
                    ];
                }
                // dd($rap);
                // if ($k == 11) {
                //     dd($v);
                // }
            }
        }
        // $this->save(['data' => $rap]);
        Session::put('salesinquiry_rap', json_encode($rap));
        // print_r('asd');
        // =Master.xlsx!unit
        // return $rap;
    }
    
    // public function save($params) {
    //     Session::put('salesinquiry_rap', json_encode($params['data']));
    // }
}