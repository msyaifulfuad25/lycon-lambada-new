<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Session;
Use DB;

class Menu extends Model {
	protected $connection = 'mysql';
	use Notifiable;
	protected $table = 'cmsmenu';
	
	public static function isRoot() {
		return Session::get('groupuser') == 0 ? true : false;
	}

	public static function getDataWithPrivilidge($params) {
        $table = $params['table'];
        $select = @$params['select'] ? $params['select'] : ['noid','nama'];
        $connection = @$params['connection'] ? $params['connection'] : 'mysql2';
        $isactive = @$params['isactive'] ? $params['isactive'] : false;
        $where = @$params['where'];
        if ($isactive) $select[] = 'isactive';

        $result = DB::connection($connection)
                    ->table($table)
                    ->select($select)
                    ->where(function($query) use ($isactive) {
                        if ($isactive) $query->where('isactive', 1);
                    })
                    ->where(function($query) use ($where) {
                        if ($where) {
                            foreach ($where as $k => $v) {
                                $query->where($v[0], $v[1], $v[2]);
                            }
                        }
                    })
                    ->orderBy('noid', 'asc')
                    ->get();

		$wherein = [];
		foreach ($result as $k => $v) {
			$wherein[$k] = $v->noid;
		}

		$mcardprivilidge = DB::connection('mysql2')
			->table('mcardprivilidge')
			->wherein('idmenu', $wherein)
			->where('idcard', Session::get('noid'))
			->get();
			
		$mcardprivilidge_isactive = [];
		foreach ($mcardprivilidge as $k => $v) {
			$mcardprivilidge_isactive[$v->idmenu] = $v->isactive;
		}

		$fixresult = [];
		foreach ($result as $k => $v) {
			if (!is_null(@$mcardprivilidge_isactive[$v->noid]) && $mcardprivilidge_isactive[$v->noid] == 0) {
			} else {
				$fixresult[$k] = $v;
			}
		}
                   
        return $fixresult;
    }
	
	public static  function getAll() {
		$users = DB::table('cmsmenu')
					->whereIn('menulevel', [1, 2])
					->where('isactive',1)
					->where('ispublic',1)
					->get();

		foreach($users as $menu) {
			$submenu = DB::table('cmsmenu')
						->whereIn('menulevel', [1, 2])
						->where('isactive',1)
						->where('idparent',$menu->noid)
						->groupBy('nourut')
						->groupBy('groupmenu')
						->get();
						
						if (empty($submenu)) {
							$menu->submenu = [];
						} else {
							$menu->submenu = $submenu;
						}
			$data[] = $menu;
		}

		return $data;
	}

	public static function getBreadcrumb($noid, $breadcrumb=[], $status=FALSE) {
		$result = Menu::where('noid', $noid)->orWhere('urlmasking', $noid)->first();

		if (!$status) {
			$breadcrumb = [
				'text' => [],
				'url' => [],
				'noid' => [],
			];
		}
		
		if (@$result->idparent)	 {
			$breadcrumb['text'][]  = $result->menucaption;
			$breadcrumb['url'][]  = $result->linkidpage;
			$breadcrumb['noid'][]  = $result->noid;

			return Menu::getBreadcrumb($result->idparent, $breadcrumb, TRUE);
		} else {
			return $breadcrumb;
		}
	}

	public static  function caribreadcrumb_backup($slug){
		// $breadcrumb[] = '';
		// dd($breadcrumb);
		$data = Menu::where('noid',$slug)->first();
		if ($data) {
			$breadcrumb['text'][]  = $data->menucaption;
			$breadcrumb['url'][]  = $data->menucaption;
		}

		$data2 = Menu::where('noid',$data->idparent)->first();
		if ($data2) {
			array_push($breadcrumb['text'], $data2->menucaption);
			array_push($breadcrumb['url'], $data2->noid);
		}

		$data3 = Menu::where('noid',$data2->idparent)->first();
		if ($data3) {
			array_push($breadcrumb['text'], $data3->menucaption);
			array_push($breadcrumb['url'], $data3->noid);
		}

		$data4 = Menu::where('noid',$data3->idparent)->first();
		if ($data4) {
			array_push($breadcrumb['text'], $data4->menucaption);
			array_push($breadcrumb['url'], $data4->noid);
		}

		krsort($breadcrumb);
		return $breadcrumb;
	}

	public static function getPrivilegeMenu($params) {
		$privilege = [];
		if (count($params['data']) > 0) {
			foreach ($params['data'] as $k => $v) {
				if (preg_match('{"id":"isenable","status":1}', $v->jsonprivilege)) {
					$privilege[] = $v->idmenu;
				}
			}
		}
		return $privilege;
	}

	public static  function getAllAfter() {
		$resultprivilege = DB::connection('mysql2')
			->table('mcardprivilidge')
			->select(['idmenu','jsonprivilege'])
			->where('idcard', Session::get('noid'))
			->where('jsonprivilege', '!=', null)
			->where('isactive', 1)
			->get();
		$privilege = static::getPrivilegeMenu(['data'=>$resultprivilege]);

		$menu = DB::table('cmsmenu')
			->whereIn('menulevel', [1, 2])
			->where('isactive',1)
			->orderBy('noid')
			->groupBy('nourut')
			->groupBy('groupmenu')
			->get();
		
		$data = [];
		foreach($menu as $k => $v) {
			if (in_array($v->noid, $privilege) || static::isRoot()) {
				$resultsubmenu = DB::table('cmsmenu')
					->whereIn('menulevel', [1, 2])
					->where('isactive',1)
					->where('idparent',$v->noid)
					->orderBy('noid')
					->groupBy('nourut')
					->groupBy('groupmenu')
					->get();
				
				$submenu = [];
				if (count($resultsubmenu) > 0) {
					foreach ($resultsubmenu as $k2 => $v2) {
						if (in_array($v2->noid, $privilege) || static::isRoot()) {
							$submenu[] = $v2;
						}
					}
					// dd($submenu);
				}
				$v->submenu = $submenu;

				// $v->submenu = static::getPrivilegeMenu(['data'=>$submenu]);
				// if (empty($resultsubmenu)) {
				// 	$v->submenu = [];
				// } else {
				// 	$v->submenu = $resultsubmenu;
				// }
	
				$data[] = $v;
			}
		}

		// dd($data);
		// echo json_encode($data);
		// die;

		return $data;
  	}

	public static  function getSidebar() {
		// SELECT * FROM cmsmenu WHERE menulevel IN (3,4) AND isactive = 1 AND ispublic = 1
		$users = DB::table('cmsmenu')
					->whereIn('menulevel', [3, 4])
					->where('isactive',1)
					->where('ispublic',1)
					->get();
		return $users;
	}

	public static function ambilQuery($dat) {
		$str1 = str_replace('"',' ',$dat);
		$programs=DB::connection('mysql2')->select($str1);
		// dd($programs);
		return $programs;
  	}

	public static function ambilQuery2($dat) {
		$str1 = str_replace('"',' ',$dat);
		$programs=DB::connection('mysql2')->select($str1);
		$nilai = $programs[0]->jumlahdata;
		$nilai2 = strlen($programs[0]->jumlahdata);

		if((int)$nilai>1000) {
			$x = round($nilai);
			$x_number_format = number_format($x);
			$x_array = explode(',', $x_number_format);
			$x_parts = array(' K', ' M', ' B', ' T');
			$x_count_parts = count($x_array) - 1;
			$x_display = $x;
			$x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
			$x_display .= $x_parts[$x_count_parts - 1];
			return $x_display;
		}
	}

	public static function getResultQuery($idconnection, $query) {
		$connection = 'mysql2';
		$result = DB::connection('mysql2')->select($query);
		if ($result[0]->jumlahdata != 0) {
			$round = round($result[0]->jumlahdata);
			$number_format = number_format($round);
			$nf = explode(',', $number_format);
			$symbol = array(' K', ' M', ' B', ' T');
			$count_nf = count($nf) - 1;
			$display = $nf[0] . ((int) @$nf[1][0] !== 0 ? '.' . @$nf[1][0] : '');
			$display .= @$symbol[$count_nf - 1];
			return $display;
		}
		return $result[0]->jumlahdata;
	}

	public static function cariWarna($nama) {
		if ($nama == 'yellow') {
			$warna = '#c49f47';
		} else if ($nama == 'blue') {
			$warna = '#3598dc';
		} else if ($nama == 'green') {
			$warna = '#26a69a';
		} else if ($nama == 'red') {
			$warna = '#cb5a5e';
		} else if ($nama == 'purple-plum') {
			$warna = '#8775a7';
		} else if ($nama == 'green-seagreen') {
			$warna = '#1BA39C';
		} else if ($nama == 'yellow-crusta') {
			$warna = '#f3c200';
		} else if ($nama == 'red-intense') {
			$warna = '#e35b5a';
		} else if ($nama == 'blue-chambray') {
			$warna = '#2C3E50';
		} else if ($nama == 'blue-madison') {
			$warna = '#578ebe';
		} else if ($nama == 'purple-wisteria') {
			$warna = '#9B59B6';
		} else if ($nama == 'red-flamingo') {
			$warna = '#EF4836';
		} else if ($nama == 'yellow-casablanca') {
			$warna = '#f2784b';
		} else {
			$warna = '#c49f47';
		}
		return $warna;
	}

	public static function getColor($name) {
		if ($name == 'blue-chambray') {
			$code = '#535D78';
		} else if ($name == 'green-seagreen') {
			$code = '#2e8b57';
		} else if ($name == 'yellow-crusta') {
			$code = '#E69138';
		} else if ($name == 'red-intense') {
			$code = '#82040C';
		} else if ($name == 'blue-ebonyclay') {
			$code = '#292f42';
		} else if ($name == 'green-haze') {
			$code = '#0B9A55';
		} else if ($name == 'purple-medium') {
			$code = '#9370db';
		} else {
			$code = 'black';
		}
		return $code;
	}

	public static function cariClassWarna($nama) {
		// dd($nama);
		if ($nama == 'yellow') {
			$warna = 'morekuningkus';
		} else if ($nama == 'blue') {
			$warna = 'morebiru';
		} else if ($nama == 'green') {
			$warna = 'morehijauawal';
		} else if ($nama == 'red') {
			$warna = 'moremerah';
		} else if ($nama == 'purple-plum') {
			$warna = 'moreungu';
		} else if ($nama == 'green-seagreen') {
			$warna = 'morehijau';
		} else if ($nama == 'yellow-crusta') {
			$warna = 'morekuningkus';
		} else if ($nama == 'red-intense') {
			$warna = 'moreredintense';
		} else if ($nama == 'blue-chambray') {
			$warna = 'chambray';
		} else if ($nama == 'blue-madison') {
			$warna = 'bluemadison';
		} else if ($nama == 'purple-wisteria') {
			$warna = 'purplewisteria';
		} else if ($nama == 'red-flamingo') {
			$warna = 'redflamingo';
		} else if ($nama == 'yellow-casablanca') {
			$warna = 'yellowcasablanca';
		} else{
			$warna = 'morebiru';
		}
		return $warna;
	}

	public static  function getSidebarLoginBaru($id) {
		$menu = DB::select("SELECT * FROM cmsmenu WHERE menulevel IN (3,4) AND isactive = 1 AND ispublic = 1 ORDER BY nourut, groupmenu");
		return $menu;
		// $sql = "SELECT * FROM cmsmenu WHERE menulevel IN (3,4) AND isactive = 1 AND ispublic = 1 ORDER BY nourut, groupmenu";
	}

	public static function getSidebarBeforeLogin($id) {
		return DB::select("SELECT * FROM cmsmenu WHERE menulevel IN (3,4) AND isactive = 1 AND ispublic = 1 ORDER BY nourut, groupmenu");
	}

	public static  function getSidebarLogin($id) {
		if (is_null($id)) return [];
		$dataini = DB::table('cmsmenu')
					->where('noid', $id)
					->first();
		$page = DB::table('cmspage')
					->where('noid', $dataini->linkidpage)
					->first();

		$dataku = DB::select("SELECT *
								FROM cmsmenu
								WHERE idparent = (
									SELECT idparent
									FROM cmsmenu
									WHERE noid = (
										SELECT idparent
										FROM cmsmenu
										WHERE (linkidpage = $page->noid) AND (menulevel =4)
										ORDER BY noid
										LIMIT 0, 1)
									LIMIT 0,1)
									Group By groupmenu
								ORDER BY groupmenu, nourut");
		return $dataku;
	}

	public static  function getSidebarLogin_backup($id) {
		$cmsmenu = DB::table('cmsmenu')
					->leftJoin('cmspage','cmspage.noid','=','cmsmenu.linkidpage')
					->where('cmsmenu.noid', $id)
					->first();

		$resultcmsmenu = DB::select("SELECT *
							FROM cmsmenu
							WHERE idparent = (
								SELECT idparent
								FROM cmsmenu
								WHERE noid = (
									SELECT idparent
									FROM cmsmenu
									WHERE (linkidpage = $cmsmenu->noid) AND (menulevel =4)
									ORDER BY noid
									LIMIT 0, 1)
								LIMIT 0,1)
								Group By groupmenu
							ORDER BY groupmenu, nourut");

		$wircm = [];
		foreach ($resultcmsmenu as $k => $v) {
			$wircm[] = $v->groupmenu;
		}
		
		$sub = DB::table('cmsmenu')
				->where('menulevel',3)
				->where('isactive',1)
				->whereIn('groupmenu',$wircm)
				->orderBy('nourut')
				->get();

		$rcm = [];
		$wircm2 = [];
		foreach ($sub as $k => $v) {
			$rcm[$v->noid] = $v;
			$wircm2[] = $v->idparent;
		}

		dd($wircm2);

		$result = [];
		foreach ($resultcmsmenu as $k => $v) {
			$sub = DB::table('cmsmenu')
					->where('menulevel',3)
					->where('isactive',1)
					->where('groupmenu',$v->groupmenu)
					->orderBy('nourut')->get();
			$v->sub = $sub;
			$result[] = $v;
		}
								
		return $result;
	}

	public static function getAllPanel($data) {
		$dataku = DB::select("SELECT
								a.noid AS panel_noid,
								a.idpage AS panel_idpage,
								a.idwidget AS panel_idwidget,
								a.idparentpagepanel AS panel_idparentpagepanel,
								a.idgridaction AS panel_idgridaction,
								a.nospan AS panel_nospan,
								a.nospanxs AS panel_nospanxs,
								a.norow AS panel_norow,
								a.nocol AS panel_nocol,
								a.widget_pull AS panel_widget_pull,
								a.panelheigth AS panel_panelheigth,
								a.isportletframe AS panel_isportletframe,
								a.isonetoone AS panel_isonetoone,
								a.isshowonedit AS panel_isshowonedit,
								a.istabbed AS panel_istabbed,
								a.tabcaption AS panel_tabcaption,
								a.dataparam AS panel_dataparam,
								a.panelconfig AS panel_panelconfig,
								a.istabpanel AS panel_istabpanel,
								a.tabpanelcaption AS panel_tabpanelcaption,
								b.noid AS widget_noid,
								b.idtypewidget AS widget_idtypewidget,
								b.kode AS widget_kode,
								b.widgetcaption AS widget_widgetcaption,
								b.widgetsubcaption AS widget_widgetsubcaption,
								b.maintable AS widget_maintable,
								b.keterangan AS widget_keterangan,
								b.idlevelsecurity AS widget_idlevelsecurity,
								b.panelcolor AS widget_panelcolor,
								b.widgeticon AS widget_widgeticon,
								b.usepanel AS widget_usepanel,
								b.panelclose AS widget_panelclose,
								b.panelconfig AS widget_panelconfig,
								b.panelcolapse AS widget_panelcolapse,
								b.panelrefresh AS widget_panelrefresh,
								b.moreidpage AS widget_moreidpage,
								b.dataurl AS widget_dataurl,
								b.dataquery AS widget_dataquery,
								b.widgetconfig AS widget_widgetconfig,
								b.queryjoin AS widget_queryjoin,
								b.querywhere AS widget_querywhere,
								b.queryorderby AS widget_queryorderby,
								b.querygroupby AS widget_querygroupby,
								b.querylimit AS widget_querylimit,
								b.configwidget AS widget_configwidget,
								b.viewtable AS widget_viewtable,
								b.idconnection AS widget_idconnection,
								b.queryselect AS widget_queryselect
							FROM
								cmspagepanel a
							LEFT JOIN cmswidget b ON a.idwidget = b.noid
							WHERE a.idpage = $data
							ORDER BY  a.noid,a.nocol,a.norow");
							// ORDER BY a.norow, a.nocol");
		return $dataku;
	}

	public static function getAllPanelCart($data) {
		$dataku = DB::select("
			SELECT
			a.noid AS panel_noid,
			a.idpage AS panel_idpage,
			a.idwidget AS panel_idwidget,
			a.idparentpagepanel AS panel_idparentpagepanel,
			a.idgridaction AS panel_idgridaction,
			a.nospan AS panel_nospan,
			a.nospanxs AS panel_nospanxs,
			a.norow AS panel_norow,
			a.nocol AS panel_nocol,
			a.widget_pull AS panel_widget_pull,
			a.panelheigth AS panel_panelheigth,
			a.isportletframe AS panel_isportletframe,
			a.isonetoone AS panel_isonetoone,
			a.isshowonedit AS panel_isshowonedit,
			a.istabbed AS panel_istabbed,
			a.tabcaption AS panel_tabcaption,
			a.dataparam AS panel_dataparam,
			a.panelconfig AS panel_panelconfig,
			a.istabpanel AS panel_istabpanel,
			a.tabpanelcaption AS panel_tabpanelcaption,
			b.noid AS widget_noid,
			b.idtypewidget AS widget_idtypewidget,
			b.kode AS widget_kode,
			b.widgetcaption AS widget_widgetcaption,
			b.widgetsubcaption AS widget_widgetsubcaption,
			b.maintable AS widget_maintable,
			b.keterangan AS widget_keterangan,
			b.idlevelsecurity AS widget_idlevelsecurity,
			b.panelcolor AS widget_panelcolor,
			b.widgeticon AS widget_widgeticon,
			b.usepanel AS widget_usepanel,
			b.panelclose AS widget_panelclose,
			b.panelconfig AS widget_panelconfig,
			b.panelcolapse AS widget_panelcolapse,
			b.panelrefresh AS widget_panelrefresh,
			b.moreidpage AS widget_moreidpage,
			b.dataurl AS widget_dataurl,
			b.dataquery AS widget_dataquery,
			b.widgetconfig AS widget_widgetconfig,
			b.queryjoin AS widget_queryjoin,
			b.querywhere AS widget_querywhere,
			b.queryorderby AS widget_queryorderby,
			b.querygroupby AS widget_querygroupby,
			b.querylimit AS widget_querylimit,
			b.configwidget AS widget_configwidget,
			b.viewtable AS widget_viewtable,
			b.idconnection AS widget_idconnection,
			b.queryselect AS widget_queryselect
			FROM
			cmspagepanel a
			LEFT JOIN cmswidget b ON a.idwidget = b.noid
			WHERE a.idwidget = $data
			ORDER BY a.norow, a.nocol
			limit 1 ");
		return $dataku;
	}

	public static function getAllPanelSlide($data) {
		$dataku = DB::select("
			SELECT
			a.noid AS panel_noid,
			a.idpage AS panel_idpage,
			a.idwidget AS panel_idwidget,
			a.idparentpagepanel AS panel_idparentpagepanel,
			a.idgridaction AS panel_idgridaction,
			a.nospan AS panel_nospan,
			a.nospanxs AS panel_nospanxs,
			a.norow AS panel_norow,
			a.nocol AS panel_nocol,
			a.widget_pull AS panel_widget_pull,
			a.panelheigth AS panel_panelheigth,
			a.isportletframe AS panel_isportletframe,
			a.isonetoone AS panel_isonetoone,
			a.isshowonedit AS panel_isshowonedit,
			a.istabbed AS panel_istabbed,
			a.tabcaption AS panel_tabcaption,
			a.dataparam AS panel_dataparam,
			a.panelconfig AS panel_panelconfig,
			a.istabpanel AS panel_istabpanel,
			a.tabpanelcaption AS panel_tabpanelcaption,
			b.noid AS widget_noid,
			b.idtypewidget AS widget_idtypewidget,
			b.kode AS widget_kode,
			b.widgetcaption AS widget_widgetcaption,
			b.widgetsubcaption AS widget_widgetsubcaption,
			b.maintable AS widget_maintable,
			b.keterangan AS widget_keterangan,
			b.idlevelsecurity AS widget_idlevelsecurity,
			b.panelcolor AS widget_panelcolor,
			b.widgeticon AS widget_widgeticon,
			b.usepanel AS widget_usepanel,
			b.panelclose AS widget_panelclose,
			b.panelconfig AS widget_panelconfig,
			b.panelcolapse AS widget_panelcolapse,
			b.panelrefresh AS widget_panelrefresh,
			b.moreidpage AS widget_moreidpage,
			b.dataurl AS widget_dataurl,
			b.dataquery AS widget_dataquery,
			b.widgetconfig AS widget_widgetconfig,
			b.queryjoin AS widget_queryjoin,
			b.querywhere AS widget_querywhere,
			b.queryorderby AS widget_queryorderby,
			b.querygroupby AS widget_querygroupby,
			b.querylimit AS widget_querylimit,
			b.configwidget AS widget_configwidget,
			b.viewtable AS widget_viewtable,
			b.idconnection AS widget_idconnection

			FROM
			cmspagepanel a
			LEFT JOIN cmswidget b ON a.idwidget = b.noid
			WHERE a.idwidget = $data
			ORDER BY a.norow, a.nocol ");
		return $dataku;
	}

	public static function getMenuLevel($params) {
		// dd($params);
		// dd($params['groupmenu']);
		$resultmenulevel = DB::table('cmsmenu')
			->where('menulevel', $params['menulevel'])
			->where('isactive', 1)
			->where(function($query) use($params) {
				if (@$params['idparent']) {
					$query->where('idparent', $params['idparent']);
				}
				if (@$params['groupmenu']) {
					$query->where('groupmenu', $params['groupmenu']);
				}
			});

		if (@$params['groupby']) {
			$resultmenulevel->groupBy('groupmenu');
		}
		$resultmenulevel->orderBy('nourut', 'ASC');
		$menulevel = $resultmenulevel->get();
		$ml = [];
		foreach ($menulevel as $k => $v) {
			$ml[] = $v->noid;
		}

		$rules = DB::connection('mysql2')
			->table('mcardprivilidge')
			->whereIn('idmenu', $ml)
			->where('idcard', Session::get('noid'))
			->where('isactive', 1)
			->get();
		$r = static::getPrivilegeMenu(['data'=>$rules]);
			
		$fixmenulevel = [];
		foreach ($menulevel as $k => $v) {
			if (in_array($v->noid, $r) || static::isRoot()) {
				$fixmenulevel[] = $v;
			}
		}

		return $fixmenulevel;
	}
}
