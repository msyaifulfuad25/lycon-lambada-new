<?php

namespace App\Helpers;
use App\Model\Purchase\PurchaseRequest;
use DB;

class MyHelper{

    public static function getRomawi($i) {
        $data = [
            'I',
            'II',
            'III',
            'IV',
            'V',
            'VI',
            'VII',
            'VIII',
            'IX',
            'X',
        ];
        return $data[$i];
    }

    public static function currencyConv($value) {
        return 'Rp. '.number_format(filter_var((int)$value, FILTER_SANITIZE_NUMBER_INT),0,',','.');
    }

    public static function nextChar($params) {
        ini_set('max_execution_time', 3000);
        // $params = [
        //     'char' => 'G',
        //     'next' => 1200
        // ];

        $alphabets = [];
        $alphabetsreverse = [];
        $plus = 0;
        for ($i=0; $i<26; $i++) {
            $alphabets[] = chr(ord('A')+$i);
            $alphabetsreverse[chr(ord('A')+$i)] = $plus;
            $plus++;
        }

        for ($i=0; $i<26; $i++) {
            for ($j=0; $j<26; $j++) {
                $alphabets[] = chr(ord('A')+$i).chr(ord('A')+$j);
                $alphabetsreverse[chr(ord('A')+$i).chr(ord('A')+$j)] = $plus;
                $plus++;
            }
        }

        for ($i=0; $i<26; $i++) {
            for ($j=0; $j<26; $j++) {
                for ($k=0; $k<26; $k++) {
                    $alphabets[] = chr(ord('A')+$i).chr(ord('A')+$j).chr(ord('A')+$k);
                    $alphabetsreverse[chr(ord('A')+$i).chr(ord('A')+$j).chr(ord('A')+$k)] = $plus;
                    $plus++;
                }
            }
        }
        // dd($alphabets);

        // dd($alphabets[$alphabetsreverse[$params['char']]+$params['next']]);

        return $alphabets[$alphabetsreverse[$params['char']]+$params['next']];

        // dd(ord($params['char'])+$params['next']);
        if (ord($params['char'])+$params['next'] > 90) {
            if (ord($params['char'])+$params['next'] > 116) {
                dd('B'.chr(ord($params['char'])+$params['next']));
                return 'B'.chr(ord($params['char'])+$params['next']-26);
            }
            return 'A'.chr(ord($params['char'])+$params['next']-26);
        } else {
            return chr(ord($params['char']) + $params['next']);
        }
    }

    public static function getPrivilege($params) {
        $isroot = $params['isroot'];
        $resultprivilidge = PurchaseRequest::getData(['table'=>'mcardprivilidge','select'=>['*'],'where'=>[['idmenu','=',$params['idmenu']],['idcard','=',$params['idcard']]],'isactive'=>true]);

        if (count($resultprivilidge) == 0) {
            return [
                'isvisible' => $isroot,
                'isenable' => $isroot,
                'isadd' => $isroot,
                'isedit' => $isroot,
                'isdelete' => $isroot,
                'isdeleteother' => $isroot,
                'isview' => $isroot,
                'isreport' => $isroot,
                'isreportdetail' => $isroot,
                'isshowlog' => $isroot,
                'isexporttoxcl' => $isroot,
                'isexporttocsv' => $isroot,
                'isexporttopdf' => $isroot,
                'isexporttohtml' => $isroot,
            ];
        } else {
            $temprp = (array)$resultprivilidge[0];

            $rp = [];
            $rp['isvisible'] = $isroot || ($temprp['isvisible'] && !$temprp['isenable']);              // Bisa dilihat, tanpa diklik
            $rp['isenable'] = $isroot || ($temprp['isenable'] && !$temprp['isvisible']);               // Bisa dilihat, bisa diklik
            $rp['isadd'] = $isroot || ($temprp['isenable'] && $temprp['isadd']);                         // Bisa tambah master
            $rp['isedit'] = $isroot || ($temprp['isenable'] && $temprp['isedit']);                       // Bisa edit master
            $rp['isdelete'] = $isroot || ($temprp['isenable'] && $temprp['isdelete']);                   // Bisa delete master
            $rp['isdeleteother'] = $isroot || ($temprp['isenable'] && $temprp['isdeleteother']);         // Bisa delete data dirinya sendiri
            $rp['isview'] = $isroot || ($temprp['isenable'] && $temprp['isview']);                                            // Bisa dilihat
            $rp['isreport'] = $isroot || ($temprp['isenable'] && $temprp['isreport']);                   // Bisa print list datatable sesuai form
            $rp['isreportdetail'] = $isroot || ($temprp['isenable'] && $temprp['isreportdetail']);       // Bisa printout
            $rp['isshowlog'] = $isroot || ($temprp['isenable'] && $temprp['isshowlog']);                                      // Bisa export excel
            $rp['isexporttoxcl'] = $isroot || ($temprp['isenable'] && $temprp['isexporttoxcl']);         // Bisa export excel
            $rp['isexporttocsv'] = $isroot || ($temprp['isenable'] && $temprp['isexporttocsv']);         // Bisa export CSV
            $rp['isexporttopdf'] = $isroot || ($temprp['isenable'] && $temprp['isexporttopdf']);         // Bisa export PDF
            $rp['isexporttohtml'] = $isroot || ($temprp['isenable'] && $temprp['isexporttohtml']);       // Bisa export HTML
            
            return $rp;
        }
    }

    public static function getNewPrivilege($params) {
        $jsonprivilegerules = DB::table('cmsmenu')
            ->select(['jsonprivilegerules'])
            ->where('noid', $params['idmenu'])
            ->first();
        $jsonprivilegerules = @$jsonprivilegerules ? json_decode($jsonprivilegerules->jsonprivilegerules) : [];
        $jp = [];
        if (!is_null($jsonprivilegerules)) {
            foreach ($jsonprivilegerules as $k => $v) {
                $jp[$v->id] = $params['isroot'] ? true : false;
            }
        }

        if (!$params['isroot']) {
            $jsonprivilege = DB::connection('mysql2')
                ->table('mcardprivilidge')
                ->where('idcard', $params['idcard'])
                ->where('idmenu', $params['idmenu'])
                ->where('isactive', 1)
                ->first();
            $jsonprivilege = @$jsonprivilege ? json_decode($jsonprivilege->jsonprivilege) : [];$jpr = [];
            foreach ($jsonprivilege as $k => $v) {
                $jp[$v->id] = $v->status == 1 ? true : false;
            }
        }
        $jp['idmenu'] = $params['idmenu'];

        return $jp;
    }

    public static function humandateTodbdate($date) {
        if ($date) {
            $arrdate = explode('-', $date);
            $arrtime = explode(' ', $date);
            $year = explode(' ',$arrdate[2])[0];
            $month = $arrdate[1];
            $day = $arrdate[0];
            $time = count(explode(' ',$arrdate[2])) == 2 ? explode(' ',$arrdate[2])[1] : '';

            if ($month == 'Jan') {
                $month = '01';
            } else if ($month == 'Feb') {
                $month = '02';
            } else if ($month == 'Mar') {
                $month = '03';
            } else if ($month == 'Apr') {
                $month = '04';
            } else if ($month == 'May') {
                $month = '05';
            } else if ($month == 'Jun') {
                $month = '06';
            } else if ($month == 'Jul') {
                $month = '07';
            } else if ($month == 'Aug') {
                $month = '08';
            } else if ($month == 'Sep') {
                $month = '09';
            } else if ($month == 'Oct') {
                $month = '10';
            } else if ($month == 'Nov') {
                $month = '11';
            } else if ($month == 'Dec') {
                $month = '12';
            }

            return $year.'-'.$month.'-'.$day.' '.$time;
        } else {
            return NULL;
        }
    }

    public static function dbdateTohumandate($date) {
        if ($date) {
            $arrdate = explode('-', $date);
            $arrtime = explode(' ', $date);
            $year = $arrdate[0];
            $month = $arrdate[1];
            $day = explode(' ',$arrdate[2])[0];

            if ($month == '01') {
                $month = 'Jan';
            } else if ($month == '02') {
                $month = 'Feb';
            } else if ($month == '03') {
                $month = 'Mar';
            } else if ($month == '04') {
                $month = 'Apr';
            } else if ($month == '05') {
                $month = 'May';
            } else if ($month == '06') {
                $month = 'Jun';
            } else if ($month == '07') {
                $month = 'Jul';
            } else if ($month == '08') {
                $month = 'Aug';
            } else if ($month == '09') {
                $month = 'Sep';
            } else if ($month == '10') {
                $month = 'Oct';
            } else if ($month == '11') {
                $month = 'Nov';
            } else if ($month == '12') {
                $month = 'Dec';
            }

            return count($arrtime) > 1 ? $day.'-'.$month.'-'.$year.' '.$arrtime[1] : $day.'-'.$month.'-'.$year;
        } else {
            return NULL;
        }
    }

    public static function dbdateTohumandateReporting($type, $date) {
        if ($date) {
            $arrdate = explode('-', $date);
            $arrtime = explode(' ', $date);
            $year = $arrdate[0];
            $month = $arrdate[1];
            $day = explode(' ',$arrdate[2])[0];

            if ($month == '01') {
                $month = 'Jan';
            } else if ($month == '02') {
                $month = 'Feb';
            } else if ($month == '03') {
                $month = 'Mar';
            } else if ($month == '04') {
                $month = 'Apr';
            } else if ($month == '05') {
                $month = 'May';
            } else if ($month == '06') {
                $month = 'Jun';
            } else if ($month == '07') {
                $month = 'Jul';
            } else if ($month == '08') {
                $month = 'Aug';
            } else if ($month == '09') {
                $month = 'Sep';
            } else if ($month == '10') {
                $month = 'Oct';
            } else if ($month == '11') {
                $month = 'Nov';
            } else if ($month == '12') {
                $month = 'Dec';
            }

            if ($type == 'd') {
                return $day.'-'.$month.'-'.$year;
            } else if ($type == 'w') {
                return $month.'-'.$year;
            } else if ($type == 'm') {
                return $month.'-'.$year;
            }
        } else {
            return NULL;
        }
    }

    public static function dbdateTohumandateWithDay($date) {
        if ($date) {
            $arrdate = explode('-', $date);
            $arrtime = explode(' ', $date);
            $year = $arrdate[0];
            $month = $arrdate[1];
            $day = explode(' ',$arrdate[2])[0];

            if ($month == '01') {
                $month = 'Jan';
            } else if ($month == '02') {
                $month = 'Feb';
            } else if ($month == '03') {
                $month = 'Mar';
            } else if ($month == '04') {
                $month = 'Apr';
            } else if ($month == '05') {
                $month = 'May';
            } else if ($month == '06') {
                $month = 'Jun';
            } else if ($month == '07') {
                $month = 'Jul';
            } else if ($month == '08') {
                $month = 'Aug';
            } else if ($month == '09') {
                $month = 'Sep';
            } else if ($month == '10') {
                $month = 'Oct';
            } else if ($month == '11') {
                $month = 'Nov';
            } else if ($month == '12') {
                $month = 'Dec';
            }

            $getday = date('D', strtotime($date));

            switch($getday){
                case 'Sun':
                    $theday = "Minggu";
                break;
         
                case 'Mon':			
                    $theday = "Senin";
                break;
         
                case 'Tue':
                    $theday = "Selasa";
                break;
         
                case 'Wed':
                    $theday = "Rabu";
                break;
         
                case 'Thu':
                    $theday = "Kamis";
                break;
         
                case 'Fri':
                    $theday = "Jumat";
                break;
         
                case 'Sat':
                    $theday = "Sabtu";
                break;
                
                default:
                    $theday = "Tidak di ketahui";		
                break;
            }

            return $theday.', '.$day.'-'.$month.'-'.$year;
            return count($arrtime) > 1 ? $theday.', '.$day.'-'.$month.'-'.$year.' '.$arrtime[1] : $theday.', '.$day.'-'.$month.'-'.$year;
        } else {
            return NULL;
        }
    }
    
}