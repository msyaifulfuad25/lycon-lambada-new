<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
Use DB;

class User extends Authenticatable {
    protected $connection = 'mysql2';
    use Notifiable;
    protected $table = 'mcarduser';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'myusername',
        'idhomepage',
         'mypassword',
         'islogin',
         'lastlogin',
         'lastaction',
         'lastactionurl',
         'lastipaddress',
         'lastnotif',
         'lasttask',
         'lastinbox',
         'lastsms',
         'groupuser',
         'securitylevel',
         'profilepic',
         'thumbnailpic',
         'backgroundpic',
         'idcreate',
         'docreate',
         'idupdate',
         'lastupdate',
    ];



    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'mypassword',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];
    public static  function getAll() {
        $users = DB::table('mcarduser')
                        ->whereIn('menulevel', [1, 2])
                        ->where('isactive',1)
                        ->where('ispublic',1)
                        ->get();
        return $users;
        //   $names = array('1', '2');
        // $this->db->where_in('menulevel', $names);
        // $this->db->where("isactive = 1");
        // $this->db->where("ispublic = 1");
    }

    public static function findData($myusername) {
        $result = DB::connection('mysql2')
                    ->table('mcarduser AS mcu')
                    ->leftJoin('mcard AS mc','mcu.myusername','=','mc.kode')
                    ->leftJoin('mcardemployee AS mce', 'mcu.noid', '=', 'mce.noid')
                    ->select([
                        'mcu.noid AS mcarduser_noid',
                        'mcu.myusername AS mcarduser_myusername',
                        'mcu.mypassword AS mcarduser_mypassword',
                        'mcu.idhomepage AS mcarduser_idhomepage',
                        'mcu.groupuser AS mcarduser_groupuser',
                        'mcu.groupuser AS mcarduser_groupuser',
                        'mce.idcompany AS mcardemployee_idcompany',
                        'mce.iddepartment AS mcardemployee_iddepartment',
                        'mce.idgudang AS mcardemployee_idgudang',
                        'mc.nama AS mcard_nama',
                        'mc.firstname AS mcard_firstname',
                        'mc.middlename AS mcard_middlename',
                        'mc.lastname AS mcard_lastname',
                        'mc.email AS mcard_email',
                        'mc.profilepicture AS mcard_profilepicture',
                    ])
                    ->where('mcu.myusername', $myusername)
                    ->first();
        return $result;
    }

    public static function findPosition($myusername) {
        $result = DB::connection('mysql2')
            ->table('mcardposition AS mcp')
            ->leftJoin('mposition AS mp', 'mp.noid', '=', 'mcp.idposition')
            ->select([
                'mp.nama'
            ])
            ->where('mcp.kodecard', $myusername)
            ->get();
        return $result;
    }
}
