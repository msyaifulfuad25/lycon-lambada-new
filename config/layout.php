<?php

return [

    // Self
    'self' => [
        'layout' => 'default', // blank, default
        'rtl' => false, // true, false
    ],

    // Base Layout
    'js' => [
        'breakpoints' => [
            'sm' => 576,
            'md' => 768,
            'lg' => 992,
            'xl' => 1200,
            'xxl' => 1200
        ],
        'colors' => [
            'theme' => [
                'base' => [
                    'white' => '#ffffff',
                    'primary' => '#6993FF',
                    'secondary' => '#E5EAEE',
                    'success' => '#1BC5BD',
                    'info' => '#8950FC',
                    'warning' => '#FFA800',
                    'danger' => '#F64E60',
                    'light' => '#F3F6F9',
                    'dark' => '#212121'
                ],
                'light' => [
                    'white' => '#ffffff',
                    'primary' => '#E1E9FF',
                    'secondary' => '#ECF0F3',
                    'success' => '#C9F7F5',
                    'info' => '#EEE5FF',
                    'warning' => '#FFF4DE',
                    'danger' => '#FFE2E5',
                    'light' => '#F3F6F9',
                    'dark' => '#D6D6E0'
                ],
                'inverse' => [
                    'white' => '#ffffff',
                    'primary' => '#ffffff',
                    'secondary' => '#212121',
                    'success' => '#ffffff',
                    'info' => '#ffffff',
                    'warning' => '#ffffff',
                    'danger' => '#ffffff',
                    'light' => '#464E5F',
                    'dark' => '#ffffff'
                ]
            ],
            'gray' => [
                'gray-100' => '#F3F6F9',
                'gray-200' => '#ECF0F3',
                'gray-300' => '#E5EAEE',
                'gray-400' => '#D6D6E0',
                'gray-500' => '#B5B5C3',
                'gray-600' => '#80808F',
                'gray-700' => '#464E5F',
                'gray-800' => '#1B283F',
                'gray-900' => '#212121'
            ]
        ],
        'font-family' => 'Poppins'
    ],

    // Page loader
    'page-loader' => [
        'type' => '' // default, spinner-message, spinner-logo
    ],

    // Header
    'header' => [
        'self' => [
            'display' => true,
            'width' => 'fluid', // fixed, fluid
            'theme' => 'light', // light, dark
            'fixed' => [
                'desktop' => true,
                'mobile' => true
            ]
        ],

        'menu' => [
            'self' => [
                'display' => true,
                'layout'  => 'default', // tab, default
                'root-arrow' => false, // true, false
            ],

            'desktop' => [
                'arrow' => true,
                'toggle' => 'click',
                'submenu' => [
                    'theme' => 'light',
                    'arrow' => true,
                ]
            ],

            'mobile' => [
                'submenu' => [
                    'theme' => 'dark',
                    'accordion' => true
                ],
            ],
        ]
    ],

    // Subheader
    'subheader' => [
        'display' => true,
        'displayDesc' => true,
        'layout' => 'subheader-v1',
        'fixed' => true,
        'width' => 'fluid', // fixed, fluid
        'clear' => false,
        'layouts' => [
            'subheader-v1' => 'Subheader v1',
            'subheader-v2' => 'Subheader v2',
            'subheader-v3' => 'Subheader v3',
            'subheader-v4' => 'Subheader v4',
        ],
        'style' => 'solid' // transparent, solid. can be transparent only if 'fixed' => false
    ],

    // Content
    'content' => [
        'width' => 'fixed', // fluid, fixed
        'extended' => false, // true, false
    ],

    // Brand
    'brand' => [
        'self' => [
            'theme' => 'dark' // light, dark
        ]
    ],

    // Aside
    'aside' => [
        'self' => [
            'theme' => 'dark', // light, dark
            'display' => true,
            'fixed' => true,
            'minimize' => [
                'toggle' => true, // allow toggle
                'default' => false // default state
            ]
        ],

        'menu' => [
            'dropdown' => false, // ok
            'scroll' => false, // ok
            'submenu' => [
                'accordion' => true, // true, false
                'dropdown' => [
                    'arrow' => true,
                    'hover-timeout' => 500 // in milliseconds
                ]
            ]
        ]
    ],

    // Footer
    'footer' => [
        'width' => 'fluid', // fluid, fixed
        'fixed' => false
    ],

    // Extras
    'extras' => [

        // Search
        'search' => [
            'display' => true,
            'layout' => 'dropdown', // offcanvas, dropdown
            'offcanvas' => [
                'direction' => 'right'
            ],
        ],

        // Notifications
        'notifications' => [
            'display' => true,
            'layout' => 'dropdown', // offcanvas, dropdown
            'dropdown' => [
                'style' => 'dark' // light|dark
            ],
            'offcanvas' => [
                'direction' => 'right'
            ]
        ],

        // Quick Actions
        'quick-actions' => [
            'display' => true,
            'layout' => 'dropdown', // offcanvas, dropdown
            'dropdown' => [
                'style' => 'dark' // light|dark
            ],
            'offcanvas' => [
                'direction' => 'right'
            ]
        ],

        // User
        'user' => [
            'display' => true,
            'layout' => 'offcanvas', // offcanvas, dropdown
            'dropdown' => [
                'style' => 'dark' // light|dark
            ],
            'offcanvas' => [
                'direction' => 'right'
            ]
        ],

        // Languages
        'languages' => [
            'display' => true
        ],

        // Cart
        'cart' => [
            'display' => true,
            'dropdown' => [
                'style' => 'dark' // light|dark
            ]
        ],

        // Quick Panel
        'quick-panel' => [
            'display' => true,
            'offcanvas' => [
                'direction' => 'right'
            ]
        ],

        // Chat
        'chat' => [
            'display' => true,
        ],

        // Page Toolbar
        'toolbar' => [
            'display' => true
        ],

        // Scrolltop
        'scrolltop' => [
            'display' => true
        ]
    ],

    // Demo Assets
    'resources' => [
        'favicon' => 'media/img/logo/favicon.ico',
        'fonts' => [
            'google' => [
                'families' => [
                    'Poppins:300,400,500,600,700'
                ]
            ]
        ],
        'css' => [
            'plugins/global/plugins.bundle.css',
            'plugins/custom/prismjs/prismjs.bundle.css',
            'css/style.bundle.css',
        ],
        'js' => [
            'plugins/global/plugins.bundle.js',
            'plugins/custom/prismjs/prismjs.bundle.js',
            'js/scripts.bundle.js',
        ],
    ],

    'color' => [
        'white' => [
            'primary' => '#FFFFFF',
            'btn' => '#2196F3',
            'info' => '#90CAF9',
            'thead' => '#BBDEFB',
            'td' => '#E3F2FD'
        ],
        'default' => [
            'primary' => '#E1E5EC',
            'btn' => '#2196F3',
            'info' => '#90CAF9',
            'thead' => '#BBDEFB',
            'td' => '#E3F2FD'
        ],
        'dark' => [
            'primary' => '#2F353B',
            'btn' => '#2196F3',
            'info' => '#90CAF9',
            'thead' => '#BBDEFB',
            'td' => '#E3F2FD'
        ],
        'blue' => [
            'primary' => '#3598DC',
            'btn' => '#2196F3',
            'info' => '#90CAF9',
            'thead' => '#BBDEFB',
            'td' => '#E3F2FD'
        ],
        'blue-madison' => [
            'primary' => '#578EBE',
            'btn' => '#2196F3',
            'info' => '#90CAF9',
            'thead' => '#BBDEFB',
            'td' => '#E3F2FD'
        ],
        'blue-chambray' => [
            'primary' => '#2C3E50',
            'btn' => '#2196F3',
            'info' => '#90CAF9',
            'thead' => '#BBDEFB',
            'td' => '#E3F2FD'
        ],
        'blue-ebonyclay' => [
            'primary' => '#22313F',
            'btn' => '#2196F3',
            'info' => '#90CAF9',
            'thead' => '#BBDEFB',
            'td' => '#E3F2FD'
        ],
        'blue-hoki' => [
            'primary' => '#67809F',
            'btn' => '#2196F3',
            'info' => '#90CAF9',
            'thead' => '#BBDEFB',
            'td' => '#E3F2FD'
        ],
        'blue-steel' => [
            'primary' => '#4B77BE',
            'btn' => '#2196F3',
            'info' => '#90CAF9',
            'thead' => '#BBDEFB',
            'td' => '#E3F2FD'
        ],
        'blue-soft' => [
            'primary' => '#4C87B9',
            'btn' => '#2196F3',
            'info' => '#90CAF9',
            'thead' => '#BBDEFB',
            'td' => '#E3F2FD'
        ],
        'blue-dark' => [
            'primary' => '#5E738B',
            'btn' => '#2196F3',
            'info' => '#90CAF9',
            'thead' => '#BBDEFB',
            'td' => '#E3F2FD'
        ],
        'blue-sharp' => [
            'primary' => '#5C9BD1',
            'btn' => '#2196F3',
            'info' => '#90CAF9',
            'thead' => '#BBDEFB',
            'td' => '#E3F2FD'
        ],
        'blue-oleo' => [
            'primary' => '#94A0B2',
            'btn' => '#2196F3',
            'info' => '#90CAF9',
            'thead' => '#BBDEFB',
            'td' => '#E3F2FD'
        ],
        'green' => [
            'primary' => '#26a69a',
            'btn' => '#4CAF50',
            'info' => '#A5D6A7',
            'thead' => '#C8E6C9',
            'td' => '#E8F5E9'
        ],
        'green-meadow' => [
            'primary' => '#1BBC9B',
            'btn' => '#4CAF50',
            'info' => '#A5D6A7',
            'thead' => '#C8E6C9',
            'td' => '#E8F5E9'
        ],
        'green-seagreen' => [
            'primary' => '#1BA39C',
            'btn' => '#4CAF50',
            'info' => '#A5D6A7',
            'thead' => '#C8E6C9',
            'td' => '#E8F5E9'
        ],
        'green-torquoise' => [
            'primary' => '#36D7B7',
            'btn' => '#4CAF50',
            'info' => '#A5D6A7',
            'thead' => '#C8E6C9',
            'td' => '#E8F5E9'
        ],
        'green-haze' => [
            'primary' => '#44B6AE',
            'btn' => '#4CAF50',
            'info' => '#A5D6A7',
            'thead' => '#C8E6C9',
            'td' => '#E8F5E9'
        ],
        'green-jungle' => [
            'primary' => '#26C281',
            'btn' => '#4CAF50',
            'info' => '#A5D6A7',
            'thead' => '#C8E6C9',
            'td' => '#E8F5E9'
        ],
        'green-soft' => [
            'primary' => '#3FABA4',
            'btn' => '#4CAF50',
            'info' => '#A5D6A7',
            'thead' => '#C8E6C9',
            'td' => '#E8F5E9'
        ],
        'green-dark' => [
            'primary' => '#4DB3A2',
            'btn' => '#4CAF50',
            'info' => '#A5D6A7',
            'thead' => '#C8E6C9',
            'td' => '#E8F5E9'
        ],
        'green-sharp' => [
            'primary' => '#2AB4C0',
            'btn' => '#4CAF50',
            'info' => '#A5D6A7',
            'thead' => '#C8E6C9',
            'td' => '#E8F5E9'
        ],
        'green-steel' => [
            'primary' => '#29B4B6',
            'btn' => '#4CAF50',
            'info' => '#A5D6A7',
            'thead' => '#C8E6C9',
            'td' => '#E8F5E9'
        ],
        'grey' => [
            'primary' => '#E5E5E5',
            'btn' => '#607D8B',
            'info' => '#B0BEC5',
            'thead' => '#CFD8DC',
            'td' => '#ECEFF1'
        ],
        'grey-steel' => [
            'primary' => '#E9EDEF',
            'btn' => '#607D8B',
            'info' => '#B0BEC5',
            'thead' => '#CFD8DC',
            'td' => '#ECEFF1'
        ],
        'grey-cararra' => [
            'primary' => '#FAFAFA',
            'btn' => '#607D8B',
            'info' => '#B0BEC5',
            'thead' => '#CFD8DC',
            'td' => '#ECEFF1'
        ],
        'grey-gallery' => [
            'primary' => '#555555',
            'btn' => '#607D8B',
            'info' => '#B0BEC5',
            'thead' => '#CFD8DC',
            'td' => '#ECEFF1'
        ],
        'grey-cascade' => [
            'primary' => '#95A5A6',
            'btn' => '#607D8B',
            'info' => '#B0BEC5',
            'thead' => '#CFD8DC',
            'td' => '#ECEFF1'
        ],
        'grey-silver' => [
            'primary' => '#BFBFBF',
            'btn' => '#607D8B',
            'info' => '#B0BEC5',
            'thead' => '#CFD8DC',
            'td' => '#ECEFF1'
        ],
        'grey-salsa' => [
            'primary' => '#ACB5C3',
            'btn' => '#607D8B',
            'info' => '#B0BEC5',
            'thead' => '#CFD8DC',
            'td' => '#ECEFF1'
        ],
        'grey-salt' => [
            'primary' => '#BFCAD1',
            'btn' => '#607D8B',
            'info' => '#B0BEC5',
            'thead' => '#CFD8DC',
            'td' => '#ECEFF1'
        ],
        'grey-mint' => [
            'primary' => '#525E64',
            'btn' => '#607D8B',
            'info' => '#B0BEC5',
            'thead' => '#CFD8DC',
            'td' => '#ECEFF1'
        ],
        'red' => [
            'primary' => '#cb5a5e',
            'btn' => '#F44336',
            'info' => '#EF9A9A',
            'thead' => '#FFCDD2',
            'td' => '#FFEBEE'
        ],
        'red-pink' => [
            'primary' => '#E08283',
            'btn' => '#F44336',
            'info' => '#EF9A9A',
            'thead' => '#FFCDD2',
            'td' => '#FFEBEE'
        ],
        'red-sunglo' => [
            'primary' => '#E26A6A',
            'btn' => '#F44336',
            'info' => '#EF9A9A',
            'thead' => '#FFCDD2',
            'td' => '#FFEBEE'
        ],
        'red-intense' => [
            'primary' => '#E35B5A',
            'btn' => '#F44336',
            'info' => '#EF9A9A',
            'thead' => '#FFCDD2',
            'td' => '#FFEBEE'
        ],
        'red-thunderbird' => [
            'primary' => '#D91E18',
            'btn' => '#F44336',
            'info' => '#EF9A9A',
            'thead' => '#FFCDD2',
            'td' => '#FFEBEE'
        ],
        'red-flamingo' => [
            'primary' => '#EF4836',
            'btn' => '#F44336',
            'info' => '#EF9A9A',
            'thead' => '#FFCDD2',
            'td' => '#FFEBEE'
        ],
        'red-soft' => [
            'primary' => '#D05454',
            'btn' => '#F44336',
            'info' => '#EF9A9A',
            'thead' => '#FFCDD2',
            'td' => '#FFEBEE'
        ],
        'red-haze' => [
            'primary' => '#F36A5A',
            'btn' => '#F44336',
            'info' => '#EF9A9A',
            'thead' => '#FFCDD2',
            'td' => '#FFEBEE'
        ],
        'red-mint' => [
            'primary' => '#E43A45',
            'btn' => '#F44336',
            'info' => '#EF9A9A',
            'thead' => '#FFCDD2',
            'td' => '#FFEBEE'
        ],
        'yellow' => [
            'primary' => '#C49F47',
            'btn' => '#FF9800',
            'info' => '#FFCC80',
            'thead' => '#FFE0B2',
            'td' => '#FFF3E0'
        ],
        'yellow-gold' => [
            'primary' => '#E87E04',
            'btn' => '#FF9800',
            'info' => '#FFCC80',
            'thead' => '#FFE0B2',
            'td' => '#FFF3E0'
        ],
        'yellow-casablanca' => [
            'primary' => '#F2784B',
            'btn' => '#FF9800',
            'info' => '#FFCC80',
            'thead' => '#FFE0B2',
            'td' => '#FFF3E0'
        ],
        'yellow-crusta' => [
            'primary' => '#F3C200',
            'btn' => '#FF9800',
            'info' => '#FFCC80',
            'thead' => '#FFE0B2',
            'td' => '#FFF3E0'
        ],
        'yellow-lemon' => [
            'primary' => '#F7CA18',
            'btn' => '#FF9800',
            'info' => '#FFCC80',
            'thead' => '#FFE0B2',
            'td' => '#FFF3E0'
        ],
        'yellow-saffron' => [
            'primary' => '#F4D03F',
            'btn' => '#FF9800',
            'info' => '#FFCC80',
            'thead' => '#FFE0B2',
            'td' => '#FFF3E0'
        ],
        'yellow-soft' => [
            'primary' => '#C8D046',
            'btn' => '#FF9800',
            'info' => '#FFCC80',
            'thead' => '#FFE0B2',
            'td' => '#FFF3E0'
        ],
        'yellow-haze' => [
            'primary' => '#C5BF66',
            'btn' => '#FF9800',
            'info' => '#FFCC80',
            'thead' => '#FFE0B2',
            'td' => '#FFF3E0'
        ],
        'yellow-mint' => [
            'primary' => '#C5B96B',
            'btn' => '#FF9800',
            'info' => '#FFCC80',
            'thead' => '#FFE0B2',
            'td' => '#FFF3E0'
        ],
        'purple' => [
            'primary' => '#8e5fa2',
            'btn' => '#9C27B0',
            'info' => '#CE93D8',
            'thead' => '#E1BEE7',
            'td' => '#F3E5F5'
        ],
        'purple-plum' => [
            'primary' => '#8775A7',
            'btn' => '#9C27B0',
            'info' => '#CE93D8',
            'thead' => '#E1BEE7',
            'td' => '#F3E5F5'
        ],
        'purple-medium' => [
            'primary' => '#BF55EC',
            'btn' => '#9C27B0',
            'info' => '#CE93D8',
            'thead' => '#E1BEE7',
            'td' => '#F3E5F5'
        ],
        'purple-studio' => [
            'primary' => '#8E44AD',
            'btn' => '#9C27B0',
            'info' => '#CE93D8',
            'thead' => '#E1BEE7',
            'td' => '#F3E5F5'
        ],
        'purple-wisteria' => [
            'primary' => '#9B59B6',
            'btn' => '#9C27B0',
            'info' => '#CE93D8',
            'thead' => '#E1BEE7',
            'td' => '#F3E5F5'
        ],
        'purple-seance' => [
            'primary' => '#9A12B3',
            'btn' => '#9C27B0',
            'info' => '#CE93D8',
            'thead' => '#E1BEE7',
            'td' => '#F3E5F5'
        ],
        'purple-intense' => [
            'primary' => '#8775A7',
            'btn' => '#9C27B0',
            'info' => '#CE93D8',
            'thead' => '#E1BEE7',
            'td' => '#F3E5F5'
        ],
        'purple-sharp' => [
            'primary' => '#796799',
            'btn' => '#9C27B0',
            'info' => '#CE93D8',
            'thead' => '#E1BEE7',
            'td' => '#F3E5F5'
        ],
        'purple-soft' => [
            'primary' => '#8877A9',
            'btn' => '#9C27B0',
            'info' => '#CE93D8',
            'thead' => '#E1BEE7',
            'td' => '#F3E5F5'
        ],
    ]

];
